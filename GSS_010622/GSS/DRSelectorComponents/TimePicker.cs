using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace DRSelectorComponents
{
  /// <summary>
  /// Provides means for selecting time intervals for DR Selectors
  /// </summary>
  public partial class TimePicker : UserControl
  {
    public TimePicker()
    {
      InitializeComponent();
    }

    public DateTime TimeFrom
    {
      get { return dtpFrom.Value; }
      set { dtpFrom.Value = value; }
    }

    public bool FromSelected
    {
      get { return dtpFrom.Checked; }
      set { dtpFrom.Checked = value; }
    }

    public DateTime TimeTo
    {
      get { return dtpTo.Value; }
      set { dtpTo.Value = value; }
    }

    public bool ToSelected
    {
      get { return dtpTo.Checked; }
      set { dtpTo.Checked = value; }
    }

    public int PredefIntervalIndex
    {
      get { return cbIntervals.SelectedIndex; }
      set { cbIntervals.SelectedIndex = value; }
    }
    /// <summary>
    /// Initiate control 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void TimePicker_Load(object sender, EventArgs e)
    {
      cbIntervals.SelectedIndex = 0;
      dtpFrom.Value = DateTime.Now;
      dtpTo.Value = DateTime.Now.AddDays(-1);
    }

    /// <summary>
    /// Change date time pickers' values
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void cbIntervals_SelectedIndexChanged(object sender, EventArgs e)
    {
      switch (cbIntervals.SelectedIndex)
      {
        case 0:
          dtpTo.Enabled = true;
          dtpFrom.Enabled = true;
          return;
        case 1:
          dtpFrom.Value = DateTime.Now.AddHours(-1);
          break;
        case 2:
          dtpFrom.Value = DateTime.Now.AddDays(-1);
          break;
        case 3:
          dtpFrom.Value = DateTime.Now.AddDays(-2);
          break;
        case 4:
          dtpFrom.Value = DateTime.Now.AddDays(-3);
          break;
        case 5:
          dtpFrom.Value = DateTime.Now.AddDays(-4);
          break;
        case 6:
          dtpFrom.Value = DateTime.Now.AddDays(-7);
          break;
        case 7:
          dtpFrom.Value = DateTime.Now.AddDays(-14);
          break;
        case 8:
          dtpFrom.Value = DateTime.Now.AddMonths(-1);
          break;
        case 9:
          dtpFrom.Value = DateTime.Now.AddMonths(-2);
          break;
        case 10:
          dtpFrom.Value = DateTime.Now.AddMonths(-4);
          break;
        case 11:
          dtpFrom.Value = DateTime.Now.AddMonths(-6);
          break;
      }
      dtpTo.Value = DateTime.Now;
      dtpTo.Enabled = false;
      dtpFrom.Enabled = false;
    }

    private void dtpTo_ValueChanged(object sender, EventArgs e)
    {
      if (dtpTo.Value < dtpFrom.Value)
        dtpTo.Value = dtpFrom.Value;
    }
  }
}
