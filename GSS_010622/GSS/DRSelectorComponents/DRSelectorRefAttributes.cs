using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml;
using System.Threading;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using DSTU_RecType;

namespace DRSelectorComponents
{
  /// <summary>
  /// Component enabling to select diagnostic records based on TUInstance and time
  /// </summary>
  public partial class DRSelectorRefAttributes : DRSelectorComponents.DRSelectorBase
  {
    #region Members
    protected SqlCommand cmdRefAttributes; //Get list of referential attributes
    protected SqlCommand cmdSelectDR; //Stored procedure selecting DR identifiers into temporary table according to specified parameters
    Dictionary<String, int> refAttributes; //Dictionary of RefAttributes names and their ID's
    #endregion //Members

    #region Construction and initialization
    public DRSelectorRefAttributes() 
      : base() //Call base constructor
    {
      InitializeComponent();
      //Initialize member variables
      InitMembers();
    }

    public DRSelectorRefAttributes(Dictionary<String, Object> selParams)
      : base(selParams) //Call base constructor
    {
      InitializeComponent();
      //Initialize member variables
      InitMembers();
    }

    protected void InitMembers()
    {
      refAttributes = new Dictionary<String, int>();
      //Create SQL commands objects
      cmdSelectDR = new SqlCommand();
      cmdRefAttributes = new SqlCommand();
      //Create and prepare command selecting list of referential attributes
      cmdRefAttributes.CommandType = CommandType.Text;
      cmdRefAttributes.CommandText = DRSelectors.selectRefAttributesList;
      //Create and prepare command selecting DR
      cmdSelectDR.CommandType = CommandType.StoredProcedure;
      cmdSelectDR.CommandText = DRSelectors.selectDRSelectorRefAttributes;
      cmdSelectDR.Parameters.Add("@TUInstances", SqlDbType.NVarChar, 4000);
      cmdSelectDR.Parameters.Add("@HierarchyNames", SqlDbType.NVarChar, 2147483647);
      cmdSelectDR.Parameters.Add("@From", SqlDbType.DateTime);
      cmdSelectDR.Parameters.Add("@To", SqlDbType.DateTime);
      cmdSelectDR.Parameters.Add("@RecTypeMask", SqlDbType.Int);

      cmdSelectDR.Parameters.Add("@Sources", SqlDbType.NVarChar, 255);
      cmdSelectDR.Parameters.Add("@FaultCodes", SqlDbType.NVarChar, 255);
      cmdSelectDR.Parameters.Add("@TitleKeywords", SqlDbType.NVarChar, 255);
      cmdSelectDR.Parameters.Add("@MatchAllKeywords", SqlDbType.Bit);
      cmdSelectDR.Parameters.Add("@Expression", SqlDbType.NVarChar, 2000);
      cmdSelectDR.Parameters.Add("@MaxRows", SqlDbType.Int);
    }

    #endregion //Construction and initialization

    #region Component event handlers
    private void btRefresh_Click(object sender, EventArgs e)
    {
      //Reload the list of TU Instances and referential attributes
      RefreshHierarchyInstancesRefAttr();
    }

    private void btSelect_Click(object sender, EventArgs e)
    {
      //Start asynchronous record load
      LoadRecordsAsync();
    }

    private void DRSelectorRefAttributes_Load(object sender, EventArgs e)
    {
      //Reload the list of TU Instances and referential attributes
      RefreshHierarchyInstancesRefAttr();
      //Set items of referential attributes combo boxes
      SetComboBoxItems();
    }
    #endregion //Component event handlers

    #region Helper functions
    /// <summary>
    /// Selects list of all TU Instances from DB and fills the grid,
    /// Selects list of referential attributes from DB and fills the grid
    /// </summary>
    protected void RefreshHierarchyInstancesRefAttr()
    {
      try
      {
        //Set proper connection string to DDD Helper
        if (dbConnection != null)
          dddHelper.ConnectionString = dbConnection.ConnectionString;

        hierarchyTUInstances.FleetHierarchy.ShowCurrentHierarchy(dddHelper.HierarchyHelper, null);
        hierarchyTUInstances.TUInstancesList.ShowTUInstances(dddHelper);
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRSelectorException(ex, "Failed to load fleet hierarchy or TU Instances list"));
      }

      try
      {
        //Load list of referential attributes
        //Set command parameters
        cmdRefAttributes.CommandTimeout = sqlCmdTimeout;
        cmdRefAttributes.Connection = dbConnection;
        //Execute command
        SqlDataReader refAttrReader = cmdRefAttributes.ExecuteReader();
        //Process returned rows
        refAttributes.Clear();
        while (refAttrReader.Read())
          refAttributes.Add(refAttrReader.GetString(1), refAttrReader.GetInt32(0));
        refAttrReader.Close();
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRSelectorException(ex, "Failed to load list of referential attributes from DB"));
      }
    }

    /// <summary>
    /// Sets combo box items according to selected referential attributes from DB
    /// </summary>
    private void SetComboBoxItems()
    {
      //First set first item as selected for all operator combo boxes
      cbOper1.SelectedIndex = 0;
      cbOper2.SelectedIndex = 0;
      cbOper3.SelectedIndex = 0;
      cbOper4.SelectedIndex = 0;
      cbOper5.SelectedIndex = 0;
      cbOper6.SelectedIndex = 0;
      //Clear all ref attributes combo boxes and set add first empty item
      cbRefAttr1.Items.Clear();
      cbRefAttr2.Items.Clear();
      cbRefAttr3.Items.Clear();
      cbRefAttr4.Items.Clear();
      cbRefAttr5.Items.Clear();
      cbRefAttr6.Items.Clear();
      cbRefAttr1.Items.Add("");
      cbRefAttr2.Items.Add("");
      cbRefAttr3.Items.Add("");
      cbRefAttr4.Items.Add("");
      cbRefAttr5.Items.Add("");
      cbRefAttr6.Items.Add("");
      //Now set list of referential attributes for each combo box
      foreach (KeyValuePair<String, int> refAttrPair in refAttributes)
      {
        cbRefAttr1.Items.Add(refAttrPair.Key);
        cbRefAttr2.Items.Add(refAttrPair.Key);
        cbRefAttr3.Items.Add(refAttrPair.Key);
        cbRefAttr4.Items.Add(refAttrPair.Key);
        cbRefAttr5.Items.Add(refAttrPair.Key);
        cbRefAttr6.Items.Add(refAttrPair.Key);
      }
    }

    /// <summary>
    /// Creates string representing expression tree from values entered by the user into selector GUI
    /// </summary>
    /// <returns>String representing expression tree</returns>
    private String BuildExpressionString()
    {
      List<ExpTerm> ORList1 = new List<ExpTerm>();
      List<ExpTerm> ORList2 = new List<ExpTerm>();
      List<ExpTerm> ANDList = new List<ExpTerm>();
      ExpTerm TopExp = null;

      //First go throug all GUI controls, create elementary expression terms and store into OR lists
      if (cbRefAttr1.Text != "")
        ORList1.Add(CreateElemExpTerm(cbRefAttr1.Text, cbOper1.Text, tbVal1.Text));
      if (cbRefAttr2.Text != "")
        ORList1.Add(CreateElemExpTerm(cbRefAttr2.Text, cbOper2.Text, tbVal2.Text));
      if (cbRefAttr3.Text != "")
        ORList1.Add(CreateElemExpTerm(cbRefAttr3.Text, cbOper3.Text, tbVal3.Text));
      if (cbRefAttr4.Text != "")
        ORList2.Add(CreateElemExpTerm(cbRefAttr4.Text, cbOper4.Text, tbVal4.Text));
      if (cbRefAttr5.Text != "")
        ORList2.Add(CreateElemExpTerm(cbRefAttr5.Text, cbOper5.Text, tbVal5.Text));
      if (cbRefAttr6.Text != "")
        ORList2.Add(CreateElemExpTerm(cbRefAttr6.Text, cbOper6.Text, tbVal6.Text));
      //Now create terms in AND list
      if (ORList1.Count > 1)
      { //Create new OR expression term
        ExpTerm ORTerm = new ExpTerm();
        ORTerm.BoolOper = "OR";
        foreach (ExpTerm child in ORList1)
          ORTerm.AddChild(child);
        ANDList.Add(ORTerm);
      }
      else if (ORList1.Count == 1)
        ANDList.Add(ORList1[0]);
      if (ORList2.Count > 1)
      { //Create new OR expression term
        ExpTerm ORTerm = new ExpTerm();
        ORTerm.BoolOper = "OR";
        foreach (ExpTerm child in ORList2)
          ORTerm.AddChild(child);
        ANDList.Add(ORTerm);
      }
      else if (ORList2.Count == 1)
        ANDList.Add(ORList2[0]);
      //Create top level term from AND List
      if (ANDList.Count > 1)
      { //Create new AND expression term
        ExpTerm ANDTerm = new ExpTerm();
        ANDTerm.BoolOper = "AND";
        foreach (ExpTerm child in ANDList)
          ANDTerm.AddChild(child);
        TopExp = ANDTerm;
      }
      else if (ANDList.Count == 1)
        TopExp = ANDList[0];
      //Return resulting string representing expression tree
      int expID = 1;
      if (TopExp == null)
        return "";
      else
        return TopExp.GenerateStrTerm(ref expID, null, 1);
    }

    /// <summary>
    /// Constructs ExpTerm object for one elementary expression represented by one set of GUI controls
    /// </summary>
    private ExpTerm CreateElemExpTerm(String refAttrName, String compOper, String value)
    {
      ExpTerm term = new ExpTerm();
      term.CompOper = compOper;
      double val;
      if (!double.TryParse(value, out val))
        throw new DRSelectorException("{0} is not valid float number", value);
      term.Val1 = val;
      //Determine refAttrID from refAttrName
      term.RefAttrID = refAttributes[refAttrName];
      return term;
    }
    #endregion //Helper functions

    #region Virtual method overrides
    /// <summary>
    /// Method reads values from internal components and prepares DRLoadParametersRefAttributes object
    /// </summary>
    /// <returns>Object containing all necessary parameters for DR selection</returns>
    protected override DRLoadParameters PrepareLoadParameters()
    {
      //Create parameters object
      DRLoadParametersRefAttributes loadParams = new DRLoadParametersRefAttributes();
      //Obtain string containing selected TUInstanceIDs
      loadParams.selTUInstances = hierarchyTUInstances.TUInstancesList.GetSelectedInstanceIDs();
      //Obtain string containing XML with selected hierarchy items
      loadParams.selHierarchyItems = hierarchyTUInstances.FleetHierarchy.GetSelectedNamesXML();
      //Set time parameters
      if (dtInterval.FromSelected)
        loadParams.timeFrom = dddHelper.UserTimeToUTC(dtInterval.TimeFrom);
      else
        loadParams.timeFrom = DBNull.Value;
      if (dtInterval.ToSelected)
        loadParams.timeTo = dddHelper.UserTimeToUTC(dtInterval.TimeTo);
      else
        loadParams.timeTo = DBNull.Value;
      //Set mask for record types
      loadParams.recTypeMask = 0;
      if (chbEventRec.Checked)
        loadParams.recTypeMask |= 0x02;
      if (chbTraceRec.Checked)
        loadParams.recTypeMask |= 0x04;
      if (chbSnapRec.Checked)
        loadParams.recTypeMask |= 0x08;
      //Set fault codes
      loadParams.faultCodes = String.Join(" ", tbFaultCodes.Text.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries));
      //Set sources
      loadParams.sources = String.Join(" ", tbRecSources.Text.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries));
      //Set title keywords
      loadParams.titleKeywords = String.Join(" ", tbTitleKeyWords.Text.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries));
      //Set match all
      loadParams.matchAllKeywords = chbMatchAllKeywords.Checked;
      //Create and store expression tree in string form
      loadParams.expression = BuildExpressionString();
      return loadParams;
    }

    /// <summary>
    /// Method is invoked asynchronously and performs the load of DR.
    /// </summary>
    /// <param name="loadParams">Object containing all necessary parameters for DR selection</param>
    /// <param name="asyncOp">AsyncOperation used to post callbacks</param>
    protected override void PerformAsyncDRLoad(DRLoadParameters loadParams, AsyncOperation asyncOp)
    {
      try
      {
        //Cast parameters object to proper type
        DRLoadParametersRefAttributes parameters = loadParams as DRLoadParametersRefAttributes;
        //Check connection to database
        if (!CheckDBConnection())
          throw new DRSelectorException("Connection to database could not be opened");

        //Select data from database
        //Set command parameters
        cmdSelectDR.Parameters["@From"].Value = parameters.timeFrom;
        cmdSelectDR.Parameters["@To"].Value = parameters.timeTo;
        if (parameters.selTUInstances != String.Empty)
          cmdSelectDR.Parameters["@TUInstances"].Value = parameters.selTUInstances;
        else
          cmdSelectDR.Parameters["@TUInstances"].Value = DBNull.Value;
        if (parameters.selHierarchyItems != String.Empty)
          cmdSelectDR.Parameters["@HierarchyNames"].Value = parameters.selHierarchyItems;
        else
          cmdSelectDR.Parameters["@HierarchyNames"].Value = DBNull.Value;
        cmdSelectDR.Parameters["@RecTypeMask"].Value = parameters.recTypeMask;
        if (parameters.faultCodes != "")
          cmdSelectDR.Parameters["@FaultCodes"].Value = parameters.faultCodes;
        else
          cmdSelectDR.Parameters["@FaultCodes"].Value = DBNull.Value;
        if (parameters.sources != "")
          cmdSelectDR.Parameters["@Sources"].Value = parameters.sources;
        else
          cmdSelectDR.Parameters["@Sources"].Value = DBNull.Value;
        if (parameters.titleKeywords != "")
          cmdSelectDR.Parameters["@TitleKeywords"].Value = parameters.titleKeywords;
        else
          cmdSelectDR.Parameters["@TitleKeywords"].Value = DBNull.Value;
        cmdSelectDR.Parameters["@MatchAllKeywords"].Value = parameters.matchAllKeywords;
        if (parameters.expression != "")
          cmdSelectDR.Parameters["@Expression"].Value = parameters.expression;
        else
          cmdSelectDR.Parameters["@Expression"].Value = DBNull.Value;
        cmdSelectDR.Parameters["@MaxRows"].Value = MaxRecCount;
            
        cmdSelectDR.CommandTimeout = sqlCmdTimeout;
        cmdSelectDR.Connection = dbConnection;
        //Execute SQL command
        SqlDataReader reader;
        //XmlReader reader;
        try
        {
          reader = cmdSelectDR.ExecuteReader();
          //reader = cmdSelectDR.ExecuteXmlReader();
        }
        catch (Exception ex)
        {
          throw new DRSelectorException(ex, "Failed to execute DB stored procedure");
        }

        //Post info message
        asyncOp.Post(onInfoMessageDelegate, "Loading records to data set");
        //Process returned XML document and fill internal dataset
        try
        {
          FillDataSetFromResultSets(reader, asyncOp);
          //FillDataSetFromXml(reader, asyncOp);
        }
        catch (Exception)
        {
          throw;
        }
        finally
        {
          //Close reader object
          reader.Close();
        }

        //Post completed event
        AsyncCompletedEventArgs result = new AsyncCompletedEventArgs(null, false, null);
        asyncOp.PostOperationCompleted(onDRLoadCompletedDelegate, result);
      }
      catch (Exception ex)
      {
        //Post completed event with error
        AsyncCompletedEventArgs result = new AsyncCompletedEventArgs(ex, false, null);
        asyncOp.PostOperationCompleted(onDRLoadCompletedDelegate, result);
      }
    }

    /// <summary>
    /// Saves settings of selector to file
    /// </summary>
    /// <param name="file">Stream to save to</param>
    /// <param name="formatter">Formatter used for saving objects</param>
    public override void SaveSettingsToFile(FileStream file, BinaryFormatter formatter)
    {
      //Serialize
      formatter.Serialize(file, hierarchyTUInstances.TUInstancesList.GetSelectedInstanceIDs());
      formatter.Serialize(file, hierarchyTUInstances.FleetHierarchy.GetSelectedNamesXML());
      formatter.Serialize(file, dtInterval.TimeFrom);
      formatter.Serialize(file, dtInterval.FromSelected);
      formatter.Serialize(file, dtInterval.TimeTo);
      formatter.Serialize(file, dtInterval.ToSelected);
      formatter.Serialize(file, dtInterval.PredefIntervalIndex);
      formatter.Serialize(file, chbEventRec.Checked);
      formatter.Serialize(file, chbSnapRec.Checked);
      formatter.Serialize(file, chbTraceRec.Checked);

      formatter.Serialize(file, tbRecSources.Text);
      formatter.Serialize(file, tbFaultCodes.Text);
      formatter.Serialize(file, tbTitleKeyWords.Text);
      formatter.Serialize(file, chbMatchAllKeywords.Checked);

      formatter.Serialize(file, cbRefAttr1.Text);
      formatter.Serialize(file, cbOper1.Text);
      formatter.Serialize(file, tbVal1.Text);
      formatter.Serialize(file, cbRefAttr2.Text);
      formatter.Serialize(file, cbOper2.Text);
      formatter.Serialize(file, tbVal2.Text);
      formatter.Serialize(file, cbRefAttr3.Text);
      formatter.Serialize(file, cbOper3.Text);
      formatter.Serialize(file, tbVal3.Text);
      formatter.Serialize(file, cbRefAttr4.Text);
      formatter.Serialize(file, cbOper4.Text);
      formatter.Serialize(file, tbVal4.Text);
      formatter.Serialize(file, cbRefAttr5.Text);
      formatter.Serialize(file, cbOper5.Text);
      formatter.Serialize(file, tbVal5.Text);
      formatter.Serialize(file, cbRefAttr6.Text);
      formatter.Serialize(file, cbOper6.Text);
      formatter.Serialize(file, tbVal6.Text);
    }

    /// <summary>
    /// Loads settings of selector from file
    /// </summary>
    /// <param name="file">Stream to save to</param>
    /// <param name="formatter">Formatter used for saving objects</param>
    public override void LoadSettingsFromFile(FileStream file, BinaryFormatter formatter)
    {
      //Check TU Instances
      hierarchyTUInstances.TUInstancesList.CheckTUInstances((String)formatter.Deserialize(file));
      //Check fleet hierarchy items
      hierarchyTUInstances.FleetHierarchy.CheckHierarchyItems((String)formatter.Deserialize(file));
      //Retrieve rest of parameters
      dtInterval.TimeFrom = (DateTime)formatter.Deserialize(file);
      dtInterval.FromSelected = (bool)formatter.Deserialize(file);
      dtInterval.TimeTo = (DateTime)formatter.Deserialize(file);
      dtInterval.ToSelected = (bool)formatter.Deserialize(file);
      dtInterval.PredefIntervalIndex = (int)formatter.Deserialize(file);
      chbEventRec.Checked = (bool)formatter.Deserialize(file);
      chbSnapRec.Checked = (bool)formatter.Deserialize(file);
      chbTraceRec.Checked = (bool)formatter.Deserialize(file);

      tbRecSources.Text = (String)formatter.Deserialize(file);
      tbFaultCodes.Text = (String)formatter.Deserialize(file);
      tbTitleKeyWords.Text = (String)formatter.Deserialize(file);
      chbMatchAllKeywords.Checked = (bool)formatter.Deserialize(file);
      
      cbRefAttr1.SelectedItem = (String)formatter.Deserialize(file);
      cbOper1.SelectedItem = (String)formatter.Deserialize(file);
      tbVal1.Text = (String)formatter.Deserialize(file);
      cbRefAttr2.SelectedItem = (String)formatter.Deserialize(file);
      cbOper2.SelectedItem = (String)formatter.Deserialize(file);
      tbVal2.Text = (String)formatter.Deserialize(file);
      cbRefAttr3.SelectedItem = (String)formatter.Deserialize(file);
      cbOper3.SelectedItem = (String)formatter.Deserialize(file);
      tbVal3.Text = (String)formatter.Deserialize(file);
      cbRefAttr4.SelectedItem = (String)formatter.Deserialize(file);
      cbOper4.SelectedItem = (String)formatter.Deserialize(file);
      tbVal4.Text = (String)formatter.Deserialize(file);
      cbRefAttr5.SelectedItem = (String)formatter.Deserialize(file);
      cbOper5.SelectedItem = (String)formatter.Deserialize(file);
      tbVal5.Text = (String)formatter.Deserialize(file);
      cbRefAttr6.SelectedItem = (String)formatter.Deserialize(file);
      cbOper6.SelectedItem = (String)formatter.Deserialize(file);
      tbVal6.Text = (String)formatter.Deserialize(file);
    }
    #endregion //Virtual method overrides
  }

  #region Load parameters class
  /// <summary>
  /// Class representing parameters for DR Load operation.
  /// </summary>
  public class DRLoadParametersRefAttributes : DRLoadParameters
  {
    public Object timeFrom; //start of the interval
    public Object timeTo; //end of the interval
    public String selTUInstances; //list of selected TU Instances
    public String selHierarchyItems; //XML with selected hierarchy items
    public int recTypeMask = 0; //Mask for record types
    public String sources; //list of sources
    public String faultCodes; //list of faultCodes
    public String titleKeywords; //list of keywords for record title
    public bool matchAllKeywords = false; //all title keywords has to match
    public String expression = ""; //string representing expression which must values of referentail attributes match
  }
  #endregion //Load parameters class

  #region ExpTerm class
  /// <summary>
  /// Represents one expression term, contains references to child expressions
  /// </summary>
  internal class ExpTerm
  {
    public Object BoolOper = null;
    public Object CompOper = null;
    public Object RefAttrID = null;
    public Object Val1 = null;
    public Object Val2 = null;

    private List<ExpTerm> childTerms;

    public ExpTerm()
    {
      childTerms = new List<ExpTerm>();
    }

    public void AddChild(ExpTerm childTerm)
    {
      childTerms.Add(childTerm);
    }

    /// <summary>
    /// Generates string representing expression term, together with all child terms
    /// </summary>
    /// <param name="ExpID">Unique ID assigned to the term</param>
    /// <param name="ParentExpID">ID of parent term</param>
    /// <param name="Level">Level of nesting for child expressions</param>
    /// <returns></returns>
    public String GenerateStrTerm(ref int ExpID, Object ParentExpID, int Level)
    {
      NumberFormatInfo formatInfo = new NumberFormatInfo();
      formatInfo.NumberDecimalSeparator = ".";
      String strTerm = "";
      strTerm += ExpID.ToString() + "|";
      if (ParentExpID != null)
        strTerm += ParentExpID.ToString();
      strTerm += "|";
      strTerm += Level.ToString() + "|";
      if (BoolOper != null)
        strTerm += BoolOper.ToString();
      strTerm += "|";
      strTerm += childTerms.Count.ToString() + "|";
      if (CompOper != null)
        strTerm += CompOper.ToString();
      strTerm += "|";
      if (RefAttrID != null)
        strTerm += RefAttrID.ToString();
      strTerm += "|";
      if (Val1 != null)
        strTerm += ((double)Val1).ToString(formatInfo);
      strTerm += "|";
      if (Val2 != null)
        strTerm += ((double)Val2).ToString(formatInfo);
      strTerm += "|";

      //Now append string for all child expressions
      int locExpID = ExpID;
      ExpID++;
      foreach(ExpTerm childTerm in childTerms)
        strTerm += childTerm.GenerateStrTerm(ref ExpID, locExpID, Level+1);
      return strTerm;
    }
  }
  #endregion
}

