using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml;
using System.Threading;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DRSelectorComponents
{
  /// <summary>
  /// Component enabling to select diagnostic records based on TUInstance and time
  /// </summary>
  public partial class DRSelectorFile : DRSelectorComponents.DRSelectorBase
  {
    #region Public properties
    /// <summary>
    /// Name of selected file
    /// </summary>
    public String FileName
    {
      get { return tbFileName.Text; }
      set { tbFileName.Text = value; }
    }
    #endregion //Public properties

    #region Construction and initialization
    public DRSelectorFile() 
      : base() //Call base constructor
    {
      InitializeComponent();
    }

    public DRSelectorFile(Dictionary<String, Object> selParams)
      : base(selParams) //Call base constructor
    {
      InitializeComponent();
    }
    #endregion //Construction and initialization

    #region Component event handlers
    private void btLoadFile_Click(object sender, EventArgs e)
    {
      //Start asynchronous record load
      LoadRecordsAsync();
      //Set form title
      this.ParentForm.Text = tbFileName.Text;
    }

    /// <summary>
    /// Opens dialog for selecting files
    /// </summary>
    private void btSelectFile_Click(object sender, EventArgs e)
    {
      //Prepare load file dialog
      dlgOpenFile.Reset();
      dlgOpenFile.DefaultExt = DRSelectors.DRFileExtension;
      dlgOpenFile.Filter = DRSelectors.DRFilesFilter;
      //Show file open dialog
      if (dlgOpenFile.ShowDialog() == DialogResult.OK)
      { //file was selected
        tbFileName.Text = dlgOpenFile.FileName;
        //Load records from file
        btLoadFile_Click(this, null);
      }
    }
    
    private void DRSelectorFile_Load(object sender, EventArgs e)
    {
      if (FileName == "")
        btSelectFile_Click(this, null); //No file selected, automatically open file dialog
      else
        btLoadFile_Click(this, null); //File already selected - load records from file
    }
    #endregion //Component event handlers

    #region Virtual method overrides
    /// <summary>
    /// Method reads values from internal components and prepares DRLoadParametersFile object
    /// </summary>
    /// <returns>Object containing all necessary parameters for DR selection</returns>
    protected override DRLoadParameters PrepareLoadParameters()
    {
      //Create parameters object
      DRLoadParametersFile loadParams = new DRLoadParametersFile();
      //Set name of selected file
      loadParams.fileName = tbFileName.Text;
      String extension = Path.GetExtension(tbFileName.Text);
      switch (extension.ToUpper())
      {
        case ".DR":
          loadParams.fileType = eRecFileType.BinaryFile;
          break;
        case ".XGZ":
          loadParams.fileType = eRecFileType.XMLFile;
          break;
        default:
          loadParams.fileType = eRecFileType.BinaryFile;
          break;
      }
      return loadParams;
    }

    /// <summary>
    /// Method is invoked asynchronously and performs the load of DR.
    /// </summary>
    /// <param name="loadParams">Object containing all necessary parameters for DR selection</param>
    /// <param name="asyncOp">AsyncOperation used to post callbacks</param>
    protected override void PerformAsyncDRLoad(DRLoadParameters loadParams, AsyncOperation asyncOp)
    {
      try
      {
        //Cast parameters object to proper type
        DRLoadParametersFile parameters = loadParams as DRLoadParametersFile;
        //Check if file name is specified
        if (!File.Exists(parameters.fileName))
          throw new DRSelectorException("Invalid file name specified");
        //Post info message
        asyncOp.Post(onInfoMessageDelegate, "Loading records to data set");
        //Try to load records from file
        try
        {
          LoadRecordsFromFile((String)(parameters.fileName), parameters.fileType);
        }
        catch (Exception)
        {
          throw;
        }
        //Post completed event
        AsyncCompletedEventArgs result = new AsyncCompletedEventArgs(null, false, null);
        asyncOp.PostOperationCompleted(onDRLoadCompletedDelegate, result);
      }
      catch (Exception ex)
      {
        //Post completed event with error
        AsyncCompletedEventArgs result = new AsyncCompletedEventArgs(ex, false, null);
        asyncOp.PostOperationCompleted(onDRLoadCompletedDelegate, result);
      }
    }

    /// <summary>
    /// Saves settings of selector to file
    /// </summary>
    /// <param name="file">Stream to save to</param>
    /// <param name="formatter">Formatter used for saving objects</param>
    public override void SaveSettingsToFile(FileStream file, BinaryFormatter formatter)
    {
      formatter.Serialize(file, tbFileName.Text);
    }

    /// <summary>
    /// Loads settings of selector from file
    /// </summary>
    /// <param name="file">Stream to save to</param>
    /// <param name="formatter">Formatter used for saving objects</param>
    public override void LoadSettingsFromFile(FileStream file, BinaryFormatter formatter)
    {
      tbFileName.Text = (String)formatter.Deserialize(file);
    }
    #endregion //Virtual method overrides

  }

  #region Load parameters class
  /// <summary>
  /// Class representing parameters for DR Load operation.
  /// </summary>
  public class DRLoadParametersFile : DRLoadParameters
  {
    public String fileName; //name of file to load
    public eRecFileType fileType; //type of file to load
  }
  #endregion //Load parameters class
}

