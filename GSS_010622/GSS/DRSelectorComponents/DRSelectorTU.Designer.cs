namespace DRSelectorComponents
{
  partial class DRSelectorTU
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      this.dgvTU = new System.Windows.Forms.DataGridView();
      this.selectedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
      this.tUNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.tUInstanceIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.tuInstances = new DSTU_RecType.TU_RecType();
      this.btSelect = new System.Windows.Forms.Button();
      this.panel2 = new System.Windows.Forms.Panel();
      this.dtInterval = new DRSelectorComponents.TimePicker();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.gbRecType = new System.Windows.Forms.GroupBox();
      this.chbSnapRec = new System.Windows.Forms.CheckBox();
      this.chbTraceRec = new System.Windows.Forms.CheckBox();
      this.chbEventRec = new System.Windows.Forms.CheckBox();
      ((System.ComponentModel.ISupportInitialize)(this.diagRecords)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.recordsView)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dgvTU)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.tuInstances)).BeginInit();
      this.panel2.SuspendLayout();
      this.gbRecType.SuspendLayout();
      this.SuspendLayout();
      // 
      // dgvTU
      // 
      this.dgvTU.AllowUserToAddRows = false;
      this.dgvTU.AllowUserToDeleteRows = false;
      this.dgvTU.AllowUserToOrderColumns = true;
      this.dgvTU.AllowUserToResizeRows = false;
      this.dgvTU.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.dgvTU.AutoGenerateColumns = false;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
      dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgvTU.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgvTU.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvTU.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.selectedDataGridViewCheckBoxColumn,
            this.tUNameDataGridViewTextBoxColumn,
            this.tUInstanceIDDataGridViewTextBoxColumn});
      this.dgvTU.DataMember = "TU";
      this.dgvTU.DataSource = this.tuInstances;
      this.dgvTU.Location = new System.Drawing.Point(0, 0);
      this.dgvTU.Name = "dgvTU";
      this.dgvTU.RowHeadersVisible = false;
      this.dgvTU.Size = new System.Drawing.Size(461, 91);
      this.dgvTU.TabIndex = 0;
      // 
      // selectedDataGridViewCheckBoxColumn
      // 
      this.selectedDataGridViewCheckBoxColumn.DataPropertyName = "Selected";
      this.selectedDataGridViewCheckBoxColumn.FalseValue = "false";
      this.selectedDataGridViewCheckBoxColumn.HeaderText = "Selected";
      this.selectedDataGridViewCheckBoxColumn.MinimumWidth = 50;
      this.selectedDataGridViewCheckBoxColumn.Name = "selectedDataGridViewCheckBoxColumn";
      this.selectedDataGridViewCheckBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
      this.selectedDataGridViewCheckBoxColumn.ToolTipText = "Selected instances";
      this.selectedDataGridViewCheckBoxColumn.TrueValue = "true";
      this.selectedDataGridViewCheckBoxColumn.Width = 50;
      // 
      // tUNameDataGridViewTextBoxColumn
      // 
      this.tUNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.tUNameDataGridViewTextBoxColumn.DataPropertyName = "TUName";
      this.tUNameDataGridViewTextBoxColumn.HeaderText = "Telediagnostic Unit";
      this.tUNameDataGridViewTextBoxColumn.MinimumWidth = 300;
      this.tUNameDataGridViewTextBoxColumn.Name = "tUNameDataGridViewTextBoxColumn";
      this.tUNameDataGridViewTextBoxColumn.ToolTipText = "Telediagnostic unit name";
      // 
      // tUInstanceIDDataGridViewTextBoxColumn
      // 
      this.tUInstanceIDDataGridViewTextBoxColumn.DataPropertyName = "TUInstanceID";
      this.tUInstanceIDDataGridViewTextBoxColumn.HeaderText = "TUInstanceID";
      this.tUInstanceIDDataGridViewTextBoxColumn.Name = "tUInstanceIDDataGridViewTextBoxColumn";
      this.tUInstanceIDDataGridViewTextBoxColumn.Visible = false;
      // 
      // tuInstances
      // 
      this.tuInstances.DataSetName = "TU_RecType";
      this.tuInstances.Locale = new System.Globalization.CultureInfo("");
      this.tuInstances.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
      // 
      // btSelect
      // 
      this.btSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btSelect.Location = new System.Drawing.Point(467, 16);
      this.btSelect.Name = "btSelect";
      this.btSelect.Size = new System.Drawing.Size(75, 23);
      this.btSelect.TabIndex = 0;
      this.btSelect.Text = "Select";
      this.btSelect.UseVisualStyleBackColor = true;
      this.btSelect.Click += new System.EventHandler(this.btSelect_Click);
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.dtInterval);
      this.panel2.Controls.Add(this.btSelect);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel2.Location = new System.Drawing.Point(0, 98);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(552, 47);
      this.panel2.TabIndex = 9;
      // 
      // dtInterval
      // 
      this.dtInterval.FromSelected = true;
      this.dtInterval.Location = new System.Drawing.Point(3, -1);
      this.dtInterval.Name = "dtInterval";
      this.dtInterval.PredefIntervalIndex = 0;
      this.dtInterval.Size = new System.Drawing.Size(452, 45);
      this.dtInterval.TabIndex = 8;
      this.dtInterval.TimeFrom = new System.DateTime(2007, 3, 16, 16, 24, 42, 503);
      this.dtInterval.TimeTo = new System.DateTime(2007, 3, 16, 16, 24, 42, 503);
      this.dtInterval.ToSelected = true;
      // 
      // splitter1
      // 
      this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter1.Location = new System.Drawing.Point(0, 95);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(552, 3);
      this.splitter1.TabIndex = 10;
      this.splitter1.TabStop = false;
      // 
      // gbRecType
      // 
      this.gbRecType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.gbRecType.Controls.Add(this.chbSnapRec);
      this.gbRecType.Controls.Add(this.chbTraceRec);
      this.gbRecType.Controls.Add(this.chbEventRec);
      this.gbRecType.Location = new System.Drawing.Point(467, 3);
      this.gbRecType.Name = "gbRecType";
      this.gbRecType.Size = new System.Drawing.Size(82, 88);
      this.gbRecType.TabIndex = 11;
      this.gbRecType.TabStop = false;
      this.gbRecType.Text = "Record type";
      // 
      // chbSnapRec
      // 
      this.chbSnapRec.AutoSize = true;
      this.chbSnapRec.Location = new System.Drawing.Point(6, 65);
      this.chbSnapRec.Name = "chbSnapRec";
      this.chbSnapRec.Size = new System.Drawing.Size(51, 17);
      this.chbSnapRec.TabIndex = 2;
      this.chbSnapRec.Text = "Snap";
      this.chbSnapRec.UseVisualStyleBackColor = true;
      // 
      // chbTraceRec
      // 
      this.chbTraceRec.AutoSize = true;
      this.chbTraceRec.Location = new System.Drawing.Point(6, 42);
      this.chbTraceRec.Name = "chbTraceRec";
      this.chbTraceRec.Size = new System.Drawing.Size(54, 17);
      this.chbTraceRec.TabIndex = 1;
      this.chbTraceRec.Text = "Trace";
      this.chbTraceRec.UseVisualStyleBackColor = true;
      // 
      // chbEventRec
      // 
      this.chbEventRec.AutoSize = true;
      this.chbEventRec.Checked = true;
      this.chbEventRec.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chbEventRec.Location = new System.Drawing.Point(6, 19);
      this.chbEventRec.Name = "chbEventRec";
      this.chbEventRec.Size = new System.Drawing.Size(54, 17);
      this.chbEventRec.TabIndex = 0;
      this.chbEventRec.Text = "Event";
      this.chbEventRec.UseVisualStyleBackColor = true;
      // 
      // DRSelectorTU
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.Controls.Add(this.gbRecType);
      this.Controls.Add(this.dgvTU);
      this.Controls.Add(this.splitter1);
      this.Controls.Add(this.panel2);
      this.Name = "DRSelectorTU";
      this.Size = new System.Drawing.Size(552, 145);
      this.Load += new System.EventHandler(this.DRSelectorTU_Load);
      ((System.ComponentModel.ISupportInitialize)(this.diagRecords)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.recordsView)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dgvTU)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.tuInstances)).EndInit();
      this.panel2.ResumeLayout(false);
      this.gbRecType.ResumeLayout(false);
      this.gbRecType.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.DataGridView dgvTU;
    private DSTU_RecType.TU_RecType tuInstances;
    private System.Windows.Forms.Button btSelect;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Splitter splitter1;
    private System.Windows.Forms.DataGridViewCheckBoxColumn selectedDataGridViewCheckBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn tUNameDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn tUInstanceIDDataGridViewTextBoxColumn;
    private TimePicker dtInterval;
    private System.Windows.Forms.GroupBox gbRecType;
    private System.Windows.Forms.CheckBox chbEventRec;
    private System.Windows.Forms.CheckBox chbSnapRec;
    private System.Windows.Forms.CheckBox chbTraceRec;
  }
}
