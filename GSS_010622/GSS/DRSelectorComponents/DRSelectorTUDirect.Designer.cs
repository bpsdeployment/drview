namespace DRSelectorComponents
{
  partial class DRSelectorTUDirect
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.dlgOpenFile = new System.Windows.Forms.OpenFileDialog();
      this.cbTUAddress = new System.Windows.Forms.ComboBox();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.cbScope = new System.Windows.Forms.ComboBox();
      this.label3 = new System.Windows.Forms.Label();
      this.tbSource = new System.Windows.Forms.TextBox();
      this.dtInterval = new DRSelectorComponents.TimePicker();
      this.label4 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.tbMaxRecords = new System.Windows.Forms.MaskedTextBox();
      this.tbMaxRecTypes = new System.Windows.Forms.MaskedTextBox();
      this.tbFaultCode = new System.Windows.Forms.MaskedTextBox();
      this.btSelectRecords = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.diagRecords)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.recordsView)).BeginInit();
      this.SuspendLayout();
      // 
      // cbTUAddress
      // 
      this.cbTUAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.cbTUAddress.FormattingEnabled = true;
      this.cbTUAddress.Location = new System.Drawing.Point(178, 6);
      this.cbTUAddress.Name = "cbTUAddress";
      this.cbTUAddress.Size = new System.Drawing.Size(146, 21);
      this.cbTUAddress.Sorted = true;
      this.cbTUAddress.TabIndex = 0;
      this.cbTUAddress.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbTUAddress_KeyPress);
      this.cbTUAddress.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cbTUAddress_KeyUp);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label1.Location = new System.Drawing.Point(0, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(165, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Telediagnostic Unit address";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(-1, 45);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(70, 13);
      this.label2.TabIndex = 2;
      this.label2.Text = "Query scope:";
      // 
      // cbScope
      // 
      this.cbScope.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbScope.FormattingEnabled = true;
      this.cbScope.Items.AddRange(new object[] {
            "all",
            "severity A",
            "severity A1",
            "severity B",
            "severity B1",
            "severity C",
            "(specify fault code)"});
      this.cbScope.Location = new System.Drawing.Point(177, 42);
      this.cbScope.Name = "cbScope";
      this.cbScope.Size = new System.Drawing.Size(147, 21);
      this.cbScope.TabIndex = 3;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(-1, 98);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(80, 13);
      this.label3.TabIndex = 4;
      this.label3.Text = "Record source:";
      // 
      // tbSource
      // 
      this.tbSource.Location = new System.Drawing.Point(177, 95);
      this.tbSource.Name = "tbSource";
      this.tbSource.Size = new System.Drawing.Size(84, 20);
      this.tbSource.TabIndex = 5;
      // 
      // dtInterval
      // 
      this.dtInterval.FromSelected = true;
      this.dtInterval.Location = new System.Drawing.Point(0, 173);
      this.dtInterval.Name = "dtInterval";
      this.dtInterval.PredefIntervalIndex = 0;
      this.dtInterval.Size = new System.Drawing.Size(458, 45);
      this.dtInterval.TabIndex = 6;
      this.dtInterval.TimeFrom = new System.DateTime(2009, 3, 18, 10, 38, 4, 307);
      this.dtInterval.TimeTo = new System.DateTime(2009, 3, 18, 10, 38, 4, 307);
      this.dtInterval.ToSelected = true;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(-1, 72);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(60, 13);
      this.label4.TabIndex = 7;
      this.label4.Text = "Fault code:";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(-1, 124);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(142, 13);
      this.label5.TabIndex = 10;
      this.label5.Text = "Maximum number of records:";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(0, 150);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(165, 13);
      this.label6.TabIndex = 12;
      this.label6.Text = "Maximum number of record types:";
      // 
      // tbMaxRecords
      // 
      this.tbMaxRecords.Location = new System.Drawing.Point(177, 121);
      this.tbMaxRecords.Mask = "0000000000000";
      this.tbMaxRecords.Name = "tbMaxRecords";
      this.tbMaxRecords.Size = new System.Drawing.Size(84, 20);
      this.tbMaxRecords.TabIndex = 13;
      // 
      // tbMaxRecTypes
      // 
      this.tbMaxRecTypes.Location = new System.Drawing.Point(177, 147);
      this.tbMaxRecTypes.Mask = "0000000000000";
      this.tbMaxRecTypes.Name = "tbMaxRecTypes";
      this.tbMaxRecTypes.Size = new System.Drawing.Size(84, 20);
      this.tbMaxRecTypes.TabIndex = 14;
      // 
      // tbFaultCode
      // 
      this.tbFaultCode.Location = new System.Drawing.Point(177, 69);
      this.tbFaultCode.Mask = "0000000000000";
      this.tbFaultCode.Name = "tbFaultCode";
      this.tbFaultCode.Size = new System.Drawing.Size(84, 20);
      this.tbFaultCode.TabIndex = 15;
      // 
      // btSelectRecords
      // 
      this.btSelectRecords.Location = new System.Drawing.Point(464, 190);
      this.btSelectRecords.Name = "btSelectRecords";
      this.btSelectRecords.Size = new System.Drawing.Size(75, 23);
      this.btSelectRecords.TabIndex = 16;
      this.btSelectRecords.Text = "Select";
      this.btSelectRecords.UseVisualStyleBackColor = true;
      this.btSelectRecords.Click += new System.EventHandler(this.btSelectRecords_Click);
      // 
      // DRSelectorTUDirect
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.Controls.Add(this.btSelectRecords);
      this.Controls.Add(this.tbFaultCode);
      this.Controls.Add(this.tbMaxRecTypes);
      this.Controls.Add(this.tbMaxRecords);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.dtInterval);
      this.Controls.Add(this.tbSource);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.cbScope);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.cbTUAddress);
      this.MinimumSize = new System.Drawing.Size(250, 75);
      this.Name = "DRSelectorTUDirect";
      this.Size = new System.Drawing.Size(546, 223);
      this.Load += new System.EventHandler(this.DRSelectorTUDirect_Load);
      ((System.ComponentModel.ISupportInitialize)(this.diagRecords)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.recordsView)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.OpenFileDialog dlgOpenFile;
    private System.Windows.Forms.ComboBox cbTUAddress;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.ComboBox cbScope;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox tbSource;
    private TimePicker dtInterval;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.MaskedTextBox tbMaxRecords;
    private System.Windows.Forms.MaskedTextBox tbMaxRecTypes;
    private System.Windows.Forms.MaskedTextBox tbFaultCode;
    private System.Windows.Forms.Button btSelectRecords;
  }
}
