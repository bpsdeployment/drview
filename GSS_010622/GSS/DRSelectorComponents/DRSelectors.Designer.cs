﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Il codice è stato generato da uno strumento.
//     Versione runtime:4.0.30319.42000
//
//     Le modifiche apportate a questo file possono provocare un comportamento non corretto e andranno perse se
//     il codice viene rigenerato.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DRSelectorComponents {
    using System;
    
    
    /// <summary>
    ///   Classe di risorse fortemente tipizzata per la ricerca di stringhe localizzate e così via.
    /// </summary>
    // Questa classe è stata generata automaticamente dalla classe StronglyTypedResourceBuilder.
    // tramite uno strumento quale ResGen o Visual Studio.
    // Per aggiungere o rimuovere un membro, modificare il file con estensione ResX ed eseguire nuovamente ResGen
    // con l'opzione /str oppure ricompilare il progetto VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class DRSelectors {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal DRSelectors() {
        }
        
        /// <summary>
        ///   Restituisce l'istanza di ResourceManager nella cache utilizzata da questa classe.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("DRSelectorComponents.DRSelectors", typeof(DRSelectors).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Esegue l'override della proprietà CurrentUICulture del thread corrente per tutte le
        ///   ricerche di risorse eseguite utilizzando questa classe di risorse fortemente tipizzata.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a DR.
        /// </summary>
        internal static string DRFileExtension {
            get {
                return ResourceManager.GetString("DRFileExtension", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Compressed binary record files|*.DR|Compressed XML record files|*.XGZ.
        /// </summary>
        internal static string DRFilesFilter {
            get {
                return ResourceManager.GetString("DRFilesFilter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a DRSelector_FullParams.
        /// </summary>
        internal static string selectDRSelectorRecParams {
            get {
                return ResourceManager.GetString("selectDRSelectorRecParams", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a DRSelector_RefAttributes.
        /// </summary>
        internal static string selectDRSelectorRefAttributes {
            get {
                return ResourceManager.GetString("selectDRSelectorRefAttributes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a DRSelect_Recent_TU.
        /// </summary>
        internal static string selectDRSelectorTU {
            get {
                return ResourceManager.GetString("selectDRSelectorTU", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a GetRefAttributesList.
        /// </summary>
        internal static string selectRefAttributesList {
            get {
                return ResourceManager.GetString("selectRefAttributesList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a drstat.
        /// </summary>
        internal static string StatFileExtension {
            get {
                return ResourceManager.GetString("StatFileExtension", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a diagnostic records statistic files|*.drstat.
        /// </summary>
        internal static string StatFilesFilter {
            get {
                return ResourceManager.GetString("StatFilesFilter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a SELECT * FROM vDRSel_TUInstances.
        /// </summary>
        internal static string viewTUInstances {
            get {
                return ResourceManager.GetString("viewTUInstances", resourceCulture);
            }
        }
    }
}
