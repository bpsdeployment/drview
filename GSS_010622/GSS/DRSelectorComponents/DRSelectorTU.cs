using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml;
using System.Threading;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using DSTU_RecType;

namespace DRSelectorComponents
{
  /// <summary>
  /// Component enabling to select diagnostic records based on TUInstance and time
  /// </summary>
  public partial class DRSelectorTU : DRSelectorComponents.DRSelectorBase
  {
    #region Members
    protected SqlCommand cmdTUInstances; //View representing all TU Instances to display in grid view
    protected SqlCommand cmdSelectDR; //Stored procedure selecting DR identifiers into temporary table according to specified parameters
    #endregion //Members

    #region Construction and initialization
    public DRSelectorTU() 
      : base() //Call base constructor
    {
      InitializeComponent();
      //Initialize member variables
      InitMembers();
    }

    protected void InitMembers()
    {
      //Create SQL commands objects
      cmdTUInstances = new SqlCommand();
      cmdSelectDR = new SqlCommand();
      //Create and prepare command selecting TUInstances
      cmdTUInstances.CommandType = CommandType.Text;
      cmdTUInstances.CommandText = DRSelectors.viewTUInstances;
      //Create and prepare command selecting DR
      cmdSelectDR.CommandType = CommandType.StoredProcedure;
      cmdSelectDR.CommandText = DRSelectors.selectDRSelectorTU;
      cmdSelectDR.Parameters.Add("@TUInstances", SqlDbType.NVarChar, 4000);
      cmdSelectDR.Parameters.Add("@From", SqlDbType.DateTime);
      cmdSelectDR.Parameters.Add("@To", SqlDbType.DateTime);
      cmdSelectDR.Parameters.Add("@RecTypeMask", SqlDbType.Int);
      cmdSelectDR.Parameters.Add("@MaxRows", SqlDbType.Int);
    }

    #endregion //Construction and initialization

    #region Component event handlers
    private void btRefresh_Click(object sender, EventArgs e)
    {
      //Reload the list of TU Instances
      RefreshTUInstances();
    }

    private void btSelect_Click(object sender, EventArgs e)
    {
      //Start asynchronous record load
      LoadRecordsAsync();
    }

    private void DRSelectorTU_Load(object sender, EventArgs e)
    {
      //Load TU Instances into grid
      RefreshTUInstances();
    }
    #endregion //Component event handlers

    #region Helper functions
    /// <summary>
    /// Selects list of all TU Instances from DB and fills the grid
    /// </summary>
    protected void RefreshTUInstances()
    {
      try
      {
        //Check connection to database
        if (!CheckDBConnection())
          return;

        //Set command parameters
        cmdTUInstances.CommandTimeout = sqlCmdTimeout;
        cmdTUInstances.Connection = dbConnection;
        //Execute command
        SqlDataReader tuInstReader = cmdTUInstances.ExecuteReader();
        //Process returned rows
        tuInstances.TU.Clear();
        while (tuInstReader.Read())
        {
          //Create new row
          TU_RecType.TURow row = tuInstances.TU.NewTURow();
          //Set column values
          row.TUInstanceID = tuInstReader.GetGuid(0);
          row.TUName = tuInstReader.GetString(1);
          row.Selected = false;
          //Add row to table
          tuInstances.TU.AddTURow(row);
        }
        tuInstReader.Close();
        //Sort grid of TU instances
        dgvTU.Sort(tUNameDataGridViewTextBoxColumn, ListSortDirection.Ascending);
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRSelectorException(ex, "Failed to load TUInstances from DB"));
      }
    }
    #endregion //Helper functions

    #region Virtual method overrides
    /// <summary>
    /// Method reads values from internal components and prepares DRLoadParametersTU object
    /// </summary>
    /// <returns>Object containing all necessary parameters for DR selection</returns>
    protected override DRLoadParameters PrepareLoadParameters()
    {
      //Create parameters object
      DRLoadParametersTU loadParams = new DRLoadParametersTU();
      //Build string containing selected TUInstanceIDs
      loadParams.selTUInstances = "";
      foreach (TU_RecType.TURow row in tuInstances.TU.Rows)
      { //Walk through each row in TU grid
        if (row.Selected)
        { //Test if row is selected
          if (loadParams.selTUInstances != "")
            loadParams.selTUInstances += ", ";
          loadParams.selTUInstances += row.TUInstanceID.ToString();
        }
      }
      //Set time parameters
      if (dtInterval.FromSelected)
        loadParams.timeFrom = dddHelper.UserTimeToUTC(dtInterval.TimeFrom);
      else
        loadParams.timeFrom = DBNull.Value;
      if (dtInterval.ToSelected)
        loadParams.timeTo = dddHelper.UserTimeToUTC(dtInterval.TimeTo);
      else
        loadParams.timeTo = DBNull.Value;
      //Set mask for record types
      loadParams.recTypeMask = 0;
      if (chbEventRec.Checked)
        loadParams.recTypeMask |= 0x02;
      if (chbTraceRec.Checked)
        loadParams.recTypeMask |= 0x04;
      if (chbSnapRec.Checked)
        loadParams.recTypeMask |= 0x08;
      return loadParams;
    }

    /// <summary>
    /// Method is invoked asynchronously and performs the load of DR.
    /// </summary>
    /// <param name="loadParams">Object containing all necessary parameters for DR selection</param>
    /// <param name="asyncOp">AsyncOperation used to post callbacks</param>
    protected override void PerformAsyncDRLoad(DRLoadParameters loadParams, AsyncOperation asyncOp)
    {
      try
      {
        //Cast parameters object to proper type
        DRLoadParametersTU parameters = loadParams as DRLoadParametersTU;
        //Check connection to database
        if (!CheckDBConnection())
          throw new DRSelectorException("Connection to database could not be opened");

        //Select data from database
        //Set command parameters
        cmdSelectDR.Parameters["@From"].Value = parameters.timeFrom;
        cmdSelectDR.Parameters["@To"].Value = parameters.timeTo;
        if (parameters.selTUInstances != "")
          cmdSelectDR.Parameters["@TUInstances"].Value = parameters.selTUInstances;
        else
          cmdSelectDR.Parameters["@TUInstances"].Value = DBNull.Value;
        cmdSelectDR.Parameters["@RecTypeMask"].Value = parameters.recTypeMask;
        cmdSelectDR.Parameters["@MaxRows"].Value = MaxRecCount;
        cmdSelectDR.CommandTimeout = sqlCmdTimeout;
        cmdSelectDR.Connection = dbConnection;
        //Execute SQL command
        SqlDataReader reader;
        //XmlReader reader;
        try
        {
          reader = cmdSelectDR.ExecuteReader();
          //reader = cmdSelectDR.ExecuteXmlReader();
        }
        catch (Exception ex)
        {
          throw new DRSelectorException(ex, "Failed to execute DB stored procedure");
        }

        //Post info message
        asyncOp.Post(onInfoMessageDelegate, "Loading records to data set");
        //Process returned XML document and fill internal dataset
        try
        {
          FillDataSetFromResultSets(reader, asyncOp);
          //FillDataSetFromXml(reader, asyncOp);
        }
        catch (Exception)
        {
          throw;
        }
        finally
        {
          //Close reader object
          reader.Close();
        }

        //Post completed event
        AsyncCompletedEventArgs result = new AsyncCompletedEventArgs(null, false, null);
        asyncOp.PostOperationCompleted(onDRLoadCompletedDelegate, result);
      }
      catch (Exception ex)
      {
        //Post completed event with error
        AsyncCompletedEventArgs result = new AsyncCompletedEventArgs(ex, false, null);
        asyncOp.PostOperationCompleted(onDRLoadCompletedDelegate, result);
      }
    }

    /// <summary>
    /// Saves settings of selector to file
    /// </summary>
    /// <param name="file">Stream to save to</param>
    /// <param name="formatter">Formatter used for saving objects</param>
    public override void SaveSettingsToFile(FileStream file, BinaryFormatter formatter)
    {
      //Build list of selected TUInstances
      List<Guid> selTUInstances = new List<Guid>();
      foreach (TU_RecType.TURow row in tuInstances.TU.Rows)
        if (row.Selected)
          selTUInstances.Add(row.TUInstanceID);
      
      //Serialize
      formatter.Serialize(file, selTUInstances);
      formatter.Serialize(file, dtInterval.FromSelected);
      formatter.Serialize(file, dtInterval.TimeFrom);
      formatter.Serialize(file, dtInterval.ToSelected);
      formatter.Serialize(file, dtInterval.TimeTo);
      formatter.Serialize(file, dtInterval.PredefIntervalIndex);
      formatter.Serialize(file, chbEventRec.Checked);
      formatter.Serialize(file, chbSnapRec.Checked);
      formatter.Serialize(file, chbTraceRec.Checked);
    }

    /// <summary>
    /// Loads settings of selector from file
    /// </summary>
    /// <param name="file">Stream to save to</param>
    /// <param name="formatter">Formatter used for saving objects</param>
    
    public override void  LoadSettingsFromFile(FileStream file, BinaryFormatter formatter)
    {
      List<Guid> selTUInstances = (List<Guid>)formatter.Deserialize(file);
      //Try to check selected TUInstances
      foreach (TU_RecType.TURow row in tuInstances.TU.Rows)
        row.Selected = selTUInstances.Contains(row.TUInstanceID);

      dtInterval.FromSelected = (bool)formatter.Deserialize(file);
      dtInterval.TimeFrom = (DateTime)formatter.Deserialize(file);
      dtInterval.ToSelected = (bool)formatter.Deserialize(file);
      dtInterval.TimeTo = (DateTime)formatter.Deserialize(file);
      dtInterval.PredefIntervalIndex = (int)formatter.Deserialize(file);
      chbEventRec.Checked = (bool)formatter.Deserialize(file);
      chbSnapRec.Checked = (bool)formatter.Deserialize(file);
      chbTraceRec.Checked = (bool)formatter.Deserialize(file);
    }
    #endregion //Virtual method overrides
  }

  #region Load parameters class
  /// <summary>
  /// Class representing parameters for DR Load operation.
  /// </summary>
  public class DRLoadParametersTU : DRLoadParameters
  {
    public Object timeFrom; //start of the interval
    public Object timeTo; //end of the interval
    public String selTUInstances; //list of selected TU Instances
    public int recTypeMask = 0; //Mask for record types
  }
  #endregion //Load parameters class
}

