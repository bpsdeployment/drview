namespace DRSelectorComponents
{
  partial class DRSelectorRefAttributes
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btSelect = new System.Windows.Forms.Button();
      this.panel2 = new System.Windows.Forms.Panel();
      this.dtInterval = new DRSelectorComponents.TimePicker();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.gbRecType = new System.Windows.Forms.GroupBox();
      this.chbSnapRec = new System.Windows.Forms.CheckBox();
      this.chbTraceRec = new System.Windows.Forms.CheckBox();
      this.chbEventRec = new System.Windows.Forms.CheckBox();
      this.label1 = new System.Windows.Forms.Label();
      this.tbRecSources = new System.Windows.Forms.TextBox();
      this.tbTitleKeyWords = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.tbFaultCodes = new System.Windows.Forms.MaskedTextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.chbMatchAllKeywords = new System.Windows.Forms.CheckBox();
      this.cbRefAttr1 = new System.Windows.Forms.ComboBox();
      this.cbOper1 = new System.Windows.Forms.ComboBox();
      this.tbVal1 = new System.Windows.Forms.TextBox();
      this.tbVal2 = new System.Windows.Forms.TextBox();
      this.cbOper2 = new System.Windows.Forms.ComboBox();
      this.cbRefAttr2 = new System.Windows.Forms.ComboBox();
      this.label4 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.tbVal3 = new System.Windows.Forms.TextBox();
      this.cbOper3 = new System.Windows.Forms.ComboBox();
      this.cbRefAttr3 = new System.Windows.Forms.ComboBox();
      this.label6 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.tbVal6 = new System.Windows.Forms.TextBox();
      this.cbOper6 = new System.Windows.Forms.ComboBox();
      this.cbRefAttr6 = new System.Windows.Forms.ComboBox();
      this.label8 = new System.Windows.Forms.Label();
      this.tbVal5 = new System.Windows.Forms.TextBox();
      this.cbOper5 = new System.Windows.Forms.ComboBox();
      this.cbRefAttr5 = new System.Windows.Forms.ComboBox();
      this.tbVal4 = new System.Windows.Forms.TextBox();
      this.cbOper4 = new System.Windows.Forms.ComboBox();
      this.cbRefAttr4 = new System.Windows.Forms.ComboBox();
      this.label9 = new System.Windows.Forms.Label();
      this.hierarchyTUInstances = new DRSelectorComponents.FleetHierarchy_TUInstances();
      ((System.ComponentModel.ISupportInitialize)(this.diagRecords)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.recordsView)).BeginInit();
      this.panel2.SuspendLayout();
      this.gbRecType.SuspendLayout();
      this.SuspendLayout();
      // 
      // btSelect
      // 
      this.btSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btSelect.Location = new System.Drawing.Point(467, 16);
      this.btSelect.Name = "btSelect";
      this.btSelect.Size = new System.Drawing.Size(75, 23);
      this.btSelect.TabIndex = 0;
      this.btSelect.Text = "Select";
      this.btSelect.UseVisualStyleBackColor = true;
      this.btSelect.Click += new System.EventHandler(this.btSelect_Click);
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.dtInterval);
      this.panel2.Controls.Add(this.btSelect);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel2.Location = new System.Drawing.Point(0, 381);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(585, 47);
      this.panel2.TabIndex = 9;
      // 
      // dtInterval
      // 
      this.dtInterval.FromSelected = true;
      this.dtInterval.Location = new System.Drawing.Point(3, -1);
      this.dtInterval.Name = "dtInterval";
      this.dtInterval.PredefIntervalIndex = 0;
      this.dtInterval.Size = new System.Drawing.Size(452, 45);
      this.dtInterval.TabIndex = 8;
      this.dtInterval.TimeFrom = new System.DateTime(2007, 10, 4, 13, 35, 17, 171);
      this.dtInterval.TimeTo = new System.DateTime(2007, 10, 4, 13, 35, 17, 171);
      this.dtInterval.ToSelected = true;
      // 
      // splitter1
      // 
      this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter1.Location = new System.Drawing.Point(0, 378);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(585, 3);
      this.splitter1.TabIndex = 10;
      this.splitter1.TabStop = false;
      // 
      // gbRecType
      // 
      this.gbRecType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.gbRecType.Controls.Add(this.chbSnapRec);
      this.gbRecType.Controls.Add(this.chbTraceRec);
      this.gbRecType.Controls.Add(this.chbEventRec);
      this.gbRecType.Location = new System.Drawing.Point(497, 160);
      this.gbRecType.Name = "gbRecType";
      this.gbRecType.Size = new System.Drawing.Size(82, 88);
      this.gbRecType.TabIndex = 11;
      this.gbRecType.TabStop = false;
      this.gbRecType.Text = "Record type";
      // 
      // chbSnapRec
      // 
      this.chbSnapRec.AutoSize = true;
      this.chbSnapRec.Location = new System.Drawing.Point(6, 65);
      this.chbSnapRec.Name = "chbSnapRec";
      this.chbSnapRec.Size = new System.Drawing.Size(51, 17);
      this.chbSnapRec.TabIndex = 2;
      this.chbSnapRec.Text = "Snap";
      this.chbSnapRec.UseVisualStyleBackColor = true;
      // 
      // chbTraceRec
      // 
      this.chbTraceRec.AutoSize = true;
      this.chbTraceRec.Location = new System.Drawing.Point(6, 42);
      this.chbTraceRec.Name = "chbTraceRec";
      this.chbTraceRec.Size = new System.Drawing.Size(54, 17);
      this.chbTraceRec.TabIndex = 1;
      this.chbTraceRec.Text = "Trace";
      this.chbTraceRec.UseVisualStyleBackColor = true;
      // 
      // chbEventRec
      // 
      this.chbEventRec.AutoSize = true;
      this.chbEventRec.Checked = true;
      this.chbEventRec.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chbEventRec.Location = new System.Drawing.Point(6, 19);
      this.chbEventRec.Name = "chbEventRec";
      this.chbEventRec.Size = new System.Drawing.Size(54, 17);
      this.chbEventRec.TabIndex = 0;
      this.chbEventRec.Text = "Event";
      this.chbEventRec.UseVisualStyleBackColor = true;
      // 
      // label1
      // 
      this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(3, 160);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(80, 13);
      this.label1.TabIndex = 12;
      this.label1.Text = "Record source:";
      // 
      // tbRecSources
      // 
      this.tbRecSources.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbRecSources.Location = new System.Drawing.Point(6, 176);
      this.tbRecSources.Name = "tbRecSources";
      this.tbRecSources.Size = new System.Drawing.Size(215, 20);
      this.tbRecSources.TabIndex = 13;
      // 
      // tbTitleKeyWords
      // 
      this.tbTitleKeyWords.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbTitleKeyWords.Location = new System.Drawing.Point(6, 215);
      this.tbTitleKeyWords.Name = "tbTitleKeyWords";
      this.tbTitleKeyWords.Size = new System.Drawing.Size(373, 20);
      this.tbTitleKeyWords.TabIndex = 14;
      // 
      // label2
      // 
      this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(236, 160);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(60, 13);
      this.label2.TabIndex = 15;
      this.label2.Text = "Fault code:";
      // 
      // tbFaultCodes
      // 
      this.tbFaultCodes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbFaultCodes.Location = new System.Drawing.Point(239, 176);
      this.tbFaultCodes.Mask = "999999999999999999999999999999999999999999999999999999999999999999999999999999999" +
          "9999999999999999999";
      this.tbFaultCodes.Name = "tbFaultCodes";
      this.tbFaultCodes.Size = new System.Drawing.Size(215, 20);
      this.tbFaultCodes.TabIndex = 16;
      // 
      // label3
      // 
      this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(3, 199);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(112, 13);
      this.label3.TabIndex = 17;
      this.label3.Text = "Record title keywords:";
      // 
      // chbMatchAllKeywords
      // 
      this.chbMatchAllKeywords.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.chbMatchAllKeywords.AutoSize = true;
      this.chbMatchAllKeywords.Location = new System.Drawing.Point(385, 217);
      this.chbMatchAllKeywords.Name = "chbMatchAllKeywords";
      this.chbMatchAllKeywords.Size = new System.Drawing.Size(69, 17);
      this.chbMatchAllKeywords.TabIndex = 18;
      this.chbMatchAllKeywords.Text = "Match all";
      this.chbMatchAllKeywords.UseVisualStyleBackColor = true;
      // 
      // cbRefAttr1
      // 
      this.cbRefAttr1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.cbRefAttr1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
      this.cbRefAttr1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
      this.cbRefAttr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbRefAttr1.FormattingEnabled = true;
      this.cbRefAttr1.Location = new System.Drawing.Point(6, 267);
      this.cbRefAttr1.Name = "cbRefAttr1";
      this.cbRefAttr1.Size = new System.Drawing.Size(131, 21);
      this.cbRefAttr1.TabIndex = 19;
      // 
      // cbOper1
      // 
      this.cbOper1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.cbOper1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbOper1.FormattingEnabled = true;
      this.cbOper1.Items.AddRange(new object[] {
            "=",
            ">",
            "<",
            ">=",
            "<=",
            "<>",
            "!="});
      this.cbOper1.Location = new System.Drawing.Point(143, 267);
      this.cbOper1.Name = "cbOper1";
      this.cbOper1.Size = new System.Drawing.Size(42, 21);
      this.cbOper1.TabIndex = 20;
      // 
      // tbVal1
      // 
      this.tbVal1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbVal1.Location = new System.Drawing.Point(191, 268);
      this.tbVal1.Name = "tbVal1";
      this.tbVal1.Size = new System.Drawing.Size(83, 20);
      this.tbVal1.TabIndex = 21;
      this.tbVal1.Text = "0";
      this.tbVal1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // tbVal2
      // 
      this.tbVal2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbVal2.Location = new System.Drawing.Point(191, 308);
      this.tbVal2.Name = "tbVal2";
      this.tbVal2.Size = new System.Drawing.Size(83, 20);
      this.tbVal2.TabIndex = 24;
      this.tbVal2.Text = "0";
      this.tbVal2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // cbOper2
      // 
      this.cbOper2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.cbOper2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbOper2.FormattingEnabled = true;
      this.cbOper2.Items.AddRange(new object[] {
            "=",
            ">",
            "<",
            ">=",
            "<=",
            "<>",
            "!="});
      this.cbOper2.Location = new System.Drawing.Point(143, 307);
      this.cbOper2.Name = "cbOper2";
      this.cbOper2.Size = new System.Drawing.Size(42, 21);
      this.cbOper2.TabIndex = 23;
      // 
      // cbRefAttr2
      // 
      this.cbRefAttr2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.cbRefAttr2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbRefAttr2.FormattingEnabled = true;
      this.cbRefAttr2.Location = new System.Drawing.Point(6, 307);
      this.cbRefAttr2.Name = "cbRefAttr2";
      this.cbRefAttr2.Size = new System.Drawing.Size(131, 21);
      this.cbRefAttr2.TabIndex = 22;
      // 
      // label4
      // 
      this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(151, 291);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(23, 13);
      this.label4.TabIndex = 25;
      this.label4.Text = "OR";
      // 
      // label5
      // 
      this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(151, 333);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(23, 13);
      this.label5.TabIndex = 29;
      this.label5.Text = "OR";
      // 
      // tbVal3
      // 
      this.tbVal3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbVal3.Location = new System.Drawing.Point(191, 350);
      this.tbVal3.Name = "tbVal3";
      this.tbVal3.Size = new System.Drawing.Size(83, 20);
      this.tbVal3.TabIndex = 28;
      this.tbVal3.Text = "0";
      this.tbVal3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // cbOper3
      // 
      this.cbOper3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.cbOper3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbOper3.FormattingEnabled = true;
      this.cbOper3.Items.AddRange(new object[] {
            "=",
            ">",
            "<",
            ">=",
            "<=",
            "<>",
            "!="});
      this.cbOper3.Location = new System.Drawing.Point(143, 349);
      this.cbOper3.Name = "cbOper3";
      this.cbOper3.Size = new System.Drawing.Size(42, 21);
      this.cbOper3.TabIndex = 27;
      // 
      // cbRefAttr3
      // 
      this.cbRefAttr3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.cbRefAttr3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbRefAttr3.FormattingEnabled = true;
      this.cbRefAttr3.Location = new System.Drawing.Point(6, 349);
      this.cbRefAttr3.Name = "cbRefAttr3";
      this.cbRefAttr3.Size = new System.Drawing.Size(131, 21);
      this.cbRefAttr3.TabIndex = 26;
      // 
      // label6
      // 
      this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(280, 311);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(30, 13);
      this.label6.TabIndex = 30;
      this.label6.Text = "AND";
      // 
      // label7
      // 
      this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(456, 333);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(23, 13);
      this.label7.TabIndex = 41;
      this.label7.Text = "OR";
      // 
      // tbVal6
      // 
      this.tbVal6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbVal6.Location = new System.Drawing.Point(496, 350);
      this.tbVal6.Name = "tbVal6";
      this.tbVal6.Size = new System.Drawing.Size(83, 20);
      this.tbVal6.TabIndex = 40;
      this.tbVal6.Text = "0";
      this.tbVal6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // cbOper6
      // 
      this.cbOper6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.cbOper6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbOper6.FormattingEnabled = true;
      this.cbOper6.Items.AddRange(new object[] {
            "=",
            ">",
            "<",
            ">=",
            "<=",
            "<>",
            "!="});
      this.cbOper6.Location = new System.Drawing.Point(448, 349);
      this.cbOper6.Name = "cbOper6";
      this.cbOper6.Size = new System.Drawing.Size(42, 21);
      this.cbOper6.TabIndex = 39;
      // 
      // cbRefAttr6
      // 
      this.cbRefAttr6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.cbRefAttr6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbRefAttr6.FormattingEnabled = true;
      this.cbRefAttr6.Location = new System.Drawing.Point(311, 349);
      this.cbRefAttr6.Name = "cbRefAttr6";
      this.cbRefAttr6.Size = new System.Drawing.Size(131, 21);
      this.cbRefAttr6.TabIndex = 38;
      // 
      // label8
      // 
      this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(456, 291);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(23, 13);
      this.label8.TabIndex = 37;
      this.label8.Text = "OR";
      // 
      // tbVal5
      // 
      this.tbVal5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbVal5.Location = new System.Drawing.Point(496, 308);
      this.tbVal5.Name = "tbVal5";
      this.tbVal5.Size = new System.Drawing.Size(83, 20);
      this.tbVal5.TabIndex = 36;
      this.tbVal5.Text = "0";
      this.tbVal5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // cbOper5
      // 
      this.cbOper5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.cbOper5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbOper5.FormattingEnabled = true;
      this.cbOper5.Items.AddRange(new object[] {
            "=",
            ">",
            "<",
            ">=",
            "<=",
            "<>",
            "!="});
      this.cbOper5.Location = new System.Drawing.Point(448, 307);
      this.cbOper5.Name = "cbOper5";
      this.cbOper5.Size = new System.Drawing.Size(42, 21);
      this.cbOper5.TabIndex = 35;
      // 
      // cbRefAttr5
      // 
      this.cbRefAttr5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.cbRefAttr5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbRefAttr5.FormattingEnabled = true;
      this.cbRefAttr5.Location = new System.Drawing.Point(311, 307);
      this.cbRefAttr5.Name = "cbRefAttr5";
      this.cbRefAttr5.Size = new System.Drawing.Size(131, 21);
      this.cbRefAttr5.TabIndex = 34;
      // 
      // tbVal4
      // 
      this.tbVal4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbVal4.Location = new System.Drawing.Point(496, 268);
      this.tbVal4.Name = "tbVal4";
      this.tbVal4.Size = new System.Drawing.Size(83, 20);
      this.tbVal4.TabIndex = 33;
      this.tbVal4.Text = "0";
      this.tbVal4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // cbOper4
      // 
      this.cbOper4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.cbOper4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbOper4.FormattingEnabled = true;
      this.cbOper4.Items.AddRange(new object[] {
            "=",
            ">",
            "<",
            ">=",
            "<=",
            "<>",
            "!="});
      this.cbOper4.Location = new System.Drawing.Point(448, 267);
      this.cbOper4.Name = "cbOper4";
      this.cbOper4.Size = new System.Drawing.Size(42, 21);
      this.cbOper4.TabIndex = 32;
      // 
      // cbRefAttr4
      // 
      this.cbRefAttr4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.cbRefAttr4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbRefAttr4.FormattingEnabled = true;
      this.cbRefAttr4.Location = new System.Drawing.Point(311, 267);
      this.cbRefAttr4.Name = "cbRefAttr4";
      this.cbRefAttr4.Size = new System.Drawing.Size(131, 21);
      this.cbRefAttr4.TabIndex = 31;
      // 
      // label9
      // 
      this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(3, 251);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(107, 13);
      this.label9.TabIndex = 42;
      this.label9.Text = "Referential attributes:";
      // 
      // hierarchyTUInstances
      // 
      this.hierarchyTUInstances.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.hierarchyTUInstances.Location = new System.Drawing.Point(0, 0);
      this.hierarchyTUInstances.MinimumSize = new System.Drawing.Size(195, 90);
      this.hierarchyTUInstances.Name = "hierarchyTUInstances";
      this.hierarchyTUInstances.Size = new System.Drawing.Size(585, 157);
      this.hierarchyTUInstances.TabIndex = 43;
      // 
      // DRSelectorRefAttributes
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.Controls.Add(this.hierarchyTUInstances);
      this.Controls.Add(this.label9);
      this.Controls.Add(this.label7);
      this.Controls.Add(this.tbVal6);
      this.Controls.Add(this.cbOper6);
      this.Controls.Add(this.cbRefAttr6);
      this.Controls.Add(this.label8);
      this.Controls.Add(this.tbVal5);
      this.Controls.Add(this.cbOper5);
      this.Controls.Add(this.cbRefAttr5);
      this.Controls.Add(this.tbVal4);
      this.Controls.Add(this.cbOper4);
      this.Controls.Add(this.cbRefAttr4);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.tbVal3);
      this.Controls.Add(this.cbOper3);
      this.Controls.Add(this.cbRefAttr3);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.tbVal2);
      this.Controls.Add(this.cbOper2);
      this.Controls.Add(this.cbRefAttr2);
      this.Controls.Add(this.tbVal1);
      this.Controls.Add(this.cbOper1);
      this.Controls.Add(this.cbRefAttr1);
      this.Controls.Add(this.chbMatchAllKeywords);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.tbFaultCodes);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.tbTitleKeyWords);
      this.Controls.Add(this.tbRecSources);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.gbRecType);
      this.Controls.Add(this.splitter1);
      this.Controls.Add(this.panel2);
      this.MinimumSize = new System.Drawing.Size(585, 320);
      this.Name = "DRSelectorRefAttributes";
      this.Size = new System.Drawing.Size(585, 428);
      this.Load += new System.EventHandler(this.DRSelectorRefAttributes_Load);
      ((System.ComponentModel.ISupportInitialize)(this.diagRecords)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.recordsView)).EndInit();
      this.panel2.ResumeLayout(false);
      this.gbRecType.ResumeLayout(false);
      this.gbRecType.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btSelect;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Splitter splitter1;
    private TimePicker dtInterval;
    private System.Windows.Forms.GroupBox gbRecType;
    private System.Windows.Forms.CheckBox chbEventRec;
    private System.Windows.Forms.CheckBox chbSnapRec;
    private System.Windows.Forms.CheckBox chbTraceRec;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox tbRecSources;
    private System.Windows.Forms.TextBox tbTitleKeyWords;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.MaskedTextBox tbFaultCodes;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.CheckBox chbMatchAllKeywords;
    private System.Windows.Forms.ComboBox cbRefAttr1;
    private System.Windows.Forms.ComboBox cbOper1;
    private System.Windows.Forms.TextBox tbVal1;
    private System.Windows.Forms.TextBox tbVal2;
    private System.Windows.Forms.ComboBox cbOper2;
    private System.Windows.Forms.ComboBox cbRefAttr2;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.TextBox tbVal3;
    private System.Windows.Forms.ComboBox cbOper3;
    private System.Windows.Forms.ComboBox cbRefAttr3;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.TextBox tbVal6;
    private System.Windows.Forms.ComboBox cbOper6;
    private System.Windows.Forms.ComboBox cbRefAttr6;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.TextBox tbVal5;
    private System.Windows.Forms.ComboBox cbOper5;
    private System.Windows.Forms.ComboBox cbRefAttr5;
    private System.Windows.Forms.TextBox tbVal4;
    private System.Windows.Forms.ComboBox cbOper4;
    private System.Windows.Forms.ComboBox cbRefAttr4;
    private System.Windows.Forms.Label label9;
    private FleetHierarchy_TUInstances hierarchyTUInstances;
  }
}
