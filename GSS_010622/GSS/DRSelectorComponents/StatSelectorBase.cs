using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Xml;
using System.Threading;
using System.Globalization;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using DDDObjects;
using DRQueries;
using FleetHierarchies;
using DRStatistics;

namespace StatSelectorComponents
{
  #region Delegates definitions
  /// <summary>
  /// Delegate defines event handler for New Data Recieved event
  /// </summary>
  /// <param name="sender">Sender of the event</param>
  public delegate void NDREventHandler(StatSelectorBase sender, NDREventArgs args);

  /// <summary>
  /// Defines handler for InfoMessage event
  /// </summary>
  /// <param name="sender">Sender of the event</param>
  /// <param name="message">Message to pass</param>
  public delegate void InfoMessageEventhandler(Object sender, String message);

  /// <summary>
  /// Defines handler for ErrorMessage event
  /// </summary>
  /// <param name="sender">Sender of the event</param>
  /// <param name="error">Exception describing the error</param>
  public delegate void ErrorMessageEventhandler(Object sender, Exception error);
  #endregion //Delegates definitions

  /// <summary>
  /// Base class for all Statistic Records selectors
  /// Defines common properties and events
  /// </summary>
  public partial class StatSelectorBase : UserControl
  {
    #region Members
    protected SqlConnection dbConnection; //connection to database
    protected DDDHelper dddHelper; //class providing DDD objects functionality
    protected int sqlCmdTimeout = 30; //timeout for SQL command operations
    protected AsyncOperation asyncOp; //Object tracking the lifetime of asynchronous operation
    protected DRStatsHelper statsHelper; //Helper class for statistics queries and results
    #endregion //Members

    #region Public properties
    /// <summary>
    /// Connection to database
    /// </summary>
    public SqlConnection DBConnection
    {
      get { return dbConnection; }
      set { dbConnection = value; }
    }

    /// <summary>
    /// Timeout for SQL records
    /// </summary>
    public int CommandTimeout
    {
      get { return sqlCmdTimeout; }
      set { sqlCmdTimeout = value; }
    }

    /// <summary>
    /// DDDHelper object providing DDD objects functionality
    /// </summary>
    public DDDHelper DDDHelper
    {
      get { return dddHelper; }
      set
      {
        dddHelper = value;
        statsHelper.DDDHelper = value;
      }
    }

    /// <summary>
    /// Component is performing asynchronous load of diagnostic records
    /// </summary>
    public bool IsBusy
    {
      get { return asyncOp != null; }
    }
    
    /// <summary>
    /// Helper with statistics query and loaded results
    /// </summary>
    public DRStatsHelper StatisticsHelper
    {
      get { return statsHelper; }
    }
    #endregion //Public properties

    #region Public Events
    /// <summary>
    /// Event occurs when new data are selected and ready for visualisation
    /// </summary>
    public event NDREventHandler NewDataReady;
    /// <summary>
    /// Event occurs when there is an information message to pass to component owner
    /// </summary>
    public event InfoMessageEventhandler InfoMessage;
    /// <summary>
    /// Event occurs when there is an error encountered during selection process
    /// </summary>
    public event ErrorMessageEventhandler ErrorMessage;
    #endregion //Public Events

    #region Internal delegates
    /// <summary>
    /// Delegate of function which is called by AsyncOperation and raises InfoMessage event
    /// </summary>
    protected SendOrPostCallback onInfoMessageDelegate;

    /// <summary>
    /// Delegate of function which is called by AsyncOperation and raises either NewDataReady
    /// or ErrorMessage event
    /// </summary>
    protected SendOrPostCallback onLoadCompletedDelegate;

    /// <summary>
    /// Prototype of delegate which handles asynchronous load of data
    /// </summary>
    /// <param name="statsQuery">Parameters for the load operation</param>
    /// <param name="asyncOp">AsyncOperation which helps to post callbacks</param>
    protected delegate void AsyncStatsLoadHandler(DRQueryHelper statsQuery, AsyncOperation asyncOp);

    /// <summary>
    /// Delegate of function which handles asynchronous statistics load
    /// </summary>
    protected AsyncStatsLoadHandler asyncStatsLoadDelegate;
    #endregion //Internal delegates

    #region Construction and initialization
    /// <summary>
    /// Standard constructor
    /// </summary>
    public StatSelectorBase()
    {
      InitializeComponent();
      //Initialize internal delegates
      InitializeDelegates();
      //Create statistics helper
      statsHelper = new DRStatsHelper(dddHelper);
    }

    /// <summary>
    /// Initializes internal delegates with functions
    /// </summary>
    protected void InitializeDelegates()
    {
      onLoadCompletedDelegate = LoadCompletedAsync;
      onInfoMessageDelegate = InfoMessageAsync;
      asyncStatsLoadDelegate = PerformAsyncStatsLoad;
    }
    #endregion //Construction and initialization

    #region Event raisers
    /// <summary>
    /// Method raises NewDataReady event
    /// </summary>
    protected void OnNewDataReady()
    {
      OnInfoMessage("Statistical data ready");
      NDREventArgs args = new NDREventArgs();
      if (NewDataReady != null)
        NewDataReady(this, args);
    }

    /// <summary>
    /// Method raises InfoMessage event
    /// </summary>
    protected void OnInfoMessage(String message)
    {
      if (InfoMessage != null)
        InfoMessage(this, message);
    }

    /// <summary>
    /// Raises ErrorMessage event
    /// </summary>
    /// <param name="error">Exception describing the error</param>
    protected void OnErrorMessage(Exception error)
    {
      if (ErrorMessage != null)
        ErrorMessage(this, error);
    }

    /// <summary>
    /// Method raises InfoMessage event
    /// </summary>
    protected void OnInfoMessage(String message, params Object[] args)
    {
      if (InfoMessage != null)
        InfoMessage(this, String.Format(message, args));
    }

    /// <summary>
    /// Called by AsyncOperation on correct thread to raise InfoMessage event
    /// </summary>
    /// <param name="state">Message to sent</param>
    protected void InfoMessageAsync(object state)
    {
      OnInfoMessage(state as String);
    }

    /// <summary>
    /// Method is called after asynchronous Stat Load is completed.
    /// Depending on result, one of the public events is raised.
    /// </summary>
    /// <param name="state">Result of asynchronous operation</param>
    protected void LoadCompletedAsync(object state)
    {
      //Enable component
      this.Enabled = true;
      //Clear async operation object
      asyncOp = null;
      //Call one of the public events depending on operation result
      AsyncCompletedEventArgs result = state as AsyncCompletedEventArgs;
      if (result.Cancelled)
        OnInfoMessage("Statistical query cancelled");
      else if (result.Error != null)
        OnErrorMessage(result.Error);
      else
      {//Records loaded, check for count limit
        OnNewDataReady();
      }
      //Return cursor to normal
      this.ParentForm.Enabled = true;
      this.ParentForm.Cursor = Cursors.Arrow;
    }
    #endregion //Event raisers

    #region Helper functions
    /// <summary>
    /// Checks if there is any connection associated with the component
    /// and its state. Tries to open the connection if necessary.
    /// </summary>
    /// <returns>true if there is open connection available, false otherwise</returns>
    protected bool CheckDBConnection()
    {
      if (dbConnection == null)
        return false;
      if (dbConnection.State != ConnectionState.Open)
      {
        dbConnection.Close();
        dbConnection.Open();
      }
      return (dbConnection.State == ConnectionState.Open);
    }

    /// <summary>
    /// Reads statistic results from results sets provided by passed reader
    /// into internal statistics helper.
    /// Reader is assumed to be positioned before the first result set.
    /// </summary>
    /// <param name="reader">Reader providing result sets from db</param>
    /// <param name="asyncOp">Asynchronous operation used to post callbacks</param>
    protected void LoadStatisticResultSets(SqlDataReader reader, AsyncOperation asyncOp)
    {
      try
      {
        int resCount = StatisticsHelper.ReadQueryResults(reader);
        //Post info message
        asyncOp.Post(onInfoMessageDelegate, String.Format("Loaded {0} query results", resCount));
      }
      catch (Exception ex)
      {
        throw new StatSelectorException(ex, "Failed to load statistical results from database");
      }
      finally
      {
      }
    }
    #endregion //Helper functions

    #region Virtual prototypes
    /// <summary>
    /// Prototype of method which prepares the statistical database query.
    /// Method is called synchronously, so it can access all internal components
    /// </summary>
    /// <returns>Statistical query object</returns>
    protected virtual DRQueryHelper PrepareStatsQuery()
    {
      return StatisticsHelper.CreateStatsQuery();
    }

    /// <summary>
    /// Method signature conforms to AsyncStatsLoadHandler delegate.
    /// Method is invoked asynchronously and performs the load of statistical data.
    /// All callbacks has to be performed using internal delegates.
    /// Method must not perform any cross thread calls!!!
    /// </summary>
    /// <param name="statsQuery">Object containing statistical query</param>
    /// <param name="asyncOp">AsyncOperation used to post callbacks</param>
    protected virtual void PerformAsyncStatsLoad(DRQueryHelper statsQuery, AsyncOperation asyncOp)
    {
      return;
    }

    /// <summary>
    /// Saves settings of selector to file
    /// </summary>
    /// <param name="file">Stream to save to</param>
    /// <param name="formatter">Formatter used for saving objects</param>
    public virtual void SaveSettingsToFile(FileStream file, BinaryFormatter formatter)
    {
      return;
    }

    /// <summary>
    /// Loads settings of selector from file
    /// </summary>
    /// <param name="file">Stream to load from</param>
    /// <param name="formatter">Formatter used for loading objects</param>
    public virtual void LoadSettingsFromFile(FileStream file, BinaryFormatter formatter)
    {
      return;
    }
    #endregion //Virtual prototypes

    #region Public methods
    /// <summary>
    /// Method performs statistical query with current parameters and loads the result.
    /// Method performs the load asynchronously.
    /// After completition, NewDataReady event is raised.
    /// In case of failure ErrorMessage event is raised.
    /// During load operation InfoMessage events are raised reporting progress.
    /// </summary>
    public void LoadStatisticsAsync()
    {
      try
      {
        //Check if the operation is not running already
        if (IsBusy)
          throw new InvalidOperationException("Component is already performing statistical query");
        //Set component cursor to busy
        this.ParentForm.Enabled = false;
        this.ParentForm.Activate();
        this.ParentForm.Cursor = Cursors.WaitCursor;
        //Disable component
        this.Enabled = false;
      
        //Prepare load parameters
        DRQueryHelper statsQuery = PrepareStatsQuery();
        OnInfoMessage("Starting asynchronous operation");
        //Crate AsyncOperation object
        asyncOp = AsyncOperationManager.CreateOperation(this);
        //Invoke asynchronously load operation
        asyncStatsLoadDelegate.BeginInvoke(statsQuery, asyncOp, null, null);
      }
      catch (Exception ex)
      { //Handle all errors
        //Set component cursor to normal
        this.ParentForm.Enabled = true;
        this.ParentForm.Cursor = Cursors.Arrow;
        this.Enabled = true;
        OnErrorMessage(ex); //Pass errors into error event handler
      }
    }

    /// <summary>
    /// Save all statistics results into the given file
    /// </summary>
    /// <param name="fileName">Name of target file</param>
    public void SaveStatisticsToFile(String fileName)
    {
      try
      {
        OnInfoMessage("Saving statistical data to file " + fileName);
        //Open file stream
        FileStream outFile = new FileStream(fileName, FileMode.Create, FileAccess.Write);
        //Create binary formatter for object serialization
        BinaryFormatter formatter = new BinaryFormatter();
        //Save content of statistic helper
        formatter.Serialize(outFile, StatisticsHelper.GroupsDescriptors);
        formatter.Serialize(outFile, StatisticsHelper.GroupValue);
        outFile.Close();
        outFile.Dispose();
        OnInfoMessage("Statistical data saved");
      }
      catch (Exception ex)
      {
        OnErrorMessage(new Exception("Failed to save statistical data", ex));
      }
    }

    /// <summary>
    /// Asynchronously loads statistic data to internal helper from specified file
    /// </summary>
    /// <param name="fileName">Source file name</param>
    public void LoadStatisticsFromFile(String fileName)
    {
      //Open file stream
      FileStream inFile = new FileStream(fileName, FileMode.Open, FileAccess.Read);
      //Create binary formatter for object serialization
      BinaryFormatter formatter = new BinaryFormatter();
      //Prepare helper
      StatisticsHelper.ClearGroups();
      //Deserialize helper objects
      StatisticsHelper.GroupsDescriptors = (GroupsDescriptors)formatter.Deserialize(inFile);
      StatisticsHelper.GroupValue = (GroupValue)formatter.Deserialize(inFile);
      //Close and dispose stream
      inFile.Close();
      inFile.Dispose();
    }
    #endregion //Public methods
  }

  #region Selector exception
  /// <summary>
  /// Exception class thrown by all DR Selector objects
  /// </summary>
  class StatSelectorException : Exception
  {
    public StatSelectorException(String message, params Object[] args)
      : base(String.Format(message, args))
    { }
    public StatSelectorException(Exception inner, String message, params Object[] args)
      : base(String.Format(message, args), inner)
    { }
  }
  #endregion //Selector exception

  #region NDREventArgs class
  public class NDREventArgs : EventArgs
  {
    public NDREventArgs()
    {
    }
  }
  #endregion //NDREventArgs class
}
