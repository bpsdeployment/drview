using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Xml;
using System.Threading;
using System.Globalization;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using DDDObjects;
using FleetHierarchies;
using DSDiagnosticRecords;
//using EnvVarExpressions;

namespace DRSelectorComponents
{
  #region Delegates definitions
  /// <summary>
  /// Delegate defines event handler for New Data Recieved event
  /// </summary>
  /// <param name="sender">Sender of the event</param>
  public delegate void NDREventHandler(DRSelectorBase sender, NDREventArgs args);

  /// <summary>
  /// Defines handler for InfoMessage event
  /// </summary>
  /// <param name="sender">Sender of the event</param>
  /// <param name="message">Message to pass</param>
  public delegate void InfoMessageEventhandler(Object sender, String message);

  /// <summary>
  /// Defines handler for ErrorMessage event
  /// </summary>
  /// <param name="sender">Sender of the event</param>
  /// <param name="error">Exception describing the error</param>
  public delegate void ErrorMessageEventhandler(Object sender, Exception error);

  /// <summary>
  /// Defines handler for RecordCount event
  /// </summary>
  /// <param name="sender">Event sender</param>
  /// <param name="args">Parameters specifying the number of records</param>
  public delegate void RecordCountEventHandler(Object sender, RecCounEventArgs args);

  /// <summary>
  /// Defines handler for Report progress event
  /// </summary>
  /// <param name="sender">Sender of the event</param>
  /// <param name="progress">Operation progress 0-100</param>
  /// <param name="showBar">Visibility of progress bar</param>
  public delegate void ReportProgressEventHandler(Object sender, int progress, bool showBar);

  /// <summary>
  /// Defines handler for SelectRecord event. 
  /// Event is raised each time any record shall be selected in all views.
  /// </summary>
  /// <param name="sender">Event sender</param>
  /// <param name="args">Parameters specifying selected record</param>
  public delegate void SelectRecordEventHandler(DRSelectorBase sender, SelectRecordEventArgs args);
  #endregion //Delegates definitions

  /// <summary>
  /// Base class for all Diagnostic Records selectors
  /// Defines common properties and events
  /// </summary>
  public partial class DRSelectorBase : UserControl
  {
    #region Members
    protected SqlConnection dbConnection = null; //connection to database
    protected DDDHelper dddHelper = null; //class providing DDD objects functionality
    protected int sqlCmdTimeout = 30; //timeout for SQL command operations
    protected bool removeOldRecords = true; //Old records shall be removed from dataset before loading new ones
    protected AsyncOperation asyncOp; //Object tracking the lifetime of asynchronous operation
    protected DiagnosticRecords diagRecords = null; //Dataset for diagnostic records
    protected DataView recordsView = null; //View of records table in dataset
    protected VarIDDictionary varFilter = null; //filter for variables
    protected int maxRecCount = 0; //Maximum number of records which can be loaded into dataset (0 = unlimited)
    protected Dictionary<String, int> recCounts = new Dictionary<String, int>(); //Dictionary with record counts for each rec type
    protected bool bComputeRecTypeCounts = true;
    #endregion //Members

    #region Public properties
    /// <summary>
    /// Connection to database
    /// </summary>
    public SqlConnection DBConnection
    {
      get { return dbConnection; }
      set { dbConnection = value; }
    }

    /// <summary>
    /// Timeout for SQL records
    /// </summary>
    public int CommandTimeout
    {
      get { return sqlCmdTimeout; }
      set { sqlCmdTimeout = value; }
    }

    /// <summary>
    /// Old records shall be removed from dataset before loading new ones
    /// </summary>
    public bool RemoveOldRecords
    {
      get { return removeOldRecords; }
      set { removeOldRecords = value; }
    }

    /// <summary>
    /// DDDHelper object providing DDD objects functionality
    /// </summary>
    public DDDHelper DDDHelper
    {
      get { return dddHelper; }
      set { dddHelper = value; }
    }

    /// <summary>
    /// View of the internal data table containing selected records
    /// </summary>
    public DataView RecordsView
    {
      get { return recordsView; }
    }

    /// <summary>
    /// Component is performing asynchronous load of diagnostic records
    /// </summary>
    public bool IsBusy
    {
      get { return asyncOp != null; }
    }

    /// <summary>
    /// Maximum number of records that can be loaded into dataset
    /// </summary>
    public int MaxRecCount
    {
      get { return maxRecCount; }
      set { maxRecCount = value; }
    }

    /// <summary>
    /// Count statistics for each record type shall be computed
    /// </summary>
    public bool ComputeRecTypeCounts
    {
      get { return bComputeRecTypeCounts; }
      set { bComputeRecTypeCounts = value; }
    }

    public VarIDDictionary VariableFilter
    {
      get {return varFilter;}
    }
    #endregion //Public properties

    #region Public Events
    /// <summary>
    /// Event occurs when new data are selected and ready for visualisation
    /// </summary>
    public event NDREventHandler NewDataReady;
    /// <summary>
    /// Event occurs when there is an information message to pass to component owner
    /// </summary>
    public event InfoMessageEventhandler InfoMessage;
    /// <summary>
    /// Event occurs when there is an error encountered during selection process
    /// </summary>
    public event ErrorMessageEventhandler ErrorMessage;
    /// <summary>
    /// Event occurs when record count changes
    /// </summary>
    public event RecordCountEventHandler RecordCountMessage;
    /// <summary>
    /// Event occurs when component needs to report progress of running operation
    /// </summary>
    public event ReportProgressEventHandler ProgressReport;
    /// <summary>
    /// Event occurs when one record is explicitly selected
    /// Notifies subscribers, that this record shall be highlighted
    /// </summary>
    public event SelectRecordEventHandler OnSelectRecord;
    #endregion //Public Events

    #region Internal delegates
    /// <summary>
    /// Delegate of function which is called by AsyncOperation and raises InfoMessage event
    /// </summary>
    protected SendOrPostCallback onInfoMessageDelegate;

    /// <summary>
    /// Delegate of function which is called by AsyncOperation and raises either NewDataReady
    /// or ErrorMessage event
    /// </summary>
    protected SendOrPostCallback onDRLoadCompletedDelegate;

    /// <summary>
    /// Delegate of function called by AsyncOperation to raise StatusTextMessage event
    /// </summary>
    protected SendOrPostCallback onRecordCountDelegate;

    /// <summary>
    /// Prototype of delegate which handles asynchronous load of data
    /// </summary>
    /// <param name="loadParams">Parameters for the load operation</param>
    /// <param name="asyncOp">AsyncOperation which helps to post callbacks</param>
    protected delegate void AsyncDRLoadHandler(DRLoadParameters loadParams, AsyncOperation asyncOp);

    /// <summary>
    /// Delegate of function which handles asynchronous data load
    /// </summary>
    protected AsyncDRLoadHandler asyncDRLoadDelegate;
    #endregion //Internal delegates

    #region Construction and initialization
    /// <summary>
    /// Standard default constructor
    /// </summary>
    public DRSelectorBase()
    {
      InitializeComponent();
      //Create DataSet instance
      InitiateDataSet();
      //Initialize internal delegates
      InitializeDelegates();
      //Add event handler to listen to disposed event
      this.Disposed += new EventHandler(DRSelectorBase_Disposed);
    }

    /// <summary>
    /// Constructor which accepts dictionary of named parameters
    /// </summary>
    /// <param name="selParams">Dictionary with named parameters</param>
    public DRSelectorBase(Dictionary<String, Object> selParams)
    {
      InitializeComponent();
      //Create DataSet instance
      InitiateDataSet();
      //Initialize internal delegates
      InitializeDelegates();
      //Add event handler to listen to disposed event
      this.Disposed += new EventHandler(DRSelectorBase_Disposed);
      //Set selector parameters from array of named parameters
      if (selParams.ContainsKey("CommandTimeout"))
        this.CommandTimeout = Convert.ToInt32(selParams["CommandTimeout"]);
      if (selParams.ContainsKey("MaxRecCount"))
        this.MaxRecCount = Convert.ToInt32(selParams["MaxRecCount"]);
      if (selParams.ContainsKey("ComputeRecTypeCount"))
        this.ComputeRecTypeCounts = Convert.ToBoolean(selParams["ComputeRecTypeCount"]);
      if (selParams.ContainsKey("DBConnection"))
        this.DBConnection = (SqlConnection)(selParams["DBConnection"]);
      if (selParams.ContainsKey("DDDHelper"))
        this.DDDHelper = (DDDHelper)(selParams["DDDHelper"]);
    }

    /// <summary>
    /// Initializes internal delegates with functions
    /// </summary>
    protected void InitializeDelegates()
    {
      onDRLoadCompletedDelegate = LoadCompletedAsync;
      onInfoMessageDelegate = InfoMessageAsync;
      onRecordCountDelegate = RecordCountMessageAsync;
      asyncDRLoadDelegate = PerformAsyncDRLoad;
    }

    /// <summary>
    /// Creates new instance of dataset and associated views
    /// Destroys old instances if necessary
    /// </summary>
    protected void InitiateDataSet()
    {
      String recordsFilter = "";
      //Store filter strings from old view and dispose old instance
      if (recordsView != null)
      {
        recordsFilter = recordsView.RowFilter;
        recordsView.Dispose();
      }
      //Dispose old dataset instance
      if (diagRecords != null)
      {
        diagRecords.Dispose();
      }
      //Create new dataset instance
      diagRecords = new DSDiagnosticRecords.DiagnosticRecords();
      ((System.ComponentModel.ISupportInitialize)(diagRecords)).BeginInit();
      diagRecords.DataSetName = "DiagnosticRecords";
      diagRecords.Locale = new System.Globalization.CultureInfo("");
      diagRecords.SchemaSerializationMode = System.Data.SchemaSerializationMode.ExcludeSchema;
      ((System.ComponentModel.ISupportInitialize)(diagRecords)).EndInit();
      //Create views for dataset tables
      recordsView = new DataView(diagRecords.Records, recordsFilter, "TUInstanceID, RecordInstID, RecordDefID", DataViewRowState.CurrentRows);
    }

    void DRSelectorBase_Disposed(object sender, EventArgs e)
    {
      if (recordsView != null)
        recordsView.Dispose();
      //Dispose old dataset instance
      if (diagRecords != null)
        diagRecords.Dispose();
      recordsView = null;
      diagRecords = null;
    }
    #endregion //Construction and initialization

    #region Event raisers
    /// <summary>
    /// Method raises NewDataReady event
    /// </summary>
    /// <param name="filtered">records are filtered</param>
    /// <param name="countLimit">count limit was reached - not all data read</param>
    protected void OnNewDataReady(bool filtered, bool countLimit)
    {
      ComputeRecStats(); 
      if (!filtered)
        OnInfoMessage("New data ready. ({0} records)", recordsView.Count);
      else
        OnInfoMessage("Filter applied. ({0} records)", recordsView.Count);
      NDREventArgs args = new NDREventArgs(filtered, countLimit);
      if (NewDataReady != null)
        NewDataReady(this, args);
    }

    /// <summary>
    /// Method raises InfoMessage event
    /// </summary>
    protected void OnInfoMessage(String message)
    {
      if (InfoMessage != null)
        InfoMessage(this, message);
    }

    /// <summary>
    /// Raises ErrorMessage event
    /// </summary>
    /// <param name="error">Exception describing the error</param>
    protected void OnErrorMessage(Exception error)
    {
      if (ErrorMessage != null)
        ErrorMessage(this, error);
    }

    /// <summary>
    /// Method raises InfoMessage event
    /// </summary>
    protected void OnInfoMessage(String message, params Object[] args)
    {
      if (InfoMessage != null)
        InfoMessage(this, String.Format(message, args));
    }

    /// <summary>
    /// Method raises Report progress event
    /// </summary>
    /// <param name="progress">reported progress</param>
    /// <param name="barVisible">visibility of progress bar</param>
    protected void OnReportProgress(int progress, bool barVisible)
    {
      if (ProgressReport != null)
        ProgressReport(this, progress, barVisible);
    }

    /// <summary>
    /// Raises an OnSelectRecord event for given record
    /// </summary>
    /// <param name="recordIndex">Index of record to select in data table</param>
    public void SelectRecord(int recordIndex)
    {
      if (recordIndex > 0 && recordIndex < RecordsView.Table.Rows.Count)
      { //Valid record index
        //Create event arguments
        DiagnosticRecords.RecordsRow row = (DiagnosticRecords.RecordsRow)RecordsView.Table.Rows[recordIndex];
        SelectRecordEventArgs args = 
          new SelectRecordEventArgs(row.TUInstanceID, row.RecordInstID, row.RecordDefID, recordIndex);
        //Raise the evnt
        if (OnSelectRecord != null)
          OnSelectRecord(this, args);
      }
    }

    /// <summary>
    /// Raises an OnSelectRecord event for given record
    /// </summary>
    /// <param name="tuInstanceID">ID of TU Instance which generated the record</param>
    /// <param name="recordInstID">ID of record instance</param>
    /// <param name="recordDefID">ID of record definition</param>
    public void SelectRecord(Guid tuInstanceID, Int64 recordInstID, Int32 recordDefID)
    {
      //Find record in view
      int recViewIndx = RecordsView.Find(new Object[] { tuInstanceID, recordInstID, recordDefID });
      if (recViewIndx == -1)
        return; //Record not found - no event raised
      //Create event arguments
      int recRowIndx = RecordsView.Table.Rows.IndexOf(RecordsView[recViewIndx].Row);
      SelectRecordEventArgs args =
        new SelectRecordEventArgs(tuInstanceID, recordInstID, recordDefID, recRowIndx);
      //Raise the evnt
      if (OnSelectRecord != null)
        OnSelectRecord(this, args);
    }

    /// <summary>
    /// Called by AsyncOperation on correct thread to raise InfoMessage event
    /// </summary>
    /// <param name="state">Message to sent</param>
    protected void InfoMessageAsync(object state)
    {
      OnInfoMessage(state as String);
    }

    /// <summary>
    /// Called by AsyncOperation on correct thread to raise RecordCountMessage event
    /// </summary>
    /// <param name="state">RecCounEventArgs with record counts</param>
    protected void RecordCountMessageAsync(object state)
    {
      if (RecordCountMessage != null)
        RecordCountMessage(this, state as RecCounEventArgs);
    }

    /// <summary>
    /// Method is called after asynchronous DRLoad is completed.
    /// Depending on result, one of the public events is raised.
    /// </summary>
    /// <param name="state">Result of asynchronous operation</param>
    protected void LoadCompletedAsync(object state)
    {
      //Enable component
      this.Enabled = true;
      //Clear async operation object
      asyncOp = null;
      //Call one of the public events depending on operation result
      AsyncCompletedEventArgs result = state as AsyncCompletedEventArgs;
      if (result.Cancelled)
        OnInfoMessage("Canceled loading of diagnostic records");
      else if (result.Error != null)
        OnErrorMessage(result.Error);
      else
      {//Records loaded, check for count limit
        OnNewDataReady(false, CountLimitExceeded());
      }
      //Return cursor to normal
      this.ParentForm.Enabled = true;
      this.ParentForm.Cursor = Cursors.Arrow;
    }
    #endregion //Event raisers

    #region Helper functions
    /// <summary>
    /// Checks if there is any connection associated with the component
    /// and its state. Tries to open the connection if necessary.
    /// </summary>
    /// <returns>true if there is open connection available, false otherwise</returns>
    protected bool CheckDBConnection()
    {
      if (dbConnection == null)
        return false;
      if (dbConnection.State != ConnectionState.Open)
      {
        dbConnection.Close();
        dbConnection.Open();
      }
      return (dbConnection.State == ConnectionState.Open);
    }

    /// <summary>
    /// Reads records from results sets provided by passed reader and adds records to internal data set.
    /// Existing records are ignored.
    /// Reader is assumed to be positioned before the first result set.
    /// </summary>
    /// <param name="reader">Reader providing result sets from db</param>
    /// <param name="asyncOp">Asynchronous operation used to post callbacks</param>
    protected void FillDataSetFromResultSets(SqlDataReader reader, AsyncOperation asyncOp)
    {
      try
      {
        //First read DiagnosticRecords and fill data set
        //Check for maximum number of records allowed
        while (reader.Read() && !CountLimitExceeded())
        { //For each record in result set
          //Create new records row
          DiagnosticRecords.RecordsRow record = diagRecords.Records.NewRecordsRow();
          //Fill columns with attribute values
          record.TUInstanceID = reader.GetGuid(0);
          record.RecordInstID = reader.GetInt64(1);
          record.DDDVersionID = reader.GetGuid(2);
          record.RecordDefID = reader.GetInt32(3);
          record.StartTime = dddHelper.UTCToUserTime(reader.GetDateTime(4));
          if (!reader.IsDBNull(5))
            record.EndTime = dddHelper.UTCToUserTime(reader.GetDateTime(5));
          record.LastModified = dddHelper.UTCToUserTime(reader.GetDateTime(6));
          record.ParseStatusID = reader.GetInt32(7);
          if (!reader.IsDBNull(8))
            record.BinData = reader.GetSqlBinary(8).Value;
          record.BigEndian = reader.GetBoolean(9);
          if (!reader.IsDBNull(10))
            record.Source = reader.GetString(10);
          else
            record.Source = "";
          if (!reader.IsDBNull(11))
            record.HierarchyItemID = reader.GetInt32(11);
          if (!reader.IsDBNull(12))
            record.ImportTime = reader.GetDateTime(12);
          if (!reader.IsDBNull(13))
            record.AcknowledgeTime = reader.GetDateTime(13);
          if (!reader.IsDBNull(14))
            record.VehicleNumber = reader.GetInt16(14);
          if (!reader.IsDBNull(15))
            record.DeviceCode = reader.GetInt16(15);
          //Fill in common attributes for record
          FillCommonRecAttributes(record);
          //Add row to data table
          if (removeOldRecords || !diagRecords.Records.Rows.Contains(new Object[] { record.TUInstanceID, record.RecordInstID }))
          {//Record does not exist in database, we can safely add it
            diagRecords.Records.AddRecordsRow(record); //Record was parsed, var values are in second result set
          }
          //Send message with record count if necessary
          if ((diagRecords.Records.Count % 50) == 0)
            asyncOp.Post(onRecordCountDelegate, new RecCounEventArgs(diagRecords.Records.Count));
        }
      }
      catch (Exception ex)
      {
        throw new DRSelectorException(ex, "Failed to fill data set with selected data");
      }
      finally
      {
      }
    }

    /// <summary>
    /// Fills columns with common attributes for record obtained from dddHelper
    /// </summary>
    /// <param name="recRow">Row to fill</param>
    protected void FillCommonRecAttributes(DiagnosticRecords.RecordsRow recRow)
    {
      try
      {
        if (dbConnection != null)
          dddHelper.ConnectionString = dbConnection.ConnectionString;
        TUInstance inst = dddHelper.GetTUInstanceDB(recRow.TUInstanceID);
        TUType tuType = dddHelper.GetTUType(inst.TUTypeID);
        DDDVersion ver = dddHelper.GetVersion(recRow.DDDVersionID);
        DDDRecord rec = ver.GetRecord(recRow.RecordDefID);
        recRow.TUInstanceName = inst.TUName;
        recRow.TUTypeName = tuType.TypeName;
        recRow.DDDVersionName = ver.UserVersion;
        recRow.RecTitle = rec.Title;
        recRow.RecordName = rec.Name;
        if (rec.GetType() == typeof(DDDEventRecord))
        {
          recRow.RecordType = "event";
          recRow.FaultCode = ((DDDEventRecord)rec).FaultCode;
          recRow.FaultSeverity = ((DDDEventRecord)rec).Severity.ToString();
          recRow.FaultSubSeverity = ((DDDEventRecord)rec).SubSeverity;
        }
        else if (rec.GetType() == typeof(DDDTraceRecord))
        {
          recRow.RecordType = "trace";
          recRow.TrcSnpCode = ((DDDTraceRecord)rec).Code;
        }
        else if (rec.GetType() == typeof(DDDSnapRecord))
        {
          recRow.RecordType = "snap";
          recRow.TrcSnpCode = ((DDDSnapRecord)rec).Code;
        }
        //Obtain source from device code if necessary
        if ((recRow.Source == null || recRow.Source == String.Empty) &&
            (!recRow.IsDeviceCodeNull() && recRow.DeviceCode != -1))
          recRow.Source = ver.GetDeviceName(recRow.DeviceCode);
        //Fill in values of some referential attributes (e.g. GPS pos)
        FillRefAttrVals(recRow);
        //Obtain names of fleet hierarchy items based on HierarchyItemID
        if (!recRow.IsHierarchyItemIDNull() && recRow.HierarchyItemID != -1)
        { //Valid hierarchy item ID - obtain corresponding vehicle and train names
          FleetHierarchy hier = dddHelper.HierarchyHelper.ObtainHierarchy(recRow.StartTime);
          FleetHierarchyItem hierItem = hier.GetHierarchyItem(recRow.HierarchyItemID);
          FHVehicle vehicle = hierItem.GetVehicle();
          FHTrain train = hierItem.GetTrain();
          if (vehicle != null)
            recRow.VehicleName = vehicle.Name;
          if (train != null)
            recRow.TrainName = train.Name;
        }
        //Set to NULL attributes with special value
        if (!recRow.IsHierarchyItemIDNull() && recRow.HierarchyItemID == -1)
          recRow.SetHierarchyItemIDNull();
        if (!recRow.IsEndTimeNull() && recRow.EndTime == DateTime.MinValue)
          recRow.SetEndTimeNull();
        if (!recRow.IsImportTimeNull() && recRow.ImportTime == DateTime.MinValue)
          recRow.SetImportTimeNull();
        if (!recRow.IsAcknowledgeTimeNull() && recRow.AcknowledgeTime == DateTime.MinValue)
          recRow.SetAcknowledgeTimeNull();
        if (!recRow.IsVehicleNumberNull() && recRow.VehicleNumber == -1)
          recRow.SetVehicleNumberNull();
        if (!recRow.IsDeviceCodeNull() && recRow.DeviceCode == -1)
          recRow.SetDeviceCodeNull();
      }
      catch (Exception)
      { } //Failed to obtain record attributes
    }

    /// <summary>
    /// Parses binary data for given record and fills parsed values into record row.
    /// </summary>
    /// <param name="recRow">Row with record data, where parsed values are inserted</param>
    private void FillRefAttrVals(DiagnosticRecords.RecordsRow recRow)
    {
      if (recRow.IsBinDataNull())
        return; //No binary data to parse

      sRecBinData recBinData;
      List<sParsedRefAttrValue> parsedValues;
      eParseStatus parseStat;
      //Create structure with record binary data
      recBinData.TUInstanceID = recRow.TUInstanceID;
      recBinData.RecordInstID = recRow.RecordInstID;
      recBinData.DDDVersionID = recRow.DDDVersionID;
      recBinData.RecordDefID = recRow.RecordDefID;
      recBinData.StartTime = recRow.StartTime;
      if (!recRow.IsEndTimeNull())
        recBinData.EndTime = recRow.EndTime;
      else
        recBinData.EndTime = DateTime.MinValue;
      recBinData.BigEndian = recRow.BigEndian;
      recBinData.BinData = (byte[])recRow.BinData;
      //Call parse method of DDDHelper
      parseStat = dddHelper.ParseBinRecordRefAttr(recBinData, out parsedValues);
      //Check parsing result
      if (parseStat == eParseStatus.parseSuccessfull)
      { //Parsing succeeded
        foreach (sParsedRefAttrValue refAttrVal in parsedValues)
        { //Iterate over parsed values, check ID of referential attributes
          if (refAttrVal.RefAttrID == (int)eSystemRefAttributes.gpsLatitude)
            recRow.GPSLatitude = (float)refAttrVal.Value; //Found GPS latitude
          else if (refAttrVal.RefAttrID == (int)eSystemRefAttributes.gpsLongitude)
            recRow.GPSLongitude = (float)refAttrVal.Value; //Found GPS longitude
        }
      }
    }

    private bool CountLimitExceeded()
    {
      if ((MaxRecCount != 0) && (diagRecords.Records.Count >= MaxRecCount))
        return true;
      return false;
    }

    /// <summary>
    /// Computes number of records for each fault code
    /// </summary>
    private void ComputeRecStats()
    {
      recCounts.Clear(); //Clear record counts
      if (!ComputeRecTypeCounts)
        return;
      OnInfoMessage("Computing record counts for each type");
      String recIDStr;
      foreach (DataRowView rowView in RecordsView)
      {//Iterate over all rows in selector Rows table
        DiagnosticRecords.RecordsRow srcRow = (DiagnosticRecords.RecordsRow)rowView.Row;
        recIDStr = DDDRecord.GetRecIDString(srcRow.TUInstanceID, srcRow.DDDVersionID, srcRow.RecordDefID);
        if (recCounts.ContainsKey(recIDStr))
          recCounts[recIDStr]++;
        else
          recCounts[recIDStr] = 1;
      }
    }

    #endregion //Helper functions

    #region Virtual prototypes
    /// <summary>
    /// Prototype of method which prepares parameters for DR load.
    /// Method is called synchronously, so it can access all internal components
    /// </summary>
    /// <returns>Object containing all necessary parameters for DR selection</returns>
    protected virtual DRLoadParameters PrepareLoadParameters()
    {
      return new DRLoadParameters();
    }

    /// <summary>
    /// Method signature conforms to AsyncDRLoadHandler delegate.
    /// Method is invoked asynchronously and performs the load of DR.
    /// All callbacks has to be performed using internal delegates.
    /// Method must not perform any cross thread calls!!!
    /// </summary>
    /// <param name="loadParams">Object containing all necessary parameters for DR selection</param>
    /// <param name="asyncOp">AsyncOperation used to post callbacks</param>
    protected virtual void PerformAsyncDRLoad(DRLoadParameters loadParams, AsyncOperation asyncOp)
    {
      return;
    }

    /// <summary>
    /// Saves settings of selector to file
    /// </summary>
    /// <param name="file">Stream to save to</param>
    /// <param name="formatter">Formatter used for saving objects</param>
    public virtual void SaveSettingsToFile(FileStream file, BinaryFormatter formatter)
    {
      return;
    }

    /// <summary>
    /// Loads settings of selector from file
    /// </summary>
    /// <param name="file">Stream to load from</param>
    /// <param name="formatter">Formatter used for loading objects</param>
    public virtual void LoadSettingsFromFile(FileStream file, BinaryFormatter formatter)
    {
      return;
    }
    #endregion //Virtual prototypes

    #region Public methods
    /// <summary>
    /// Method fills internal data set with selected records.
    /// Method performs the load asynchronously.
    /// After completition, NewDataReady event is raised.
    /// In case of failure ErrorMessage event is raised.
    /// During load operation InfoMessage events are raised reporting progress.
    /// </summary>
    public void LoadRecordsAsync()
    {
      try
      {
        //Check if the operation is not running already
        if (IsBusy)
          throw new InvalidOperationException("Component is already performing load of DR");
        //Set component cursor to busy
        this.ParentForm.Enabled = false;
        this.ParentForm.Activate();
        this.ParentForm.Cursor = Cursors.WaitCursor;
        //Disable component
        this.Enabled = false;
        if (removeOldRecords)
        { //clear dataset first
          InitiateDataSet();
        }
      
        //Prepare load parameters
        DRLoadParameters loadParams = PrepareLoadParameters();
        /*TEST*/OnInfoMessage("Starting asynchronous operation");
        //Crate AsyncOperation object
        asyncOp = AsyncOperationManager.CreateOperation(this);
        //Invoke asynchronously load operation
        asyncDRLoadDelegate.BeginInvoke(loadParams, asyncOp, null, null);
      }
      catch (Exception ex)
      { //Handle all errors
        //Set component cursor to normal
        this.ParentForm.Enabled = true;
        this.ParentForm.Cursor = Cursors.Arrow;
        this.Enabled = true;
        OnErrorMessage(ex); //Pass errors into error event handler
      }
    }

    /// <summary>
    /// Selects list of variable values for specified record
    /// </summary>
    /// <param name="TUInstanceID">Identification of TU Instance</param>
    /// <param name="RecordInstID">Identification of selected record</param>
    /// <param name="RecordDefID">Identification of selected record</param>
    /// <returns>list of variable values for specified record</returns>
    public List<VarValue> GetVarValuesForRecord(Guid TUInstanceID, Int64 RecordInstID, Int32 RecordDefID)
    {
      sRecBinData recBinData;
      List<sParsedVarValue> parsedValues;
      eParseStatus parseStat;
      List<VarValue> values = new List<VarValue>();
      VarValue value;
      //Find record in view
      int recRowIndx = RecordsView.Find(new Object[] { TUInstanceID, RecordInstID, RecordDefID });
      if (recRowIndx == -1)
        return values; //Record not found - no values available
      DiagnosticRecords.RecordsRow recRow = (DiagnosticRecords.RecordsRow)RecordsView[recRowIndx].Row;
      //Parse binary data for record
      recBinData.TUInstanceID = recRow.TUInstanceID;
      recBinData.RecordInstID = recRow.RecordInstID;
      recBinData.DDDVersionID = recRow.DDDVersionID;
      recBinData.RecordDefID = recRow.RecordDefID;
      recBinData.StartTime = recRow.StartTime;
      if (!recRow.IsEndTimeNull())
        recBinData.EndTime = recRow.EndTime;
      else
        recBinData.EndTime = DateTime.MinValue;
      recBinData.BigEndian = recRow.BigEndian;
      if (!recRow.IsBinDataNull())
        recBinData.BinData = (byte[])recRow.BinData;
      else
        recBinData.BinData = new byte[0];
      //Call parse method of DDDHelper
      parseStat = dddHelper.ParseBinRecord(recBinData, out parsedValues);
      //Check parsing result
      if (parseStat == eParseStatus.parseSuccessfull)
      { //Parsing succeeded
        foreach (sParsedVarValue parsedVarVal in parsedValues)
        { //Insert each parsed value into Variables table
          //Check for filter
          if ((varFilter != null) && !varFilter.ContainsVarID(recRow.TUInstanceID, recRow.DDDVersionID, parsedVarVal.VariableID))
            continue; //Variable is filtered out
          value = new VarValue();
          value.TUInstanceID = recRow.TUInstanceID;
          value.RecordInstID = recRow.RecordInstID;
          value.DDDVersionID = recRow.DDDVersionID;
          value.RecordDefID = recRow.RecordDefID;
          value.InputIndex = parsedVarVal.InputIndex;
          value.VariableID = parsedVarVal.VariableID;
          value.TimeStamp = parsedVarVal.TimeStamp;
          value.ValueIndex = parsedVarVal.ValueIndex;
          value.ValueArrayIndex = parsedVarVal.ValueArrayIndex;
          value.Value = parsedVarVal.Value;
          values.Add(value);
        }
      }
      return values;
    }

    /// <summary>
    /// Selects list of variable values for specified variables from all records
    /// </summary>
    /// <param name="variables">dictionary with definition of requiered variables</param>
    /// <returns>list of variable values for specified variables</returns>
    public List<VarValue> GetValuesOfVariables(VarIDDictionary variables)
    {
      sRecBinData recBinData;
      List<sParsedVarValue> parsedValues;
      eParseStatus parseStat;
      List<VarValue> values = new List<VarValue>();
      VarValue value;
      //Walk over all records in records view
      foreach (DataRowView recRowView in RecordsView)
      {
        DiagnosticRecords.RecordsRow recRow = (DiagnosticRecords.RecordsRow)recRowView.Row;
        //Parse binary data for record
        recBinData.TUInstanceID = recRow.TUInstanceID;
        recBinData.RecordInstID = recRow.RecordInstID;
        recBinData.DDDVersionID = recRow.DDDVersionID;
        recBinData.RecordDefID = recRow.RecordDefID;
        recBinData.StartTime = recRow.StartTime;
        if (!recRow.IsEndTimeNull())
          recBinData.EndTime = recRow.EndTime;
        else
          recBinData.EndTime = DateTime.MinValue;
        recBinData.BigEndian = recRow.BigEndian;
        if (!recRow.IsBinDataNull())
          recBinData.BinData = (byte[])recRow.BinData;
        else
          recBinData.BinData = new byte[0];
        parsedValues = null;
        //Call parse method of DDDHelper
        parseStat = dddHelper.ParseBinRecord(recBinData, out parsedValues);
        //Check parsing result
        if (parseStat == eParseStatus.parseSuccessfull)
        { //Parsing succeeded
          foreach (sParsedVarValue parsedVarVal in parsedValues)
          { //Add requiered values to output list
            if (!variables.ContainsVarID(recRow.TUInstanceID, recRow.DDDVersionID, parsedVarVal.VariableID))
              continue; //Variable not requiered 
            //Check for filter
            if ((varFilter != null) && !varFilter.ContainsVarID(recRow.TUInstanceID, recRow.DDDVersionID, parsedVarVal.VariableID))
              continue; //Variable is filtered out
            //Add variable to output list
            value = new VarValue();
            value.TUInstanceID = recRow.TUInstanceID;
            value.RecordInstID = recRow.RecordInstID;
            value.DDDVersionID = recRow.DDDVersionID;
            value.RecordDefID = recRow.RecordDefID;
            value.InputIndex = parsedVarVal.InputIndex;
            value.VariableID = parsedVarVal.VariableID;
            value.TimeStamp = parsedVarVal.TimeStamp;
            value.ValueIndex = parsedVarVal.ValueIndex;
            value.ValueArrayIndex = parsedVarVal.ValueArrayIndex;
            value.Value = parsedVarVal.Value;
            values.Add(value);
          }
        }
      }
      return values;
    }
    
    /// <summary>
    /// Selects list of variable values for specified variable from all records
    /// </summary>
    /// <param name="tuInstance"></param>
    /// <param name="dddVersion"></param>
    /// <param name="variable"></param>
    /// <returns>list of variable values for specified variable</returns>
    public List<VarValue> GetValuesOfVariable(TUInstance tuInstance, DDDVersion dddVersion, DDDVariable variable)
    {
      VarIDDictionary dict = new VarIDDictionary();
      dict.AddVarID(tuInstance, dddVersion, variable);
      return GetValuesOfVariables(dict);
    }

    /// <summary>
    /// Save all records from internal data set as XML file
    /// </summary>
    /// <param name="fileName">Name of target file</param>
    public void SaveRecordsToFile(String fileName)
    {
      try
      {
        OnInfoMessage("Saving selected records to file " + fileName);

        //Open file stream
        FileStream outFile = new FileStream(fileName, FileMode.Create, FileAccess.Write);
        //Create records serializer
        DRSerializer recSer = new DRSerializer(DDDHelper, outFile, true);
        //Write all records from dataset to file
        foreach (DiagnosticRecords.RecordsRow row in diagRecords.Records)
          recSer.WriteRecordToStream(
            row.TUInstanceID, row.RecordInstID, row.DDDVersionID, 
            row.RecordDefID, DDDHelper.UserTimeToUTC(row.StartTime),
            (!row.IsEndTimeNull()) ? DDDHelper.UserTimeToUTC(row.EndTime) : DateTime.MinValue,
            DDDHelper.UserTimeToUTC(row.LastModified), 
            row.ParseStatusID, row.BigEndian, row.Source, 
            (!row.IsHierarchyItemIDNull()) ? row.HierarchyItemID : -1, 
            (!row.IsImportTimeNull()) ? row.ImportTime : DateTime.MinValue,
            (!row.IsAcknowledgeTimeNull()) ? DDDHelper.UserTimeToUTC(row.AcknowledgeTime) : DateTime.MinValue,
            (!row.IsVehicleNumberNull()) ? (short)row.VehicleNumber : (short)-1,
            (!row.IsDeviceCodeNull()) ? (short)row.DeviceCode : (short)-1,
            row.BinData);
        //Write used DDD objects to file
        recSer.WriteUsedDDDObjects();

        //Close and dispose stream
        recSer.CloseZipStream();
        outFile.Close();
        outFile.Dispose();
        OnInfoMessage("Records saved");
      }
      catch (Exception ex)
      {
        OnErrorMessage(new Exception("Failed to save diagnostic records", ex));
      }
    }

    /// <summary>
    /// Asynchronously loads records to internal dataset from specified file
    /// </summary>
    /// <param name="fileName">Source file name</param>
    /// <param name="fileType">Type of the file to load</param>
    public void LoadRecordsFromFile(String fileName, eRecFileType fileType)
    {
      //Clear old data from datase
      InitiateDataSet();
      //Open file stream
      FileStream inFile = new FileStream(fileName, FileMode.Open, FileAccess.Read);
      //Create records serializer
      DRSerializerBase recSer;
      switch(fileType)
      {
        case eRecFileType.BinaryFile:
          recSer = new DRSerializer(DDDHelper, inFile, false);
          break;
        case eRecFileType.XMLFile:
          recSer = new DRSerializerXML(DDDHelper, inFile, false);
          break;
        default:
          recSer = new DRSerializer(DDDHelper, inFile, false);
          break;
      }
      //Read all available records from a stream
      DRSerializer.DiagnosticRecord record;
      while ((record = recSer.ReadRecordFromStream()) != null)
        diagRecords.Records.AddRecordsRow(
          record.TUInstanceID, record.RecordInstID, record.DDDVersionID,
          record.RecordDefID, DDDHelper.UTCToUserTime(record.StartTime),
          (record.EndTime != DateTime.MinValue) ? DDDHelper.UTCToUserTime(record.EndTime) : record.EndTime,
          (record.LastModified != DateTime.MinValue) ? DDDHelper.UTCToUserTime(record.LastModified) : record.LastModified,
          record.Source,
          record.HierarchyItemID, record.ImportTime,
          (record.AcknowledgeTime != DateTime.MinValue) ? DDDHelper.UTCToUserTime(record.AcknowledgeTime) : record.AcknowledgeTime,
          record.VehicleNumber, record.DeviceCode,
          record.ParseStatusID, null, null, null, null, 0, null, null, null, null, null, 0, 0,
          record.BigEndian, record.BinData, 0, 0); 
      //Deserialize and store all available DDD objects
      recSer.ReadUsedDDDObjects();
      //Walk through all records in dataset again and add attributes from DDD Objects
      foreach (DiagnosticRecords.RecordsRow row in diagRecords.Records)
        FillCommonRecAttributes(row);

      //Close and dispose stream
      inFile.Close();
      inFile.Dispose();
    }

    /// <summary>
    /// Applies specified filters to data views and raises new data ready event
    /// </summary>
    /// <param name="recordFilter">Filter for records data view</param>
    /// <param name="variableFilter">Filter for variables data view</param>
    public void ApplyFilter(String recordFilter, VarIDDictionary variableFilter)
    {
      if ((recordFilter == recordsView.RowFilter) && (variableFilter == null))
        return;
      try
      {
        //Set filter expressions
        if (recordFilter != recordsView.RowFilter)
          recordsView.RowFilter = recordFilter;
        //Store filter for variables
        this.varFilter = variableFilter;
        //Raise new data ready event
        OnNewDataReady(true, false);
      }
      catch(Exception ex)
      {
        OnErrorMessage(new Exception("Failed to apply filter expressions", ex));
      }
    }

    /// <summary>
    /// Removes filter expressions from data views and raises new data ready event
    /// </summary>
    public void RemoveFilter()
    {
      if ((recordsView.RowFilter == "") && (varFilter == null))
        return;
      try
      {
        //Remove filter expressions
        recordsView.RowFilter = "";
        //Remove filter for variables
        varFilter = null;
        //Raise new data ready event
        OnNewDataReady(true, false);
      }
      catch (Exception ex)
      {
        OnErrorMessage(new Exception("Failed to remove filter expressions", ex));
      }
    }

    /// <summary>
    /// Returns number of instances of specified record type in internal dataset.
    /// If specified rec type is not in dictionary, returns -1
    /// </summary>
    /// <param name="TUInstanceID"></param>
    /// <param name="DDDVersionID"></param>
    /// <param name="RecordDefID"></param>
    /// <returns>Number of record instances or -1</returns>
    public int GetRecTypeCount(Guid TUInstanceID, Guid DDDVersionID, int RecordDefID)
    {
      int count = -1;
      String recIDStr = DDDRecord.GetRecIDString(TUInstanceID, DDDVersionID, RecordDefID);
      if (recCounts.TryGetValue(recIDStr, out count))
        return count;
      else
        return -1;
    }
    #endregion //Public methods
  }

  #region Selector exception
  /// <summary>
  /// Exception class thrown by all DR Selector objects
  /// </summary>
  class DRSelectorException : Exception
  {
    public DRSelectorException(String message, params Object[] args)
      : base(String.Format(message, args))
    { }
    public DRSelectorException(Exception inner, String message, params Object[] args)
      : base(String.Format(message, args), inner)
    { }
  }
  #endregion //Selector exception

  #region Load parameters class
  /// <summary>
  /// Internal class representing parameters for DR Load operation.
  /// Class is specialized by each selector.
  /// </summary>
  public class DRLoadParameters
  {
  }
  #endregion //Load parameters class

  #region NDREventArgs class
  public class NDREventArgs : EventArgs
  {
    /// <summary>
    /// Event is raised just because filter expressions changed
    /// </summary>
    public bool Filtered = false;

    /// <summary>
    /// Limit for record count or variable count was reached
    /// </summary>
    public bool CountLimitReached = false;

    public NDREventArgs(bool filtered, bool countLimit)
    {
      Filtered = filtered;
      CountLimitReached = countLimit;
    }
  }
  #endregion //NDREventArgs class

  #region RecCounEventArgs class
  public class RecCounEventArgs : EventArgs
  {
    /// <summary>
    /// Number of available records
    /// </summary>
    public int RecCount = 0;

    public RecCounEventArgs(int recCount)
    {
      RecCount = recCount;
    }
  }
  #endregion //RecCounEventArgs class

  #region SelectRecordEventArgs class
  public class SelectRecordEventArgs : EventArgs
  {
    /// <summary>
    /// ID of TU Instance which generated the record
    /// </summary>
    public Guid TUInstanceID;

    /// <summary>
    /// ID of record instance
    /// </summary>
    public Int64 RecordInstID;

    /// <summary>
    /// ID of record definition
    /// </summary>
    public Int32 RecordDefID;

    /// <summary>
    /// Index of record in data table
    /// </summary>
    public Int32 DataTableIndex;

    public SelectRecordEventArgs(Guid tuInstanceID, Int64 recordInstID, Int32 recordDefID, Int32 dataTableIndex)
    {
      TUInstanceID = tuInstanceID;
      RecordInstID = recordInstID;
      RecordDefID = recordDefID;
      DataTableIndex = dataTableIndex;
    }
  }
  #endregion //SelectRecordEventArgs class


  #region Variable value and dictionary
  #region VarValue class
  public class VarValue : IComparable<VarValue>
  {
    public Guid TUInstanceID;
    public Int64 RecordInstID;
    public Guid DDDVersionID;
    public int RecordDefID;
    public int InputIndex;
    public int VariableID;
    public DateTime TimeStamp;
    public int ValueIndex;
    public int ValueArrayIndex;
    public Object Value;

    #region IComparable implementation
    public int CompareTo(VarValue val)
    {
      return TimeStamp.CompareTo(val.TimeStamp);
    }
    #endregion //IComparable implementation
  }
  #endregion //VarValue class

  #region Internal hleper classes
  internal class VarDefIDKey : IEquatable<VarDefIDKey>
  {
    public Guid TUInstanceID;
    public Guid DDDVersionID;
    public int VariableID;
    public VarDefIDKey(Guid TUInstanceID, Guid DDDVersionID, int VariableID)
    {
      this.TUInstanceID = TUInstanceID;
      this.DDDVersionID = DDDVersionID;
      this.VariableID = VariableID;
    }
    public bool Equals(VarDefIDKey varDef)
    {
      return ((this.TUInstanceID == varDef.TUInstanceID) && (this.DDDVersionID == varDef.DDDVersionID) && 
              (this.VariableID == varDef.VariableID));
    }
    public override int GetHashCode()
    {
      return TUInstanceID.GetHashCode() ^ DDDVersionID.GetHashCode() ^ VariableID;
    }
  }

  public class VarDefIDValue
  {
    public int Index;
    public DDDVariable DDDVariable;
    public DDDVersion DDDVersion;
    public TUInstance TUInstance;
    public Object UserObj;
    public VarDefIDValue(int Index, DDDVariable variable, DDDVersion version, TUInstance tuInstance)
    {
      this.Index = Index;
      this.DDDVariable = variable;
      this.DDDVersion = version;
      this.TUInstance = tuInstance;
      UserObj = null;
    }
  }
  public class BitDefIDValue : VarDefIDValue
  {
    public Int64 Mask;
    public BitDefIDValue(int Index, DDDVariable variable, DDDVersion version, TUInstance tuInstance, Int64 mask)
      : base(Index, variable, version, tuInstance)
    {
      Mask = mask;
    }
  }
  public class BitsetDefIDValue : VarDefIDValue
  {
    private SortedList<Int64, BitDefIDValue> bits;
    public BitsetDefIDValue(DDDVariable variable, DDDVersion version, TUInstance tuInstance)
      : base(-1, variable, version, tuInstance)
    {
      bits = new SortedList<Int64, BitDefIDValue>();
    }
    public BitDefIDValue AddBit(Int64 mask, int index)
    {
      BitDefIDValue bit;
      if ((bit = GetBit(mask)) == null)
      {
        bit = new BitDefIDValue(index, DDDVariable, DDDVersion, TUInstance, mask);
        bits.Add(mask, bit);
      }
      return bit;
    }
    public BitDefIDValue GetBit(Int64 mask)
    {
      if (bits.ContainsKey(mask))
        return bits[mask];
      else
        return null;
    }
    public int GetBitIndex(Int64 mask)
    {
      if (bits.ContainsKey(mask))
        return bits[mask].Index;
      else
        return -1;
    }
    public bool ContainsBit(Int64 mask)
    {
      return bits.ContainsKey(mask);
    }
    public IList<Int64> GetUsedMasks()
    {
      return bits.Keys;
    }
    public IList<BitDefIDValue> GetUsedBits()
    {
      return bits.Values;
    }
  }
  #endregion //Internal hleper classes

  #region VarIDDictionary
  public class VarIDDictionary
  {
    private Dictionary<VarDefIDKey, VarDefIDValue> dictionary;
    private int currIndex = 0;
    public List<VarDefIDValue> Variables;
    public VarIDDictionary()
    {
      dictionary = new Dictionary<VarDefIDKey, VarDefIDValue>();
      Variables = new List<VarDefIDValue>();
    }
    public void Clear()
    {
      dictionary.Clear();
      currIndex = 0;
    }
    public bool ContainsVarID(Guid TUInstanceID, Guid DDDVersionID, int VariableID)
    {
      return dictionary.ContainsKey(new VarDefIDKey(TUInstanceID, DDDVersionID, VariableID));
    }
    public bool ContainsBit(Guid TUInstanceID, Guid DDDVersionID, int VariableID, Int64 mask)
    {
      VarDefIDKey key = new VarDefIDKey(TUInstanceID, DDDVersionID, VariableID);
      VarDefIDValue val;
      if (!dictionary.TryGetValue(key, out val) || (val.GetType() != typeof(BitsetDefIDValue)))
        return false;
      else
        return ((BitsetDefIDValue)dictionary[key]).ContainsBit(mask);
    }
    public void AddVarID(TUInstance tuInstance, DDDVersion dddVersion, DDDVariable variable)
    {
      if (!ContainsVarID(tuInstance.TUInstanceID, dddVersion.VersionID, variable.VariableID))
      {
        VarDefIDValue val = new VarDefIDValue(currIndex++, variable, dddVersion, tuInstance);
        dictionary.Add(new VarDefIDKey(tuInstance.TUInstanceID, dddVersion.VersionID, variable.VariableID), val);
        Variables.Add(val);
      }
    }
    public void AddBitID(TUInstance tuInstance, DDDVersion dddVersion, DDDVariable variable, Int64 mask)
    {
      VarDefIDKey key = new VarDefIDKey(tuInstance.TUInstanceID, dddVersion.VersionID, variable.VariableID);
      VarDefIDValue val;
      BitsetDefIDValue bitset;
      if (!dictionary.TryGetValue(key, out val))
      {
        val = new BitsetDefIDValue(variable, dddVersion, tuInstance);
        dictionary.Add(key, val);
      }
      bitset = val as BitsetDefIDValue;
      if (!bitset.ContainsBit(mask))
        Variables.Add(bitset.AddBit(mask, currIndex++));
    }
    public void AddBitsetBitIDs(TUInstance tuInstance, DDDVersion dddVersion, DDDElemVar variable)
    {
      DDDBitsetRepres bitsetRepres = (DDDBitsetRepres)variable.Representation;
      foreach (long mask in bitsetRepres.GetUsedBitMasks())
        AddBitID(tuInstance, dddVersion, variable, mask);
    }
    public VarDefIDValue GetVariable(Guid TUInstanceID, Guid DDDVersionID, int VariableID)
    {
      VarDefIDKey key = new VarDefIDKey(TUInstanceID, DDDVersionID, VariableID);
      if (dictionary.ContainsKey(key))
        return dictionary[key];
      else
        return null;
    }
    public BitDefIDValue GetBit(Guid TUInstanceID, Guid DDDVersionID, int VariableID, Int64 mask)
    {
      VarDefIDKey key = new VarDefIDKey(TUInstanceID, DDDVersionID, VariableID);
      VarDefIDValue val;
      if (dictionary.TryGetValue(key, out val) && (val.GetType() == typeof(BitsetDefIDValue)))
        return ((BitsetDefIDValue)dictionary[key]).GetBit(mask);
      else
        return null;
    }
    public bool HasBits(Guid TUInstanceID, Guid DDDVersionID, int VariableID)
    {
      VarDefIDKey key = new VarDefIDKey(TUInstanceID, DDDVersionID, VariableID);
      VarDefIDValue val;
      if (dictionary.TryGetValue(key, out val) && (val.GetType() == typeof(BitsetDefIDValue)))
        return true;
      else
        return false;
    }
    public List<String> GetVarIDStrings()
    {
      List<String> strIDs = new List<string>();
      foreach (VarDefIDValue varDef in Variables)
      {
        if (varDef.GetType() == typeof(BitDefIDValue))
          strIDs.Add( DDDBitsetRepres.GetBitIDString((ulong)((BitDefIDValue)varDef).Mask, 
            DDDVariable.GetVarIDString(varDef.TUInstance.TUInstanceID, varDef.DDDVersion.VersionID, varDef.DDDVariable.VariableID)));
        else
          strIDs.Add(DDDVariable.GetVarIDString(varDef.TUInstance.TUInstanceID, varDef.DDDVersion.VersionID, varDef.DDDVariable.VariableID));
      }
      return strIDs;
    }
  }
  #endregion //VarIDDictionary
  #endregion //Variables and values

  #region Record file type enumeration
  public enum eRecFileType
  {
    BinaryFile = 1,
    XMLFile = 2
  }
  #endregion //Record file type enumeration
}
