using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net;
using ZetaLibWeb;

namespace DRSelectorComponents
{
  /// <summary>
  /// Component enabling to select diagnostic records based on TUInstance and time
  /// </summary>
  public partial class DRSelectorTUDirect : DRSelectorComponents.DRSelectorBase
  {
    #region Public properties
    #endregion //Public properties

    #region Construction and initialization
    public DRSelectorTUDirect() 
      : base() //Call base constructor
    {
      InitializeComponent();
    }

    public DRSelectorTUDirect(Dictionary<String, Object> selParams)
      : base(selParams) //Call base constructor
    {
      InitializeComponent();
    }
    #endregion //Construction and initialization

    #region Component event handlers
    private void DRSelectorTUDirect_Load(object sender, EventArgs e)
    {
      //Set query scope to all
      cbScope.SelectedIndex = 0;
      //Fill list of previously stored addresses of TUs
      LoadTUAddresses();
      //Preset time interval
      dtInterval.PredefIntervalIndex = 1;
    }

    /// <summary>
    /// Handles event from Select button, calls base selector methods
    /// </summary>
    private void btSelectRecords_Click(object sender, EventArgs e)
    {
      //Store selected TU address if it does not exist yet
      StoreTUAddress(cbTUAddress.Text);
      //Start asynchronous record load
      LoadRecordsAsync();
      //Set form title
      this.ParentForm.Text = cbTUAddress.Text;
    }

    /// <summary>
    /// Handles key up event from addresses combo box.
    /// Provides the lookup feature.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void cbTUAddress_KeyUp(object sender, KeyEventArgs e)
    {
      int index;
      string actual;
      string found;

      // Do nothing for certain keys, such as navigation keys.
      if ((e.KeyCode == Keys.Back) ||
          (e.KeyCode == Keys.Left) ||
          (e.KeyCode == Keys.Right) ||
          (e.KeyCode == Keys.Up) ||
          (e.KeyCode == Keys.Down) ||
          (e.KeyCode == Keys.Delete) ||
          (e.KeyCode == Keys.PageUp) ||
          (e.KeyCode == Keys.PageDown) ||
          (e.KeyCode == Keys.Home) ||
          (e.KeyCode == Keys.End) ||
          (e.KeyCode == Keys.ShiftKey) ||
          (e.KeyCode == Keys.Tab) ||
          (e.KeyCode == Keys.Menu))
      {
        return;
      }

      // Store the actual text that has been typed.
      actual = cbTUAddress.Text.Substring(0, cbTUAddress.SelectionStart);

      // Find the first match for the typed value.
      index = cbTUAddress.FindString(actual);

      // If current text is correct
      if (index > -1)
      {
        found = cbTUAddress.Items[index].ToString();

        // Select this item from the list.
        cbTUAddress.SelectedIndex = index;
        cbTUAddress.Text = found;

        // Select the portion of the text that was automatically
        // added so that additional typing replaces it.
        cbTUAddress.SelectionStart = actual.Length;
        cbTUAddress.SelectionLength = found.Length;
      }
    }

    /// <summary>
    /// Displays the drop down portion of addresses combo box
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void cbTUAddress_KeyPress(object sender, KeyPressEventArgs e)
    {
      //Drop down the list box if there is more than one item
      if (cbTUAddress.Items.Count > 0)
        cbTUAddress.DroppedDown = true;
    }
    #endregion //Component event handlers

    #region Virtual method overrides
    /// <summary>
    /// Method reads values from internal components and prepares DRLoadParametersTUDirect object
    /// </summary>
    /// <returns>Object containing all necessary parameters for DR selection</returns>
    protected override DRLoadParameters PrepareLoadParameters()
    {
      //Create parameters object
      DRLoadParametersTUDirect loadParams = new DRLoadParametersTUDirect();
      //Store selected values to load params
      loadParams.tuAddress = cbTUAddress.Text;
      if (dtInterval.FromSelected)
        //loadParams.timeFrom = dddHelper.UserTimeToUTC(dtInterval.TimeFrom).ToString("dd-MM-yyyy HH:mm").Replace("-","/");
        loadParams.timeFrom = dtInterval.TimeFrom.ToString("dd-MM-yyyy HH:mm").Replace("-", "/");
      else
        loadParams.timeFrom = "";
      if (dtInterval.ToSelected)
        //loadParams.timeTo = dddHelper.UserTimeToUTC(dtInterval.TimeTo).ToString("dd-MM-yyyy HH:mm").Replace("-", "/");
        loadParams.timeTo = dtInterval.TimeTo.ToString("dd-MM-yyyy HH:mm").Replace("-", "/");
      else
        loadParams.timeTo = "";
      switch(cbScope.SelectedIndex)
      {
        case 0:
          loadParams.queryScope = "all";
          break;
        case 1:
          loadParams.queryScope = "A";
          break;
        case 2:
          loadParams.queryScope = "A1";
          break;
        case 3:
          loadParams.queryScope = "B";
          break;
        case 4:
          loadParams.queryScope = "B1";
          break;
        case 5:
          loadParams.queryScope = "C";
          break;
        case 6:
          loadParams.queryScope = "evt";
          break;
        default:
          loadParams.queryScope = "all";
          break;
      }
      loadParams.faultCode = tbFaultCode.Text;
      loadParams.recSource = tbSource.Text;
      loadParams.maxRecords = tbMaxRecords.Text;
      loadParams.maxRecTypes = tbMaxRecTypes.Text;
      return loadParams;
    }

    /// <summary>
    /// Method is invoked asynchronously and performs the load of DR.
    /// </summary>
    /// <param name="loadParams">Object containing all necessary parameters for DR selection</param>
    /// <param name="asyncOp">AsyncOperation used to post callbacks</param>
    protected override void PerformAsyncDRLoad(DRLoadParameters loadParams, AsyncOperation asyncOp)
    {
      try
      {
        //Cast parameters object to proper type
        DRLoadParametersTUDirect parameters = loadParams as DRLoadParametersTUDirect;
        String tuQueryName;
        String tmpFileName;

        //Execute query in TU
        asyncOp.Post(onInfoMessageDelegate, "Executing query in " + parameters.tuAddress);
        tuQueryName = ExecuteTUQuery(parameters);
        //Download records from TU
        asyncOp.Post(onInfoMessageDelegate, "Downloading selected records from " + parameters.tuAddress);
        tmpFileName = DownloadRecords(parameters, tuQueryName);
        //Load records from tmp file
        asyncOp.Post(onInfoMessageDelegate, "Loading records from temporary file");
        LoadRecordsFromFile(tmpFileName, eRecFileType.XMLFile);
        //Delete temporary file
        asyncOp.Post(onInfoMessageDelegate, "Deleting temporary file");
        File.Delete(tmpFileName);

        //Post completed event
        AsyncCompletedEventArgs result = new AsyncCompletedEventArgs(null, false, null);
        asyncOp.PostOperationCompleted(onDRLoadCompletedDelegate, result);
      }
      catch (Exception ex)
      {
        //Post completed event with error
        AsyncCompletedEventArgs result = new AsyncCompletedEventArgs(ex, false, null);
        asyncOp.PostOperationCompleted(onDRLoadCompletedDelegate, result);
      }
    }

    /// <summary>
    /// Saves settings of selector to file
    /// </summary>
    /// <param name="file">Stream to save to</param>
    /// <param name="formatter">Formatter used for saving objects</param>
    public override void SaveSettingsToFile(FileStream file, BinaryFormatter formatter)
    {
      //Save time range
      formatter.Serialize(file, dtInterval.FromSelected);
      formatter.Serialize(file, dtInterval.TimeFrom);
      formatter.Serialize(file, dtInterval.ToSelected);
      formatter.Serialize(file, dtInterval.TimeTo);
      formatter.Serialize(file, dtInterval.PredefIntervalIndex);
      //Save TU address
      formatter.Serialize(file, cbTUAddress.Text);
      //Save all stored TU addresses
      formatter.Serialize(file, cbTUAddress.Items.Count);
      foreach(String item in cbTUAddress.Items)
        formatter.Serialize(file, cbTUAddress.Items.Count);
      //Save query scope
      formatter.Serialize(file, cbScope.SelectedIndex);
      //Save other parameters
      formatter.Serialize(file, tbFaultCode.Text);
      formatter.Serialize(file, tbSource.Text);
      formatter.Serialize(file, tbMaxRecords.Text);
      formatter.Serialize(file, tbMaxRecTypes.Text);
    }

    /// <summary>
    /// Loads settings of selector from file
    /// </summary>
    /// <param name="file">Stream to save to</param>
    /// <param name="formatter">Formatter used for saving objects</param>
    public override void LoadSettingsFromFile(FileStream file, BinaryFormatter formatter)
    {
      //Load time range
      dtInterval.FromSelected = (bool)formatter.Deserialize(file);
      dtInterval.TimeFrom = (DateTime)formatter.Deserialize(file);
      dtInterval.ToSelected = (bool)formatter.Deserialize(file);
      dtInterval.TimeTo = (DateTime)formatter.Deserialize(file);
      dtInterval.PredefIntervalIndex = (int)formatter.Deserialize(file);
      //Load TU address
      cbTUAddress.Text = (String)formatter.Deserialize(file);
      //Load all stored TU addresses
      int addrCount = (int)formatter.Deserialize(file);
      for(int i=0; i<addrCount; i++)
        cbTUAddress.Items.Add(formatter.Deserialize(file));
      //Load query scope
      cbScope.SelectedIndex = (int)formatter.Deserialize(file);
      //Load other parameters
      tbFaultCode.Text = (String)formatter.Deserialize(file);
      tbSource.Text = (String)formatter.Deserialize(file);
      tbMaxRecords.Text = (String)formatter.Deserialize(file);
      tbMaxRecTypes.Text = (String)formatter.Deserialize(file);
    }
    #endregion //Virtual method overrides

    #region Helper methods
    /// <summary>
    /// Executes script on TU which executes query with given parameters and saves result under query name, which returns
    /// </summary>
    /// <param name="queryParams">Parameters of the query to execute</param>
    /// <returns>Name of the query generated by TU</returns>
    private String ExecuteTUQuery(DRLoadParametersTUDirect queryParams)
    {
      WebResponse resp = null;
      Stream respStream = null;
      StreamReader reader = null;
      String strResp = null;

      try
      {
        //Prepare query string for the script
        QueryString url = new QueryString();
        url.BeforeUrl = "http://" + queryParams.tuAddress + "/database/query_generate.cgi";
        url["scope"] = queryParams.queryScope;
        url["fcode"] = queryParams.faultCode;
        url["src"] = queryParams.recSource;

        url["from"] = queryParams.timeFrom;
        url["to"] = queryParams.timeTo;
        url["fset"] = "";
        url["fres"] = "";
        url["sort"] = "time";
        url["records"] = queryParams.maxRecords;
        url["types"] = queryParams.maxRecTypes;
        HttpWebRequest queryWebRequest = (HttpWebRequest)System.Net.WebRequest.Create(url.All);
        // Set the 'Method' property of the 'Webrequest' to 'GET'.
        queryWebRequest.Method = "GET";
        //Send request and obtain response
        resp = queryWebRequest.GetResponse();
        //Obtain response stream
        respStream = resp.GetResponseStream();
        //Create reader for response stream
        reader = new StreamReader(respStream, Encoding.ASCII);
        //Read response
        strResp = reader.ReadLine();
      }
      catch (Exception ex)
      { throw ex; }
      finally
      {
        //Close reader
        if (reader != null)
        {
          reader.Close();
          reader.Dispose();
        }
        //Close response stream
        if (respStream != null)
        {
          respStream.Close();
          respStream.Dispose();
        }
        //Close response
        if (resp != null)
          resp.Close();
      }
      //Check if response is an error
      if (strResp == null || (strResp.Length < 3))
        throw new Exception("Empty response from web server");
      if (strResp.ToUpper().StartsWith("ERROR"))
        throw new Exception(strResp);
      //Return response which contains name of stored query
      return strResp;
    }

    /// <summary>
    /// Executes script on TU which generates file with diagnostic records.
    /// File is downloaded and stored in system temp directory.
    /// Path to this file is returned.
    /// </summary>
    /// <param name="queryParams">Parameters of the query specified by user</param>
    /// <param name="tuQueryName">Name of the query stored in TU</param>
    /// <returns>Path of the downloaded file with diagnostic records</returns>
    private String DownloadRecords(DRLoadParametersTUDirect queryParams, String tuQueryName)
    {
      WebResponse resp = null;
      Stream respStream = null;
      FileStream tmpFile = null;
      String fileName = "";

      try
      {
        //Prepare query string for the script
        QueryString url = new QueryString();
        url.BeforeUrl = "http://" + queryParams.tuAddress + "/database/query_export.cgi";
        HttpWebRequest queryWebRequest = (HttpWebRequest)System.Net.WebRequest.Create(url.All);
        // Set the 'Method' property of the 'Webrequest' to 'POST'.
        queryWebRequest.Method = "POST";
        //Write query name to request
        ASCIIEncoding encoding = new ASCIIEncoding();
        byte[] postData = encoding.GetBytes("query="+tuQueryName);
        //Set the content type of the data being posted.
        queryWebRequest.ContentType = "application/x-www-form-urlencoded";
        //Set the content length of the data being posted.
        queryWebRequest.ContentLength = postData.Length;
        //Write post data to request stream
        Stream reqStream = queryWebRequest.GetRequestStream();
        reqStream.Write(postData, 0, postData.Length);
        //Close request stream
        reqStream.Close();
        //Send request and obtain response
        resp = queryWebRequest.GetResponse();
        //Obtain file name from response or generate new one
        String contentDisp = resp.Headers["Content-Disposition"];
        if (contentDisp != null)
          foreach (String item in contentDisp.Split(new String[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
            if (item.Trim().ToLower().StartsWith("filename="))
              fileName = item.Trim().Substring(9);
        if (fileName == "")
          fileName = (new Guid().ToString()) + ".xgz";
        //Obtain response stream
        respStream = resp.GetResponseStream();
        //Create file with specified name
        fileName = Path.Combine(Path.GetTempPath(), fileName);
        tmpFile = new FileStream(fileName, FileMode.Create, FileAccess.Write);
        byte[] tmpBuff = new byte[0x40000]; //temporary buffer 256k
        int readCount = 0;
        while ((readCount = respStream.Read(tmpBuff, 0, tmpBuff.Length)) > 0)
          tmpFile.Write(tmpBuff, 0, readCount);
      }
      catch (Exception ex)
      { throw ex; }
      finally
      {
        //Close tmp file
        if (tmpFile != null)
        {
          tmpFile.Close();
          tmpFile.Dispose();
        }
        //Close response stream
        if (respStream != null)
        {
          respStream.Close();
          respStream.Dispose();
        }
        //Close response
        if (resp != null)
          resp.Close();
      }
      //Return name of temporary file
      return fileName;
    }

    /// <summary>
    /// Loads known TU Addresses from XML file into combo box
    /// </summary>
    /// <returns></returns>
    private void LoadTUAddresses()
    {
      try
      {
        XmlDocument tuAddrDoc = new XmlDocument();
        //Load XML file with TU addresses
        tuAddrDoc.Load(Path.Combine(Application.StartupPath, Resource.TUAddressesFileName));
        //Clear combo box with TU Addresses
        cbTUAddress.Items.Clear();
        //Obtain list of all Address elements
        XmlNodeList addrNodes = tuAddrDoc.GetElementsByTagName("Address");
        //Fill in addresses into combo box
        foreach (XmlElement adrElem in addrNodes)
          cbTUAddress.Items.Add(adrElem.InnerText);
      }
      catch (Exception ex)
      {
        OnErrorMessage(new Exception("Failed to load list of TU address", ex));
      }
    }

    /// <summary>
    /// Checks if given TU address exists in known addresses
    /// If not, it is added to combo box and stored in XML file
    /// </summary>
    /// <param name="address"></param>
    private void StoreTUAddress(String address)
    {
      try
      {
        //First check if address already exists in combo box
        if (cbTUAddress.FindStringExact(address) > -1)
          return; //address already exists
        //Try to add the address to XML file
        XmlDocument tuAddrDoc = new XmlDocument();
        //Load XML file with TU addresses
        tuAddrDoc.Load(Path.Combine(Application.StartupPath, Resource.TUAddressesFileName));
        //Check if address does not exist in the document
        String expr = String.Format("./Address['{0}']", address);
        if (tuAddrDoc.SelectSingleNode(expr) == null)
        { //Element with specified address does not exist in the document
          //Add new element to the doument
          XmlElement addrElem = tuAddrDoc.CreateElement("Address");
          addrElem.InnerText = address;
          //Append element
          tuAddrDoc.DocumentElement.AppendChild(addrElem);
          //Save the document back to XML file
          tuAddrDoc.Save(Path.Combine(Application.StartupPath, Resource.TUAddressesFileName));
        }
        //Add address to combo box
        cbTUAddress.Items.Add(address);
      }
      catch (Exception ex)
      {
        OnErrorMessage(new Exception("Failed to store TU address", ex));
      }
    }
    #endregion //Helper methods
  }

  #region Load parameters class
  /// <summary>
  /// Class representing parameters for DR Load operation.
  /// </summary>
  public class DRLoadParametersTUDirect : DRLoadParameters
  {
    public String tuAddress; //Address of Telediagnostic Unit
    public String timeFrom; //start of the interval
    public String timeTo; //end of the interval    
    public String queryScope; //scope of query (severity or all events)
    public String faultCode; //required fault code
    public String recSource; //required source of records
    public String maxRecords; //maximum number of records to select
    public String maxRecTypes;  //maximum number of record types to select
  }
  #endregion //Load parameters class
}

