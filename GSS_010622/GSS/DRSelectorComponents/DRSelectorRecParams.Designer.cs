namespace DRSelectorComponents
{
  partial class DRSelectorRecParams
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btSelect = new System.Windows.Forms.Button();
      this.panel2 = new System.Windows.Forms.Panel();
      this.dtInterval = new DRSelectorComponents.TimePicker();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.gbRecType = new System.Windows.Forms.GroupBox();
      this.chbSnapRec = new System.Windows.Forms.CheckBox();
      this.chbTraceRec = new System.Windows.Forms.CheckBox();
      this.chbEventRec = new System.Windows.Forms.CheckBox();
      this.label1 = new System.Windows.Forms.Label();
      this.tbRecSources = new System.Windows.Forms.TextBox();
      this.tbTitleKeyWords = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.tbFaultCodes = new System.Windows.Forms.MaskedTextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.chbMatchAllKeywords = new System.Windows.Forms.CheckBox();
      this.hierarchyTUInstances = new DRSelectorComponents.FleetHierarchy_TUInstances();
      ((System.ComponentModel.ISupportInitialize)(this.diagRecords)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.recordsView)).BeginInit();
      this.panel2.SuspendLayout();
      this.gbRecType.SuspendLayout();
      this.SuspendLayout();
      // 
      // btSelect
      // 
      this.btSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btSelect.Location = new System.Drawing.Point(467, 16);
      this.btSelect.Name = "btSelect";
      this.btSelect.Size = new System.Drawing.Size(75, 23);
      this.btSelect.TabIndex = 0;
      this.btSelect.Text = "Select";
      this.btSelect.UseVisualStyleBackColor = true;
      this.btSelect.Click += new System.EventHandler(this.btSelect_Click);
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.dtInterval);
      this.panel2.Controls.Add(this.btSelect);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel2.Location = new System.Drawing.Point(0, 305);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(549, 47);
      this.panel2.TabIndex = 9;
      // 
      // dtInterval
      // 
      this.dtInterval.FromSelected = true;
      this.dtInterval.Location = new System.Drawing.Point(3, -1);
      this.dtInterval.Name = "dtInterval";
      this.dtInterval.PredefIntervalIndex = 0;
      this.dtInterval.Size = new System.Drawing.Size(452, 45);
      this.dtInterval.TabIndex = 8;
      this.dtInterval.TimeFrom = new System.DateTime(2007, 10, 4, 13, 20, 28, 248);
      this.dtInterval.TimeTo = new System.DateTime(2007, 10, 4, 13, 20, 28, 248);
      this.dtInterval.ToSelected = true;
      // 
      // splitter1
      // 
      this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter1.Location = new System.Drawing.Point(0, 302);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(549, 3);
      this.splitter1.TabIndex = 10;
      this.splitter1.TabStop = false;
      // 
      // gbRecType
      // 
      this.gbRecType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.gbRecType.Controls.Add(this.chbSnapRec);
      this.gbRecType.Controls.Add(this.chbTraceRec);
      this.gbRecType.Controls.Add(this.chbEventRec);
      this.gbRecType.Location = new System.Drawing.Point(460, 211);
      this.gbRecType.Name = "gbRecType";
      this.gbRecType.Size = new System.Drawing.Size(82, 88);
      this.gbRecType.TabIndex = 11;
      this.gbRecType.TabStop = false;
      this.gbRecType.Text = "Record type";
      // 
      // chbSnapRec
      // 
      this.chbSnapRec.AutoSize = true;
      this.chbSnapRec.Location = new System.Drawing.Point(6, 65);
      this.chbSnapRec.Name = "chbSnapRec";
      this.chbSnapRec.Size = new System.Drawing.Size(51, 17);
      this.chbSnapRec.TabIndex = 2;
      this.chbSnapRec.Text = "Snap";
      this.chbSnapRec.UseVisualStyleBackColor = true;
      // 
      // chbTraceRec
      // 
      this.chbTraceRec.AutoSize = true;
      this.chbTraceRec.Location = new System.Drawing.Point(6, 42);
      this.chbTraceRec.Name = "chbTraceRec";
      this.chbTraceRec.Size = new System.Drawing.Size(54, 17);
      this.chbTraceRec.TabIndex = 1;
      this.chbTraceRec.Text = "Trace";
      this.chbTraceRec.UseVisualStyleBackColor = true;
      // 
      // chbEventRec
      // 
      this.chbEventRec.AutoSize = true;
      this.chbEventRec.Checked = true;
      this.chbEventRec.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chbEventRec.Location = new System.Drawing.Point(6, 19);
      this.chbEventRec.Name = "chbEventRec";
      this.chbEventRec.Size = new System.Drawing.Size(54, 17);
      this.chbEventRec.TabIndex = 0;
      this.chbEventRec.Text = "Event";
      this.chbEventRec.UseVisualStyleBackColor = true;
      // 
      // label1
      // 
      this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(3, 211);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(80, 13);
      this.label1.TabIndex = 12;
      this.label1.Text = "Record source:";
      // 
      // tbRecSources
      // 
      this.tbRecSources.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbRecSources.Location = new System.Drawing.Point(6, 227);
      this.tbRecSources.Name = "tbRecSources";
      this.tbRecSources.Size = new System.Drawing.Size(215, 20);
      this.tbRecSources.TabIndex = 13;
      // 
      // tbTitleKeyWords
      // 
      this.tbTitleKeyWords.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbTitleKeyWords.Location = new System.Drawing.Point(6, 266);
      this.tbTitleKeyWords.Name = "tbTitleKeyWords";
      this.tbTitleKeyWords.Size = new System.Drawing.Size(373, 20);
      this.tbTitleKeyWords.TabIndex = 14;
      // 
      // label2
      // 
      this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(236, 211);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(60, 13);
      this.label2.TabIndex = 15;
      this.label2.Text = "Fault code:";
      // 
      // tbFaultCodes
      // 
      this.tbFaultCodes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbFaultCodes.Location = new System.Drawing.Point(239, 227);
      this.tbFaultCodes.Mask = "999999999999999999999999999999999999999999999999999999999999999999999999999999999" +
          "9999999999999999999";
      this.tbFaultCodes.Name = "tbFaultCodes";
      this.tbFaultCodes.Size = new System.Drawing.Size(215, 20);
      this.tbFaultCodes.TabIndex = 16;
      // 
      // label3
      // 
      this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(3, 250);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(112, 13);
      this.label3.TabIndex = 17;
      this.label3.Text = "Record title keywords:";
      // 
      // chbMatchAllKeywords
      // 
      this.chbMatchAllKeywords.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.chbMatchAllKeywords.AutoSize = true;
      this.chbMatchAllKeywords.Location = new System.Drawing.Point(385, 268);
      this.chbMatchAllKeywords.Name = "chbMatchAllKeywords";
      this.chbMatchAllKeywords.Size = new System.Drawing.Size(69, 17);
      this.chbMatchAllKeywords.TabIndex = 18;
      this.chbMatchAllKeywords.Text = "Match all";
      this.chbMatchAllKeywords.UseVisualStyleBackColor = true;
      // 
      // hierarchyTUInstances
      // 
      this.hierarchyTUInstances.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.hierarchyTUInstances.Location = new System.Drawing.Point(0, 0);
      this.hierarchyTUInstances.MinimumSize = new System.Drawing.Size(195, 90);
      this.hierarchyTUInstances.Name = "hierarchyTUInstances";
      this.hierarchyTUInstances.Size = new System.Drawing.Size(549, 208);
      this.hierarchyTUInstances.TabIndex = 19;
      // 
      // DRSelectorRecParams
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.Controls.Add(this.hierarchyTUInstances);
      this.Controls.Add(this.chbMatchAllKeywords);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.tbFaultCodes);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.tbTitleKeyWords);
      this.Controls.Add(this.tbRecSources);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.gbRecType);
      this.Controls.Add(this.splitter1);
      this.Controls.Add(this.panel2);
      this.Name = "DRSelectorRecParams";
      this.Size = new System.Drawing.Size(549, 352);
      this.Load += new System.EventHandler(this.DRSelectorRecParams_Load);
      ((System.ComponentModel.ISupportInitialize)(this.diagRecords)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.recordsView)).EndInit();
      this.panel2.ResumeLayout(false);
      this.gbRecType.ResumeLayout(false);
      this.gbRecType.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btSelect;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Splitter splitter1;
    private TimePicker dtInterval;
    private System.Windows.Forms.GroupBox gbRecType;
    private System.Windows.Forms.CheckBox chbEventRec;
    private System.Windows.Forms.CheckBox chbSnapRec;
    private System.Windows.Forms.CheckBox chbTraceRec;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox tbRecSources;
    private System.Windows.Forms.TextBox tbTitleKeyWords;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.MaskedTextBox tbFaultCodes;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.CheckBox chbMatchAllKeywords;
    private FleetHierarchy_TUInstances hierarchyTUInstances;
  }
}
