namespace DRSelectorComponents
{
  partial class TimePicker
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.cbIntervals = new System.Windows.Forms.ComboBox();
      this.dtpFrom = new System.Windows.Forms.DateTimePicker();
      this.dtpTo = new System.Windows.Forms.DateTimePicker();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // cbIntervals
      // 
      this.cbIntervals.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbIntervals.FormattingEnabled = true;
      this.cbIntervals.Items.AddRange(new object[] {
            "Custom...",
            "Last Hour",
            "Last Day",
            "Last 2 Days",
            "Last 3 Days",
            "Last 4 Days",
            "Last Week",
            "Last 2 Weeks",
            "Last Month",
            "Last 2 Months",
            "Last 4 Months",
            "Last 6 Months"});
      this.cbIntervals.Location = new System.Drawing.Point(4, 19);
      this.cbIntervals.Name = "cbIntervals";
      this.cbIntervals.Size = new System.Drawing.Size(121, 21);
      this.cbIntervals.TabIndex = 0;
      this.cbIntervals.SelectedIndexChanged += new System.EventHandler(this.cbIntervals_SelectedIndexChanged);
      // 
      // dtpFrom
      // 
      this.dtpFrom.CustomFormat = " yyyy-MM-dd  HH:mm:ss";
      this.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.dtpFrom.Location = new System.Drawing.Point(140, 19);
      this.dtpFrom.MinDate = new System.DateTime(2006, 1, 1, 0, 0, 0, 0);
      this.dtpFrom.Name = "dtpFrom";
      this.dtpFrom.ShowCheckBox = true;
      this.dtpFrom.Size = new System.Drawing.Size(160, 20);
      this.dtpFrom.TabIndex = 6;
      // 
      // dtpTo
      // 
      this.dtpTo.CustomFormat = " yyyy-MM-dd  HH:mm:ss";
      this.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.dtpTo.Location = new System.Drawing.Point(302, 19);
      this.dtpTo.MinDate = new System.DateTime(2006, 1, 1, 0, 0, 0, 0);
      this.dtpTo.Name = "dtpTo";
      this.dtpTo.ShowCheckBox = true;
      this.dtpTo.Size = new System.Drawing.Size(160, 20);
      this.dtpTo.TabIndex = 7;
      this.dtpTo.ValueChanged += new System.EventHandler(this.dtpTo_ValueChanged);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(3, 3);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(100, 13);
      this.label1.TabIndex = 8;
      this.label1.Text = "Predefined intervals";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(136, 3);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(56, 13);
      this.label2.TabIndex = 9;
      this.label2.Text = "Time from:";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(298, 3);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(45, 13);
      this.label3.TabIndex = 10;
      this.label3.Text = "Time to:";
      // 
      // TimePicker
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.dtpTo);
      this.Controls.Add(this.dtpFrom);
      this.Controls.Add(this.cbIntervals);
      this.Name = "TimePicker";
      this.Size = new System.Drawing.Size(460, 45);
      this.Load += new System.EventHandler(this.TimePicker_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ComboBox cbIntervals;
    private System.Windows.Forms.DateTimePicker dtpFrom;
    private System.Windows.Forms.DateTimePicker dtpTo;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
  }
}
