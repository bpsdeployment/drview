namespace DRSelectorComponents
{
  partial class DRSelectorMDS
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.dlgOpenFile = new System.Windows.Forms.OpenFileDialog();
      this.cbTUAddress = new System.Windows.Forms.ComboBox();
      this.dtInterval = new DRSelectorComponents.TimePicker();
      this.btSelectRecords = new System.Windows.Forms.Button();
      this.chbSelectTraces = new System.Windows.Forms.CheckBox();
      this.chbSelectEvents = new System.Windows.Forms.CheckBox();
      this.nudMDSTimeOffset = new System.Windows.Forms.NumericUpDown();
      this.label2 = new System.Windows.Forms.Label();
      this.gbEvents = new System.Windows.Forms.GroupBox();
      this.cbEventName = new System.Windows.Forms.ComboBox();
      this.label9 = new System.Windows.Forms.Label();
      this.tbDeviceCode = new System.Windows.Forms.TextBox();
      this.label7 = new System.Windows.Forms.Label();
      this.nudVehicleNumber = new System.Windows.Forms.NumericUpDown();
      this.label5 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.chbSevC = new System.Windows.Forms.CheckBox();
      this.chbSevB = new System.Windows.Forms.CheckBox();
      this.chbSevB1 = new System.Windows.Forms.CheckBox();
      this.chbSevA = new System.Windows.Forms.CheckBox();
      this.chbSevA1 = new System.Windows.Forms.CheckBox();
      this.label3 = new System.Windows.Forms.Label();
      this.nudMaxEvents = new System.Windows.Forms.NumericUpDown();
      this.gbTrace = new System.Windows.Forms.GroupBox();
      this.cbTraceName = new System.Windows.Forms.ComboBox();
      this.label8 = new System.Windows.Forms.Label();
      this.label12 = new System.Windows.Forms.Label();
      this.nudMaxTraces = new System.Windows.Forms.NumericUpDown();
      this.gbSnap = new System.Windows.Forms.GroupBox();
      this.cbSnapName = new System.Windows.Forms.ComboBox();
      this.label10 = new System.Windows.Forms.Label();
      this.chbSelectSnaps = new System.Windows.Forms.CheckBox();
      this.gbMDSUnit = new System.Windows.Forms.GroupBox();
      this.btGetRecNames = new System.Windows.Forms.Button();
      this.label11 = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.diagRecords)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.recordsView)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMDSTimeOffset)).BeginInit();
      this.gbEvents.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudVehicleNumber)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMaxEvents)).BeginInit();
      this.gbTrace.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudMaxTraces)).BeginInit();
      this.gbSnap.SuspendLayout();
      this.gbMDSUnit.SuspendLayout();
      this.SuspendLayout();
      // 
      // cbTUAddress
      // 
      this.cbTUAddress.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
      this.cbTUAddress.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
      this.cbTUAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.cbTUAddress.FormattingEnabled = true;
      this.cbTUAddress.Location = new System.Drawing.Point(6, 32);
      this.cbTUAddress.Name = "cbTUAddress";
      this.cbTUAddress.Size = new System.Drawing.Size(254, 21);
      this.cbTUAddress.Sorted = true;
      this.cbTUAddress.TabIndex = 0;
      this.cbTUAddress.TextChanged += new System.EventHandler(this.cbTUAddress_TextChanged);
      // 
      // dtInterval
      // 
      this.dtInterval.FromSelected = true;
      this.dtInterval.Location = new System.Drawing.Point(-1, 249);
      this.dtInterval.Name = "dtInterval";
      this.dtInterval.PredefIntervalIndex = 0;
      this.dtInterval.Size = new System.Drawing.Size(458, 45);
      this.dtInterval.TabIndex = 6;
      this.dtInterval.TimeFrom = new System.DateTime(2009, 6, 19, 16, 1, 11, 957);
      this.dtInterval.TimeTo = new System.DateTime(2009, 6, 19, 16, 1, 11, 957);
      this.dtInterval.ToSelected = true;
      // 
      // btSelectRecords
      // 
      this.btSelectRecords.Enabled = false;
      this.btSelectRecords.Location = new System.Drawing.Point(463, 267);
      this.btSelectRecords.Name = "btSelectRecords";
      this.btSelectRecords.Size = new System.Drawing.Size(75, 23);
      this.btSelectRecords.TabIndex = 16;
      this.btSelectRecords.Text = "Select";
      this.btSelectRecords.UseVisualStyleBackColor = true;
      this.btSelectRecords.Click += new System.EventHandler(this.btSelectRecords_Click);
      // 
      // chbSelectTraces
      // 
      this.chbSelectTraces.AutoSize = true;
      this.chbSelectTraces.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.chbSelectTraces.Location = new System.Drawing.Point(204, 0);
      this.chbSelectTraces.Name = "chbSelectTraces";
      this.chbSelectTraces.Size = new System.Drawing.Size(56, 17);
      this.chbSelectTraces.TabIndex = 1;
      this.chbSelectTraces.Text = "Select";
      this.chbSelectTraces.UseVisualStyleBackColor = true;
      // 
      // chbSelectEvents
      // 
      this.chbSelectEvents.AutoSize = true;
      this.chbSelectEvents.Checked = true;
      this.chbSelectEvents.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chbSelectEvents.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.chbSelectEvents.Location = new System.Drawing.Point(204, 0);
      this.chbSelectEvents.Name = "chbSelectEvents";
      this.chbSelectEvents.Size = new System.Drawing.Size(56, 17);
      this.chbSelectEvents.TabIndex = 0;
      this.chbSelectEvents.Text = "Select";
      this.chbSelectEvents.UseVisualStyleBackColor = true;
      // 
      // nudMDSTimeOffset
      // 
      this.nudMDSTimeOffset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.nudMDSTimeOffset.Location = new System.Drawing.Point(412, 33);
      this.nudMDSTimeOffset.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
      this.nudMDSTimeOffset.Minimum = new decimal(new int[] {
            12,
            0,
            0,
            -2147483648});
      this.nudMDSTimeOffset.Name = "nudMDSTimeOffset";
      this.nudMDSTimeOffset.Size = new System.Drawing.Size(118, 20);
      this.nudMDSTimeOffset.TabIndex = 18;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label2.Location = new System.Drawing.Point(409, 16);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(121, 13);
      this.label2.TabIndex = 19;
      this.label2.Text = "Time offset against UTC";
      // 
      // gbEvents
      // 
      this.gbEvents.Controls.Add(this.cbEventName);
      this.gbEvents.Controls.Add(this.label9);
      this.gbEvents.Controls.Add(this.tbDeviceCode);
      this.gbEvents.Controls.Add(this.label7);
      this.gbEvents.Controls.Add(this.nudVehicleNumber);
      this.gbEvents.Controls.Add(this.label5);
      this.gbEvents.Controls.Add(this.label6);
      this.gbEvents.Controls.Add(this.chbSevC);
      this.gbEvents.Controls.Add(this.chbSevB);
      this.gbEvents.Controls.Add(this.chbSevB1);
      this.gbEvents.Controls.Add(this.chbSevA);
      this.gbEvents.Controls.Add(this.chbSevA1);
      this.gbEvents.Controls.Add(this.label3);
      this.gbEvents.Controls.Add(this.nudMaxEvents);
      this.gbEvents.Controls.Add(this.chbSelectEvents);
      this.gbEvents.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.gbEvents.Location = new System.Drawing.Point(2, 66);
      this.gbEvents.Name = "gbEvents";
      this.gbEvents.Size = new System.Drawing.Size(266, 177);
      this.gbEvents.TabIndex = 20;
      this.gbEvents.TabStop = false;
      this.gbEvents.Text = "Events";
      // 
      // cbEventName
      // 
      this.cbEventName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.cbEventName.FormattingEnabled = true;
      this.cbEventName.Location = new System.Drawing.Point(6, 108);
      this.cbEventName.Name = "cbEventName";
      this.cbEventName.Size = new System.Drawing.Size(254, 21);
      this.cbEventName.Sorted = true;
      this.cbEventName.TabIndex = 45;
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label9.Location = new System.Drawing.Point(3, 92);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(38, 13);
      this.label9.TabIndex = 44;
      this.label9.Text = "Name:";
      // 
      // tbDeviceCode
      // 
      this.tbDeviceCode.Location = new System.Drawing.Point(6, 69);
      this.tbDeviceCode.Name = "tbDeviceCode";
      this.tbDeviceCode.Size = new System.Drawing.Size(116, 20);
      this.tbDeviceCode.TabIndex = 43;
      this.tbDeviceCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericTextBox_KeyPress);
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label7.Location = new System.Drawing.Point(3, 53);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(71, 13);
      this.label7.TabIndex = 42;
      this.label7.Text = "Device code:";
      // 
      // nudVehicleNumber
      // 
      this.nudVehicleNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.nudVehicleNumber.Location = new System.Drawing.Point(6, 151);
      this.nudVehicleNumber.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
      this.nudVehicleNumber.Name = "nudVehicleNumber";
      this.nudVehicleNumber.Size = new System.Drawing.Size(116, 20);
      this.nudVehicleNumber.TabIndex = 39;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label5.Location = new System.Drawing.Point(3, 135);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(83, 13);
      this.label5.TabIndex = 38;
      this.label5.Text = "Vehicle number:";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label6.Location = new System.Drawing.Point(3, 17);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(48, 13);
      this.label6.TabIndex = 36;
      this.label6.Text = "Severity:";
      // 
      // chbSevC
      // 
      this.chbSevC.AutoSize = true;
      this.chbSevC.Checked = true;
      this.chbSevC.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chbSevC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.chbSevC.Location = new System.Drawing.Point(147, 33);
      this.chbSevC.Name = "chbSevC";
      this.chbSevC.Size = new System.Drawing.Size(33, 17);
      this.chbSevC.TabIndex = 35;
      this.chbSevC.Text = "C";
      this.chbSevC.UseVisualStyleBackColor = true;
      // 
      // chbSevB
      // 
      this.chbSevB.AutoSize = true;
      this.chbSevB.Checked = true;
      this.chbSevB.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chbSevB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.chbSevB.Location = new System.Drawing.Point(116, 33);
      this.chbSevB.Name = "chbSevB";
      this.chbSevB.Size = new System.Drawing.Size(33, 17);
      this.chbSevB.TabIndex = 34;
      this.chbSevB.Text = "B";
      this.chbSevB.UseVisualStyleBackColor = true;
      // 
      // chbSevB1
      // 
      this.chbSevB1.AutoSize = true;
      this.chbSevB1.Checked = true;
      this.chbSevB1.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chbSevB1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.chbSevB1.Location = new System.Drawing.Point(79, 33);
      this.chbSevB1.Name = "chbSevB1";
      this.chbSevB1.Size = new System.Drawing.Size(39, 17);
      this.chbSevB1.TabIndex = 33;
      this.chbSevB1.Text = "B1";
      this.chbSevB1.UseVisualStyleBackColor = true;
      // 
      // chbSevA
      // 
      this.chbSevA.AutoSize = true;
      this.chbSevA.Checked = true;
      this.chbSevA.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chbSevA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.chbSevA.Location = new System.Drawing.Point(45, 33);
      this.chbSevA.Name = "chbSevA";
      this.chbSevA.Size = new System.Drawing.Size(33, 17);
      this.chbSevA.TabIndex = 32;
      this.chbSevA.Text = "A";
      this.chbSevA.UseVisualStyleBackColor = true;
      // 
      // chbSevA1
      // 
      this.chbSevA1.AutoSize = true;
      this.chbSevA1.Checked = true;
      this.chbSevA1.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chbSevA1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.chbSevA1.Location = new System.Drawing.Point(8, 33);
      this.chbSevA1.Name = "chbSevA1";
      this.chbSevA1.Size = new System.Drawing.Size(39, 17);
      this.chbSevA1.TabIndex = 31;
      this.chbSevA1.Text = "A1";
      this.chbSevA1.UseVisualStyleBackColor = true;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label3.Location = new System.Drawing.Point(148, 135);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(89, 13);
      this.label3.TabIndex = 21;
      this.label3.Text = "Maximum records";
      // 
      // nudMaxEvents
      // 
      this.nudMaxEvents.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.nudMaxEvents.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
      this.nudMaxEvents.Location = new System.Drawing.Point(151, 151);
      this.nudMaxEvents.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
      this.nudMaxEvents.Name = "nudMaxEvents";
      this.nudMaxEvents.Size = new System.Drawing.Size(109, 20);
      this.nudMaxEvents.TabIndex = 1;
      this.nudMaxEvents.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      // 
      // gbTrace
      // 
      this.gbTrace.Controls.Add(this.cbTraceName);
      this.gbTrace.Controls.Add(this.label8);
      this.gbTrace.Controls.Add(this.chbSelectTraces);
      this.gbTrace.Controls.Add(this.label12);
      this.gbTrace.Controls.Add(this.nudMaxTraces);
      this.gbTrace.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.gbTrace.Location = new System.Drawing.Point(274, 66);
      this.gbTrace.Name = "gbTrace";
      this.gbTrace.Size = new System.Drawing.Size(266, 98);
      this.gbTrace.TabIndex = 21;
      this.gbTrace.TabStop = false;
      this.gbTrace.Text = "Trace";
      // 
      // cbTraceName
      // 
      this.cbTraceName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.cbTraceName.FormattingEnabled = true;
      this.cbTraceName.Location = new System.Drawing.Point(6, 31);
      this.cbTraceName.Name = "cbTraceName";
      this.cbTraceName.Size = new System.Drawing.Size(254, 21);
      this.cbTraceName.Sorted = true;
      this.cbTraceName.TabIndex = 43;
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label8.Location = new System.Drawing.Point(6, 17);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(38, 13);
      this.label8.TabIndex = 42;
      this.label8.Text = "Name:";
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label12.Location = new System.Drawing.Point(6, 53);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(89, 13);
      this.label12.TabIndex = 21;
      this.label12.Text = "Maximum records";
      // 
      // nudMaxTraces
      // 
      this.nudMaxTraces.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.nudMaxTraces.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
      this.nudMaxTraces.Location = new System.Drawing.Point(6, 69);
      this.nudMaxTraces.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
      this.nudMaxTraces.Name = "nudMaxTraces";
      this.nudMaxTraces.Size = new System.Drawing.Size(109, 20);
      this.nudMaxTraces.TabIndex = 1;
      this.nudMaxTraces.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      // 
      // gbSnap
      // 
      this.gbSnap.Controls.Add(this.cbSnapName);
      this.gbSnap.Controls.Add(this.label10);
      this.gbSnap.Controls.Add(this.chbSelectSnaps);
      this.gbSnap.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.gbSnap.Location = new System.Drawing.Point(272, 183);
      this.gbSnap.Name = "gbSnap";
      this.gbSnap.Size = new System.Drawing.Size(266, 60);
      this.gbSnap.TabIndex = 44;
      this.gbSnap.TabStop = false;
      this.gbSnap.Text = "Snap";
      // 
      // cbSnapName
      // 
      this.cbSnapName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.cbSnapName.FormattingEnabled = true;
      this.cbSnapName.Location = new System.Drawing.Point(6, 32);
      this.cbSnapName.Name = "cbSnapName";
      this.cbSnapName.Size = new System.Drawing.Size(254, 21);
      this.cbSnapName.Sorted = true;
      this.cbSnapName.TabIndex = 43;
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label10.Location = new System.Drawing.Point(6, 16);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(38, 13);
      this.label10.TabIndex = 42;
      this.label10.Text = "Name:";
      // 
      // chbSelectSnaps
      // 
      this.chbSelectSnaps.AutoSize = true;
      this.chbSelectSnaps.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.chbSelectSnaps.Location = new System.Drawing.Point(204, 0);
      this.chbSelectSnaps.Name = "chbSelectSnaps";
      this.chbSelectSnaps.Size = new System.Drawing.Size(56, 17);
      this.chbSelectSnaps.TabIndex = 1;
      this.chbSelectSnaps.Text = "Select";
      this.chbSelectSnaps.UseVisualStyleBackColor = true;
      // 
      // gbMDSUnit
      // 
      this.gbMDSUnit.Controls.Add(this.btGetRecNames);
      this.gbMDSUnit.Controls.Add(this.label11);
      this.gbMDSUnit.Controls.Add(this.cbTUAddress);
      this.gbMDSUnit.Controls.Add(this.label2);
      this.gbMDSUnit.Controls.Add(this.nudMDSTimeOffset);
      this.gbMDSUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.gbMDSUnit.Location = new System.Drawing.Point(3, 3);
      this.gbMDSUnit.Name = "gbMDSUnit";
      this.gbMDSUnit.Size = new System.Drawing.Size(538, 59);
      this.gbMDSUnit.TabIndex = 45;
      this.gbMDSUnit.TabStop = false;
      this.gbMDSUnit.Text = "MDS Unit";
      // 
      // btGetRecNames
      // 
      this.btGetRecNames.Enabled = false;
      this.btGetRecNames.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.btGetRecNames.Location = new System.Drawing.Point(276, 30);
      this.btGetRecNames.Name = "btGetRecNames";
      this.btGetRecNames.Size = new System.Drawing.Size(120, 23);
      this.btGetRecNames.TabIndex = 44;
      this.btGetRecNames.Text = "Get record names";
      this.btGetRecNames.UseVisualStyleBackColor = true;
      this.btGetRecNames.Click += new System.EventHandler(this.btGetRecNames_Click);
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.label11.Location = new System.Drawing.Point(6, 16);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(48, 13);
      this.label11.TabIndex = 43;
      this.label11.Text = "Address:";
      // 
      // DRSelectorMDS
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.Controls.Add(this.gbMDSUnit);
      this.Controls.Add(this.gbSnap);
      this.Controls.Add(this.gbTrace);
      this.Controls.Add(this.gbEvents);
      this.Controls.Add(this.btSelectRecords);
      this.Controls.Add(this.dtInterval);
      this.MinimumSize = new System.Drawing.Size(542, 295);
      this.Name = "DRSelectorMDS";
      this.Size = new System.Drawing.Size(542, 295);
      this.Load += new System.EventHandler(this.DRSelectorMDS_Load);
      ((System.ComponentModel.ISupportInitialize)(this.diagRecords)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.recordsView)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMDSTimeOffset)).EndInit();
      this.gbEvents.ResumeLayout(false);
      this.gbEvents.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudVehicleNumber)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMaxEvents)).EndInit();
      this.gbTrace.ResumeLayout(false);
      this.gbTrace.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudMaxTraces)).EndInit();
      this.gbSnap.ResumeLayout(false);
      this.gbSnap.PerformLayout();
      this.gbMDSUnit.ResumeLayout(false);
      this.gbMDSUnit.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.OpenFileDialog dlgOpenFile;
    private System.Windows.Forms.ComboBox cbTUAddress;
    private TimePicker dtInterval;
    private System.Windows.Forms.Button btSelectRecords;
    private System.Windows.Forms.CheckBox chbSelectTraces;
    private System.Windows.Forms.CheckBox chbSelectEvents;
    private System.Windows.Forms.NumericUpDown nudMDSTimeOffset;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.GroupBox gbEvents;
    private System.Windows.Forms.NumericUpDown nudMaxEvents;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.CheckBox chbSevC;
    private System.Windows.Forms.CheckBox chbSevB;
    private System.Windows.Forms.CheckBox chbSevB1;
    private System.Windows.Forms.CheckBox chbSevA;
    private System.Windows.Forms.CheckBox chbSevA1;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.NumericUpDown nudVehicleNumber;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.TextBox tbDeviceCode;
    private System.Windows.Forms.GroupBox gbTrace;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.NumericUpDown nudMaxTraces;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.ComboBox cbEventName;
    private System.Windows.Forms.ComboBox cbTraceName;
    private System.Windows.Forms.GroupBox gbSnap;
    private System.Windows.Forms.ComboBox cbSnapName;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.CheckBox chbSelectSnaps;
    private System.Windows.Forms.GroupBox gbMDSUnit;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Button btGetRecNames;
  }
}
