using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml;
using System.Threading;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using DSTU_RecType;
using DRQueries;
using DDDObjects;

namespace DRSelectorComponents
{
  /// <summary>
  /// Component enabling to select diagnostic records based on TUInstance and time
  /// </summary>
  public partial class DRSelectorDB : DRSelectorComponents.DRSelectorBase
  {
    #region Static members
    private static String NumTbTemplate = "0123456789, ";
    #endregion 

    #region Construction and initialization
    public DRSelectorDB() 
      : base() //Call base constructor
    {
      InitializeComponent();
      InitMembers();
    }

    public DRSelectorDB(Dictionary<String, Object> selParams)
      : base(selParams) //Call base constructor
    {
      InitializeComponent();
      InitMembers();
    }

    private void InitMembers()
    {
      cbSortColumn.SelectedIndex = 0;
      cbSortDirection.SelectedIndex = 1;
    }
    #endregion //Construction and initialization

    #region Component event handlers
    private void btRefresh_Click(object sender, EventArgs e)
    {
      //Reload the list of TU Instances
      RefreshHierarchyAndInstances();
    }

    private void btSelect_Click(object sender, EventArgs e)
    {
      //Start asynchronous record load
      LoadRecordsAsync();
    }

    private void DRSelectorDB_Load(object sender, EventArgs e)
    {
      //Load TU Instances into grid
      RefreshHierarchyAndInstances();
    }

    /// <summary>
    /// Check whether pressed key represents allowed character.
    /// If not, event is discarded.
    /// Used for text boxes which accept only characters [0-9, ] 
    /// </summary>
    private void NumericTextBox_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (NumTbTemplate.IndexOf(e.KeyChar) == -1 && e.KeyChar != '\b')
        e.Handled = true; //Character is not in template string -> it is invalid
    }
    #endregion //Component event handlers

    #region Helper functions
    /// <summary>
    /// Selects list of all TU Instances from DB and fills the grid
    /// </summary>
    protected void RefreshHierarchyAndInstances()
    {
      try
      {
        //Set proper connection string to DDD Helper
        if (dbConnection != null)
          dddHelper.ConnectionString = dbConnection.ConnectionString;

        hierarchyTUInstances.FleetHierarchy.ShowCurrentHierarchy(dddHelper.HierarchyHelper, null);
        hierarchyTUInstances.TUInstancesList.ShowTUInstances(dddHelper);
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRSelectorException(ex, "Failed to load fleet hierarchy or TU Instances list"));
      }
    }
    #endregion //Helper functions

    #region Virtual method overrides
    /// <summary>
    /// Method reads values from internal components and prepares DRLoadParametersRecParams object
    /// </summary>
    /// <returns>Object containing all necessary parameters for DR selection</returns>
    protected override DRLoadParameters PrepareLoadParameters()
    {
      //Build string list of selected hierarchy item IDs
      List<int> selIDs = hierarchyTUInstances.FleetHierarchy.GetSelectedIDs();
      String strSelIDs = String.Empty;
      foreach(int itemID in selIDs)
        strSelIDs += itemID.ToString() + " ";
      //Create mask for record types
      eRecordCategory recTypes = (eRecordCategory)0;
      if (chbEventRec.Checked)
        recTypes |= eRecordCategory.Event;
      if (chbTraceRec.Checked)
        recTypes |= eRecordCategory.Trace;
      if (chbSnapRec.Checked)
        recTypes |= eRecordCategory.Snap;
      //Create mask for severities
      String strSeverities = String.Empty;
      if (chbSevA1.Checked) strSeverities += ((int)(SeverityType.A1)).ToString() + " ";
      if (chbSevA.Checked) strSeverities += ((int)(SeverityType.A)).ToString() + " ";
      if (chbSevB1.Checked) strSeverities += ((int)(SeverityType.B1)).ToString() + " ";
      if (chbSevB.Checked) strSeverities += ((int)(SeverityType.B)).ToString() + " ";
      if (chbSevC.Checked) strSeverities += ((int)(SeverityType.C)).ToString() + " ";
      
      //Create parameters object
      DRLoadParametersDB loadParams = new DRLoadParametersDB();
      //Create query text
      String queryText = DRQueryHelper.GetQuery_RecordsApp(
        hierarchyTUInstances.TUInstancesList.GetSelectedInstanceIDs(),
        strSelIDs,
        (dtInterval.FromSelected) ? (DateTime?)dddHelper.UserTimeToUTC(dtInterval.TimeFrom) : null,
        (dtInterval.ToSelected) ? (DateTime?)dddHelper.UserTimeToUTC(dtInterval.TimeTo) : null,
        tbRecSources.Text, tbVehicleNumbers.Text, tbFaultCodes.Text, tbTrcSnpCodes.Text, 
        tbTitleKeyWords.Text, chbMatchAllKeywords.Checked, 
        (recTypes != 0) ? (eRecordCategory?)recTypes : null, 
        strSeverities, tbFaultSubseverities.Text,
        (int)(nudMaxRecords.Value), cbSortColumn.SelectedText,
        (cbSortDirection.SelectedText == "ASC") ? eSortOrder.Ascending : eSortOrder.Descending);
      //Store query text in params object
      loadParams.dbCmdText = queryText;
      //Set selectors maximum number of records
      this.MaxRecCount = (int)nudMaxRecords.Value;

      //Return params
      return loadParams;
    }

    /// <summary>
    /// Method is invoked asynchronously and performs the load of DR.
    /// </summary>
    /// <param name="loadParams">Object containing all necessary parameters for DR selection</param>
    /// <param name="asyncOp">AsyncOperation used to post callbacks</param>
    protected override void PerformAsyncDRLoad(DRLoadParameters loadParams, AsyncOperation asyncOp)
    {
      try
      {
        //Cast parameters object to proper type
        DRLoadParametersDB parameters = loadParams as DRLoadParametersDB;
        //Check connection to database
        if (!CheckDBConnection())
          throw new DRSelectorException("Connection to database could not be opened");
        
        //Create and execute DB command which selects records
        using (SqlCommand cmdSelectDR = new SqlCommand(parameters.dbCmdText, dbConnection))
        { //Set additional command parameters
          cmdSelectDR.CommandTimeout = sqlCmdTimeout;
          cmdSelectDR.CommandType = CommandType.Text;
          //Execute command and obtain reader
          try
          {
            using (SqlDataReader reader = cmdSelectDR.ExecuteReader())
            {
              //Post info message
              asyncOp.Post(onInfoMessageDelegate, "Loading records to data set");
              FillDataSetFromResultSets(reader, asyncOp);
            }
          }
          catch (SqlException sqlEx)
          {
            throw new DRSelectorException(sqlEx, "Failed to execute DB command");
          }
        }
            
        //Post completed event
        AsyncCompletedEventArgs result = new AsyncCompletedEventArgs(null, false, null);
        asyncOp.PostOperationCompleted(onDRLoadCompletedDelegate, result);
      }
      catch (Exception ex)
      {
        //Post completed event with error
        AsyncCompletedEventArgs result = new AsyncCompletedEventArgs(ex, false, null);
        asyncOp.PostOperationCompleted(onDRLoadCompletedDelegate, result);
      }
    }

    /// <summary>
    /// Saves settings of selector to file
    /// </summary>
    /// <param name="file">Stream to save to</param>
    /// <param name="formatter">Formatter used for saving objects</param>
    public override void SaveSettingsToFile(FileStream file, BinaryFormatter formatter)
    {
      //Serialize
      formatter.Serialize(file, hierarchyTUInstances.TUInstancesList.GetSelectedInstanceIDs());
      formatter.Serialize(file, hierarchyTUInstances.FleetHierarchy.GetSelectedNamesXML());
      formatter.Serialize(file, dtInterval.TimeFrom);
      formatter.Serialize(file, dtInterval.FromSelected);
      formatter.Serialize(file, dtInterval.TimeTo);
      formatter.Serialize(file, dtInterval.ToSelected);
      formatter.Serialize(file, dtInterval.PredefIntervalIndex);
      formatter.Serialize(file, chbEventRec.Checked);
      formatter.Serialize(file, chbSnapRec.Checked);
      formatter.Serialize(file, chbTraceRec.Checked);

      formatter.Serialize(file, tbTitleKeyWords.Text);
      formatter.Serialize(file, chbMatchAllKeywords.Checked);
      formatter.Serialize(file, tbRecSources.Text);
      formatter.Serialize(file, tbFaultCodes.Text);
      formatter.Serialize(file, tbTrcSnpCodes.Text);
      formatter.Serialize(file, tbVehicleNumbers.Text);
      formatter.Serialize(file, chbSevA1.Checked);
      formatter.Serialize(file, chbSevA.Checked);
      formatter.Serialize(file, chbSevB1.Checked);
      formatter.Serialize(file, chbSevB.Checked);
      formatter.Serialize(file, chbSevC.Checked);
      formatter.Serialize(file, tbFaultSubseverities.Text);

      formatter.Serialize(file, nudMaxRecords.Value);
      formatter.Serialize(file, cbSortColumn.SelectedIndex);
      formatter.Serialize(file, cbSortDirection.SelectedIndex);
    }

    /// <summary>
    /// Loads settings of selector from file
    /// </summary>
    /// <param name="file">Stream to save to</param>
    /// <param name="formatter">Formatter used for saving objects</param>
    public override void LoadSettingsFromFile(FileStream file, BinaryFormatter formatter)
    {
      //Check TU Instances
      hierarchyTUInstances.TUInstancesList.CheckTUInstances((String)formatter.Deserialize(file));
      //Check fleet hierarchy items
      hierarchyTUInstances.FleetHierarchy.CheckHierarchyItems((String)formatter.Deserialize(file));
      //Retrieve rest of parameters
      dtInterval.TimeFrom = (DateTime)formatter.Deserialize(file);
      dtInterval.FromSelected = (bool)formatter.Deserialize(file);
      dtInterval.TimeTo = (DateTime)formatter.Deserialize(file);
      dtInterval.ToSelected = (bool)formatter.Deserialize(file);
      dtInterval.PredefIntervalIndex = (int)formatter.Deserialize(file);
      chbEventRec.Checked = (bool)formatter.Deserialize(file);
      chbSnapRec.Checked = (bool)formatter.Deserialize(file);
      chbTraceRec.Checked = (bool)formatter.Deserialize(file);

      tbTitleKeyWords.Text = (String)formatter.Deserialize(file);
      chbMatchAllKeywords.Checked = (bool)formatter.Deserialize(file);
      tbRecSources.Text = (String)formatter.Deserialize(file);
      tbFaultCodes.Text = (String)formatter.Deserialize(file);
      tbTrcSnpCodes.Text = (String)formatter.Deserialize(file);
      tbVehicleNumbers.Text = (String)formatter.Deserialize(file);
      chbSevA1.Checked = (bool)formatter.Deserialize(file);
      chbSevA.Checked = (bool)formatter.Deserialize(file);
      chbSevB1.Checked = (bool)formatter.Deserialize(file);
      chbSevB.Checked = (bool)formatter.Deserialize(file);
      chbSevC.Checked = (bool)formatter.Deserialize(file);
      tbFaultSubseverities.Text = (String)formatter.Deserialize(file);

      nudMaxRecords.Value = (decimal)formatter.Deserialize(file);
      cbSortColumn.SelectedIndex = (int)formatter.Deserialize(file);
      cbSortDirection.SelectedIndex = (int)formatter.Deserialize(file);
    }
    #endregion //Virtual method overrides
  }

  #region Load parameters class
  /// <summary>
  /// Class representing parameters for DR Load operation.
  /// </summary>
  public class DRLoadParametersDB : DRLoadParameters
  {
    public String dbCmdText; //Text of database command
  }
  #endregion //Load parameters class
}

