using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Data.SqlClient;
using DDDObjects;

namespace DRSelectorComponents
{
  /// <summary>
  /// User control displays all TU Instances in the form of tree,
  /// grouped by TU Types. User may select desired TU Instances using check boxes.
  /// </summary>
  public partial class TUInstancesList : TriStateTreeView
  {
    #region Private members
    #endregion //Private members

    #region Public properties
    #endregion //Public properties

    #region Construction initialization
    public TUInstancesList()
    {
      InitializeComponent();
      //Initialize parameters of TreeView
      this.ShowLines = true;
      this.ShowRootLines = true;
      this.ShowNodeToolTips = true;
    }
    #endregion //Construction initialization

    #region Public methods
    /// <summary>
    /// Displays list of TU Types and instances in the tree.
    /// TU objects are loaded from given DDD helper.
    /// </summary>
    /// <param name="dddHelper">Helper object with dictionary of TU types and instances</param>
    public void ShowTUInstances(DDDHelper dddHelper)
    {
      try
      {
        //Refresh list of TU Instances from DB
        dddHelper.LoadTUObjectsFromDB();
        //Put tree view into update mode
        this.BeginUpdate();
        //Iterate over all TUInstances, create nodes
        foreach (KeyValuePair<Guid, TUInstance> instPair in dddHelper.TUInstances)
        {
          TUInstance tuInst = instPair.Value;
          TUType tuType = tuInst.TUTypeObject;
          int typeNodeIndex;
          TreeNode tuTypeNode;
          //First obtain node of corresponding TUType
          if ((typeNodeIndex = this.Nodes.IndexOfKey(tuType.TypeName)) != -1)
            tuTypeNode = this.Nodes[typeNodeIndex];
          else
          { //Type node has to be created
            tuTypeNode = new TreeNode();
            tuTypeNode.Tag = tuType.TUTypeID;
            tuTypeNode.Name = tuType.TypeName;
            tuTypeNode.Text = tuType.TypeName;
            tuTypeNode.ToolTipText = tuType.Comment;
            tuTypeNode.NodeFont = new Font(this.Font, FontStyle.Bold);
            this.Nodes.Add(tuTypeNode);
          }
          //Now create instance node and add to type node
          TreeNode tuInstanceNode;
          tuInstanceNode = new TreeNode();
          tuInstanceNode.Tag = tuInst.TUInstanceID;
          tuInstanceNode.Name = tuInst.TUName;
          tuInstanceNode.Text = tuInst.TUName;
          tuInstanceNode.ToolTipText = tuInst.Comment;
          tuTypeNode.Nodes.Add(tuInstanceNode);
        }
        //Sort items
        this.Sort();
        //End update mode
        this.EndUpdate();
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to show TU types and instances", ex);
      }
    }

    /// <summary>
    /// Creates list of selected (checked) TU Instance IDs
    /// in the form of blank separated string.
    /// </summary>
    /// <returns>IDs of checked TU Instances in the form of blank separated string or NULL</returns>
    public String GetSelectedInstanceIDs()
    {
      //First create list of selected IDs, converted to String
      List<String> liIDs = new List<string>();
      foreach (TreeNode typeNode in this.Nodes)
        foreach (TreeNode instNode in typeNode.Nodes)
          if (GetChecked(instNode) == CheckState.Checked || GetChecked(typeNode) == CheckState.Checked)
            liIDs.Add(instNode.Tag.ToString());
      //When nothing is selected, return empty string
      if (liIDs.Count == 0)
        return String.Empty;
      //Create blank separated string
      String[] arIDs = liIDs.ToArray();
      return String.Join(" ", arIDs);
    }

    /// <summary>
    /// Checks nodes representing TU Instances according to given
    /// list of TU Instance IDs.
    /// </summary>
    /// <param name="selInstances">List of TU IDs in the form of blank separated string</param>
    public void CheckTUInstances(String selInstances)
    {
      //Check for null or empty string
      if (selInstances == null || selInstances == String.Empty)
        return;
      
      //Iterate over all TU Instance nodes
      foreach (TreeNode typeNode in this.Nodes)
        foreach (TreeNode instNode in typeNode.Nodes)
          if (selInstances.Contains(instNode.Tag.ToString()))
          {
            SetChecked(instNode, CheckState.Checked);
            typeNode.Expand();
          }
    }
    #endregion //Public methods

    #region Helper methods
    #endregion //Helper methods
  }
}
