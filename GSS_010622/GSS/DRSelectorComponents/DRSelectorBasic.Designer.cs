namespace DRSelectorComponents
{
  partial class DRSelectorBasic
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btSelect = new System.Windows.Forms.Button();
      this.panBottom = new System.Windows.Forms.Panel();
      this.dtInterval = new DRSelectorComponents.TimePicker();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.gbRecType = new System.Windows.Forms.GroupBox();
      this.chbSnapRec = new System.Windows.Forms.CheckBox();
      this.chbTraceRec = new System.Windows.Forms.CheckBox();
      this.chbEventRec = new System.Windows.Forms.CheckBox();
      this.hierarchyTUInstances = new DRSelectorComponents.FleetHierarchy_TUInstances();
      ((System.ComponentModel.ISupportInitialize)(this.diagRecords)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.recordsView)).BeginInit();
      this.panBottom.SuspendLayout();
      this.gbRecType.SuspendLayout();
      this.SuspendLayout();
      // 
      // btSelect
      // 
      this.btSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btSelect.Location = new System.Drawing.Point(467, 16);
      this.btSelect.Name = "btSelect";
      this.btSelect.Size = new System.Drawing.Size(75, 23);
      this.btSelect.TabIndex = 0;
      this.btSelect.Text = "Select";
      this.btSelect.UseVisualStyleBackColor = true;
      this.btSelect.Click += new System.EventHandler(this.btSelect_Click);
      // 
      // panBottom
      // 
      this.panBottom.Controls.Add(this.dtInterval);
      this.panBottom.Controls.Add(this.btSelect);
      this.panBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panBottom.Location = new System.Drawing.Point(0, 101);
      this.panBottom.Name = "panBottom";
      this.panBottom.Size = new System.Drawing.Size(548, 47);
      this.panBottom.TabIndex = 9;
      // 
      // dtInterval
      // 
      this.dtInterval.FromSelected = true;
      this.dtInterval.Location = new System.Drawing.Point(3, -1);
      this.dtInterval.Name = "dtInterval";
      this.dtInterval.PredefIntervalIndex = 0;
      this.dtInterval.Size = new System.Drawing.Size(452, 45);
      this.dtInterval.TabIndex = 8;
      this.dtInterval.TimeFrom = new System.DateTime(2007, 9, 17, 18, 26, 50, 485);
      this.dtInterval.TimeTo = new System.DateTime(2007, 9, 17, 18, 26, 50, 485);
      this.dtInterval.ToSelected = true;
      // 
      // splitter1
      // 
      this.splitter1.Cursor = System.Windows.Forms.Cursors.Default;
      this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter1.Enabled = false;
      this.splitter1.Location = new System.Drawing.Point(0, 98);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(548, 3);
      this.splitter1.TabIndex = 10;
      this.splitter1.TabStop = false;
      // 
      // gbRecType
      // 
      this.gbRecType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.gbRecType.Controls.Add(this.chbSnapRec);
      this.gbRecType.Controls.Add(this.chbTraceRec);
      this.gbRecType.Controls.Add(this.chbEventRec);
      this.gbRecType.Location = new System.Drawing.Point(463, 7);
      this.gbRecType.Name = "gbRecType";
      this.gbRecType.Size = new System.Drawing.Size(82, 88);
      this.gbRecType.TabIndex = 11;
      this.gbRecType.TabStop = false;
      this.gbRecType.Text = "Record type";
      // 
      // chbSnapRec
      // 
      this.chbSnapRec.AutoSize = true;
      this.chbSnapRec.Location = new System.Drawing.Point(6, 65);
      this.chbSnapRec.Name = "chbSnapRec";
      this.chbSnapRec.Size = new System.Drawing.Size(51, 17);
      this.chbSnapRec.TabIndex = 2;
      this.chbSnapRec.Text = "Snap";
      this.chbSnapRec.UseVisualStyleBackColor = true;
      // 
      // chbTraceRec
      // 
      this.chbTraceRec.AutoSize = true;
      this.chbTraceRec.Location = new System.Drawing.Point(6, 42);
      this.chbTraceRec.Name = "chbTraceRec";
      this.chbTraceRec.Size = new System.Drawing.Size(54, 17);
      this.chbTraceRec.TabIndex = 1;
      this.chbTraceRec.Text = "Trace";
      this.chbTraceRec.UseVisualStyleBackColor = true;
      // 
      // chbEventRec
      // 
      this.chbEventRec.AutoSize = true;
      this.chbEventRec.Checked = true;
      this.chbEventRec.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chbEventRec.Location = new System.Drawing.Point(6, 19);
      this.chbEventRec.Name = "chbEventRec";
      this.chbEventRec.Size = new System.Drawing.Size(54, 17);
      this.chbEventRec.TabIndex = 0;
      this.chbEventRec.Text = "Event";
      this.chbEventRec.UseVisualStyleBackColor = true;
      // 
      // hierarchyTUInstances
      // 
      this.hierarchyTUInstances.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.hierarchyTUInstances.Location = new System.Drawing.Point(0, 0);
      this.hierarchyTUInstances.Name = "hierarchyTUInstances";
      this.hierarchyTUInstances.Size = new System.Drawing.Size(457, 95);
      this.hierarchyTUInstances.TabIndex = 12;
      // 
      // DRSelectorBasic
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.Controls.Add(this.hierarchyTUInstances);
      this.Controls.Add(this.gbRecType);
      this.Controls.Add(this.splitter1);
      this.Controls.Add(this.panBottom);
      this.Name = "DRSelectorBasic";
      this.Size = new System.Drawing.Size(548, 148);
      this.Load += new System.EventHandler(this.DRSelectorTU_Load);
      ((System.ComponentModel.ISupportInitialize)(this.diagRecords)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.recordsView)).EndInit();
      this.panBottom.ResumeLayout(false);
      this.gbRecType.ResumeLayout(false);
      this.gbRecType.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btSelect;
    private System.Windows.Forms.Panel panBottom;
    private System.Windows.Forms.Splitter splitter1;
    private TimePicker dtInterval;
    private System.Windows.Forms.GroupBox gbRecType;
    private System.Windows.Forms.CheckBox chbEventRec;
    private System.Windows.Forms.CheckBox chbSnapRec;
    private System.Windows.Forms.CheckBox chbTraceRec;
    private FleetHierarchy_TUInstances hierarchyTUInstances;
  }
}
