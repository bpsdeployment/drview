namespace DRSelectorComponents
{
  partial class FleetHierarchy_TUInstances
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FleetHierarchy_TUInstances));
      this.splitCntHierInst = new System.Windows.Forms.SplitContainer();
      this.panHierBottom = new System.Windows.Forms.Panel();
      this.fleetHierarchy = new DRSelectorComponents.FleetHierarchyTree();
      this.splitterHierarchy = new System.Windows.Forms.Splitter();
      this.panHierHead = new System.Windows.Forms.Panel();
      this.btHierarchy = new System.Windows.Forms.Button();
      this.imgListIcons = new System.Windows.Forms.ImageList(this.components);
      this.label1 = new System.Windows.Forms.Label();
      this.panTUBottom = new System.Windows.Forms.Panel();
      this.tuInstancesList = new DRSelectorComponents.TUInstancesList();
      this.splitterTU = new System.Windows.Forms.Splitter();
      this.panTUHead = new System.Windows.Forms.Panel();
      this.btTUInstances = new System.Windows.Forms.Button();
      this.label2 = new System.Windows.Forms.Label();
      this.splitCntHierInst.Panel1.SuspendLayout();
      this.splitCntHierInst.Panel2.SuspendLayout();
      this.splitCntHierInst.SuspendLayout();
      this.panHierBottom.SuspendLayout();
      this.panHierHead.SuspendLayout();
      this.panTUBottom.SuspendLayout();
      this.panTUHead.SuspendLayout();
      this.SuspendLayout();
      // 
      // splitCntHierInst
      // 
      this.splitCntHierInst.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitCntHierInst.Location = new System.Drawing.Point(0, 0);
      this.splitCntHierInst.Name = "splitCntHierInst";
      // 
      // splitCntHierInst.Panel1
      // 
      this.splitCntHierInst.Panel1.Controls.Add(this.panHierBottom);
      this.splitCntHierInst.Panel1.Controls.Add(this.splitterHierarchy);
      this.splitCntHierInst.Panel1.Controls.Add(this.panHierHead);
      // 
      // splitCntHierInst.Panel2
      // 
      this.splitCntHierInst.Panel2.Controls.Add(this.panTUBottom);
      this.splitCntHierInst.Panel2.Controls.Add(this.splitterTU);
      this.splitCntHierInst.Panel2.Controls.Add(this.panTUHead);
      this.splitCntHierInst.Panel2Collapsed = true;
      this.splitCntHierInst.Size = new System.Drawing.Size(195, 90);
      this.splitCntHierInst.SplitterDistance = 97;
      this.splitCntHierInst.TabIndex = 0;
      // 
      // panHierBottom
      // 
      this.panHierBottom.Controls.Add(this.fleetHierarchy);
      this.panHierBottom.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panHierBottom.Location = new System.Drawing.Point(0, 20);
      this.panHierBottom.Name = "panHierBottom";
      this.panHierBottom.Size = new System.Drawing.Size(195, 70);
      this.panHierBottom.TabIndex = 2;
      // 
      // fleetHierarchy
      // 
      this.fleetHierarchy.Dock = System.Windows.Forms.DockStyle.Fill;
      this.fleetHierarchy.ImageIndex = 1;
      this.fleetHierarchy.Location = new System.Drawing.Point(0, 0);
      this.fleetHierarchy.Name = "fleetHierarchy";
      this.fleetHierarchy.SelectedImageIndex = 1;
      this.fleetHierarchy.ShowNodeToolTips = true;
      this.fleetHierarchy.Size = new System.Drawing.Size(195, 70);
      this.fleetHierarchy.TabIndex = 0;
      // 
      // splitterHierarchy
      // 
      this.splitterHierarchy.Dock = System.Windows.Forms.DockStyle.Top;
      this.splitterHierarchy.Location = new System.Drawing.Point(0, 17);
      this.splitterHierarchy.Name = "splitterHierarchy";
      this.splitterHierarchy.Size = new System.Drawing.Size(195, 3);
      this.splitterHierarchy.TabIndex = 1;
      this.splitterHierarchy.TabStop = false;
      // 
      // panHierHead
      // 
      this.panHierHead.Controls.Add(this.btHierarchy);
      this.panHierHead.Controls.Add(this.label1);
      this.panHierHead.Dock = System.Windows.Forms.DockStyle.Top;
      this.panHierHead.Location = new System.Drawing.Point(0, 0);
      this.panHierHead.Name = "panHierHead";
      this.panHierHead.Size = new System.Drawing.Size(195, 17);
      this.panHierHead.TabIndex = 0;
      // 
      // btHierarchy
      // 
      this.btHierarchy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btHierarchy.ImageIndex = 1;
      this.btHierarchy.ImageList = this.imgListIcons;
      this.btHierarchy.Location = new System.Drawing.Point(179, 0);
      this.btHierarchy.Name = "btHierarchy";
      this.btHierarchy.Size = new System.Drawing.Size(16, 16);
      this.btHierarchy.TabIndex = 0;
      this.btHierarchy.UseVisualStyleBackColor = true;
      this.btHierarchy.Click += new System.EventHandler(this.btHierarchy_Click);
      // 
      // imgListIcons
      // 
      this.imgListIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgListIcons.ImageStream")));
      this.imgListIcons.TransparentColor = System.Drawing.Color.Transparent;
      this.imgListIcons.Images.SetKeyName(0, "cross.ico");
      this.imgListIcons.Images.SetKeyName(1, "expand.ico");
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(3, 3);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(78, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Fleet Hierarchy";
      // 
      // panTUBottom
      // 
      this.panTUBottom.Controls.Add(this.tuInstancesList);
      this.panTUBottom.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panTUBottom.Location = new System.Drawing.Point(0, 20);
      this.panTUBottom.Name = "panTUBottom";
      this.panTUBottom.Size = new System.Drawing.Size(94, 70);
      this.panTUBottom.TabIndex = 2;
      // 
      // tuInstancesList
      // 
      this.tuInstancesList.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tuInstancesList.ImageIndex = 1;
      this.tuInstancesList.Location = new System.Drawing.Point(0, 0);
      this.tuInstancesList.Name = "tuInstancesList";
      this.tuInstancesList.SelectedImageIndex = 1;
      this.tuInstancesList.ShowNodeToolTips = true;
      this.tuInstancesList.Size = new System.Drawing.Size(94, 70);
      this.tuInstancesList.TabIndex = 0;
      // 
      // splitterTU
      // 
      this.splitterTU.Dock = System.Windows.Forms.DockStyle.Top;
      this.splitterTU.Location = new System.Drawing.Point(0, 17);
      this.splitterTU.Name = "splitterTU";
      this.splitterTU.Size = new System.Drawing.Size(94, 3);
      this.splitterTU.TabIndex = 1;
      this.splitterTU.TabStop = false;
      // 
      // panTUHead
      // 
      this.panTUHead.Controls.Add(this.btTUInstances);
      this.panTUHead.Controls.Add(this.label2);
      this.panTUHead.Dock = System.Windows.Forms.DockStyle.Top;
      this.panTUHead.Location = new System.Drawing.Point(0, 0);
      this.panTUHead.Name = "panTUHead";
      this.panTUHead.Size = new System.Drawing.Size(94, 17);
      this.panTUHead.TabIndex = 0;
      // 
      // btTUInstances
      // 
      this.btTUInstances.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btTUInstances.ImageIndex = 0;
      this.btTUInstances.ImageList = this.imgListIcons;
      this.btTUInstances.Location = new System.Drawing.Point(78, 0);
      this.btTUInstances.Name = "btTUInstances";
      this.btTUInstances.Size = new System.Drawing.Size(16, 16);
      this.btTUInstances.TabIndex = 1;
      this.btTUInstances.UseVisualStyleBackColor = true;
      this.btTUInstances.Click += new System.EventHandler(this.btTUInstances_Click);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(3, 3);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(71, 13);
      this.label2.TabIndex = 1;
      this.label2.Text = "TU Instances";
      // 
      // FleetHierarchy_TUInstances
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.splitCntHierInst);
      this.MinimumSize = new System.Drawing.Size(195, 90);
      this.Name = "FleetHierarchy_TUInstances";
      this.Size = new System.Drawing.Size(195, 90);
      this.splitCntHierInst.Panel1.ResumeLayout(false);
      this.splitCntHierInst.Panel2.ResumeLayout(false);
      this.splitCntHierInst.ResumeLayout(false);
      this.panHierBottom.ResumeLayout(false);
      this.panHierHead.ResumeLayout(false);
      this.panHierHead.PerformLayout();
      this.panTUBottom.ResumeLayout(false);
      this.panTUHead.ResumeLayout(false);
      this.panTUHead.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.SplitContainer splitCntHierInst;
    private System.Windows.Forms.Panel panHierHead;
    private System.Windows.Forms.Panel panHierBottom;
    private System.Windows.Forms.Splitter splitterHierarchy;
    private FleetHierarchyTree fleetHierarchy;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button btHierarchy;
    private System.Windows.Forms.ImageList imgListIcons;
    private System.Windows.Forms.Panel panTUHead;
    private System.Windows.Forms.Splitter splitterTU;
    private System.Windows.Forms.Panel panTUBottom;
    private TUInstancesList tuInstancesList;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Button btTUInstances;
  }
}
