namespace StatSelectorComponents
{
  partial class StatSelectorDB
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.hierarchyTUInstances = new DRSelectorComponents.FleetHierarchy_TUInstances();
      this.dtInterval = new DRSelectorComponents.TimePicker();
      this.label4 = new System.Windows.Forms.Label();
      this.cbGroup1 = new System.Windows.Forms.ComboBox();
      this.gbConditions = new System.Windows.Forms.GroupBox();
      this.gbGrouping = new System.Windows.Forms.GroupBox();
      this.label6 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.cbGroup3 = new System.Windows.Forms.ComboBox();
      this.cbGroup2 = new System.Windows.Forms.ComboBox();
      this.btSelect = new System.Windows.Forms.Button();
      this.tbFaultSubseverities = new System.Windows.Forms.TextBox();
      this.label7 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.chbSevC = new System.Windows.Forms.CheckBox();
      this.chbSevB = new System.Windows.Forms.CheckBox();
      this.chbSevB1 = new System.Windows.Forms.CheckBox();
      this.chbSevA = new System.Windows.Forms.CheckBox();
      this.chbSevA1 = new System.Windows.Forms.CheckBox();
      this.label2 = new System.Windows.Forms.Label();
      this.tbVehicleNumbers = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.tbTrcSnpCodes = new System.Windows.Forms.TextBox();
      this.tbFaultCodes = new System.Windows.Forms.TextBox();
      this.chbMatchAllKeywords = new System.Windows.Forms.CheckBox();
      this.label8 = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.tbTitleKeyWords = new System.Windows.Forms.TextBox();
      this.tbRecSources = new System.Windows.Forms.TextBox();
      this.label10 = new System.Windows.Forms.Label();
      this.gbRecType = new System.Windows.Forms.GroupBox();
      this.chbSnapRec = new System.Windows.Forms.CheckBox();
      this.chbTraceRec = new System.Windows.Forms.CheckBox();
      this.chbEventRec = new System.Windows.Forms.CheckBox();
      this.gbConditions.SuspendLayout();
      this.gbGrouping.SuspendLayout();
      this.gbRecType.SuspendLayout();
      this.SuspendLayout();
      // 
      // hierarchyTUInstances
      // 
      this.hierarchyTUInstances.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.hierarchyTUInstances.Location = new System.Drawing.Point(6, 15);
      this.hierarchyTUInstances.MinimumSize = new System.Drawing.Size(195, 90);
      this.hierarchyTUInstances.Name = "hierarchyTUInstances";
      this.hierarchyTUInstances.Size = new System.Drawing.Size(534, 198);
      this.hierarchyTUInstances.TabIndex = 0;
      // 
      // dtInterval
      // 
      this.dtInterval.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.dtInterval.FromSelected = true;
      this.dtInterval.Location = new System.Drawing.Point(6, 331);
      this.dtInterval.Name = "dtInterval";
      this.dtInterval.PredefIntervalIndex = 0;
      this.dtInterval.Size = new System.Drawing.Size(452, 45);
      this.dtInterval.TabIndex = 27;
      this.dtInterval.TimeFrom = new System.DateTime(2009, 1, 12, 15, 3, 1, 721);
      this.dtInterval.TimeTo = new System.DateTime(2009, 1, 12, 15, 3, 1, 721);
      this.dtInterval.ToSelected = true;
      // 
      // label4
      // 
      this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(9, 18);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(54, 13);
      this.label4.TabIndex = 28;
      this.label4.Text = "First level:";
      // 
      // cbGroup1
      // 
      this.cbGroup1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.cbGroup1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
      this.cbGroup1.FormattingEnabled = true;
      this.cbGroup1.Items.AddRange(new object[] {
            "(none)",
            "Train",
            "Vehicle",
            "Year",
            "Month",
            "Day",
            "Hour",
            "Fault code",
            "Trace or Snap code",
            "Source",
            "Device code",
            "Vehicle number",
            "Fault severity",
            "Fault subseverity",
            "Record type",
            "TU Instance",
            "TU Type"});
      this.cbGroup1.Location = new System.Drawing.Point(12, 34);
      this.cbGroup1.Name = "cbGroup1";
      this.cbGroup1.Size = new System.Drawing.Size(121, 21);
      this.cbGroup1.TabIndex = 29;
      this.cbGroup1.SelectedIndexChanged += new System.EventHandler(this.cbGroup1_SelectedIndexChanged);
      // 
      // gbConditions
      // 
      this.gbConditions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.gbConditions.Controls.Add(this.tbFaultSubseverities);
      this.gbConditions.Controls.Add(this.label7);
      this.gbConditions.Controls.Add(this.label1);
      this.gbConditions.Controls.Add(this.chbSevC);
      this.gbConditions.Controls.Add(this.chbSevB);
      this.gbConditions.Controls.Add(this.chbSevB1);
      this.gbConditions.Controls.Add(this.chbSevA);
      this.gbConditions.Controls.Add(this.chbSevA1);
      this.gbConditions.Controls.Add(this.label2);
      this.gbConditions.Controls.Add(this.tbVehicleNumbers);
      this.gbConditions.Controls.Add(this.label3);
      this.gbConditions.Controls.Add(this.tbTrcSnpCodes);
      this.gbConditions.Controls.Add(this.tbFaultCodes);
      this.gbConditions.Controls.Add(this.chbMatchAllKeywords);
      this.gbConditions.Controls.Add(this.label8);
      this.gbConditions.Controls.Add(this.label9);
      this.gbConditions.Controls.Add(this.tbTitleKeyWords);
      this.gbConditions.Controls.Add(this.tbRecSources);
      this.gbConditions.Controls.Add(this.label10);
      this.gbConditions.Controls.Add(this.gbRecType);
      this.gbConditions.Controls.Add(this.hierarchyTUInstances);
      this.gbConditions.Controls.Add(this.dtInterval);
      this.gbConditions.Location = new System.Drawing.Point(4, 0);
      this.gbConditions.Name = "gbConditions";
      this.gbConditions.Size = new System.Drawing.Size(545, 381);
      this.gbConditions.TabIndex = 30;
      this.gbConditions.TabStop = false;
      this.gbConditions.Text = "Query conditions";
      // 
      // gbGrouping
      // 
      this.gbGrouping.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.gbGrouping.Controls.Add(this.label6);
      this.gbGrouping.Controls.Add(this.label5);
      this.gbGrouping.Controls.Add(this.cbGroup3);
      this.gbGrouping.Controls.Add(this.cbGroup2);
      this.gbGrouping.Controls.Add(this.cbGroup1);
      this.gbGrouping.Controls.Add(this.label4);
      this.gbGrouping.Location = new System.Drawing.Point(4, 382);
      this.gbGrouping.Name = "gbGrouping";
      this.gbGrouping.Size = new System.Drawing.Size(418, 63);
      this.gbGrouping.TabIndex = 31;
      this.gbGrouping.TabStop = false;
      this.gbGrouping.Text = "Group result by";
      // 
      // label6
      // 
      this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(284, 18);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(59, 13);
      this.label6.TabIndex = 33;
      this.label6.Text = "Third level:";
      // 
      // label5
      // 
      this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(146, 18);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(72, 13);
      this.label5.TabIndex = 32;
      this.label5.Text = "Second level:";
      // 
      // cbGroup3
      // 
      this.cbGroup3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.cbGroup3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
      this.cbGroup3.Enabled = false;
      this.cbGroup3.FormattingEnabled = true;
      this.cbGroup3.Items.AddRange(new object[] {
            "(none)",
            "Train",
            "Vehicle",
            "Year",
            "Month",
            "Day",
            "Hour",
            "Fault code",
            "Trace or Snap code",
            "Source",
            "Device code",
            "Vehicle number",
            "Fault severity",
            "Fault subseverity",
            "Record type",
            "TU Instance",
            "TU Type"});
      this.cbGroup3.Location = new System.Drawing.Point(287, 34);
      this.cbGroup3.Name = "cbGroup3";
      this.cbGroup3.Size = new System.Drawing.Size(121, 21);
      this.cbGroup3.TabIndex = 31;
      this.cbGroup3.SelectedIndexChanged += new System.EventHandler(this.cbGroup3_SelectedIndexChanged);
      // 
      // cbGroup2
      // 
      this.cbGroup2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.cbGroup2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
      this.cbGroup2.Enabled = false;
      this.cbGroup2.FormattingEnabled = true;
      this.cbGroup2.Items.AddRange(new object[] {
            "(none)",
            "Train",
            "Vehicle",
            "Year",
            "Month",
            "Day",
            "Hour",
            "Fault code",
            "Trace or Snap code",
            "Source",
            "Device code",
            "Vehicle number",
            "Fault severity",
            "Fault subseverity",
            "Record type",
            "TU Instance",
            "TU Type"});
      this.cbGroup2.Location = new System.Drawing.Point(149, 34);
      this.cbGroup2.Name = "cbGroup2";
      this.cbGroup2.Size = new System.Drawing.Size(121, 21);
      this.cbGroup2.TabIndex = 30;
      this.cbGroup2.SelectedIndexChanged += new System.EventHandler(this.cbGroup2_SelectedIndexChanged);
      // 
      // btSelect
      // 
      this.btSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btSelect.Location = new System.Drawing.Point(455, 414);
      this.btSelect.Name = "btSelect";
      this.btSelect.Size = new System.Drawing.Size(75, 23);
      this.btSelect.TabIndex = 32;
      this.btSelect.Text = "Select";
      this.btSelect.UseVisualStyleBackColor = true;
      this.btSelect.Click += new System.EventHandler(this.btSelect_Click);
      // 
      // tbFaultSubseverities
      // 
      this.tbFaultSubseverities.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbFaultSubseverities.Location = new System.Drawing.Point(187, 312);
      this.tbFaultSubseverities.Name = "tbFaultSubseverities";
      this.tbFaultSubseverities.Size = new System.Drawing.Size(115, 20);
      this.tbFaultSubseverities.TabIndex = 52;
      this.tbFaultSubseverities.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericTextBox_KeyPress);
      // 
      // label7
      // 
      this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(184, 298);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(97, 13);
      this.label7.TabIndex = 51;
      this.label7.Text = "Fault subseverities:";
      // 
      // label1
      // 
      this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(3, 298);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(80, 13);
      this.label1.TabIndex = 50;
      this.label1.Text = "Fault severities:";
      // 
      // chbSevC
      // 
      this.chbSevC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.chbSevC.AutoSize = true;
      this.chbSevC.Location = new System.Drawing.Point(148, 314);
      this.chbSevC.Name = "chbSevC";
      this.chbSevC.Size = new System.Drawing.Size(33, 17);
      this.chbSevC.TabIndex = 49;
      this.chbSevC.Text = "C";
      this.chbSevC.UseVisualStyleBackColor = true;
      // 
      // chbSevB
      // 
      this.chbSevB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.chbSevB.AutoSize = true;
      this.chbSevB.Location = new System.Drawing.Point(117, 314);
      this.chbSevB.Name = "chbSevB";
      this.chbSevB.Size = new System.Drawing.Size(33, 17);
      this.chbSevB.TabIndex = 48;
      this.chbSevB.Text = "B";
      this.chbSevB.UseVisualStyleBackColor = true;
      // 
      // chbSevB1
      // 
      this.chbSevB1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.chbSevB1.AutoSize = true;
      this.chbSevB1.Location = new System.Drawing.Point(80, 314);
      this.chbSevB1.Name = "chbSevB1";
      this.chbSevB1.Size = new System.Drawing.Size(39, 17);
      this.chbSevB1.TabIndex = 47;
      this.chbSevB1.Text = "B1";
      this.chbSevB1.UseVisualStyleBackColor = true;
      // 
      // chbSevA
      // 
      this.chbSevA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.chbSevA.AutoSize = true;
      this.chbSevA.Location = new System.Drawing.Point(46, 314);
      this.chbSevA.Name = "chbSevA";
      this.chbSevA.Size = new System.Drawing.Size(33, 17);
      this.chbSevA.TabIndex = 46;
      this.chbSevA.Text = "A";
      this.chbSevA.UseVisualStyleBackColor = true;
      // 
      // chbSevA1
      // 
      this.chbSevA1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.chbSevA1.AutoSize = true;
      this.chbSevA1.Location = new System.Drawing.Point(9, 314);
      this.chbSevA1.Name = "chbSevA1";
      this.chbSevA1.Size = new System.Drawing.Size(39, 17);
      this.chbSevA1.TabIndex = 45;
      this.chbSevA1.Text = "A1";
      this.chbSevA1.UseVisualStyleBackColor = true;
      // 
      // label2
      // 
      this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(305, 259);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(88, 13);
      this.label2.TabIndex = 44;
      this.label2.Text = "Vehicle numbers:";
      // 
      // tbVehicleNumbers
      // 
      this.tbVehicleNumbers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbVehicleNumbers.Location = new System.Drawing.Point(308, 275);
      this.tbVehicleNumbers.Name = "tbVehicleNumbers";
      this.tbVehicleNumbers.Size = new System.Drawing.Size(144, 20);
      this.tbVehicleNumbers.TabIndex = 43;
      this.tbVehicleNumbers.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericTextBox_KeyPress);
      // 
      // label3
      // 
      this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(154, 259);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(110, 13);
      this.label3.TabIndex = 42;
      this.label3.Text = "Trace or Snap codes:";
      // 
      // tbTrcSnpCodes
      // 
      this.tbTrcSnpCodes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbTrcSnpCodes.Location = new System.Drawing.Point(157, 275);
      this.tbTrcSnpCodes.Name = "tbTrcSnpCodes";
      this.tbTrcSnpCodes.Size = new System.Drawing.Size(145, 20);
      this.tbTrcSnpCodes.TabIndex = 41;
      this.tbTrcSnpCodes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericTextBox_KeyPress);
      // 
      // tbFaultCodes
      // 
      this.tbFaultCodes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbFaultCodes.Location = new System.Drawing.Point(6, 275);
      this.tbFaultCodes.Name = "tbFaultCodes";
      this.tbFaultCodes.Size = new System.Drawing.Size(145, 20);
      this.tbFaultCodes.TabIndex = 40;
      this.tbFaultCodes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericTextBox_KeyPress);
      // 
      // chbMatchAllKeywords
      // 
      this.chbMatchAllKeywords.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.chbMatchAllKeywords.AutoSize = true;
      this.chbMatchAllKeywords.Location = new System.Drawing.Point(157, 219);
      this.chbMatchAllKeywords.Name = "chbMatchAllKeywords";
      this.chbMatchAllKeywords.Size = new System.Drawing.Size(69, 17);
      this.chbMatchAllKeywords.TabIndex = 39;
      this.chbMatchAllKeywords.Text = "Match all";
      this.chbMatchAllKeywords.UseVisualStyleBackColor = true;
      // 
      // label8
      // 
      this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(3, 220);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(112, 13);
      this.label8.TabIndex = 38;
      this.label8.Text = "Record title keywords:";
      // 
      // label9
      // 
      this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(3, 259);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(65, 13);
      this.label9.TabIndex = 37;
      this.label9.Text = "Fault codes:";
      // 
      // tbTitleKeyWords
      // 
      this.tbTitleKeyWords.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbTitleKeyWords.Location = new System.Drawing.Point(6, 236);
      this.tbTitleKeyWords.Name = "tbTitleKeyWords";
      this.tbTitleKeyWords.Size = new System.Drawing.Size(220, 20);
      this.tbTitleKeyWords.TabIndex = 36;
      // 
      // tbRecSources
      // 
      this.tbRecSources.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbRecSources.Location = new System.Drawing.Point(232, 236);
      this.tbRecSources.Name = "tbRecSources";
      this.tbRecSources.Size = new System.Drawing.Size(220, 20);
      this.tbRecSources.TabIndex = 35;
      // 
      // label10
      // 
      this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(229, 220);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(85, 13);
      this.label10.TabIndex = 34;
      this.label10.Text = "Record sources:";
      // 
      // gbRecType
      // 
      this.gbRecType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.gbRecType.Controls.Add(this.chbSnapRec);
      this.gbRecType.Controls.Add(this.chbTraceRec);
      this.gbRecType.Controls.Add(this.chbEventRec);
      this.gbRecType.Location = new System.Drawing.Point(458, 220);
      this.gbRecType.Name = "gbRecType";
      this.gbRecType.Size = new System.Drawing.Size(82, 75);
      this.gbRecType.TabIndex = 33;
      this.gbRecType.TabStop = false;
      this.gbRecType.Text = "Record type";
      // 
      // chbSnapRec
      // 
      this.chbSnapRec.AutoSize = true;
      this.chbSnapRec.Location = new System.Drawing.Point(6, 54);
      this.chbSnapRec.Name = "chbSnapRec";
      this.chbSnapRec.Size = new System.Drawing.Size(51, 17);
      this.chbSnapRec.TabIndex = 2;
      this.chbSnapRec.Text = "Snap";
      this.chbSnapRec.UseVisualStyleBackColor = true;
      // 
      // chbTraceRec
      // 
      this.chbTraceRec.AutoSize = true;
      this.chbTraceRec.Location = new System.Drawing.Point(6, 35);
      this.chbTraceRec.Name = "chbTraceRec";
      this.chbTraceRec.Size = new System.Drawing.Size(54, 17);
      this.chbTraceRec.TabIndex = 1;
      this.chbTraceRec.Text = "Trace";
      this.chbTraceRec.UseVisualStyleBackColor = true;
      // 
      // chbEventRec
      // 
      this.chbEventRec.AutoSize = true;
      this.chbEventRec.Location = new System.Drawing.Point(6, 16);
      this.chbEventRec.Name = "chbEventRec";
      this.chbEventRec.Size = new System.Drawing.Size(54, 17);
      this.chbEventRec.TabIndex = 0;
      this.chbEventRec.Text = "Event";
      this.chbEventRec.UseVisualStyleBackColor = true;
      // 
      // StatSelectorDB
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.btSelect);
      this.Controls.Add(this.gbGrouping);
      this.Controls.Add(this.gbConditions);
      this.Name = "StatSelectorDB";
      this.Size = new System.Drawing.Size(553, 450);
      this.Load += new System.EventHandler(this.StatSelectorDB_Load);
      this.gbConditions.ResumeLayout(false);
      this.gbConditions.PerformLayout();
      this.gbGrouping.ResumeLayout(false);
      this.gbGrouping.PerformLayout();
      this.gbRecType.ResumeLayout(false);
      this.gbRecType.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private DRSelectorComponents.FleetHierarchy_TUInstances hierarchyTUInstances;
    private DRSelectorComponents.TimePicker dtInterval;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.ComboBox cbGroup1;
    private System.Windows.Forms.GroupBox gbConditions;
    private System.Windows.Forms.GroupBox gbGrouping;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.ComboBox cbGroup3;
    private System.Windows.Forms.ComboBox cbGroup2;
    private System.Windows.Forms.Button btSelect;
    private System.Windows.Forms.TextBox tbFaultSubseverities;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.CheckBox chbSevC;
    private System.Windows.Forms.CheckBox chbSevB;
    private System.Windows.Forms.CheckBox chbSevB1;
    private System.Windows.Forms.CheckBox chbSevA;
    private System.Windows.Forms.CheckBox chbSevA1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox tbVehicleNumbers;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox tbTrcSnpCodes;
    private System.Windows.Forms.TextBox tbFaultCodes;
    private System.Windows.Forms.CheckBox chbMatchAllKeywords;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.TextBox tbTitleKeyWords;
    private System.Windows.Forms.TextBox tbRecSources;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.GroupBox gbRecType;
    private System.Windows.Forms.CheckBox chbSnapRec;
    private System.Windows.Forms.CheckBox chbTraceRec;
    private System.Windows.Forms.CheckBox chbEventRec;
  }
}
