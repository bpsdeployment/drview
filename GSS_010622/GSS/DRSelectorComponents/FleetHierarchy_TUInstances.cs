using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace DRSelectorComponents
{
  /// <summary>
  /// Visual component displaying both fleet hierarchy and TU Instances list in resizable containers
  /// </summary>
  public partial class FleetHierarchy_TUInstances : UserControl
  {
    /// <summary>
    /// Fleet hierarchy component
    /// </summary>
    public FleetHierarchyTree FleetHierarchy
    {
      get { return fleetHierarchy; }
    }

    /// <summary>
    /// TU Instances component
    /// </summary>
    public TUInstancesList TUInstancesList
    {
      get { return tuInstancesList; }
    }

    public FleetHierarchy_TUInstances()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Event handler for click on close Hierarchy / show TUInstances button
    /// </summary>
    private void btHierarchy_Click(object sender, EventArgs e)
    {
      if (splitCntHierInst.Panel2Collapsed)
      { //TU Instances hidden - show TU Instances
        splitCntHierInst.Panel2Collapsed = false;
        btHierarchy.ImageIndex = 0; //Set close image to hierarchy button
        btTUInstances.ImageIndex = 0; //Set close image to TU Instances button
      }
      else
      { //Hide Fleet hierarchy
        splitCntHierInst.Panel1Collapsed = true; //Hide fleet hierarchy
        btTUInstances.ImageIndex = 1; //Set expand image to TU Instances button
      }
    }

    /// <summary>
    /// Event handler for click on close TU Instances / show Hierarchy button
    /// </summary>
    private void btTUInstances_Click(object sender, EventArgs e)
    {
      if (splitCntHierInst.Panel1Collapsed)
      { //Hierarchy hidden - show hierarchy
        splitCntHierInst.Panel1Collapsed = false;
        btHierarchy.ImageIndex = 0; //Set close image to hierarchy button
        btTUInstances.ImageIndex = 0; //Set close image to TU Instances button
      }
      else
      { //Hide TU Instances
        splitCntHierInst.Panel2Collapsed = true; //Hide TU Instances
        btHierarchy.ImageIndex = 1; //Set expand image to hierarchy button
      }
    }
  }
}
