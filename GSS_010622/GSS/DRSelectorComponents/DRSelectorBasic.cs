using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml;
using System.Threading;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using DSTU_RecType;

namespace DRSelectorComponents
{
  /// <summary>
  /// Component enabling to select diagnostic records based on TUInstance and time
  /// </summary>
  public partial class DRSelectorBasic : DRSelectorComponents.DRSelectorBase
  {
    #region Members
    protected SqlCommand cmdSelectDR; //Stored procedure selecting DR identifiers into temporary table according to specified parameters
    #endregion //Members

    #region Construction and initialization
    public DRSelectorBasic() 
      : base() //Call base constructor
    {
      InitializeComponent();
      //Initialize member variables
      InitMembers();
    }

    public DRSelectorBasic(Dictionary<String, Object> selParams)
      : base(selParams) //Call base constructor
    {
      InitializeComponent();
      //Initialize member variables
      InitMembers();
    }

    protected void InitMembers()
    {
      //Create SQL command object
      cmdSelectDR = new SqlCommand();
      //Create and prepare command selecting DR
      cmdSelectDR.CommandType = CommandType.StoredProcedure;
      cmdSelectDR.CommandText = DRSelectors.selectDRSelectorTU;
      cmdSelectDR.Parameters.Add("@TUInstances", SqlDbType.NVarChar, 4000);
      cmdSelectDR.Parameters.Add("@HierarchyNames", SqlDbType.NVarChar, 2147483647);
      cmdSelectDR.Parameters.Add("@From", SqlDbType.DateTime);
      cmdSelectDR.Parameters.Add("@To", SqlDbType.DateTime);
      cmdSelectDR.Parameters.Add("@RecTypeMask", SqlDbType.Int);
      cmdSelectDR.Parameters.Add("@MaxRows", SqlDbType.Int);
    }

    #endregion //Construction and initialization

    #region Component event handlers
    private void btSelect_Click(object sender, EventArgs e)
    {
      //Start asynchronous record load
      LoadRecordsAsync();
    }

    private void DRSelectorTU_Load(object sender, EventArgs e)
    {
      //Refresh fleet hierarchy and list of TU Instances
      RefreshHierarchyAndInstances();
    }
    #endregion //Component event handlers

    #region Helper functions
    /// <summary>
    /// Selects list of all TU Instances from DB and fills the grid
    /// </summary>
    protected void RefreshHierarchyAndInstances()
    {
      try
      {
        //Set proper connection string to DDD Helper
        if (dbConnection != null)
          dddHelper.ConnectionString = dbConnection.ConnectionString;
        
        hierarchyTUInstances.FleetHierarchy.ShowCurrentHierarchy(dddHelper.HierarchyHelper, null);
        hierarchyTUInstances.TUInstancesList.ShowTUInstances(dddHelper);
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRSelectorException(ex, "Failed to load fleet hierarchy or TU Instances list"));
      }
    }
    #endregion //Helper functions

    #region Virtual method overrides
    /// <summary>
    /// Method reads values from internal components and prepares DRLoadParametersTU object
    /// </summary>
    /// <returns>Object containing all necessary parameters for DR selection</returns>
    protected override DRLoadParameters PrepareLoadParameters()
    {
      //Create parameters object
      DRLoadParametersTU loadParams = new DRLoadParametersTU();
      //Obtain string containing selected TUInstanceIDs
      loadParams.selTUInstances = hierarchyTUInstances.TUInstancesList.GetSelectedInstanceIDs();
      //Obtain string containing XML with selected hierarchy items
      loadParams.selHierarchyItems = hierarchyTUInstances.FleetHierarchy.GetSelectedNamesXML();
      //Set time parameters
      if (dtInterval.FromSelected)
        loadParams.timeFrom = dddHelper.UserTimeToUTC(dtInterval.TimeFrom);
      else
        loadParams.timeFrom = DBNull.Value;
      if (dtInterval.ToSelected)
        loadParams.timeTo = dddHelper.UserTimeToUTC(dtInterval.TimeTo);
      else
        loadParams.timeTo = DBNull.Value;
      //Set mask for record types
      loadParams.recTypeMask = 0;
      if (chbEventRec.Checked)
        loadParams.recTypeMask |= 0x02;
      if (chbTraceRec.Checked)
        loadParams.recTypeMask |= 0x04;
      if (chbSnapRec.Checked)
        loadParams.recTypeMask |= 0x08;
      return loadParams;
    }

    /// <summary>
    /// Method is invoked asynchronously and performs the load of DR.
    /// </summary>
    /// <param name="loadParams">Object containing all necessary parameters for DR selection</param>
    /// <param name="asyncOp">AsyncOperation used to post callbacks</param>
    protected override void PerformAsyncDRLoad(DRLoadParameters loadParams, AsyncOperation asyncOp)
    {
      try
      {
        //Cast parameters object to proper type
        DRLoadParametersTU parameters = loadParams as DRLoadParametersTU;
        //Check connection to database
        if (!CheckDBConnection())
          throw new DRSelectorException("Connection to database could not be opened");

        //Select data from database
        //Set command parameters
        cmdSelectDR.Parameters["@From"].Value = parameters.timeFrom;
        cmdSelectDR.Parameters["@To"].Value = parameters.timeTo;
        if (parameters.selTUInstances != String.Empty)
          cmdSelectDR.Parameters["@TUInstances"].Value = parameters.selTUInstances;
        else
          cmdSelectDR.Parameters["@TUInstances"].Value = DBNull.Value;
        if (parameters.selHierarchyItems != String.Empty)
          cmdSelectDR.Parameters["@HierarchyNames"].Value = parameters.selHierarchyItems;
        else
          cmdSelectDR.Parameters["@HierarchyNames"].Value = DBNull.Value;
        cmdSelectDR.Parameters["@RecTypeMask"].Value = parameters.recTypeMask;
        cmdSelectDR.Parameters["@MaxRows"].Value = MaxRecCount;
        cmdSelectDR.CommandTimeout = sqlCmdTimeout;
        cmdSelectDR.Connection = dbConnection;
        //Execute SQL command
        SqlDataReader reader;
        //XmlReader reader;
        try
        {
          reader = cmdSelectDR.ExecuteReader();
          //reader = cmdSelectDR.ExecuteXmlReader();
        }
        catch (Exception ex)
        {
          throw new DRSelectorException(ex, "Failed to execute DB stored procedure");
        }

        //Post info message
        asyncOp.Post(onInfoMessageDelegate, "Loading records to data set");
        //Process returned XML document and fill internal dataset
        try
        {
          FillDataSetFromResultSets(reader, asyncOp);
          //FillDataSetFromXml(reader, asyncOp);
        }
        catch (Exception)
        {
          throw;
        }
        finally
        {
          //Close reader object
          reader.Close();
        }

        //Post completed event
        AsyncCompletedEventArgs result = new AsyncCompletedEventArgs(null, false, null);
        asyncOp.PostOperationCompleted(onDRLoadCompletedDelegate, result);
      }
      catch (Exception ex)
      {
        //Post completed event with error
        AsyncCompletedEventArgs result = new AsyncCompletedEventArgs(ex, false, null);
        asyncOp.PostOperationCompleted(onDRLoadCompletedDelegate, result);
      }
    }

    /// <summary>
    /// Saves settings of selector to file
    /// </summary>
    /// <param name="file">Stream to save to</param>
    /// <param name="formatter">Formatter used for saving objects</param>
    public override void SaveSettingsToFile(FileStream file, BinaryFormatter formatter)
    {
      //Serialize
      formatter.Serialize(file, hierarchyTUInstances.TUInstancesList.GetSelectedInstanceIDs());
      formatter.Serialize(file, hierarchyTUInstances.FleetHierarchy.GetSelectedNamesXML());
      formatter.Serialize(file, dtInterval.TimeFrom);
      formatter.Serialize(file, dtInterval.FromSelected);
      formatter.Serialize(file, dtInterval.TimeTo);
      formatter.Serialize(file, dtInterval.ToSelected);
      formatter.Serialize(file, dtInterval.PredefIntervalIndex);
      formatter.Serialize(file, chbEventRec.Checked);
      formatter.Serialize(file, chbSnapRec.Checked);
      formatter.Serialize(file, chbTraceRec.Checked);
    }

    /// <summary>
    /// Loads settings of selector from file
    /// </summary>
    /// <param name="file">Stream to save to</param>
    /// <param name="formatter">Formatter used for saving objects</param>
    
    public override void  LoadSettingsFromFile(FileStream file, BinaryFormatter formatter)
    {
      //Check TU Instances
      hierarchyTUInstances.TUInstancesList.CheckTUInstances((String)formatter.Deserialize(file));
      //Check fleet hierarchy items
      hierarchyTUInstances.FleetHierarchy.CheckHierarchyItems((String)formatter.Deserialize(file));
      //Retrieve rest of parameters
      dtInterval.TimeFrom = (DateTime)formatter.Deserialize(file);
      dtInterval.FromSelected = (bool)formatter.Deserialize(file);
      dtInterval.TimeTo = (DateTime)formatter.Deserialize(file);
      dtInterval.ToSelected = (bool)formatter.Deserialize(file);
      dtInterval.PredefIntervalIndex = (int)formatter.Deserialize(file);
      chbEventRec.Checked = (bool)formatter.Deserialize(file);
      chbSnapRec.Checked = (bool)formatter.Deserialize(file);
      chbTraceRec.Checked = (bool)formatter.Deserialize(file);
    }
    #endregion //Virtual method overrides
  }

  #region Load parameters class
  /// <summary>
  /// Class representing parameters for DR Load operation.
  /// </summary>
  public class DRLoadParametersTU : DRLoadParameters
  {
    public Object timeFrom; //start of the interval
    public Object timeTo; //end of the interval
    public String selTUInstances; //list of selected TU Instances
    public String selHierarchyItems; //XML with selected hierarchy items
    public int recTypeMask = 0; //Mask for record types
  }
  #endregion //Load parameters class
}

