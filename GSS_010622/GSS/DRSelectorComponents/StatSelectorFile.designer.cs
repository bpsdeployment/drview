namespace StatSelectorComponents
{
  partial class StatSelectorFile
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btLoadFile = new System.Windows.Forms.Button();
      this.dlgOpenFile = new System.Windows.Forms.OpenFileDialog();
      this.tbFileName = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.btSelectFile = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // btLoadFile
      // 
      this.btLoadFile.Location = new System.Drawing.Point(3, 48);
      this.btLoadFile.Name = "btLoadFile";
      this.btLoadFile.Size = new System.Drawing.Size(75, 23);
      this.btLoadFile.TabIndex = 0;
      this.btLoadFile.Text = "Load file";
      this.btLoadFile.UseVisualStyleBackColor = true;
      this.btLoadFile.Click += new System.EventHandler(this.btLoadFile_Click);
      // 
      // tbFileName
      // 
      this.tbFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.tbFileName.Location = new System.Drawing.Point(3, 22);
      this.tbFileName.Name = "tbFileName";
      this.tbFileName.Size = new System.Drawing.Size(213, 20);
      this.tbFileName.TabIndex = 1;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(0, 6);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(55, 13);
      this.label1.TabIndex = 2;
      this.label1.Text = "File name:";
      // 
      // btSelectFile
      // 
      this.btSelectFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btSelectFile.Location = new System.Drawing.Point(220, 20);
      this.btSelectFile.Name = "btSelectFile";
      this.btSelectFile.Size = new System.Drawing.Size(25, 23);
      this.btSelectFile.TabIndex = 3;
      this.btSelectFile.Text = "...";
      this.btSelectFile.UseVisualStyleBackColor = true;
      this.btSelectFile.Click += new System.EventHandler(this.btSelectFile_Click);
      // 
      // StatSelectorFile
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.Controls.Add(this.btSelectFile);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.tbFileName);
      this.Controls.Add(this.btLoadFile);
      this.MinimumSize = new System.Drawing.Size(250, 75);
      this.Name = "StatSelectorFile";
      this.Size = new System.Drawing.Size(250, 75);
      this.Load += new System.EventHandler(this.DRSelectorFile_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btLoadFile;
    private System.Windows.Forms.OpenFileDialog dlgOpenFile;
    private System.Windows.Forms.TextBox tbFileName;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button btSelectFile;
  }
}
