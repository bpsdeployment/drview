using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net;
using System.IO.Compression;
using ZetaLibWeb;
using DDDObjects;
using DSDiagnosticRecords;

namespace DRSelectorComponents
{
  /// <summary>
  /// Component enabling to select diagnostic records based on TUInstance and time
  /// </summary>
  public partial class DRSelectorMDS : DRSelectorComponents.DRSelectorBase
  {
    #region Static members
    private static String NumTbTemplate = "0123456789";
    private static String UrlExport = "http://{0}/database/export.cgi";
    private static String UrlTUInfo = "http://{0}/database/gettuinfo.cgi";
    private static String UrlDDDVer = "http://{0}/database/getdddver.cgi";
    private static String UrlDDD    = "http://{0}/database/getddd.cgi";
    #endregion 
    
    #region Public properties
    #endregion //Public properties

    #region Private properties
    XmlDocument tuAddrDoc = null; //XML document with addresses of Telediagnostic units
    #endregion //Private properties

    #region Construction and initialization
    public DRSelectorMDS() 
      : base() //Call base constructor
    {
      InitializeComponent();
    }

    public DRSelectorMDS(Dictionary<String, Object> selParams)
      : base(selParams) //Call base constructor
    {
      InitializeComponent();
    }
    #endregion //Construction and initialization

    #region Component event handlers
    private void DRSelectorMDS_Load(object sender, EventArgs e)
    {
      //Fill list of previously stored addresses of TUs
      LoadTUAddresses();
      //Preset time interval
      dtInterval.PredefIntervalIndex = 1;
    }

    /// <summary>
    /// Handles event from Select button, calls base selector methods
    /// </summary>
    private void btSelectRecords_Click(object sender, EventArgs e)
    {
      //Store selected TU address if it does not exist yet
      StoreTUAddress(cbTUAddress.Text);
      //Start asynchronous record load
      LoadRecordsAsync();
      //Set form title
      this.ParentForm.Text = cbTUAddress.Text;
    }
    #endregion //Component event handlers

    #region Virtual method overrides
    /// <summary>
    /// Method reads values from internal components and prepares DRLoadParametersTUDirect object
    /// </summary>
    /// <returns>Object containing all necessary parameters for DR selection</returns>
    protected override DRLoadParameters PrepareLoadParameters()
    {
      //Create parameters object
      DRLoadParametersMDS loadParams = new DRLoadParametersMDS();
      //Store selected values to load params
      loadParams.tuAddress = cbTUAddress.Text;
      loadParams.timeOffset = (int)nudMDSTimeOffset.Value;
      if (dtInterval.FromSelected)
        //loadParams.timeFrom = dddHelper.UserTimeToUTC(dtInterval.TimeFrom).ToString("dd-MM-yyyy HH:mm").Replace("-","/");
        loadParams.timeFrom = dtInterval.TimeFrom.ToUniversalTime().AddHours(loadParams.timeOffset).ToString("dd-MM-yyyy HH:mm").Replace("-", "/");
      else
        loadParams.timeFrom = "";
      if (dtInterval.ToSelected)
        //loadParams.timeTo = dddHelper.UserTimeToUTC(dtInterval.TimeTo).ToString("dd-MM-yyyy HH:mm").Replace("-", "/");
        loadParams.timeTo = dtInterval.TimeTo.ToUniversalTime().AddHours(loadParams.timeOffset).ToString("dd-MM-yyyy HH:mm").Replace("-", "/");
      else
        loadParams.timeTo = "";

      //Event record parameters
      loadParams.selectEvents = chbSelectEvents.Checked;
      loadParams.A1 = chbSevA1.Checked;
      loadParams.A = chbSevA.Checked;
      loadParams.B1 = chbSevB1.Checked;
      loadParams.B = chbSevB.Checked;
      loadParams.C = chbSevC.Checked;
      if (tbDeviceCode.Text.Trim() != String.Empty)
        loadParams.deviceCode = Int32.Parse(tbDeviceCode.Text);
      if (cbEventName.SelectedItem != null)
        loadParams.eventName = ((RecNameTitle)cbEventName.SelectedItem).Name;
      else
        loadParams.eventName = String.Empty;
      if (nudVehicleNumber.Value != 0)
        loadParams.vehicleNumber = (int)nudVehicleNumber.Value;
      loadParams.maxEvtRecords = (int)nudMaxEvents.Value;

      //Trace record parameters
      loadParams.selectTraces = chbSelectTraces.Checked;
      if (cbTraceName.SelectedItem != null)
        loadParams.traceName = ((RecNameTitle)cbTraceName.SelectedItem).Name;
      else
        loadParams.traceName = String.Empty;
      loadParams.maxTraceRecords = (int)nudMaxTraces.Value;

      //Snap record parameters
      loadParams.selectSnaps = chbSelectSnaps.Checked;
      if (cbSnapName.SelectedItem != null)
        loadParams.snapName = ((RecNameTitle)cbSnapName.SelectedItem).Name;
      else
        loadParams.snapName = String.Empty;

      return loadParams;
    }

    /// <summary>
    /// Method is invoked asynchronously and performs the load of DR.
    /// </summary>
    /// <param name="loadParams">Object containing all necessary parameters for DR selection</param>
    /// <param name="asyncOp">AsyncOperation used to post callbacks</param>
    protected override void PerformAsyncDRLoad(DRLoadParameters loadParams, AsyncOperation asyncOp)
    {
      try
      {
        //Document for each record category
        FileInfo fileEvents = null;
        FileInfo fileTraces = null;
        FileInfo fileSnaps = null;
        Guid? versionID = null;
        Guid? instanceID = null;
        //Cast parameters object to proper type
        DRLoadParametersMDS parameters = loadParams as DRLoadParametersMDS;
        //Clear old data from dataset
        asyncOp.Post(onInfoMessageDelegate, "Clearing old records from dataset");
        InitiateDataSet();
        //Log unit address
        asyncOp.Post(onInfoMessageDelegate, "Uploading data from " + parameters.tuAddress);
        if (parameters.selectEvents)
        { //Select event records
          asyncOp.Post(onInfoMessageDelegate, "Selecting event records");
          fileEvents = DownloadEventRecords(parameters);
          asyncOp.Post(onInfoMessageDelegate, "Event records selected");
        }
        if (parameters.selectTraces)
        { //Select trace records
          asyncOp.Post(onInfoMessageDelegate, "Selecting trace records");
          fileTraces = DownloadTraceRecords(parameters);
          asyncOp.Post(onInfoMessageDelegate, "Trace records selected");
        }
        if (parameters.selectSnaps)
        { //Select snap records
          asyncOp.Post(onInfoMessageDelegate, "Selecting snap records");
          fileSnaps = DownloadSnapRecords(parameters);
          asyncOp.Post(onInfoMessageDelegate, "Snap records selected");
        }
        if (fileEvents != null)
        { //Load event records to dataset
          asyncOp.Post(onInfoMessageDelegate, "Loading event records to dataset");
          LoadRecords(fileEvents, parameters.timeOffset, ref versionID, ref instanceID);
          asyncOp.Post(onInfoMessageDelegate, "Event records loaded");
        }
        if (fileTraces != null)
        { //Load trace records to dataset
          asyncOp.Post(onInfoMessageDelegate, "Loading trace records to dataset");
          LoadRecords(fileTraces, parameters.timeOffset, ref versionID, ref instanceID);
          asyncOp.Post(onInfoMessageDelegate, "Trace records loaded");
        }
        if (fileSnaps != null)
        { //Load snap records to dataset
          asyncOp.Post(onInfoMessageDelegate, "Loading snap records to dataset");
          LoadRecords(fileSnaps, parameters.timeOffset, ref versionID, ref instanceID);
          asyncOp.Post(onInfoMessageDelegate, "Snap records loaded");
        }
        //Check if DDD Version exists
        if (versionID.HasValue && !DDDHelper.CheckVersionExists(versionID.Value))
          CheckAndGetDDDVersion(parameters.tuAddress, asyncOp);
        //Check if TU Instance exists
        if (instanceID.HasValue && !DDDHelper.CheckInstanceExists(instanceID.Value))
          CheckAndRegisterTUInstance(parameters.tuAddress, versionID.Value, asyncOp);
        //Walk through all records in dataset and add attributes from DDD Objects
        asyncOp.Post(onInfoMessageDelegate, "Updating record attributes from configuration");
        foreach (DiagnosticRecords.RecordsRow row in diagRecords.Records)
          FillCommonRecAttributes(row);

        //Post completed event
        AsyncCompletedEventArgs result = new AsyncCompletedEventArgs(null, false, null);
        asyncOp.PostOperationCompleted(onDRLoadCompletedDelegate, result);
      }
      catch (Exception ex)
      {
        //Post completed event with error
        AsyncCompletedEventArgs result = new AsyncCompletedEventArgs(ex, false, null);
        asyncOp.PostOperationCompleted(onDRLoadCompletedDelegate, result);
      }
    }

    /// <summary>
    /// Saves settings of selector to file
    /// </summary>
    /// <param name="file">Stream to save to</param>
    /// <param name="formatter">Formatter used for saving objects</param>
    public override void SaveSettingsToFile(FileStream file, BinaryFormatter formatter)
    {
      //Save time range
      formatter.Serialize(file, dtInterval.FromSelected);
      formatter.Serialize(file, dtInterval.TimeFrom);
      formatter.Serialize(file, dtInterval.ToSelected);
      formatter.Serialize(file, dtInterval.TimeTo);
      formatter.Serialize(file, dtInterval.PredefIntervalIndex);
      //Save TU address
      formatter.Serialize(file, cbTUAddress.Text);
      //Save all stored TU addresses
      formatter.Serialize(file, cbTUAddress.Items.Count);
      foreach(String item in cbTUAddress.Items)
        formatter.Serialize(file, cbTUAddress.Items.Count);
      //Time offset
      formatter.Serialize(file, nudMDSTimeOffset.Value);
      //Event record parameters
      formatter.Serialize(file, chbSelectEvents.Checked);
      formatter.Serialize(file, cbEventName.Items.Count);
      foreach (RecNameTitle item in cbEventName.Items)
        formatter.Serialize(file, item);
      formatter.Serialize(file, cbEventName.SelectedIndex);
      formatter.Serialize(file, chbSevA1.Checked);
      formatter.Serialize(file, chbSevA.Checked);
      formatter.Serialize(file, chbSevB1.Checked);
      formatter.Serialize(file, chbSevB.Checked);
      formatter.Serialize(file, chbSevC.Checked);
      formatter.Serialize(file, tbDeviceCode.Text);
      formatter.Serialize(file, nudVehicleNumber.Value);
      formatter.Serialize(file, nudMaxEvents.Value);
      //Trace record parameters
      formatter.Serialize(file, chbSelectTraces.Checked);
      formatter.Serialize(file, cbTraceName.Items.Count);
      foreach (RecNameTitle item in cbTraceName.Items)
        formatter.Serialize(file, item);
      formatter.Serialize(file, cbTraceName.SelectedIndex);
      formatter.Serialize(file, nudMaxTraces.Value);
      //Snap record parameters
      formatter.Serialize(file, chbSelectSnaps.Checked);
      formatter.Serialize(file, cbSnapName.Items.Count);
      foreach (RecNameTitle item in cbSnapName.Items)
        formatter.Serialize(file, item);
      formatter.Serialize(file, cbSnapName.SelectedIndex);
    }

    /// <summary>
    /// Loads settings of selector from file
    /// </summary>
    /// <param name="file">Stream to save to</param>
    /// <param name="formatter">Formatter used for saving objects</param>
    public override void LoadSettingsFromFile(FileStream file, BinaryFormatter formatter)
    {
      //Load time range
      dtInterval.FromSelected = (bool)formatter.Deserialize(file);
      dtInterval.TimeFrom = (DateTime)formatter.Deserialize(file);
      dtInterval.ToSelected = (bool)formatter.Deserialize(file);
      dtInterval.TimeTo = (DateTime)formatter.Deserialize(file);
      dtInterval.PredefIntervalIndex = (int)formatter.Deserialize(file);
      //Load TU address
      cbTUAddress.Text = (String)formatter.Deserialize(file);
      //Load all stored TU addresses
      int addrCount = (int)formatter.Deserialize(file);
      for(int i=0; i<addrCount; i++)
        cbTUAddress.Items.Add(formatter.Deserialize(file));

      //Time offset
      nudMDSTimeOffset.Value = (decimal)formatter.Deserialize(file);
      //Event record parameters
      chbSelectEvents.Checked = (bool)formatter.Deserialize(file);
      int itemCount = (int)formatter.Deserialize(file);
      for (int i = 0; i < itemCount; i++)
        cbEventName.Items.Add((RecNameTitle)formatter.Deserialize(file));
      int index = (int)formatter.Deserialize(file);
      if (index != -1)
        cbEventName.SelectedIndex = index;
      chbSevA1.Checked = (bool)formatter.Deserialize(file);
      chbSevA.Checked = (bool)formatter.Deserialize(file);
      chbSevB1.Checked = (bool)formatter.Deserialize(file);
      chbSevB.Checked = (bool)formatter.Deserialize(file);
      chbSevC.Checked = (bool)formatter.Deserialize(file);
      tbDeviceCode.Text = (string)formatter.Deserialize(file);
      nudVehicleNumber.Value = (decimal)formatter.Deserialize(file);
      nudMaxEvents.Value = (decimal)formatter.Deserialize(file);

      //Trace record parameters
      chbSelectTraces.Checked = (bool)formatter.Deserialize(file);
      itemCount = (int)formatter.Deserialize(file);
      for (int i = 0; i < itemCount; i++)
        cbTraceName.Items.Add((RecNameTitle)formatter.Deserialize(file));
      index = (int)formatter.Deserialize(file);
      if (index != -1)
        cbTraceName.SelectedIndex = index;
      nudMaxTraces.Value = (decimal)formatter.Deserialize(file);
      //Snap record parameters
      chbSelectSnaps.Checked = (bool)formatter.Deserialize(file);
      itemCount = (int)formatter.Deserialize(file);
      for (int i = 0; i < itemCount; i++)
        cbSnapName.Items.Add((RecNameTitle)formatter.Deserialize(file));
      index = (int)formatter.Deserialize(file);
      if (index != -1)
        cbSnapName.SelectedIndex = index;
    }
    #endregion //Virtual method overrides

    #region Helper methods
    /// <summary>
    /// Downloads file obtained from given URL into temporary file.
    /// File is created in temporary directory with unique generated name, 
    /// Returns FileInfo object with downloaded file
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>
    private FileInfo DownloadDataToTmpFile(String url)
    {
      FileInfo tmpFile = null;
      try
      { //Create tmp file
        tmpFile = new FileInfo(Path.GetTempFileName());
        //Create web client
        using (WebClient client = new WebClient())
        { //Download file from given URL
          client.DownloadFile(url, tmpFile.FullName);
        }
        //Return reference to downloaded file
        return tmpFile;
      }
      catch (Exception ex)
      { //Delete temporary file, if created
        if (tmpFile != null)
          tmpFile.Delete();
        throw new Exception("Failed to download data into temporary file", ex);
      }
    }

    /// <summary>
    /// Obtain XML document from given URL.
    /// Downloads gziped document into tmp file.
    /// Loads the file into document.
    /// Deletes downloaded tmp file.
    /// </summary>
    /// <param name="address">URL where to obtain the document</param>
    /// <returns>Downloaded XML document</returns>
    private XmlDocument DownloadXMLDocument(String url)
    {
      try
      { //Prepare output XML document
        XmlDocument xmlDoc = new XmlDocument();
        //Download temporary file
        FileInfo tmpFile = DownloadDataToTmpFile(url);
        //Load data from temporary file
        using (FileStream fileStream = tmpFile.OpenRead())
        { //Open file for reading
          using (GZipStream zipStream = new GZipStream(fileStream, CompressionMode.Decompress))
          { //Unzip the file
            xmlDoc.Load(zipStream); //Load the file into XML document
          }
        }
        //Delete temporary file
        tmpFile.Delete();
        //Return the document
        return xmlDoc;
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to download XML document from " + url, ex);
      }
    }

    /// <summary>
    /// Loads known TU Addresses from XML file into combo box
    /// </summary>
    /// <returns></returns>
    private void LoadTUAddresses()
    {
      try
      {
        tuAddrDoc = new XmlDocument();
        //Load XML file with TU addresses
        tuAddrDoc.Load(Path.Combine(Application.StartupPath, Resource.TUAddressesFileName));
        //Clear combo box with TU Addresses
        cbTUAddress.Items.Clear();
        //Obtain list of all Address elements
        XmlNodeList addrNodes = tuAddrDoc.GetElementsByTagName("Address");
        //Fill in addresses into combo box
        foreach (XmlElement adrElem in addrNodes)
          cbTUAddress.Items.Add(adrElem.InnerText);
      }
      catch (Exception ex)
      {
        OnErrorMessage(new Exception("Failed to load list of TU address", ex));
      }
    }

    /// <summary>
    /// Checks if given TU address exists in known addresses
    /// If not, it is added to combo box and stored in XML file
    /// </summary>
    /// <param name="address"></param>
    private void StoreTUAddress(String address)
    {
      try
      {
        //First check if address already exists in combo box
        if (cbTUAddress.FindStringExact(address) > -1)
          return; //address already exists
        //Check if address does not exist in the document
        String expr = String.Format("./Address['{0}']", address);
        if (tuAddrDoc.SelectSingleNode(expr) == null)
        { //Element with specified address does not exist in the document
          //Add new element to the doument
          XmlElement addrElem = tuAddrDoc.CreateElement("Address");
          addrElem.InnerText = address;
          //Append element
          tuAddrDoc.DocumentElement.AppendChild(addrElem);
          //Save the document back to XML file
          tuAddrDoc.Save(Path.Combine(Application.StartupPath, Resource.TUAddressesFileName));
        }
        //Add address to combo box
        cbTUAddress.Items.Add(address);
      }
      catch (Exception ex)
      {
        OnErrorMessage(new Exception("Failed to store TU address", ex));
      }
    }

    /// <summary>
    /// Check DDD version downloaded in device with given address.
    /// In case of unknown DDD version ID, download and register the DDD version to DDDHelper.
    /// </summary>
    /// <param name="address">Address of the unit (device)</param>
    /// <param name="asyncOp">AsyncOperation used to post callbacks</param>
    /// <returns>ID of the DDD version in the device</returns>
    private Guid CheckAndGetDDDVersion(String address, AsyncOperation asyncOp)
    {
      Guid versionID;
      String userVersion;
      try
      { //Obtain DDD Version Info document
        String url = String.Format(UrlDDDVer, address);
        XmlDocument doc = DownloadXMLDocument(url);
        //Get VersioID from the document
        versionID = new Guid(doc.DocumentElement.GetAttribute("VersionID"));
        userVersion = doc.DocumentElement.GetAttribute("UserVersion");
        //Check if version is already registered in DDD Helper
        if (DDDHelper.CheckVersionExists(versionID))
        { //Known version
          if (asyncOp != null)
            asyncOp.Post(onInfoMessageDelegate, "Known DDD version " + userVersion);
          return versionID; //Version already exists in repository
        }
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to obtain DDD version information", ex);
      }
      //Version has to be downloaded and imported into the repository
      try
      {
        if (asyncOp != null)
          asyncOp.Post(onInfoMessageDelegate, "Importing DDD version " + userVersion);
        //Prepare URL for download
        String url = String.Format(UrlDDD, address);
        //Obtain DDD document
        XmlDocument doc = DownloadXMLDocument(url);
        //Parse DDD document
        DDDVersion version = DDDRepository.CreateObjectsFromDDDXML(doc, versionID);
        //Register version in helper
        DDDHelper.AddDDDVersion(version);
        if (asyncOp != null)
          asyncOp.Post(onInfoMessageDelegate, "DDD version imported");
      }
      catch (Exception ex)
      { 
        throw new Exception("Failed to import DDD version", ex);
      }
      //Return ID of running DDD version
      return versionID;
    }

    /// <summary>
    /// Check if ID of TUInstance with given address exists in DDD Helper. 
    /// Add TUInstance to the helper if necessary.
    /// </summary>
    /// <param name="address">Address of the device (unit)</param>
    /// <param name="asyncOp">AsyncOperation used to post callbacks</param>
    /// <param name="dddVersionID">ID of ddd version running in the device</param>
    private void CheckAndRegisterTUInstance(String address, Guid dddVersionID, AsyncOperation asyncOp)
    {
      try
      { //Obtain TUInstance info document
        String url = String.Format(UrlTUInfo, address);
        XmlDocument doc = DownloadXMLDocument(url);
        //Get TUType element from the documen
        XmlElement tuTypeElem = (XmlElement)doc.DocumentElement.SelectSingleNode("TUType");
        //Get type ID
        Guid typeID = new Guid(tuTypeElem.GetAttribute("TypeID"));
        //Create type object
        TUType tuType = new TUType(typeID, tuTypeElem.GetAttribute("Name"), tuTypeElem.GetAttribute("Comment"));
        //Check if Type is already registered in DDD Helper
        if (!DDDHelper.CheckTypeExists(typeID))
        {
          DDDHelper.AddTUType(tuType);
          if (asyncOp != null)
            asyncOp.Post(onInfoMessageDelegate, "Registered TU type " + tuType.TypeName);
        }
        else if (asyncOp != null)
          asyncOp.Post(onInfoMessageDelegate, "Known TU type " + tuType.TypeName);
        //Get instance ID
        Guid tuInstanceID = new Guid(doc.DocumentElement.GetAttribute("InstanceID"));
        String tuName = doc.DocumentElement.GetAttribute("Name");
        //Check if instance exists in DDD Helper
        if (!DDDHelper.CheckInstanceExists(tuInstanceID))
        { //Add instance to the repository
          TUInstance tuInstance = new TUInstance(tuInstanceID, dddVersionID,
          tuName, 0, doc.DocumentElement.GetAttribute("Comment"), tuType);
          DDDHelper.AddTUInstance(tuInstance);
          if (asyncOp != null)
            asyncOp.Post(onInfoMessageDelegate, "Registered TU instance " + tuName);
        }
        else if (asyncOp != null)
          asyncOp.Post(onInfoMessageDelegate, "Known TU instance " + tuName);
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to obtain unit type and instance ID", ex);
      }
    }

    /// <summary>
    /// Download event records from device (unit) according to given parameters
    /// </summary>
    /// <param name="parameters">Parameters used to select records from device</param>
    /// <returns>File containing downloaded records in the form of XML document</returns>
    private FileInfo DownloadEventRecords(DRLoadParametersMDS parameters)
    {
      try
      {
        QueryString url = new QueryString(String.Format(UrlExport, parameters.tuAddress));
        url["mode"] = "events";
        url["noddd"] = "";
        if (parameters.timeFrom != String.Empty)
          url["from"] = parameters.timeFrom;
        if (parameters.timeTo != String.Empty)
          url["to"] = parameters.timeTo;
        return DownloadDataToTmpFile(url.All);
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to download event records", ex);
      }
    }

    /// <summary>
    /// Download trace records from device (unit) according to given parameters
    /// </summary>
    /// <param name="parameters">Parameters used to select records from device</param>
    /// <returns>File containing downloaded records in the form of XML document</returns>
    private FileInfo DownloadTraceRecords(DRLoadParametersMDS parameters)
    {
      try
      {
        QueryString url = new QueryString(String.Format(UrlExport, parameters.tuAddress));
        url["mode"] = "trace";
        url["noddd"] = "";
        url["trace"] = parameters.traceName;
        //url["startid"] = "0";
        return DownloadDataToTmpFile(url.All);
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to download trace records", ex);
      }
    }

    /// <summary>
    /// Download snap records from device (unit) according to given parameters
    /// </summary>
    /// <param name="parameters">Parameters used to select records from device</param>
    /// <returns>File containing downloaded records in the form of XML document</returns>
    private FileInfo DownloadSnapRecords(DRLoadParametersMDS parameters)
    {
      try
      {
        QueryString url = new QueryString(String.Format(UrlExport, parameters.tuAddress));
        url["mode"] = "snap";
        url["noddd"] = "";
        url["snap"] = parameters.snapName;
        return DownloadDataToTmpFile(url.All);
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to download snap records", ex);
      }
    }

    /// <summary>
    /// Load records from temporary file into dataset.
    /// Returns id of DDD Version and TUInstance from tmp file.
    /// Deletes the file once records are loaded.
    /// </summary>
    private void LoadRecords(FileInfo file, int timeOffsetH, ref Guid? versionID, ref Guid? instanceID)
    {
      try
      { 
        if (file == null)
          return; //No file specified
        //Check file length
        if (file.Length != 0)
        { //File has non zero length
          using (FileStream stream = file.OpenRead())
          { //Open file for reading
            //Open record serializer
            DRSerializerXML recSer = new DRSerializerXML(DDDHelper, stream, false);
            //Obtain version and instance ID
            versionID = recSer.DDDVersionID;
            instanceID = recSer.TUInstanceID;
            //Read all available records from downloaded document
            DRSerializer.DiagnosticRecord record;
            DiagnosticRecords.RecordsRow recRow;
            while ((record = recSer.ReadRecordFromStream()) != null)
              diagRecords.Records.AddRecordsRow(
                record.TUInstanceID, record.RecordInstID, record.DDDVersionID,
                record.RecordDefID, DDDHelper.UTCToUserTime(record.StartTime),
                (record.EndTime != DateTime.MinValue) ? DDDHelper.UTCToUserTime(record.EndTime) : record.EndTime, 
                (record.LastModified != DateTime.MinValue) ? DDDHelper.UTCToUserTime(record.LastModified) : record.LastModified,
                (record.Source != String.Empty && record.DeviceCode == -1) ? record.Source : null, 
                record.HierarchyItemID, record.ImportTime, 
                (record.AcknowledgeTime != DateTime.MinValue) ? DDDHelper.UTCToUserTime(record.AcknowledgeTime) : record.AcknowledgeTime,
                record.VehicleNumber, record.DeviceCode,
                record.ParseStatusID, null, null, null, null, 0, null, null, null, null, null, 0, 0,
                record.BigEndian, record.BinData, 0, 0);              
          }
        }
        //Delete the temporary file
        file.Delete();
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to load records to dataset", ex);
      }
    }
    #endregion //Helper methods

    /// <summary>
    /// Check whether pressed key represents allowed character.
    /// If not, event is discarded.
    /// Used for text boxes which accept only characters [0-9] 
    /// </summary>
    private void NumericTextBox_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (NumTbTemplate.IndexOf(e.KeyChar) == -1 && e.KeyChar != '\b')
        e.Handled = true; //Character is not in template string -> it is invalid
    }

    /// <summary>
    /// Fill in names for known record types into combo boxes.
    /// Obtains DDD version running in MDS and uses to fill in record names.
    /// </summary>
    private void btGetRecNames_Click(object sender, EventArgs e)
    {
      try
      { //Clear combo boxes with names
        cbEventName.Items.Clear();
        cbTraceName.Items.Clear();
        cbSnapName.Items.Clear();
        //Obtain DDD version from Device
        Guid versionID = CheckAndGetDDDVersion(cbTUAddress.Text, null);
        DDDVersion version = DDDHelper.GetVersion(versionID);
        //Add representative for any record type
        cbEventName.Items.Add(new RecNameTitle(String.Empty, String.Empty));
        cbSnapName.Items.Add(new RecNameTitle(String.Empty, String.Empty));
        cbTraceName.Items.Add(new RecNameTitle(String.Empty, String.Empty));
        //Iterate over all records in dictionary
        foreach(DDDRecord record in version.Records.Values)
        { //Add record object to proper combo box
          if (record.GetType() == typeof(DDDEventRecord))
            cbEventName.Items.Add(new RecNameTitle(record.Name, record.Title));
          else if (record.GetType() == typeof(DDDTraceRecord))
            cbTraceName.Items.Add(new RecNameTitle(record.Name, record.Title));
          else if (record.GetType() == typeof(DDDSnapRecord))
            cbSnapName.Items.Add(new RecNameTitle(record.Name, record.Title));
        }
      }
      catch (Exception ex)
      { //Failed to obtain record names
        Exception err = new Exception("Failed to obtain record names", ex);
        OnErrorMessage(err);
      }
    }

    /// <summary>
    /// Check, if any address is selected. 
    /// If not disable select and get records button.
    /// </summary>
    private void cbTUAddress_TextChanged(object sender, EventArgs e)
    {
      if (cbTUAddress.Text == String.Empty)
      {
        btSelectRecords.Enabled = false;
        btGetRecNames.Enabled = false;
      }
      else
      {
        btSelectRecords.Enabled = true;
        btGetRecNames.Enabled = true;
      }
    }
  }

  #region Record name title class
  /// <summary>
  /// Class representing name and title of the record.
  /// Used to display record list in combobox
  /// </summary>
  [Serializable]
  class RecNameTitle
  {
    public String Name;
    public String Title;

    public RecNameTitle(String name, String title)
    {
      this.Name = name;
      this.Title = title;
    }

    /// <summary>
    /// Return record title as default implementation
    /// of to string method.
    /// </summary>
    public override string ToString()
    {
      return Title;
    }
  }
  #endregion

  #region Load parameters class
  /// <summary>
  /// Class representing parameters for DR Load operation.
  /// </summary>
  public class DRLoadParametersMDS : DRLoadParameters
  {
    public String tuAddress; //Address of Telediagnostic Unit
    public String timeFrom; //start of the interval
    public String timeTo; //end of the interval    
    public int timeOffset; //Time offset of MDS system to UTC
    
    public bool selectEvents; //Select event records
    public bool A1; //Severity A1
    public bool A;  //Severity A
    public bool B1; //Severity B1
    public bool B;  //Severity B
    public bool C;  //Severity C
    public int? deviceCode;
    public String eventName;
    public int? vehicleNumber;
    public int maxEvtRecords;

    public bool selectTraces; //Select trace records
    public String traceName;
    public int maxTraceRecords;

    public bool selectSnaps;  //Select snap records
    public String snapName;
  }
  #endregion //Load parameters class
}

