using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml;
using System.Threading;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using DRStatistics;
using DRQueries;

namespace StatSelectorComponents
{
  /// <summary>
  /// Component enabling to select diagnostic records based on TUInstance and time
  /// </summary>
  public partial class StatSelectorFile : StatSelectorBase
  {
    #region Public properties
    /// <summary>
    /// Name of selected file
    /// </summary>
    public String FileName
    {
      get { return tbFileName.Text; }
      set { tbFileName.Text = value; }
    }
    #endregion //Public properties

    #region Construction and initialization
    public StatSelectorFile() 
      : base() //Call base constructor
    {
      InitializeComponent();
    }
    #endregion //Construction and initialization

    #region Component event handlers
    private void btLoadFile_Click(object sender, EventArgs e)
    {
      //Start asynchronous record load
      LoadStatisticsAsync();
      //Set form title
      this.ParentForm.Text = tbFileName.Text;
    }

    /// <summary>
    /// Opens dialog for selecting files
    /// </summary>
    private void btSelectFile_Click(object sender, EventArgs e)
    {
      //Prepare load file dialog
      dlgOpenFile.Reset();
      dlgOpenFile.DefaultExt = DRSelectorComponents.DRSelectors.StatFileExtension ;
      dlgOpenFile.Filter = DRSelectorComponents.DRSelectors.StatFilesFilter;
      //Show file open dialog
      if (dlgOpenFile.ShowDialog() == DialogResult.OK)
      { //file was selected
        tbFileName.Text = dlgOpenFile.FileName;
        //Load records from file
        btLoadFile_Click(this, null);
      }
    }
    
    private void DRSelectorFile_Load(object sender, EventArgs e)
    {
      if (FileName == "")
        btSelectFile_Click(this, null); //No file selected, automatically open file dialog
      else
        btLoadFile_Click(this, null); //File already selected - load records from file
    }
    #endregion //Component event handlers

    #region Virtual method overrides
    protected override DRQueryHelper PrepareStatsQuery()
    {
 	    //Create stat query object just to hold file name
      DRQueryHelper query = new DRQueryHelper();
      query.FileName = FileName;
      return query;
    }

    protected override void PerformAsyncStatsLoad(DRQueryHelper statsQuery, AsyncOperation asyncOp)
    {
      try
      {
        //Check if file name is specified
        if (!File.Exists(statsQuery.FileName))
          throw new StatSelectorException("Invalid file name specified");
        //Post info message
        asyncOp.Post(onInfoMessageDelegate, "Loading statistics from file");
        //Try to load statistics from file
        try
        {
          LoadStatisticsFromFile(statsQuery.FileName);
        }
        catch (Exception)
        {
          throw;
        }
        //Post completed event
        AsyncCompletedEventArgs result = new AsyncCompletedEventArgs(null, false, null);
        asyncOp.PostOperationCompleted(onLoadCompletedDelegate, result);
      }
      catch (Exception ex)
      {
        //Post completed event with error
        AsyncCompletedEventArgs result = new AsyncCompletedEventArgs(ex, false, null);
        asyncOp.PostOperationCompleted(onLoadCompletedDelegate, result);
      }
    }

    /// <summary>
    /// Saves settings of selector to file
    /// </summary>
    /// <param name="file">Stream to save to</param>
    /// <param name="formatter">Formatter used for saving objects</param>
    public override void SaveSettingsToFile(FileStream file, BinaryFormatter formatter)
    {
      formatter.Serialize(file, tbFileName.Text);
    }

    /// <summary>
    /// Loads settings of selector from file
    /// </summary>
    /// <param name="file">Stream to save to</param>
    /// <param name="formatter">Formatter used for saving objects</param>
    public override void LoadSettingsFromFile(FileStream file, BinaryFormatter formatter)
    {
      tbFileName.Text = (String)formatter.Deserialize(file);
    }
    #endregion //Virtual method overrides

  }
}

