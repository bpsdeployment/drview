namespace DRSelectorComponents
{
  partial class DRSelectorDB
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btSelect = new System.Windows.Forms.Button();
      this.panel2 = new System.Windows.Forms.Panel();
      this.dtInterval = new DRSelectorComponents.TimePicker();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.gbRecType = new System.Windows.Forms.GroupBox();
      this.chbSnapRec = new System.Windows.Forms.CheckBox();
      this.chbTraceRec = new System.Windows.Forms.CheckBox();
      this.chbEventRec = new System.Windows.Forms.CheckBox();
      this.label1 = new System.Windows.Forms.Label();
      this.tbRecSources = new System.Windows.Forms.TextBox();
      this.tbTitleKeyWords = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.chbMatchAllKeywords = new System.Windows.Forms.CheckBox();
      this.hierarchyTUInstances = new DRSelectorComponents.FleetHierarchy_TUInstances();
      this.tbFaultCodes = new System.Windows.Forms.TextBox();
      this.tbTrcSnpCodes = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.tbVehicleNumbers = new System.Windows.Forms.TextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.chbSevA1 = new System.Windows.Forms.CheckBox();
      this.chbSevA = new System.Windows.Forms.CheckBox();
      this.chbSevB1 = new System.Windows.Forms.CheckBox();
      this.chbSevB = new System.Windows.Forms.CheckBox();
      this.chbSevC = new System.Windows.Forms.CheckBox();
      this.label6 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.tbFaultSubseverities = new System.Windows.Forms.TextBox();
      this.nudMaxRecords = new System.Windows.Forms.NumericUpDown();
      this.cbSortColumn = new System.Windows.Forms.ComboBox();
      this.cbSortDirection = new System.Windows.Forms.ComboBox();
      this.label8 = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.diagRecords)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.recordsView)).BeginInit();
      this.panel2.SuspendLayout();
      this.gbRecType.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudMaxRecords)).BeginInit();
      this.SuspendLayout();
      // 
      // btSelect
      // 
      this.btSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btSelect.Location = new System.Drawing.Point(461, 15);
      this.btSelect.Name = "btSelect";
      this.btSelect.Size = new System.Drawing.Size(75, 23);
      this.btSelect.TabIndex = 0;
      this.btSelect.Text = "Select";
      this.btSelect.UseVisualStyleBackColor = true;
      this.btSelect.Click += new System.EventHandler(this.btSelect_Click);
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.dtInterval);
      this.panel2.Controls.Add(this.btSelect);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel2.Location = new System.Drawing.Point(0, 329);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(543, 47);
      this.panel2.TabIndex = 9;
      // 
      // dtInterval
      // 
      this.dtInterval.FromSelected = true;
      this.dtInterval.Location = new System.Drawing.Point(3, -1);
      this.dtInterval.Name = "dtInterval";
      this.dtInterval.PredefIntervalIndex = 0;
      this.dtInterval.Size = new System.Drawing.Size(452, 45);
      this.dtInterval.TabIndex = 8;
      this.dtInterval.TimeFrom = new System.DateTime(2009, 1, 11, 14, 38, 57, 321);
      this.dtInterval.TimeTo = new System.DateTime(2009, 1, 11, 14, 38, 57, 321);
      this.dtInterval.ToSelected = true;
      // 
      // splitter1
      // 
      this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter1.Location = new System.Drawing.Point(0, 326);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(543, 3);
      this.splitter1.TabIndex = 10;
      this.splitter1.TabStop = false;
      // 
      // gbRecType
      // 
      this.gbRecType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.gbRecType.Controls.Add(this.chbSnapRec);
      this.gbRecType.Controls.Add(this.chbTraceRec);
      this.gbRecType.Controls.Add(this.chbEventRec);
      this.gbRecType.Location = new System.Drawing.Point(455, 206);
      this.gbRecType.Name = "gbRecType";
      this.gbRecType.Size = new System.Drawing.Size(82, 75);
      this.gbRecType.TabIndex = 11;
      this.gbRecType.TabStop = false;
      this.gbRecType.Text = "Record type";
      // 
      // chbSnapRec
      // 
      this.chbSnapRec.AutoSize = true;
      this.chbSnapRec.Location = new System.Drawing.Point(6, 54);
      this.chbSnapRec.Name = "chbSnapRec";
      this.chbSnapRec.Size = new System.Drawing.Size(51, 17);
      this.chbSnapRec.TabIndex = 2;
      this.chbSnapRec.Text = "Snap";
      this.chbSnapRec.UseVisualStyleBackColor = true;
      // 
      // chbTraceRec
      // 
      this.chbTraceRec.AutoSize = true;
      this.chbTraceRec.Location = new System.Drawing.Point(6, 35);
      this.chbTraceRec.Name = "chbTraceRec";
      this.chbTraceRec.Size = new System.Drawing.Size(54, 17);
      this.chbTraceRec.TabIndex = 1;
      this.chbTraceRec.Text = "Trace";
      this.chbTraceRec.UseVisualStyleBackColor = true;
      // 
      // chbEventRec
      // 
      this.chbEventRec.AutoSize = true;
      this.chbEventRec.Location = new System.Drawing.Point(6, 16);
      this.chbEventRec.Name = "chbEventRec";
      this.chbEventRec.Size = new System.Drawing.Size(54, 17);
      this.chbEventRec.TabIndex = 0;
      this.chbEventRec.Text = "Event";
      this.chbEventRec.UseVisualStyleBackColor = true;
      // 
      // label1
      // 
      this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(226, 206);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(85, 13);
      this.label1.TabIndex = 12;
      this.label1.Text = "Record sources:";
      // 
      // tbRecSources
      // 
      this.tbRecSources.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbRecSources.Location = new System.Drawing.Point(229, 222);
      this.tbRecSources.Name = "tbRecSources";
      this.tbRecSources.Size = new System.Drawing.Size(220, 20);
      this.tbRecSources.TabIndex = 13;
      // 
      // tbTitleKeyWords
      // 
      this.tbTitleKeyWords.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbTitleKeyWords.Location = new System.Drawing.Point(3, 222);
      this.tbTitleKeyWords.Name = "tbTitleKeyWords";
      this.tbTitleKeyWords.Size = new System.Drawing.Size(220, 20);
      this.tbTitleKeyWords.TabIndex = 14;
      // 
      // label2
      // 
      this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(0, 245);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(65, 13);
      this.label2.TabIndex = 15;
      this.label2.Text = "Fault codes:";
      // 
      // label3
      // 
      this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(0, 206);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(112, 13);
      this.label3.TabIndex = 17;
      this.label3.Text = "Record title keywords:";
      // 
      // chbMatchAllKeywords
      // 
      this.chbMatchAllKeywords.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.chbMatchAllKeywords.AutoSize = true;
      this.chbMatchAllKeywords.Location = new System.Drawing.Point(154, 205);
      this.chbMatchAllKeywords.Name = "chbMatchAllKeywords";
      this.chbMatchAllKeywords.Size = new System.Drawing.Size(69, 17);
      this.chbMatchAllKeywords.TabIndex = 18;
      this.chbMatchAllKeywords.Text = "Match all";
      this.chbMatchAllKeywords.UseVisualStyleBackColor = true;
      // 
      // hierarchyTUInstances
      // 
      this.hierarchyTUInstances.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.hierarchyTUInstances.Location = new System.Drawing.Point(0, 0);
      this.hierarchyTUInstances.MinimumSize = new System.Drawing.Size(195, 90);
      this.hierarchyTUInstances.Name = "hierarchyTUInstances";
      this.hierarchyTUInstances.Size = new System.Drawing.Size(543, 199);
      this.hierarchyTUInstances.TabIndex = 19;
      // 
      // tbFaultCodes
      // 
      this.tbFaultCodes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbFaultCodes.Location = new System.Drawing.Point(3, 261);
      this.tbFaultCodes.Name = "tbFaultCodes";
      this.tbFaultCodes.Size = new System.Drawing.Size(145, 20);
      this.tbFaultCodes.TabIndex = 20;
      this.tbFaultCodes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericTextBox_KeyPress);
      // 
      // tbTrcSnpCodes
      // 
      this.tbTrcSnpCodes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbTrcSnpCodes.Location = new System.Drawing.Point(154, 261);
      this.tbTrcSnpCodes.Name = "tbTrcSnpCodes";
      this.tbTrcSnpCodes.Size = new System.Drawing.Size(145, 20);
      this.tbTrcSnpCodes.TabIndex = 21;
      this.tbTrcSnpCodes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericTextBox_KeyPress);
      // 
      // label4
      // 
      this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(151, 245);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(110, 13);
      this.label4.TabIndex = 22;
      this.label4.Text = "Trace or Snap codes:";
      // 
      // tbVehicleNumbers
      // 
      this.tbVehicleNumbers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbVehicleNumbers.Location = new System.Drawing.Point(305, 261);
      this.tbVehicleNumbers.Name = "tbVehicleNumbers";
      this.tbVehicleNumbers.Size = new System.Drawing.Size(144, 20);
      this.tbVehicleNumbers.TabIndex = 23;
      this.tbVehicleNumbers.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericTextBox_KeyPress);
      // 
      // label5
      // 
      this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(302, 245);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(88, 13);
      this.label5.TabIndex = 24;
      this.label5.Text = "Vehicle numbers:";
      // 
      // chbSevA1
      // 
      this.chbSevA1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.chbSevA1.AutoSize = true;
      this.chbSevA1.Location = new System.Drawing.Point(6, 300);
      this.chbSevA1.Name = "chbSevA1";
      this.chbSevA1.Size = new System.Drawing.Size(39, 17);
      this.chbSevA1.TabIndex = 25;
      this.chbSevA1.Text = "A1";
      this.chbSevA1.UseVisualStyleBackColor = true;
      // 
      // chbSevA
      // 
      this.chbSevA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.chbSevA.AutoSize = true;
      this.chbSevA.Location = new System.Drawing.Point(43, 300);
      this.chbSevA.Name = "chbSevA";
      this.chbSevA.Size = new System.Drawing.Size(33, 17);
      this.chbSevA.TabIndex = 26;
      this.chbSevA.Text = "A";
      this.chbSevA.UseVisualStyleBackColor = true;
      // 
      // chbSevB1
      // 
      this.chbSevB1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.chbSevB1.AutoSize = true;
      this.chbSevB1.Location = new System.Drawing.Point(77, 300);
      this.chbSevB1.Name = "chbSevB1";
      this.chbSevB1.Size = new System.Drawing.Size(39, 17);
      this.chbSevB1.TabIndex = 27;
      this.chbSevB1.Text = "B1";
      this.chbSevB1.UseVisualStyleBackColor = true;
      // 
      // chbSevB
      // 
      this.chbSevB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.chbSevB.AutoSize = true;
      this.chbSevB.Location = new System.Drawing.Point(114, 300);
      this.chbSevB.Name = "chbSevB";
      this.chbSevB.Size = new System.Drawing.Size(33, 17);
      this.chbSevB.TabIndex = 28;
      this.chbSevB.Text = "B";
      this.chbSevB.UseVisualStyleBackColor = true;
      // 
      // chbSevC
      // 
      this.chbSevC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.chbSevC.AutoSize = true;
      this.chbSevC.Location = new System.Drawing.Point(145, 300);
      this.chbSevC.Name = "chbSevC";
      this.chbSevC.Size = new System.Drawing.Size(33, 17);
      this.chbSevC.TabIndex = 29;
      this.chbSevC.Text = "C";
      this.chbSevC.UseVisualStyleBackColor = true;
      // 
      // label6
      // 
      this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(0, 284);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(80, 13);
      this.label6.TabIndex = 30;
      this.label6.Text = "Fault severities:";
      // 
      // label7
      // 
      this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(181, 284);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(97, 13);
      this.label7.TabIndex = 31;
      this.label7.Text = "Fault subseverities:";
      // 
      // tbFaultSubseverities
      // 
      this.tbFaultSubseverities.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.tbFaultSubseverities.Location = new System.Drawing.Point(184, 298);
      this.tbFaultSubseverities.Name = "tbFaultSubseverities";
      this.tbFaultSubseverities.Size = new System.Drawing.Size(99, 20);
      this.tbFaultSubseverities.TabIndex = 32;
      this.tbFaultSubseverities.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericTextBox_KeyPress);
      // 
      // nudMaxRecords
      // 
      this.nudMaxRecords.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.nudMaxRecords.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudMaxRecords.Location = new System.Drawing.Point(305, 297);
      this.nudMaxRecords.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
      this.nudMaxRecords.Name = "nudMaxRecords";
      this.nudMaxRecords.Size = new System.Drawing.Size(89, 20);
      this.nudMaxRecords.TabIndex = 33;
      this.nudMaxRecords.Value = new decimal(new int[] {
            5000,
            0,
            0,
            0});
      // 
      // cbSortColumn
      // 
      this.cbSortColumn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.cbSortColumn.FormattingEnabled = true;
      this.cbSortColumn.Items.AddRange(new object[] {
            "Start Time",
            "End Time",
            "Acknowledge Time",
            "Import Time",
            "Last Modified",
            "Fault Code",
            "Trace or Snap Code",
            "Record Source",
            "Vehicle Number",
            "Device Code"});
      this.cbSortColumn.Location = new System.Drawing.Point(400, 297);
      this.cbSortColumn.Name = "cbSortColumn";
      this.cbSortColumn.Size = new System.Drawing.Size(86, 21);
      this.cbSortColumn.TabIndex = 34;
      // 
      // cbSortDirection
      // 
      this.cbSortDirection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.cbSortDirection.FormattingEnabled = true;
      this.cbSortDirection.Items.AddRange(new object[] {
            "ASC",
            "DESC"});
      this.cbSortDirection.Location = new System.Drawing.Point(492, 297);
      this.cbSortDirection.Name = "cbSortDirection";
      this.cbSortDirection.Size = new System.Drawing.Size(45, 21);
      this.cbSortDirection.TabIndex = 35;
      // 
      // label8
      // 
      this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(302, 284);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(92, 13);
      this.label8.TabIndex = 36;
      this.label8.Text = "Maximum records:";
      // 
      // label9
      // 
      this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(397, 284);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(43, 13);
      this.label9.TabIndex = 37;
      this.label9.Text = "Sort by:";
      // 
      // DRSelectorDB
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.Controls.Add(this.label9);
      this.Controls.Add(this.label8);
      this.Controls.Add(this.cbSortDirection);
      this.Controls.Add(this.cbSortColumn);
      this.Controls.Add(this.nudMaxRecords);
      this.Controls.Add(this.tbFaultSubseverities);
      this.Controls.Add(this.label7);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.chbSevC);
      this.Controls.Add(this.chbSevB);
      this.Controls.Add(this.chbSevB1);
      this.Controls.Add(this.chbSevA);
      this.Controls.Add(this.chbSevA1);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.tbVehicleNumbers);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.tbTrcSnpCodes);
      this.Controls.Add(this.tbFaultCodes);
      this.Controls.Add(this.hierarchyTUInstances);
      this.Controls.Add(this.chbMatchAllKeywords);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.tbTitleKeyWords);
      this.Controls.Add(this.tbRecSources);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.gbRecType);
      this.Controls.Add(this.splitter1);
      this.Controls.Add(this.panel2);
      this.Name = "DRSelectorDB";
      this.Size = new System.Drawing.Size(543, 376);
      this.Load += new System.EventHandler(this.DRSelectorDB_Load);
      ((System.ComponentModel.ISupportInitialize)(this.diagRecords)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.recordsView)).EndInit();
      this.panel2.ResumeLayout(false);
      this.gbRecType.ResumeLayout(false);
      this.gbRecType.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudMaxRecords)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btSelect;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Splitter splitter1;
    private TimePicker dtInterval;
    private System.Windows.Forms.GroupBox gbRecType;
    private System.Windows.Forms.CheckBox chbEventRec;
    private System.Windows.Forms.CheckBox chbSnapRec;
    private System.Windows.Forms.CheckBox chbTraceRec;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox tbRecSources;
    private System.Windows.Forms.TextBox tbTitleKeyWords;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.CheckBox chbMatchAllKeywords;
    private FleetHierarchy_TUInstances hierarchyTUInstances;
    private System.Windows.Forms.TextBox tbFaultCodes;
    private System.Windows.Forms.TextBox tbTrcSnpCodes;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox tbVehicleNumbers;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.CheckBox chbSevA1;
    private System.Windows.Forms.CheckBox chbSevA;
    private System.Windows.Forms.CheckBox chbSevB1;
    private System.Windows.Forms.CheckBox chbSevB;
    private System.Windows.Forms.CheckBox chbSevC;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.TextBox tbFaultSubseverities;
    private System.Windows.Forms.NumericUpDown nudMaxRecords;
    private System.Windows.Forms.ComboBox cbSortColumn;
    private System.Windows.Forms.ComboBox cbSortDirection;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label9;
  }
}
