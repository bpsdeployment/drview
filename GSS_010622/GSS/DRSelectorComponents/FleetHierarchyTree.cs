using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Data.SqlClient;
using DDDObjects;
using FleetHierarchies;
using DSDiagnosticRecords;

namespace DRSelectorComponents
{
  /// <summary>
  /// User control which displays fleet hierarchy loaded from CDDB in the form of tree.
  /// User may select desired hierarchy items using check boxes.
  /// </summary>
  public partial class FleetHierarchyTree : TriStateTreeView
  {
    #region Private members
    #endregion //Private members

    #region Public properties
    #endregion //Public properties

    #region Construction initialization
    public FleetHierarchyTree()
    {
      InitializeComponent();
      //Initialize parameters of TreeView
      this.ShowLines = true;
      this.ShowRootLines = true;
      this.ShowNodeToolTips = true;
    }
    #endregion //Construction initialization

    #region Public methods
    /// <summary>
    /// Shows current fleet hierarchy from given hierarchy helper
    /// in the tree view.
    /// In case of specified data view, 
    /// hierarchy is filtered by hierarchy items contained in records in given view.
    /// </summary>
    /// <param name="hierarchyHelper">Helper object containing hierarchy</param>
    /// <param name="recViewFilter">View of diagnostic records used to filter the hierarchy or null</param>
    public void ShowCurrentHierarchy(FleetHierarchyHelper hierarchyHelper, DataView recViewFilter)
    {
      try
      {
        //Obtain current hierarchy
        FleetHierarchy currentHierarchy = hierarchyHelper.ObtainCurrentHierarchy();
        //Build ID filter if data view is specified
        List<int> idFilter = null;
        if (recViewFilter != null)
          idFilter = BuildIDFilter(recViewFilter, currentHierarchy);


        TreeNode node = CreateHierarchyNode(currentHierarchy.TopItem, idFilter);
        //Set update mode for trre view
        this.BeginUpdate();
        //Clear treeview and append expanded node
        this.Nodes.Clear();
        if (node != null)
        { //Some node was created
          node.Expand();
          this.Nodes.Add(node);
          //Sort nodes
          this.Sort();
        }
        //End update mode
        this.EndUpdate();
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to display current fleet hierarchy", ex);
      }
    }

    /// <summary>
    /// Creates list of selected (checked) item names
    /// in the form of XML. Only top level checked items are 
    /// added into the list.
    /// </summary>
    /// <returns>Names of checked items in the form of XML</returns>
    public String GetSelectedNamesXML()
    {
      List<String> liNames = new List<string>();
      String[] arNames;
      String selNames = "";
      if (this.Nodes.Count > 0)
        GetSelectedNodeNames(this.Nodes[0], ref liNames);
      arNames = liNames.ToArray();
      selNames = String.Join(";", arNames);
      return FleetHierarchyHelper.GetHierarchyNamesXml(selNames);
    }

    /// <summary>
    /// Creates list of selected (checked) item IDs.
    /// </summary>
    /// <returns>List of item IDs</returns>
    public List<int> GetSelectedIDs()
    {
      List<int> liIDs = new List<int>();
      if (this.Nodes.Count > 0)
        GetSelectedNodeIDs(this.Nodes[0], ref liIDs);
      return liIDs;
    }

    /// <summary>
    /// Checks nodes representing hierarchy items with names according to
    /// given XML document
    /// </summary>
    /// <param name="selItemsXML"></param>
    public void CheckHierarchyItems(String selItemsXML)
    {
      //Uncheck all nodes first
      foreach (TreeNode node in this.Nodes)
        this.SetChecked(node, CheckState.Unchecked);

      //Check for empty or null XML
      if (selItemsXML == null || selItemsXML == String.Empty)
        return;

      //Create XML document from given string
      XmlDocument doc = new XmlDocument();
      doc.LoadXml(selItemsXML);

      //Select all item elements
      XmlNodeList items = doc.GetElementsByTagName("item");
      foreach (XmlElement item in items)
      { //Iterate over all items
        String name = item.GetAttribute("name");
        if (name == null || name == String.Empty)
          continue;
        //Find coresponding node
        TreeNode[] nodes = this.Nodes.Find(name, true);
        //Check node
        if (nodes != null && nodes.Length > 0)
        {
          this.SetChecked(nodes[0], CheckState.Checked);
          //Expand all the way to the parent
          TreeNode parent = nodes[0];
          while ((parent = parent.Parent) != null)
            parent.Expand();
        }
      }
    }

    /// <summary>
    /// Expand all checked and undetermined nodes
    /// </summary>
    public void ExpandCheckedNodes()
    {
      if (this.Nodes.Count > 0)
        ExpandChecked(this.Nodes[0]);
    }
    #endregion //Public methods

    #region Helper methods
    /// <summary>
    /// Creates tree view node from given hierarchy item.
    /// Method is called recursively for each child item.
    /// </summary>
    /// <param name="hierarchyItem">Part of the fleet hierarchy in the form hierarchy item object</param>
    /// <param name="idFilter">List of item ID used to filter the tree or null</param>
    /// <returns>Tree view node created from hierarchy item</returns>
    protected TreeNode CreateHierarchyNode(FleetHierarchyItem hierarchyItem, List<int> idFilter)
    {
      //Check the filter
      if (idFilter != null && idFilter.BinarySearch(hierarchyItem.ItemID) < 0)
        return null; //no node created - filtered out

      //Create tree node according to hierarchy item
      TreeNode node = new TreeNode();
      //Set node properties
      node.Name = hierarchyItem.Name;
      node.Text = hierarchyItem.Name;
      node.ToolTipText = hierarchyItem.Description;
      node.Tag = hierarchyItem.ItemID;
      if (hierarchyItem.ChildItems.Count > 0)
        node.NodeFont = new Font(this.Font, FontStyle.Bold);
      //Append child nodes from child hierarchy items
      TreeNode childNode;
      foreach (FleetHierarchyItem child in hierarchyItem.ChildItems)
        if ((childNode = CreateHierarchyNode(child, idFilter)) != null)
          node.Nodes.Add(childNode);
      //Return created node
      return node;
    }

    /// <summary>
    /// Creates list of selected node names from given node and its descendants.
    /// Method is called recursively, until first selected node is reached.
    /// </summary>
    /// <param name="node">Node where to start searching for selected nodes</param>
    /// <param name="selNames">List of selected names</param>
    protected void GetSelectedNodeNames(TreeNode node, ref List<String> selNames)
    {
      if (GetChecked(node) == CheckState.Checked)
      { //Node is checked, append name to list and return
        selNames.Add(node.Name);
        return;
      }
      //Call recursively for all children
      foreach (TreeNode child in node.Nodes)
        GetSelectedNodeNames(child, ref selNames);
    }

    /// <summary>
    /// Creates list of selected node IDs from given node and its descendants.
    /// Method is called recursively.
    /// </summary>
    /// <param name="node">Node where to start searching for selected nodes</param>
    /// <param name="selNames">List of selected IDs</param>
    protected void GetSelectedNodeIDs(TreeNode node, ref List<int> selIDs)
    {
      if (GetChecked(node) == CheckState.Checked)
      { //Node is checked, append name to list
        selIDs.Add((int)node.Tag);
      }
      //Call recursively for all children
      foreach (TreeNode child in node.Nodes)
        GetSelectedNodeIDs(child, ref selIDs);
    }

    /// <summary>
    /// Expand given node if not uncheked and call recursively for all chilren
    /// </summary>
    /// <param name="node">Node to expand</param>
    protected void ExpandChecked(TreeNode node)
    {
      if (GetChecked(node) != CheckState.Unchecked)
        node.Expand(); //Expand checked and undetermined
      //Call recursively for all children
      foreach (TreeNode child in node.Nodes)
        ExpandChecked(child);
    }

    /// <summary>
    /// Method creates sorted list of all hierarchy item IDs
    /// which are containe in given view of diagnostic records and
    /// all their parents.
    /// </summary>
    /// <param name="filterRecView">View of diagnostic records from which to build the filter</param>
    /// <param name="fleetHierarchy">Fleet hierarchy object</param>
    /// <returns>List of hierarchy item IDs by which the hierarchy shall be filtered</returns>
    protected List<int> BuildIDFilter(DataView filterRecView, FleetHierarchy fleetHierarchy)
    {
      //Create list of hierarchy tem IDs contained in given view and their parents
      List<int> itemIDs = new List<int>();
      int itemIndex = 0;
      foreach (DataRowView rowView in filterRecView)
      {
        DiagnosticRecords.RecordsRow recRow = (DiagnosticRecords.RecordsRow)rowView.Row;
        if (recRow.IsHierarchyItemIDNull() || recRow.HierarchyItemID < 0)
          continue; //No hierarchy item ID specified
        //Item ID not yet in the list, add ID of item and all parents
        FleetHierarchyItem hierItem = fleetHierarchy.GetHierarchyItem(recRow.HierarchyItemID);
        while (hierItem != null)
          if ((itemIndex = itemIDs.BinarySearch(hierItem.ItemID)) < 0)
          { //Item id not in the list yet
            itemIDs.Insert(~itemIndex, hierItem.ItemID);
            hierItem = hierItem.Parent; //Continue with parent
          }
          else
            break; //ID in the list, no need to continue with parents
      }
      //Return list of itemIDs
      return itemIDs;
    }
    #endregion //Helper methods
  }
}
