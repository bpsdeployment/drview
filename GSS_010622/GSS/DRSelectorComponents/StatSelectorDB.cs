using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using DDDObjects;
using DRQueries;
using DRStatistics;

namespace StatSelectorComponents
{
  /// <summary>
  /// Statistical selector.
  /// Record parameters specified as conditions.
  /// </summary>
  public partial class StatSelectorDB : StatSelectorBase
  {
    #region Static members
    private static String NumTbTemplate = "0123456789, ";
    #endregion 

    #region Private members
    SqlCommand statCommand; //Statistical command
    #endregion //Private members

    #region Construction, initialization
    /// <summary>
    /// Standard constructor
    /// </summary>
    public StatSelectorDB()
    {
      InitializeComponent();
      statCommand = new SqlCommand();
      statCommand.CommandType = CommandType.Text;
    }
    #endregion //Construction, initialization

    #region Event handlers
    private void StatSelectorDB_Load(object sender, EventArgs e)
    {
      cbGroup1.SelectedIndex = 0;
      RefreshHierarchyAndInstances();
    }

    private void cbGroup1_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (cbGroup1.SelectedIndex > 0)
        cbGroup2.Enabled = true;
      else
      {
        cbGroup2.Enabled = false;
        cbGroup2.SelectedIndex = 0;
      }
    }

    private void cbGroup2_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (cbGroup2.SelectedIndex > 0)
        cbGroup3.Enabled = true;
      else
      {
        cbGroup3.Enabled = false;
        cbGroup3.SelectedIndex = 0;
      }
    }

    private void cbGroup3_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    private void btSelect_Click(object sender, EventArgs e)
    {
      LoadStatisticsAsync();
    }

    /// <summary>
    /// Check whether pressed key represents allowed character.
    /// If not, event is discarded.
    /// Used for text boxes which accept only characters [0-9, ] 
    /// </summary>
    private void NumericTextBox_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (NumTbTemplate.IndexOf(e.KeyChar) == -1 && e.KeyChar != '\b')
        e.Handled = true; //Character is not in template string -> it is invalid
    }
    #endregion //Event handlers

    #region Helper functions
    /// <summary>
    /// Selects list of all TU Instances from DB and fills the grid
    /// </summary>
    protected void RefreshHierarchyAndInstances()
    {
      try
      {
        //Set proper connection string to DDD Helper
        if (dbConnection != null)
          dddHelper.ConnectionString = dbConnection.ConnectionString;

        hierarchyTUInstances.FleetHierarchy.ShowCurrentHierarchy(dddHelper.HierarchyHelper, null);
        hierarchyTUInstances.TUInstancesList.ShowTUInstances(dddHelper);
      }
      catch (Exception ex)
      {
        OnErrorMessage(new StatSelectorException(ex, "Failed to load fleet hierarchy or TU Instances list"));
      }
    }

    /// <summary>
    /// Obtain type of statistics group from selected item index in combo box
    /// </summary>
    /// <returns>Type of statistic group</returns>
    protected DRStatsHelper.eStatGroup GetStatGroup(int selectedItem)
    {
      switch (selectedItem)
      {
        case 1:
          return DRStatsHelper.eStatGroup.Train;
        case 2:
          return DRStatsHelper.eStatGroup.Vehicle;
        case 3:
          return DRStatsHelper.eStatGroup.Year;
        case 4:
          return DRStatsHelper.eStatGroup.Month;
        case 5:
          return DRStatsHelper.eStatGroup.Day;
        case 6:
          return DRStatsHelper.eStatGroup.Hour;
        case 7:
          return DRStatsHelper.eStatGroup.FaultCode;
        case 8:
          return DRStatsHelper.eStatGroup.TrcSnpCode;
        case 9:
          return DRStatsHelper.eStatGroup.Source;
        case 10:
          return DRStatsHelper.eStatGroup.DeviceCode;
        case 11:
          return DRStatsHelper.eStatGroup.VehicleNumber;
        case 12:
          return DRStatsHelper.eStatGroup.FaultSeverity;
        case 13:
          return DRStatsHelper.eStatGroup.FaultSubSeverity;
        case 14:
          return DRStatsHelper.eStatGroup.RecordType;
        case 15:
          return DRStatsHelper.eStatGroup.TUInstance;
        case 16:
          return DRStatsHelper.eStatGroup.TUType;
      }
      throw new StatSelectorException("Invalid type of group selected with index {0}", selectedItem);
    }
    #endregion //Helper functions

    #region Virtual methods overrides
    /// <summary>
    /// Build and return statistical query according to user set parameters
    /// </summary>
    /// <returns></returns>
    protected override DRQueryHelper PrepareStatsQuery()
    {
      //Add groups to statistics helper
      StatisticsHelper.ClearGroups();
      if (cbGroup1.SelectedIndex > 0)
        StatisticsHelper.AddGroup(GetStatGroup(cbGroup1.SelectedIndex));
      if (cbGroup2.SelectedIndex > 0)
        StatisticsHelper.AddGroup(GetStatGroup(cbGroup2.SelectedIndex));
      if (cbGroup3.SelectedIndex > 0)
        StatisticsHelper.AddGroup(GetStatGroup(cbGroup3.SelectedIndex));
      //Obtain statistical query object
      DRQueryHelper query = StatisticsHelper.CreateStatsQuery();
      //Set query conditions
      String tmpStr = String.Empty;
      //Obtain string containing selected TUInstanceIDs
      tmpStr = hierarchyTUInstances.TUInstancesList.GetSelectedInstanceIDs();
      if (tmpStr == null)
        query.TUInstanceIDs = String.Empty;
      else
        query.TUInstanceIDs = tmpStr;
      //Build string list of selected hierarchy item IDs
      List<int> selIDs = hierarchyTUInstances.FleetHierarchy.GetSelectedIDs();
      String strSelIDs = String.Empty;
      foreach (int itemID in selIDs)
        strSelIDs += itemID.ToString() + " ";
      query.HierarchyItemIDs = strSelIDs;

      //Set time parameters
      if (dtInterval.FromSelected)
        query.StartTimeFrom = dddHelper.UserTimeToUTC(dtInterval.TimeFrom);
      else
        query.StartTimeFrom = null;
      if (dtInterval.ToSelected)
        query.StartTimeTo = dddHelper.UserTimeToUTC(dtInterval.TimeTo);
      else
        query.StartTimeTo = null;
      //Sources
      query.Sources = tbRecSources.Text.Trim();
      //Vehicle numbers
      query.VehicleNumbers = tbVehicleNumbers.Text.Trim();
      //Fault codes
      query.FaultCodes = tbFaultCodes.Text.Trim();
      //Trace or snap codes
      query.TrcSnpCodes = tbTrcSnpCodes.Text.Trim();
      //Title keywords
      query.TitleKeywords = tbTitleKeyWords.Text.Trim();
      query.MatchAllKeywords = chbMatchAllKeywords.Checked;
      //Record types
      eRecordCategory recTypes = (eRecordCategory)0;
      if (chbEventRec.Checked)
        recTypes |= eRecordCategory.Event;
      if (chbTraceRec.Checked)
        recTypes |= eRecordCategory.Trace;
      if (chbSnapRec.Checked)
        recTypes |= eRecordCategory.Snap;
      if (recTypes == 0)
        query.RecordTypes = null;
      else
        query.RecordTypes = recTypes;
      //Severities
      String strSeverities = String.Empty;
      if (chbSevA1.Checked) strSeverities += ((int)(SeverityType.A1)).ToString() + " ";
      if (chbSevA.Checked) strSeverities += ((int)(SeverityType.A)).ToString() + " ";
      if (chbSevB1.Checked) strSeverities += ((int)(SeverityType.B1)).ToString() + " ";
      if (chbSevB.Checked) strSeverities += ((int)(SeverityType.B)).ToString() + " ";
      if (chbSevC.Checked) strSeverities += ((int)(SeverityType.C)).ToString() + " ";
      query.Severities = strSeverities;
      //Sub severities
      query.SubSeverities = tbFaultSubseverities.Text.Trim();
      //Return the query
      return query;
    }

    /// <summary>
    /// Asynchronously execute prepared statistical query.
    /// Fill helper object with the results.
    /// </summary>
    /// <param name="statsQuery">Prepared statistics query</param>
    /// <param name="asyncOp">Representant of async operation</param>
    protected override void PerformAsyncStatsLoad(DRQueryHelper statsQuery, AsyncOperation asyncOp)
    {
      try
      {
        //Check connection to database
        if (!CheckDBConnection())
          throw new StatSelectorException("Connection to database could not be opened");

        //Select database command parameters
        statCommand.CommandText = statsQuery.GetQuery_Statistical();
        statCommand.CommandTimeout = sqlCmdTimeout;
        statCommand.Connection = dbConnection;
        //Execute SQL command
        SqlDataReader reader;
        //XmlReader reader;
        try
        {
          reader = statCommand.ExecuteReader();
          //reader = cmdSelectDR.ExecuteXmlReader();
        }
        catch (Exception ex)
        {
          throw new StatSelectorException(ex, "Failed to execute statistical query in database");
        }

        //Post info message
        asyncOp.Post(onInfoMessageDelegate, "Loading results of statistical query");
        //Process returned data using the helper
        try
        {
          LoadStatisticResultSets(reader, asyncOp);
        }
        catch (Exception)
        {
          throw;
        }
        finally
        {
          //Close reader object
          reader.Close();
        }

        //Post completed event
        AsyncCompletedEventArgs result = new AsyncCompletedEventArgs(null, false, null);
        asyncOp.PostOperationCompleted(onLoadCompletedDelegate, result);
      }
      catch (Exception ex)
      {
        //Post completed event with error
        AsyncCompletedEventArgs result = new AsyncCompletedEventArgs(ex, false, null);
        asyncOp.PostOperationCompleted(onLoadCompletedDelegate, result);
      }
    }

    public override void LoadSettingsFromFile(FileStream file, BinaryFormatter formatter)
    {
      //Check TU Instances
      hierarchyTUInstances.TUInstancesList.CheckTUInstances((String)formatter.Deserialize(file));
      //Check fleet hierarchy items
      hierarchyTUInstances.FleetHierarchy.CheckHierarchyItems((String)formatter.Deserialize(file));
      //Retrieve rest of parameters
      dtInterval.TimeFrom = (DateTime)formatter.Deserialize(file);
      dtInterval.FromSelected = (bool)formatter.Deserialize(file);
      dtInterval.TimeTo = (DateTime)formatter.Deserialize(file);
      dtInterval.ToSelected = (bool)formatter.Deserialize(file);
      dtInterval.PredefIntervalIndex = (int)formatter.Deserialize(file);
      chbEventRec.Checked = (bool)formatter.Deserialize(file);
      chbSnapRec.Checked = (bool)formatter.Deserialize(file);
      chbTraceRec.Checked = (bool)formatter.Deserialize(file);

      tbTitleKeyWords.Text = (String)formatter.Deserialize(file);
      chbMatchAllKeywords.Checked = (bool)formatter.Deserialize(file);
      tbRecSources.Text = (String)formatter.Deserialize(file);
      tbFaultCodes.Text = (String)formatter.Deserialize(file);
      tbTrcSnpCodes.Text = (String)formatter.Deserialize(file);
      tbVehicleNumbers.Text = (String)formatter.Deserialize(file);
      chbSevA1.Checked = (bool)formatter.Deserialize(file);
      chbSevA.Checked = (bool)formatter.Deserialize(file);
      chbSevB1.Checked = (bool)formatter.Deserialize(file);
      chbSevB.Checked = (bool)formatter.Deserialize(file);
      chbSevC.Checked = (bool)formatter.Deserialize(file);
      tbFaultSubseverities.Text = (String)formatter.Deserialize(file);

      //Retrieve selected indexes for groups
      cbGroup1.SelectedIndex = (int)formatter.Deserialize(file);
      cbGroup2.SelectedIndex = (int)formatter.Deserialize(file);
      cbGroup3.SelectedIndex = (int)formatter.Deserialize(file);
    }

    public override void SaveSettingsToFile(FileStream file, BinaryFormatter formatter)
    {
      //Serialize
      formatter.Serialize(file, hierarchyTUInstances.TUInstancesList.GetSelectedInstanceIDs());
      formatter.Serialize(file, hierarchyTUInstances.FleetHierarchy.GetSelectedNamesXML());
      formatter.Serialize(file, dtInterval.TimeFrom);
      formatter.Serialize(file, dtInterval.FromSelected);
      formatter.Serialize(file, dtInterval.TimeTo);
      formatter.Serialize(file, dtInterval.ToSelected);
      formatter.Serialize(file, dtInterval.PredefIntervalIndex);
      formatter.Serialize(file, chbEventRec.Checked);
      formatter.Serialize(file, chbSnapRec.Checked);
      formatter.Serialize(file, chbTraceRec.Checked);

      formatter.Serialize(file, tbTitleKeyWords.Text);
      formatter.Serialize(file, chbMatchAllKeywords.Checked);
      formatter.Serialize(file, tbRecSources.Text);
      formatter.Serialize(file, tbFaultCodes.Text);
      formatter.Serialize(file, tbTrcSnpCodes.Text);
      formatter.Serialize(file, tbVehicleNumbers.Text);
      formatter.Serialize(file, chbSevA1.Checked);
      formatter.Serialize(file, chbSevA.Checked);
      formatter.Serialize(file, chbSevB1.Checked);
      formatter.Serialize(file, chbSevB.Checked);
      formatter.Serialize(file, chbSevC.Checked);
      formatter.Serialize(file, tbFaultSubseverities.Text);

      formatter.Serialize(file, cbGroup1.SelectedIndex);
      formatter.Serialize(file, cbGroup2.SelectedIndex);
      formatter.Serialize(file, cbGroup3.SelectedIndex);
    }
    #endregion //Virtual methods overrides
  }
}
