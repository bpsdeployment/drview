using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Common;
using System.Data.SqlClient;
using DDDObjects;

public partial class Administration_RefAttributes : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
  }

  /// <summary>
  /// Handler for any errors in page not handled in code by catch blocks.
  /// It is actually page-level exception handler for all types of exceptions.
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  private void Page_Error(object sender, EventArgs e)
  {
    Exception ex = Server.GetLastError();
    Server.Transfer("~/Administration/Error.aspx");
  }

  protected void gvRefAttrList_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    if (e.Row.RowIndex >= 0)
    { //Not a header row
      if ((bool)(((DbDataRecord)(e.Row.DataItem))[5]))
      {
        e.Row.Enabled = false;
        e.Row.BackColor = System.Drawing.Color.White;
        return;
      }
      if ((bool)(((DbDataRecord)(e.Row.DataItem))[3]))
        e.Row.BackColor = System.Drawing.Color.LimeGreen;
      if ((bool)(((DbDataRecord)(e.Row.DataItem))[4]))
      { //Deleted row
        e.Row.BackColor = System.Drawing.Color.OrangeRed;
        LinkButton lnkButt = e.Row.Cells[1].Controls[0] as LinkButton;
        lnkButt.Text = "Cancel";
      }
    }
  }

  protected void btRefAttrIns_Click(object sender, EventArgs e)
  {
    dsRefAttrList.Insert();
  }

  protected void grRefAttrMapping_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    if (e.Row.RowIndex >= 0)
    { //Not a header row
      if ((bool)(((DbDataRecord)(e.Row.DataItem))[5]))
        e.Row.BackColor = System.Drawing.Color.LimeGreen;
      if ((bool)(((DbDataRecord)(e.Row.DataItem))[6]))
      { //Deleted row
        e.Row.BackColor = System.Drawing.Color.OrangeRed;
        LinkButton lnkButt = e.Row.Cells[0].Controls[0] as LinkButton;
        lnkButt.Text = "Cancel";
      }
    }
  }

  protected void gvRefAttrList_DataBound(object sender, EventArgs e)
  {
    gvRefAttrMapping.DataBind();
  }

  protected void ddlDDDVersion_SelectedIndexChanged(object sender, EventArgs e)
  {
    //Refresh data binding for dependent controls
    gvRefAttrMapping.DataBind();
    ddlVariable.DataBind();
    //Erase statistics
    lProcRecCount.Text = "";
    lUnprocRecCount.Text = "";
  }
  protected void btRefAttrMapIns_Click(object sender, EventArgs e)
  {
    //Test copile the expression if necessary
    tbExpression.Text = tbExpression.Text.Trim();
    if (tbExpression.Text != "")
    {
      DDDHelper dddHelper = (DDDHelper)Application["DDDHelper"];
      Guid dddVersionID = new Guid(ddlDDDVersion.SelectedValue);
      dddHelper.GetVersion(dddVersionID).RefAttrEvaluator.TestCompileExpression(tbExpression.Text);
    }
    //Try to insert the mapping
    try
    {
      dsRefAttrMapping.Insert();
    }
    catch (Exception ex)
    {
      throw new Exception("Failed to insert mapping of referential attribute", ex);
    }
  }
  protected void btCommitChanges_Click(object sender, EventArgs e)
  {
    try
    {
      //Creade and prepare SQL command
      SqlCommand cmd = new SqlCommand();
      cmd.Connection = (SqlConnection)Session["DBConnection"];
      if (cmd.Connection.State != ConnectionState.Open)
      {
        cmd.Connection.Close();
        cmd.Connection.Open();
      }
      cmd.CommandType = CommandType.StoredProcedure;
      cmd.CommandText = Resources.Resource.cmdCommitRefAttributes;
      cmd.CommandTimeout = (int)Session["QueryTimeout"];
      //Execute command
      cmd.ExecuteNonQuery();
      //Dispose command
      cmd.Dispose();
      //Refresh data
      gvRefAttrList.DataBind();
      ddlAttribute.DataBind();
    }
    catch (Exception ex)
    {
      throw new Exception("Failed to commit changes in configuration of referential attributes", ex);
    }
  }
  protected void btGetStats_Click(object sender, EventArgs e)
  {
    try
    {
      //Creade and prepare SQL command
      SqlCommand cmd = new SqlCommand();
      cmd.Connection = (SqlConnection)Session["DBConnection"];
      if (cmd.Connection.State != ConnectionState.Open)
      {
        cmd.Connection.Close();
        cmd.Connection.Open();
      }
      cmd.CommandType = CommandType.StoredProcedure;
      cmd.CommandText = Resources.Resource.cmdSelRefAttrStats;
      //Add command parameters
      cmd.Parameters.Add("@VersionID", SqlDbType.Variant);
      cmd.Parameters.Add("@ProcRecCount", SqlDbType.Int).Direction = ParameterDirection.Output;
      cmd.Parameters.Add("@UnprocRecCount", SqlDbType.Int).Direction = ParameterDirection.Output;
      //Set VersionID
      cmd.Parameters[0].Value = ddlDDDVersion.SelectedValue;
      cmd.CommandTimeout = (int)Session["QueryTimeout"];
      //Execute command
      cmd.ExecuteNonQuery();
      //Read output parameters
      lProcRecCount.Text = cmd.Parameters[1].Value.ToString();
      lUnprocRecCount.Text = cmd.Parameters[2].Value.ToString();
      //Dispose command
      cmd.Dispose();
    }
    catch (Exception ex)
    {
      throw new Exception("Failed to select records statistics", ex);
    }
  }

  protected void dsRefAttrMapping_Deleted(object sender, SqlDataSourceStatusEventArgs e)
  {
    gvRefAttrMapping.DataBind();
    gvRefAttrMapping.SelectedIndex = -1;
  }

  protected void ddlDDDVersion_DataBound(object sender, EventArgs e)
  {
    //Display mapping of referential attributes for initial DDD Version
    ddlDDDVersion_SelectedIndexChanged(this, null);
  }
}
