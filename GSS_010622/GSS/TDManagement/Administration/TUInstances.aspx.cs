using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Xml;
using System.IO;
using System.IO.Compression;

public partial class Administration_TUTypes : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {

  }
  protected void ddlDDDVersions_DataBound(object sender, EventArgs e)
  {
    //Add representant of all DDD Versions
    ListItem item = new ListItem(Resources.Resource.ddlAllText, "All", true);
    ddlDDDVersions.Items.Insert(0, item);
  }
  protected void ddlTUTypes_DataBound(object sender, EventArgs e)
  {
    //Add representant of all TU Types
    ListItem item = new ListItem(Resources.Resource.ddlAllText, "All", true);
    ddlTUTypes.Items.Insert(0, item);
  }
  protected void gvTUInstances_DataBound(object sender, EventArgs e)
  {
    if (gvTUInstances.Rows.Count < 1 || gvTUInstances.SelectedIndex == -1)
    {
      btDelete.Enabled = false;
      btUpdate.Enabled = false;
      btExportConf.Enabled = false;
      btDwnlTUInstInfo.Enabled = false;
    }
    else
    {
      btDelete.Enabled = true;
      btUpdate.Enabled = true;
      btExportConf.Enabled = true;
      btDwnlTUInstInfo.Enabled = true;
    }
  }
  /// <summary>
  /// User selected row in TU Instances grid
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  protected void gvTUInstances_SelectedIndexChanged(object sender, EventArgs e)
  {
    if (gvTUInstances.SelectedIndex == -1)
    { //no row selected - disable update and delete buttons
      btDelete.Enabled = false;
      btUpdate.Enabled = false;
      btExportConf.Enabled = false;
      btDwnlTUInstInfo.Enabled = false;
      return;
    }
    //Set detail controls content, based on grid selection
    tbDetailName.Text = gvTUInstances.SelectedRow.Cells[1].Text;
    //UIC number
    if (gvTUInstances.SelectedRow.Cells[3].Text != "&nbsp;")
      tbDetailUICNo.Text = gvTUInstances.SelectedRow.Cells[3].Text;
    else
      tbDetailUICNo.Text = "";
    //TU Inst comment
    if (gvTUInstances.SelectedRow.Cells[4].Text != "&nbsp;")
      tbDetailComment.Text = gvTUInstances.SelectedRow.Cells[4].Text;
    else
      tbDetailComment.Text = "";
    //TU Type
    ddlDetailTUType.SelectedValue = gvTUInstances.DataKeys[gvTUInstances.SelectedIndex].Values[1].ToString();
    //Configured DDD Version
    ddlConfiguredDDD.DataBind();
    if (gvTUInstances.DataKeys[gvTUInstances.SelectedIndex].Values[3].GetType() == typeof(DBNull))
      ddlConfiguredDDD.SelectedIndex = 0; //There is no DDD Version associated with TU Instance
    else
      ddlConfiguredDDD.SelectedValue = gvTUInstances.DataKeys[gvTUInstances.SelectedIndex].Values[3].ToString();
    //Running DDD Version
    if (gvTUInstances.SelectedRow.Cells[5].Text != "&nbsp;")
      tbRunningDDD.Text = gvTUInstances.SelectedRow.Cells[5].Text;
    else
      tbRunningDDD.Text = "";
    //Enable update and delete buttons
    btDelete.Enabled = true;
    btUpdate.Enabled = true;
    btExportConf.Enabled = true;
    btDwnlTUInstInfo.Enabled = true;
  }
  protected void ddlConfiguredDDD_DataBound(object sender, EventArgs e)
  {
    //Add representant of empty (null) DDD version
    ListItem item = new ListItem("", "", true);
    ddlConfiguredDDD.Items.Insert(0, item);
  }
  protected void btUpdate_Click(object sender, EventArgs e)
  {
    if (gvTUInstances.SelectedIndex == -1)
      return;
    try
    {
      //Run update command for currently selected TU Instance
      dsTUInstances.Update();
    }
    catch (Exception ex)
    {
      lbError.Text = Resources.Resource.msgErrorPrefix + ex.Message;
      lbError.Visible = true;
    }
  }
  protected void btNew_Click(object sender, EventArgs e)
  {
    try
    {
      //Insert new TU Instance record
      dsTUInstances.Insert();
    }
    catch (Exception ex)
    {
      lbError.Text = Resources.Resource.msgErrorPrefix + ex.Message;
      lbError.Visible = true;
    }
  }
  protected void btDelete_Click(object sender, EventArgs e)
  {
    if (gvTUInstances.SelectedIndex == -1)
      return;
    try
    {
      //Run delete command for currently selected record
      dsTUInstances.Delete();
    }
    catch (Exception ex)
    {
      lbError.Text = Resources.Resource.msgErrorPrefix + ex.Message;
      lbError.Visible = true;
    }
  }
  /// <summary>
  /// Handler for any errors in page not handled in code by catch blocks.
  /// It is actually page-level exception handler for all types of exceptions.
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  private void Page_Error(object sender, EventArgs e)
  {
    //Page error occured - display the text in response
    Response.Write(Resources.Resource.pageErrorText + Server.GetLastError().Message);
    Server.ClearError();
  }
  /// <summary>
  /// Event handler, that retrieves the TUInstanceInfo document from DB and pushes it to the client
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  protected void btDwnlTUInstInfo_Click(object sender, EventArgs e)
  {
    if (gvTUInstances.SelectedIndex == -1 || gvTUInstances.SelectedDataKey.Value == null)
      return;
    SqlCommand getInfoProc = new SqlCommand();
    SqlParameter procParam;
    XmlReader infoReader;
    XmlDocument infoDocument;
    XmlTextWriter infoWriter;
    try
    {
      //Create the SQL procedure command and fill parameter
      getInfoProc.Connection = (SqlConnection)Session["DBConnection"];
      getInfoProc.CommandType = CommandType.StoredProcedure;
      getInfoProc.CommandText = "GetTUInstanceInfoXML";
      getInfoProc.CommandTimeout = (int)Session["QueryTimeout"];
      procParam = getInfoProc.Parameters.Add("@TUInstanceID", SqlDbType.UniqueIdentifier);
      procParam.Value = gvTUInstances.SelectedDataKey.Value;
      //Execute the command - get XML reader
      infoReader = getInfoProc.ExecuteXmlReader();
      //Load XML into document
      infoDocument = new XmlDocument();
      infoDocument.Load(infoReader);
      infoReader.Close();
      //Prepare Http response to write the document into      
      //Clear the response - only the file will be included
      Response.Clear();
      // set the http content type to "APPLICATION/OCTET-STREAM
      Response.ContentType = "APPLICATION/OCTET-STREAM";
      // initialize the http content-disposition header to indicate a file attachment
      String disHeader = "Attachment; Filename=\"" + Resources.Resource.TUTypeInfoFileName + "\"";
      Response.AppendHeader("Content-Disposition", disHeader);
      //Create XML text writer with intended formatting (that is the reason for all the hustle)
      //writing to the response output stream
      infoWriter = new XmlTextWriter(Response.OutputStream, System.Text.Encoding.UTF8);
      infoWriter.Formatting = Formatting.Indented;
      //Write the document into the output stream
      infoWriter.WriteStartDocument();
      infoDocument.WriteTo(infoWriter);
      infoWriter.Close();
      Response.Flush();
      //End the response - response contains the file only
      Response.End();
    }
    catch (Exception ex)
    {
      lbError.Text = Resources.Resource.msgErrorPrefix + ex.Message;
      lbError.Visible = true;
    }
    finally
    {
      getInfoProc.Dispose();
    }
  }

  /// <summary>
  /// Change color of the row, based on the value of last update time
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  protected void gvTUInstances_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    DateTime updTime;
    if ((e.Row.Cells.Count < 10) || !DateTime.TryParse(e.Row.Cells[9].Text, out updTime))
      return;
    if (updTime < DateTime.Now.AddMinutes(-5))
      e.Row.Cells[9].BackColor = System.Drawing.Color.OrangeRed;
    else
      e.Row.Cells[9].BackColor = System.Drawing.Color.LimeGreen;
  }

  /// <summary>
  /// Exports configuration for selected TU instance to directory available over FTP
  /// </summary>
  protected void btExportConf_Click(object sender, EventArgs e)
  {
    Guid tuTypeID;
    Guid tuInstanceID;
    Guid dddVersionID;
    String tuConfDir;
    //Check if there is some DDD version selected
    if (ddlConfiguredDDD.SelectedIndex < 1)
      return;
    try
    {
      //Obtain selected identifiers
      tuTypeID = (Guid)gvTUInstances.SelectedDataKey[1];
      tuInstanceID = (Guid)gvTUInstances.SelectedDataKey[0];
      dddVersionID = new Guid(ddlConfiguredDDD.SelectedValue);
      //First check for or create direcory for selected TU configuration
      tuConfDir = ConfigurationManager.AppSettings["TUConfRootDir"];
      tuConfDir = Path.Combine(tuConfDir, tuTypeID.ToString());
      tuConfDir = Path.Combine(tuConfDir, tuInstanceID.ToString());
      if (!Directory.Exists(tuConfDir))
        Directory.CreateDirectory(tuConfDir);
    }
    catch (Exception ex)
    {
      lbError.Text = "Failed to obtain directory for TU configuration: " + ex.Message;
      lbError.Visible = true;
      return;
    }
    //Run stored procedure which updates the configuration in DB and returns all files for selected version
    SqlCommand updateConfProc = new SqlCommand();
    SqlDataReader dbFileReader = null;
    try
    {
      //Set command properties and parameters
      updateConfProc.CommandText = "ConfDDDVerGetFiles";
      updateConfProc.CommandType = CommandType.StoredProcedure;
      updateConfProc.Connection = (SqlConnection)Session["DBConnection"];
      updateConfProc.CommandTimeout = (int)Session["QueryTimeout"];
      updateConfProc.Parameters.Add("@VersionID", SqlDbType.UniqueIdentifier).Value = dddVersionID;
      updateConfProc.Parameters.Add("@TUInstanceID", SqlDbType.UniqueIdentifier).Value = tuInstanceID;
      //Execute command and obtain data reader
      dbFileReader = updateConfProc.ExecuteReader(CommandBehavior.SequentialAccess);
      //Read each file from DB and save to specified directory
      String fileName;
      FileStream fileStream;
      Stream topStream;
      byte[] buffer = new byte[0x19000];
      while (dbFileReader.Read())
      {
        //Create the file
        fileName = dbFileReader.GetString(0);
        if (fileName.ToLower() == Resources.ConfigImport.DDDFileName.ToLower())
        {
          String zipName = Path.Combine(tuConfDir, fileName) + ".gz";
          fileStream = new FileStream(zipName, FileMode.Create, FileAccess.Write);
          topStream = new GZipStream(fileStream, CompressionMode.Compress);
        }
        else
        {
          fileStream = new FileStream(Path.Combine(tuConfDir, fileName), FileMode.Create, FileAccess.Write);
          topStream = fileStream;
        }
        //Read data from DB to buffer and write to file in loop
        long dataIndex = 0;
        long readCount = 0;
        while ((readCount = dbFileReader.GetBytes(2, dataIndex, buffer, 0, buffer.Length)) > 0)
        {
          dataIndex += readCount;
          //Write buffer to file
          topStream.Write(buffer, 0, (int)readCount);
        }
        //Close file
        topStream.Close();
        fileStream.Close();
        topStream.Dispose();
      }
    }
    catch (Exception ex)
    {
      lbError.Text = "Failed to export files from DB into TU directory: " + ex.Message;
      lbError.Visible = true;
      return;
    }
    finally
    {//Dispose command and reader
      if (dbFileReader != null)
      {
        dbFileReader.Close();
        dbFileReader.Dispose();
      }
      updateConfProc.Dispose();
    }
    //Refresh grid with TU instances
    gvTUInstances.DataBind();
  }
  protected void ddlConfiguredDDD_SelectedIndexChanged(object sender, EventArgs e)
  {
  }
}
