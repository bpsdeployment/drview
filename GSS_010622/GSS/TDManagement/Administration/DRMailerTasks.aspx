<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="DRMailerTasks.aspx.cs"
  Inherits="Administration_DRMailerTasks" Title="Task definintion for DRMailer application" %>

<asp:Content ID="cntMailerTasks" ContentPlaceHolderID="contentPH" runat="Server">
  <table cellspacing="5" style="width: 100%">
    <tr>
      <td>
        <h4>
          <asp:Label ID="Label1" runat="server" Text="Diagnostic Records Mailer Tasks" Width="300px"></asp:Label>
        </h4>
      </td>
    </tr>
    <tr>
      <td>
        <asp:GridView ID="gvTaskDefs" runat="server" AllowSorting="True" AutoGenerateColumns="False"
          BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3"
          DataKeyNames="TaskName" DataSourceID="dsTaskDefs" ForeColor="Black" GridLines="Both"
          OnSelectedIndexChanged="gvTaskDefs_SelectedIndexChanged" OnRowDataBound="gvTaskDefs_RowDataBound"
          Font-Names="Tahoma" Font-Size="Small">
          <FooterStyle BackColor="#CCCCCC" />
          <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="TaskName" HeaderText="Task Name" ReadOnly="True" SortExpression="TaskName" />
            <asp:BoundField DataField="StartTime" DataFormatString="{0:yyyy-MM-dd HH:mm:ss}"
              HeaderText="First run" SortExpression="StartTime" HtmlEncode="False" />
            <asp:BoundField DataField="LastSuccRun" DataFormatString="{0:yyyy-MM-dd HH:mm:ss}"
              HeaderText="Last successful run" SortExpression="LastSuccRun" HtmlEncode="False" />
            <asp:BoundField DataField="Period" HeaderText="Query period" SortExpression="Period"
              DataFormatString="{0:d}h" />
            <asp:BoundField DataField="Interval" HeaderText="Query interval" SortExpression="Interval"
              DataFormatString="{0:d}h" />
            <asp:BoundField DataField="Recipients" HeaderText="Recipients" SortExpression="Recipients" />
            <asp:BoundField DataField="Comment" HeaderText="Comment" SortExpression="Comment" />
            <asp:CheckBoxField DataField="Enabled" HeaderText="Enabled" SortExpression="Enabled" />
            <asp:CheckBoxField DataField="LastFailed" HeaderText="Failed" SortExpression="LastFailed" />
          </Columns>
          <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="Black" />
          <PagerStyle BackColor="#FFDDDD" ForeColor="Blue" HorizontalAlign="Center" />
          <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
          <AlternatingRowStyle BackColor="#DDDDFF" BorderColor="White" BorderStyle="Solid"
            BorderWidth="2px" />
          <RowStyle BackColor="#DDFFDD" BorderColor="White" BorderStyle="Solid" BorderWidth="2px" />
        </asp:GridView>
        &nbsp;
        <asp:ObjectDataSource ID="dsTaskDefs" runat="server" DeleteMethod="DeleteTask" InsertMethod="InsertNewTask"
          OldValuesParameterFormatString="original_{0}" SelectMethod="GetTaskDefinitions"
          TypeName="BLL.DRMailerTaskDefsBLL" UpdateMethod="UpdateTask" OnObjectCreated="dsTaskDefs_ObjectCreated"
          OnUpdating="dsTaskDefs_Updating" OnInserting="dsTaskDefs_Inserting">
          <DeleteParameters>
            <asp:ControlParameter ControlID="gvTaskDefs" Name="TaskName" PropertyName="SelectedValue"
              Type="String" />
          </DeleteParameters>
          <UpdateParameters>
            <asp:ControlParameter ControlID="gvTaskDefs" Name="TaskName" PropertyName="SelectedValue"
              Type="String" />
            <asp:ControlParameter ControlID="tbTaskName" Name="NewTaskName" PropertyName="Text"
              Type="String" />
            <asp:ControlParameter ControlID="ddlPeriod" Name="Period" PropertyName="SelectedValue"
              Type="Int32" />
            <asp:ControlParameter ControlID="tbStartTime" Name="StartTime" PropertyName="Text"
              Type="DateTime" />
            <asp:Parameter Name="MaxDelay" Type="Int32" />
            <asp:ControlParameter ControlID="tbMailRecipients" Name="Recipients" PropertyName="Text"
              Type="String" />
            <asp:ControlParameter ControlID="tbSubject" Name="Subject" PropertyName="Text" Type="String" />
            <asp:Parameter Name="TUInstances" Type="String" />
            <asp:ControlParameter ControlID="ddlInterval" Name="Interval" PropertyName="SelectedValue"
              Type="Int32" />
            <asp:ControlParameter ControlID="ddlDelay" Name="Delay" PropertyName="SelectedValue"
              Type="Int32" />
            <asp:Parameter Name="RecTypeMask" Type="Int32" />
            <asp:ControlParameter ControlID="tbSources" Name="Sources" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="tbFaultCodes" Name="FaultCodes" PropertyName="Text"
              Type="String" />
            <asp:ControlParameter ControlID="tbTitleKeywords" Name="TitleKeywords" PropertyName="Text"
              Type="String" />
            <asp:ControlParameter ControlID="chbMatchAllKeywords" Name="MatchAllKeywords" PropertyName="Checked"
              Type="Boolean" />
            <asp:ControlParameter ControlID="tbMaxRecords" Name="MaxRecords" PropertyName="Text"
              Type="Int32" />
            <asp:ControlParameter ControlID="tbMaxFileLength" Name="MaxFileLengthBytes" PropertyName="Text"
              Type="Int32" />
            <asp:ControlParameter ControlID="chbSendEmptyFile" Name="SendEmptyFile" PropertyName="Checked"
              Type="Boolean" />
            <asp:ControlParameter ControlID="tbFileName" Name="FileNameFormat" PropertyName="Text"
              Type="String" />
            <asp:ControlParameter ControlID="tbComment" Name="Comment" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="chbEnabled" Name="Enabled" PropertyName="Checked"
              Type="Boolean" />
          </UpdateParameters>
          <InsertParameters>
            <asp:ControlParameter ControlID="tbTaskName" Name="TaskName" PropertyName="Text"
              Type="String" />
            <asp:ControlParameter ControlID="ddlPeriod" Name="Period" PropertyName="SelectedValue"
              Type="Int32" />
            <asp:ControlParameter ControlID="tbStartTime" Name="StartTime" PropertyName="Text"
              Type="DateTime" />
            <asp:Parameter Name="MaxDelay" Type="Int32" />
            <asp:ControlParameter ControlID="tbMailRecipients" Name="Recipients" PropertyName="Text"
              Type="String" />
            <asp:ControlParameter ControlID="tbSubject" Name="Subject" PropertyName="Text" Type="String" />
            <asp:Parameter Name="TUInstances" Type="String" />
            <asp:ControlParameter ControlID="ddlInterval" Name="Interval" PropertyName="SelectedValue"
              Type="Int32" />
            <asp:ControlParameter ControlID="ddlDelay" Name="Delay" PropertyName="SelectedValue"
              Type="Int32" />
            <asp:Parameter Name="RecTypeMask" Type="Int32" />
            <asp:ControlParameter ControlID="tbSources" Name="Sources" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="tbFaultCodes" Name="FaultCodes" PropertyName="Text"
              Type="String" />
            <asp:ControlParameter ControlID="tbTitleKeywords" Name="TitleKeywords" PropertyName="Text"
              Type="String" />
            <asp:ControlParameter ControlID="chbMatchAllKeywords" Name="MatchAllKeywords" PropertyName="Checked"
              Type="Boolean" />
            <asp:ControlParameter ControlID="tbMaxRecords" Name="MaxRecords" PropertyName="Text"
              Type="Int32" />
            <asp:ControlParameter ControlID="tbMaxFileLength" Name="MaxFileLengthBytes" PropertyName="Text"
              Type="Int32" />
            <asp:ControlParameter ControlID="chbSendEmptyFile" Name="SendEmptyFile" PropertyName="Checked"
              Type="Boolean" />
            <asp:ControlParameter ControlID="tbFileName" Name="FileNameFormat" PropertyName="Text"
              Type="String" />
            <asp:ControlParameter ControlID="tbComment" Name="Comment" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="chbEnabled" Name="Enabled" PropertyName="Checked"
              Type="Boolean" />
          </InsertParameters>
        </asp:ObjectDataSource>
      </td>
    </tr>
  </table>
  <div style="margin: 5px">
    <table style="width: 100%">
      <tr>
        <td>
          <h4>
            <asp:Label ID="lTaskName" runat="server" Text="Task"></asp:Label>
            Details
          </h4>
        </td>
      </tr>
    </table>
    <table>
      <tr>
        <td valign="bottom" style="width: 200px">
          <h5>
            Task Name</h5>
        </td>
        <td width="300" valign="bottom">
          <h5>
            Subject</h5>
        </td>
        <td width="300" valign="bottom">
          <h5>
            Comment</h5>
        </td>
      </tr>
      <tr>
        <td style="height: 26px">
          <asp:TextBox ID="tbTaskName" runat="server" Width="200px"></asp:TextBox></td>
        <td width="300" style="height: 26px">
          <asp:TextBox ID="tbSubject" runat="server" Width="312px"></asp:TextBox></td>
        <td width="300" style="height: 26px">
          <asp:TextBox ID="tbComment" runat="server" Width="311px"></asp:TextBox></td>
      </tr>
    </table>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="tbTaskName"
      Display="Dynamic" ErrorMessage="Task Name has to be max. 255 characters long" ValidationExpression=".{0,255}"></asp:RegularExpressionValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="tbSubject"
      Display="Dynamic" ErrorMessage="Subject has to be max. 512 characters long" ValidationExpression=".{0,512}"></asp:RegularExpressionValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="tbComment"
      Display="Dynamic" ErrorMessage="Comment has to be max. 255 characters long" ValidationExpression=".{0,255}"></asp:RegularExpressionValidator>
    <table>
      <tr>
        <td valign="bottom">
          <h5>
            First run</h5>
        </td>
        <td valign="bottom">
          <h5>
            Query period</h5>
        </td>
        <td valign="bottom">
          <h5>
            Query interval</h5>
        </td>
        <td valign="bottom">
          <h5>
            Query interval offset</h5>
        </td>
        <td valign="bottom">
        </td>
      </tr>
      <tr>
        <td style="height: 26px">
          <asp:TextBox ID="tbStartTime" runat="server" Width="177px"></asp:TextBox>
          <img src="../Pictures/calendar.gif" id="btFirstRun" onmouseover="this.style.background='black';"
            onmouseout="this.style.background=''" />
        </td>
        <td style="height: 26px">
          <asp:DropDownList ID="ddlPeriod" runat="server" Width="157px">
            <asp:ListItem Value="1">1 hour</asp:ListItem>
            <asp:ListItem Value="2">2 hours</asp:ListItem>
            <asp:ListItem Value="4">4 hours</asp:ListItem>
            <asp:ListItem Value="8">8 hours</asp:ListItem>
            <asp:ListItem Value="12">12 hours</asp:ListItem>
            <asp:ListItem Value="16">16 hours</asp:ListItem>
            <asp:ListItem Selected="True" Value="24">1 day</asp:ListItem>
            <asp:ListItem Value="48">2 days</asp:ListItem>
            <asp:ListItem Value="96">4 days</asp:ListItem>
            <asp:ListItem Value="168">1 week</asp:ListItem>
            <asp:ListItem Value="336">2 weeks</asp:ListItem>
            <asp:ListItem Value="672">4 weeks</asp:ListItem>
          </asp:DropDownList>
        </td>
        <td style="height: 26px">
          <asp:DropDownList ID="ddlInterval" runat="server" Width="157px">
            <asp:ListItem Value="1">1 hour</asp:ListItem>
            <asp:ListItem Value="2">2 hours</asp:ListItem>
            <asp:ListItem Value="4">4 hours</asp:ListItem>
            <asp:ListItem Value="8">8 hours</asp:ListItem>
            <asp:ListItem Value="12">12 hours</asp:ListItem>
            <asp:ListItem Value="16">16 hours</asp:ListItem>
            <asp:ListItem Selected="True" Value="24">1 day</asp:ListItem>
            <asp:ListItem Value="48">2 days</asp:ListItem>
            <asp:ListItem Value="96">4 days</asp:ListItem>
            <asp:ListItem Value="168">1 week</asp:ListItem>
            <asp:ListItem Value="336">2 weeks</asp:ListItem>
            <asp:ListItem Value="672">4 weeks</asp:ListItem>
          </asp:DropDownList>
        </td>
        <td style="height: 26px">
          <asp:DropDownList ID="ddlDelay" runat="server" Width="157px">
            <asp:ListItem Selected="True" Value="1">1 hour</asp:ListItem>
            <asp:ListItem Value="2">2 hours</asp:ListItem>
            <asp:ListItem Value="4">4 hours</asp:ListItem>
            <asp:ListItem Value="8">8 hours</asp:ListItem>
            <asp:ListItem Value="12">12 hours</asp:ListItem>
            <asp:ListItem Value="16">16 hours</asp:ListItem>
            <asp:ListItem Value="24">1 day</asp:ListItem>
          </asp:DropDownList></td>
        <td>
          <h5>
            <asp:CheckBox ID="chbEnabled" runat="server" Text="Enabled" Width="155px" /></h5>
        </td>
      </tr>
    </table>

    <script language="JavaScript" src="calendar/calendar.js"></script>

    <script language="JavaScript" src="calendar/calendar-en.js"></script>

    <script language="JavaScript" src="calendar/calendar-setup.js"></script>

    <script language="JavaScript">
    Calendar.setup({
        inputField     :    "ctl00$contentPH$tbStartTime",
        ifFormat       :    "%Y-%m-%d %H:%M:%S",
        button         :    "btFirstRun",
        showsTime      :    true,
        timeFormat     :    "24",
        singleClick    :    true
    });
    </script>

    <asp:RegularExpressionValidator ID="validatorTimeFrom" runat="server" ControlToValidate="tbStartTime"
      Display="Dynamic" ErrorMessage="Ivalid format of First run (YYYY-MM-DD hh:mm:ss required)"
      ValidationExpression="\d\d\d\d-[01]\d-[0-3]\d( )?([0-2]\d:)?([0-5]\d:)?([0-5]\d)?"></asp:RegularExpressionValidator>
    <table>
      <tr>
        <td valign="bottom">
          <h5>
            Mail recipients</h5>
        </td>
      </tr>
      <tr>
        <td>
          <asp:TextBox ID="tbMailRecipients" runat="server" Width="846px"></asp:TextBox></td>
      </tr>
    </table>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="tbMailRecipients"
      Display="Dynamic" ErrorMessage="Invalid format of mail recipients (mail1@mail.com, mail2@mail.com...)"
      ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,]([ ])*\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*"></asp:RegularExpressionValidator>
    <table>
      <tr>
        <td valign="bottom">
          <h5>
            Telediagnostic Units</h5>
        </td>
        <td colspan="2" valign="bottom">
          <h5>
            Fault Codes</h5>
        </td>
        <td colspan="2" valign="bottom">
          <h5>
            Sources</h5>
        </td>
      </tr>
      <tr>
        <td valign="top" rowspan="5">
        <div style="overflow-y:scroll; height:129px; border-right: blue 1px solid; border-top: blue 1px solid; border-left: blue 1px solid; border-bottom: blue 1px solid;">
          <h5>
            <asp:CheckBoxList ID="chblTUInstances" runat="server" Width="205px" BorderColor="CornflowerBlue"
              BorderStyle="Solid" BorderWidth="1px">
              <asp:ListItem Selected="True">Test TU</asp:ListItem>
            </asp:CheckBoxList></h5>
            </div>
        </td>
        <td colspan="2">
          <asp:TextBox ID="tbFaultCodes" runat="server" Width="312px"></asp:TextBox></td>
        <td colspan="2">
          <asp:TextBox ID="tbSources" runat="server" Width="311px"></asp:TextBox></td>
      </tr>
      <tr>
        <td colspan="2">
          <h5>
            Title Keywords</h5>
        </td>
        <td align="right">
          <h5>
            <asp:CheckBox ID="chbMatchAllKeywords" runat="server" Text="Match all" /></h5>
        </td>
        <td rowspan="3" style="width: 112px">
          <h5>
            <asp:CheckBoxList ID="chblRecCategories" runat="server" BorderColor="CornflowerBlue"
              BorderStyle="Solid" BorderWidth="1px">
              <asp:ListItem Selected="True" Value="2">Event</asp:ListItem>
              <asp:ListItem Value="4">Trace</asp:ListItem>
              <asp:ListItem Value="8">Snap</asp:ListItem>
            </asp:CheckBoxList></h5>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <asp:TextBox ID="tbTitleKeywords" runat="server" Width="475px"></asp:TextBox></td>
      </tr>
      <tr>
        <td>
          <h5>
            Maximum records</h5>
        </td>
        <td>
          <h5>
            Max file length</h5>
        </td>
        <td>
          <h5>
            File name</h5>
        </td>
      </tr>
      <tr>
        <td valign="top">
          <asp:TextBox ID="tbMaxRecords" runat="server" Width="150px"></asp:TextBox></td>
        <td valign="top">
          <asp:TextBox ID="tbMaxFileLength" runat="server" Width="150px"></asp:TextBox></td>
        <td valign="top">
          <asp:TextBox ID="tbFileName" runat="server" Width="150px"></asp:TextBox></td>
        <td valign="top">
          <h5>
            <asp:CheckBox ID="chbSendEmptyFile" runat="server" Text="Send empty file" Width="150px" /></h5>
        </td>
      </tr>
    </table>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="tbFaultCodes"
      Display="Dynamic" ErrorMessage="Fault Codes field has to be max. 255 characters long"
      ValidationExpression=".{0,255}"></asp:RegularExpressionValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="tbSources"
      Display="Dynamic" ErrorMessage="Sources field has to be max. 255 characters long"
      ValidationExpression=".{0,255}"></asp:RegularExpressionValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="tbTitleKeywords"
      Display="Dynamic" ErrorMessage="Title Keywords field has to be max. 255 characters long"
      ValidationExpression=".{0,255}"></asp:RegularExpressionValidator><asp:RangeValidator
        ID="RangeValidator1" runat="server" ControlToValidate="tbMaxRecords" Display="Dynamic"
        ErrorMessage="Maximum records has to be lower than 20000" MaximumValue="20000"
        MinimumValue="0" Type="Integer"></asp:RangeValidator>
    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="tbMaxRecords"
      Display="Dynamic" ErrorMessage="Max file length has to be lower than 10000000"
      MaximumValue="10000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="tbSources"
      Display="Dynamic" ErrorMessage="File name has to be max. 255 characters long" ValidationExpression=".{0,255}"></asp:RegularExpressionValidator><br />
    <table>
      <tr>
        <td>
          <asp:Button ID="btUpdate" runat="server" Text="Update selected" OnClick="btUpdate_Click" Enabled="False" />
        </td>
        <td>
          <asp:Button ID="btInsert" runat="server" OnClick="btInsert_Click" Text="Insert new" />
        </td>
        <td>
          <asp:Button ID="btDelete" runat="server" OnClick="btDelete_Click" Text="Delete selected" Enabled="False" />
        </td>
        <td>
          <asp:Button ID="btTaskHistory" runat="server" Text="Execution history" OnClick="btTaskHistory_Click" Enabled="False" />
        </td>
      </tr>
    </table>
  </div>
</asp:Content>
