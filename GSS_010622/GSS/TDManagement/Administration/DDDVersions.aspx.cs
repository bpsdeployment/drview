using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class Administration_DDDVersions : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!IsPostBack)
    {
      //For initial page load, preset the values of calendar controls
      tbFrom.Text = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd");
      tbTo.Text = DateTime.Today.ToString("yyyy-MM-dd");
    }
  }
  protected void ddlTUTypes_SelectedIndexChanged(object sender, EventArgs e)
  {
    if (ddlTUTypes.SelectedIndex > 0)
      ddlUserVersions.Enabled = true;
    else
      ddlUserVersions.Enabled = false;
  }
  protected void ddlTUTypes_DataBound(object sender, EventArgs e)
  {
    //Add representant of all TU Types
    ListItem item = new ListItem(Resources.Resource.ddlAllText, "%", true);
    ddlTUTypes.Items.Insert(0, item);
  }
  protected void ddlUserVersions_DataBound(object sender, EventArgs e)
  {
    //Add representant of all User versions
    ListItem item = new ListItem(Resources.Resource.ddlAllText, "%", true);
    ddlUserVersions.Items.Insert(0, item);
  }
  /// <summary>
  /// Handler for any errors in page not handled in code by catch blocks.
  /// It is actually page-level exception handler for all types of exceptions.
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  private void Page_Error(object sender, EventArgs e)
  {
    //Page error occured - display the text in response
    Response.Write(Resources.Resource.pageErrorText + Server.GetLastError().Message);
    Server.ClearError();
  }
  protected void gvDDDVersions_SelectedIndexChanged(object sender, EventArgs e)
  {
    if (gvDDDVersions.SelectedIndex == -1)
      panDDDCommands.Enabled = false;
    else
      panDDDCommands.Enabled = true;
  }
  protected void gvDDDVersions_DataBound(object sender, EventArgs e)
  {
    if (gvDDDVersions.Rows.Count < 1 || gvDDDVersions.SelectedIndex == -1)
      panDDDCommands.Enabled = false;
    else
      panDDDCommands.Enabled = true;
  }

  /// <summary>
  /// Event handler for Download file button.
  /// Reads specified file from DB using the GetImportedFile procedure and pushes the file to client.
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  protected void btDownloadFile_Click(object sender, EventArgs e)
  {
    SqlCommand getFileCmd = new SqlCommand();
    SqlDataReader fileReader;
    SqlParameter procParam;
    Byte[] fileBuffer;
    int buffSize;
    int readSize;
    int readOffset;
    //Check if there is any file selected
    if ((gvDDDVersions.SelectedValue == null) || (ddlConfigFiles.SelectedValue == null) || (ddlConfigFiles.SelectedValue == ""))
    {
      lbError.Text = Resources.DDDVersions.msgNoFileSelected;
      lbError.Visible = true;
      return;
    }
    try
    {
      //Create command object
      getFileCmd.Connection = (SqlConnection)Session["DBConnection"];
      getFileCmd.CommandType = CommandType.StoredProcedure;
      getFileCmd.CommandText = "GetImportedFile";
      getFileCmd.CommandTimeout = (int)Session["QueryTimeout"];
      //Set command parameters
      procParam = getFileCmd.Parameters.Add("@VersionID", SqlDbType.UniqueIdentifier);
      procParam.Value = gvDDDVersions.SelectedValue;
      procParam = getFileCmd.Parameters.Add("@FileName", SqlDbType.NVarChar, 50);
      procParam.Value = ddlConfigFiles.SelectedValue;
      procParam = getFileCmd.Parameters.Add("@FileLength", SqlDbType.Int);
      procParam.Direction = ParameterDirection.Output;
      //Execute the command
      fileReader = getFileCmd.ExecuteReader(CommandBehavior.SequentialAccess);
      //Prepare Http response to write the file into      
      // set the http content type to "APPLICATION/OCTET-STREAM
      Response.ContentType = "APPLICATION/OCTET-STREAM";
      //Clear the response - only the file will be included
      Response.Clear();
      // initialize the http content-disposition header to indicate a file attachment
      String disHeader = "Attachment; Filename=\"" + ddlConfigFiles.SelectedValue + "\"";
      Response.AppendHeader("Content-Disposition", disHeader);
      // read the file in blocks and transfer into response stream
      fileReader.Read();
      readOffset = 0;
      buffSize = Convert.ToInt32(Resources.DDDVersions.intDwnlBuffSize);
      fileBuffer = new byte[buffSize];
      do
      {
        //Read chunk from DB
        readSize = (int)fileReader.GetBytes(0, readOffset, fileBuffer, 0, buffSize);
        readOffset += readSize;
        //Write the chunk into output stream
        Response.OutputStream.Write(fileBuffer, 0, readSize);
      } while (readSize == buffSize);
      fileReader.Dispose();
      Response.Flush();
      //End the response - response contains the file only
      Response.End();
    }
    catch (Exception ex)
    {
      lbError.Text = ex.Message;
      lbError.Visible = true;
    }
    finally
    {
      if (null != getFileCmd)
        getFileCmd.Dispose();
    }
  }
  protected void dbDeleteVersion_Click(object sender, EventArgs e)
  {
    if (gvDDDVersions.SelectedIndex >= 0 && gvDDDVersions.SelectedValue != null)
    {
      //Get the count of DR stored for this Version
      SqlCommand getCountProc = new SqlCommand();
      SqlParameter procParam;
      int recCount = 0;
      try
      {
        getCountProc.Connection = (SqlConnection)Session["DBConnection"];
        getCountProc.CommandType = CommandType.StoredProcedure;
        getCountProc.CommandText = "GetDDDVersionDRCount";
        getCountProc.CommandTimeout = Convert.ToInt32(Resources.DDDVersions.intDeleteProcTimeout);
        procParam = getCountProc.Parameters.Add("@VersionID", SqlDbType.UniqueIdentifier);
        procParam.Value = gvDDDVersions.SelectedValue;
        //Execute the procedure
        recCount = (int)getCountProc.ExecuteScalar();
      }
      catch (Exception ex)
      {
        lbError.Text = Resources.Resource.msgErrorPrefix + ex.Message;
        lbError.Visible = true;
      }
      finally
      {
        getCountProc.Dispose();
      }
      //Show warning message together with record count
      lbDeleteWarning.Text = String.Format(Resources.DDDVersions.msgDeleteVersionWarning, 
                                           gvDDDVersions.SelectedRow.Cells[2].Text, gvDDDVersions.SelectedRow.Cells[1].Text, recCount);
      panDeleteVersion.Visible = true;
    }
  }
  protected void btDeleteCancel_Click1(object sender, EventArgs e)
  {
    panDeleteVersion.Visible = false;
  }
  protected void btDeleteConfirm_Click(object sender, EventArgs e)
  {
    //Call the stored procedure for DDD Version delete
    SqlCommand deleteProc = new SqlCommand();
    SqlParameter procParam;
    try
    {
      if (gvDDDVersions.SelectedIndex < 0 || gvDDDVersions.SelectedValue == null)
        return;
      deleteProc.Connection = (SqlConnection)Session["DBConnection"];
      deleteProc.CommandType = CommandType.StoredProcedure;
      deleteProc.CommandText = "DeleteDDDVersion";
      deleteProc.CommandTimeout = Convert.ToInt32(Resources.DDDVersions.intDeleteProcTimeout);
      procParam = deleteProc.Parameters.Add("@VersionID", SqlDbType.UniqueIdentifier);
      procParam.Value = gvDDDVersions.SelectedValue;
      //Execute the procedure
      deleteProc.ExecuteNonQuery();
      //Show message, that version was deleted
      lbError.Text = String.Format(Resources.DDDVersions.msgVersionDeleted,
                                   gvDDDVersions.SelectedRow.Cells[2].Text, gvDDDVersions.SelectedRow.Cells[1].Text);
      lbError.Visible = true;
    }
    catch(Exception ex)
    {
      lbError.Text = Resources.Resource.msgErrorPrefix + ex.Message;
      lbError.Visible = true;
    }
    finally
    {
      deleteProc.Dispose();
      gvDDDVersions.DataBind();
    }
  }
}