<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="TUTypes.aspx.cs"
  Inherits="Administration_TUTypes" Title="TU Types" %>

<asp:Content ID="cntTUTypes" runat="server" ContentPlaceHolderID="contentPH">
  <table border="0" cellpadding="0" cellspacing="5" style="width: 100%; height: 100%">
    <tr>
      <td colspan="2">
        <h4>
          Telediagnostic Unit Types</h4>
      </td>
    </tr>
    <tr>
      <td colspan="2" style="width: 100%">
        <asp:GridView ID="gvTUTypes" runat="server" AllowPaging="True" AllowSorting="True"
          BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3"
          DataSourceID="dsTUTypes" ForeColor="Black" GridLines="Both" AutoGenerateColumns="False"
          DataKeyNames="TUTypeID" OnDataBound="gvTUTypes_DataBound" OnSelectedIndexChanged="gvTUTypes_SelectedIndexChanged"
          Font-Names="Tahoma" Font-Size="Small">
          <FooterStyle BackColor="#CCCCCC" />
          <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="TypeName" HeaderText="Name" SortExpression="TypeName" />
            <asp:BoundField DataField="Comment" HeaderText="Comment" SortExpression="Comment" />
            <asp:BoundField DataField="TUTypeID" HeaderText="ID" ReadOnly="True" SortExpression="TUTypeID" />
          </Columns>
          <SelectedRowStyle Font-Bold="True" ForeColor="Black" />
          <PagerStyle BackColor="#FFDDDD" ForeColor="Blue" HorizontalAlign="Center" />
          <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
          <AlternatingRowStyle BackColor="#DDDDFF" BorderColor="White" BorderStyle="Solid"
            BorderWidth="2px" />
          <RowStyle BackColor="#DDFFDD" BorderColor="White" BorderStyle="Solid" BorderWidth="2px" />
        </asp:GridView>
        <asp:SqlDataSource ID="dsTUTypes" runat="server" ConnectionString="<%$ ConnectionStrings:CDDBConnectionString %>"
          SelectCommand="SELECT [TypeName], [TUTypeID], [Comment] FROM [TUTypes] ORDER BY [TypeName]"
          DeleteCommand="DELETE FROM TUTypes WHERE (TUTypeID = @TUTypeID)" InsertCommand="INSERT INTO TUTypes(TUTypeID, TypeName, Comment) VALUES (NEWID(), @TypeName, @Comment)"
          UpdateCommand="UPDATE TUTypes SET TypeName = @TypeName, Comment = @Comment WHERE (TUTypeID = @TUTypeID)">
          <DeleteParameters>
            <asp:ControlParameter ControlID="gvTUTypes" Name="TUTypeID" PropertyName="SelectedValue" />
          </DeleteParameters>
          <UpdateParameters>
            <asp:ControlParameter ControlID="tbDetailTUType" Name="TypeName" PropertyName="Text" />
            <asp:ControlParameter ControlID="tbDetailComment" Name="Comment" PropertyName="Text" />
            <asp:ControlParameter ControlID="gvTUTypes" Name="TUTypeID" PropertyName="SelectedValue" />
          </UpdateParameters>
          <InsertParameters>
            <asp:ControlParameter ControlID="tbDetailTUType" Name="TypeName" PropertyName="Text" />
            <asp:ControlParameter ControlID="tbDetailComment" Name="Comment" PropertyName="Text" />
          </InsertParameters>
        </asp:SqlDataSource>
        <br />
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <h4>
          Type details</h4>
      </td>
    </tr>
    <tr>
      <td>
        <h5>
          Name</h5>
      </td>
      <td>
        <asp:TextBox ID="tbDetailTUType" runat="server" Width="300px"></asp:TextBox>
        <asp:Button ID="btUpdate" runat="server" Enabled="False" OnClick="btUpdate_Click"
          Text="Update" Width="70px" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tbDetailTUType"
          ErrorMessage="TU Type name can not be empty" SetFocusOnError="True" Display="None"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="regularValName" runat="server" ControlToValidate="tbDetailTUType"
          Display="None" ErrorMessage="Name has to be maximum 50 characters long" ValidationExpression="(.){1,50}"></asp:RegularExpressionValidator>
      </td>
    </tr>
    <tr>
      <td>
        <h5>
          Comment</h5>
      </td>
      <td>
        <asp:TextBox ID="tbDetailComment" runat="server" Width="300px"></asp:TextBox>
        <asp:Button ID="btNew" runat="server" OnClick="btNew_Click" Text="New" Width="70px" />
        <asp:Button ID="btDelete" runat="server" Enabled="False" OnClick="btDelete_Click"
          Text="Delete" Width="70px" />
        <asp:RegularExpressionValidator ID="regulaValComment" runat="server" ControlToValidate="tbDetailComment"
          Display="None" ErrorMessage="Comment has to be maximum 255 characters long" ValidationExpression=".{0,255}"></asp:RegularExpressionValidator>
      </td>
    </tr>
  </table>
  <div style="margin: 5px">
    <asp:Label ID="lbError" runat="server" EnableViewState="False" ForeColor="Red" Text="Label"
      Visible="False" Width="100%"></asp:Label><br />
    <br />
    <asp:ValidationSummary ID="valSummaryTUTypes" runat="server" Width="100%" />
  </div>
</asp:Content>
