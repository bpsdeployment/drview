<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="RefAttributes.aspx.cs"
  Inherits="Administration_RefAttributes" Title="Referential Attributes" %>

<asp:Content ID="cntRefAttributes" ContentPlaceHolderID="contentPH" runat="Server">
  <table cellspacing="5" style="width: 100%">
    <tr>
      <td colspan="3">
        <h4>
          Attributes List</h4>
    </tr>
    <tr>
      <td>
        <asp:GridView ID="gvRefAttrList" runat="server" AutoGenerateColumns="False" BackColor="White"
          BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" DataKeyNames="RefAttrID"
          DataSourceID="dsRefAttrList" ForeColor="Black" Font-Names="Tahoma" Font-Size="Small"
          OnRowDataBound="gvRefAttrList_RowDataBound" AutoGenerateEditButton="True" OnDataBound="gvRefAttrList_DataBound">
          <FooterStyle BackColor="#CCCCCC" />
          <Columns>
            <asp:CommandField ShowDeleteButton="True" />
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
            <asp:BoundField DataField="Comment" HeaderText="Comment" SortExpression="Comment" />
            <asp:BoundField DataField="RefAttrID" HeaderText="RefAttrID" InsertVisible="False"
              ReadOnly="True" SortExpression="RefAttrID" Visible="False" />
            <asp:CheckBoxField DataField="Added" HeaderText="Added" SortExpression="Added" Visible="False" />
            <asp:CheckBoxField DataField="Deleted" HeaderText="Deleted" SortExpression="Deleted"
              Visible="False" />
          </Columns>
          <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
          <PagerStyle BackColor="#FFDDDD" ForeColor="Blue" HorizontalAlign="Center" />
          <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
          <AlternatingRowStyle BackColor="#DDDDFF" BorderColor="White" BorderStyle="Solid"
            BorderWidth="2px" />
          <RowStyle BackColor="#DDFFDD" BorderColor="White" BorderStyle="Solid" BorderWidth="2px" />
        </asp:GridView>
        <asp:SqlDataSource ID="dsRefAttrList" runat="server" ConnectionString="<%$ ConnectionStrings:CDDBConnectionString %>"
          DeleteCommand="TDM_RefAttrDelete" DeleteCommandType="StoredProcedure" InsertCommand="TDM_RefAttrIns"
          InsertCommandType="StoredProcedure" SelectCommand="TDM_RefAttrSelect" SelectCommandType="StoredProcedure"
          DataSourceMode="DataReader" UpdateCommand="TDM_RefAttrUpd" UpdateCommandType="StoredProcedure">
          <DeleteParameters>
            <asp:ControlParameter ControlID="gvRefAttrList" Name="RefAttrID" PropertyName="SelectedDataKey"
              Type="Int32" />
          </DeleteParameters>
          <InsertParameters>
            <asp:ControlParameter ControlID="tbRefAttrName" Name="Name" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="tbRefAttrComment" Name="Comment" PropertyName="Text"
              Type="String" />
          </InsertParameters>
          <UpdateParameters>
            <asp:ControlParameter ControlID="gvRefAttrList" Name="RefAttrID" PropertyName="SelectedDataKey"
              Type="Int32" />
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="Comment" Type="String" />
          </UpdateParameters>
        </asp:SqlDataSource>
      </td>
    </tr>
  </table>
  <div style="margin: 5px">
    <table>
      <tr>
        <td>
          <h5>
            Name</h5>
          <td>
            <h5>
              Comment</h5>
            <td>
            </td>
      </tr>
      <tr>
        <td>
          <asp:TextBox ID="tbRefAttrName" runat="server" Width="150px"></asp:TextBox></td>
        <td>
          <asp:TextBox ID="tbRefAttrComment" runat="server" Width="250px"></asp:TextBox></td>
        <td>
          <asp:Button ID="btRefAttrIns" runat="server" Text="Insert" Width="80px" OnClick="btRefAttrIns_Click" /></td>
      </tr>
    </table>
  </div>
  <br />
  <table cellspacing="5" style="width: 100%">
    <tr>
      <td>
        <h4>
          Attribute Mappings</h4>
    </tr>
    <tr>
      <td>
        <h5>
          DDD Version</h5>
    </tr>
    <tr>
      <td>
        <asp:DropDownList ID="ddlDDDVersion" runat="server" DataSourceID="dsDDDVersions"
          DataTextField="UserVersion" DataValueField="VersionID" Width="200px" OnSelectedIndexChanged="ddlDDDVersion_SelectedIndexChanged"
          AutoPostBack="True" OnDataBound="ddlDDDVersion_DataBound">
        </asp:DropDownList></td>
    </tr>
    <tr>
      <td>
        <asp:GridView ID="gvRefAttrMapping" runat="server" AutoGenerateColumns="False" BackColor="White"
          BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" DataKeyNames="MappingIndex"
          DataSourceID="dsRefAttrMapping" ForeColor="Black" Font-Names="Tahoma" Font-Size="Small"
          OnRowDataBound="grRefAttrMapping_RowDataBound">
          <FooterStyle BackColor="#CCCCCC" />
          <Columns>
            <asp:CommandField ShowDeleteButton="True" />
            <asp:BoundField DataField="Attribute" HeaderText="Attribute" SortExpression="Attribute" />
            <asp:BoundField DataField="DDD Version" HeaderText="DDD Version" SortExpression="DDD Version" />
            <asp:BoundField DataField="Variable" HeaderText="Variable" SortExpression="Variable" />
            <asp:BoundField DataField="Expression" HeaderText="Expression" SortExpression="Expression" />
            <asp:BoundField DataField="MappingIndex" HeaderText="MappingIndex" SortExpression="MappingIndex"
              Visible="False" />
            <asp:CheckBoxField DataField="Added" HeaderText="Added" SortExpression="Added" Visible="False" />
            <asp:CheckBoxField DataField="Deleted" HeaderText="Deleted" SortExpression="Deleted"
              Visible="False" />
          </Columns>
          <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
          <PagerStyle BackColor="#FFDDDD" ForeColor="Blue" HorizontalAlign="Center" />
          <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
          <AlternatingRowStyle BackColor="#DDDDFF" BorderColor="White" BorderStyle="Solid"
            BorderWidth="2px" />
          <RowStyle BackColor="#DDFFDD" BorderColor="White" BorderStyle="Solid" BorderWidth="2px" />
        </asp:GridView>
        <asp:SqlDataSource ID="dsRefAttrMapping" runat="server" ConnectionString="<%$ ConnectionStrings:CDDBConnectionString %>"
          DataSourceMode="DataReader" DeleteCommand="TDM_RefAttrMapDelete" DeleteCommandType="StoredProcedure"
          InsertCommand="TDM_RefAttrMapIns" InsertCommandType="StoredProcedure" SelectCommand="TDM_RefAttrMapSelect"
          SelectCommandType="StoredProcedure" OnDeleted="dsRefAttrMapping_Deleted">
          <DeleteParameters>
            <asp:Parameter Name="MappingIndex" Type="Int32" />
          </DeleteParameters>
          <InsertParameters>
            <asp:ControlParameter ControlID="ddlAttribute" Name="RefAttrID" PropertyName="SelectedValue"
              Type="Int32" />
            <asp:ControlParameter ControlID="ddlDDDVersion" Name="VersionID" PropertyName="SelectedValue"
              Type="Object" />
            <asp:ControlParameter ControlID="ddlVariable" Name="VariableID" PropertyName="SelectedValue"
              Type="Int32" />
            <asp:ControlParameter ControlID="tbExpression" Name="Expression" PropertyName="Text"
              Type="String" />
          </InsertParameters>
          <SelectParameters>
            <asp:ControlParameter ControlID="ddlDDDVersion" Name="VersionID" PropertyName="SelectedValue"
              Type="Object" />
          </SelectParameters>
        </asp:SqlDataSource>
      </td>
    </tr>
  </table>
  <div style="margin: 5px">
    <table>
      <tr>
        <td width="200">
          <h5>
            Attribute</h5>
        </td>
        <td width="400">
          <h5>
            Variable</h5>
        </td>
        <td width="80">
        </td>
      </tr>
      <tr>
        <td>
          <asp:DropDownList ID="ddlAttribute" runat="server" DataSourceID="dsRefAttrList" DataTextField="Name"
            DataValueField="RefAttrID" Width="200px">
          </asp:DropDownList></td>
        <td>
          <asp:DropDownList ID="ddlVariable" runat="server" Width="400px" DataSourceID="dsVariables"
            DataTextField="Name" DataValueField="VariableID">
          </asp:DropDownList></td>
        <td>
          <asp:Button ID="btRefAttrMapIns" runat="server" Text="Insert" Width="80px" OnClick="btRefAttrMapIns_Click" /></td>
      </tr>
      <tr>
        <td colspan="3">
          <h5>
            Expression</h5>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <asp:TextBox ID="tbExpression" runat="server" Width="680px"></asp:TextBox>
        </td>
      </tr>
    </table>
    <asp:SqlDataSource ID="dsDDDVersions" runat="server" ConnectionString="<%$ ConnectionStrings:CDDBConnectionString %>"
      SelectCommand="TDM_DDDVersionSel" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsVariables" runat="server" ConnectionString="<%$ ConnectionStrings:CDDBConnectionString %>"
      SelectCommand="TDM_EleVarNamesSel" SelectCommandType="StoredProcedure">
      <SelectParameters>
        <asp:ControlParameter ControlID="ddlDDDVersion" DefaultValue="null" Name="VersionID"
          PropertyName="SelectedValue" Type="Object" />
        <asp:Parameter DefaultValue="1" Name="LanguageID" Type="Int32" />
      </SelectParameters>
    </asp:SqlDataSource>
    <table>
      <tr>
        <td colspan="3">
          <h5>
            Statistics</h5>
        </td>
      </tr>
      <tr>
        <td width="200" valign="bottom">
          <h5>
            Processed records</h5>
        </td>
        <td width="200" valign="bottom">
          <h5>
            Unprocessed records</h5>
        </td>
        <td width="80">
        </td>
      </tr>
      <tr>
        <td style="width: 91px" valign="top">
          <asp:Label ID="lProcRecCount" runat="server" Width="137px"></asp:Label>
        </td>
        <td style="width: 140px" valign="top">
          <asp:Label ID="lUnprocRecCount" runat="server" Width="140px"></asp:Label>
        </td>
        <td>
          <asp:Button ID="btGetStats" runat="server" Text="Get statistics" Width="87px" OnClick="btGetStats_Click" /></td>
      </tr>
    </table>
    <asp:SqlDataSource ID="dsRecStatistics" runat="server" ConnectionString="<%$ ConnectionStrings:CDDBConnectionString %>"
      SelectCommand="TDM_RefAttrStatsSel" SelectCommandType="StoredProcedure" UpdateCommand="TDM_RefAttrStatsSel"
      UpdateCommandType="StoredProcedure">
      <UpdateParameters>
        <asp:ControlParameter ControlID="ddlDDDVersion" Name="VersionID" PropertyName="SelectedValue"
          Type="Object" />
        <asp:ControlParameter ControlID="lProcRecCount" Direction="Output" Name="ProcRecCount"
          PropertyName="Text" Type="Int32" />
        <asp:ControlParameter ControlID="lUnprocRecCount" Direction="Output" Name="UnprocRecCount"
          PropertyName="Text" Type="Int32" />
      </UpdateParameters>
      <SelectParameters>
        <asp:ControlParameter ControlID="ddlDDDVersion" Name="VersionID" PropertyName="SelectedValue"
          Type="Object" />
        <asp:ControlParameter ControlID="lProcRecCount" Direction="Output" Name="ProcRecCount"
          PropertyName="Text" Type="Int32" />
        <asp:ControlParameter ControlID="lUnprocRecCount" Direction="Output" Name="UnprocRecCount"
          PropertyName="Text" Type="Int32" />
      </SelectParameters>
    </asp:SqlDataSource>
  </div>
  <br />
  <table cellspacing="5" style="width: 100%">
    <tr>
      <td>
        <h4>
          Commit Changes</h4>
    </tr>
    <tr>
      <td>
        <asp:Button ID="btCommitChanges" runat="server" Font-Bold="True" Font-Names="Tahoma"
          Font-Size="Large" OnClick="btCommitChanges_Click" Text="Commit" /></td>
    </tr>
  </table>
</asp:Content>
