<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="LogViewer.aspx.cs"
  Inherits="Administration_LogViewer" Title="Log viewer" %>

<asp:Content ID="cntLogViewer" ContentPlaceHolderID="contentPH" runat="Server">
  <div style="margin: 5px">
    <table cellspacing="10">
      <tr>
        <td>
          <h5>
            Log file
            <asp:DropDownList ID="ddlLogFile" runat="server" Width="150px">
              <asp:ListItem Selected="True" Value="d:\PokLog.log">Pokusny</asp:ListItem>
              <asp:ListItem Value="d:\Pokus.log">Vadny</asp:ListItem>
            </asp:DropDownList>
          </h5>
        </td>
        <td>
          <h5>
            Show last
            <asp:TextBox ID="tbMaxSize" runat="server" Width="70px">256</asp:TextBox>kB
          </h5>
        </td>
        <td>
          <asp:Button ID="btRefresh" runat="server" Text="Show" />
        </td>
      </tr>
    </table>
    <h4>
      Log</h4>
    <asp:Literal ID="litLog" runat="server" Mode="PassThrough" EnableViewState="False"></asp:Literal>
  </div>
</asp:Content>
