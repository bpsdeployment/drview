using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Administration_TUTypes : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {

  }

  /// <summary>
  /// user selected a row in TU Types grid
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  protected void gvTUTypes_SelectedIndexChanged(object sender, EventArgs e)
  {
    if (gvTUTypes.SelectedIndex == -1)
    { //nothing selected - disable update and delete buttons
      btDelete.Enabled = false;
      btUpdate.Enabled = false;
      return;
    }
    //set the content of detail controls according to master grid
    tbDetailTUType.Text = gvTUTypes.SelectedRow.Cells[1].Text;
    tbDetailComment.Text = gvTUTypes.SelectedRow.Cells[2].Text;
    //enable buttons
    btDelete.Enabled = true;
    btUpdate.Enabled = true;
  }
  protected void btUpdate_Click(object sender, EventArgs e)
  {
    if (gvTUTypes.SelectedIndex == -1)
      return;
    try
    {
      dsTUTypes.Update();
    }
    catch(Exception ex)
    {
      lbError.Text = Resources.Resource.msgErrorPrefix + ex.Message;
      lbError.Visible = true;
    }
  }
  protected void btNew_Click(object sender, EventArgs e)
  {
    try
    {
      dsTUTypes.Insert();
    }
    catch (Exception ex)
    {
      lbError.Text = Resources.Resource.msgErrorPrefix + ex.Message;
      lbError.Visible = true;
    }
  }
  protected void btDelete_Click(object sender, EventArgs e)
  {
    if (gvTUTypes.SelectedIndex == -1)
      return;
    try
    {
      dsTUTypes.Delete();
    }
    catch (Exception ex)
    {
      lbError.Text = Resources.Resource.msgErrorPrefix + ex.Message;
      lbError.Visible = true;
    }
  }
  protected void gvTUTypes_DataBound(object sender, EventArgs e)
  {
    gvTUTypes_SelectedIndexChanged(this, e);
  }

  /// <summary>
  /// Handler for any errors in page not handled in code by catch blocks.
  /// It is actually page-level exception handler for all types of exceptions.
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  private void Page_Error(object sender, EventArgs e)
  {
    //Page error occured - display the text in response
    Response.Write(Resources.Resource.pageErrorText + Server.GetLastError().Message);
    Server.ClearError();
  }
}
