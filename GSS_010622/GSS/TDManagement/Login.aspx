<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Tmp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Telediagnostica login</title>
  <style type="text/css">
   <!--
   h4 {font-weight: bold; font-size: 16px; font-family: Tahoma; border-bottom: 1px solid orange; margin-bottom: 5px; }
   h5 {font-weight: bold; font-size: 13px; font-family: Tahoma; margin-bottom: 2px; }
   h6 {font-weight: bold; font-size: 11px; font-family: Tahoma }
   -->
  </style>
</head>
<body>
  <form id="frmLogin" runat="server">
    <div align="center">
      <h5>
        <asp:Login ID="loginTDManagement" runat="server" BackColor="#F7F6F3" BorderColor="Black"
          BorderPadding="4" BorderStyle="Solid" BorderWidth="1px" DestinationPageUrl="~/Default.aspx"
          ForeColor="Black" MembershipProvider="TelediagnosticaSqlMembershipProvider" TitleText="Telediagnostica Log In"
          LoginButtonText="Log In" UserNameLabelText="User Name:" PasswordLabelText="Password:"
          RememberMeText="Remember me next time">
          <TitleTextStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
          <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
          <LoginButtonStyle BackColor="#FFFBFF" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"
            ForeColor="#284775" Font-Bold="True" />
        </asp:Login>
      </h5>
    </div>
  </form>
</body>
</html>
