<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
      // Code that runs on application startup
      //Clean any leftover session directories
      String[] dirNames;
      try
      {
        dirNames = System.IO.Directory.GetDirectories(Server.MapPath(Resources.Resource.sessionDirsPath));
        foreach (String dir in dirNames)
        {
          try
          {
            System.IO.Directory.Delete(dir, true);
          }
          catch (Exception)
          { }
        }
      }
      catch (Exception)
      { }
      //Create and store DDDHelepr object
      DDDObjects.DDDHelper dddHelper = new DDDObjects.DDDHelper();
      dddHelper.ConnectionString = ConfigurationManager.ConnectionStrings["CDDBConnectionString"].ConnectionString;
      Application["DDDHelper"] = dddHelper;
      //Load set of known telediagnostic units from DB
      dddHelper.LoadTUObjectsFromDB();
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
      // Code that runs when an unhandled error occurs
      Exception ex = Server.GetLastError();
      Session["Exception"] = ex;
      Server.ClearError();
      Response.Redirect("~/Administration/Error.aspx");
    }

    void Session_Start(object sender, EventArgs e) 
    {
      // Code that runs when a new session is started
      //Create new DB connection and store it in Session object
      System.Data.SqlClient.SqlConnection sqlDBConnection = new System.Data.SqlClient.SqlConnection();
      sqlDBConnection.ConnectionString = ConfigurationManager.ConnectionStrings["CDDBConnectionString"].ConnectionString;
      sqlDBConnection.Open();
      Session["DBConnection"] = sqlDBConnection;
      //Create directory for session files and store the path to session state
      String sessionDir;
      sessionDir = Server.MapPath(Resources.Resource.sessionDirsPath + Session.SessionID);
      System.IO.Directory.CreateDirectory(sessionDir);
      Session["SessionDir"] = sessionDir + "\\";
      //Store timeout for DB queries in session state
      int queryTimeout = Int32.Parse(ConfigurationManager.AppSettings["DBQueryTimeoutSec"]);
      Session["QueryTimeout"] = queryTimeout;
    }

    void Session_End(object sender, EventArgs e) 
    {
      // Code that runs when a session ends. 
      // Note: The Session_End event is raised only when the sessionstate mode
      // is set to InProc in the Web.config file. If session mode is set to StateServer 
      // or SQLServer, the event is not raised.
      //Close DB connection
      System.Data.SqlClient.SqlConnection sqlDBConnection = (System.Data.SqlClient.SqlConnection)Session["DBConnection"];
      if (sqlDBConnection != null)
        sqlDBConnection.Close();
      //Delete session directory and all its content
      try
      {
        System.IO.Directory.Delete((String)Session["SessionDir"], true);
      }
      catch (Exception)
      { }
    }

    /// <summary>
    /// Returns new connection to database based on CDDBConnectionString from configuration file
    /// </summary>
    /// <returns></returns>
    System.Data.SqlClient.SqlConnection GetDBConnection()
    {
      return new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["CDDBConnectionString"].ConnectionString);
    }
</script>
