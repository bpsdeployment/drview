<%@ Application Language="C#" %>

<script runat="server">
    void Application_Start(object sender, EventArgs e) 
    {
      // Code that runs on application startup
      TDManagementForms.Logger logger = null;
      try
      {
        //Load fresh application configuration
        ConfigurationManager.RefreshSection("appSettings");
        String xslDir = ConfigurationManager.AppSettings["XSDFilesDir"];
        String importDir = ConfigurationManager.AppSettings["ImportDir"];
        //Create logger
        logger = DDUpload.CreateLogger();
        //Create XslTransformation and load xsl file
        System.Xml.Xsl.XslCompiledTransform DRMessTransform = new System.Xml.Xsl.XslCompiledTransform();
        DRMessTransform.Load(xslDir + Resources.DDUpload.xslFileName);
        //Get ID for new DR file
        uint DRFileID = DDUpload.GetNextFreeDRFileID(importDir);
        //Create name for output file
        String DRFileName = importDir + DRFileID.ToString("000000") + ".xml";
        //Create stream for output file
        System.IO.FileStream DRFileStream = new System.IO.FileStream(DRFileName, System.IO.FileMode.Create, 
                                                                     System.IO.FileAccess.Write, System.IO.FileShare.None);
        //Create XML writer for the stream
        System.Xml.XmlWriterSettings DRWriterSettings = new System.Xml.XmlWriterSettings();
        DRWriterSettings.Indent = true;
        DRWriterSettings.Encoding = Encoding.UTF8;
        DRWriterSettings.OmitXmlDeclaration = true;
        DRWriterSettings.ConformanceLevel = System.Xml.ConformanceLevel.Fragment;
        System.Xml.XmlWriter DRXmlWriter = System.Xml.XmlWriter.Create(DRFileStream, DRWriterSettings);
        //Create FileInfo for DR File
        System.IO.FileInfo DRFileInfo = new System.IO.FileInfo(DRFileName);
        //Store objects in ApplicationState
        Application["DRMessTransform"] = DRMessTransform;
        Application["DRFileID"] = DRFileID;
        Application["DRFileStream"] = DRFileStream;
        Application["DRXmlWriter"] = DRXmlWriter;
        Application["DRFileInfo"] = DRFileInfo;
        Application["Logger"] = logger;
        //Create and store validating document for DDMessage
        Application["ValDocument"] = DDUpload.CreateValidatingDocument(xslDir + Resources.DDUpload.xsdDDMessageName);
        logger.LogText(0, "DDUpload", Resources.DDUpload.msgApplicationStart);
      }
      catch (Exception ex)
      {
        if (logger != null)
          logger.LogText(0, "DDUpload", Resources.DDUpload.msgErrApplicationStart, ex.Message);
        throw ex;
      }
    }
    
    void Application_End(object sender, EventArgs e) 
    {
      TDManagementForms.Logger logger = Application["Logger"] as TDManagementForms.Logger;
      if (logger != null)
        logger.LogText(0, "DDUpload", Resources.DDUpload.msgApplicationEnd);
      //  Code that runs on application shutdown
      System.Xml.XmlWriter DRXmlWriter = (System.Xml.XmlWriter)Application["DRXmlWriter"];
      if (DRXmlWriter != null)
        DRXmlWriter.Close();
      System.IO.FileStream DRFileStream = (System.IO.FileStream)Application["DRFileStream"];
      if (DRFileStream != null)
        DRFileStream.Dispose();
      
    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e) 
    {
      // Code that runs when a new session is started
      //Load fresh application configuration
      ConfigurationManager.RefreshSection("appSettings");
    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
     
</script>
