using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using DDDObjects;
using TDManagementForms;

namespace DDExpertExchange
{
  /// <summary>
  /// Class represent one configuration of record export
  /// Defines fault codes, output columns and record sources for which this configuration is used
  /// </summary>
  class RecordExport
  {
    #region Private members
    Logger logger;
    DDDHelper dddHelper;
    List<VariableColumn> columns; //List of columns to export
    #endregion //Private members

    #region Public members
    public String UserVersion; //DDD Version for this configuration
    #endregion //Public members

    #region Construction initialization
    /// <summary>
    /// Public constructor
    /// </summary>
    /// <param name="logger">Logger object for output messages</param>
    /// <param name="dddHelper">DDD Helper object</param>
    /// <param name="configElem">XML Element with source configuration</param>
    public RecordExport(Logger logger, DDDHelper dddHelper, XmlElement configElem)
    {
      this.logger = logger;
      this.dddHelper = dddHelper;
      //Load the configuration
      LoadConfiguration(configElem);
    }

    /// <summary>
    /// Loads source configuration from given XML element
    /// </summary>
    /// <param name="configElem">Configuration XML element</param>
    private void LoadConfiguration(XmlElement configElem)
    {
      try
      {
        UserVersion = configElem.GetAttribute("DDDVersion");
        //Load columns configuration
        columns = new List<VariableColumn>();
        foreach (XmlElement colElem in configElem.SelectSingleNode("Columns").ChildNodes)
          columns.Add(VariableColumn.CreateFromXMLElem(colElem, dddHelper));
        logger.LogText(2, "RecordExport", "Loaded configuration for version {0} with {1} columns", UserVersion, columns.Count);
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to load the configuration for record source", ex);
      }
    }
    #endregion //Construction initialization

    #region Public methods
    /// <summary>
    /// Transforms one given record into string line according to current configuration.
    /// Record binary data are parsed.
    /// </summary>
    /// <param name="recordRow">Row with record loaded from DB</param>
    /// <returns>Text line with transformed record</returns>
    public String TransformRecord(DiagnosticRecords.SelRecordsRow recordRow)
    {
      //First we have to parse the binary data
      List<sParsedVarValue> parsedVals = new List<sParsedVarValue>();
      if (!recordRow.IsBinDataNull())
      { //Binary data exist - parse it
        sRecBinData recBinData = new sRecBinData();
        recBinData.BigEndian = recordRow.BigEndian;
        recBinData.BinData = recordRow.BinData;
        recBinData.DDDVersionID = recordRow.DDDVersionID;
        recBinData.EndTime = DateTime.MaxValue; //No need for EndTime - we want non historized inputs only
        recBinData.RecordDefID = recordRow.RecordDefID;
        recBinData.RecordInstID = recordRow.RecordInstID;
        recBinData.StartTime = recordRow.StartTime;
        recBinData.TUInstanceID = recordRow.TUInstanceID;
        if (dddHelper.ParseBinRecord(recBinData, out parsedVals) != eParseStatus.parseSuccessfull)
          throw new Exception("Failed to parse record binary data");
      }
      //Now prepare empty output line
      String line = String.Empty;
      //Transform each column, append result to output line
      foreach (VariableColumn col in columns)
        line += col.FormatValue(recordRow, parsedVals) + "  ";
      //Return the result
      return line;
    }
    #endregion //Public methods
  }

  /// <summary>
  /// Defines one source of records TU and source filter
  /// </summary>
  class RecordSource
  {
    public String TUName;
    public String Source;
    public DDDVersion DDDVersion = null;
    public Guid? TUInstanceID = null;

    public RecordSource(XmlElement confElem)
    {
      TUName = confElem.GetAttribute("TUName");
      Source = confElem.GetAttribute("Source");
    }
  }
}
