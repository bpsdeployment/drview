using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Globalization;
using DDDObjects;

namespace DDExpertExchange
{
  abstract class VariableColumn
  {
    protected String VarName;
    protected String Format;
    protected DDDHelper dddHelper;
    public static NumberFormatInfo formatInfo = new NumberFormatInfo();

    /// <summary>
    /// Creates appropriate descendant of VariableColumn according to the name of given configuration element
    /// </summary>
    /// <param name="configElem">Configuration element from which column is created</param>
    /// <param name="dddHelper">DDD Helper object</param>
    /// <returns>Object of class inherited from VariableColumn</returns>
    public static VariableColumn CreateFromXMLElem(XmlElement configElem, DDDHelper dddHelper)
    {
      //Determine type of the column from name of element
      switch (configElem.Name)
      {
        case "SpecialColumn":
          return new SpecialColumn(configElem, dddHelper);
        case "BitColumn":
          return new BitColumn(configElem, dddHelper);
        case "NumberColumn":
          return new NumberColumn(configElem, dddHelper);
        default:
          throw new Exception("Unknown type of column " + configElem.Name);
      }
    }

    /// <summary>
    /// Public constructor - creates the object from XML configuration element
    /// </summary>
    /// <param name="configElem">XML configuration element</param>
    /// <param name="dddHelper">DDD Helper object</param>
    public VariableColumn(XmlElement configElem, DDDHelper dddHelper)
    {
      VarName = configElem.GetAttribute("VarName");
      Format = configElem.GetAttribute("Format");
      this.dddHelper = dddHelper;
    }

    /// <summary>
    /// Returns string representing value of this column for given record and parsed values
    /// </summary>
    /// <param name="record">Record row from DB</param>
    /// <param name="parsedValues">Parsed binary data</param>
    /// <returns>String value of the column</returns>
    public abstract String FormatValue(DiagnosticRecords.SelRecordsRow record, List<sParsedVarValue> parsedValues);
  }

  class NumberColumn : VariableColumn
  {
    public NumberColumn(XmlElement configElem, DDDHelper dddHelper)
      : base(configElem, dddHelper)
    {
    }

    public override string FormatValue(DiagnosticRecords.SelRecordsRow record, List<sParsedVarValue> parsedValues)
    {
      //Obtain DDDVersion object from helper
      DDDVersion version = dddHelper.GetVersion(record.DDDVersionID);
      //Obtain variable object
      DDDVariable variable = version.GetVariableByName(VarName, ".");
      //Check that it is elementary variable
      DDDElemVar elemVar;
      if (variable.GetType() == typeof(DDDElemVar))
        elemVar = (DDDElemVar)variable;
      else
        throw new Exception(String.Format("Variable specified by name {0} is not elementary variable", VarName));
      //Find parsed value by VariableID
      foreach (sParsedVarValue value in parsedValues)
        if (value.VariableID == elemVar.VariableID && value.ValueArrayIndex == 1 && value.ValueIndex == 0)
        { //Found variable with proper ID from non historized input
          //Return formatted value
          return String.Format(formatInfo, "{0:" + Format + "}", value.Value);
          //return elemVar.Representation.DataTypeObj.FormatValue(value.Value, Format, 1);
        }
      //No value for variable found
      throw new Exception("No parsed value for variable " + VarName);
    }
  }

  class BitColumn : VariableColumn
  {
    protected Int64 mask; //Bit mask
    protected int bitNumber; //Bit number

    public BitColumn(XmlElement configElem, DDDHelper dddHelper)
      : base(configElem, dddHelper)
    {
      try
      {
        //Read bit number and compute mask
        bitNumber = Int32.Parse(configElem.GetAttribute("BitNumber"));
        mask = 1;
        mask <<= bitNumber;
      }
      catch(Exception)
      {
        throw new Exception("Invalid bit number specified for bit column of variable " + VarName);
      }
    }

    public override string FormatValue(DiagnosticRecords.SelRecordsRow record, List<sParsedVarValue> parsedValues)
    {
      //Obtain DDDVersion object from helper
      DDDVersion version = dddHelper.GetVersion(record.DDDVersionID);
      //Obtain variable object
      DDDVariable variable = version.GetVariableByName(VarName, ".");
      //Check that it is elementary variable with bitset representation
      DDDBitsetRepres bitsetRepres;
      if (variable.GetType() == typeof(DDDElemVar) && ((DDDElemVar)variable).Representation.GetType() == typeof(DDDBitsetRepres))
        bitsetRepres = (DDDBitsetRepres)((DDDElemVar)variable).Representation;
      else
        throw new Exception(String.Format("Variable specified by name {0} is not an elementary bitset variable", VarName));
      //Find parsed value by VariableID
      foreach (sParsedVarValue value in parsedValues)
        if (value.VariableID == variable.VariableID && value.ValueArrayIndex == 1 && value.ValueIndex == 0)
        { //Found variable with proper ID from non historized input
          //Obtain value of given bit
          Int32 bitValue = Convert.ToInt32(bitsetRepres.GetBitValue(value.Value, mask));
          //Return formatted value
          return bitValue.ToString(Format, formatInfo);
        }
      //No value for variable found
      throw new Exception(String.Format("No parsed value for variable {0} bit {1}", VarName, bitNumber));
    }
  }

  class SpecialColumn : VariableColumn
  {
    private static String knownSpecialColumns = "FaultCode StartTime EndTime";
    private static DateTime unixStartTime = new DateTime(1970,1,1,0,0,0); //Start time of UNIX calendar

    public SpecialColumn(XmlElement configElem, DDDHelper dddHelper)
      : base(configElem, dddHelper)
    {
      //Check column name for known special columns
      if (!knownSpecialColumns.Contains(VarName))
        throw new Exception("Unknown name of special column: " + VarName);
    }

    public override string FormatValue(DiagnosticRecords.SelRecordsRow record, List<sParsedVarValue> parsedValues)
    {
      //Return string according to name of special column
      switch (VarName)
      {
        case "FaultCode":
          DDDRecord recObj = dddHelper.GetRecord(record.DDDVersionID, record.RecordDefID);
          if (recObj.GetType() == typeof(DDDEventRecord))
            return ((DDDEventRecord)recObj).FaultCode.ToString(Format, formatInfo);
          else
            throw new Exception(String.Format("FaultCode cannot be obtained for record {0} - it is not an event record", recObj.Name));
        case "StartTime":
          TimeSpan span = record.StartTime - unixStartTime;
          int seconds = (int)span.TotalSeconds;
          return seconds.ToString(Format, formatInfo);
        case "EndTime":
          if (record.IsEndTimeNull())
            span = TimeSpan.FromTicks(0);
          else
            span = record.EndTime - unixStartTime;
          seconds = (int)span.TotalSeconds;
          return seconds.ToString(Format, formatInfo);
        default:
          throw new Exception("Unknown name of special column: " + VarName);
      }
    }
  }
}
