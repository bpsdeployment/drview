
;;;======================================================
;;;     Trenitalia
;;;
;;;     TELEDIAG
;;;     Sistema Esperto
;;;     v0.04
;;;     Dipartimento di Ingegneria Industriale - UniPG
;;;     Prof. F. Mariani
;;;     Ing. A. Cancellieri
;;;======================================================

(defmodule MAIN (export ?ALL))

;;****************
;;* DEFFUNCTIONS *
;;****************

(deffunction MAIN::ask-question (?question ?allowed-values)
   (printout t ?question)
   (bind ?answer (read))
   (if (lexemep ?answer) then (bind ?answer (lowcase ?answer)))
   (while (not (member ?answer ?allowed-values)) do
      (printout t ?question)
      (bind ?answer (read))
      (if (lexemep ?answer) then (bind ?answer (lowcase ?answer))))
   ?answer)

;;******************
;;* STATO INIZIALE *
;;******************

(deftemplate MAIN::attribute
   (slot name)
   (slot value)
   (slot certainty (default 100.0)))

(defrule MAIN::start
  (declare (salience 10000))
  =>
  (set-fact-duplication TRUE)
  (focus QUESTIONS SCELTA-CAUSE INTERVENTI PRINT-RESULTS))

(defrule MAIN::combine-certainties ""
  (declare (salience 100)
           (auto-focus TRUE))
  ?rem1 <- (attribute (name ?rel) (value ?val) (certainty ?per1))
  ?rem2 <- (attribute (name ?rel) (value ?val) (certainty ?per2))
  (test (neq ?rem1 ?rem2))
  =>
  (retract ?rem1)
  (modify ?rem2 (certainty (/ (- (* 100 (+ ?per1 ?per2)) (* ?per1 ?per2)) 100))))
  
;;************************
;;* REGOLE SULLE DOMANDE *
;;************************

(defmodule QUESTIONS (import MAIN ?ALL) (export ?ALL))

(deftemplate QUESTIONS::question
   (slot attribute (default ?NONE))
   (slot the-question (default ?NONE))
   (multislot valid-answers (default ?NONE))
   (slot already-asked (default FALSE))
   (multislot precursors (default ?DERIVE)))
   
(defrule QUESTIONS::ask-a-question
   ?f <- (question (already-asked FALSE)
                   (precursors)
                   (the-question ?the-question)
                   (attribute ?the-attribute)
                   (valid-answers $?valid-answers))
   =>
   (modify ?f (already-asked TRUE))
   (assert (attribute (name ?the-attribute)
                      (value (ask-question ?the-question ?valid-answers)))))

(defrule QUESTIONS::precursor-is-satisfied
   ?f <- (question (already-asked FALSE)
                   (precursors ?name is ?value $?rest))
         (attribute (name ?name) (value ?value))
   =>
   (if (eq (nth 1 ?rest) and) 
    then (modify ?f (precursors (rest$ ?rest)))
    else (modify ?f (precursors ?rest))))

(defrule QUESTIONS::precursor-is-not-satisfied
   ?f <- (question (already-asked FALSE)
                   (precursors ?name is-not ?value $?rest))
         (attribute (name ?name) (value ~?value))
   =>
   (if (eq (nth 1 ?rest) and) 
    then (modify ?f (precursors (rest$ ?rest)))
    else (modify ?f (precursors ?rest))))

;;***************************
;;* DOMANDE TELEDIAGNOSTICA *
;;***************************

(defmodule TELEDIAG-QUESTIONS (import QUESTIONS ?ALL))

(deffacts TELEDIAG-QUESTIONS::question-attributes

(question (attribute DC521SEC)
            (the-question "DC521SEC = ")
            (valid-answers a b c d))

(question (attribute DC520SEC)
            (the-question "DC520SEC = ")
            (valid-answers a b c d))

(question (attribute DC519SEC)
            (the-question "DC519SEC = ")
            (valid-answers a b c d))

(question (attribute DC518SEC)
            (the-question "DC518SEC = ")
            (valid-answers a b c d))

(question (attribute DC145SPD)
            (the-question "DC145SPD = ")
            (valid-answers a b c d))

(question (attribute DC144SPD)
            (the-question "DC144SPD = ")
            (valid-answers a b c d))

(question (attribute DC145SEC)
            (the-question "DC145SEC = ")
            (valid-answers a b c d))

(question (attribute DC144SEC)
            (the-question "DC144SEC = ")
            (valid-answers a b c d))

(question (attribute KLIP2042)
            (the-question "KLIP2042 = ")
            (valid-answers a b c))

(question (attribute KLIP2041)
            (the-question "KLIP2041 = ")
            (valid-answers a b c))

(question (attribute DC014VO)
            (the-question "DC014VO = ")
            (valid-answers a b c d))

(question (attribute DC013VO)
            (the-question "DC013VO = ")
            (valid-answers a b c d))

(question (attribute DC012VO)
            (the-question "DC012VO = ")
            (valid-answers a b c d))

(question (attribute DC011VO)
            (the-question "DC011VO = ")
            (valid-answers a b c d))

(question (attribute DC560)
            (the-question "DC560 = ")
            (valid-answers a b c d))

(question (attribute DC559)
            (the-question "DC559 = ")
            (valid-answers a b c d))

(question (attribute DC521)
            (the-question "DC521 = ")
            (valid-answers a b c d))

(question (attribute DC520)
            (the-question "DC520 = ")
            (valid-answers a b c d))

(question (attribute DC519)
            (the-question "DC519 = ")
            (valid-answers a b c d))

(question (attribute DC518)
            (the-question "DC518 = ")
            (valid-answers a b c d))

(question (attribute DC445)
            (the-question "DC445 = ")
            (valid-answers a b c d))

(question (attribute DC433)
            (the-question "DC433 = ")
            (valid-answers a b c d))

(question (attribute DC432)
            (the-question "DC432 = ")
            (valid-answers a b c d))

(question (attribute DC431)
            (the-question "DC431 = ")
            (valid-answers a b c d))

(question (attribute DC429)
            (the-question "DC429 = ")
            (valid-answers a b c d))

(question (attribute DC428)
            (the-question "DC428 = ")
            (valid-answers a b c d))

(question (attribute DC421)
            (the-question "DC421 = ")
            (valid-answers a b c d))

(question (attribute DC225)
            (the-question "DC225 = ")
            (valid-answers a b c d))

(question (attribute DC224)
            (the-question "DC224 = ")
            (valid-answers a b c d))

(question (attribute DC207)
            (the-question "DC207 = ")
            (valid-answers a b c d))

(question (attribute DC206)
            (the-question "DC206 = ")
            (valid-answers a b c d))

(question (attribute DC178)
            (the-question "DC178 = ")
            (valid-answers a b c d))

(question (attribute DC170)
            (the-question "DC170 = ")
            (valid-answers a b c d))

(question (attribute DC169)
            (the-question "DC169 = ")
            (valid-answers a b c d))

(question (attribute DC145)
            (the-question "DC145 = ")
            (valid-answers a b c d))

(question (attribute DC144)
            (the-question "DC144 = ")
            (valid-answers a b c d))

(question (attribute DC143)
            (the-question "DC143 = ")
            (valid-answers a b c d))

(question (attribute DC142)
            (the-question "DC142 = ")
            (valid-answers a b c d))

(question (attribute DC124)
            (the-question "DC124 = ")
            (valid-answers a b c d))

(question (attribute DC123)
            (the-question "DC123 = ")
            (valid-answers a b c d))

(question (attribute DC117)
            (the-question "DC117 = ")
            (valid-answers a b c d))

(question (attribute DC116)
            (the-question "DC116 = ")
            (valid-answers a b c d))

(question (attribute DC076)
            (the-question "DC076 = ")
            (valid-answers a b c d))

(question (attribute DC014)
            (the-question "DC014 = ")
            (valid-answers a b c d))

(question (attribute DC013)
            (the-question "DC013 = ")
            (valid-answers a b c d))

(question (attribute DC012)
            (the-question "DC012 = ")
            (valid-answers a b c d))

(question (attribute DC011)
            (the-question "DC011 = ")
            (valid-answers a b c d))

(question (attribute TEMPO)
            (the-question "TEMPO = ")
            (valid-answers a b c d))

)
;;******************
;; MODULO REGOLE
;;******************

(defmodule RULES (import MAIN ?ALL) (export ?ALL))

(deftemplate RULES::rule
  (slot certainty (default 100.0))
  (multislot if)
  (multislot then))

(defrule RULES::throw-away-ands-in-antecedent
  ?f <- (rule (if and $?rest))
  =>
  (modify ?f (if ?rest)))

(defrule RULES::throw-away-ands-in-consequent
  ?f <- (rule (then and $?rest))
  =>
  (modify ?f (then ?rest)))

(defrule RULES::remove-is-condition-when-satisfied
  ?f <- (rule (certainty ?c1) 
              (if ?attribute is ?value $?rest))
  (attribute (name ?attribute) 
             (value ?value) 
             (certainty ?c2))
  =>
  (modify ?f (certainty (min ?c1 ?c2)) (if ?rest)))

(defrule RULES::remove-is-not-condition-when-satisfied
  ?f <- (rule (certainty ?c1) 
              (if ?attribute is-not ?value $?rest))
  (attribute (name ?attribute) (value ~?value) (certainty ?c2))
  =>
  (modify ?f (certainty (min ?c1 ?c2)) (if ?rest)))

(defrule RULES::perform-rule-consequent-with-certainty
  ?f <- (rule (certainty ?c1) 
              (if) 
              (then ?attribute is ?value with certainty ?c2 $?rest))
  =>
  (modify ?f (then ?rest))
  (assert (attribute (name ?attribute) 
                     (value ?value)
                     (certainty (/ (* ?c1 ?c2) 100)))))

(defrule RULES::perform-rule-consequent-without-certainty
  ?f <- (rule (certainty ?c1)
              (if)
              (then ?attribute is ?value $?rest))
  (test (or (eq (length$ ?rest) 0)
            (neq (nth 1 ?rest) with)))
  =>
  (modify ?f (then ?rest))
  (assert (attribute (name ?attribute) (value ?value) (certainty ?c1))))

;;***************************
;;* REGOLE TELEDIAG CAUSE *
;;***************************

(defmodule SCELTA-CAUSE (import RULES ?ALL)
                          (import QUESTIONS ?ALL)
                          (import MAIN ?ALL))

(defrule SCELTA-CAUSE::startit => (focus RULES))

(deffacts regole-intervento

  ; Regole per stabilire la priorita

  (rule (if TEMPO is a)
        (then priorita is alta))

  (rule (if TEMPO is b)
        (then priorita is bassa with certainty 30 and
              priorita is media with certainty 60 and
              priorita is alta with certainty 30))

  (rule (if TEMPO is c)
        (then priorita is media with certainty 40 and
              priorita is alta with certainty 80))

  (rule (if TEMPO is d)
        (then priorita is alta))

  ; Regole per il caricabatterie

  (rule (if DC123 is c and
            KLIP2041 is b)
        (then carbatt is controllo1 with certainty 80))

  (rule (if DC123 is d and
            KLIP2041 is b)
        (then carbatt is controllo1 with certainty 90))

  (rule (if DC123 is c and
            KLIP2041 is a)
        (then carbatt is potenza1 with certainty 80))

  (rule (if DC123 is d and
            KLIP2041 is a)
        (then carbatt is potenza1 with certainty 90))

  (rule (if DC123 is c and
            KLIP2041 is a)
        (then carbatt is potenza1 with certainty 50))

  (rule (if DC123 is c and
            KLIP2041 is b)
        (then carbatt is controllo1 with certainty 50))

  (rule (if DC123 is d and
            KLIP2041 is a)
        (then carbatt is potenza1 with certainty 50))

  (rule (if DC123 is d and
            KLIP2041 is b)
        (then carbatt is controllo1 with certainty 50))

  (rule (if DC124 is c and
            KLIP2042 is b)
        (then carbatt is controllo2 with certainty 80))

  (rule (if DC124 is d and
            KLIP2042 is b)
        (then carbatt is controllo2 with certainty 90))

  (rule (if DC124 is c and
            KLIP2042 is a)
        (then carbatt is potenza2 with certainty 80))

  (rule (if DC124 is d and
            KLIP2042 is a)
        (then carbatt is potenza2 with certainty 90))

  (rule (if DC124 is c and
            KLIP2042 is a)
        (then carbatt is potenza2 with certainty 50))

  (rule (if DC124 is c and
            KLIP2042 is b)
        (then carbatt is controllo2 with certainty 50))

  (rule (if DC124 is d and
            KLIP2042 is a)
        (then carbatt is potenza2 with certainty 50))

  (rule (if DC124 is d and
            KLIP2042 is b)
        (then carbatt is controllo2 with certainty 50))

  ; Regole per la comunicazione tra CCU e TCU

  (rule (if DC011 is a and
            DC011V0 is a)
        (then comm is ignor11 with certainty 95))

  (rule (if DC011 is a and
            DC011V0 is b)
        (then comm is ignor11 with certainty 95))

  (rule (if DC011 is b and
            DC011V0 is a)
        (then comm is ignor11 with certainty 90))

  (rule (if DC011 is b and
            DC011V0 is b)
        (then comm is ignor11 with certainty 80))

  (rule (if DC011 is c and
            DC011V0 is c)
        (then comm is vercabl11 with certainty 80))

  (rule (if DC011 is d and
            DC011V0 is c)
        (then comm is vercabl11 with certainty 90))

  (rule (if DC011 is c and
            DC011V0 is d)
        (then comm is vercabl11 with certainty 90))

  (rule (if DC011 is d and
            DC011V0 is d)
        (then comm is vercabl11 with certainty 95))

  (rule (if DC012 is a and
            DC012V0 is a)
        (then comm is ignor12 with certainty 95))

  (rule (if DC012 is a and
            DC012V0 is b)
        (then comm is ignor12 with certainty 95))

  (rule (if DC012 is b and
            DC012V0 is a)
        (then comm is ignor12 with certainty 90))

  (rule (if DC012 is b and
            DC012V0 is b)
        (then comm is ignor12 with certainty 80))

  (rule (if DC012 is c and
            DC012V0 is c)
        (then comm is vercabl12 with certainty 80))

  (rule (if DC012 is d and
            DC012V0 is c)
        (then comm is vercabl12 with certainty 90))

  (rule (if DC012 is c and
            DC012V0 is d)
        (then comm is vercabl12 with certainty 90))

  (rule (if DC012 is d and
            DC012V0 is d)
        (then comm is vercabl12 with certainty 95))

  (rule (if DC013 is a and
            DC013V0 is a)
        (then comm is ignor13 with certainty 95))

  (rule (if DC013 is a and
            DC013V0 is b)
        (then comm is ignor13 with certainty 95))

  (rule (if DC013 is b and
            DC013V0 is a)
        (then comm is ignor13 with certainty 90))

  (rule (if DC013 is b and
            DC013V0 is b)
        (then comm is ignor13 with certainty 80))

  (rule (if DC013 is c and
            DC013V0 is c)
        (then comm is vercabl13 with certainty 80))

  (rule (if DC013 is d and
            DC013V0 is c)
        (then comm is vercabl13 with certainty 90))

  (rule (if DC013 is c and
            DC013V0 is d)
        (then comm is vercabl13 with certainty 90))

  (rule (if DC013 is d and
            DC013V0 is d)
        (then comm is vercabl13 with certainty 95))

  (rule (if DC014 is a and
            DC014V0 is a)
        (then comm is ignor14 with certainty 95))

  (rule (if DC014 is a and
            DC014V0 is b)
        (then comm is ignor14 with certainty 95))

  (rule (if DC014 is b and
            DC014V0 is a)
        (then comm is ignor14 with certainty 90))

  (rule (if DC014 is b and
            DC014V0 is b)
        (then comm is ignor14 with certainty 80))

  (rule (if DC014 is c and
            DC014V0 is c)
        (then comm is vercabl14 with certainty 80))

  (rule (if DC014 is d and
            DC014V0 is c)
        (then comm is vercabl14 with certainty 90))

  (rule (if DC014 is c and
            DC014V0 is d)
        (then comm is vercabl14 with certainty 90))

  (rule (if DC014 is d and
            DC014V0 is d)
        (then comm is vercabl14 with certainty 95))

  ; Regole per i pantografi

  (rule (if DC144 is b and
            DC144SEC is a
            DC144SPD is a)
        (then pantografi is taratura1 with certainty 70))

  (rule (if DC144 is c and
            DC144SEC is a
            DC144SPD is a)
        (then pantografi is taratura1 with certainty 80))
 
  (rule (if DC144 is d and
            DC144SEC is a
            DC144SPD is a)
        (then pantografi is taratura1 with certainty 90))

  (rule (if DC144 is b and
            DC144SEC is a
            DC144SPD is b)
        (then pantografi is strisc1 with certainty 70))

  (rule (if DC144 is c and
            DC144SEC is a
            DC144SPD is b)
        (then pantografi is strisc1 with certainty 80))
 
  (rule (if DC144 is d and
            DC144SEC is a
            DC144SPD is b)
        (then pantografi is strisc1 with certainty 90))

  (rule (if DC145 is b and
            DC145SEC is a
            DC145SPD is a)
        (then pantografi is taratura2 with certainty 70))

  (rule (if DC145 is c and
            DC145SEC is a
            DC145SPD is a)
        (then pantografi is taratura2 with certainty 80))
 
  (rule (if DC145 is d and
            DC145SEC is a
            DC145SPD is a)
        (then pantografi is taratura2 with certainty 90))

  (rule (if DC145 is b and
            DC145SEC is a
            DC145SPD is b)
        (then pantografi is strisc2 with certainty 70))

  (rule (if DC145 is c and
            DC145SEC is a
            DC145SPD is b)
        (then pantografi is strisc2 with certainty 80))
 
  (rule (if DC145 is d and
            DC145SEC is a
            DC145SPD is b)
        (then pantografi is strisc2 with certainty 90))

  ; Regole per l'olio assi

  (rule (if DC518 is c and
            DC518SEC is a)
        (then olio-assi is oscill1 with certainty 90))

  (rule (if DC518 is d and
            DC518SEC is a)
        (then olio-assi is oscill1 with certainty 95))

  (rule (if DC518 is c and
            DC518SEC is b)
        (then olio-assi is livello1 with certainty 70 and
              olio-assi is livelst1 with certainty 50))

  (rule (if DC518 is d and
            DC518SEC is b)
        (then olio-assi is livello1 with certainty 90 and
              olio-assi is livelst1 with certainty 30))

  (rule (if DC519 is c and
            DC519SEC is a)
        (then olio-assi is oscill2 with certainty 90))

  (rule (if DC519 is d and
            DC519SEC is a)
        (then olio-assi is oscill2 with certainty 95))

  (rule (if DC519 is c and
            DC519SEC is b)
        (then olio-assi is livello2 with certainty 70 and
              olio-assi is livelst2 with certainty 50))

  (rule (if DC519 is d and
            DC519SEC is b)
        (then olio-assi is livello2 with certainty 90 and
              olio-assi is livelst2 with certainty 30))

  (rule (if DC520 is c and
            DC520SEC is a)
        (then olio-assi is oscill3 with certainty 90))

  (rule (if DC520 is d and
            DC520SEC is b)
        (then olio-assi is oscill3 with certainty 95))

  (rule (if DC520 is c and
            DC520SEC is b)
        (then olio-assi is livello3 with certainty 70 and
              olio-assi is livelst3 with certainty 50))

  (rule (if DC520 is d and
            DC520SEC is b)
        (then olio-assi is livello3 with certainty 90 and
              olio-assi is livelst3 with certainty 30))

  (rule (if DC521 is c and
            DC521SEC is a)
        (then olio-assi is oscill4 with certainty 90))

  (rule (if DC521 is d and
            DC521SEC is a)
        (then olio-assi is oscill4 with certainty 95))

  (rule (if DC521 is c and
            DC521SEC is b)
        (then olio-assi is livello4 with certainty 70 and
              olio-assi is livelst4 with certainty 50))

  (rule (if DC521 is d and
            DC521SEC is b)
        (then olio-assi is livello4 with certainty 90 and
              olio-assi is livelst4 with certainty 30))

)

;;********************************
;;* REGOLE DI SCELTA INTERVENTO  *
;;********************************

(defmodule INTERVENTI (import MAIN ?ALL))

(deffacts any-attributes
  (attribute (name carbatt) (value any))
  (attribute (name priorita) (value any))
  (attribute (name pantografi) (value any))
  (attribute (name olio-assi) (value any))
  (attribute (name comm) (value any))
)

(deftemplate INTERVENTI::intervento
  (slot name (default ?NONE))
  (multislot carbatt (default any))
  (multislot priorita (default any))
  (multislot pantografi (default any))
  (multislot olio-assi (default any))
  (multislot comm (default any))
)

(deffacts INTERVENTI::lista-interventi 
  (intervento (name 3001) (olio-assi livelst1) (priorita bassa media alta))
  (intervento (name 3002) (olio-assi livello1) (priorita bassa media alta))
  (intervento (name 3003) (olio-assi oscill1) (priorita bassa media alta))
  (intervento (name 3004) (olio-assi livelst2) (priorita bassa media alta))
  (intervento (name 3005) (olio-assi livello2) (priorita bassa media alta))
  (intervento (name 3006) (olio-assi oscill2) (priorita bassa media alta))
  (intervento (name 3007) (olio-assi livelst3) (priorita bassa media alta))
  (intervento (name 3008) (olio-assi livello3) (priorita bassa media alta))
  (intervento (name 3009) (olio-assi oscill3) (priorita bassa media alta))
  (intervento (name 3010) (olio-assi livelst4) (priorita bassa media alta))
  (intervento (name 3011) (olio-assi livello4) (priorita bassa media alta))
  (intervento (name 3012) (olio-assi oscill4) (priorita bassa media alta))
  (intervento (name 3013) (pantografi taratura1) (priorita bassa media alta))
  (intervento (name 3014) (pantografi strisc1) (priorita bassa media alta))
  (intervento (name 3015) (pantografi taratura2) (priorita bassa media alta))
  (intervento (name 3016) (pantografi strisc2) (priorita bassa media alta))
  (intervento (name 3017) (carbatt potenza1) (priorita bassa media alta))
  (intervento (name 3018) (carbatt controllo1) (priorita bassa media alta))
  (intervento (name 3019) (carbatt potenza2) (priorita bassa media alta))
  (intervento (name 3020) (carbatt controllo2) (priorita bassa media alta))
  (intervento (name 3021) (comm vercabl11) (priorita bassa media alta))
  (intervento (name 3022) (comm vercabl12) (priorita bassa media alta))
  (intervento (name 3023) (comm vercabl13) (priorita bassa media alta))
  (intervento (name 3024) (comm vercabl14) (priorita bassa media alta))
  (intervento (name 3025) (comm ignor11) (priorita bassa media alta))
  (intervento (name 3026) (comm ignor12) (priorita bassa media alta))
  (intervento (name 3027) (comm ignor13) (priorita bassa media alta))
  (intervento (name 3028) (comm ignor14) (priorita bassa media alta))
  )

(defrule INTERVENTI::genera-interventi
  (intervento (name ?name)
        (carbatt $? ?c $?)
        (priorita $? ?b $?)
        (pantografi $? ?s $?)
        (olio-assi $? ?r $?)
        (comm $? ?q $?))
  (attribute (name carbatt) (value ?c) (certainty ?certainty-1))
  (attribute (name priorita) (value ?b) (certainty ?certainty-2))
  (attribute (name pantografi) (value ?s) (certainty ?certainty-3))
  (attribute (name olio-assi) (value ?r) (certainty ?certainty-4))
  (attribute (name comm) (value ?q) (certainty ?certainty-5))
  =>
  (assert (attribute (name intervento) (value ?name)
                     (certainty (min ?certainty-1 ?certainty-2 ?certainty-3 ?certainty-4 ?certainty-5)))))

;;*******************************
;;* PRINT DEI RISULTATI SU FILE *
;;*******************************

(defmodule PRINT-RESULTS (import MAIN ?ALL))

(defrule PRINT-RESULTS::header ""
   (declare (salience 10))
   =>
   (open "xpert.res" risult "w")
;;   (printout risult t)
;;   (printout risult "   * INTERVENTI CONSIGLIATI *" t t)
;;   (printout risult " INTERVENTO                           CONFIDENZA" t)
;;   (printout risult " -----------------------------------------------" t)
   (assert (phase print-interventi)))

(defrule PRINT-RESULTS::print-intervento ""
  ?rem <- (attribute (name intervento) (value ?name) (certainty ?per))		  
  (not (attribute (name intervento) (certainty ?per1&:(> ?per1 ?per))))
  =>
  (retract ?rem)
  (format risult "%-4d %2d%n" ?name ?per))

(defrule PRINT-RESULTS::rimuovi-interventi-bassi ""
  ?rem <- (attribute (name intervento) (certainty ?per&:(< ?per 20)))
  =>
  (retract ?rem))

(defrule PRINT-RESULTS::end-spaces ""
   (not (attribute (name intervento)))
   =>
   (printout risult t))


;;*******************************
;;* PRINT DEI RISULTATI A VIDEO *
;;*******************************

;;(defmodule PRINT-RESULTS (import MAIN ?ALL))

;;(defrule PRINT-RESULTS::header ""
;;   (declare (salience 10))
;;   =>
;;   (printout t t)
;;   (printout t "   * INTERVENTI CONSIGLIATI *" t t)
;;   (printout t " INTERVENTO                           CONFIDENZA" t)
;;   (printout t " -----------------------------------------------" t)
;;   (assert (phase print-interventi)))

;;(defrule PRINT-RESULTS::print-intervento ""
;;  ?rem <- (attribute (name intervento) (value ?name) (certainty ?per))		  
;;  (not (attribute (name intervento) (certainty ?per1&:(> ?per1 ?per))))
;;  =>
;;  (retract ?rem)
;;  (format t " %-24s %2d%%%n" ?name ?per))

;;(defrule PRINT-RESULTS::rimuovi-interventi-bassi ""
;;  ?rem <- (attribute (name intervento) (certainty ?per&:(< ?per 20)))
;;  =>
;;  (retract ?rem))

;;(defrule PRINT-RESULTS::end-spaces ""
;;   (not (attribute (name intervento)))
;;   =>
;;   (printout t t))



