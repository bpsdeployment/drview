#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*legge il file 'Test_TU-CCU2.rep' e restituisce il batch per CLIPS*/
main()
{
 FILE *fp, *fpp;   /* file pointer */
 /* variabili di appoggio */
 int i,j,k,iniz,fin,bas,durata,speed;
 /* contatori eventi */
 int C011=0,C012=0,C013=0,C014=0,C076=0,C116=0,C117=0,C123=0,C124=0,C142=0;
 int C143=0,C144=0,C145=0,C169=0,C170=0,C178=0,C206=0,C207=0,C224=0,C225=0;
 int C421=0,C428=0,C429=0,C431=0,C432=0,C433=0,C445=0,C518=0,C519=0,C520=0;
 int C521=0,C559=0,C560=0;
 /* contatori stato */
 int CSPD011=0,CSPD012=0,CSPD013=0,CSPD014=0,CSPD144=0,CSPD145=0;
 int CDUR144=0,CDUR145=0,CDUR518=0,CDUR519=0,CDUR520=0,CDUR521=0;
 int CCB11=0,CCB12=0,CCB21=0,CCB22=0;
 int thre1=2,thre2=5,thre3=10;
 /* variabili di lettura */
 char DIAGCB11,DIAGCB12,DIAGCB21,DIAGCB22,P25KV,P3KV,AUX1,AUX2;
 char CCU[5],riga[86],DC[3],twec[10],twel[10],SPD[5],BATTV[4],BATTI[5];
 char TOPM1[4],TOPM2[4];
 /* variabili di scrittura */
 char TEMPO,DC011,DC012,DC013,DC014,DC076,DC116,DC117,DC123,DC124,DC142,DC143;
 char DC144,DC145,DC169,DC170,DC178,DC206,DC207,DC224,DC225,DC421,DC428,DC429;
 char DC431,DC432,DC433,DC445,DC518,DC519,DC520,DC521,DC559,DC560,DC011V0;
 char DC012V0,DC013V0,DC014V0,KLIP2041,KLIP2042,DC144SEC,DC145SEC;
 char DC144SPD,DC145SPD,DC518SEC,DC519SEC,DC520SEC,DC521SEC;
 /* inizializzazione variabili */
i=0;
TEMPO = 'a';
DC011 = 'a';
DC012 = 'a';
DC013 = 'a';
DC014 = 'a';
DC076 = 'a';
DC116 = 'a';
DC117 = 'a';
DC123 = 'a';
DC124 = 'a';
DC142 = 'a';
DC143 = 'a';
DC144 = 'a';
DC145 = 'a';
DC169 = 'a';
DC170 = 'a';
DC178 = 'a';
DC206 = 'a';
DC207 = 'a';
DC224 = 'a';
DC225 = 'a';
DC421 = 'a';
DC428 = 'a';
DC429 = 'a';
DC431 = 'a';
DC432 = 'a';
DC433 = 'a';
DC445 = 'a';
DC518 = 'a';
DC519 = 'a';
DC520 = 'a';
DC521 = 'a';
DC559 = 'a';
DC560 = 'a';
DC011V0 = 'a';
DC012V0 = 'a';
DC013V0 = 'a';
DC014V0 = 'a';
KLIP2041 = 'a';
KLIP2042 = 'a';
DC144SEC = 'a';
DC145SEC = 'a';
DC144SPD = 'a';
DC145SPD = 'a';
DC518SEC = 'a';
DC519SEC = 'a';
DC520SEC = 'a';
DC521SEC = 'a';
/* apertura del file di input */
fpp = fopen("test.rep", "r");
/* controllo del file di input */
if ((fpp = fopen("test.rep", "r"))==NULL){
  printf("Errore in lettura di test.rep \n");
  exit(1);
 }
/* apertura del file di output */
fp = fopen("run.bat", "w");
/* controllo del file in scrittura */
  if ((fp = fopen("run.bat", "w"))==NULL){
  printf("Errore in scrittura \n");
  exit(1);
 }
/* lettura della prima riga CCU number */
fgets(CCU,5,fpp);
/* INIZIO DEL CICLO DI LETTURA */
do{
/* fa scorrere la posizione di lettura all'inizio della riga */
fseek(fpp,2,SEEK_CUR);
/* legge l'intera riga */
fgets(riga,86,fpp);
if(riga[0]=='\n'){fseek(fpp,0,SEEK_CUR);
                  fgets(riga,86,fpp);
                  }
/* estrae la sottostringa relativa al DC */
for(i=0;i<3;++i)DC[i]=riga[i];
/* estrae la sottostringa relativa al twec */
for(i=0;i<10;++i)twec[i]=riga[i+5];
/* estrae la sottostringa relativa al twel */
for(i=0;i<10;++i)twel[i]=riga[i+17];
/* estrae lo stato di DIAGCB11  */
DIAGCB11=riga[29];
/* estrae lo stato di DIAGCB12  */
DIAGCB12=riga[32];
/* estrae lo stato di DIAGCB21  */
DIAGCB21=riga[35];
/* estrae lo stato di DIAGCB22  */
DIAGCB22=riga[38];
/* estrae lo stato di P25KV  */
P25KV=riga[41];
/* estrae lo stato di P3KV  */
P3KV=riga[44];
/* estrae la sottostringa relativa alla velocit�*/ 
for(i=0;i<5;++i)SPD[i]=riga[i+47];
/* estrae la sottostringa relativa alla tensione batteria */ 
for(i=0;i<4;++i)BATTV[i]=riga[i+54];
/* estrae la sottostringa relativa alla corrente batteria */ 
for(i=0;i<5;++i)BATTI[i]=riga[i+60];
/* estrae la sottostringa relativa al topmeter signal 1 */ 
for(i=0;i<4;++i)TOPM1[i]=riga[i+67];
/* estrae la sottostringa relativa al topmeter signal 2 */ 
for(i=0;i<4;++i)TOPM2[i]=riga[i+73];
/* estrae lo stato di AUX1  */
AUX1=riga[79];
/* estrae lo stato di AUX2  */
AUX2=riga[82];
/* converte in numero twec */
j=9;
iniz=0;
k=0;
for(i=0;i<10;++i){bas=twec[i]-48;
                  for(k=1;k<j+1;++k){bas=bas*10;}
                  iniz=iniz+bas;
                  --j;
}
/* converte in numero twel */
j=9;
fin=0;
k=0;
for(i=0;i<10;++i){bas=twel[i]-48;
                  for(k=1;k<j+1;++k){bas=bas*10;}
                  fin=fin+bas;
                  --j;
}
/* calcola la durata dell'evento */
durata=fin-iniz;
/* converte in numero SPD */
j=2;
speed=0;
k=0;
for(i=0;i<3;++i){bas=SPD[i]-48;
                  for(k=1;k<j+1;++k){bas=bas*10;}
                  speed=speed+bas;
                  --j;
}
if(!strcmp(DC,"011")){++C011;
                      if(!strcmp(SPD,"000.0"))++CSPD011;
                      }
if(!strcmp(DC,"012")){++C012;
                      if(!strcmp(SPD,"000.0"))++CSPD012;
                      }
if(!strcmp(DC,"013")){++C013;
                      if(!strcmp(SPD,"000.0"))++CSPD013;
                      }
if(!strcmp(DC,"014")){++C014;
                      if(!strcmp(SPD,"000.0"))++CSPD014;
                      }
if(!strcmp(DC,"076"))++C076;
if(!strcmp(DC,"116"))++C116;
if(!strcmp(DC,"117"))++C117;
if(!strcmp(DC,"123")){++C123;
                      if(DIAGCB11=='1')++CCB11;
                      if(DIAGCB12=='1')++CCB12;
                      }
if(!strcmp(DC,"124")){++C124;
                      if(DIAGCB21=='1')++CCB21;
                      if(DIAGCB22=='1')++CCB22;
                      }
if(!strcmp(DC,"142"))++C142;
if(!strcmp(DC,"143"))++C143;
if(!strcmp(DC,"144")){++C144;
                     if(durata<5)++CDUR144;
                     if(speed>120)++CSPD144;
                     }
if(!strcmp(DC,"145")){++C145;
                     if(durata<5)++CDUR145;
                     if(speed>120)++CSPD145;
                     }
if(!strcmp(DC,"169"))++C169;
if(!strcmp(DC,"170"))++C170;
if(!strcmp(DC,"178"))++C178;
if(!strcmp(DC,"206"))++C206;
if(!strcmp(DC,"207"))++C207;
if(!strcmp(DC,"224"))++C224;
if(!strcmp(DC,"225"))++C225;
if(!strcmp(DC,"421"))++C421;
if(!strcmp(DC,"428"))++C428;
if(!strcmp(DC,"429"))++C429;
if(!strcmp(DC,"431"))++C431;
if(!strcmp(DC,"432"))++C432;
if(!strcmp(DC,"433"))++C433;
if(!strcmp(DC,"445"))++C445;
if(!strcmp(DC,"518")){++C518;
                     if(durata<2)++CDUR518;
                     }
if(!strcmp(DC,"519")){++C519;
                     if(durata<2)++CDUR519;
                     }
if(!strcmp(DC,"520")){++C520;
                     if(durata<2)++CDUR520;
                     }
if(!strcmp(DC,"521")){++C521;
                     if(durata<2)++CDUR521;
                     }
if(!strcmp(DC,"559"))++C559;
if(!strcmp(DC,"560"))++C560;                     

}while(riga[0]=='0'||riga[0]=='1'||riga[0]=='2'||riga[0]=='3'||riga[0]=='4'||riga[0]=='5');
/* FINE DEL CICLO DI LETTURA */

/* chiusura del file di input */
fclose(fpp);

/* inizio aggiornamento variabili */
if(C011<thre1) 
DC011='a';
else if(C011>(thre1-1)&&C011<thre2)
DC011='b';
else if(C011>(thre2-1)&&C011<thre3) 
DC011='c';
else if(C011>(thre3-1)) 
DC011='d';

if(C012<thre1) 
DC012='a';
else if(C012>(thre1-1)&&C012<thre2)
DC012='b';
else if(C012>(thre2-1)&&C012<thre3) 
DC012='c';
else if(C012>(thre3-1)) 
DC012='d';

if(C013<thre1) 
DC013='a';
else if(C013>(thre1-1)&&C013<thre2)
DC013='b';
else if(C013>(thre2-1)&&C013<thre3) 
DC013='c';
else if(C013>(thre3-1)) 
DC013='d';

if(C014<thre1) 
DC014='a';
else if(C014>(thre1-1)&&C014<thre2)
DC014='b';
else if(C014>(thre2-1)&&C014<thre3) 
DC014='c';
else if(C014>(thre3-1)) 
DC014='d';

if(C076<thre1) 
DC076='a';
else if(C076>(thre1-1)&&C076<thre2)
DC076='b';
else if(C076>(thre2-1)&&C076<thre3) 
DC076='c';
else if(C076>(thre3-1)) 
DC076='d';

if(C116<thre1) 
DC116='a';
else if(C116>(thre1-1)&&C116<thre2)
DC116='b';
else if(C116>(thre2-1)&&C116<thre3) 
DC116='c';
else if(C116>(thre3-1)) 
DC116='d';

if(C117<thre1) 
DC117='a';
else if(C117>(thre1-1)&&C117<thre2)
DC117='b';
else if(C117>(thre2-1)&&C117<thre3) 
DC117='c';
else if(C117>(thre3-1)) 
DC117='d';

if(C123<thre1) 
DC123='a';
else if(C123>(thre1-1)&&C123<thre2)
DC123='b';
else if(C123>(thre2-1)&&C123<thre3) 
DC123='c';
else if(C123>(thre3-1)) 
DC123='d';

if(C124<thre1) 
DC124='a';
else if(C124>(thre1-1)&&C124<thre2)
DC124='b';
else if(C124>(thre2-1)&&C124<thre3) 
DC124='c';
else if(C124>(thre3-1)) 
DC124='d';

if(C142<thre1) 
DC142='a';
else if(C142>(thre1-1)&&C142<thre2)
DC142='b';
else if(C142>(thre2-1)&&C142<thre3) 
DC142='c';
else if(C142>(thre3-1)) 
DC142='d';

if(C143<thre1) 
DC143='a';
else if(C143>(thre1-1)&&C143<thre2)
DC143='b';
else if(C143>(thre2-1)&&C143<thre3) 
DC143='c';
else if(C143>(thre3-1)) 
DC143='d';

if(C144<thre1) 
DC144='a';
else if(C144>(thre1-1)&&C144<thre2)
DC144='b';
else if(C144>(thre2-1)&&C144<thre3) 
DC144='c';
else if(C144>(thre3-1)) 
DC144='d';

if(C145<thre1) 
DC145='a';
else if(C145>(thre1-1)&&C145<thre2)
DC145='b';
else if(C145>(thre2-1)&&C145<thre3) 
DC145='c';
else if(C145>(thre3-1)) 
DC145='d';


if(C169<thre1) 
DC169='a';
else if(C169>(thre1-1)&&C169<thre2)
DC169='b';
else if(C169>(thre2-1)&&C169<thre3) 
DC169='c';
else if(C169>(thre3-1)) 
DC169='d';

if(C170<thre1) 
DC170='a';
else if(C170>(thre1-1)&&C170<thre2)
DC170='b';
else if(C170>(thre2-1)&&C170<thre3) 
DC170='c';
else if(C170>(thre3-1)) 
DC170='d';

if(C178<thre1) 
DC178='a';
else if(C178>(thre1-1)&&C178<thre2)
DC178='b';
else if(C178>(thre2-1)&&C178<thre3) 
DC178='c';
else if(C178>(thre3-1)) 
DC178='d';

if(C206<thre1) 
DC206='a';
else if(C206>(thre1-1)&&C206<thre2)
DC206='b';
else if(C206>(thre2-1)&&C206<thre3) 
DC206='c';
else if(C206>(thre3-1)) 
DC206='d';

if(C207<thre1) 
DC207='a';
else if(C207>(thre1-1)&&C207<thre2)
DC207='b';
else if(C207>(thre2-1)&&C207<thre3) 
DC207='c';
else if(C207>(thre3-1)) 
DC207='d';

if(C224<thre1) 
DC224='a';
else if(C224>(thre1-1)&&C224<thre2)
DC224='b';
else if(C224>(thre2-1)&&C224<thre3) 
DC224='c';
else if(C224>(thre3-1)) 
DC224='d';

if(C225<thre1) 
DC225='a';
else if(C225>(thre1-1)&&C225<thre2)
DC225='b';
else if(C225>(thre2-1)&&C225<thre3) 
DC225='c';
else if(C225>(thre3-1)) 
DC225='d';

if(C421<thre1) 
DC421='a';
else if(C421>(thre1-1)&&C421<thre2)
DC421='b';
else if(C421>(thre2-1)&&C421<thre3) 
DC421='c';
else if(C421>(thre3-1)) 
DC421='d';

if(C428<thre1) 
DC428='a';
else if(C428>(thre1-1)&&C428<thre2)
DC428='b';
else if(C428>(thre2-1)&&C428<thre3) 
DC428='c';
else if(C428>(thre3-1)) 
DC428='d';

if(C429<thre1) 
DC429='a';
else if(C429>(thre1-1)&&C429<thre2)
DC429='b';
else if(C429>(thre2-1)&&C429<thre3) 
DC429='c';
else if(C429>(thre3-1)) 
DC429='d';

if(C431<thre1) 
DC431='a';
else if(C431>(thre1-1)&&C431<thre2)
DC431='b';
else if(C431>(thre2-1)&&C431<thre3) 
DC431='c';
else if(C431>(thre3-1)) 
DC431='d';

if(C432<thre1) 
DC432='a';
else if(C432>(thre1-1)&&C432<thre2)
DC432='b';
else if(C432>(thre2-1)&&C432<thre3) 
DC432='c';
else if(C432>(thre3-1)) 
DC432='d';

if(C433<thre1) 
DC433='a';
else if(C433>(thre1-1)&&C433<thre2)
DC433='b';
else if(C433>(thre2-1)&&C433<thre3) 
DC433='c';
else if(C433>(thre3-1)) 
DC433='d';

if(C445<thre1) 
DC445='a';
else if(C445>(thre1-1)&&C445<thre2)
DC445='b';
else if(C445>(thre2-1)&&C445<thre3) 
DC445='c';
else if(C445>(thre3-1)) 
DC445='d';

if(C518<thre1) 
DC518='a';
else if(C518>(thre1-1)&&C518<thre2)
DC518='b';
else if(C518>(thre2-1)&&C518<thre3) 
DC518='c';
else if(C518>(thre3-1)) 
DC518='d';

if(C519<thre1) 
DC519='a';
else if(C519>(thre1-1)&&C519<thre2)
DC519='b';
else if(C519>(thre2-1)&&C519<thre3) 
DC519='c';
else if(C519>(thre3-1)) 
DC519='d';

if(C520<thre1) 
DC520='a';
else if(C520>(thre1-1)&&C520<thre2)
DC520='b';
else if(C520>(thre2-1)&&C520<thre3) 
DC520='c';
else if(C520>(thre3-1)) 
DC520='d';

if(C521<thre1) 
DC521='a';
else if(C521>(thre1-1)&&C521<thre2)
DC521='b';
else if(C521>(thre2-1)&&C521<thre3) 
DC521='c';
else if(C521>(thre3-1)) 
DC521='d';

if(C559<thre1) 
DC559='a';
else if(C559>(thre1-1)&&C559<thre2)
DC559='b';
else if(C559>(thre2-1)&&C559<thre3) 
DC559='c';
else if(C559>(thre3-1)) 
DC559='d';

if(C560<thre1) 
DC560='a';
else if(C560>(thre1-1)&&C560<thre2)
DC560='b';
else if(C560>(thre2-1)&&C560<thre3) 
DC560='c';
else if(C560>(thre3-1)) 
DC560='d';

if(CSPD011==C011) 
DC011V0='a';
else if(CSPD011<C011&&CSPD011>C011/2)
DC011V0='b';
else if(CSPD011<C011/2&&CSPD011>C011/4) 
DC011V0='c';
else if(CSPD011==0) 
DC011V0='d';

if(CSPD012==C012) 
DC012V0='a';
else if(CSPD012<C012&&CSPD012>C012/2)
DC012V0='b';
else if(CSPD012<C012/2&&CSPD012>C012/4) 
DC012V0='c';
else if(CSPD012==0) 
DC012V0='d';

if(CSPD013==C013) 
DC013V0='a';
else if(CSPD013<C013&&CSPD013>C013/2)
DC013V0='b';
else if(CSPD013<C013/2&&CSPD013>C013/4) 
DC013V0='c';
else if(CSPD013==0) 
DC013V0='d';

if(CSPD014==C014) 
DC014V0='a';
else if(CSPD014<C014&&CSPD014>C014/2)
DC014V0='b';
else if(CSPD014<C014/2&&CSPD014>C014/4) 
DC014V0='c';
else if(CSPD014==0) 
DC014V0='d';

if(CCB11>CCB12) 
KLIP2041='a';
else if(CCB11<CCB12)
KLIP2041='b';
else if(CCB11==CCB12) 
KLIP2041='c';

if(CCB21>CCB22) 
KLIP2042='a';
else if(CCB21<CCB22)
KLIP2042='b';
else if(CCB21==CCB22) 
KLIP2042='c';

if(CDUR144==C144) 
DC144SEC='a';
else if(CDUR144<C144&&CDUR144>C144/2)
DC144SEC='b';
else if(CDUR144<C144/2&&CDUR144>C144/4) 
DC144SEC='c';
else if(CDUR144==0) 
DC144SEC='d';

if(CDUR145==C145) 
DC145SEC='a';
else if(CDUR145<C145&&CDUR145>C145/2)
DC145SEC='b';
else if(CDUR145<C145/2&&CDUR145>C145/4) 
DC145SEC='c';
else if(CDUR145==0) 
DC145SEC='d';

if(CSPD144==C144) 
DC144SPD='a';
else if(CSPD144<C144&&CSPD144>C144/2)
DC144SPD='b';
else if(CSPD144<C144/2&&CSPD144>C144/4) 
DC144SPD='c';
else if(CSPD144==0) 
DC144SPD='d';

if(CSPD145==C145) 
DC145SPD='a';
else if(CSPD145<C145&&CSPD145>C145/2)
DC145SPD='b';
else if(CSPD145<C145/2&&CSPD145>C145/4) 
DC145SPD='c';
else if(CSPD145==0) 
DC145SPD='d';

if(CDUR518==C518) 
DC518SEC='a';
else if(CDUR518<C518&&CDUR518>C518/2)
DC518SEC='b';
else if(CDUR518<C518/2&&CDUR518>C518/4) 
DC518SEC='c';
else if(CDUR518==0) 
DC518SEC='d';

if(CDUR519==C519) 
DC519SEC='a';
else if(CDUR519<C519&&CDUR519>C519/2)
DC519SEC='b';
else if(CDUR519<C519/2&&CDUR519>C519/4) 
DC519SEC='c';
else if(CDUR519==0) 
DC519SEC='d';

if(CDUR520==C520) 
DC520SEC='a';
else if(CDUR520<C520&&CDUR520>C520/2)
DC520SEC='b';
else if(CDUR520<C520/2&&CDUR520>C520/4) 
DC520SEC='c';
else if(CDUR520==0) 
DC520SEC='d';

if(CDUR521==C521) 
DC521SEC='a';
else if(CDUR521<C521&&CDUR521>C521/2)
DC521SEC='b';
else if(CDUR521<C521/2&&CDUR521>C521/4) 
DC521SEC='c';
else if(CDUR521==0) 
DC521SEC='d';

/* fine aggiornamento variabili /*
/* inizio scrittura file di output */ 
fputs(CCU,fp);
fprintf(fp,"\n");
fprintf(fp,"(load Telediag.clp)\n");
fprintf(fp,"(reset)\n");
fprintf(fp,"(run)\n");
fputc(TEMPO, fp);
fprintf(fp,"\n");
fputc(DC011, fp);
fprintf(fp,"\n");
fputc(DC012, fp);
fprintf(fp,"\n");
fputc(DC013, fp);
fprintf(fp,"\n");
fputc(DC014, fp);
fprintf(fp,"\n");
fputc(DC076, fp);
fprintf(fp,"\n");
fputc(DC116, fp);
fprintf(fp,"\n");
fputc(DC117, fp);
fprintf(fp,"\n");
fputc(DC123, fp);
fprintf(fp,"\n");
fputc(DC124, fp);
fprintf(fp,"\n");
fputc(DC142, fp);
fprintf(fp,"\n");
fputc(DC143, fp);
fprintf(fp,"\n");
fputc(DC144, fp);
fprintf(fp,"\n");
fputc(DC145, fp);
fprintf(fp,"\n");
fputc(DC169, fp);
fprintf(fp,"\n");
fputc(DC170, fp);
fprintf(fp,"\n");
fputc(DC178, fp);
fprintf(fp,"\n");
fputc(DC206, fp);
fprintf(fp,"\n");
fputc(DC207, fp);
fprintf(fp,"\n");
fputc(DC224, fp);
fprintf(fp,"\n");
fputc(DC225, fp);
fprintf(fp,"\n");
fputc(DC421, fp);
fprintf(fp,"\n");
fputc(DC428, fp);
fprintf(fp,"\n");
fputc(DC429, fp);
fprintf(fp,"\n");
fputc(DC431, fp);
fprintf(fp,"\n");
fputc(DC432, fp);
fprintf(fp,"\n");
fputc(DC433, fp);
fprintf(fp,"\n");
fputc(DC445, fp);
fprintf(fp,"\n");
fputc(DC518, fp);
fprintf(fp,"\n");
fputc(DC519, fp);
fprintf(fp,"\n");
fputc(DC520, fp);
fprintf(fp,"\n");
fputc(DC521, fp);
fprintf(fp,"\n");
fputc(DC559, fp);
fprintf(fp,"\n");
fputc(DC560, fp);
fprintf(fp,"\n");
fputc(DC011V0, fp);
fprintf(fp,"\n");
fputc(DC012V0, fp);
fprintf(fp,"\n");
fputc(DC013V0, fp);
fprintf(fp,"\n");
fputc(DC014V0, fp);
fprintf(fp,"\n");
fputc(KLIP2041, fp);
fprintf(fp,"\n");
fputc(KLIP2042, fp);
fprintf(fp,"\n");
fputc(DC144SEC, fp);
fprintf(fp,"\n");
fputc(DC145SEC, fp);
fprintf(fp,"\n");
fputc(DC144SPD, fp);
fprintf(fp,"\n");
fputc(DC145SPD, fp);
fprintf(fp,"\n");
fputc(DC518SEC, fp);
fprintf(fp,"\n");
fputc(DC519SEC, fp);
fprintf(fp,"\n");
fputc(DC520SEC, fp);
fprintf(fp,"\n");
fputc(DC521SEC, fp);
fprintf(fp,"\n");
fprintf(fp,"(exit)\n");
fprintf(fp,"\n");
/* fine scrittura file di output */
/* chiusura del file di output */
 fclose(fp);
/* esecuzione da shell */
system("clips -f run.bat");
}

