using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using TDManagementForms;

namespace DDExpertExchange
{
  /// <summary>
  /// Class represents windows service which periodically runs expert exchange application
  /// </summary>
  partial class ExpExchangeService : ServiceBase
  {
    #region Private members
    private DDExchangeHelper exchangeHelper;
    private System.Threading.Timer baseTimer; //Timer for basic periods
    private bool bWorkerRunning; //Worker thread is already running
    private DateTime nextExchangeTime; //Time of next expert exchange
    #endregion //Private members

    #region Construction initialization
    /// <summary>
    /// Service constructor
    /// </summary>
    public ExpExchangeService()
    {
      InitializeComponent();
    }
    #endregion //Construction initialization

    #region Event handlers
    protected override void OnStart(string[] args)
    {
      //Create logger objec
      logger.FileName = Properties.Settings.Default.LogFileName;
      logger.MaxFileLength = Properties.Settings.Default.MaxLogSize;
      logger.MaxHistoryFiles = Properties.Settings.Default.MaxLogHistoryFiles;
      logger.LogToFile = true;
      logger.FileVerbosity = 10;
      //Create and initialize exchange helper
      exchangeHelper = new DDExchangeHelper();
      exchangeHelper.Initialize(logger);
      //Create and initialize base timer
      //First create delegate for callback method
      TimerCallback timerDelegate = new TimerCallback(baseTimer_Elapsed);
      //Create and start timer
      baseTimer = new System.Threading.Timer(
        timerDelegate,
        null,
        (int)Properties.Settings.Default.BaseTimerPeriod.TotalMilliseconds,
        (int)Properties.Settings.Default.BaseTimerPeriod.TotalMilliseconds);
      logger.LogText(0, "Service", "Created and started base timer with period {0} ms",
        Properties.Settings.Default.BaseTimerPeriod.TotalMilliseconds);
      //Compute time of next exchange
      nextExchangeTime = exchangeHelper.GetFirstExchangePeriodStart();
      logger.LogText(0, "Service", "Time of first record exchange (UTC) {0:yyyy-MM-dd HH:mm}",
        nextExchangeTime);
    }

    protected override void OnStop()
    {
      logger.LogText(0, "Service", "TD Expert Exchange Service stopped");
    }

    /// <summary>
    /// Raised after timer is elapsed
    /// </summary>
    /// <param name="state"></param>
    void baseTimer_Elapsed(object state)
    {
      //Check if worker thread is not already running
      if (bWorkerRunning)
      {
        logger.LogText(0, "Service", "Expert exchange service is overloading");
        return;
      }
      //Set worker thread is running
      bWorkerRunning = true;
      try
      {
        if (DateTime.UtcNow > nextExchangeTime)
        {//Time of next exchange
          //Exchange records using helper
          DateTime timeFrom = nextExchangeTime - Properties.Settings.Default.ExchangeDelay;
          DateTime timeTo = timeFrom + Properties.Settings.Default.ExchangePeriod;
          exchangeHelper.ExchangeRecords(timeFrom, timeTo);
          //Compute next exchange period
          nextExchangeTime += Properties.Settings.Default.ExchangePeriod;
          logger.LogText(2, "Service", "Time of next record exchange (UTC) {0:yyyy-MM-dd HH:mm}",
            nextExchangeTime);
        }
      }
      catch (Exception ex)
      {
        logger.LogText(0, "Service", "Fatal error occured during expert record exchange");
        logger.LogException(1, "Service", ex);
      }
      finally
      {
        //Worker thread is not running
        bWorkerRunning = false;
      }
    }
    #endregion //Event handlers
  }
}
