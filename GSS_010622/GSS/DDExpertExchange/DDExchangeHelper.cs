using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Diagnostics;
using System.IO;
using DDDObjects;
using TDManagementForms;

namespace DDExpertExchange
{
  /// <summary>
  /// Windows service class for diagnostic data exchange
  /// Class contains all sources of diagnostic records for exchange.
  /// It is capable of exporting the records into text file, running external expert application 
  /// and exporting the result in DRImport format.
  /// </summary>
  class DDExchangeHelper
  {
    #region Private members
    private DDDHelper dddHelper;
    private Logger logger;
    private DiagnosticRecordsTableAdapters.SelRecordsTableAdapter selRecAdapter;
    private DiagnosticRecordsTableAdapters.ActiveRecordsTableAdapter actRecAdapter;
    private Process expertAppl;
    private XmlWriterSettings xmlSettings;
    private String FaultCodes;
    private String ExpertFaultCodes;
    private List<RecordSource> RecordSources;
    private Dictionary<String, RecordExport> exportConfigs;
    private DiagnosticRecords records; //Dataset with records
    #endregion //Private members

    #region Public properties
    #endregion //Public properties

    #region Construction initialization
    /// <summary>
    /// Default public constructor
    /// </summary>
    public DDExchangeHelper()
    {
    }

    /// <summary>
    /// Provides all necessary initialization
    /// </summary>
    public void Initialize(Logger logger)
    {
      try
      {
        //Store logger object
        this.logger = logger;
        //Create DDD Helper object
        dddHelper = new DDDHelper();
        dddHelper.ConnectionString = Properties.Settings.Default.CDDBConnectionString;
        //Create table adapters
        selRecAdapter = new DiagnosticRecordsTableAdapters.SelRecordsTableAdapter();
        actRecAdapter = new DiagnosticRecordsTableAdapters.ActiveRecordsTableAdapter();
        //Create dataset
        records = new DiagnosticRecords();
        //Log application start
        logger.LogText("");
        logger.LogText("-------------------------------------------");
        logger.LogText(1, "DDExchange", "DDExchange service started");
        //Load record sources from configuration file
        logger.LogText(1, "DDExchange", "Loading configuration from file {0}", Properties.Settings.Default.ExportCfgFileName);
        //Load config file into XML Document
        XmlDocument configDoc = new XmlDocument();
        configDoc.Load(Properties.Settings.Default.ExportCfgFileName);
        //Load list of fault codes
        FaultCodes = configDoc.DocumentElement.GetAttribute("FaultCodes");
        ExpertFaultCodes = configDoc.DocumentElement.GetAttribute("ExpertFaultCodes");
        logger.LogText(2, "DDExchange", "Fault codes {0}", FaultCodes);
        logger.LogText(2, "DDExchange", "Expert Fault codes {0}", ExpertFaultCodes);
        //Load record sources
        RecordSources = new List<RecordSource>();
        foreach (XmlElement srcElem in configDoc.DocumentElement.SelectSingleNode("Sources").ChildNodes)
        {
          RecordSource recSource = new RecordSource(srcElem);
          logger.LogText(2, "DDExchange", "Loaded record source TU {0}, Source {1}", recSource.TUName, recSource.Source);
          RecordSources.Add(recSource);
        }
        //Select all RecExport elements
        XmlNodeList expCfgNodes = configDoc.SelectNodes("//RecExport");
        //Create new list of record sources
        exportConfigs = new Dictionary<string, RecordExport>();
        //Create record source object for each source
        foreach (XmlElement expCfgElem in expCfgNodes)
        {
          RecordExport recExport = new RecordExport(logger, dddHelper, expCfgElem);
          exportConfigs.Add(recExport.UserVersion, recExport);
        }
        logger.LogText(1, "DDExchange", "Configuration loaded");
        //Initialize static number format info for variable columns
        VariableColumn.formatInfo.NumberDecimalSeparator = ".";
        //Initialize process class for expert application
        expertAppl = new Process();
        String expApplPath = Properties.Settings.Default.ExpertApplication;
        if (!Path.IsPathRooted(expApplPath))
          expApplPath = Path.Combine(Directory.GetCurrentDirectory(), expApplPath);
        expertAppl.StartInfo.FileName = expApplPath;
        expertAppl.StartInfo.WorkingDirectory = Path.GetDirectoryName(expApplPath);
        expertAppl.StartInfo.CreateNoWindow = true;
        expertAppl.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
        //Intialize XML writer settings
        xmlSettings = new XmlWriterSettings();
        xmlSettings.Indent = true;
        xmlSettings.Encoding = Encoding.UTF8;
        xmlSettings.OmitXmlDeclaration = true;
        xmlSettings.ConformanceLevel = ConformanceLevel.Fragment;
      }
      catch (Exception ex)
      {
        logger.LogText(1, "DDExchange", "Failed to initialize DDExchange service");
        logger.LogException(2, "DDExchange", ex);
        throw ex;
      }
    }
    #endregion //Construction initialization

    #region Public methods
    /// <summary>
    /// Perfor exchange of diagnostic records for each configured record source
    /// </summary>
    /// <param name="startTime">Start of exchange time range</param>
    /// <param name="endTime">End of exchang time range</param>
    public void ExchangeRecords(DateTime startTime, DateTime endTime)
    {
      logger.LogText(2, "DDExchange", "Performing diagnostic data exchange from {0:yyyy-MM-dd HH:mm} to {1:yyyy-MM-dd HH:mm}", startTime, endTime);
      //Create data table for expert records
      DiagnosticRecords.ExpertRecordsDataTable expertRecords = new DiagnosticRecords.ExpertRecordsDataTable();
      //Create subdirectory for file backups
      DirectoryInfo backupDirInfo = null;
      String dirName = startTime.ToString("yyyyMMddHHmm");
      try
      {
        backupDirInfo = Directory.CreateDirectory(Path.Combine(Properties.Settings.Default.BackupDirName, dirName));
      }
      catch (Exception ex)
      {
        logger.LogText(1, "DDExchange", "Failed to create backup directory {0}", dirName);
        logger.LogException(1, "DDExchange", ex);
      }
      //Iterate over all record sources
      foreach(RecordSource recSource in RecordSources)
      {
        try
        {
          logger.LogText(3, "DDExchange", "Exchanging records for TU {0} source {1}", recSource.TUName, recSource.Source);
          //Expord records
          TransformRecords(Properties.Settings.Default.RecExportFileName, 
            startTime, endTime, recSource);
          //Run expert application
          RunExpertApplication();
          //Create expert records
          CreateExpertRecords(Properties.Settings.Default.ExpertFileName, recSource,
            expertRecords, startTime, endTime);
        }
        catch (Exception)
        {
          logger.LogText(1, "DDExchange", "Diagnostic data exchange for source failed");
        }
        //Backup files
        BackupFiles(backupDirInfo, recSource);
      }
      //Store expert records into file
      SaveExpertRecordsToXml(expertRecords, startTime, backupDirInfo);
      //Delete old backup directories
      DeleteOldBackupDirs(backupDirInfo);
      logger.LogText(2, "DDExchange", "Diagnostic data exchange finished");
    }
    #endregion //Public methods

    #region Helper methods
    /// <summary>
    /// Selects from DB and transforms into output text file records for one record source (TU and source)
    /// and given time range.
    /// </summary>
    /// <param name="outputFile">Name of outptu file</param>
    /// <param name="timeFrom">Start of time range</param>
    /// <param name="timeTo">End of time range</param>
    /// <param name="recSource">Record source object</param>
    public void TransformRecords(String outputFile, DateTime timeFrom, DateTime timeTo, RecordSource recSource)
    {
      try
      {
        logger.LogText(4, "DDExchange", "Transforming records for TU {0}, Source {1}", recSource.TUName, recSource.Source);
        //Clear records table
        records.SelRecords.Clear();
        //Select records from DB
        selRecAdapter.Fill(records.SelRecords, timeFrom, timeTo, recSource.TUName, recSource.Source, FaultCodes);
        logger.LogText(5, "DDExchange", "Selected {0} records", records.SelRecords.Count);
        //Open output file
        using (StreamWriter writer = new StreamWriter(outputFile, false, Encoding.ASCII))
        {
          //Write source as first line
          writer.WriteLine(recSource.Source);
          int lastRecDefID = -1;
          //Try to transform each record and write to output file
          foreach (DiagnosticRecords.SelRecordsRow recRow in records.SelRecords)
          {
            try
            {
              //Write extra empty line for each new recordDefID
              if (recRow.RecordDefID != lastRecDefID && lastRecDefID != -1)
                writer.WriteLine();
              lastRecDefID = recRow.RecordDefID;
              //Obtain correct RecExport object for transformation
              RecordExport recordExport;
              DDDVersion dddVer = dddHelper.GetVersion(recRow.DDDVersionID);
              if (!exportConfigs.TryGetValue(dddVer.UserVersion, out recordExport))
                throw new Exception("Failed to obtain export configuration for DDD Version " + dddVer.UserVersion);
              //Transform record
              String line = recordExport.TransformRecord(recRow);
              //Write to output file
              writer.WriteLine(line);
            }
            catch (Exception ex)
            {
              logger.LogText(1, "DDExchange", "Failed to transform record with RecordInstID {0}:", recRow.RecordInstID);
              logger.LogException(2, "DDExchange", ex);
            }
          }
          //Extra empty line at the end of the file
          writer.WriteLine();
          //Try to obtain current DDDVersion and TUInstanceID for this source
          if (records.SelRecords.Count > 0)
          { //At least one record selected
            recSource.TUInstanceID = records.SelRecords[0].TUInstanceID;
            recSource.DDDVersion = dddHelper.GetVersion(records.SelRecords[0].DDDVersionID);
          }
        }
        logger.LogText(4, "DDExchange", "Transformed {0} records", records.SelRecords.Count);
      }
      catch (Exception ex)
      {
        logger.LogText(1, "DDExchange", "Failed to transform records:");
        logger.LogException(2, "DDExchange", ex);
        throw ex;
      }
    }

    /// <summary>
    /// Method loads active expert records from DB, new expert records from expert file
    /// and creates expert records as result of the combination of these two.
    /// Created records are added to outputTable
    /// </summary>
    /// <param name="expertFile">File generated by expert application</param>
    /// <param name="recSource">Source of diagnostic records</param>
    /// <param name="tableAdapter">Adapter used to select active records</param>
    /// <param name="outputTable">Table for new expert records</param>
    /// <param name="timeFrom"></param>
    /// <param name="timeTo"></param>
    public void CreateExpertRecords(String expertFile, RecordSource recSource,
      DiagnosticRecords.ExpertRecordsDataTable outputTable,
      DateTime timeFrom, DateTime timeTo)
    {
      try
      {
        int newExpertRecords = 0;
        int finishedExpertRecords = 0;
        int stillActiveRecords = 0;
        logger.LogText(4, "DDExchange", "Creating expert records for TU {0}, Source {1}", recSource.TUName, recSource.Source);
        //Load fault codes from expert file
        List<int> faultCodes = ReadFaultCodesFromFile(expertFile);
        logger.LogText(5, "DDExchange", "Expert application generated {0} fault codes", faultCodes.Count);
        //Load active records from DB
        records.ActiveRecords.Clear();
        actRecAdapter.Fill(records.ActiveRecords, recSource.TUName, recSource.Source, ExpertFaultCodes);
        logger.LogText(5, "DDExchange", "Loaded {0} active expert records from DB", records.ActiveRecords.Count);
        //Iterate over all generated expert codes
        foreach (int faultCode in faultCodes)
        {
          //First check, if this fault code is active in DB
          DiagnosticRecords.ActiveRecordsRow actRecord;
          if ((actRecord = records.ActiveRecords.FindByFaultCode(faultCode)) != null)
          { //Active record with this fault code found - no new record generated
            //Delete record from active records table and continue
            records.ActiveRecords.Rows.Remove(actRecord);
            stillActiveRecords++;
            continue;
          }
          //New record has to be added to output table
          if (recSource.DDDVersion == null || !recSource.TUInstanceID.HasValue)
            throw new Exception("No DDD Version or TUInstanceID specified in record source");
          //Obtain recorDefID
          int recordDefID = recSource.DDDVersion.GetRecordByFaultCode(faultCode).RecordDefID;
          //Obtain record instance ID
          Int64 recordInstID = CreateRecordInstID(timeFrom, faultCode);
          //Create binary data - dummy for now
          byte[] binData = BitConverter.GetBytes((double)0);
          //Add record to output table
          outputTable.Rows.Add(new Object[] 
            {recSource.TUInstanceID.Value, recordInstID, recSource.DDDVersion.VersionID, 
            recordDefID, timeFrom, null, timeFrom, binData});
          newExpertRecords++;
        }

        //Finish records which still remain in ActiveRecords table
        foreach (DiagnosticRecords.ActiveRecordsRow activeRec in records.ActiveRecords)
        { //Add each remaining active record into output table
          byte[] binData = null;
          if (!activeRec.IsBinDataNull())
            binData = activeRec.BinData;
          outputTable.AddExpertRecordsRow(
            activeRec.TUInstanceID, activeRec.RecordInstID, activeRec.DDDVersionID,
            activeRec.RecordDefID, activeRec.StartTime, timeFrom, timeFrom, binData);
          finishedExpertRecords++;
        }

        logger.LogText(4, "DDExchange", "Added {0} new expert records, finished {1} active expert records, {2} expert records remain active",
          newExpertRecords, finishedExpertRecords, stillActiveRecords);
      }
      catch (Exception ex)
      {
        logger.LogText(1, "DDExchange", "Failed to create expert records");
        logger.LogException(2, "DDExchange", ex);
        throw ex;
      }
    }

    /// <summary>
    /// Create record instance ID from start time and fault code.
    /// High 4 bytes - time, low 4 bytes - fault code
    /// </summary>
    private Int64 CreateRecordInstID(DateTime startTime, int faultCode)
    {
      Int64 recInstID = (startTime.Ticks >> 32);
      return ((recInstID << 32) | (Int64)faultCode);
    }

    /// <summary>
    /// Reads all fault codes from given text file.
    /// Assumes each fault code is written on one line.
    /// </summary>
    /// <param name="fileName">Name of the file</param>
    /// <returns>List of read fault codes</returns>
    private List<int> ReadFaultCodesFromFile(String fileName)
    {
      try
      {
        //Create list
        List<int> faultCodes = new List<int>();
        //Open text file
        using (StreamReader reader = new StreamReader(fileName))
        {
          String line = reader.ReadLine();
          line = line.Trim();
          if (line.Length > 0)
            faultCodes.Add(Int32.Parse(line));
        }
        //Return list
        return faultCodes;
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to read fault codes from expert file", ex);
      }
    }

    /// <summary>
    /// Starts predefined expert application.
    /// </summary>
    private void RunExpertApplication()
    {
      try
      {
        //Start the application
        expertAppl.Start();
        if (!expertAppl.WaitForExit(Properties.Settings.Default.ExpertApplicationTimeout))
        { //Did not finish in timeout - kill the application
          logger.LogText(1, "DDExchange", "Killing expert application after {0} ms timeout", Properties.Settings.Default.ExpertApplicationTimeout);
          expertAppl.Kill();
          throw new Exception(String.Format("Application did not finish in {0} ms timeout", Properties.Settings.Default.ExpertApplicationTimeout));
        }
        if (expertAppl.ExitCode != 0)
          throw new Exception(String.Format("Application finished with ExitCode {0}", expertAppl.ExitCode));
        logger.LogText(4, "DDExchange", "Expert application finished");

      }
      catch (Exception ex)
      {
        logger.LogText(1, "DDExchange", "Failed to run expert application");
        logger.LogException(1, "DDExchange", ex);
        throw ex;
      }
    }

    /// <summary>
    /// Backups current exchange files into given backup directory
    /// </summary>
    /// <param name="backupDirInfo">Directory into which files are backuped</param>
    /// <param name="recSource">Record source for which files are backuped</param>
    private void BackupFiles(DirectoryInfo backupDirInfo, RecordSource recSource)
    {
      try
      {
        if (backupDirInfo == null)
          throw new Exception("No backup directory specified");
        String fileName = recSource.TUName + "_" + recSource.Source;
        //Copy rec export file
        File.Copy(
          Properties.Settings.Default.RecExportFileName, 
          Path.Combine(backupDirInfo.FullName, fileName + ".rep"), true);
        //Copy expert appl. result file
        File.Copy(
          Properties.Settings.Default.ExpertFileName,
          Path.Combine(backupDirInfo.FullName, fileName + ".res"), true);
      }
      catch (Exception ex)
      {
        logger.LogText(1, "DDExchange", "Failed to backup exchange files");
        logger.LogException(1, "DDExchange", ex);
      }
    }

    /// <summary>
    /// Deletes old backup subdirectories from backup direcory
    /// </summary>
    /// <param name="backupSubDirInfo">Current backup subdirectory</param>
    private void DeleteOldBackupDirs(DirectoryInfo backupSubDirInfo)
    {
      try
      {
        //Delete old backup direcories
        String minDirName = DateTime.UtcNow.AddDays(-1 * Properties.Settings.Default.KeepBackupDays).ToString("yyyyMMddHHmm");
        int delCount = 0;
        foreach (DirectoryInfo dirInfo in backupSubDirInfo.Parent.GetDirectories())
          if (String.Compare(dirInfo.Name, minDirName) < 0)
          {
            dirInfo.Delete(true);
            delCount++;
          }
        logger.LogText(3, "DDExchange", "Deleted {0} old backup directories older than {1} days", delCount, Properties.Settings.Default.KeepBackupDays);
      }
      catch (Exception ex)
      {
        logger.LogText(1, "DDExchange", "Failed to delete old backup directories");
        logger.LogException(1, "DDExchange", ex);
      }
    }

    /// <summary>
    /// Stores given expert records into XML file suitable for DR Import
    /// </summary>
    /// <param name="expertRecords">Expert records to store</param>
    /// <param name="timeFrom"></param>
    /// <param name="backupDirInfo"></param>
    private void SaveExpertRecordsToXml(DiagnosticRecords.ExpertRecordsDataTable expertRecords, 
      DateTime timeFrom, DirectoryInfo backupDirInfo)
    {
      try
      {
        //Check if there are any records in given table
        if (expertRecords.Count < 1)
        {
          logger.LogText(3, "DDExchange", "No records to store into DRImport file");
          return;
        }
        //Create XML file in local working directory
        String fileName = "Exp" + timeFrom.ToString("yyyyMMddHHmm") + ".xml";
        String completeFileName = Path.Combine(Properties.Settings.Default.TmpDirectory, fileName);
        using(FileStream stream = new FileStream(completeFileName, FileMode.CreateNew, FileAccess.Write, FileShare.None))
          using(XmlWriter writer = XmlWriter.Create(stream, xmlSettings))
            foreach (DiagnosticRecords.ExpertRecordsRow expertRec in expertRecords)
            {//Iterate over all expert records and store them into the file
              writer.WriteStartElement("ImportedDR");
              writer.WriteElementString("TUInstanceID", expertRec.TUInstanceID.ToString());
              writer.WriteElementString("DDDVersionID", expertRec.DDDVersionID.ToString());
              writer.WriteElementString("RecordDefID", expertRec.RecordDefID.ToString());
              writer.WriteElementString("RecordInstID", expertRec.RecordInstID.ToString());
              writer.WriteElementString("StartTime", expertRec.StartTime.ToString("yyyy-MM-ddTHH:mm:ss.fff"));
              if (!expertRec.IsEndTimeNull())
                writer.WriteElementString("EndTime", expertRec.EndTime.ToString("yyyy-MM-ddTHH:mm:ss.fff"));
              writer.WriteElementString("LastModified", expertRec.LastModified.ToString("yyyy-MM-ddTHH:mm:ss.fff"));
              writer.WriteElementString("CompleteData", "true");
              writer.WriteElementString("BigEndianData", "false");
              writer.WriteElementString("Source", Properties.Settings.Default.ExpertSourceName);
              if (!expertRec.IsBinDataNull())
                writer.WriteElementString("BinaryData", Convert.ToBase64String(expertRec.BinData));
              writer.WriteEndElement();
            }
        logger.LogText(3, "DDExchange", "Stored {0} expert records into file {1}", expertRecords.Count, fileName);
        //Copy file to backup dirctory
        File.Copy(completeFileName, Path.Combine(backupDirInfo.FullName, fileName));
        //Move file to DRImport direcory
        File.Move(completeFileName, Path.Combine(Properties.Settings.Default.DRImportDirectory, fileName));
        logger.LogText(3, "DDExchange", "Moved file into DRImport directory");
      }
      catch (Exception ex)
      {
        logger.LogText(1, "DDExchange", "Failed to store records into DRImport file");
        logger.LogException(1, "DDExchange", ex);
      }
    }

    /// <summary>
    /// Computes start time of first exchange period after startup.
    /// Based on ExchangePeriod and ExchnageDelay.
    /// </summary>
    /// <returns></returns>
    public DateTime GetFirstExchangePeriodStart()
    {
      DateTime firstAvailable = DateTime.UtcNow - Properties.Settings.Default.ExchangeDelay;
      long perTicks = Properties.Settings.Default.ExchangePeriod.Ticks;
      DateTime firstPerEnd = new DateTime(((firstAvailable.Ticks / perTicks) + 1) * perTicks);
      DateTime firstPerStart = firstPerEnd - Properties.Settings.Default.ExchangePeriod;
      return firstPerStart;
    }
    #endregion //Helper methods
  }
}
