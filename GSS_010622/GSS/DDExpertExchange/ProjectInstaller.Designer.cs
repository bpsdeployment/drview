namespace DDExpertExchange
{
  partial class ProjectInstaller
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.DDExpertExchange = new System.ServiceProcess.ServiceInstaller();
      this.DDExpExchangeProcess = new System.ServiceProcess.ServiceProcessInstaller();
      // 
      // DDExpertExchange
      // 
      this.DDExpertExchange.Description = "Exports records for expert application and imports the results into CDDB";
      this.DDExpertExchange.DisplayName = "DD Expert Exchange";
      this.DDExpertExchange.ServiceName = "ExpExchangeService";
      this.DDExpertExchange.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
      // 
      // DDExpExchangeProcess
      // 
      this.DDExpExchangeProcess.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
      this.DDExpExchangeProcess.Password = null;
      this.DDExpExchangeProcess.Username = null;
      // 
      // ProjectInstaller
      // 
      this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.DDExpertExchange,
            this.DDExpExchangeProcess});

    }

    #endregion

    private System.ServiceProcess.ServiceInstaller DDExpertExchange;
    private System.ServiceProcess.ServiceProcessInstaller DDExpExchangeProcess;
  }
}