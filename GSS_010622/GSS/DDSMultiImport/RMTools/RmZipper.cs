using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.Diagnostics;

namespace RMTools
{
    public class RmZipper
    {
        private string cmd7Zip;
        private bool bLogOutput;
        private int TimeoutSec;
        private bool bAsynchronousOutput;
        private string OutputReceived;
        private Process proc;

        public RmZipper()
        {
            cmd7Zip = "";
            bLogOutput = true;
            TimeoutSec = 120;
            bAsynchronousOutput = false;
            proc = null;
        }
        public RmZipper(bool LogOutput)
        {
            cmd7Zip = "";
            bLogOutput = LogOutput;
            TimeoutSec = 120;
            bAsynchronousOutput = false;
            proc = null;
        }
        ~RmZipper()
        {
            if (proc != null)
                try
                {
                    proc.Kill();
                    proc.Dispose();
                }
                catch (Exception)
                { }
        }
        public bool LogOutput
        {
            get { return bLogOutput; }
            set { bLogOutput = value; }
        }
        /// <summary>
        /// Gets or sets zip process timeout.
        /// </summary>
        public int Timeout
        {
            get { return TimeoutSec; }
            set { TimeoutSec = value; }
        }
        /// <summary>
        /// Gets or sets full path of zip program.
        /// </summary>
        public String SevenZipExeFile
        {
            get { return cmd7Zip; }
            set { cmd7Zip = value; }
        }
        public bool AsynchronousOutput
        {
            get { return bAsynchronousOutput; }
            set { bAsynchronousOutput = value; }
        }

        public void SevenZip(string file, string workDir)
        {
            System.IO.FileInfo prg = new System.IO.FileInfo(cmd7Zip);
            if (!prg.Exists)
                throw new Exception("Program 7z not found");

            string args = " a " + file;
            try
            {
                //Create ProcessStartInfo
                ProcessStartInfo startInfo = new ProcessStartInfo(cmd7Zip, args);
                //startInfo.Arguments = "";
                startInfo.WorkingDirectory = workDir;
                startInfo.CreateNoWindow = true;
                startInfo.UseShellExecute = false;
                startInfo.RedirectStandardOutput = true;

                //Create ZIP process
                proc = new Process();
                proc.StartInfo = startInfo;

                //Add handler for output data of the process
                if (bAsynchronousOutput && bLogOutput)
                {
                    OutputReceived = "";
                    proc.OutputDataReceived += new DataReceivedEventHandler(ProcessOutputDataReceived);
                }

                //Start Application process
                proc.Start();
                //Start asynchronous reading of output data
                if (bAsynchronousOutput && bLogOutput)
                    proc.BeginOutputReadLine();

                //Wait for process to exit with specified timeout
                if (!proc.WaitForExit(TimeoutSec * 1000))
                { //Timeout expired, kill the process
                    //logger.LogText(0, "DDSImp", "Application did not finish in {0} sec. Killing the process.", Properties.Settings.Default.MWDConvTimeoutSec);
                    proc.Kill();
                    if (!proc.WaitForExit(TimeoutSec * 1000))
                        throw new Exception("Application did not finish in " + TimeoutSec + " sec. and the process did not Kill-able.");
                    else
                        throw new Exception("Application did not finish in " + TimeoutSec + " sec. Killing the process.");
                }
                proc.Dispose();
                proc = null;
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to zip using 7z", ex);
            }
        }

        public void SevenUnZip(string file, string workDir)
        {
            System.IO.FileInfo prg = new System.IO.FileInfo(cmd7Zip);
            if (!prg.Exists)
                throw new Exception("Program 7z not found");
            
            string args = " e " + file;

            //Create ProcessStartInfo
            ProcessStartInfo startInfo = new ProcessStartInfo(cmd7Zip, args);
            //startInfo.Arguments = "";
            startInfo.WorkingDirectory = workDir;
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;

            //Create ZIP process
            Process proc = new Process();
            proc.StartInfo = startInfo;

            try
            {
                //Add handler for output data of the process
                if (bAsynchronousOutput && bLogOutput)
                {
                    OutputReceived = "";
                    proc.OutputDataReceived += new DataReceivedEventHandler(ProcessOutputDataReceived);
                }

                //Start Application process
                proc.Start();
                //Start asynchronous reading of output data
                if (bAsynchronousOutput && bLogOutput)
                    proc.BeginOutputReadLine();
                //Wait for process to exit with specified timeout
                if (!proc.WaitForExit(TimeoutSec * 1000))
                { //Timeout expired, kill the process
                    //logger.LogText(0, "DDSImp", "Application did not finish in {0} sec. Killing the process.", Properties.Settings.Default.MWDConvTimeoutSec);
                    proc.Kill();
                    if (!proc.WaitForExit(TimeoutSec * 1000))
                        throw new Exception("Application did not finish in " + TimeoutSec + " sec. and the process is not Kill-able.");
                    else
                        throw new Exception("Application did not finish in " + TimeoutSec + " sec. Killing the process.");
                }
                //Start synchronous reading of output data
                else if (!bAsynchronousOutput && bLogOutput)
                {
                    string rtn = proc.StandardOutput.ReadToEnd();
                    int pos = rtn.IndexOf("Error");
                    if (pos >= 0)
                    {
                        string err = rtn.Substring(pos);
                        err = err.Replace("\r\n\r\n", " ");
                        throw new Exception(err);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to unzip using 7z", ex);
            }
            finally
            {
                proc.Dispose();
            }
        }

        /// <summary>
        /// Handler called automatically when zip process writes data to its standard output
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Data written to the output</param>
        void ProcessOutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if ((e.Data != null) && (e.Data.Trim() != String.Empty))
            {
                if (OutputReceived.Length != 0) OutputReceived += "\r\n";
                OutputReceived += e.Data;
            }
            /*
            //Write data to log file (only if some files were moved to convert dir and line is not empty)
            if ((movedCount > 0) && (e.Data != null) && (e.Data.Trim() != String.Empty))
                logger.LogText(6, "Zipper", e.Data);
        */
        }

     
/*
        public void WinZIP(string file, string workDir)
        {
            string filename;
            RegistryKey reg;
            object obj;
            //string args;
            //ProcessStartInfo procInfo;
            Process proc;
            try
            {
                reg = Registry.ClassesRoot.OpenSubKey(@"Applications\WinZIP32.exe\Shell\Open\Command");
                obj = reg.GetValue("");
                filename = obj.ToString();
                reg.Close();
                filename = filename.Substring(1, filename.Length - 7);
                string args = " a " + " test.rar " + " " + @"C:\test\test.txt";

                ProcessStartInfo procInfo = new ProcessStartInfo();
                procInfo.FileName = filename;
                procInfo.Arguments = args;
                procInfo.WindowStyle = ProcessWindowStyle.Hidden;
                procInfo.WorkingDirectory = @"C:\test\";

                proc = new Process();
                proc.StartInfo = procInfo;
                proc.Start();
            }
            catch
            {
            }
        }

        public void WinRAR(string file, string workDir)
        {
            string filename;
            RegistryKey reg;
            object obj;
            //string args;
            //ProcessStartInfo procInfo;
            Process proc;
            try
            {
                reg = Registry.ClassesRoot.OpenSubKey(@"Applications\WinRAR.exe\Shell\Open\Command");
                obj = reg.GetValue("");
                filename = obj.ToString();
                reg.Close();
                filename = filename.Substring(1, filename.Length - 7);
                string args = " a " + " test.rar " + " " + @"C:\test\test.txt";

                ProcessStartInfo procInfo = new ProcessStartInfo();
                procInfo.FileName = filename;
                procInfo.Arguments = args;
                procInfo.WindowStyle = ProcessWindowStyle.Hidden;
                procInfo.WorkingDirectory = @"C:\test\";

                proc = new Process();
                proc.StartInfo = procInfo;
                proc.Start();
            }
            catch
            {
            }
        }
*/
    }
}
