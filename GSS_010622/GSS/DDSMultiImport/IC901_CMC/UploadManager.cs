using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
//using RmZipper;
using System.Xml;
using System.Xml.Xsl;
using System.Collections;
using RMTools;

namespace IC901_CMC
{
    public partial class UploadManager
    {
        private bool enabled;

        private string uploadDir;
        private string convertDir;
        private string importDir;
        private string repositoryDir;
        private string subOkDir;
        private string subFailedDir;
        private string badFileName;

        private string zipFileName;
        private int zipTimeout;

        private DirectoryInfo WorkDirInfo;
        private DirectoryInfo ImportDir;

        private Logger logger;
        private CMC appl;

        private int maxFileInBatch;

        public UploadManager()
        {
            logger = new Logger();
            logger.FileName = Properties.Settings.Default.LogFileName;
            logger.FileVerbosity = int.Parse(Properties.Settings.Default.LogVerbosity);
            logger.LogToFile = true;
            logger.MaxFileLength = int.Parse(Properties.Settings.Default.MaxLogFileLength);
            logger.MaxHistoryFiles = int.Parse(Properties.Settings.Default.MaxLogHistoryFiles);
            logger.ListBox = null;
            logger.ListBoxVerbosity = 3;
            logger.LogToListBox = false;
            logger.MaxListBoxLines = 1000;

            Properties.Settings.Default.Reload();
            enabled = Properties.Settings.Default.Enabled;
            uploadDir = Properties.Settings.Default.DDSSourceDir;
            convertDir = Properties.Settings.Default.DDSConvertDir;
            importDir = Properties.Settings.Default.DRImportDir;
            repositoryDir = Properties.Settings.Default.DDSRepositoryDir;
            subOkDir = Properties.Settings.Default.DDSSourceSubdirOk;
            subFailedDir = Properties.Settings.Default.DDSSourceSubdirFailed;
            badFileName = Properties.Settings.Default.DDSSourceSubdirBadfiles;
            maxFileInBatch = Properties.Settings.Default.maxFileInBatch;

            zipFileName = Properties.Settings.Default.ZipExe;
            zipTimeout = Properties.Settings.Default.SubFnTimeout;
        }

        public UploadManager(Logger theLogger, string UploadDir, string WorkDir, string ImportDir, string RepositoryDir, string OKDir, string FailedDir, string BadFilesName)
        {
            enabled = true;
            logger = theLogger;
            uploadDir = UploadDir;
            convertDir = WorkDir;
            importDir = ImportDir;
            repositoryDir = RepositoryDir;
            subOkDir = OKDir;
            subFailedDir = FailedDir;
            badFileName = BadFilesName;
            //Init();
        }

        public UploadManager(Logger theLogger, string UploadDir, string WorkDir, string ImportFileDir, string RepositoryDir)
        {
            enabled = true;
            logger = theLogger;
            uploadDir = UploadDir;
            convertDir = WorkDir;
            importDir = ImportFileDir;
            repositoryDir = RepositoryDir;
            subOkDir = "ok";
            subFailedDir = "Failed";
            badFileName = "badfiles.txt";
            //Init();
        }

        ~UploadManager()
        {
        }
        /// <summary>
        /// Reload all settings
        /// </summary>
        /// <param name="theLogger"></param>
        /// <param name="UploadDir"></param>
        /// <param name="WorkDir"></param>
        /// <param name="ImportDir"></param>
        /// <param name="RepositoryDir"></param>
        /// <param name="OKDir"></param>
        /// <param name="FailedDir"></param>
        /// <param name="BadFilesName"></param>
        public void Settings(Logger theLogger, string UploadDir, string WorkDir, string ImportDir, string RepositoryDir, string OKDir, string FailedDir, string BadFilesName)
        {
            logger = theLogger;
            uploadDir = UploadDir;
            convertDir = WorkDir;
            importDir = ImportDir;
            subOkDir = OKDir;
            subFailedDir = FailedDir;
            badFileName = BadFilesName;
            //Init();
        }

        private void Init()
        {
            WorkDirInfo = new DirectoryInfo(convertDir);
            //appl = new CMC(Properties.Settings.Default.CDDBConnString, Properties.Settings.Default.TUType);
            if (importDir != "")
                ImportDir = new DirectoryInfo(importDir);
            else
                ImportDir = null;
        }

        /// <summary>
        /// Verify every uploaded files then notify each file into OK directory or move file into Failed directory and sign into badfiles.txt file.
        /// </summary>
        public void Execute()
        {
            int fileCnt = 0;

            if (!enabled)
            {
                System.Console.WriteLine("Application isn't enabled!");
                return;
            }
            try
            {
                Init();

                XslCompiledTransform xsltGuasti = new System.Xml.Xsl.XslCompiledTransform();
                xsltGuasti.Load("CMC_Guasti.xslt");
                XslCompiledTransform xsltLog = new System.Xml.Xsl.XslCompiledTransform();
                xsltLog.Load("CMC_Log.xslt");


                DirectoryInfo DirInfo1 = new DirectoryInfo(uploadDir);
                DirInfo1.Refresh();
                foreach (DirectoryInfo dir1 in DirInfo1.GetDirectories())
                {
                    DirectoryInfo DirInfo2 = new DirectoryInfo(dir1.FullName);
                    DirInfo2.Refresh();
                    foreach (DirectoryInfo dir2 in DirInfo2.GetDirectories())
                    {
                        //logger.LogText(1, "Execute", "Look at {0}", dir.Name);
                        // Make environment
                        string dirname = dir2.FullName;

                        string Composition = Path.GetFileName(dirname);
                        //string filesDir = Path.Combine(dirname, "Files");
                        DirectoryInfo DirInfoFiles = new DirectoryInfo(dirname);
                        DirInfoFiles.Refresh();

                        /*
                        DirectoryInfo OKDir = new DirectoryInfo(Path.Combine(DirInfoFiles.FullName, subOkDir));
                        DirectoryInfo FailedDir = new DirectoryInfo(Path.Combine(DirInfoFiles.FullName, subFailedDir));
                        FileInfo badFiles = new FileInfo(Path.Combine(DirInfoFiles.FullName, badFileName));
                        if (!OKDir.Exists) OKDir.Create();
                        if (!FailedDir.Exists) FailedDir.Create();
                        if (!badFiles.Exists) badFiles.Create();

                        ArrayList badfiles = GetBadList(badFiles.FullName);
                        */

                        //DirectoryInfo RepDir = new DirectoryInfo(Path.Combine(Path.Combine(repositoryDir, DirInfo1.Name), dir2.Name));
                        //if (!RepDir.Exists) RepDir.Create();



                        foreach (FileInfo xmlfile in DirInfoFiles.GetFiles("*.xml", SearchOption.TopDirectoryOnly))
                        {
                            //zipfile.Refresh();
                            if (!xmlfile.IsReadOnly && xmlfile.Length > 0)
                            {
                                //XmlDocument xdoc = new XmlDocument();
                                //xdoc.Load(zipfile.FullName);


                                XmlDocument xdoc = null;
                                if (xmlfile.Name.ToLower().StartsWith("guasti"))
                                    xdoc = TransformXml(xmlfile.FullName, xsltGuasti);
                                else if (xmlfile.Name.ToLower().StartsWith("log"))
                                    xdoc = TransformXml(xmlfile.FullName, xsltLog);

                                if (xdoc != null)
                                {
                                    // add train composition name and time
                                    #region add train composition name and time
                                    foreach (XmlNode nVehicle in xdoc.DocumentElement.SelectNodes("/dataroot/vehicle"))
                                    {
                                        XmlAttribute aComposition = xdoc.CreateAttribute("TrainComp");
                                        aComposition.Value = dir2.Name;
                                        nVehicle.Attributes.Append(aComposition);

                                        // replace time
                                        foreach (XmlNode n in nVehicle.ChildNodes)
                                        {
                                            XmlAttribute aCome = n.Attributes["come"];
                                            if (aCome != null)
                                            {
                                                string s = aCome.Value;
                                                double secs;
                                                if (double.TryParse(s, out secs))
                                                {
                                                    DateTime dt = new DateTime(1970, 1, 1).AddSeconds(secs);
                                                    XmlAttribute aTime = xdoc.CreateAttribute("Time");
                                                    aTime.Value = dt.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
                                                    n.Attributes.Append(aTime);
                                                }
                                            }
                                        }
                                    }
                                    #endregion //add train composition name and time

                                    // compact binary data
                                    #region compact binary data
                                    try
                                    {
                                        foreach (XmlNode nDEvent in xdoc.DocumentElement.SelectNodes("/dataroot/vehicle/dia_event"))
                                        {
                                            XmlAttribute aEvID = nDEvent.Attributes["ev_id"];
                                            if (aEvID == null)
                                                throw new Exception("Attribute 'ev_id' missing");
                                            foreach (XmlNode nDamb in nDEvent.SelectNodes("damb"))
                                            {

                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.LogText(1, "CompactBinaryData", "Error: {0}", ex.Message);
                                        continue;
                                    }
                                    #endregion //compact binary data

                                    // save file to upload
                                    #region save file to upload
                                    #endregion //save file to upload

                                    // move file as zip to repository
                                    #region move file as zip to repository
                                    #endregion //move file as zip to repository
                                }




                                //transform.Transform(

                                /*
                                // Unzip file
                                ClearWorkDir();
                                try
                                {
                                    string zipfilename = zipfile.FullName;
                                    RmZipper zip = new RmZipper();
                                    zip.SevenZipExeFile = zipFileName;
                                    zip.Timeout = zipTimeout;
                                    zip.SevenUnZip(zipfilename, convertDir);
                                    //zip.SevenZip(Path.GetFileName(filename), workDir);
                                }
                                catch (Exception ex)
                                {
                                    logger.LogText(2, "Unzip", "File {0}: {1}", zipfile.Name, ex.Message);
                                    logger.LogText(3, "Unzip", "{0}", ex.InnerException);
                                    try
                                    {
                                        MoveAndNotifyBadfile(badfiles, badFiles, zipfile, FailedDir);
                                    }
                                    catch (Exception ex2)
                                    {
                                        logger.LogText(4, "Moving", "{0}", ex2.Message);
                                        continue;
                                    }
                                }*/

                                /*WorkDirInfo.Refresh();
                                FileInfo[] filelist = WorkDirInfo.GetFiles("*.md5");
                                if (filelist.Length == 0)
                                {
                                    logger.LogText(2, "Unzip", "Missing MD5 file into {0} file", zipfile.Name);
                                    // Move zip file to Failed directory
                                    if (zipfile.Exists)
                                        MoveFile(zipfile, FailedDir);
                                }*/

                                /*
                                // scan each md5 file
                                foreach (FileInfo file in filelist)
                                {
                                    try
                                    {
                                        if (ValidateFileMD5(file, TUInstanceID))
                                        {
                                            // Release name from badfiles.txt file
                                            if (badFileName != "")
                                            {
                                                int position = badfiles.IndexOf(zipfile.Name);
                                                if (position >= 0)
                                                {
                                                    // remove
                                                    badfiles.RemoveAt(position);
                                                    // save
                                                    SaveBadList(badFiles.FullName, badfiles);
                                                }
                                            }

                                            // Move converted file to ImportDR directory
                                            MoveXmlFiles();

                                            // Move zip file to Repository directory
                                            MoveFile(zipfile, RepDir);

                                            // Notify good file into OK directory
                                            //MoveFile(file, OKDir);

                                            fileCnt++;
                                            if (fileCnt >= maxFileInBatch)
                                            {
                                                logger.LogText(2, "ValidateMD5", "Converted {0} files", fileCnt);
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            // Move converted file to ImportDR directory
                                            MoveXmlFiles();

                                            logger.LogText(2, "ValidateMD5", "Moving failed {0}", zipfile.Name);
                                            try
                                            {
                                                MoveAndNotifyBadfile(badfiles, badFiles, zipfile, FailedDir);
                                            }
                                            catch (Exception ex)
                                            {
                                                logger.LogText(3, "Moving", "{0}", ex.Message);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.LogText(2, "Managing", "File {0}: {1}", file.Name, ex.Message);
                                        throw ex;
                                    }
                                }*/


                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // log error
                logger.LogText(1, "Execute", "{0}", ex.Message);
                // write to console
                System.Console.WriteLine(ex.Message);
            }
        }

        public XmlDocument TransformXml(string xmlFile, XslCompiledTransform xslt)
        {
            //XmlDocument xd = new XmlDocument();
            //xd.Load(xmlFile);

            try
            {

                MemoryStream ms = new MemoryStream();
                //xslt.Transform(xd, null, ms);
                xslt.Transform(xmlFile, null, ms);

                ms.Position = 1;
                StreamReader sr = new StreamReader(ms);
                string s = sr.ReadToEnd();
                sr.Dispose();

                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(s.Substring(2));

                return xdoc;

            }
            catch (Exception ex)
            {
                logger.LogText(1, "TransformXml", "Error: {0}", ex.Message);
            }
            finally
            {
            }
            return null;
        }

        private void ClearWorkDir()
        {
            try
            {
                WorkDirInfo.Refresh();
                foreach (DirectoryInfo dir in WorkDirInfo.GetDirectories())
                {
                    dir.Delete(true);
                }
                foreach (FileInfo file in WorkDirInfo.GetFiles())
                {
                    file.Delete();
                }
            }
            catch (Exception ex)
            {
                // stop execution
                throw ex;
            }
        }

        private void MoveAndNotifyBadfile(ArrayList badfiles, FileInfo badFiles, FileInfo zipfile, DirectoryInfo FailedDir)
        {
            // Move zip file to Failed directory
            MoveFile(zipfile, FailedDir);

            // Notify bad file into badfiles.txt file
            if (badFileName != "")
            {
                int position = badfiles.IndexOf(zipfile.Name);
                if (position == -1)
                {
                    // add
                    badfiles.Add(zipfile.Name);
                    // save
                    SaveBadList(badFiles.FullName, badfiles);
                }
            }
        }

        private ArrayList GetBadList(string name)
        {
            ArrayList badfiles = new ArrayList();
            try
            {
                string[] sList;
                StreamReader sr = new StreamReader(name);
                string list = sr.ReadToEnd();
                sList = list.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                sr.Close();
                foreach (string s in sList) badfiles.Add(s);
            }
            catch (Exception ex)
            {
                // stop execution
                throw ex;
            }
            return badfiles;
        }

        private void SaveBadList(string name, ArrayList badfiles)
        {
            try
            {
                // get list into string form
                string bf = "";
                foreach (string s in badfiles)
                {
                    if (bf.Length != 0) bf += " ";
                    bf += s;
                }
                // write badfiles file
                StreamWriter sw = new StreamWriter(name);
                sw.Write(bf);
                sw.Close();
            }
            catch (Exception ex)
            {
                // stop execution
                throw ex;
            }
        }

        private void MoveXmlFiles()
        {
            if (ImportDir == null || !ImportDir.Exists)
                return;
            try
            {
                int nr = WorkDirInfo.GetFiles("*.xml").Length;
                logger.LogText(2, "DDSImport", "Coverted {0} xml files", nr);
                foreach (FileInfo file in WorkDirInfo.GetFiles("*.xml"))
                {
                    string dest = Path.Combine(ImportDir.FullName, file.Name);
                    try
                    {
                        file.MoveTo(dest);
                    }
                    catch (Exception) { }
                }
            }
            catch (Exception ex)
            {
                // stop execution
                throw ex;
            }
        }

        private void MoveFile(FileInfo from, DirectoryInfo to)
        {
            if (to == null) return;
            if (!to.Exists) to.Create();

            try
            {
                string dest = Path.Combine(to.FullName, from.Name);
                FileInfo destfile = new FileInfo(dest);
                if (destfile.Exists)
                    destfile.Delete();
                from.MoveTo(dest);
            }
            catch (Exception ex)
            {
                logger.LogText(3, "DDSImp", "Failed to move file: {0}", from.Name);
                // stop execution
                throw ex;
            }
        }

        private static int ReadAllBytesFromStream(Stream stream, byte[] buffer)
        {
            // Use this method is used to read all bytes from a stream.
            int offset = 0;
            int totalCount = 0;
            while (true)
            {
                int bytesRead = stream.Read(buffer, offset, 100);
                if (bytesRead == 0)
                {
                    break;
                }
                offset += bytesRead;
                totalCount += bytesRead;
            }
            return totalCount;
        }
        public static bool CompareData(byte[] buf1, int len1, byte[] buf2, int len2)
        {
            // Use this method to compare data from two different buffers.
            if (len1 != len2)
            {
                //Console.WriteLine("Number of bytes in two buffer are different {0}:{1}", len1, len2);
                return false;
            }

            for (int i = 0; i < len1; i++)
            {
                if (buf1[i] != buf2[i])
                {
                    //Console.WriteLine("byte {0} is different {1}|{2}", i, buf1[i], buf2[i]);
                    return false;
                }
            }
            Console.WriteLine("All bytes compare.");
            return true;
        }


        private bool ValidateFileMD5(FileInfo fileMD5, string TUInstanceID)
        {
            bool result = true;
            bool rtn = true;
            string file = "";

            //FileInfo fileMD5 = new FileInfo(fileMD5xxx);
            if (fileMD5.Exists)
            {
                StreamReader sr = new StreamReader(fileMD5.FullName);
                while (!sr.EndOfStream)
                {
                    try
                    {
                        string ln = sr.ReadLine();
                        //ln.Replace("  ", " ");
                        string[] fields = ln.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        byte[] md5 = new byte[16];
                        if (fields[0].Length == 32)
                        {
                            for (int i = 0; i < 16; i++)
                                md5[i] = byte.Parse(fields[0].Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                            file = fields[1];
                            file = Path.Combine(fileMD5.DirectoryName, file.Replace(":", "_"));
                            result = VerifyMD5(file, md5);
                            rtn &= result;
                        }
                        else
                            continue;
                    }
                    catch (Exception ex)
                    {
                        logger.LogText(3, "Validate", "{0}", ex.Message);
                        rtn = false;
                        continue;
                    }
                    if (result)
                    {
                        FileStream currDRFileStream = null;
                        XmlWriter dstWriter = null;
                        try
                        {
                            string drFileName = Path.Combine(Path.GetDirectoryName(file), TUInstanceID + "_" + Path.GetFileNameWithoutExtension(file) + ".xml");
                            FileInfo f = new FileInfo(drFileName);
                            if (f.Exists) f.Delete();
                            //Open new file stream with name
                            currDRFileStream = new FileStream(drFileName, FileMode.CreateNew, FileAccess.Write, FileShare.None);
                            //Open new XML writer
                            XmlWriterSettings settings = new XmlWriterSettings();
                            settings.Indent = true;
                            settings.Encoding = Encoding.UTF8;
                            settings.OmitXmlDeclaration = true;
                            settings.ConformanceLevel = ConformanceLevel.Fragment;
                            settings.CloseOutput = true;
                            dstWriter = XmlWriter.Create(currDRFileStream, settings);

                            //dstWriter.WriteStartElement("Records");
                            //dstWriter.WriteAttributeString("TUInstanceID", TUInstanceID);

                            rtn &= appl.FileConvert(TUInstanceID, file, dstWriter);

                            //dstWriter.WriteEndElement();
                        }
                        catch (Exception ex)
                        {
                            //throw ex;
                            logger.LogText(1, "Validate", "ValidateFileMD5 error file {0} : {1}", fileMD5.Name, ex.Message);
                            rtn = false;
                        }
                        finally
                        {
                            //Close writer
                            if (dstWriter != null)
                            {
                                dstWriter.Close();
                            }
                            //Close and dispose file stream
                            if (currDRFileStream != null)
                            {
                                currDRFileStream.Close();
                                currDRFileStream.Dispose();
                            }
                            currDRFileStream = null;
                            dstWriter = null;
                        }
                    }
                }
                sr.Close();
            }
            return rtn;
        }

        protected bool VerifyMD5(string file, byte[] md5)
        {
            FileInfo locFileInfo = null;
            FileStream locFile = null;

            //Create file info and file stream for local file
            //locFile = File.Create(file);
            locFileInfo = new FileInfo(file);
            locFile = new FileStream(file, FileMode.Open, FileAccess.Read);

            try
            {
                //Compute MD5 digest for file
                byte[] digest;
                locFile.Seek(0, SeekOrigin.Begin);
                MD5 hashGenerator = MD5CryptoServiceProvider.Create();
                digest = hashGenerator.ComputeHash(locFile);

                for (int i = 0; i < 16; i++)
                    if (md5[i] != digest[i])
                    {
                        //Close file stream
                        locFile.Dispose();
                        return false;
                    }
            }
            //catch (Exception ex)
            catch (Exception ex)
            {
                throw ex;
            }
            //Close file stream
            locFile.Dispose();
            return true;
        }
    }
}


