﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="xml" indent="yes"/>

  <!--DiagnoasticRecords-->
  <xsl:template match="dataroot">
    <xsl:element name="dataroot">
      <xsl:attribute name="vers">
        <xsl:value-of select="vers/node()"/>
      </xsl:attribute>
      <xsl:attribute name="file">
        <xsl:value-of select="'guasti'"/>
      </xsl:attribute>

      <xsl:for-each select="vehicle">
        <xsl:element name="vehicle">
          <xsl:attribute name="id">
            <xsl:value-of select="id/node()"/>
          </xsl:attribute>
          <xsl:attribute name="cmcvers">
            <xsl:value-of select="../cmcvers/node()"/>
          </xsl:attribute>

          <xsl:for-each select="dia_event">
            <xsl:element name="dia_event">
              <xsl:attribute name="ev_id">
                <xsl:value-of select="ev_id/node()"/>
              </xsl:attribute>
              <xsl:attribute name="come">
                <xsl:value-of select="come/node()"/>
              </xsl:attribute>
              <xsl:attribute name="pri">
                <xsl:value-of select="pri/node()"/>
              </xsl:attribute>
              <xsl:attribute name="frq">
                <xsl:value-of select="frq/node()"/>
              </xsl:attribute>
              
              <xsl:for-each select="damb">
                <xsl:element name="damb">

                  <xsl:attribute name="pos">
                    <xsl:value-of select="position()-1"/>
                  </xsl:attribute>
                  <xsl:attribute name="var">
                    <xsl:value-of select="desc_id/node()"/>
                  </xsl:attribute>
                  <xsl:choose>
                    <xsl:when test="valore/node() ='***'">
                      <xsl:attribute name="val">
                        <xsl:value-of select="''"/>
                      </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:attribute name="val">
                        <xsl:value-of select="valore/node()"/>
                      </xsl:attribute>
                    </xsl:otherwise>
                  </xsl:choose>

                </xsl:element>
              </xsl:for-each>

            </xsl:element>
          </xsl:for-each>

        </xsl:element>
      </xsl:for-each>

    </xsl:element>
  </xsl:template>
  <!--/DiagnoasticRecords-->
</xsl:stylesheet>
