﻿namespace IC901_CMC
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblChgLogFileName = new System.Windows.Forms.Label();
            this.lblChgTUSubDirectoryOK = new System.Windows.Forms.Label();
            this.btnRepositoryDir = new System.Windows.Forms.Button();
            this.txtRepositoryDir = new System.Windows.Forms.TextBox();
            this.btnImportDir = new System.Windows.Forms.Button();
            this.txtImportDir = new System.Windows.Forms.TextBox();
            this.btnWorkDir = new System.Windows.Forms.Button();
            this.txtWorkDir = new System.Windows.Forms.TextBox();
            this.btnDirectory = new System.Windows.Forms.Button();
            this.txtDirectory = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.chkEnabled = new System.Windows.Forms.CheckBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtLogVerbosity = new System.Windows.Forms.TextBox();
            this.btnSubdirBadfiles = new System.Windows.Forms.Button();
            this.txtSubdirBadfiles = new System.Windows.Forms.TextBox();
            this.btnTUSubDirectoryFailed = new System.Windows.Forms.Button();
            this.txtTUSubDirectoryFailed = new System.Windows.Forms.TextBox();
            this.btnTUSubDirectoryOK = new System.Windows.Forms.Button();
            this.txtTUSubDirectoryOK = new System.Windows.Forms.TextBox();
            this.txtConnectionString = new System.Windows.Forms.TextBox();
            this.btnZipFile = new System.Windows.Forms.Button();
            this.txtZipFile = new System.Windows.Forms.TextBox();
            this.btnLogFileName = new System.Windows.Forms.Button();
            this.txtLogFileName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnRestore = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.otherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRun = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuReset = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSaveIntoConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.optionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuUpdateConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.lblChanged = new System.Windows.Forms.Label();
            this.lblChgDirectory = new System.Windows.Forms.Label();
            this.lblChgWorkDir = new System.Windows.Forms.Label();
            this.lblChgImportDir = new System.Windows.Forms.Label();
            this.lblChgRepositoryDir = new System.Windows.Forms.Label();
            this.lblChgTUSubDirectoryFailed = new System.Windows.Forms.Label();
            this.lblChgSubdirBadfiles = new System.Windows.Forms.Label();
            this.lblChgLogVerbosity = new System.Windows.Forms.Label();
            this.lblChgZipFile = new System.Windows.Forms.Label();
            this.lblChgConnectionString = new System.Windows.Forms.Label();
            this.lblChgEnabled = new System.Windows.Forms.Label();
            this.lblChgUpdateConfig = new System.Windows.Forms.Label();
            this.lblChgMaxFileInBatch = new System.Windows.Forms.Label();
            this.lblChgTUType = new System.Windows.Forms.Label();
            this.lblChgSubFnTimeout = new System.Windows.Forms.Label();
            this.lblChgMaxLogFileLength = new System.Windows.Forms.Label();
            this.lblChgMaxLogHistoryFiles = new System.Windows.Forms.Label();
            this.lblUpdateConfig = new System.Windows.Forms.Label();
            this.lblMaxFileInBatch = new System.Windows.Forms.Label();
            this.txtMaxFileInBatch = new System.Windows.Forms.TextBox();
            this.lblTUType = new System.Windows.Forms.Label();
            this.txtTUType = new System.Windows.Forms.TextBox();
            this.lblSubFnTimeout = new System.Windows.Forms.Label();
            this.txtSubFnTimeout = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMaxLogFileLength = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMaxLogHistoryFiles = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblChgLogFileName
            // 
            this.lblChgLogFileName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblChgLogFileName.AutoSize = true;
            this.lblChgLogFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChgLogFileName.ForeColor = System.Drawing.Color.Red;
            this.lblChgLogFileName.Location = new System.Drawing.Point(668, 331);
            this.lblChgLogFileName.Name = "lblChgLogFileName";
            this.lblChgLogFileName.Size = new System.Drawing.Size(12, 13);
            this.lblChgLogFileName.TabIndex = 52;
            this.lblChgLogFileName.Text = "*";
            this.toolTip1.SetToolTip(this.lblChgLogFileName, "To make effective, push Save botton");
            this.lblChgLogFileName.Visible = false;
            // 
            // lblChgTUSubDirectoryOK
            // 
            this.lblChgTUSubDirectoryOK.AutoSize = true;
            this.lblChgTUSubDirectoryOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChgTUSubDirectoryOK.ForeColor = System.Drawing.Color.Red;
            this.lblChgTUSubDirectoryOK.Location = new System.Drawing.Point(360, 200);
            this.lblChgTUSubDirectoryOK.Name = "lblChgTUSubDirectoryOK";
            this.lblChgTUSubDirectoryOK.Size = new System.Drawing.Size(12, 13);
            this.lblChgTUSubDirectoryOK.TabIndex = 48;
            this.lblChgTUSubDirectoryOK.Text = "*";
            this.toolTip1.SetToolTip(this.lblChgTUSubDirectoryOK, "To make effective, push Save botton");
            this.lblChgTUSubDirectoryOK.Visible = false;
            // 
            // btnRepositoryDir
            // 
            this.btnRepositoryDir.Location = new System.Drawing.Point(12, 154);
            this.btnRepositoryDir.Name = "btnRepositoryDir";
            this.btnRepositoryDir.Size = new System.Drawing.Size(136, 25);
            this.btnRepositoryDir.TabIndex = 11;
            this.btnRepositoryDir.Text = "Repository Dir";
            this.btnRepositoryDir.UseVisualStyleBackColor = true;
            this.btnRepositoryDir.Click += new System.EventHandler(this.btnRepositoryDir_Click);
            // 
            // txtRepositoryDir
            // 
            this.txtRepositoryDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRepositoryDir.Location = new System.Drawing.Point(154, 154);
            this.txtRepositoryDir.Name = "txtRepositoryDir";
            this.txtRepositoryDir.Size = new System.Drawing.Size(511, 20);
            this.txtRepositoryDir.TabIndex = 12;
            this.txtRepositoryDir.Text = "D:\\Telediagnostica\\Repository";
            this.txtRepositoryDir.TextChanged += new System.EventHandler(this.txtRepositoryDir_TextChanged);
            // 
            // btnImportDir
            // 
            this.btnImportDir.Location = new System.Drawing.Point(12, 123);
            this.btnImportDir.Name = "btnImportDir";
            this.btnImportDir.Size = new System.Drawing.Size(136, 25);
            this.btnImportDir.TabIndex = 9;
            this.btnImportDir.Text = "DR Import Dir";
            this.btnImportDir.UseVisualStyleBackColor = true;
            this.btnImportDir.Click += new System.EventHandler(this.btnImportDir_Click);
            // 
            // txtImportDir
            // 
            this.txtImportDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtImportDir.Location = new System.Drawing.Point(154, 123);
            this.txtImportDir.Name = "txtImportDir";
            this.txtImportDir.Size = new System.Drawing.Size(511, 20);
            this.txtImportDir.TabIndex = 10;
            this.txtImportDir.Text = "D:\\Telediagnostica\\App_Dirs\\Mio300\\DRImport_Files";
            this.txtImportDir.TextChanged += new System.EventHandler(this.txtImportDir_TextChanged);
            // 
            // btnWorkDir
            // 
            this.btnWorkDir.Location = new System.Drawing.Point(12, 92);
            this.btnWorkDir.Name = "btnWorkDir";
            this.btnWorkDir.Size = new System.Drawing.Size(136, 25);
            this.btnWorkDir.TabIndex = 7;
            this.btnWorkDir.Text = "Work Dir";
            this.btnWorkDir.UseVisualStyleBackColor = true;
            this.btnWorkDir.Click += new System.EventHandler(this.btnWorkDir_Click);
            // 
            // txtWorkDir
            // 
            this.txtWorkDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWorkDir.Location = new System.Drawing.Point(154, 92);
            this.txtWorkDir.Name = "txtWorkDir";
            this.txtWorkDir.Size = new System.Drawing.Size(511, 20);
            this.txtWorkDir.TabIndex = 8;
            this.txtWorkDir.Text = "C:\\ftproot\\CVS\\Work";
            this.txtWorkDir.TextChanged += new System.EventHandler(this.txtWorkDir_TextChanged);
            // 
            // btnDirectory
            // 
            this.btnDirectory.Location = new System.Drawing.Point(12, 61);
            this.btnDirectory.Name = "btnDirectory";
            this.btnDirectory.Size = new System.Drawing.Size(136, 25);
            this.btnDirectory.TabIndex = 5;
            this.btnDirectory.Text = "Source Directory";
            this.btnDirectory.UseVisualStyleBackColor = true;
            this.btnDirectory.Click += new System.EventHandler(this.btnDirectory_Click);
            // 
            // txtDirectory
            // 
            this.txtDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDirectory.Location = new System.Drawing.Point(154, 61);
            this.txtDirectory.Name = "txtDirectory";
            this.txtDirectory.Size = new System.Drawing.Size(511, 20);
            this.txtDirectory.TabIndex = 6;
            this.txtDirectory.Text = "C:\\ftproot\\CVS\\CC515B73-AFDB-4ED3-AF21-4123E879D4C0\\";
            this.txtDirectory.TextChanged += new System.EventHandler(this.txtDirectory_TextChanged);
            // 
            // chkEnabled
            // 
            this.chkEnabled.AutoSize = true;
            this.chkEnabled.Location = new System.Drawing.Point(289, 32);
            this.chkEnabled.Name = "chkEnabled";
            this.chkEnabled.Size = new System.Drawing.Size(65, 17);
            this.chkEnabled.TabIndex = 4;
            this.chkEnabled.Text = "Enabled";
            this.chkEnabled.UseVisualStyleBackColor = true;
            this.chkEnabled.CheckedChanged += new System.EventHandler(this.chkEnabled_CheckedChanged);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(443, 438);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "Save";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(605, 438);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Exit";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtLogVerbosity
            // 
            this.txtLogVerbosity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLogVerbosity.Location = new System.Drawing.Point(154, 297);
            this.txtLogVerbosity.Name = "txtLogVerbosity";
            this.txtLogVerbosity.Size = new System.Drawing.Size(50, 20);
            this.txtLogVerbosity.TabIndex = 26;
            this.txtLogVerbosity.Text = "6";
            this.txtLogVerbosity.TextChanged += new System.EventHandler(this.txtLogVerbosity_TextChanged);
            // 
            // btnSubdirBadfiles
            // 
            this.btnSubdirBadfiles.Location = new System.Drawing.Point(12, 259);
            this.btnSubdirBadfiles.Name = "btnSubdirBadfiles";
            this.btnSubdirBadfiles.Size = new System.Drawing.Size(136, 25);
            this.btnSubdirBadfiles.TabIndex = 24;
            this.btnSubdirBadfiles.Text = "TU Badfiles file name";
            this.btnSubdirBadfiles.UseVisualStyleBackColor = true;
            this.btnSubdirBadfiles.Click += new System.EventHandler(this.btnSubdirBadfiles_Click);
            // 
            // txtSubdirBadfiles
            // 
            this.txtSubdirBadfiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSubdirBadfiles.Location = new System.Drawing.Point(154, 259);
            this.txtSubdirBadfiles.Name = "txtSubdirBadfiles";
            this.txtSubdirBadfiles.Size = new System.Drawing.Size(200, 20);
            this.txtSubdirBadfiles.TabIndex = 25;
            this.txtSubdirBadfiles.Text = "badfiles.txt";
            this.txtSubdirBadfiles.TextChanged += new System.EventHandler(this.txtSubdirBadfiles_TextChanged);
            // 
            // btnTUSubDirectoryFailed
            // 
            this.btnTUSubDirectoryFailed.Location = new System.Drawing.Point(12, 228);
            this.btnTUSubDirectoryFailed.Name = "btnTUSubDirectoryFailed";
            this.btnTUSubDirectoryFailed.Size = new System.Drawing.Size(136, 25);
            this.btnTUSubDirectoryFailed.TabIndex = 22;
            this.btnTUSubDirectoryFailed.Text = "TU SubDirectory Failed";
            this.btnTUSubDirectoryFailed.UseVisualStyleBackColor = true;
            this.btnTUSubDirectoryFailed.Click += new System.EventHandler(this.btnTUSubDirectoryFailed_Click);
            // 
            // txtTUSubDirectoryFailed
            // 
            this.txtTUSubDirectoryFailed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTUSubDirectoryFailed.Location = new System.Drawing.Point(154, 228);
            this.txtTUSubDirectoryFailed.Name = "txtTUSubDirectoryFailed";
            this.txtTUSubDirectoryFailed.Size = new System.Drawing.Size(200, 20);
            this.txtTUSubDirectoryFailed.TabIndex = 23;
            this.txtTUSubDirectoryFailed.Text = "Failed";
            this.txtTUSubDirectoryFailed.TextChanged += new System.EventHandler(this.txtTUSubDirectoryFailed_TextChanged);
            // 
            // btnTUSubDirectoryOK
            // 
            this.btnTUSubDirectoryOK.Location = new System.Drawing.Point(12, 197);
            this.btnTUSubDirectoryOK.Name = "btnTUSubDirectoryOK";
            this.btnTUSubDirectoryOK.Size = new System.Drawing.Size(136, 25);
            this.btnTUSubDirectoryOK.TabIndex = 20;
            this.btnTUSubDirectoryOK.Text = "TU SubDirectory OK";
            this.btnTUSubDirectoryOK.UseVisualStyleBackColor = true;
            this.btnTUSubDirectoryOK.Click += new System.EventHandler(this.btnTUSubDirectoryOK_Click);
            // 
            // txtTUSubDirectoryOK
            // 
            this.txtTUSubDirectoryOK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTUSubDirectoryOK.Location = new System.Drawing.Point(154, 197);
            this.txtTUSubDirectoryOK.Name = "txtTUSubDirectoryOK";
            this.txtTUSubDirectoryOK.Size = new System.Drawing.Size(200, 20);
            this.txtTUSubDirectoryOK.TabIndex = 21;
            this.txtTUSubDirectoryOK.Text = "ok";
            this.txtTUSubDirectoryOK.TextChanged += new System.EventHandler(this.txtTUSubDirectoryOK_TextChanged);
            // 
            // txtConnectionString
            // 
            this.txtConnectionString.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConnectionString.Location = new System.Drawing.Point(154, 403);
            this.txtConnectionString.Name = "txtConnectionString";
            this.txtConnectionString.Size = new System.Drawing.Size(511, 20);
            this.txtConnectionString.TabIndex = 31;
            this.txtConnectionString.Text = "Data Source=localhost\\sqlserver;Initial Catalog=CDDB_MIO300;Integrated Security=T" +
    "rue";
            this.txtConnectionString.TextChanged += new System.EventHandler(this.txtConnectionString_TextChanged);
            // 
            // btnZipFile
            // 
            this.btnZipFile.Location = new System.Drawing.Point(12, 372);
            this.btnZipFile.Name = "btnZipFile";
            this.btnZipFile.Size = new System.Drawing.Size(136, 25);
            this.btnZipFile.TabIndex = 29;
            this.btnZipFile.Text = "Zip file";
            this.btnZipFile.UseVisualStyleBackColor = true;
            this.btnZipFile.Click += new System.EventHandler(this.btnZipFile_Click);
            // 
            // txtZipFile
            // 
            this.txtZipFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtZipFile.Location = new System.Drawing.Point(154, 372);
            this.txtZipFile.Name = "txtZipFile";
            this.txtZipFile.Size = new System.Drawing.Size(511, 20);
            this.txtZipFile.TabIndex = 30;
            this.txtZipFile.Text = "../7za/7za.exe";
            this.txtZipFile.TextChanged += new System.EventHandler(this.txtZipFile_TextChanged);
            // 
            // btnLogFileName
            // 
            this.btnLogFileName.Location = new System.Drawing.Point(12, 328);
            this.btnLogFileName.Name = "btnLogFileName";
            this.btnLogFileName.Size = new System.Drawing.Size(136, 25);
            this.btnLogFileName.TabIndex = 27;
            this.btnLogFileName.Text = "Log FileName";
            this.btnLogFileName.UseVisualStyleBackColor = true;
            this.btnLogFileName.Click += new System.EventHandler(this.btnLogFileName_Click);
            // 
            // txtLogFileName
            // 
            this.txtLogFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLogFileName.Location = new System.Drawing.Point(154, 328);
            this.txtLogFileName.Name = "txtLogFileName";
            this.txtLogFileName.Size = new System.Drawing.Size(511, 20);
            this.txtLogFileName.TabIndex = 28;
            this.txtLogFileName.Text = "D:\\Telediagnostica\\App_Dirs\\Logs\\IC901_CVS_DDSImport.log";
            this.txtLogFileName.TextChanged += new System.EventHandler(this.txtLogFileName_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(77, 300);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 38;
            this.label1.Text = "Log Verbosity";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 406);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 39;
            this.label2.Text = "Connection String";
            // 
            // btnRestore
            // 
            this.btnRestore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRestore.Location = new System.Drawing.Point(524, 438);
            this.btnRestore.Name = "btnRestore";
            this.btnRestore.Size = new System.Drawing.Size(75, 23);
            this.btnRestore.TabIndex = 2;
            this.btnRestore.Text = "Restore";
            this.btnRestore.UseVisualStyleBackColor = true;
            this.btnRestore.Click += new System.EventHandler(this.btnRestore_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.otherToolStripMenuItem,
            this.optionToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(692, 24);
            this.menuStrip1.TabIndex = 41;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // otherToolStripMenuItem
            // 
            this.otherToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuRun,
            this.toolStripSeparator1,
            this.mnuReset,
            this.mnuSaveIntoConfig});
            this.otherToolStripMenuItem.Name = "otherToolStripMenuItem";
            this.otherToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.otherToolStripMenuItem.Text = "Other";
            // 
            // mnuRun
            // 
            this.mnuRun.Name = "mnuRun";
            this.mnuRun.Size = new System.Drawing.Size(155, 22);
            this.mnuRun.Text = "RUN";
            this.mnuRun.ToolTipText = "Run the application";
            this.mnuRun.Click += new System.EventHandler(this.mnuRun_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(152, 6);
            // 
            // mnuReset
            // 
            this.mnuReset.Name = "mnuReset";
            this.mnuReset.Size = new System.Drawing.Size(155, 22);
            this.mnuReset.Text = "Reset";
            this.mnuReset.ToolTipText = "Restore entries from App.config file";
            this.mnuReset.Click += new System.EventHandler(this.mnuReset_Click);
            // 
            // mnuSaveIntoConfig
            // 
            this.mnuSaveIntoConfig.Name = "mnuSaveIntoConfig";
            this.mnuSaveIntoConfig.Size = new System.Drawing.Size(155, 22);
            this.mnuSaveIntoConfig.Text = "Save into .config";
            this.mnuSaveIntoConfig.ToolTipText = "Save entries into App.config file";
            this.mnuSaveIntoConfig.Click += new System.EventHandler(this.mnuSaveIntoConfig_Click);
            // 
            // optionToolStripMenuItem
            // 
            this.optionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuUpdateConfig});
            this.optionToolStripMenuItem.Name = "optionToolStripMenuItem";
            this.optionToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.optionToolStripMenuItem.Text = "Option";
            // 
            // mnuUpdateConfig
            // 
            this.mnuUpdateConfig.CheckOnClick = true;
            this.mnuUpdateConfig.Name = "mnuUpdateConfig";
            this.mnuUpdateConfig.Size = new System.Drawing.Size(143, 22);
            this.mnuUpdateConfig.Text = "Update Config";
            this.mnuUpdateConfig.ToolTipText = "Update App.config on Save";
            this.mnuUpdateConfig.Click += new System.EventHandler(this.mnuUpdateConfig_Click);
            // 
            // lblChanged
            // 
            this.lblChanged.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblChanged.AutoSize = true;
            this.lblChanged.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChanged.ForeColor = System.Drawing.Color.Red;
            this.lblChanged.Location = new System.Drawing.Point(380, 443);
            this.lblChanged.Name = "lblChanged";
            this.lblChanged.Size = new System.Drawing.Size(57, 13);
            this.lblChanged.TabIndex = 43;
            this.lblChanged.Text = "Changed";
            this.toolTip1.SetToolTip(this.lblChanged, "To make effective, push Save botton");
            this.lblChanged.Visible = false;
            // 
            // lblChgDirectory
            // 
            this.lblChgDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblChgDirectory.AutoSize = true;
            this.lblChgDirectory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChgDirectory.ForeColor = System.Drawing.Color.Red;
            this.lblChgDirectory.Location = new System.Drawing.Point(668, 64);
            this.lblChgDirectory.Name = "lblChgDirectory";
            this.lblChgDirectory.Size = new System.Drawing.Size(12, 13);
            this.lblChgDirectory.TabIndex = 44;
            this.lblChgDirectory.Text = "*";
            this.toolTip1.SetToolTip(this.lblChgDirectory, "To make effective, push Save botton");
            this.lblChgDirectory.Visible = false;
            // 
            // lblChgWorkDir
            // 
            this.lblChgWorkDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblChgWorkDir.AutoSize = true;
            this.lblChgWorkDir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChgWorkDir.ForeColor = System.Drawing.Color.Red;
            this.lblChgWorkDir.Location = new System.Drawing.Point(668, 95);
            this.lblChgWorkDir.Name = "lblChgWorkDir";
            this.lblChgWorkDir.Size = new System.Drawing.Size(12, 13);
            this.lblChgWorkDir.TabIndex = 45;
            this.lblChgWorkDir.Text = "*";
            this.toolTip1.SetToolTip(this.lblChgWorkDir, "To make effective, push Save botton");
            this.lblChgWorkDir.Visible = false;
            // 
            // lblChgImportDir
            // 
            this.lblChgImportDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblChgImportDir.AutoSize = true;
            this.lblChgImportDir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChgImportDir.ForeColor = System.Drawing.Color.Red;
            this.lblChgImportDir.Location = new System.Drawing.Point(668, 126);
            this.lblChgImportDir.Name = "lblChgImportDir";
            this.lblChgImportDir.Size = new System.Drawing.Size(12, 13);
            this.lblChgImportDir.TabIndex = 46;
            this.lblChgImportDir.Text = "*";
            this.toolTip1.SetToolTip(this.lblChgImportDir, "To make effective, push Save botton");
            this.lblChgImportDir.Visible = false;
            // 
            // lblChgRepositoryDir
            // 
            this.lblChgRepositoryDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblChgRepositoryDir.AutoSize = true;
            this.lblChgRepositoryDir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChgRepositoryDir.ForeColor = System.Drawing.Color.Red;
            this.lblChgRepositoryDir.Location = new System.Drawing.Point(668, 157);
            this.lblChgRepositoryDir.Name = "lblChgRepositoryDir";
            this.lblChgRepositoryDir.Size = new System.Drawing.Size(12, 13);
            this.lblChgRepositoryDir.TabIndex = 47;
            this.lblChgRepositoryDir.Text = "*";
            this.toolTip1.SetToolTip(this.lblChgRepositoryDir, "To make effective, push Save botton");
            this.lblChgRepositoryDir.Visible = false;
            // 
            // lblChgTUSubDirectoryFailed
            // 
            this.lblChgTUSubDirectoryFailed.AutoSize = true;
            this.lblChgTUSubDirectoryFailed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChgTUSubDirectoryFailed.ForeColor = System.Drawing.Color.Red;
            this.lblChgTUSubDirectoryFailed.Location = new System.Drawing.Point(360, 231);
            this.lblChgTUSubDirectoryFailed.Name = "lblChgTUSubDirectoryFailed";
            this.lblChgTUSubDirectoryFailed.Size = new System.Drawing.Size(12, 13);
            this.lblChgTUSubDirectoryFailed.TabIndex = 49;
            this.lblChgTUSubDirectoryFailed.Text = "*";
            this.toolTip1.SetToolTip(this.lblChgTUSubDirectoryFailed, "To make effective, push Save botton");
            this.lblChgTUSubDirectoryFailed.Visible = false;
            // 
            // lblChgSubdirBadfiles
            // 
            this.lblChgSubdirBadfiles.AutoSize = true;
            this.lblChgSubdirBadfiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChgSubdirBadfiles.ForeColor = System.Drawing.Color.Red;
            this.lblChgSubdirBadfiles.Location = new System.Drawing.Point(360, 262);
            this.lblChgSubdirBadfiles.Name = "lblChgSubdirBadfiles";
            this.lblChgSubdirBadfiles.Size = new System.Drawing.Size(12, 13);
            this.lblChgSubdirBadfiles.TabIndex = 50;
            this.lblChgSubdirBadfiles.Text = "*";
            this.toolTip1.SetToolTip(this.lblChgSubdirBadfiles, "To make effective, push Save botton");
            this.lblChgSubdirBadfiles.Visible = false;
            // 
            // lblChgLogVerbosity
            // 
            this.lblChgLogVerbosity.AutoSize = true;
            this.lblChgLogVerbosity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChgLogVerbosity.ForeColor = System.Drawing.Color.Red;
            this.lblChgLogVerbosity.Location = new System.Drawing.Point(210, 300);
            this.lblChgLogVerbosity.Name = "lblChgLogVerbosity";
            this.lblChgLogVerbosity.Size = new System.Drawing.Size(12, 13);
            this.lblChgLogVerbosity.TabIndex = 51;
            this.lblChgLogVerbosity.Text = "*";
            this.toolTip1.SetToolTip(this.lblChgLogVerbosity, "To make effective, push Save botton");
            this.lblChgLogVerbosity.Visible = false;
            // 
            // lblChgZipFile
            // 
            this.lblChgZipFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblChgZipFile.AutoSize = true;
            this.lblChgZipFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChgZipFile.ForeColor = System.Drawing.Color.Red;
            this.lblChgZipFile.Location = new System.Drawing.Point(668, 375);
            this.lblChgZipFile.Name = "lblChgZipFile";
            this.lblChgZipFile.Size = new System.Drawing.Size(12, 13);
            this.lblChgZipFile.TabIndex = 53;
            this.lblChgZipFile.Text = "*";
            this.toolTip1.SetToolTip(this.lblChgZipFile, "To make effective, push Save botton");
            this.lblChgZipFile.Visible = false;
            // 
            // lblChgConnectionString
            // 
            this.lblChgConnectionString.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblChgConnectionString.AutoSize = true;
            this.lblChgConnectionString.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChgConnectionString.ForeColor = System.Drawing.Color.Red;
            this.lblChgConnectionString.Location = new System.Drawing.Point(668, 406);
            this.lblChgConnectionString.Name = "lblChgConnectionString";
            this.lblChgConnectionString.Size = new System.Drawing.Size(12, 13);
            this.lblChgConnectionString.TabIndex = 54;
            this.lblChgConnectionString.Text = "*";
            this.toolTip1.SetToolTip(this.lblChgConnectionString, "To make effective, push Save botton");
            this.lblChgConnectionString.Visible = false;
            // 
            // lblChgEnabled
            // 
            this.lblChgEnabled.AutoSize = true;
            this.lblChgEnabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChgEnabled.ForeColor = System.Drawing.Color.Red;
            this.lblChgEnabled.Location = new System.Drawing.Point(360, 33);
            this.lblChgEnabled.Name = "lblChgEnabled";
            this.lblChgEnabled.Size = new System.Drawing.Size(12, 13);
            this.lblChgEnabled.TabIndex = 55;
            this.lblChgEnabled.Text = "*";
            this.toolTip1.SetToolTip(this.lblChgEnabled, "To make effective, push Save botton");
            this.lblChgEnabled.Visible = false;
            // 
            // lblChgUpdateConfig
            // 
            this.lblChgUpdateConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblChgUpdateConfig.AutoSize = true;
            this.lblChgUpdateConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChgUpdateConfig.ForeColor = System.Drawing.Color.Red;
            this.lblChgUpdateConfig.Location = new System.Drawing.Point(668, 33);
            this.lblChgUpdateConfig.Name = "lblChgUpdateConfig";
            this.lblChgUpdateConfig.Size = new System.Drawing.Size(12, 13);
            this.lblChgUpdateConfig.TabIndex = 57;
            this.lblChgUpdateConfig.Text = "*";
            this.toolTip1.SetToolTip(this.lblChgUpdateConfig, "To make effective, push Save botton");
            this.lblChgUpdateConfig.Visible = false;
            // 
            // lblChgMaxFileInBatch
            // 
            this.lblChgMaxFileInBatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblChgMaxFileInBatch.AutoSize = true;
            this.lblChgMaxFileInBatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChgMaxFileInBatch.ForeColor = System.Drawing.Color.Red;
            this.lblChgMaxFileInBatch.Location = new System.Drawing.Point(668, 200);
            this.lblChgMaxFileInBatch.Name = "lblChgMaxFileInBatch";
            this.lblChgMaxFileInBatch.Size = new System.Drawing.Size(12, 13);
            this.lblChgMaxFileInBatch.TabIndex = 60;
            this.lblChgMaxFileInBatch.Text = "*";
            this.toolTip1.SetToolTip(this.lblChgMaxFileInBatch, "To make effective, push Save botton");
            this.lblChgMaxFileInBatch.Visible = false;
            // 
            // lblChgTUType
            // 
            this.lblChgTUType.AutoSize = true;
            this.lblChgTUType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChgTUType.ForeColor = System.Drawing.Color.Red;
            this.lblChgTUType.Location = new System.Drawing.Point(260, 33);
            this.lblChgTUType.Name = "lblChgTUType";
            this.lblChgTUType.Size = new System.Drawing.Size(12, 13);
            this.lblChgTUType.TabIndex = 63;
            this.lblChgTUType.Text = "*";
            this.toolTip1.SetToolTip(this.lblChgTUType, "To make effective, push Save botton");
            this.lblChgTUType.Visible = false;
            // 
            // lblChgSubFnTimeout
            // 
            this.lblChgSubFnTimeout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblChgSubFnTimeout.AutoSize = true;
            this.lblChgSubFnTimeout.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChgSubFnTimeout.ForeColor = System.Drawing.Color.Red;
            this.lblChgSubFnTimeout.Location = new System.Drawing.Point(668, 231);
            this.lblChgSubFnTimeout.Name = "lblChgSubFnTimeout";
            this.lblChgSubFnTimeout.Size = new System.Drawing.Size(12, 13);
            this.lblChgSubFnTimeout.TabIndex = 66;
            this.lblChgSubFnTimeout.Text = "*";
            this.toolTip1.SetToolTip(this.lblChgSubFnTimeout, "To make effective, push Save botton");
            this.lblChgSubFnTimeout.Visible = false;
            // 
            // lblChgMaxLogFileLength
            // 
            this.lblChgMaxLogFileLength.AutoSize = true;
            this.lblChgMaxLogFileLength.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChgMaxLogFileLength.ForeColor = System.Drawing.Color.Red;
            this.lblChgMaxLogFileLength.Location = new System.Drawing.Point(455, 300);
            this.lblChgMaxLogFileLength.Name = "lblChgMaxLogFileLength";
            this.lblChgMaxLogFileLength.Size = new System.Drawing.Size(12, 13);
            this.lblChgMaxLogFileLength.TabIndex = 69;
            this.lblChgMaxLogFileLength.Text = "*";
            this.toolTip1.SetToolTip(this.lblChgMaxLogFileLength, "To make effective, push Save botton");
            this.lblChgMaxLogFileLength.Visible = false;
            // 
            // lblChgMaxLogHistoryFiles
            // 
            this.lblChgMaxLogHistoryFiles.AutoSize = true;
            this.lblChgMaxLogHistoryFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChgMaxLogHistoryFiles.ForeColor = System.Drawing.Color.Red;
            this.lblChgMaxLogHistoryFiles.Location = new System.Drawing.Point(668, 300);
            this.lblChgMaxLogHistoryFiles.Name = "lblChgMaxLogHistoryFiles";
            this.lblChgMaxLogHistoryFiles.Size = new System.Drawing.Size(12, 13);
            this.lblChgMaxLogHistoryFiles.TabIndex = 72;
            this.lblChgMaxLogHistoryFiles.Text = "*";
            this.toolTip1.SetToolTip(this.lblChgMaxLogHistoryFiles, "To make effective, push Save botton");
            this.lblChgMaxLogHistoryFiles.Visible = false;
            // 
            // lblUpdateConfig
            // 
            this.lblUpdateConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUpdateConfig.AutoSize = true;
            this.lblUpdateConfig.Location = new System.Drawing.Point(450, 33);
            this.lblUpdateConfig.Name = "lblUpdateConfig";
            this.lblUpdateConfig.Size = new System.Drawing.Size(215, 13);
            this.lblUpdateConfig.TabIndex = 56;
            this.lblUpdateConfig.Text = "Update app.config on Save option changed";
            this.lblUpdateConfig.Visible = false;
            // 
            // lblMaxFileInBatch
            // 
            this.lblMaxFileInBatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMaxFileInBatch.AutoSize = true;
            this.lblMaxFileInBatch.Location = new System.Drawing.Point(517, 200);
            this.lblMaxFileInBatch.Name = "lblMaxFileInBatch";
            this.lblMaxFileInBatch.Size = new System.Drawing.Size(89, 13);
            this.lblMaxFileInBatch.TabIndex = 59;
            this.lblMaxFileInBatch.Text = "Max File In Batch";
            // 
            // txtMaxFileInBatch
            // 
            this.txtMaxFileInBatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaxFileInBatch.Location = new System.Drawing.Point(612, 197);
            this.txtMaxFileInBatch.Name = "txtMaxFileInBatch";
            this.txtMaxFileInBatch.Size = new System.Drawing.Size(50, 20);
            this.txtMaxFileInBatch.TabIndex = 58;
            this.txtMaxFileInBatch.Text = "10";
            this.txtMaxFileInBatch.TextChanged += new System.EventHandler(this.txtMaxFileInBatch_TextChanged);
            // 
            // lblTUType
            // 
            this.lblTUType.AutoSize = true;
            this.lblTUType.Location = new System.Drawing.Point(102, 33);
            this.lblTUType.Name = "lblTUType";
            this.lblTUType.Size = new System.Drawing.Size(46, 13);
            this.lblTUType.TabIndex = 62;
            this.lblTUType.Text = "TUType";
            // 
            // txtTUType
            // 
            this.txtTUType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTUType.Location = new System.Drawing.Point(154, 30);
            this.txtTUType.Name = "txtTUType";
            this.txtTUType.Size = new System.Drawing.Size(100, 20);
            this.txtTUType.TabIndex = 61;
            this.txtTUType.Text = "IC901_CVS";
            this.txtTUType.TextChanged += new System.EventHandler(this.txtTUType_TextChanged);
            // 
            // lblSubFnTimeout
            // 
            this.lblSubFnTimeout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSubFnTimeout.AutoSize = true;
            this.lblSubFnTimeout.Location = new System.Drawing.Point(477, 231);
            this.lblSubFnTimeout.Name = "lblSubFnTimeout";
            this.lblSubFnTimeout.Size = new System.Drawing.Size(130, 13);
            this.lblSubFnTimeout.TabIndex = 65;
            this.lblSubFnTimeout.Text = "Sub function timeout (sec)";
            // 
            // txtSubFnTimeout
            // 
            this.txtSubFnTimeout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSubFnTimeout.Location = new System.Drawing.Point(612, 228);
            this.txtSubFnTimeout.Name = "txtSubFnTimeout";
            this.txtSubFnTimeout.Size = new System.Drawing.Size(50, 20);
            this.txtSubFnTimeout.TabIndex = 64;
            this.txtSubFnTimeout.Text = "240";
            this.txtSubFnTimeout.TextChanged += new System.EventHandler(this.txtSubFnTimeout_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(269, 300);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 13);
            this.label4.TabIndex = 68;
            this.label4.Text = "Max Log File Length";
            // 
            // txtMaxLogFileLength
            // 
            this.txtMaxLogFileLength.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaxLogFileLength.Location = new System.Drawing.Point(378, 297);
            this.txtMaxLogFileLength.Name = "txtMaxLogFileLength";
            this.txtMaxLogFileLength.Size = new System.Drawing.Size(71, 20);
            this.txtMaxLogFileLength.TabIndex = 67;
            this.txtMaxLogFileLength.Text = "1000000";
            this.txtMaxLogFileLength.TextChanged += new System.EventHandler(this.txtMaxLogFileLength_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(500, 300);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 13);
            this.label6.TabIndex = 71;
            this.label6.Text = "Max Log History Files";
            // 
            // txtMaxLogHistoryFiles
            // 
            this.txtMaxLogHistoryFiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaxLogHistoryFiles.Location = new System.Drawing.Point(612, 297);
            this.txtMaxLogHistoryFiles.Name = "txtMaxLogHistoryFiles";
            this.txtMaxLogHistoryFiles.Size = new System.Drawing.Size(50, 20);
            this.txtMaxLogHistoryFiles.TabIndex = 70;
            this.txtMaxLogHistoryFiles.Text = "3";
            this.txtMaxLogHistoryFiles.TextChanged += new System.EventHandler(this.txtMaxLogHistoryFiles_TextChanged);
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(692, 473);
            this.Controls.Add(this.lblChgMaxLogHistoryFiles);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtMaxLogHistoryFiles);
            this.Controls.Add(this.lblChgMaxLogFileLength);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtMaxLogFileLength);
            this.Controls.Add(this.lblChgSubFnTimeout);
            this.Controls.Add(this.lblSubFnTimeout);
            this.Controls.Add(this.txtSubFnTimeout);
            this.Controls.Add(this.lblChgTUType);
            this.Controls.Add(this.lblTUType);
            this.Controls.Add(this.txtTUType);
            this.Controls.Add(this.lblChgMaxFileInBatch);
            this.Controls.Add(this.lblMaxFileInBatch);
            this.Controls.Add(this.txtMaxFileInBatch);
            this.Controls.Add(this.lblChgUpdateConfig);
            this.Controls.Add(this.lblUpdateConfig);
            this.Controls.Add(this.lblChgEnabled);
            this.Controls.Add(this.lblChgConnectionString);
            this.Controls.Add(this.lblChgZipFile);
            this.Controls.Add(this.lblChgLogFileName);
            this.Controls.Add(this.lblChgLogVerbosity);
            this.Controls.Add(this.lblChgSubdirBadfiles);
            this.Controls.Add(this.lblChgTUSubDirectoryFailed);
            this.Controls.Add(this.lblChgTUSubDirectoryOK);
            this.Controls.Add(this.lblChgRepositoryDir);
            this.Controls.Add(this.lblChgImportDir);
            this.Controls.Add(this.lblChgWorkDir);
            this.Controls.Add(this.lblChgDirectory);
            this.Controls.Add(this.lblChanged);
            this.Controls.Add(this.btnRestore);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtConnectionString);
            this.Controls.Add(this.btnZipFile);
            this.Controls.Add(this.txtZipFile);
            this.Controls.Add(this.btnLogFileName);
            this.Controls.Add(this.txtLogFileName);
            this.Controls.Add(this.txtLogVerbosity);
            this.Controls.Add(this.btnSubdirBadfiles);
            this.Controls.Add(this.txtSubdirBadfiles);
            this.Controls.Add(this.btnTUSubDirectoryFailed);
            this.Controls.Add(this.txtTUSubDirectoryFailed);
            this.Controls.Add(this.btnTUSubDirectoryOK);
            this.Controls.Add(this.txtTUSubDirectoryOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.chkEnabled);
            this.Controls.Add(this.btnRepositoryDir);
            this.Controls.Add(this.txtRepositoryDir);
            this.Controls.Add(this.btnImportDir);
            this.Controls.Add(this.txtImportDir);
            this.Controls.Add(this.btnWorkDir);
            this.Controls.Add(this.txtWorkDir);
            this.Controls.Add(this.btnDirectory);
            this.Controls.Add(this.txtDirectory);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ConfigurationForm";
            this.Text = "Application configuration: ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConfigurationForm_FormClosing);
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRepositoryDir;
        private System.Windows.Forms.TextBox txtRepositoryDir;
        private System.Windows.Forms.Button btnImportDir;
        private System.Windows.Forms.TextBox txtImportDir;
        private System.Windows.Forms.Button btnWorkDir;
        private System.Windows.Forms.TextBox txtWorkDir;
        private System.Windows.Forms.Button btnDirectory;
        private System.Windows.Forms.TextBox txtDirectory;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.CheckBox chkEnabled;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtLogVerbosity;
        private System.Windows.Forms.Button btnSubdirBadfiles;
        private System.Windows.Forms.TextBox txtSubdirBadfiles;
        private System.Windows.Forms.Button btnTUSubDirectoryFailed;
        private System.Windows.Forms.TextBox txtTUSubDirectoryFailed;
        private System.Windows.Forms.Button btnTUSubDirectoryOK;
        private System.Windows.Forms.TextBox txtTUSubDirectoryOK;
        private System.Windows.Forms.TextBox txtConnectionString;
        private System.Windows.Forms.Button btnZipFile;
        private System.Windows.Forms.TextBox txtZipFile;
        private System.Windows.Forms.Button btnLogFileName;
        private System.Windows.Forms.TextBox txtLogFileName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnRestore;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem otherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuRun;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mnuReset;
        private System.Windows.Forms.ToolStripMenuItem mnuSaveIntoConfig;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label lblChanged;
        private System.Windows.Forms.Label lblChgDirectory;
        private System.Windows.Forms.Label lblChgWorkDir;
        private System.Windows.Forms.Label lblChgImportDir;
        private System.Windows.Forms.Label lblChgRepositoryDir;
        private System.Windows.Forms.Label lblChgTUSubDirectoryFailed;
        private System.Windows.Forms.Label lblChgSubdirBadfiles;
        private System.Windows.Forms.Label lblChgLogVerbosity;
        private System.Windows.Forms.Label lblChgZipFile;
        private System.Windows.Forms.Label lblChgConnectionString;
        private System.Windows.Forms.Label lblChgEnabled;
        private System.Windows.Forms.Label lblChgTUSubDirectoryOK;
        private System.Windows.Forms.Label lblChgLogFileName;
        private System.Windows.Forms.ToolStripMenuItem optionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuUpdateConfig;
        private System.Windows.Forms.Label lblUpdateConfig;
        private System.Windows.Forms.Label lblChgUpdateConfig;
        private System.Windows.Forms.Label lblChgMaxFileInBatch;
        private System.Windows.Forms.Label lblMaxFileInBatch;
        private System.Windows.Forms.TextBox txtMaxFileInBatch;
        private System.Windows.Forms.Label lblChgTUType;
        private System.Windows.Forms.Label lblTUType;
        private System.Windows.Forms.TextBox txtTUType;
        private System.Windows.Forms.Label lblChgSubFnTimeout;
        private System.Windows.Forms.Label lblSubFnTimeout;
        private System.Windows.Forms.TextBox txtSubFnTimeout;
        private System.Windows.Forms.Label lblChgMaxLogFileLength;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMaxLogFileLength;
        private System.Windows.Forms.Label lblChgMaxLogHistoryFiles;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMaxLogHistoryFiles;
    }
}

