using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.IO;
using System.Xml;
using System.Security;

namespace DDSMultiImport
{
    partial class DDSMultiImport : ServiceBase
    {
        #region Private members
        private System.Threading.Timer baseTimer; //Timer for basic periods
        private bool bWorkerRunning; //Worker thread is already running
        private string currentApplicationName;
        #endregion

        #region Construction
        public DDSMultiImport()
        {
            InitializeComponent();
            logger.FileName = Properties.Settings.Default.LogFileName;
            logger.FileVerbosity = Properties.Settings.Default.LogVerbosity;
            logger.LogToFile = true;
            logger.LogToListBox = false;
            logger.MaxFileLength = 1000000;
            logger.MaxHistoryFiles = 3;
            logger.MaxListBoxLines = 1000;
            bWorkerRunning = false;
            currentApplicationName = "";
        }
        #endregion

        #region Event handlers
        /// <summary>
        /// Handles start of service.
        /// Import timer is started
        /// </summary>
        protected override void OnStart(string[] args)
        {
            logger.LogText(0, "", "");
            logger.LogText(0, "", "----------------------------------------------");
            logger.LogText(0, "DDSMultiImp", "DDS Multi-Import service started");
            //Create period timer
            //First create delegate for callback method
            TimerCallback timerDelegate = new TimerCallback(baseTimer_Elapsed);
            //Create and start timer
            baseTimer = new System.Threading.Timer(
              timerDelegate,
              null,
              Properties.Settings.Default.StartupDelaySec * 1000,
              Properties.Settings.Default.BasePerSec * 1000);
            logger.LogText(0, "DDSMultiImp", "Created timer with period {0} sec, startup delay {1} sec",
              Properties.Settings.Default.BasePerSec, Properties.Settings.Default.StartupDelaySec);
        }

        protected override void OnStop()
        {
            baseTimer.Dispose();
            logger.LogText(0, "DDSMultiImp", "DDS Multi-Import service stopped");
        }

        /// <summary>
        /// Raised after timer is elapsed
        /// </summary>
        /// <param name="state"></param>
        void baseTimer_Elapsed(object state)
        {
            //Check if worker thread is not already running
            if (bWorkerRunning)
            {
                logger.LogText(0, "DDSMultiImp", "DDS Import is overloading");
                return;
            }

            //logger.LogText(0, "DDSMultiImp", "Begin");
            //Set worker thread is running
            bWorkerRunning = true;

            // run each application
            try
            {
                string dir = Properties.Settings.Default.ApplicationDir;
                if (dir.StartsWith("."))
                    dir = System.Windows.Forms.Application.ExecutablePath + dir;
                DirectoryInfo di = new DirectoryInfo(Properties.Settings.Default.ApplicationDir);
                foreach(FileInfo fi in di.GetFiles("*.exe"))
                {
                    RunApplication(fi);
                }
            }
            catch (Exception ex)
            {
                //Transformation failed - log the error together with inner errors
                logger.LogText(0, "DDSMultiImp", "DDS Transformation failed: {0}", ex.Message);
                if (ex.InnerException != null)
                {
                    logger.LogText(1, "DDSMultiImp", ex.InnerException.Message);
                    if (ex.InnerException.InnerException != null)
                        logger.LogText(1, "DDSMultiImp", ex.InnerException.InnerException.Message);
                }
            }
            //Worker thread is not running
            bWorkerRunning = false;
        }
        #endregion

        #region Helper methods
        /// <summary>
        /// Run the Application
        /// </summary>
        /// <param name="Application"></param>
        private void RunApplication(FileInfo Application)
        {
            try
            {
                currentApplicationName = Application.FullName;
                logger.LogText(0, "DDSMultiImp", "Start Application: {0}", Path.GetFileNameWithoutExtension(currentApplicationName));
                //Create ProcessStartInfo
                ProcessStartInfo applicationStartInfo = new ProcessStartInfo(currentApplicationName, "-run");
                applicationStartInfo.CreateNoWindow = true;
                applicationStartInfo.UseShellExecute = false;
                applicationStartInfo.RedirectStandardOutput = true;

                //Create Application process
                Process applicationProc = new Process();
                applicationProc.StartInfo = applicationStartInfo;

                //Add handler for output data of the process
                if (Properties.Settings.Default.LogApplicationOutput)
                    applicationProc.OutputDataReceived += new DataReceivedEventHandler(ApplicationOutputDataReceived);

                //Start Application process
                applicationProc.Start();

                //Start asynchronous reading of output data
                if (Properties.Settings.Default.LogApplicationOutput)
                    applicationProc.BeginOutputReadLine();

                //Wait for process to exit with specified timeout
                if (!applicationProc.WaitForExit(Properties.Settings.Default.ApplicationTimeoutSec * 1000))
                { //Timeout expired, kill the process
                    logger.LogText(0, "DDSMultiImp", "Application {0} did not finish in {1} sec. Killing the process."
                        , Path.GetFileNameWithoutExtension(currentApplicationName)
                        , Properties.Settings.Default.ApplicationTimeoutSec);
                    applicationProc.Kill();
                    if (!applicationProc.WaitForExit(Properties.Settings.Default.ApplicationTimeoutSec * 1000))
                        throw new Exception("Application did not finish in " + Properties.Settings.Default.ApplicationTimeoutSec + " sec. and the process is not Kill-able.");
                    else
                        throw new Exception("Application did not finish in " + Properties.Settings.Default.ApplicationTimeoutSec + " sec. Killing the process.");
                }
                applicationProc.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to run Application " + currentApplicationName, ex);
            }
            finally
            {
                currentApplicationName = "";
            }
        }

        /// <summary>
        /// Handler called automatically when Application process writes data to its standard output
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Data written to the output</param>
        void ApplicationOutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            //Write data to log file when line is not empty
            if ((e.Data != null) && (e.Data.Trim() != String.Empty))
                logger.LogText(6, Path.GetFileNameWithoutExtension(currentApplicationName), e.Data);
        }
        #endregion
    }
}
