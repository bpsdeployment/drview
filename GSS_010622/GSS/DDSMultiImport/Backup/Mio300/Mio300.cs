using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;
using System.IO;
using System.Xml;
using RMTools;

namespace IC901_CVS
{
    /// <summary>
    /// 
    /// </summary>
    public class Mio300
    {
        //byte[] md5;
        private string _ConnectionString;
        private string _TUType;
        private FileInfo _StaticTraceValuesFile;
        private XmlDocument StaticTraceValues;

        protected DB_Tool tool;

        private Mio300_TraceHeader Header;
        private ArrayList Records;
        //private ArrayList recordsTime;
        //private byte[] BinaryData;

        public Mio300()
        {
            _ConnectionString = "Data Source=localhost\\sqlserver;;Initial Catalog=CDDB_MIO300;Integrated Security=True";
            _TUType = "IC901_CVS";
            tool = new DB_Tool(_ConnectionString, _TUType);
            _StaticTraceValuesFile = null;
            StaticTraceValues = null;
        }

        public Mio300(string ConnectionString, string TUType)
        {
            _ConnectionString = ConnectionString;
            _TUType = TUType;

            tool = new DB_Tool(_ConnectionString, _TUType);
            _StaticTraceValuesFile = null;
            StaticTraceValues = null;
        }

        public FileInfo StaticTraceValuesFile
        {
            get { return _StaticTraceValuesFile; }
            set
            {
                _StaticTraceValuesFile = value;
                StaticTraceValues = new XmlDocument();
                StaticTraceValues.Load(_StaticTraceValuesFile.FullName);
            }
        }

        public void Clear()
        {
            Records = new ArrayList();
        }
        /*
        public bool ConvertDirectory(string dirpath)
        {
            DirectoryInfo DirInfo = new DirectoryInfo(dirpath);
            DirInfo.Refresh();
            foreach (FileInfo file in DirInfo.GetFiles("*", SearchOption.TopDirectoryOnly))
            {
                string filename = file.FullName;
                //ConvertFile(filename);
            }
            return true;
        }*/

        public bool FileConvert(string TUInstanceID, string fullname, XmlWriter dstWriter)
        {
            bool fileOK = true;
            try
            {
                XmlNode StaticVal = StaticTraceValues.SelectSingleNode("/TUINSTANCE[@ID='" + TUInstanceID + "']");
                string SVStartTime = "";
                string SVBinaryData = "";
                string SVAlarmTime = "";
                string SVAlarmBinaryData = "";
                if (StaticVal != null)
                {
                    SVStartTime = StaticVal.Attributes["StartTime"].Value;
                    SVBinaryData = StaticVal.Attributes["BinaryData"].Value;
                    SVAlarmTime = StaticVal.Attributes["AlarmTime"].Value;
                    SVAlarmBinaryData = StaticVal.Attributes["AlarmBinaryData"].Value;
                }

                FileInfo fileinfo = new FileInfo(fullname);
                if (fileinfo.Exists)
                {
                    string filename = Path.GetFileName(fullname);
                    if (filename.StartsWith("CVS"))
                    {
                        fileOK &= LoadFile(fullname, TUInstanceID);
                        string fileOut = Path.Combine(Path.GetFullPath(fullname), Path.GetFileNameWithoutExtension(fullname) + ".xml");
                        SaveAs(fileOut);
                        int id = tool.GetTraceRecordDefID("", "MIO300_TRACE");
                        int idStaticsChange = tool.GetRecordDefID("", "1000");
                        int idAlarmChange = tool.GetRecordDefID("", "1001");
                        if (id >= 0)
                        {
                            string lastBin = "";
                            //foreach (Mio300_Record rec in Records)
                            foreach (Mio300_Record rec in Records)
                            {
                                // Main Binary Data
                                try
                                {
                                    string newBin = rec.GetMainBinaryData();
                                    if (newBin != lastBin)
                                        rec.WriteToXML(dstWriter, id, Mio300_TraceHeader.RecordType.TRACE);
                                    else
                                    {
                                        // Skip write becouse it is the same
                                    }
                                    lastBin = newBin;
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("Error when generates Main Binary Data XML record", ex);
                                }

                                // Static Binary Data
                                try
                                {
                                    string newStatic = rec.GetStaticBinaryData();
                                    if (SVBinaryData != newStatic)
                                    {
                                        SVBinaryData = newStatic;
                                        StaticVal.Attributes["StartTime"].Value = rec.StartTime.ToString();
                                        StaticVal.Attributes["BinaryData"].Value = newStatic;
                                        StaticTraceValues.Save(_StaticTraceValuesFile.FullName);
                                        rec.WriteToXML(dstWriter, idStaticsChange, Mio300_TraceHeader.RecordType.STATIC);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("Error when generates Static Binary Data XML record", ex);
                                }

                                // Alarm Binary Data
                                try
                                {
                                    string newAlarm = rec.GetAlarmBinaryData();
                                    if (SVAlarmBinaryData != newAlarm)
                                    {
                                        SVAlarmBinaryData = newAlarm;
                                        StaticVal.Attributes["AlarmTime"].Value = rec.StartTime.ToString();
                                        StaticVal.Attributes["AlarmBinaryData"].Value = newAlarm;
                                        StaticTraceValues.Save(_StaticTraceValuesFile.FullName);
                                        rec.WriteToXML(dstWriter, idAlarmChange, Mio300_TraceHeader.RecordType.ALARM);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("Error when generates Alarm Binary Data XML record", ex);
                                }
                            }
                        }
                    }

                    else if (filename.StartsWith("events"))
                    {
                        StreamReader events = new StreamReader(fullname);
                        try
                        {
                            while (!events.EndOfStream)
                            {
                                string eventsln = events.ReadLine();

                                if (eventsln.Length != FRAM.FRAM_SIZE * 2)
                                    fileOK = false;
                                else
                                {
                                    byte[] buff = new byte[FRAM.FRAM_SIZE];
                                    for (int n = 0; n < FRAM.FRAM_SIZE; n++)
                                    {
                                        byte val = 0;
                                        if (!byte.TryParse(eventsln.Substring(n * 2, 2), System.Globalization.NumberStyles.HexNumber, null, out val))
                                        {
                                            fileOK = false;
                                            break;
                                        }
                                        buff[n] = val;
                                    }

                                    if (fileOK)
                                    {
                                        FRAM fram = new FRAM(tool, TUInstanceID);
                                        fram.Upload(buff);
                                        fram.WriteToXML(dstWriter);
                                        fram.WriteToFile(Path.Combine(Path.GetDirectoryName(fullname), "FAR" + filename));
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error when parser event record", ex);
                        }
                        finally
                        {
                            events.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error from FileConvert method", ex);
            }
            return fileOK;
        }


        public bool LoadFile(string filename, string TUInstanceID)
        {
            bool badRecordInside = false;

            Clear();

            StreamReader cvs = new StreamReader(filename);
            if (!cvs.EndOfStream)
            {
                Header = new Mio300_TraceHeader();
                string strHeader = cvs.ReadLine();
                Header.Parse(strHeader);

                UInt32 recNr = 0;
                while (!cvs.EndOfStream)
                {
                    Mio300_Record trace = new Mio300_Record(tool, TUInstanceID, Header);
                    string cvsln = cvs.ReadLine();
                    if (trace.ParseRecord(recNr, cvsln)) Records.Add(trace);
                    else
                        badRecordInside = true;
                    recNr++;
                }
                cvs.Close();
            }
            return (!badRecordInside);
        }


        /// <summary>
        /// Save decoded data into a xml file as given filename
        /// </summary>
        /// <param name="filename">Full file name</param>
        /// <returns>true if no error</returns>
        public bool SaveAs(string filename)
        {
            bool rtn = true;
            /*try
            {
                //Get output XmlWriter
                XmlWriter dstWriter = null;

                XmlDocument doc = GetXML();
                doc.Save(filename);
            }
            catch (Exception ex)
            {
                rtn = false;
            }
            finally
            {
            }*/
            return rtn;
        }

        /// <summary>
        /// Format all decoded records into xml document
        /// </summary>
        public void GetXML(XmlWriter dstWriter)
        {
            /*            //Iterate over all XML files in Convert direcory
                        convDirInfo.Refresh();
                        foreach (FileInfo srcXmlFile in convDirInfo.GetFiles("*.xml", SearchOption.TopDirectoryOnly))
                        {
                            if (dstWriter == null)
                                dstWriter = CreateNewDRFile(); //Create new writer if necessary
                            tuType.GetProcessorParams(procNum, out procEvtName, out procSrcName);
                            dstWriter.WriteStartElement("ImportedDR");
                            dstWriter.WriteElementString("TUInstanceID", tuInstanceID.ToString());
                            dstWriter.WriteElementString("DDDVersionID", dddVer.DDDVersionID.ToString());
                        }

                        XmlDocument xmldoc = new XmlDocument();
                        //XmlDeclaration dec = xmldoc.CreateXmlDeclaration("1.0", null, "no");
                        XmlDeclaration dec = xmldoc.CreateXmlDeclaration("1.0", System.Text.Encoding.UTF8.HeaderName, "no");
                        xmldoc.AppendChild(dec);

                        Guid TUInstanceID;
                        Guid DDDVersionID;
                        RecordInstID;

                        recDefID = dddVer.GetRecordDefID(procEvtName, Convert.ToInt32(ddsElem.GetAttribute("DIST_NR")));
                        dstWriter.WriteElementString("RecordDefID", recDefID.ToString());
                        startTime = Convert.ToDateTime(ddsElem.GetAttribute("StartTime")).ToUniversalTime();
                        dstWriter.WriteElementString("RecordInstID", startTime.Ticks.ToString());
                        dstWriter.WriteElementString("StartTime", startTime.ToString("yyyy-MM-ddTHH:mm:ss.fff"));
                        if (ddsElem.GetAttribute("EndTime") != "")
                        {
                            endTime = Convert.ToDateTime(ddsElem.GetAttribute("EndTime")).ToUniversalTime();
                            dstWriter.WriteElementString("EndTime", endTime.ToString("yyyy-MM-ddTHH:mm:ss.fff"));
                        }
                        lastModifTime = Convert.ToDateTime(ddsElem.GetAttribute("LastModified")).ToUniversalTime();
                        dstWriter.WriteElementString("LastModified", lastModifTime.ToString("yyyy-MM-ddTHH:mm:ss.fff"));
                        dstWriter.WriteElementString("CompleteData", "true");
                        dstWriter.WriteElementString("BigEndianData", "false");
             */
            /*TEST*/
            /*
    //dstWriter.WriteElementString("Source", procSrcName);
    dstWriter.WriteElementString("BinaryData", ddsElem.InnerText);

    /*TEST*/
            /*
    dstWriter.WriteElementString("VehicleNumber", rndGen.Next(1, 6).ToString());
    /*TEST*/
            /*
    dstWriter.WriteElementString("DeviceCode", procNum.ToString());
    /*TEST*/
            /*
    dstWriter.WriteElementString("AcknowledgeTime", startTime.ToString("yyyy-MM-ddTHH:mm:ss.fff"));
    */
            //XmlNode root = xmldoc.CreateElement("DDSExport");
            /*
            XmlAttribute a1 = xmldoc.CreateAttribute("BigEndianData");
            XmlAttribute a2 = xmldoc.CreateAttribute("ConfigID");
            XmlAttribute a3 = xmldoc.CreateAttribute("VehicleID");
            a1.Value = "false";
            a2.Value = Version;
            a3.Value = Vehicle;*/
            /*            root.Attributes.Append(a1);
                        root.Attributes.Append(a2);
                        root.Attributes.Append(a3);
                        root.Attributes.Append(a4);
                        root.Attributes.Append(a5);
                        root.Attributes.Append(a6);
                        root.Attributes.Append(a7);
                        root.Attributes.Append(a8);
                        root.Attributes.Append(a9);
                        root.Attributes.Append(a10);
                        root.Attributes.Append(a11);
                        root.Attributes.Append(a12);
                        root.Attributes.Append(a13);

                        foreach (Mio300_Record rec in Records)
                        {
                            if (rec.Status == Mio300_Record.RecordResult.OK)
                            {
                                XmlNode n = rec.ToXmlNode(xmldoc);
                                root.AppendChild(n);
                            }
                        }
                        xmldoc.AppendChild(root);
                        string t = xmldoc.OuterXml;
                        return xmldoc;
            */
        }

    }



    /// <summary>
    /// Mio300_Record class
    /// </summary>
    public partial class Mio300_TraceHeader
    {
        /// <summary>
        /// Field number in single occorrence
        /// </summary>
        public static int FieldCount = 14;
        public enum Field { NULL, TIME, LAT, LON, REL, DI, DO, FREQ, AI, AIg, AIo, ALRM, ADC, PKD };
        public enum DataType { NULL, TIME, COORD, INT16, BYTE, WORD, DWORD };
        [Flags]
        public enum RecordType { SKIP = 0x00, TRACE = 0x01, STATIC = 0x02, ALARM = 0x04 };

        public static Char[] charSeparator = { ';' };

        protected string strHeader;
        private string[] header;
        private Field[] headerField;
        private DataType[] headerItemType;
        private RecordType[] headerRecordType;
        private int[] headerItemCount;
        //private int idxALRM;
        //Dictionary<string, int> ItemCount;
        /// <summary>
        /// Total number of fields
        /// </summary>
        private int _fieldCount;

        public Mio300_TraceHeader()
        {
            //ItemCount = new Dictionary<string, int>();
            strHeader = "";
            _fieldCount = 0;
        }

        public string GetString()
        {
            return strHeader;
        }

        /// <summary>
        /// Total number of fields into header
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            return _fieldCount;
        }

        public Field GetItemField(int pos)
        {
            return headerField[pos];
        }

        public DataType GetItemType(int pos)
        {
            return headerItemType[pos];
        }

        public RecordType GetRecordType(int pos)
        {
            return headerRecordType[pos];
        }
        public int GetItemCount(int i)
        {
            return headerItemCount[i];
        }
        /*public int AlarmIndex
        {
            get { return idxALRM; }
        }*/

        public bool Parse(string Header)
        {
            strHeader = Header;
            header = strHeader.Split(charSeparator, StringSplitOptions.RemoveEmptyEntries);
            _fieldCount = header.Length;
            headerField = new Field[_fieldCount];
            headerItemType = new DataType[_fieldCount];
            headerRecordType = new RecordType[_fieldCount];
            headerItemCount = new int[FieldCount];
            //idxALRM = -1;
            for (int i = 0; i < header.Length; i++)
            {
                string s = header[i];
                if (s == "TimeUTC")
                { headerField[i] = Field.TIME; headerItemType[i] = DataType.TIME; headerRecordType[i] = RecordType.SKIP; }
                else if (s == "Lat")
                { headerField[i] = Field.LAT; headerItemType[i] = DataType.COORD; headerRecordType[i] = RecordType.TRACE; }
                else if (s == "Lon")
                { headerField[i] = Field.LON; headerItemType[i] = DataType.COORD; headerRecordType[i] = RecordType.TRACE; }
                else if (s == "REL")
                { headerField[i] = Field.REL; headerItemType[i] = DataType.INT16; headerRecordType[i] = RecordType.STATIC; }
                else if (s.Substring(0, 2) == "DI")
                { headerField[i] = Field.DI; headerItemType[i] = DataType.BYTE; headerRecordType[i] = RecordType.TRACE; }
                else if (s.Substring(0, 2) == "DO")
                { headerField[i] = Field.DO; headerItemType[i] = DataType.BYTE; headerRecordType[i] = RecordType.TRACE; }
                else if (s.Length == 4 && s.Substring(0, 4) == "FREQ")
                { headerField[i] = Field.FREQ; headerItemType[i] = DataType.WORD; headerRecordType[i] = RecordType.TRACE; }
                else if (s.Length > 4 && s.Substring(0, 4) == "AI_g")
                { headerField[i] = Field.AIg; headerItemType[i] = DataType.WORD; headerRecordType[i] = RecordType.STATIC; }
                else if (s.Length > 4 && s.Substring(0, 4) == "AI_o")
                { headerField[i] = Field.AIo; headerItemType[i] = DataType.WORD; headerRecordType[i] = RecordType.STATIC; }
                else if (s.Substring(0, 2) == "AI")
                { headerField[i] = Field.AI; headerItemType[i] = DataType.DWORD; headerRecordType[i] = RecordType.TRACE; }
                else if (s.Length > 4 && s.Substring(0, 4) == "ALRM")
                { headerField[i] = Field.ALRM; headerItemType[i] = DataType.BYTE; headerRecordType[i] = RecordType.TRACE | RecordType.ALARM; /*idxALRM = (int)headerField[i];*/ }
                else if (s.Substring(0, 3) == "ADC")
                { headerField[i] = Field.ADC; headerItemType[i] = DataType.WORD; headerRecordType[i] = RecordType.TRACE; }
                else if (s.Substring(0, 3) == "PKD")
                { headerField[i] = Field.PKD; headerItemType[i] = DataType.WORD; headerRecordType[i] = RecordType.TRACE; }
                else
                { headerField[i] = Field.NULL; headerItemType[i] = DataType.NULL; headerRecordType[i] = RecordType.SKIP; }
                headerItemCount[(int)headerField[i]]++;
            }
            return headerItemCount[(int)Field.NULL] > 0;
        }

        public int GetTypeLength(DataType t)
        {
            int len = 0;
            switch (t)
            {
                case DataType.TIME:
                    len = 8;
                    break;
                case DataType.COORD:
                    len = 4;
                    break;
                case DataType.DWORD:
                    len = 4;
                    break;
                case DataType.WORD:
                case DataType.INT16:
                    len = 2;
                    break;
                case DataType.BYTE:
                    len = 1;
                    break;
            }
            return len;
        }

        public int GetDataLength()
        {
            int len = 0;
            for (int i = 0; i < headerItemType.Length; i++)
                len += GetTypeLength(headerItemType[i]);
            return len;
        }

        public int GetMainDataLength()
        {
            int len = 0;
            for (int i = 0; i < headerItemType.Length; i++)
            {
                Field field = headerField[i];
                if ((headerRecordType[i] & RecordType.TRACE) > 0)
                    len += GetTypeLength(headerItemType[i]);
            }
            return len;
        }

        public int GetStaticDataLength()
        {
            int len = 0;
            for (int i = 0; i < headerItemType.Length; i++)
            {
                Field field = headerField[i];
                if ((headerRecordType[i] & RecordType.STATIC) > 0)
                    len += GetTypeLength(headerItemType[i]);
            }
            return len;
        }

        public int GetAlarmDataLength()
        {
            int len = 0;
            for (int i = 0; i < headerItemType.Length; i++)
            {
                Field field = headerField[i];
                if ((headerRecordType[i] & RecordType.ALARM) > 0)
                    len += GetTypeLength(headerItemType[i]);
            }
            return len;
        }
    }



    /// <summary>
    /// Mio300_Record class
    /// </summary>
    public partial class Mio300_Record
    {
        public enum RecordResult { EMPTY = 0, OK, BAD_STARTTIME, ENVDATA_TOO_SHORT, BAD_ENVDATA };
        public static string[] RecordResultText = { "EMPTY", "OK", "BAD_STARTTIME", "ENVDATA_TOO_SHORT" };

        //public Char[] charSeparator;
        public static Char[] charSeparator = { ';' };
        private string _DateTimeFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'";

        private RecordResult _Status;
        //private bool _AdditionalInfo;

        public DateTime StartTime;
        public DateTime EndTime;

        private byte[] MainBinaryData;
        private byte[] StaticBinaryData;
        private byte[] AlarmBinaryData;

        private DB_Tool tool;
        private string tuInstanceID;
        private Mio300_TraceHeader Header;

        private UInt32 RecNumber = 0;
        private int RecNrPos = -1;

        public Mio300_Record(DB_Tool Tool, string TUInstanceID, Mio300_TraceHeader TraceHeader)
        {
            // parameters
            tool = Tool;
            tuInstanceID = TUInstanceID;
            Header = TraceHeader;

            // defaults
            _Status = RecordResult.EMPTY;
            _DateTimeFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'";
            //_AdditionalInfo = true;
            //recordsTime = new ArrayList();
        }

        public RecordResult Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
            }
        }
        public string DateTimeFormat
        {
            get
            {
                return _DateTimeFormat;
            }
            set
            {
                _DateTimeFormat = value;
            }
        }

        public string GetMainBinaryData()
        {
            return (Convert.ToBase64String(MainBinaryData));
        }
        public string GetStaticBinaryData()
        {
            return (Convert.ToBase64String(StaticBinaryData));
        }
        public string GetAlarmBinaryData()
        {
            return (Convert.ToBase64String(AlarmBinaryData));
        }

        public bool ParseRecord(UInt32 recNr, string line)
        {
            RecNumber = recNr;
            Status = RecordResult.EMPTY;
            bool rtnOK = true;

            string[] strFields = line.Split(charSeparator, StringSplitOptions.RemoveEmptyEntries);
            if (strFields.Length != Header.Count())
                return false;

            int mainBinLen = Header.GetMainDataLength();
            int staticBinLen = Header.GetStaticDataLength();
            int alarmBinLen = Header.GetAlarmDataLength();
            // Clear space for time value
            //mainBinLen -= Header.GetTypeLength(Mio300_TraceHeader.DataType.TIME);
            // Add space for frequency
            mainBinLen += Header.GetTypeLength(Mio300_TraceHeader.DataType.DWORD);
            // Add space for RecordCounter
            mainBinLen += Header.GetTypeLength(Mio300_TraceHeader.DataType.WORD);
            MainBinaryData = new byte[mainBinLen];
            StaticBinaryData = new byte[staticBinLen];
            AlarmBinaryData = new byte[alarmBinLen];

            int MainBinaryDataPos = 0;
            int StaticBinaryDataPos = 0;
            int AlarmBinaryDataPos = 0;
            ArrayList record = new ArrayList();
            //recordsTime = new ArrayList();

            DateTime dt = new DateTime();
            StartTime = DateTime.MinValue;
            float coord;
            Int16 version;
            byte b = 0;
            UInt16 w = 0;
            UInt32 dw = 0;

            try
            {
                int k = 0;
                for (int i = 0; i < strFields.Length; i++)
                {
                    //for (int j = 0; j < headerItemCount[(int)headerItemType[i]]; j++)
                    {
                        string data = strFields[i].Replace("0x", "").Trim();
                        //switch (headerItemType[i])
                        switch (Header.GetItemType(i))
                        {
                            case Mio300_TraceHeader.DataType.TIME:
                                if (DateTime.TryParse(data, out dt))
                                {
                                    dt = dt.ToUniversalTime();
                                    StartTime = dt;
                                    //recordsTime.Add(dt);
                                    record.Add(dt);
                                    //long binDate = dt.ToBinary();
                                    Int64 binDate = (Int64)dt.ToBinary();
                                    byte[] dataser = BitConverter.GetBytes(binDate);
                                    //Array.Copy(dataser, 0, BinaryData, BinaryDataPos, 8);
                                    //BinaryDataPos += 8;
                                }
                                break;
                            case Mio300_TraceHeader.DataType.COORD:
                                if (float.TryParse(data, out coord))
                                {
                                    byte[] dataser = BitConverter.GetBytes(coord);
                                    Array.Copy(dataser, 0, MainBinaryData, MainBinaryDataPos, 4);
                                }
                                MainBinaryDataPos += 4;
                                record.Add(coord);
                                break;
                            case Mio300_TraceHeader.DataType.INT16:
                                version = 0;
                                byte[] dataInt16 = BitConverter.GetBytes(version);
                                if (Int16.TryParse(data, out version))
                                {
                                    dataInt16 = BitConverter.GetBytes(version);
                                }
                                if ((Header.GetRecordType(i) & Mio300_TraceHeader.RecordType.TRACE) > 0)
                                {
                                    Array.Copy(dataInt16, 0, MainBinaryData, MainBinaryDataPos, 2);
                                    MainBinaryDataPos += 2;
                                }
                                if ((Header.GetRecordType(i) & Mio300_TraceHeader.RecordType.STATIC) > 0)
                                {
                                    Array.Copy(dataInt16, 0, StaticBinaryData, StaticBinaryDataPos, 2);
                                    StaticBinaryDataPos += 2;
                                }
                                record.Add(version);
                                break;
                            case Mio300_TraceHeader.DataType.BYTE:
                                if (!byte.TryParse(data, System.Globalization.NumberStyles.HexNumber, null, out b))
                                    b = 0;
                                byte[] dataByte = BitConverter.GetBytes(b);
                                Array.Copy(dataByte, 0, MainBinaryData, MainBinaryDataPos, 1);
                                MainBinaryDataPos += 1;
                                if ((Header.GetRecordType(i) & Mio300_TraceHeader.RecordType.ALARM) > 0)
                                {
                                    Array.Copy(dataByte, 0, AlarmBinaryData, AlarmBinaryDataPos, 1);
                                    AlarmBinaryDataPos += 1;
                                }
                                record.Add(b);
                                break;
                            case Mio300_TraceHeader.DataType.WORD:
                                w = 0;
                                byte[] dataserW = BitConverter.GetBytes(w);
                                if (UInt16.TryParse(data, System.Globalization.NumberStyles.HexNumber, null, out w))
                                {
                                    dataserW = BitConverter.GetBytes(w);
                                }
                                if ((Header.GetRecordType(i) & Mio300_TraceHeader.RecordType.TRACE) > 0)
                                {
                                    Array.Copy(dataserW, 0, MainBinaryData, MainBinaryDataPos, 2);
                                    MainBinaryDataPos += 2;
                                }
                                if ((Header.GetRecordType(i) & Mio300_TraceHeader.RecordType.STATIC) > 0)
                                {
                                    Array.Copy(dataserW, 0, StaticBinaryData, StaticBinaryDataPos, 2);
                                    StaticBinaryDataPos += 2;
                                }
                                if (Header.GetItemField(i) == Mio300_TraceHeader.Field.FREQ)
                                {
                                    float freq = 0.0F;
                                    if (w != 0)
                                    {
                                        freq = 345600.0F / w;
                                        freq = (float)Math.Round(freq, 2);
                                    }
                                    byte[] array = BitConverter.GetBytes(freq);
                                    Array.Copy(array, 0, MainBinaryData, MainBinaryDataPos, 4);
                                    MainBinaryDataPos += 4;
                                }
                                record.Add(w);
                                break;
                            case Mio300_TraceHeader.DataType.DWORD:
                                dw = 0;
                                byte[] dwordDataser = BitConverter.GetBytes(dw);
                                if (UInt32.TryParse(data, System.Globalization.NumberStyles.HexNumber, null, out dw))
                                {
                                    dwordDataser = BitConverter.GetBytes(dw);
                                }
                                Array.Copy(dwordDataser, 0, MainBinaryData, MainBinaryDataPos, 4);
                                MainBinaryDataPos += 4;
                                record.Add(dw);
                                break;
                            case Mio300_TraceHeader.DataType.NULL:
                                break;
                            default:
                                break;
                        }
                        k++;
                    }
                }
                /*
                // Test //
                string rtn = Revert();
                line = line.Replace(" ", "").ToUpper();
                rtn = rtn.Replace(" ", "").ToUpper();
                int r = line.CompareTo(rtn);
                if (r != 0)
                {
                }*/

                // Add empty RecNr to Environmentals
                byte[] dataRecNr = BitConverter.GetBytes(0);
                RecNrPos = MainBinaryDataPos;
                Array.Copy(dataRecNr, 0, MainBinaryData, MainBinaryDataPos, 2);
                MainBinaryDataPos += 2;

                // Is that Right lentgh now?
                if (MainBinaryDataPos == mainBinLen)
                {
                    Status = RecordResult.OK;
                }
                else
                {
                    Status = RecordResult.BAD_ENVDATA;
                    rtnOK = false;
                }
            }
            catch (Exception)
            {
                Status = RecordResult.BAD_ENVDATA;
                rtnOK = false;
            }

            return rtnOK;
        }

        public string Revert()
        {
            string rtn = "";
            if (MainBinaryData.Length != Header.GetDataLength())
                return "LENGTH ERROR";

            int BinaryDataPos = 0;

            int k = 0;
            for (int i = 0; i < Header.Count(); i++)
            {
                {
                    switch (Header.GetItemType(i))
                    {
                        case Mio300_TraceHeader.DataType.TIME:
                            Int64 binDate = BitConverter.ToInt64(MainBinaryData, BinaryDataPos);
                            DateTime dt = DateTime.FromBinary(binDate);
                            //rtn += dt.ToLocalTime().ToString("yyyy'-'MM'-'dd' 'HH':'mm':'ss'.'fff") + charSeparator[0] + " ";
                            //BinaryDataPos += 8;
                            break;
                        case Mio300_TraceHeader.DataType.COORD:
                            //byte[] dataser = new byte[4];
                            //Array.Copy(BinaryData, BinaryDataPos, dataser, 0, 4);
                            float f = BitConverter.ToSingle(MainBinaryData, BinaryDataPos);
                            rtn += f.ToString("0'.'000000") + charSeparator[0] + " ";
                            BinaryDataPos += 4;
                            break;
                        case Mio300_TraceHeader.DataType.INT16:
                            Int16 i16 = BitConverter.ToInt16(MainBinaryData, BinaryDataPos);
                            rtn += i16.ToString() + charSeparator[0] + " ";
                            BinaryDataPos += 2;
                            break;
                        case Mio300_TraceHeader.DataType.BYTE:
                            byte n8 = MainBinaryData[BinaryDataPos];
                            rtn += "0x" + n8.ToString("X2") + charSeparator[0] + " ";
                            BinaryDataPos += 1;
                            break;
                        case Mio300_TraceHeader.DataType.WORD:
                            UInt16 n16 = BitConverter.ToUInt16(MainBinaryData, BinaryDataPos);
                            rtn += "0x" + n16.ToString("X4") + charSeparator[0] + " ";
                            BinaryDataPos += 2;
                            if (Header.GetItemField(i) == Mio300_TraceHeader.Field.FREQ)
                            {
                                UInt32 u32 = BitConverter.ToUInt32(MainBinaryData, BinaryDataPos);
                                rtn += "0x" + u32.ToString("X8") + charSeparator[0] + " ";
                                BinaryDataPos += 4;
                            }
                            break;
                        case Mio300_TraceHeader.DataType.DWORD:
                            UInt32 n32 = BitConverter.ToUInt32(MainBinaryData, BinaryDataPos);
                            rtn += "0x" + n32.ToString("X8") + charSeparator[0] + " ";
                            BinaryDataPos += 4;
                            break;
                        case Mio300_TraceHeader.DataType.NULL:
                            break;
                        default:
                            break;
                    }
                    k++;
                }
            }
            //recordsTime.Add(dt);
            //Records.Add(BinaryData);
            if (rtn.EndsWith(charSeparator[0] + " "))
                rtn = rtn.Substring(0, rtn.Length - 2);
            return rtn;
        }

        public void WriteToXML(XmlWriter dstWriter, int RecordDefID, Mio300_TraceHeader.RecordType recordType)
        {
            if (dstWriter == null)
                return;

            // Add RecNr to Environmentals
            byte[] dataRecNr = BitConverter.GetBytes(RecNumber);
            Array.Copy(dataRecNr, 0, MainBinaryData, RecNrPos, 2); // copy only 2 lower byte
            
            //if (rec.Status == Mio300_Record.RecordResult.OK && tool != null)
            {
                try
                {
                    dstWriter.WriteStartElement("ImportedDR");

                    dstWriter.WriteElementString("TUInstanceID", tuInstanceID);
                    dstWriter.WriteElementString("RecordInstID", StartTime.Ticks.ToString());

                    Guid verID = tool.GetDDDVersionID(""); // Last DDD version
                    dstWriter.WriteElementString("DDDVersionID", verID.ToString());

                    dstWriter.WriteElementString("RecordDefID", RecordDefID.ToString());

                    //rec.WriteToXML(dstWriter);

                    //DateTime StartTime = (DateTime)recordsTime[i];
                    dstWriter.WriteElementString("StartTime", StartTime.ToString(_DateTimeFormat));

                    dstWriter.WriteElementString("LastModified", StartTime.ToString(_DateTimeFormat));
                    dstWriter.WriteElementString("CompleteData", "true");
                    dstWriter.WriteElementString("BigEndianData", "false");

                    dstWriter.WriteElementString("Source", "CVS");

                    //string rec = Convert.ToBase64String((byte[])Records[i]);
                    //string rec = Convert.ToBase64String(BinaryData);
                    if (recordType == Mio300_TraceHeader.RecordType.TRACE)
                        dstWriter.WriteElementString("BinaryData", Convert.ToBase64String(MainBinaryData));
                    else if (recordType == Mio300_TraceHeader.RecordType.STATIC)
                        dstWriter.WriteElementString("BinaryData", Convert.ToBase64String(StaticBinaryData));
                    else if (recordType == Mio300_TraceHeader.RecordType.ALARM)
                        dstWriter.WriteElementString("BinaryData", Convert.ToBase64String(AlarmBinaryData));

                    //dstWriter.WriteElementString("VehicleNumber", rndGen.Next(1, 6).ToString());
                    //dstWriter.WriteElementString("DeviceCode", "CVS");
                    //dstWriter.WriteElementString("AcknowledgeTime", StartTime.ToString(_DateTimeFormat));
                    dstWriter.WriteEndElement();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error during WriteToXML method with params: RecordDefID=" + RecordDefID.ToString() + " fromMainBinData=" + recordType.ToString(), ex);
                }
            }
        }
        /*
        public void LoadEnvData(byte[] buffer)
        {
            EnvData = new byte[buffer.Length];
            int i = 0;
            foreach (byte c in buffer)
                EnvData[i++] = c;
        }*/

        /*
        public string ToBase64String()
        {
            return Convert.ToBase64String(BinaryData);
        }
        */
    }


    /// <summary>
    /// Fram_Record class
    /// </summary>
    public class Fram_Record
    {
        public const int MAXCVP = 21;
        public const int NBYTEINP = 7;
        public const int NBYTEOUT = 3;

        public static string[] strDISt = { "OFF", "ON" };
        public static string[] strDIDesc =  
        {
            "OVER VOLTAGE"     ,    "PHASE FAILURE R"  ,    "PHASE FAILURE S", 
            "PHASE FAILURE T"  ,    "PH LOSS"          ,    "FAIL 3F"        , 
            "OVER I 3F"        ,    "OVER LOAD 3F"     ,    "OVER TEMP"      ,
            "IN RSV1"          ,    "IP1 VBATT PROT2"  ,    "IP2 VBATT PROT2", 
            "IP3 VBATT PROT2"  ,    "NU1"              ,    "NU2"            ,
            "NU3"              ,    "IP1 TEMPO CB REL.",    "IP1 CBFAIL"     , 
            "IP1 INTERV TERMIC",    "IP1 CONV FAIL"    ,    "IP1 FUNZIONE"   , 
            "IP1 PRESENZA AT"  ,    "NU4"              ,    "NU5"            ,
            "IGBT2 FAIL AT"    ,    "IGBT1 FAIL AT"    ,    "IN RSV2"        , 
            "CHOP FAIL"        ,    "IP1 VBATT PROT1"  ,    "IP2 VBATT PROT1", 
            "IP1 OUT RSV3"     ,    "IP2 TEMPO CB REL.",    "IGBT1 FAIL CB"  ,
            "I LIM AT"         ,    "OVER LOAD AT"     ,    "V OVER AT"      , 
            "ALIM AT OK"       ,    "PROT TEMP AT"     ,    "VBUS OVER AT"   ,
            "SBIL AT"          ,    "IP1 COMANDO REL." ,    "I LIM CB"       , 
            "OVER LOAD CB"     ,    "VBUS OVER CB"     ,    "ALIM CB OK"     , 
            "T PROT CB"        ,    "VMAX CB"          ,    "IGBT2 FAIL CB"  ,
            "START DIAG"       ,    "IN ONOFF"         ,    "NU6"            , 
            "STATO0"           ,    "STATO1"           ,    "STATO2"         , 
            "STATO3"           ,    "STATO4"             
        };

        public static string[] strDODesc =  
        {
            "INHIBIT VAT"      ,    "ONOFF AT"         ,    "ONOFF CB"       , 
            "CONTAORE"         ,    "RELAIS"           ,    "nc1"            , 
            "nc1"              ,    "nc1"              ,    "CB REL"         ,
            "OUT RSV3"         ,    "VBATT PROT1"      ,    "K2"             , 
            "K3"               ,    "K1"               ,    "K0"             ,
            "ONOFF 3F"         ,    "VBATT PROT2"      ,    "LED VBATT"      , 
            "LED RSV"          ,    "PRESENZA AT"      ,    "FUNZIONE"       , 
            "CONV FAIL"        ,    "INTERV TERMIC"    ,    "CB FAIL"      
        };

        public static string[] strAIDesc =
        {
            "V AT [V]"         ,    "V BUS 3F [V]"     ,    "V BUS AT [V]"   ,
            "I INP AT [A]"     ,    "I OUT AT [A]"     ,    "I R 3F [A]"     ,
            "I S 3F [A]"       ,    "I T 3F [A]"       ,    "V R 3F [V]"     ,
            "V S 3F [V]"       ,    "V T 3F [V]"       ,    "ITOT CB [A]"    ,
            "IBAT CB [A]"      ,    "VBAT CB [V]"      ,    "VDD [V]"        ,
            "VSS [V]"          ,    "NTC 3F [C]"       ,    "NTC1 AT [C]"    ,
            "NTC2 AT [C]"      ,    "NTC CB [C]"       ,    "PTC CB [C]"     
        };

        public static int[] iAIDiv = 
        {
            1000 ,    10000,    10000,    10000,    10000,    10000,    10000,
            10000,    10000,    10000,    10000,    10000,    10000,    10000,
            10000,    1000,     100,      100,      100,      100,      100,  
        };

        public long[] lConvV = new long[MAXCVP];    // Analog value
        public byte[] bInpP = new byte[NBYTEINP];   // Digital input
        public byte[] bOutP = new byte[NBYTEOUT];   // Digital tDout.aucOutPut
        public ushort unIC0_Period = 0;			    // Period
    }

    public class Fram_Event
    {
        // Stringhe allarmi
        public static string[] strAlm =   {   
            "Alimentazione",                            "Corto circuito PRESENZA AT",
            "Corto circuito CONV FAIL",                 "Corto circuito COMANDO RELAIS",
            "Corto circuito CB FAIL",                   "Corto circuito FUNZIONE",
            "Corto circuito INTERV TERMIC",             "Corto circuito TEMPO CB",
            "Corto circuito RSV3",                      "Corto circuito PROT2",
            "Corto circuito PROT1",                     "Stato K1",
            "Allarme fase",                             "Protezione ventola media",
            "Sovratensione VBUS CB",                    "SOVRACCARICO IN USCITA CARICABATTERIA",			
            "SOVRACCARICO SUL PRIMARIO CARICABATTERIA", "SOVRATENSIONE USCITA CARICABATTERIA",	            
            "Protezione termica CB",				    "Alimentazione logica CB",                          
            "Guasto IGBT1 CB",			                "Guasto IGBT2 CB",                                  
            "Vin fuori range AT",                       "SOVRACCARICO BUS INTERMEDIO",                      
            "LIMITE CORRENTE PRIMARIA",                 "SOVRATENSIONE BUS INTERMEDIO",                     
            "SOVRATEMPERATURA CIRCUITI INVERTER AT",    "Alimentazione logica AT",						    
            "GUASTO IGBT T1 INVERTER AT",			    "GUASTO IGBT T2 INVERTER AT",						
            "SBILANCIAMENTO INVERTER AT",               "GUASTO IGBT STEP-UP",	                            
            "Sovratensione 3F",                         "Guasto IGBT fase R 3F",							
            "Guasto IGBT fase S 3F",                    "Guasto IGBT fase T 3F",                            
            "Perdita fase 3F",		                    "PRESENZA COMPONENTE CONTINUA",						
            "Sovracarico 3F",   					    "SOVRACARICO INVERTER TRIFASE",
            "SOVRATEMPERATURA MT/BT",				    "Allarme connettori esterni",                       
            "Allarme presa officina",                   "Allarme STATO4",
            "Protezione ventola alta",                  "PT100 in corto circuito",
            "PT100 non collegata",                      "NTC 3F rotta",
            "NTC AT rotta",                             "Protezione termica 3F",
            "Protezione termica AT",                    "Guasto contattori presa esterna",
            "Batteria in corto circuito",               "Intervento fusibile",
            "Guasto Cond. 3F",                          "????Nuovo",
            "????"
            };

        public const int NUM_REC_EVENT = 10;
        public ulong ultimet_ev = 0;
        public byte ucEvent = 0;
        public byte ucLastRec = 0;
        public Fram_Record[] tRecords = new Fram_Record[NUM_REC_EVENT];
        //public byte[] records = new byte[1 + 3 + NUM_REC_EVENT * (Fram_Record.MAXCVP * 4 + Fram_Record.NBYTEINP + Fram_Record.NBYTEOUT + 2 + 4)];
        public byte[] records = new byte[1 + NUM_REC_EVENT * (Fram_Record.MAXCVP * 4 + Fram_Record.NBYTEINP + Fram_Record.NBYTEOUT + 2 + 4)];
    }


    public class FRAM
    {
        private string _DateTimeFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'";

        public const int FRAM_SIZE = 32768;
        //public const int NUM_B_KEY = 3;
        public const int NUM_EVENTS = 33;

        // Header
        //public byte[] bKey = new byte[NUM_B_KEY];  // Chiave inizializzazione
        public ushort unNumEv = 0;                   // Numero eventi scritti
        public ushort unEvIndex = 0;                 // Indice evento in scrittura (Next)
        public ushort unCks = 0;    			       // Checksum header
        // Eventi 
        public Fram_Event[] tEvents;// = new Fram_Event[NUM_EVENTS];
        private bool Init = false;

        DB_Tool DDD_Tool;
        string tuInstanceID;

        public bool IsInit
        {
            get
            {
                return Init;
            }
        }
        public string DateTimeFormat
        {
            get
            {
                return _DateTimeFormat;
            }
            set
            {
                _DateTimeFormat = value;
            }
        }

        public FRAM(DB_Tool Tool, string TUInstanceID)
        {
            DDD_Tool = Tool;
            tuInstanceID = TUInstanceID;
        }

        public bool WriteToXML(XmlWriter dstWriter)
        {
            if (!IsInit)
                return false;
            if (dstWriter == null)
                return false;

            int idev; //, idrec;
            Fram_Event tEv;
            //Fram_Record tRec;

            //if (unEvIndex == 0) idev = unNumEv - 1;
            //else idev = unEvIndex - 1;
            if (unEvIndex < unNumEv) idev = unEvIndex;
            else idev = 0;

            try
            {
                for (int ev = 0; ev < unNumEv; ++ev)
                //for (int ev = unNumEv - 1; ev >= 0; --ev)
                {
                    tEv = tEvents[idev];
                    DateTime startTimeUTC = Utility.UTC_time_tToLocDateTime((uint)tEv.ultimet_ev);
                    startTimeUTC = startTimeUTC.ToUniversalTime();
                    
                    dstWriter.WriteStartElement("ImportedDR");

                    dstWriter.WriteElementString("TUInstanceID", tuInstanceID);
                    dstWriter.WriteElementString("RecordInstID", startTimeUTC.Ticks.ToString());

                    if (DDD_Tool != null)
                    {
                        Guid verID = DDD_Tool.GetDDDVersionID("");
                        dstWriter.WriteElementString("DDDVersionID", verID.ToString());
                    }

                    string faultCode = ((int)tEv.ucEvent).ToString();
                    if (DDD_Tool != null)
                    {
                        int id = DDD_Tool.GetRecordDefID("", faultCode);
                        if (id >= 0)
                            dstWriter.WriteElementString("RecordDefID", id.ToString());
                        else
                        {
                            // Fault Code not found, collect to FC "999"
                            id = DDD_Tool.GetRecordDefID("", "999");
                            if (id >= 0)
                                dstWriter.WriteElementString("RecordDefID", id.ToString());
                        }
                    }
                    //dstWriter.WriteElementString("FaultCode", faultCode);

                    dstWriter.WriteElementString("StartTime", startTimeUTC.ToString(_DateTimeFormat));

                    dstWriter.WriteElementString("LastModified", startTimeUTC.ToString(_DateTimeFormat));
                    dstWriter.WriteElementString("CompleteData", "true");
                    dstWriter.WriteElementString("BigEndianData", "false");

                    dstWriter.WriteElementString("Source", "CVS");

                    string rec = Convert.ToBase64String(tEv.records);
                    dstWriter.WriteElementString("BinaryData", rec);

                    //dstWriter.WriteElementString("VehicleNumber", rndGen.Next(1, 6).ToString());
                    //dstWriter.WriteElementString("DeviceCode", "CVS");
                    dstWriter.WriteElementString("AcknowledgeTime", startTimeUTC.ToString(_DateTimeFormat));

                    //if (idev > 0) --idev;
                    //else idev = unNumEv - 1;
                    if (idev < unNumEv - 1) ++idev;
                    else idev = 0;

                    dstWriter.WriteEndElement();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        public bool Upload(byte[] bBuff)
        {
            int i, j, k;
            Init = false;
            // Header
            i = 0;
            if ((bBuff[i++] != 'F') || (bBuff[i++] != 'A') || (bBuff[i++] != 'R'))
                return false;
            unNumEv = (ushort)((bBuff[i] << 8) | bBuff[++i]);
            unEvIndex = (ushort)((bBuff[++i] << 8) | bBuff[++i]);
            unCks = (ushort)((bBuff[++i] << 8) | bBuff[++i]);
            if ((bBuff[0] + bBuff[1] + bBuff[2] + unNumEv + unEvIndex) != (~unCks & 0xFFFF))
                return false;
            //Header ok: events
            tEvents = new Fram_Event[unNumEv];
            for (j = 0; j < unNumEv; ++j)
            {
                int n = 0;
                tEvents[j] = new Fram_Event();
                tEvents[j].ultimet_ev = (ulong)((bBuff[++i] << 24) | (bBuff[++i] << 16) | (bBuff[++i] << 8) | bBuff[++i]);

                tEvents[j].records[n++] = bBuff[i + 1];
                //tEvents[j].records[n++] = 0;            // empty to structure alignment
                //tEvents[j].records[n++] = 0;            // empty to structure alignment
                //tEvents[j].records[n++] = 0;            // empty to structure alignment

                tEvents[j].ucEvent = bBuff[++i];
                tEvents[j].ucLastRec = bBuff[++i];
                //int tot = Fram_Record.MAXCVP * 4 + Fram_Record.NBYTEINP + Fram_Record.NBYTEOUT;
                for (k = 0; k < Fram_Event.NUM_REC_EVENT; ++k)
                {
                    int index;

                    tEvents[j].tRecords[k] = new Fram_Record();
                    // Analogic
                    for (index = 0; index < Fram_Record.MAXCVP; ++index)
                    {
                        tEvents[j].records[n++] = bBuff[i + 4];
                        tEvents[j].records[n++] = bBuff[i + 3];
                        tEvents[j].records[n++] = bBuff[i + 2];
                        tEvents[j].records[n++] = bBuff[i + 1];
                        tEvents[j].tRecords[k].lConvV[index] = (bBuff[++i] << 24) | (bBuff[++i] << 16) | (bBuff[++i] << 8) | bBuff[++i];
                    }
                    // Digital Input
                    for (index = 0; index < Fram_Record.NBYTEINP; ++index)
                    {
                        tEvents[j].records[n++] = bBuff[i + 1];
                        tEvents[j].tRecords[k].bInpP[index] = bBuff[++i];
                    }
                    // Digital Output
                    for (index = 0; index < Fram_Record.NBYTEOUT; ++index)
                    {
                        tEvents[j].records[n++] = bBuff[i + 1];
                        tEvents[j].tRecords[k].bOutP[index] = bBuff[++i];
                    }
                    // Freq
                    tEvents[j].records[n++] = bBuff[i + 1];
                    tEvents[j].records[n++] = bBuff[i + 2];
                    ushort period = (ushort)((bBuff[++i] << 8) | bBuff[++i]);
                    tEvents[j].tRecords[k].unIC0_Period = period;
                    float freq = 0.0F;
                    if (period != 0)
                    {
                        freq = 345600.0F / tEvents[j].tRecords[k].unIC0_Period;
                        freq = (float)Math.Round(freq, 2);
                    }
                    byte[] array = BitConverter.GetBytes(freq);
                    //System.Data.SqlTypes.SqlSingle f = freq;
                    //byte[] array2; // = BitConverter.GetBytes(f);
                    //System.Data.SqlTypes.SqlBinary sss = new System.Data.SqlTypes.SqlBinary(array2);
                    tEvents[j].records[n++] = array[0];
                    tEvents[j].records[n++] = array[1];
                    tEvents[j].records[n++] = array[2];
                    tEvents[j].records[n++] = array[3];
                }
            }
            Init = true;
            return true;
        }

        public void WriteToFile(string strpath)
        {
            int idev, idrec;
            Fram_Event tEv;
            Fram_Record tRec;

            if (!IsInit)
                return;
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(strpath);
                if (unEvIndex == 0) idev = unNumEv - 1;
                else idev = unEvIndex - 1;

                // Add some text to the file.
                sw.WriteLine("*************************************************************");
                sw.WriteLine("********************** MIO300 LOG FILE **********************");
                sw.WriteLine("*************************************************************" + Environment.NewLine);
                sw.WriteLine("Upload date: {0}", DateTime.Now);
                sw.WriteLine("{0} events recorded!!!", unNumEv);
                sw.WriteLine(Environment.NewLine + "************************* REMARKS ***************************");
                sw.WriteLine("Event list is orders from the more recent event to the oldest");
                sw.WriteLine("The same for the I/O record");
                sw.WriteLine(Environment.NewLine + "*************************************************************");

                for (int ev = 0; ev < unNumEv; ++ev)
                {
                    tEv = tEvents[idev];
                    sw.WriteLine(Environment.NewLine + "************************** EVENT {0} **************************" + Environment.NewLine, ev);
                    //sw.WriteLine("Event type: {0}", Properties.Settings.Default.StrAlarm[tEv.ucEvent]/*Fram_Event.strAlm[tEv.ucEvent]*/);
                    if (tEv.ucEvent < Fram_Event.strAlm.Length - 1)
                        sw.WriteLine("Event type: {0}", Fram_Event.strAlm[tEv.ucEvent]);
                    else
                        sw.WriteLine("Event type: {0}", Fram_Event.strAlm[Fram_Event.strAlm.Length - 1]);
                    sw.WriteLine("Time: {0}", Utility.UTC_time_tToLocDateTime((uint)tEv.ultimet_ev));
                    idrec = tEv.ucLastRec;

                    for (int rec = 0; rec < Fram_Event.NUM_REC_EVENT; ++rec)
                    {
                        int i, j, iBit;
                        tRec = tEv.tRecords[idrec];

                        sw.WriteLine(Environment.NewLine + Environment.NewLine + "----->RECORD {0}", rec);

                        sw.WriteLine(Environment.NewLine + "Digital input:");
                        for (i = j = 0; i < Fram_Record.NBYTEINP; ++i)
                        {
                            sw.WriteLine();
                            for (iBit = 0; iBit < 8; ++iBit, ++j)
                                sw.Write("{0,-18} = {1,-5}", Fram_Record.strDIDesc[j], Fram_Record.strDISt[(tRec.bInpP[i] >> iBit) & 0x01]);
                        }

                        sw.WriteLine(Environment.NewLine + Environment.NewLine + "Digital output:");
                        for (i = j = 0; i < Fram_Record.NBYTEOUT; ++i)
                        {
                            sw.WriteLine();
                            for (iBit = 0; iBit < 8; ++iBit, ++j)
                                sw.Write("{0,-18} = {1,-5}", Fram_Record.strDODesc[j], Fram_Record.strDISt[(tRec.bOutP[i] >> iBit) & 0x01]);
                        }

                        sw.WriteLine(Environment.NewLine + Environment.NewLine + "Analog input:");
                        for (i = 0; i < Fram_Record.MAXCVP; )
                        {
                            sw.WriteLine();
                            for (iBit = 0; (iBit < 4) & (i < Fram_Record.MAXCVP); ++iBit, ++i)
                                sw.Write("{0,-18} = {1,-8:F1}", Fram_Record.strAIDesc[i], (float)tRec.lConvV[i] / Fram_Record.iAIDiv[i]);
                        }

                        int freq = 0;
                        if (tRec.unIC0_Period != 0)
                            freq = 345600 / tRec.unIC0_Period;
                        else
                            sw.WriteLine(Environment.NewLine + "Out frequency [Hz] = {0}", freq);

                        if (idrec > 0) idrec--;
                        else idrec = Fram_Event.NUM_REC_EVENT - 1;
                    }

                    if (idev > 0) --idev;
                    else idev = unNumEv - 1;
                }

                sw.WriteLine(Environment.NewLine + "*************************** FINE ****************************" + Environment.NewLine);
                sw.Flush();
            }
            catch (UnauthorizedAccessException) { }
            catch (ArgumentNullException) { }
            catch (ArgumentException) { }
            catch (DirectoryNotFoundException) { }
            catch (PathTooLongException) { }
            catch (IOException) { }
            catch (System.Security.SecurityException) { }
            finally
            {
                sw.Close();
            }
        }


        class Utility
        {
            public Utility()
            {
            }

            /// <summary>
            /// Convert time_t to DateTime
            /// </summary>
            public static DateTime UTC_time_tToLocDateTime(uint time_t)
            {
                System.Globalization.CultureInfo en = new System.Globalization.CultureInfo("");

                DateTime dt = new DateTime(1970, 1, 1, en.Calendar).AddSeconds(time_t);
                return dt.ToLocalTime();
            }

            public static uint LocDateTimeTo_UTC_time_t(DateTime dtLocal)
            {
                System.Globalization.CultureInfo en = new System.Globalization.CultureInfo("");

                DateTime reftime_t = new DateTime(1970, 1, 1, en.Calendar);
                DateTime endtime_t = dtLocal.ToUniversalTime();

                if (endtime_t >= reftime_t)
                {
                    TimeSpan span = endtime_t - reftime_t;
                    return (uint)span.TotalSeconds;
                }
                else
                    return 0;
            }

            public static DateTime UTC_time_tToLocDateTime(string str_time_t)
            {
                uint time_t = 0;
                time_t = Convert.ToUInt32(str_time_t);
                return UTC_time_tToLocDateTime(time_t);
            }
        }

    }
}
