using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using System.Data;
//using TDManagementForms;

namespace RMTools
{
    public class DB_Tool
    {
        public string connString;
        public TUTypes TUType;
        //private Logger logger; //logger object

        public DB_Tool(string ConnString, string tutype)
        {
            connString = ConnString;
            Guid TUTypeID = new Guid();
            TUType = new TUTypes(ConnString, tutype, TUTypeID);
        }

        public int GetRecordDefID(string DDDVersion, string FaultCode)
        {
            return TUType.GetRecordDefID(DDDVersion, FaultCode);
        }

        public int GetTraceRecordDefID(string DDDVersion, string FaultCode)
        {
            return TUType.GetTraceRecordDefID(DDDVersion, FaultCode);
        }

        public Guid GetDDDVersionID(string DDDVersion)
        {
            return TUType.GetDDDVersionID(DDDVersion);
        }

        public void GetLastBinaryData(Guid TUTypeID, Guid TUInstanceID, string DDDVersionName, string FaultCode, ref byte[] binData)
        {
            TUType.GetLastBinaryData(TUTypeID, TUInstanceID, DDDVersionName, FaultCode, ref binData);
        }

        public string GetLastBinaryData(Guid TUTypeID, Guid TUInstanceID, string DDDVersionName, string FaultCode)
        {
            return (TUType.GetLastBinaryData(TUTypeID, TUInstanceID, DDDVersionName, FaultCode));
        }
    }



    public class TUTypes
    {
        private string connString;
        public String TUTypeName; //Name of diagnostic application
        public Guid TUTypeID; //ID of associated TUType
        //private String InstNamePrefix; //Prefix of instance name used to determine TUType of the instance
        //private BombConfig configObject; //parent configuration object

        private Dictionary<String, Guid> TUInstances; //Dictionary of TUInstances for diagnostic application
        private Dictionary<String, DDDVersion> dddVersions; //Dictionary of DDD versions
        private string lastVersion;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">Name of TUType or "" if none</param>
        /// <param name="ID">Guid of TUType or Guid(0) if none</param>
        public TUTypes(string ConnString, string Name, Guid ID)
        {
            connString = ConnString;
            TUTypeName = Name;
            TUTypeID = ID;
            lastVersion = "";
            SqlConnection dbConn = null;
            //SqlCommand dbCmd = null;
            try
            {
                //Create connection to database
                dbConn = new SqlConnection(connString);
                dbConn.Open();
                ReadTUTypes(dbConn);
                ReadTUInstances(dbConn);
                ReadVersions(dbConn);
                ReadLastVersion(dbConn);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to load TU Type " + TUTypeName, ex);
            }
            finally
            {
                //Dispose command
                //if (dbCmd != null)
                //    dbCmd.Dispose();
                //Dispose database connection
                if (dbConn != null)
                {
                    dbConn.Close();
                    dbConn.Dispose();
                }
            }
        }

        public int GetRecordDefID(string DDDVersionName, string FaultCode)
        {
            if (DDDVersionName == "")
                DDDVersionName = lastVersion;
            DDDVersion dv;
            if (dddVersions.TryGetValue(DDDVersionName, out dv))
            {
                return dv.GetRecordDefID(FaultCode);
            }
            else
                return -1;
        }

        public int GetTraceRecordDefID(string DDDVersionName, string RecordName)
        {
            if (DDDVersionName == "")
                DDDVersionName = lastVersion;
            DDDVersion dv;
            if (dddVersions.TryGetValue(DDDVersionName, out dv))
            {
                return dv.GetTraceRecordDefID(RecordName);
            }
            else
                return -1;
        }

        public Guid GetDDDVersionID(string DDDVersionName)
        {
            if (DDDVersionName == "")
                DDDVersionName = lastVersion;
            DDDVersion dv;
            if (dddVersions.TryGetValue(DDDVersionName, out dv))
            {
                return dv.DDDVersionID;
            }
            else
                return Guid.Empty;
        }

        public void ReadTUTypes(SqlConnection dbConn)
        {
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            try
            {
                string where = (TUTypeID == Guid.Empty ? "TypeName='" + TUTypeName : "TUTypeID='" + TUTypeID.ToString()) + "'";
                //logger.LogText(1, "BombCfg", "Reading configuration for type {0} from database", Name);
                //Execute stored procedure in DB to obtain configuration
                cmd = new SqlCommand("select TypeName,TUTypeID from TUTypes TUT "
                    + "where " + where, dbConn);
                cmd.CommandType = CommandType.Text; //.StoredProcedure;
                //cmd.Parameters.Add("@TUTypeName", SqlDbType.NVarChar, 50).Value = Name;
                reader = cmd.ExecuteReader();

                reader.Read();

                TUTypeName = reader.GetString(0);
                TUTypeID = reader.GetGuid(1);
            }
            catch (Exception)
            {
                //logger.LogText(1, "BombCfg", "Failed to read configuration of type {0}: {1}", Name, ex.Message);
                throw;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public void ReadTUInstances(SqlConnection dbConn)
        {
            TUInstances = new Dictionary<string, Guid>();
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            int count;
            try
            {
                //logger.LogText(1, "BombCfg", "Reading configuration for type {0} from database", Name);
                //Execute stored procedure in DB to obtain configuration
                cmd = new SqlCommand("select TUName,TUInstanceID from TUInstances TUI "
                    + "where TUTypeID='" + TUTypeID + "' order by TUName", dbConn);
                cmd.CommandType = CommandType.Text; //.StoredProcedure;
                //cmd.Parameters.Add("@TUTypeName", SqlDbType.NVarChar, 50).Value = Name;
                reader = cmd.ExecuteReader();

                //Read list of TU instance
                count = 0;
                while (reader.Read())
                {
                    TUInstances.Add(reader.GetString(0), reader.GetGuid(1));
                    count++;
                }
            }
            catch (Exception ex)
            {
                //logger.LogText(1, "BombCfg", "Failed to read configuration of type {0}: {1}", Name, ex.Message);
                throw ex;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public void ReadVersions(SqlConnection dbConn)
        {
            dddVersions = new Dictionary<string, DDDVersion>();
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            int count;
            try
            {
                //logger.LogText(1, "BombCfg", "Reading configuration for type {0} from database", Name);
                //Execute stored procedure in DB to obtain configuration
                cmd = new SqlCommand("select UserVersion,VersionID from DDDVersions DDDV "
                    + "where TUTypeID='" + TUTypeID + "' order by UserVersion", dbConn);
                cmd.CommandType = CommandType.Text; //.StoredProcedure;
                //cmd.Parameters.Add("@TUTypeName", SqlDbType.NVarChar, 50).Value = Name;
                reader = cmd.ExecuteReader();

                //Read list of TU instance
                count = 0;
                while (reader.Read())
                {
                    string userVersion = reader.GetString(0);
                    Guid userVersionID = reader.GetGuid(1);
                    DDDVersion vers = new DDDVersion(connString, userVersion);
                    vers.DDDVersionID = userVersionID;
                    dddVersions.Add(userVersion, vers);
                    count++;
                }
            }
            catch (Exception ex)
            {
                //logger.LogText(1, "BombCfg", "Failed to read configuration of type {0}: {1}", Name, ex.Message);
                throw ex;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public void ReadLastVersion(SqlConnection dbConn)
        {
            lastVersion = "";
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            int count;
            try
            {
                //logger.LogText(1, "BombCfg", "Reading configuration for type {0} from database", Name);
                //Execute stored procedure in DB to obtain configuration
                cmd = new SqlCommand("select top(1) UserVersion,VersionID from DDDVersions DDDV "
                    + "where TUTypeID='" + TUTypeID + "' order by CreationDate desc", dbConn);
                cmd.CommandType = CommandType.Text; //.StoredProcedure;
                //cmd.Parameters.Add("@TUTypeName", SqlDbType.NVarChar, 50).Value = Name;
                reader = cmd.ExecuteReader();

                //Read list of TU instance
                count = 0;
                while (reader.Read())
                {
                    lastVersion = reader.GetString(0);
                    count++;
                }
            }
            catch (Exception ex)
            {
                //logger.LogText(1, "BombCfg", "Failed to read configuration of type {0}: {1}", Name, ex.Message);
                throw ex;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public string GetLastBinaryData(Guid TUTypeID, Guid TUInstanceID, string DDDVersionName, string FaultCode)
        {
            byte[] binData = new byte[0];
            GetLastBinaryData(TUTypeID, TUInstanceID, DDDVersionName, FaultCode, ref binData);
            return Convert.ToBase64String(binData);
        }

        public void GetLastBinaryData(Guid TUTypeID, Guid TUInstanceID, string DDDVersionName, string FaultCode, ref byte[] binData)
        {
            Guid DDDVersionID = GetDDDVersionID(DDDVersionName);
            SqlConnection dbConn = null;
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            try
            {
                dbConn = new SqlConnection(connString);
                dbConn.Open();

                cmd = new SqlCommand("SELECT TOP(1) DR.BinaryData, DR.BigEndianData " +
                    "FROM DiagnosticRecords DR WITH (NOLOCK) " +
                    "INNER JOIN RecordDefinitions RD " +
                    "  ON (DR.DDDVersionID = RD.VersionID AND DR.RecordDefID = RD.RecordDefID) " +
                    "WHERE (DR.TUInstanceID = '" + TUInstanceID.ToString() + "')" +
                    "  AND (DR.DDDVersionID = '" + DDDVersionID.ToString() + "')" +
                    "  AND (RD.RecordTypeID = 1) AND (RD.FaultCode IN (" + FaultCode + ")) " +
                    "ORDER BY DR.StartTime DESC "
                    , dbConn);
                cmd.CommandType = CommandType.Text;
                reader = cmd.ExecuteReader();

                //Read list of BinaryData field
                if (reader.Read())
                {
                    binData = new byte[reader.GetBytes(0, 0, null, 0, int.MaxValue)];
                    long nBytes = reader.GetBytes(0, 0, binData, 0, int.MaxValue);
                }
            }
            catch (Exception ex)
            {
                //logger.LogText(1, "BombCfg", "Failed to read configuration of type {0}: {1}", Name, ex.Message);
                throw ex;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (cmd != null)
                    cmd.Dispose();
                if (dbConn != null)
                {
                    dbConn.Close();
                    dbConn.Dispose();
                }
            }
        }
    }



    public class DDDVersion
    {
        private string connString;
        private String userVersion; //Name of the configuration version
        public Guid DDDVersionID; //ID of associated DDD Version

        // < RecordDefId , RecordName >
        private Dictionary<int, String> diagRecords; //Dictionary of diagnostic records configured for the version
        // < FaultCode.ToString() , RecordDefId >
        private Dictionary<String, int> diagEventFCs; //Dictionary of diagnostic records configured for the version
        // < RecordName , RecordDefId >
        private Dictionary<String, int> diagEventNames; //Dictionary of diagnostic records configured for the version
        private Dictionary<String, int> diagTraceNames; //Dictionary of diagnostic records configured for the version

        public DDDVersion(string ConnString, string UserVersion)
        {
            connString = ConnString;
            userVersion = UserVersion;
            SqlConnection dbConn = null;
            //SqlCommand dbCmd = null;
            try
            {
                //Create connection to database
                dbConn = new SqlConnection(connString);
                dbConn.Open();
                ReadDDDVersion(dbConn);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to load Version " + userVersion, ex);
            }
            finally
            {
                //Dispose command
                //if (dbCmd != null)
                //    dbCmd.Dispose();
                //Dispose database connection
                if (dbConn != null)
                {
                    dbConn.Close();
                    dbConn.Dispose();
                }
            }
        }

        public int GetRecordDefID(string FaultCode)
        {
            int id;
            if (diagEventFCs.TryGetValue(FaultCode, out id))
            {
                return id;
            }
            else
                return -1;
        }

        public int GetTraceRecordDefID(string RecordName)
        {
            int id;
            if (diagTraceNames.TryGetValue(RecordName, out id))
            {
                return id;
            }
            else
                return -1;
        }

        public void ReadDDDVersion(SqlConnection dbConn)
        {
            diagRecords = new Dictionary<int, string>();
            diagEventFCs = new Dictionary<string, int>();
            diagEventNames = new Dictionary<string, int>();
            diagTraceNames = new Dictionary<string, int>();
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            int count;
            try
            {
                //logger.LogText(1, "BombCfg", "Reading configuration for type {0} from database", Name);
                //Execute stored procedure in DB to obtain configuration
                cmd = new SqlCommand("select RecordDefID,RT.RecordType,Name,cast(FaultCode as varchar) from RecordDefinitions RD "
                    + "inner join DDDVersions DDDV on DDDV.VersionID=RD.VersionID "
                    + "inner join RecordTypes RT on RT.RecordTypeID=RD.RecordTypeID "
                    + "where DDDV.UserVersion='" + userVersion + "' order by FaultCode", dbConn);
                cmd.CommandType = CommandType.Text; //.StoredProcedure;
                //cmd.Parameters.Add("@TUTypeName", SqlDbType.NVarChar, 50).Value = Name;
                reader = cmd.ExecuteReader();

                //Read list of DDD Version
                count = 0;
                while (reader.Read())
                {
                    string RecType = reader.GetString(1);
                    int id = reader.GetInt32(0);
                    string RecordName = reader.GetString(2);
                    string FaultCode = "";
                    if (!reader.IsDBNull(3))
                        FaultCode = (string)reader.GetSqlString(3);
                    //string FaultCode = (string)reader.GetSqlString(3);

                    diagRecords.Add(id, RecordName);
                    
                    switch (RecType)
                    {
                        case "Event":
                            diagEventFCs.Add(FaultCode, id);
                            diagEventNames.Add(RecordName, id);
                            break;
                        case "Trace":
                            //diagTraceFCs.Add(reader.GetString(3), reader.GetInt32(0));
                            diagTraceNames.Add(RecordName, id);
                            break;
                    }
                    count++;
                }
                //logger.LogText(2, "BombCfg", "{0} TU Instances", count);

                /*
                reader.NextResult();

                //Read list of DDD versions
                count = 0;
                while (reader.Read())
                {
                    //Add DDD versions to repository
                    AddDDDVersion(reader.GetString(0), reader.GetGuid(1));
                    count++;
                }
                logger.LogText(2, "BombCfg", "{0} DDD Versions", count);

                reader.NextResult();

                //Read list of record definitions
                count = 0;
                while (reader.Read())
                {
                    AddRecordDef(reader.GetString(0), reader.GetString(1), reader.GetInt32(2));
                    count++;
                }
                logger.LogText(2, "BombCfg", "{0} Record definitions", count);
                logger.LogText(1, "BombCfg", "Reading of configuration finished");
                */
            }
            catch (Exception ex)
            {
                //logger.LogText(1, "BombCfg", "Failed to read configuration of type {0}: {1}", Name, ex.Message);
                throw ex;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (cmd != null)
                    cmd.Dispose();
            }
        }
    }
}
