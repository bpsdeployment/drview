using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;
using System.Runtime.InteropServices;

namespace DDSMultiImport
{
    static class Program
    {
        [DllImport("kernel32.dll")]
        static extern bool AttachConsole(int dwProcessId);
        private const int ATTACH_PARENT_PROCESS = -1;
        
        static void Main(string[] args)
        {
            bool service = true;
            bool help = false; // (args == null || args.Length == 0);
            bool config = false;

            foreach (string s in args)
            {
                switch (s.ToLower())
                {
                    case "-h":
                    case "-help":
                        service = false;
                        config = false;
                        help = true;
                        break;

                    case "-run":
                        // 
                        //UploadManager manager = new UploadManager();
                        //manager.Execute();
                        service = false;
                        config = false;
                        break;

                    case "-form":
                    case "-config":
                        service = false;
                        config = true;
                        break;
                }
            }

            if (help)
            {
                // redirect console output to parent process
                AttachConsole(ATTACH_PARENT_PROCESS);

                Console.WriteLine("");
                Console.WriteLine("----------");
                Console.WriteLine("-- Help --");
                Console.WriteLine("----------");
                Console.WriteLine("");
                Console.WriteLine("-h -help ........... this help");
                Console.WriteLine("-run  .............. run the application");
                Console.WriteLine("-form -config  ..... configuration helper using windows form");
                Console.WriteLine("                     with possibility of application running");
                //Console.WriteLine("");
                //string name = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                //Console.WriteLine("Configure " + name + ".config");
            }

            if (config)
            {
                // launch Windows Form
                //Application.EnableVisualStyles();
                //Application.SetCompatibleTextRenderingDefault(false);
                //Application.Run(new ConfigurationForm());
            }

            if (service)
            {
                ServiceBase[] ServicesToRun;

                // More than one user Service may run within the same process. To add
                // another service to this process, change the following line to
                // create a second service object. For example,
                //
                //   ServicesToRun = new ServiceBase[] {new Service1(), new MySecondUserService()};
                //
                ServicesToRun = new ServiceBase[] { new DDSMultiImport() };

                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
