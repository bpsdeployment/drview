using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace E403_Atrterm
{
    public partial class ConfigurationForm : Form
    {
        private string appFilename;
        private string appName;

        public ConfigurationForm()
        {
            InitializeComponent();
        }

        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            appFilename = Path.GetFileName(Application.ExecutablePath);
            appName = Application.ProductName;
            this.Text += appName;
            RestoreSettings();
        }

        private void ConfigurationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (lblChanged.Visible)
                e.Cancel = (MessageBox.Show("Application is disabled, exit anyway?", "WARNING", MessageBoxButtons.OKCancel) == DialogResult.Cancel);
        }

        private void RestoreSettings()
        {
            Properties.Settings.Default.Reload();

            mnuUpdateConfig.Checked = Properties.Settings.Default.UpdateConfigOnSave;
            if (mnuUpdateConfig.Checked)
                Properties.Settings.Default.Reset();

            txtTUType.Text = Properties.Settings.Default.TUType;
            chkEnabled.Checked = Properties.Settings.Default.Enabled;
            txtDirectory.Text = Properties.Settings.Default.DDSSourceDir;
            txtWorkDir.Text = Properties.Settings.Default.DDSConvertDir;
            txtImportDir.Text = Properties.Settings.Default.DRImportDir;
            txtRepositoryDir.Text = Properties.Settings.Default.DDSRepositoryDir;

            txtTUSubDirectoryOK.Text = Properties.Settings.Default.DDSSourceSubdirOk;
            txtTUSubDirectoryFailed.Text = Properties.Settings.Default.DDSSourceSubdirFailed;
            txtSubdirBadfiles.Text = Properties.Settings.Default.DDSSourceSubdirBadfiles;

            txtLogVerbosity.Text = Properties.Settings.Default.LogVerbosity;
            txtLogFileName.Text = Properties.Settings.Default.LogFileName;

            txtZipFile.Text = Properties.Settings.Default.ZipExe;
            txtConnectionString.Text = Properties.Settings.Default.CDDBConnString;

            txtMaxFileInBatch.Text = Properties.Settings.Default.maxFileInBatch.ToString();
            txtSubFnTimeout.Text = Properties.Settings.Default.SubFnTimeout.ToString();
        }

        private void SaveSettings()
        {
            Properties.Settings.Default.UpdateConfigOnSave = mnuUpdateConfig.Checked;

            Properties.Settings.Default.TUType = txtTUType.Text;
            Properties.Settings.Default.Enabled = chkEnabled.Checked;
            Properties.Settings.Default.DDSSourceDir = txtDirectory.Text;
            Properties.Settings.Default.DDSConvertDir = txtWorkDir.Text;
            Properties.Settings.Default.DRImportDir = txtImportDir.Text;
            Properties.Settings.Default.DDSRepositoryDir = txtRepositoryDir.Text;

            Properties.Settings.Default.DDSSourceSubdirOk = txtTUSubDirectoryOK.Text;
            Properties.Settings.Default.DDSSourceSubdirFailed = txtTUSubDirectoryFailed.Text;
            Properties.Settings.Default.DDSSourceSubdirBadfiles = txtSubdirBadfiles.Text;

            Properties.Settings.Default.LogVerbosity = txtLogVerbosity.Text;
            Properties.Settings.Default.LogFileName = txtLogFileName.Text;

            Properties.Settings.Default.ZipExe = txtZipFile.Text;
            Properties.Settings.Default.CDDBConnString = txtConnectionString.Text;

            Properties.Settings.Default.maxFileInBatch = int.Parse(txtMaxFileInBatch.Text);
            Properties.Settings.Default.SubFnTimeout = int.Parse(txtSubFnTimeout.Text);

            Properties.Settings.Default.Save();

            ClearLblChanged();
        }

        private void ClearLblChanged()
        {
            lblUpdateConfig.Visible = false;
            lblChgUpdateConfig.Visible = false;
            lblChgTUType.Visible = false;
            lblChgEnabled.Visible = false;
            lblChgDirectory.Visible = false;
            lblChgWorkDir.Visible = false;
            lblChgImportDir.Visible = false;
            lblChgRepositoryDir.Visible = false;
            lblChgTUSubDirectoryOK.Visible = false;
            lblChgTUSubDirectoryFailed.Visible = false;
            lblChgSubdirBadfiles.Visible = false;
            lblChgLogVerbosity.Visible = false;
            lblChgLogFileName.Visible = false;
            lblChgZipFile.Visible = false;
            lblChgConnectionString.Visible = false;
            lblChgMaxFileInBatch.Visible = false;
            lblChgSubFnTimeout.Visible = false;

            NotifyChanging();
        }

        private void UpdateConfig()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(appFilename + ".config");
            XmlNode user = doc.SelectSingleNode("//userSettings/" + appName + ".Properties.Settings");
            foreach (XmlElement n in user.ChildNodes)
            {
                string name = n.Attributes.GetNamedItem("name").InnerText;
                string value = n.InnerText;
                string serialized = n.Attributes.GetNamedItem("serializeAs").InnerText;
                string newValue = "";
                try
                {
                    object ob = Properties.Settings.Default[name];
                    string obType = ob.GetType().ToString();
                    switch (obType)
                    {
                        case "System.Boolean":
                            newValue = ((bool)Properties.Settings.Default[name]).ToString();
                            break;
                        case "System.String":
                            newValue = (string)Properties.Settings.Default[name];
                            break;
                    }
                    if (newValue != value)
                    {
                        XmlNode val = n.FirstChild;
                        val.InnerText = newValue;
                    }
                }
                catch(Exception){}
            }
            doc.Save(appFilename + ".config");
            doc = null;
        }


        #region Command Button handler
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (lblChanged.Visible)
                SaveSettings();
            if (mnuUpdateConfig.Checked)
                UpdateConfig();
        }

        private void btnRestore_Click(object sender, EventArgs e)
        {
            RestoreSettings();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion //Command Button handler

        #region Menu handler
        private void mnuRun_Click(object sender, EventArgs e)
        {
            //Properties.Settings.Default.Upgrade();
            if (MessageBox.Show("You are sure to execute " + appName + " application ?", "WARNING", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                // Create and execute application
                UploadManager manager = new UploadManager();
                manager.Execute();
            }
        }

        private void mnuReset_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Sicuro di voler riportare i valori a quelli di origine ?", "ATTENZIONE", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                if (!mnuUpdateConfig.Checked)
                    Properties.Settings.Default.Reset();
                RestoreSettings();
            }
        }

        private void mnuSaveIntoConfig_Click(object sender, EventArgs e)
        {
            if (lblChanged.Visible)
                SaveSettings();
            UpdateConfig();
        }
        #endregion //Menu handler

        #region Configuration Button handler
        private void btnDirectory_Click(object sender, EventArgs e)
        {
            string current = txtDirectory.Text;
            if (!Path.IsPathRooted(current))
            {
                current = Application.StartupPath;
            }
            folderBrowserDialog1.SelectedPath = current;
            folderBrowserDialog1.Description = "Select Source directory";

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtDirectory.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnWorkDir_Click(object sender, EventArgs e)
        {
            string current = txtWorkDir.Text;
            if (!Path.IsPathRooted(current))
            {
                current = Application.StartupPath;
            }
            folderBrowserDialog1.SelectedPath = current;
            folderBrowserDialog1.Description = "Select Work directory";

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtWorkDir.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnImportDir_Click(object sender, EventArgs e)
        {
            string current = txtImportDir.Text;
            if (!Path.IsPathRooted(current))
            {
                current = Application.StartupPath;
            }
            folderBrowserDialog1.SelectedPath = current;
            folderBrowserDialog1.Description = "Select DR Import directory";

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtImportDir.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnRepositoryDir_Click(object sender, EventArgs e)
        {
            string current = txtRepositoryDir.Text;
            if (!Path.IsPathRooted(current))
            {
                current = Application.StartupPath;
            }
            folderBrowserDialog1.SelectedPath = current;
            folderBrowserDialog1.Description = "Select Same Structure Repository directory";

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtRepositoryDir.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnTUSubDirectoryOK_Click(object sender, EventArgs e)
        {
            string current = txtDirectory.Text;
            folderBrowserDialog1.SelectedPath = current;
            folderBrowserDialog1.Description = "Select OK Notify folder that is a subdirectory of each Instance";

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtTUSubDirectoryOK.Text = Path.GetFileName(folderBrowserDialog1.SelectedPath);
            }
        }

        private void btnTUSubDirectoryFailed_Click(object sender, EventArgs e)
        {
            string current = txtDirectory.Text;
            folderBrowserDialog1.SelectedPath = current;
            folderBrowserDialog1.Description = "Select folder name where it store Failed files that is a subdirectory of each Instance";

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtTUSubDirectoryFailed.Text = Path.GetFileName(folderBrowserDialog1.SelectedPath);
            }
        }

        private void btnSubdirBadfiles_Click(object sender, EventArgs e)
        {
            string currentDir = txtDirectory.Text;
            if (!Path.IsPathRooted(currentDir))
            {
                currentDir = Application.StartupPath;
            }
            openFileDialog1.FileName = txtSubdirBadfiles.Text;
            openFileDialog1.InitialDirectory = currentDir;
            openFileDialog1.Title = "Select badfiles file name that is in a subdirectory of each Instance";
            openFileDialog1.CheckFileExists = false;
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.Filter = "";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtSubdirBadfiles.Text = Path.GetFileName(openFileDialog1.FileName);
            }
        }

        private void btnLogFileName_Click(object sender, EventArgs e)
        {
            string current = txtLogFileName.Text;
            string currentDir = "";
            if (!Path.IsPathRooted(current))
            {
                currentDir = Path.GetPathRoot(txtImportDir.Text);
                current = "";
            }
            else
            {
                currentDir = Path.GetFullPath(current);
                string filename = Path.GetFileName(current);
                currentDir = currentDir.Substring(0, currentDir.Length - filename.Length);
            }
            openFileDialog1.FileName = current;
            openFileDialog1.InitialDirectory = currentDir;
            openFileDialog1.Title = "Select DR Import directory";
            openFileDialog1.CheckFileExists = false;
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.Filter = "";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtLogFileName.Text = openFileDialog1.FileName;
            }
        }

        private void btnZipFile_Click(object sender, EventArgs e)
        {
            string current = txtZipFile.Text;
            if (current.StartsWith("."))
            {
                current = Path.Combine(Application.StartupPath, current);
            }
            string currentDir = "";// txtDirectory.Text;
            if (!Path.IsPathRooted(current))
            {
                current = "";
                currentDir = Application.StartupPath;
            }
            else
            {
                currentDir = Path.GetFullPath(current);
                string filename = Path.GetFileName(current);
                currentDir = currentDir.Substring(0, currentDir.Length - filename.Length);
                current = filename;
            }
            openFileDialog1.FileName = current;
            openFileDialog1.InitialDirectory = currentDir;
            openFileDialog1.Title = "Select zip program";
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.Filter = "exe|*.exe";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtZipFile.Text = openFileDialog1.FileName;
            }
        }
        #endregion //Configuration Button handler

        #region Notify Changing Section
        private void NotifyChanging()
        {
            bool changed =
                lblChgUpdateConfig.Visible ||
                lblChgTUType.Visible ||
                lblChgEnabled.Visible ||
                lblChgDirectory.Visible ||
                lblChgWorkDir.Visible ||
                lblChgImportDir.Visible ||
                lblChgRepositoryDir.Visible ||
                lblChgTUSubDirectoryOK.Visible ||
                lblChgTUSubDirectoryFailed.Visible ||
                lblChgSubdirBadfiles.Visible ||
                lblChgLogVerbosity.Visible ||
                lblChgLogFileName.Visible ||
                lblChgZipFile.Visible ||
                lblChgConnectionString.Visible ||
                lblChgMaxFileInBatch.Visible ||
                lblChgSubFnTimeout.Visible;

            lblChanged.Visible = changed;
            btnOK.Enabled = changed;
        }

        private void mnuUpdateConfig_Click(object sender, EventArgs e)
        {
            bool changed = mnuUpdateConfig.Checked != Properties.Settings.Default.UpdateConfigOnSave;
            lblChgUpdateConfig.Visible = changed;
            lblUpdateConfig.Visible = changed;
            NotifyChanging();
        }

        private void txtTUType_TextChanged(object sender, EventArgs e)
        {
            lblChgTUType.Visible = (txtTUType.Text.Trim() != Properties.Settings.Default.TUType);
            NotifyChanging();
        }

        private void chkEnabled_CheckedChanged(object sender, EventArgs e)
        {
            lblChgEnabled.Visible = (chkEnabled.Checked != Properties.Settings.Default.Enabled);
            NotifyChanging();
        }

        private void txtDirectory_TextChanged(object sender, EventArgs e)
        {
            lblChgDirectory.Visible = (txtDirectory.Text.Trim() != Properties.Settings.Default.DDSSourceDir);
            NotifyChanging();
        }

        private void txtWorkDir_TextChanged(object sender, EventArgs e)
        {
            lblChgWorkDir.Visible = (txtWorkDir.Text.Trim() != Properties.Settings.Default.DDSConvertDir);
            NotifyChanging();
        }

        private void txtImportDir_TextChanged(object sender, EventArgs e)
        {
            lblChgImportDir.Visible = (txtImportDir.Text.Trim() != Properties.Settings.Default.DRImportDir);
            NotifyChanging();
        }

        private void txtRepositoryDir_TextChanged(object sender, EventArgs e)
        {
            lblChgRepositoryDir.Visible = (txtRepositoryDir.Text.Trim() != Properties.Settings.Default.DDSRepositoryDir);
            NotifyChanging();
        }

        private void txtTUSubDirectoryOK_TextChanged(object sender, EventArgs e)
        {
            lblChgTUSubDirectoryOK.Visible = (txtTUSubDirectoryOK.Text.Trim() != Properties.Settings.Default.DDSSourceSubdirOk);
            NotifyChanging();
        }

        private void txtTUSubDirectoryFailed_TextChanged(object sender, EventArgs e)
        {
            lblChgTUSubDirectoryFailed.Visible = (txtTUSubDirectoryFailed.Text.Trim() != Properties.Settings.Default.DDSSourceSubdirFailed);
            NotifyChanging();
        }

        private void txtSubdirBadfiles_TextChanged(object sender, EventArgs e)
        {
            lblChgSubdirBadfiles.Visible = (txtSubdirBadfiles.Text.Trim() != Properties.Settings.Default.DDSSourceSubdirBadfiles);
            NotifyChanging();
        }

        private void txtLogVerbosity_TextChanged(object sender, EventArgs e)
        {
            lblChgLogVerbosity.Visible = (txtLogVerbosity.Text.Trim() != Properties.Settings.Default.LogVerbosity);
            NotifyChanging();
        }

        private void txtLogFileName_TextChanged(object sender, EventArgs e)
        {
            lblChgLogFileName.Visible = (txtLogFileName.Text.Trim() != Properties.Settings.Default.LogFileName);
            NotifyChanging();
        }

        private void txtZipFile_TextChanged(object sender, EventArgs e)
        {
            lblChgZipFile.Visible = (txtZipFile.Text.Trim() != Properties.Settings.Default.ZipExe);
            NotifyChanging();
        }

        private void txtConnectionString_TextChanged(object sender, EventArgs e)
        {
            lblChgConnectionString.Visible = (txtConnectionString.Text.Trim() != Properties.Settings.Default.CDDBConnString);
            NotifyChanging();
        }

        private void txtMaxFileInBatch_TextChanged(object sender, EventArgs e)
        {
            lblChgMaxFileInBatch.Visible = (txtMaxFileInBatch.Text.Trim() != Properties.Settings.Default.maxFileInBatch.ToString());
            NotifyChanging();
        }

        private void txtSubFnTimeout_TextChanged(object sender, EventArgs e)
        {
            lblChgSubFnTimeout.Visible = (txtSubFnTimeout.Text.Trim() != Properties.Settings.Default.SubFnTimeout.ToString());
            NotifyChanging();
        }
        #endregion //Notify Changing Section
    }
}