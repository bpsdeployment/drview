﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace E403_Atrterm
{
    static class Program
    {
        [DllImport("kernel32.dll")]
        static extern bool AttachConsole(int dwProcessId);
        private const int ATTACH_PARENT_PROCESS = -1;

        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            bool help = false; // (args == null || args.Length == 0);
            bool config = true;
            //for (int i = 0; i < args.Length; i++)
            foreach (string s in args)
            {
                switch (s.ToLower())
                {
                    case "-h":
                    case "-help":
                        config = false;
                        help = true;
                        break;

                    case "-run":
                        // 
                        UploadManager manager = new UploadManager();
                        manager.Execute();

                        config = false;
                        break;

                    case "-form":
                    case "-config":
                        config = true;
                        break;
                }
            }

            if (help)
            {
                // redirect console output to parent process
                AttachConsole(ATTACH_PARENT_PROCESS);

                Console.WriteLine("");
                Console.WriteLine("----------");
                Console.WriteLine("-- Help --");
                Console.WriteLine("----------");
                Console.WriteLine("");
                Console.WriteLine("-h -help ........... this help");
                Console.WriteLine("-run  .............. run the dos application");
                Console.WriteLine("-form -config  ..... configuration helper using windows form");
                Console.WriteLine("                     with application run possibility");
                //Console.WriteLine("");
                //string name = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                //Console.WriteLine("Configure " + name + ".config");
            }

            if (config)
            {
                // launch then Windows Form
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new ConfigurationForm());
            }
        }
    }
}