namespace DRView
{
  partial class frmDBConnection
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.chbUseDBConn = new System.Windows.Forms.CheckBox();
      this.gbDBConn = new System.Windows.Forms.GroupBox();
      this.gbConnSpec = new System.Windows.Forms.GroupBox();
      this.label1 = new System.Windows.Forms.Label();
      this.label12 = new System.Windows.Forms.Label();
      this.tbDBName = new System.Windows.Forms.TextBox();
      this.tbDBServerName = new System.Windows.Forms.TextBox();
      this.cbConnType = new System.Windows.Forms.ComboBox();
      this.label11 = new System.Windows.Forms.Label();
      this.rbDBSqlAuth = new System.Windows.Forms.RadioButton();
      this.tbDBPassword = new System.Windows.Forms.TextBox();
      this.label10 = new System.Windows.Forms.Label();
      this.tbDBUser = new System.Windows.Forms.TextBox();
      this.rbDBWinAuth = new System.Windows.Forms.RadioButton();
      this.btOK = new System.Windows.Forms.Button();
      this.persistWindow = new Mowog.PersistWindowComponent(this.components);
      this.gbDBConn.SuspendLayout();
      this.gbConnSpec.SuspendLayout();
      this.SuspendLayout();
      // 
      // chbUseDBConn
      // 
      this.chbUseDBConn.AutoSize = true;
      this.chbUseDBConn.Location = new System.Drawing.Point(8, 7);
      this.chbUseDBConn.Name = "chbUseDBConn";
      this.chbUseDBConn.Size = new System.Drawing.Size(119, 17);
      this.chbUseDBConn.TabIndex = 0;
      this.chbUseDBConn.Text = "Use DB connection";
      this.chbUseDBConn.UseVisualStyleBackColor = true;
      this.chbUseDBConn.CheckedChanged += new System.EventHandler(this.chbUseDBConn_CheckedChanged);
      // 
      // gbDBConn
      // 
      this.gbDBConn.Controls.Add(this.gbConnSpec);
      this.gbDBConn.Controls.Add(this.cbConnType);
      this.gbDBConn.Controls.Add(this.label11);
      this.gbDBConn.Controls.Add(this.rbDBSqlAuth);
      this.gbDBConn.Controls.Add(this.tbDBPassword);
      this.gbDBConn.Controls.Add(this.label10);
      this.gbDBConn.Controls.Add(this.tbDBUser);
      this.gbDBConn.Controls.Add(this.rbDBWinAuth);
      this.gbDBConn.Location = new System.Drawing.Point(8, 21);
      this.gbDBConn.Name = "gbDBConn";
      this.gbDBConn.Size = new System.Drawing.Size(170, 275);
      this.gbDBConn.TabIndex = 1;
      this.gbDBConn.TabStop = false;
      // 
      // gbConnSpec
      // 
      this.gbConnSpec.Controls.Add(this.label1);
      this.gbConnSpec.Controls.Add(this.label12);
      this.gbConnSpec.Controls.Add(this.tbDBName);
      this.gbConnSpec.Controls.Add(this.tbDBServerName);
      this.gbConnSpec.Location = new System.Drawing.Point(6, 46);
      this.gbConnSpec.Name = "gbConnSpec";
      this.gbConnSpec.Size = new System.Drawing.Size(156, 99);
      this.gbConnSpec.TabIndex = 2;
      this.gbConnSpec.TabStop = false;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(6, 54);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(82, 13);
      this.label1.TabIndex = 40;
      this.label1.Text = "Database name";
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(6, 16);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(67, 13);
      this.label12.TabIndex = 39;
      this.label12.Text = "Server name";
      // 
      // tbDBName
      // 
      this.tbDBName.Location = new System.Drawing.Point(9, 70);
      this.tbDBName.Name = "tbDBName";
      this.tbDBName.Size = new System.Drawing.Size(136, 20);
      this.tbDBName.TabIndex = 36;
      this.tbDBName.Text = "CDDB";
      // 
      // tbDBServerName
      // 
      this.tbDBServerName.Location = new System.Drawing.Point(9, 32);
      this.tbDBServerName.Name = "tbDBServerName";
      this.tbDBServerName.Size = new System.Drawing.Size(136, 20);
      this.tbDBServerName.TabIndex = 35;
      // 
      // cbConnType
      // 
      this.cbConnType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbConnType.FormattingEnabled = true;
      this.cbConnType.Items.AddRange(new object[] {
            "First",
            "Second",
            "Custom..."});
      this.cbConnType.Location = new System.Drawing.Point(6, 19);
      this.cbConnType.Name = "cbConnType";
      this.cbConnType.Size = new System.Drawing.Size(156, 21);
      this.cbConnType.TabIndex = 0;
      this.cbConnType.SelectedIndexChanged += new System.EventHandler(this.cbConnType_SelectedIndexChanged);
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(25, 190);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(58, 13);
      this.label11.TabIndex = 38;
      this.label11.Text = "User name";
      // 
      // rbDBSqlAuth
      // 
      this.rbDBSqlAuth.AutoSize = true;
      this.rbDBSqlAuth.Location = new System.Drawing.Point(15, 170);
      this.rbDBSqlAuth.Name = "rbDBSqlAuth";
      this.rbDBSqlAuth.Size = new System.Drawing.Size(116, 17);
      this.rbDBSqlAuth.TabIndex = 32;
      this.rbDBSqlAuth.Text = "SQL authentication";
      this.rbDBSqlAuth.UseVisualStyleBackColor = true;
      this.rbDBSqlAuth.CheckedChanged += new System.EventHandler(this.rbDBSqlAuth_CheckedChanged);
      // 
      // tbDBPassword
      // 
      this.tbDBPassword.Location = new System.Drawing.Point(28, 248);
      this.tbDBPassword.Name = "tbDBPassword";
      this.tbDBPassword.PasswordChar = '*';
      this.tbDBPassword.Size = new System.Drawing.Size(123, 20);
      this.tbDBPassword.TabIndex = 34;
      this.tbDBPassword.UseSystemPasswordChar = true;
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(25, 232);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(53, 13);
      this.label10.TabIndex = 37;
      this.label10.Text = "Password";
      // 
      // tbDBUser
      // 
      this.tbDBUser.Location = new System.Drawing.Point(28, 206);
      this.tbDBUser.Name = "tbDBUser";
      this.tbDBUser.Size = new System.Drawing.Size(123, 20);
      this.tbDBUser.TabIndex = 33;
      // 
      // rbDBWinAuth
      // 
      this.rbDBWinAuth.AutoSize = true;
      this.rbDBWinAuth.Checked = true;
      this.rbDBWinAuth.Location = new System.Drawing.Point(15, 151);
      this.rbDBWinAuth.Name = "rbDBWinAuth";
      this.rbDBWinAuth.Size = new System.Drawing.Size(144, 17);
      this.rbDBWinAuth.TabIndex = 31;
      this.rbDBWinAuth.TabStop = true;
      this.rbDBWinAuth.Text = "Windows authentification";
      this.rbDBWinAuth.UseVisualStyleBackColor = true;
      // 
      // btOK
      // 
      this.btOK.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btOK.Location = new System.Drawing.Point(54, 302);
      this.btOK.Name = "btOK";
      this.btOK.Size = new System.Drawing.Size(75, 23);
      this.btOK.TabIndex = 2;
      this.btOK.Text = "OK";
      this.btOK.UseVisualStyleBackColor = true;
      // 
      // persistWindow
      // 
      this.persistWindow.Form = this;
      this.persistWindow.UseLocAppDataDir = true;
      this.persistWindow.XMLFilePath = "DRViewWindowState.xml";
      // 
      // frmDBConnection
      // 
      this.AcceptButton = this.btOK;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(186, 329);
      this.ControlBox = false;
      this.Controls.Add(this.btOK);
      this.Controls.Add(this.gbDBConn);
      this.Controls.Add(this.chbUseDBConn);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frmDBConnection";
      this.Text = "Specify DB connection";
      this.gbDBConn.ResumeLayout(false);
      this.gbDBConn.PerformLayout();
      this.gbConnSpec.ResumeLayout(false);
      this.gbConnSpec.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.CheckBox chbUseDBConn;
    private System.Windows.Forms.GroupBox gbDBConn;
    private System.Windows.Forms.ComboBox cbConnType;
    private System.Windows.Forms.GroupBox gbConnSpec;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.RadioButton rbDBSqlAuth;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.RadioButton rbDBWinAuth;
    private System.Windows.Forms.TextBox tbDBName;
    private System.Windows.Forms.TextBox tbDBUser;
    private System.Windows.Forms.TextBox tbDBServerName;
    private System.Windows.Forms.TextBox tbDBPassword;
    private System.Windows.Forms.Button btOK;
    private Mowog.PersistWindowComponent persistWindow;
  }
}