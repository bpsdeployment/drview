namespace DRView
{
  partial class frmProperties
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProperties));
            this.btSave = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btBrowseRuntimeObjDir = new System.Windows.Forms.Button();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lbTimeSample = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbctrlProperties = new System.Windows.Forms.TabControl();
            this.tpDiagRecords = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.nudCmdTimeout = new System.Windows.Forms.NumericUpDown();
            this.lbTimeSampleGraph = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbTimeFormatGraph = new System.Windows.Forms.TextBox();
            this.chbCompRecTypeCount = new System.Windows.Forms.CheckBox();
            this.chbShowSystemMessages = new System.Windows.Forms.CheckBox();
            this.chbUseLocTime = new System.Windows.Forms.CheckBox();
            this.tbTimeFromat = new System.Windows.Forms.TextBox();
            this.nudTimeOffset = new System.Windows.Forms.NumericUpDown();
            this.nudMaxRecords = new System.Windows.Forms.NumericUpDown();
            this.tpDBConnection = new System.Windows.Forms.TabPage();
            this.chbAskForDBConn = new System.Windows.Forms.CheckBox();
            this.gbDBConn2Params = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chbDefConn2 = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.tbDBName2 = new System.Windows.Forms.TextBox();
            this.tbDBServerName2 = new System.Windows.Forms.TextBox();
            this.tbDBPassword2 = new System.Windows.Forms.TextBox();
            this.tbDBUser2 = new System.Windows.Forms.TextBox();
            this.rbDBSqlAuth2 = new System.Windows.Forms.RadioButton();
            this.rbDBWinAuth2 = new System.Windows.Forms.RadioButton();
            this.gbDBConn1Params = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chbDefConn1 = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tbDBName1 = new System.Windows.Forms.TextBox();
            this.tbDBServerName1 = new System.Windows.Forms.TextBox();
            this.tbDBPassword1 = new System.Windows.Forms.TextBox();
            this.tbDBUser1 = new System.Windows.Forms.TextBox();
            this.rbDBSqlAuth1 = new System.Windows.Forms.RadioButton();
            this.rbDBWinAuth1 = new System.Windows.Forms.RadioButton();
            this.chbUseDBConnection = new System.Windows.Forms.CheckBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.tbRuntimeObjsDir = new System.Windows.Forms.TextBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.tpFileAssociations = new System.Windows.Forms.TabPage();
            this.btSaveAssoc = new System.Windows.Forms.Button();
            this.chbAssocSTATQ = new System.Windows.Forms.CheckBox();
            this.chbAssocDRSTAT = new System.Windows.Forms.CheckBox();
            this.chbAssocDRQ = new System.Windows.Forms.CheckBox();
            this.chbAssocXGZ = new System.Windows.Forms.CheckBox();
            this.chbAssocDR = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tpImportXMLDiagnostics = new System.Windows.Forms.TabPage();
            this.btBrowseBaselineDir = new System.Windows.Forms.Button();
            this.tbBaselineDir = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.DRViewHelpProvider = new System.Windows.Forms.HelpProvider();
            this.persistWindow = new Mowog.PersistWindowComponent(this.components);
            this.tbctrlProperties.SuspendLayout();
            this.tpDiagRecords.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCmdTimeout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTimeOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxRecords)).BeginInit();
            this.tpDBConnection.SuspendLayout();
            this.gbDBConn2Params.SuspendLayout();
            this.gbDBConn1Params.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tpFileAssociations.SuspendLayout();
            this.tpImportXMLDiagnostics.SuspendLayout();
            this.SuspendLayout();
            // 
            // btSave
            // 
            this.btSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btSave.Location = new System.Drawing.Point(71, 282);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(75, 23);
            this.btSave.TabIndex = 1;
            this.btSave.Text = "Save";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // btCancel
            // 
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Location = new System.Drawing.Point(197, 282);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(75, 23);
            this.btCancel.TabIndex = 2;
            this.btCancel.Text = "Cancel";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(168, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Directory for runtime DDD objects:";
            // 
            // btBrowseRuntimeObjDir
            // 
            this.btBrowseRuntimeObjDir.Location = new System.Drawing.Point(265, 85);
            this.btBrowseRuntimeObjDir.Name = "btBrowseRuntimeObjDir";
            this.btBrowseRuntimeObjDir.Size = new System.Drawing.Size(28, 23);
            this.btBrowseRuntimeObjDir.TabIndex = 1;
            this.btBrowseRuntimeObjDir.Text = "...";
            this.btBrowseRuntimeObjDir.UseVisualStyleBackColor = true;
            this.btBrowseRuntimeObjDir.Click += new System.EventHandler(this.btBrowseRuntimeObjDir_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(139, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Maximum number of records";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Time format in grid";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 111);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Sample:";
            // 
            // lbTimeSample
            // 
            this.lbTimeSample.AutoSize = true;
            this.lbTimeSample.Location = new System.Drawing.Point(46, 111);
            this.lbTimeSample.Name = "lbTimeSample";
            this.lbTimeSample.Size = new System.Drawing.Size(61, 13);
            this.lbTimeSample.TabIndex = 12;
            this.lbTimeSample.Text = "timeSample";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 131);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Time zone offset:";
            // 
            // tbctrlProperties
            // 
            this.tbctrlProperties.Controls.Add(this.tpDiagRecords);
            this.tbctrlProperties.Controls.Add(this.tpDBConnection);
            this.tbctrlProperties.Controls.Add(this.tabPage1);
            this.tbctrlProperties.Controls.Add(this.tpFileAssociations);
            this.tbctrlProperties.Controls.Add(this.tpImportXMLDiagnostics);
            this.tbctrlProperties.Location = new System.Drawing.Point(3, 1);
            this.tbctrlProperties.Name = "tbctrlProperties";
            this.tbctrlProperties.SelectedIndex = 0;
            this.tbctrlProperties.Size = new System.Drawing.Size(342, 279);
            this.tbctrlProperties.TabIndex = 5;
            // 
            // tpDiagRecords
            // 
            this.tpDiagRecords.Controls.Add(this.label16);
            this.tpDiagRecords.Controls.Add(this.nudCmdTimeout);
            this.tpDiagRecords.Controls.Add(this.lbTimeSampleGraph);
            this.tpDiagRecords.Controls.Add(this.label9);
            this.tpDiagRecords.Controls.Add(this.label5);
            this.tpDiagRecords.Controls.Add(this.tbTimeFormatGraph);
            this.tpDiagRecords.Controls.Add(this.chbCompRecTypeCount);
            this.tpDiagRecords.Controls.Add(this.chbShowSystemMessages);
            this.tpDiagRecords.Controls.Add(this.chbUseLocTime);
            this.tpDiagRecords.Controls.Add(this.tbTimeFromat);
            this.tpDiagRecords.Controls.Add(this.nudTimeOffset);
            this.tpDiagRecords.Controls.Add(this.nudMaxRecords);
            this.tpDiagRecords.Controls.Add(this.label8);
            this.tpDiagRecords.Controls.Add(this.lbTimeSample);
            this.tpDiagRecords.Controls.Add(this.label4);
            this.tpDiagRecords.Controls.Add(this.label7);
            this.tpDiagRecords.Controls.Add(this.label6);
            this.tpDiagRecords.Location = new System.Drawing.Point(4, 22);
            this.tpDiagRecords.Name = "tpDiagRecords";
            this.tpDiagRecords.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tpDiagRecords.Size = new System.Drawing.Size(334, 253);
            this.tpDiagRecords.TabIndex = 0;
            this.tpDiagRecords.Text = "Diagnostic records";
            this.tpDiagRecords.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(171, 7);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 13);
            this.label16.TabIndex = 23;
            this.label16.Text = "Query timeout [sec]";
            // 
            // nudCmdTimeout
            // 
            this.nudCmdTimeout.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::DRView.Properties.Settings.Default, "dbCommandTimeoutSec", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.nudCmdTimeout.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudCmdTimeout.Location = new System.Drawing.Point(174, 23);
            this.nudCmdTimeout.Maximum = new decimal(new int[] {
            7200,
            0,
            0,
            0});
            this.nudCmdTimeout.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudCmdTimeout.Name = "nudCmdTimeout";
            this.nudCmdTimeout.Size = new System.Drawing.Size(75, 20);
            this.nudCmdTimeout.TabIndex = 22;
            this.nudCmdTimeout.Value = global::DRView.Properties.Settings.Default.dbCommandTimeoutSec;
            // 
            // lbTimeSampleGraph
            // 
            this.lbTimeSampleGraph.AutoSize = true;
            this.lbTimeSampleGraph.Location = new System.Drawing.Point(213, 111);
            this.lbTimeSampleGraph.Name = "lbTimeSampleGraph";
            this.lbTimeSampleGraph.Size = new System.Drawing.Size(61, 13);
            this.lbTimeSampleGraph.TabIndex = 21;
            this.lbTimeSampleGraph.Text = "timeSample";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(171, 111);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Sample:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(171, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Time format in graph";
            // 
            // tbTimeFormatGraph
            // 
            this.tbTimeFormatGraph.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::DRView.Properties.Settings.Default, "timeFormatGraph", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbTimeFormatGraph.Location = new System.Drawing.Point(174, 88);
            this.tbTimeFormatGraph.Name = "tbTimeFormatGraph";
            this.tbTimeFormatGraph.Size = new System.Drawing.Size(153, 20);
            this.tbTimeFormatGraph.TabIndex = 18;
            this.tbTimeFormatGraph.Text = global::DRView.Properties.Settings.Default.timeFormatGraph;
            this.tbTimeFormatGraph.TextChanged += new System.EventHandler(this.tbTimeFormatGraph_TextChanged);
            // 
            // chbCompRecTypeCount
            // 
            this.chbCompRecTypeCount.AutoSize = true;
            this.chbCompRecTypeCount.Checked = global::DRView.Properties.Settings.Default.computeTypeCount;
            this.chbCompRecTypeCount.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbCompRecTypeCount.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::DRView.Properties.Settings.Default, "computeTypeCount", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chbCompRecTypeCount.Location = new System.Drawing.Point(7, 181);
            this.chbCompRecTypeCount.Name = "chbCompRecTypeCount";
            this.chbCompRecTypeCount.Size = new System.Drawing.Size(160, 17);
            this.chbCompRecTypeCount.TabIndex = 17;
            this.chbCompRecTypeCount.Text = "Compute sum by record type";
            this.chbCompRecTypeCount.UseVisualStyleBackColor = true;
            // 
            // chbShowSystemMessages
            // 
            this.chbShowSystemMessages.AutoSize = true;
            this.chbShowSystemMessages.Checked = global::DRView.Properties.Settings.Default.showSystemMessages;
            this.chbShowSystemMessages.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbShowSystemMessages.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::DRView.Properties.Settings.Default, "showSystemMessages", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chbShowSystemMessages.Location = new System.Drawing.Point(7, 49);
            this.chbShowSystemMessages.Name = "chbShowSystemMessages";
            this.chbShowSystemMessages.Size = new System.Drawing.Size(138, 17);
            this.chbShowSystemMessages.TabIndex = 16;
            this.chbShowSystemMessages.Text = "Show system messages";
            this.chbShowSystemMessages.UseVisualStyleBackColor = true;
            // 
            // chbUseLocTime
            // 
            this.chbUseLocTime.AutoSize = true;
            this.chbUseLocTime.Checked = global::DRView.Properties.Settings.Default.useLocalTime;
            this.chbUseLocTime.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbUseLocTime.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::DRView.Properties.Settings.Default, "useLocalTime", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chbUseLocTime.Location = new System.Drawing.Point(55, 150);
            this.chbUseLocTime.Name = "chbUseLocTime";
            this.chbUseLocTime.Size = new System.Drawing.Size(92, 17);
            this.chbUseLocTime.TabIndex = 15;
            this.chbUseLocTime.Text = "Use local time";
            this.chbUseLocTime.UseVisualStyleBackColor = true;
            this.chbUseLocTime.CheckedChanged += new System.EventHandler(this.chbUseLocTime_CheckedChanged);
            // 
            // tbTimeFromat
            // 
            this.tbTimeFromat.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::DRView.Properties.Settings.Default, "timeFormat", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbTimeFromat.Location = new System.Drawing.Point(7, 88);
            this.tbTimeFromat.Name = "tbTimeFromat";
            this.tbTimeFromat.Size = new System.Drawing.Size(153, 20);
            this.tbTimeFromat.TabIndex = 10;
            this.tbTimeFromat.Text = global::DRView.Properties.Settings.Default.timeFormat;
            this.tbTimeFromat.TextChanged += new System.EventHandler(this.tbTimeFromat_TextChanged);
            // 
            // nudTimeOffset
            // 
            this.nudTimeOffset.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::DRView.Properties.Settings.Default, "timeOffset", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.nudTimeOffset.Location = new System.Drawing.Point(7, 147);
            this.nudTimeOffset.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.nudTimeOffset.Minimum = new decimal(new int[] {
            12,
            0,
            0,
            -2147483648});
            this.nudTimeOffset.Name = "nudTimeOffset";
            this.nudTimeOffset.Size = new System.Drawing.Size(42, 20);
            this.nudTimeOffset.TabIndex = 14;
            this.nudTimeOffset.Value = global::DRView.Properties.Settings.Default.timeOffset;
            // 
            // nudMaxRecords
            // 
            this.nudMaxRecords.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::DRView.Properties.Settings.Default, "maxRecorCount", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.nudMaxRecords.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudMaxRecords.Location = new System.Drawing.Point(6, 23);
            this.nudMaxRecords.Maximum = new decimal(new int[] {
            500000,
            0,
            0,
            0});
            this.nudMaxRecords.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudMaxRecords.Name = "nudMaxRecords";
            this.nudMaxRecords.Size = new System.Drawing.Size(75, 20);
            this.nudMaxRecords.TabIndex = 0;
            this.nudMaxRecords.Value = global::DRView.Properties.Settings.Default.maxRecorCount;
            // 
            // tpDBConnection
            // 
            this.tpDBConnection.Controls.Add(this.chbAskForDBConn);
            this.tpDBConnection.Controls.Add(this.gbDBConn2Params);
            this.tpDBConnection.Controls.Add(this.gbDBConn1Params);
            this.tpDBConnection.Controls.Add(this.chbUseDBConnection);
            this.tpDBConnection.Location = new System.Drawing.Point(4, 22);
            this.tpDBConnection.Name = "tpDBConnection";
            this.tpDBConnection.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tpDBConnection.Size = new System.Drawing.Size(334, 253);
            this.tpDBConnection.TabIndex = 1;
            this.tpDBConnection.Text = "DB Connection";
            this.tpDBConnection.UseVisualStyleBackColor = true;
            // 
            // chbAskForDBConn
            // 
            this.chbAskForDBConn.AutoSize = true;
            this.chbAskForDBConn.Checked = global::DRView.Properties.Settings.Default.askForDBConn;
            this.chbAskForDBConn.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::DRView.Properties.Settings.Default, "askForDBConn", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chbAskForDBConn.Location = new System.Drawing.Point(179, 5);
            this.chbAskForDBConn.Name = "chbAskForDBConn";
            this.chbAskForDBConn.Size = new System.Drawing.Size(150, 17);
            this.chbAskForDBConn.TabIndex = 12;
            this.chbAskForDBConn.Text = "Always ask for connection";
            this.chbAskForDBConn.UseVisualStyleBackColor = true;
            // 
            // gbDBConn2Params
            // 
            this.gbDBConn2Params.Controls.Add(this.label2);
            this.gbDBConn2Params.Controls.Add(this.chbDefConn2);
            this.gbDBConn2Params.Controls.Add(this.label13);
            this.gbDBConn2Params.Controls.Add(this.label14);
            this.gbDBConn2Params.Controls.Add(this.label15);
            this.gbDBConn2Params.Controls.Add(this.tbDBName2);
            this.gbDBConn2Params.Controls.Add(this.tbDBServerName2);
            this.gbDBConn2Params.Controls.Add(this.tbDBPassword2);
            this.gbDBConn2Params.Controls.Add(this.tbDBUser2);
            this.gbDBConn2Params.Controls.Add(this.rbDBSqlAuth2);
            this.gbDBConn2Params.Controls.Add(this.rbDBWinAuth2);
            this.gbDBConn2Params.Location = new System.Drawing.Point(179, 28);
            this.gbDBConn2Params.Name = "gbDBConn2Params";
            this.gbDBConn2Params.Size = new System.Drawing.Size(148, 219);
            this.gbDBConn2Params.TabIndex = 11;
            this.gbDBConn2Params.TabStop = false;
            this.gbDBConn2Params.Text = "Second";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Database name";
            // 
            // chbDefConn2
            // 
            this.chbDefConn2.AutoSize = true;
            this.chbDefConn2.BackColor = System.Drawing.Color.Transparent;
            this.chbDefConn2.Location = new System.Drawing.Point(84, 10);
            this.chbDefConn2.Name = "chbDefConn2";
            this.chbDefConn2.Size = new System.Drawing.Size(58, 17);
            this.chbDefConn2.TabIndex = 9;
            this.chbDefConn2.Text = "default";
            this.chbDefConn2.UseVisualStyleBackColor = false;
            this.chbDefConn2.CheckedChanged += new System.EventHandler(this.chbDefConn2_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 14);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "Server name";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(16, 134);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "User name";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 176);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 13);
            this.label15.TabIndex = 5;
            this.label15.Text = "Password";
            // 
            // tbDBName2
            // 
            this.tbDBName2.Location = new System.Drawing.Point(6, 68);
            this.tbDBName2.Name = "tbDBName2";
            this.tbDBName2.Size = new System.Drawing.Size(136, 20);
            this.tbDBName2.TabIndex = 4;
            this.tbDBName2.Text = "CDDB";
            // 
            // tbDBServerName2
            // 
            this.tbDBServerName2.Location = new System.Drawing.Point(6, 30);
            this.tbDBServerName2.Name = "tbDBServerName2";
            this.tbDBServerName2.Size = new System.Drawing.Size(136, 20);
            this.tbDBServerName2.TabIndex = 4;
            // 
            // tbDBPassword2
            // 
            this.tbDBPassword2.Location = new System.Drawing.Point(19, 192);
            this.tbDBPassword2.Name = "tbDBPassword2";
            this.tbDBPassword2.PasswordChar = '*';
            this.tbDBPassword2.Size = new System.Drawing.Size(123, 20);
            this.tbDBPassword2.TabIndex = 3;
            this.tbDBPassword2.UseSystemPasswordChar = true;
            // 
            // tbDBUser2
            // 
            this.tbDBUser2.Location = new System.Drawing.Point(19, 150);
            this.tbDBUser2.Name = "tbDBUser2";
            this.tbDBUser2.Size = new System.Drawing.Size(123, 20);
            this.tbDBUser2.TabIndex = 2;
            // 
            // rbDBSqlAuth2
            // 
            this.rbDBSqlAuth2.AutoSize = true;
            this.rbDBSqlAuth2.Location = new System.Drawing.Point(6, 114);
            this.rbDBSqlAuth2.Name = "rbDBSqlAuth2";
            this.rbDBSqlAuth2.Size = new System.Drawing.Size(116, 17);
            this.rbDBSqlAuth2.TabIndex = 1;
            this.rbDBSqlAuth2.Text = "SQL authentication";
            this.rbDBSqlAuth2.UseVisualStyleBackColor = true;
            this.rbDBSqlAuth2.CheckedChanged += new System.EventHandler(this.rbDBSqlAuth2_CheckedChanged);
            // 
            // rbDBWinAuth2
            // 
            this.rbDBWinAuth2.AutoSize = true;
            this.rbDBWinAuth2.Checked = true;
            this.rbDBWinAuth2.Location = new System.Drawing.Point(6, 95);
            this.rbDBWinAuth2.Name = "rbDBWinAuth2";
            this.rbDBWinAuth2.Size = new System.Drawing.Size(144, 17);
            this.rbDBWinAuth2.TabIndex = 0;
            this.rbDBWinAuth2.TabStop = true;
            this.rbDBWinAuth2.Text = "Windows authentification";
            this.rbDBWinAuth2.UseVisualStyleBackColor = true;
            // 
            // gbDBConn1Params
            // 
            this.gbDBConn1Params.Controls.Add(this.label1);
            this.gbDBConn1Params.Controls.Add(this.chbDefConn1);
            this.gbDBConn1Params.Controls.Add(this.label12);
            this.gbDBConn1Params.Controls.Add(this.label11);
            this.gbDBConn1Params.Controls.Add(this.label10);
            this.gbDBConn1Params.Controls.Add(this.tbDBName1);
            this.gbDBConn1Params.Controls.Add(this.tbDBServerName1);
            this.gbDBConn1Params.Controls.Add(this.tbDBPassword1);
            this.gbDBConn1Params.Controls.Add(this.tbDBUser1);
            this.gbDBConn1Params.Controls.Add(this.rbDBSqlAuth1);
            this.gbDBConn1Params.Controls.Add(this.rbDBWinAuth1);
            this.gbDBConn1Params.Location = new System.Drawing.Point(7, 28);
            this.gbDBConn1Params.Name = "gbDBConn1Params";
            this.gbDBConn1Params.Size = new System.Drawing.Size(148, 219);
            this.gbDBConn1Params.TabIndex = 8;
            this.gbDBConn1Params.TabStop = false;
            this.gbDBConn1Params.Text = "First";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Database name";
            // 
            // chbDefConn1
            // 
            this.chbDefConn1.AutoSize = true;
            this.chbDefConn1.BackColor = System.Drawing.Color.Transparent;
            this.chbDefConn1.Location = new System.Drawing.Point(84, 10);
            this.chbDefConn1.Name = "chbDefConn1";
            this.chbDefConn1.Size = new System.Drawing.Size(58, 17);
            this.chbDefConn1.TabIndex = 9;
            this.chbDefConn1.Text = "default";
            this.chbDefConn1.UseVisualStyleBackColor = false;
            this.chbDefConn1.CheckedChanged += new System.EventHandler(this.chbDefConn1_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 14);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "Server name";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 134);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "User name";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 176);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "Password";
            // 
            // tbDBName1
            // 
            this.tbDBName1.Location = new System.Drawing.Point(6, 68);
            this.tbDBName1.Name = "tbDBName1";
            this.tbDBName1.Size = new System.Drawing.Size(136, 20);
            this.tbDBName1.TabIndex = 4;
            this.tbDBName1.Text = "CDDB";
            // 
            // tbDBServerName1
            // 
            this.tbDBServerName1.Location = new System.Drawing.Point(6, 30);
            this.tbDBServerName1.Name = "tbDBServerName1";
            this.tbDBServerName1.Size = new System.Drawing.Size(136, 20);
            this.tbDBServerName1.TabIndex = 4;
            // 
            // tbDBPassword1
            // 
            this.tbDBPassword1.Location = new System.Drawing.Point(19, 192);
            this.tbDBPassword1.Name = "tbDBPassword1";
            this.tbDBPassword1.PasswordChar = '*';
            this.tbDBPassword1.Size = new System.Drawing.Size(123, 20);
            this.tbDBPassword1.TabIndex = 3;
            this.tbDBPassword1.UseSystemPasswordChar = true;
            // 
            // tbDBUser1
            // 
            this.tbDBUser1.Location = new System.Drawing.Point(19, 150);
            this.tbDBUser1.Name = "tbDBUser1";
            this.tbDBUser1.Size = new System.Drawing.Size(123, 20);
            this.tbDBUser1.TabIndex = 2;
            // 
            // rbDBSqlAuth1
            // 
            this.rbDBSqlAuth1.AutoSize = true;
            this.rbDBSqlAuth1.Location = new System.Drawing.Point(6, 114);
            this.rbDBSqlAuth1.Name = "rbDBSqlAuth1";
            this.rbDBSqlAuth1.Size = new System.Drawing.Size(116, 17);
            this.rbDBSqlAuth1.TabIndex = 1;
            this.rbDBSqlAuth1.Text = "SQL authentication";
            this.rbDBSqlAuth1.UseVisualStyleBackColor = true;
            this.rbDBSqlAuth1.CheckedChanged += new System.EventHandler(this.rbDBSqlAuth1_CheckedChanged);
            // 
            // rbDBWinAuth1
            // 
            this.rbDBWinAuth1.AutoSize = true;
            this.rbDBWinAuth1.Checked = true;
            this.rbDBWinAuth1.Location = new System.Drawing.Point(6, 95);
            this.rbDBWinAuth1.Name = "rbDBWinAuth1";
            this.rbDBWinAuth1.Size = new System.Drawing.Size(144, 17);
            this.rbDBWinAuth1.TabIndex = 0;
            this.rbDBWinAuth1.TabStop = true;
            this.rbDBWinAuth1.Text = "Windows authentification";
            this.rbDBWinAuth1.UseVisualStyleBackColor = true;
            // 
            // chbUseDBConnection
            // 
            this.chbUseDBConnection.AutoSize = true;
            this.chbUseDBConnection.Location = new System.Drawing.Point(7, 6);
            this.chbUseDBConnection.Name = "chbUseDBConnection";
            this.chbUseDBConnection.Size = new System.Drawing.Size(148, 17);
            this.chbUseDBConnection.TabIndex = 7;
            this.chbUseDBConnection.Text = "Use database connection";
            this.chbUseDBConnection.UseVisualStyleBackColor = true;
            this.chbUseDBConnection.CheckedChanged += new System.EventHandler(this.chbUseDBConnection_CheckedChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.checkBox4);
            this.tabPage1.Controls.Add(this.checkBox3);
            this.tabPage1.Controls.Add(this.tbRuntimeObjsDir);
            this.tabPage1.Controls.Add(this.checkBox2);
            this.tabPage1.Controls.Add(this.btBrowseRuntimeObjDir);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.checkBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(334, 253);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Diagnostic Data Descriptors";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Checked = global::DRView.Properties.Settings.Default.saveRuntimeObjOriginDBOnly;
            this.checkBox4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox4.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::DRView.Properties.Settings.Default, "saveRuntimeObjOriginDBOnly", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBox4.DataBindings.Add(new System.Windows.Forms.Binding("Enabled", global::DRView.Properties.Settings.Default, "saveRuntimeObjOnExit", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBox4.Enabled = global::DRView.Properties.Settings.Default.saveRuntimeObjOnExit;
            this.checkBox4.Location = new System.Drawing.Point(23, 28);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(181, 17);
            this.checkBox4.TabIndex = 11;
            this.checkBox4.Text = "Save only DDDs loaded from DB";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Checked = global::DRView.Properties.Settings.Default.loadRuntimeObjOnStart;
            this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox3.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::DRView.Properties.Settings.Default, "loadRuntimeObjOnStart", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBox3.Location = new System.Drawing.Point(6, 49);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(174, 17);
            this.checkBox3.TabIndex = 10;
            this.checkBox3.Text = "Load DDDs on application start";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // tbRuntimeObjsDir
            // 
            this.tbRuntimeObjsDir.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::DRView.Properties.Settings.Default, "runtimeDDDObjDir", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbRuntimeObjsDir.Location = new System.Drawing.Point(6, 87);
            this.tbRuntimeObjsDir.Name = "tbRuntimeObjsDir";
            this.tbRuntimeObjsDir.Size = new System.Drawing.Size(253, 20);
            this.tbRuntimeObjsDir.TabIndex = 0;
            this.tbRuntimeObjsDir.Text = global::DRView.Properties.Settings.Default.runtimeDDDObjDir;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Checked = global::DRView.Properties.Settings.Default.preferRuntimeObjFromDB;
            this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox2.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::DRView.Properties.Settings.Default, "preferRuntimeObjFromDB", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBox2.Location = new System.Drawing.Point(6, 113);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(127, 17);
            this.checkBox2.TabIndex = 9;
            this.checkBox2.Text = "Prefer DDDs from DB";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = global::DRView.Properties.Settings.Default.saveRuntimeObjOnExit;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::DRView.Properties.Settings.Default, "saveRuntimeObjOnExit", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBox1.Location = new System.Drawing.Point(6, 7);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(171, 17);
            this.checkBox1.TabIndex = 8;
            this.checkBox1.Text = "Save DDDs on application exit";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // tpFileAssociations
            // 
            this.tpFileAssociations.Controls.Add(this.btSaveAssoc);
            this.tpFileAssociations.Controls.Add(this.chbAssocSTATQ);
            this.tpFileAssociations.Controls.Add(this.chbAssocDRSTAT);
            this.tpFileAssociations.Controls.Add(this.chbAssocDRQ);
            this.tpFileAssociations.Controls.Add(this.chbAssocXGZ);
            this.tpFileAssociations.Controls.Add(this.chbAssocDR);
            this.tpFileAssociations.Controls.Add(this.label17);
            this.tpFileAssociations.Location = new System.Drawing.Point(4, 22);
            this.tpFileAssociations.Name = "tpFileAssociations";
            this.tpFileAssociations.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tpFileAssociations.Size = new System.Drawing.Size(334, 253);
            this.tpFileAssociations.TabIndex = 4;
            this.tpFileAssociations.Text = "File associations";
            this.tpFileAssociations.UseVisualStyleBackColor = true;
            // 
            // btSaveAssoc
            // 
            this.btSaveAssoc.Location = new System.Drawing.Point(8, 155);
            this.btSaveAssoc.Name = "btSaveAssoc";
            this.btSaveAssoc.Size = new System.Drawing.Size(75, 23);
            this.btSaveAssoc.TabIndex = 6;
            this.btSaveAssoc.Text = "Save associations";
            this.btSaveAssoc.UseVisualStyleBackColor = true;
            this.btSaveAssoc.Click += new System.EventHandler(this.btSaveAssoc_Click);
            // 
            // chbAssocSTATQ
            // 
            this.chbAssocSTATQ.AutoSize = true;
            this.chbAssocSTATQ.Location = new System.Drawing.Point(8, 132);
            this.chbAssocSTATQ.Name = "chbAssocSTATQ";
            this.chbAssocSTATQ.Size = new System.Drawing.Size(143, 17);
            this.chbAssocSTATQ.TabIndex = 5;
            this.chbAssocSTATQ.Text = ".statq - Statistical queries";
            this.chbAssocSTATQ.UseVisualStyleBackColor = true;
            // 
            // chbAssocDRSTAT
            // 
            this.chbAssocDRSTAT.AutoSize = true;
            this.chbAssocDRSTAT.Location = new System.Drawing.Point(8, 109);
            this.chbAssocDRSTAT.Name = "chbAssocDRSTAT";
            this.chbAssocDRSTAT.Size = new System.Drawing.Size(195, 17);
            this.chbAssocDRSTAT.TabIndex = 4;
            this.chbAssocDRSTAT.Text = ".drstat - Diagnostic records statistics";
            this.chbAssocDRSTAT.UseVisualStyleBackColor = true;
            // 
            // chbAssocDRQ
            // 
            this.chbAssocDRQ.AutoSize = true;
            this.chbAssocDRQ.Location = new System.Drawing.Point(8, 86);
            this.chbAssocDRQ.Name = "chbAssocDRQ";
            this.chbAssocDRQ.Size = new System.Drawing.Size(170, 17);
            this.chbAssocDRQ.TabIndex = 3;
            this.chbAssocDRQ.Text = ".drq - Diagnostic records query";
            this.chbAssocDRQ.UseVisualStyleBackColor = true;
            // 
            // chbAssocXGZ
            // 
            this.chbAssocXGZ.AutoSize = true;
            this.chbAssocXGZ.Location = new System.Drawing.Point(8, 63);
            this.chbAssocXGZ.Name = "chbAssocXGZ";
            this.chbAssocXGZ.Size = new System.Drawing.Size(270, 17);
            this.chbAssocXGZ.TabIndex = 2;
            this.chbAssocXGZ.Text = ".xgz - Diagnostic records in compressed XML format";
            this.chbAssocXGZ.UseVisualStyleBackColor = true;
            // 
            // chbAssocDR
            // 
            this.chbAssocDR.AutoSize = true;
            this.chbAssocDR.Location = new System.Drawing.Point(8, 40);
            this.chbAssocDR.Name = "chbAssocDR";
            this.chbAssocDR.Size = new System.Drawing.Size(209, 17);
            this.chbAssocDR.TabIndex = 1;
            this.chbAssocDR.Text = ".dr - Diagnostic records in binary format";
            this.chbAssocDR.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(5, 13);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(303, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Select which files shall be associated with DR View application";
            // 
            // tpImportXMLDiagnostics
            // 
            this.tpImportXMLDiagnostics.Controls.Add(this.btBrowseBaselineDir);
            this.tpImportXMLDiagnostics.Controls.Add(this.tbBaselineDir);
            this.tpImportXMLDiagnostics.Controls.Add(this.label18);
            this.tpImportXMLDiagnostics.Location = new System.Drawing.Point(4, 22);
            this.tpImportXMLDiagnostics.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tpImportXMLDiagnostics.Name = "tpImportXMLDiagnostics";
            this.tpImportXMLDiagnostics.Padding = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.tpImportXMLDiagnostics.Size = new System.Drawing.Size(334, 253);
            this.tpImportXMLDiagnostics.TabIndex = 5;
            this.tpImportXMLDiagnostics.Text = "Import XML Diagnostics";
            this.tpImportXMLDiagnostics.UseVisualStyleBackColor = true;
            // 
            // btBrowseBaselineDir
            // 
            this.btBrowseBaselineDir.Location = new System.Drawing.Point(263, 17);
            this.btBrowseBaselineDir.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btBrowseBaselineDir.Name = "btBrowseBaselineDir";
            this.btBrowseBaselineDir.Size = new System.Drawing.Size(28, 23);
            this.btBrowseBaselineDir.TabIndex = 2;
            this.btBrowseBaselineDir.Text = "...";
            this.btBrowseBaselineDir.UseVisualStyleBackColor = true;
            this.btBrowseBaselineDir.Click += new System.EventHandler(this.btBrowseBaselineDir_Click);
            // 
            // tbBaselineDir
            // 
            this.tbBaselineDir.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbBaselineDir.Location = new System.Drawing.Point(7, 19);
            this.tbBaselineDir.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbBaselineDir.MaximumSize = new System.Drawing.Size(253, 26);
            this.tbBaselineDir.Name = "tbBaselineDir";
            this.tbBaselineDir.Size = new System.Drawing.Size(253, 20);
            this.tbBaselineDir.TabIndex = 1;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Top;
            this.label18.Location = new System.Drawing.Point(7, 6);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 6);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(128, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Directory of Baseline files:";
            // 
            // DRViewHelpProvider
            // 
            this.DRViewHelpProvider.HelpNamespace = "DRView.chm";
            // 
            // persistWindow
            // 
            this.persistWindow.Form = this;
            this.persistWindow.UseLocAppDataDir = true;
            this.persistWindow.XMLFilePath = "DRViewWindowState.xml";
            // 
            // frmProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 308);
            this.Controls.Add(this.tbctrlProperties);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.btSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.DRViewHelpProvider.SetHelpKeyword(this, "Properties.htm");
            this.DRViewHelpProvider.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProperties";
            this.DRViewHelpProvider.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Properties";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmProperties_FormClosed);
            this.Load += new System.EventHandler(this.Properties_Load);
            this.tbctrlProperties.ResumeLayout(false);
            this.tpDiagRecords.ResumeLayout(false);
            this.tpDiagRecords.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCmdTimeout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTimeOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxRecords)).EndInit();
            this.tpDBConnection.ResumeLayout(false);
            this.tpDBConnection.PerformLayout();
            this.gbDBConn2Params.ResumeLayout(false);
            this.gbDBConn2Params.PerformLayout();
            this.gbDBConn1Params.ResumeLayout(false);
            this.gbDBConn1Params.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tpFileAssociations.ResumeLayout(false);
            this.tpFileAssociations.PerformLayout();
            this.tpImportXMLDiagnostics.ResumeLayout(false);
            this.tpImportXMLDiagnostics.PerformLayout();
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btSave;
    private System.Windows.Forms.Button btCancel;
    private System.Windows.Forms.TextBox tbRuntimeObjsDir;
    private System.Windows.Forms.Button btBrowseRuntimeObjDir;
    private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.CheckBox checkBox2;
    private System.Windows.Forms.CheckBox checkBox1;
    private System.Windows.Forms.CheckBox checkBox3;
    private System.Windows.Forms.CheckBox checkBox4;
    private System.Windows.Forms.NumericUpDown nudMaxRecords;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox tbTimeFromat;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label lbTimeSample;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.NumericUpDown nudTimeOffset;
    private System.Windows.Forms.CheckBox chbUseLocTime;
    private System.Windows.Forms.TabControl tbctrlProperties;
    private System.Windows.Forms.TabPage tpDiagRecords;
    private System.Windows.Forms.TabPage tpDBConnection;
    private System.Windows.Forms.TabPage tabPage1;
    private System.Windows.Forms.CheckBox chbShowSystemMessages;
    private System.Windows.Forms.CheckBox chbCompRecTypeCount;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.TextBox tbTimeFormatGraph;
    private System.Windows.Forms.Label lbTimeSampleGraph;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.HelpProvider DRViewHelpProvider;
    private System.Windows.Forms.CheckBox chbUseDBConnection;
    private System.Windows.Forms.GroupBox gbDBConn1Params;
    private System.Windows.Forms.RadioButton rbDBSqlAuth1;
    private System.Windows.Forms.RadioButton rbDBWinAuth1;
    private System.Windows.Forms.TextBox tbDBPassword1;
    private System.Windows.Forms.TextBox tbDBUser1;
    private System.Windows.Forms.TextBox tbDBServerName1;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.CheckBox chbDefConn1;
    private System.Windows.Forms.TextBox tbDBName1;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.GroupBox gbDBConn2Params;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.CheckBox chbDefConn2;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.TextBox tbDBName2;
    private System.Windows.Forms.TextBox tbDBServerName2;
    private System.Windows.Forms.TextBox tbDBPassword2;
    private System.Windows.Forms.TextBox tbDBUser2;
    private System.Windows.Forms.RadioButton rbDBSqlAuth2;
    private System.Windows.Forms.RadioButton rbDBWinAuth2;
    private System.Windows.Forms.CheckBox chbAskForDBConn;
    private System.Windows.Forms.NumericUpDown nudCmdTimeout;
    private System.Windows.Forms.Label label16;
    private Mowog.PersistWindowComponent persistWindow;
    private System.Windows.Forms.TabPage tpFileAssociations;
    private System.Windows.Forms.CheckBox chbAssocXGZ;
    private System.Windows.Forms.CheckBox chbAssocDR;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.CheckBox chbAssocDRQ;
    private System.Windows.Forms.CheckBox chbAssocDRSTAT;
    private System.Windows.Forms.CheckBox chbAssocSTATQ;
    private System.Windows.Forms.Button btSaveAssoc;
        private System.Windows.Forms.TabPage tpImportXMLDiagnostics;
        private System.Windows.Forms.Button btBrowseBaselineDir;
        private System.Windows.Forms.TextBox tbBaselineDir;
        private System.Windows.Forms.Label label18;
    }
}