using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Resources;
using DRViewComponents;
using TDManagementForms;

namespace DRView
{
  public partial class MainForm : Form
  {
    #region Protected members
    private int queryNumber = 1; //Numbering of DR query windows
    private String[] cmdLineArgs; //Arguments recieved from command line
    private XmlDocument compConfigDoc; //Component configuration document
    private ViewConfiguration viewConfiguration; //Configuration of record views
    #endregion //Protected members

    #region Construction and initialization
    /// <summary>
    /// Default mainf form constructor
    /// </summary>
    /// <param name="args">Command line arguments passed to the application</param>
    public MainForm(string[] args)
    {
      InitializeComponent();
      Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
      cmdLineArgs = args;
    }
    #endregion //Construction and initialization

    #region Event handlers
    /// <summary>
    /// Occurs when application starts.
    /// Loads any necessary settings and saved objects.
    /// </summary>
    private void MainForm_Load(object sender, EventArgs e)
    {
      //Load component configuration (configuration in XML document)
      LoadComponentConfiguration();
      //Check Runtime Obj folder
      if (Properties.Settings.Default.runtimeDDDObjDir == "")
        Properties.Settings.Default.runtimeDDDObjDir = Application.StartupPath;
      //Load rutime DB objects if requested
      if (Properties.Settings.Default.loadRuntimeObjOnStart)
      { //try to load previosuly saved objects
        try
        {
          dddHelper.LoadDDDVersionsFromDir(Properties.Settings.Default.runtimeDDDObjDir);
          dddHelper.LoadTUObjectsFromFile(Properties.Settings.Default.runtimeDDDObjDir);
        }
        catch (Exception ex)
        {
          String msg = String.Format(Properties.Resources.msgFailedToLoadRuntimeObjFromDir, Properties.Settings.Default.runtimeDDDObjDir, ex.Message);
          MessageBox.Show(msg, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
      }
      //Set time properties
      SetRuntimeProperties();
      //Load (refresh) TU and Fleet configuration from CDDB
      if (dddHelper.ConnectionString != String.Empty)
      {
        //Try to load TU instances and types
        try
        { 
          dddHelper.LoadTUObjectsFromDB(); 
        }
        catch (Exception ex)
        {
          String msg = String.Format(Properties.Resources.msgFailedToLoadTUObjectsFromDB, ex.Message);
          MessageBox.Show(msg, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        //Try to load fleet hierarchy
        try
        { 
          dddHelper.HierarchyHelper.LoadCurrentHierarchy(); 
        }
        catch (Exception ex)
        {
          String msg = String.Format(Properties.Resources.msgFailedToLoadFleetHierarchyFromDB, ex.Message);
          MessageBox.Show(msg, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
      }
      
      //Process command line arguments
      ParseCmdLineArgs();
    }

    /// <summary>
    /// Occurs when the form closes. 
    /// Save user settings and perform any necessary actions.
    /// </summary>
    private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
    {
      //Save user settings
      Properties.Settings.Default.Save();
      //Save runtime objects of requested
      if (Properties.Settings.Default.saveRuntimeObjOnExit)
      {
        DDDObjects.DDDVersionOrigin verOrig = DDDObjects.DDDVersionOrigin.Unknonwn;
        if (Properties.Settings.Default.saveRuntimeObjOriginDBOnly)
          verOrig = DDDObjects.DDDVersionOrigin.DB;
        dddHelper.SaveAllDDDVersions(Properties.Settings.Default.runtimeDDDObjDir, verOrig);
        dddHelper.SaveTUObjectsToFile(Properties.Settings.Default.runtimeDDDObjDir);
      }
    }

    /// <summary>
    /// Create new query window with appropriate selector
    /// </summary>
    /// <param name="sender">Menu item - determines selector type</param>
    private void NewQueryWindow_Click(object sender, EventArgs e)
    {
      //Determine selector type
      DRQuery.eSelectorType selType = DRQuery.eSelectorType.Basic;
        /*
        if (sender == tUInstanceToolStripMenuItem)
            selType = DRQuery.eSelectorType.Basic;
        else if (sender == advancedQueryToolStripMenuItem)
            selType = DRQuery.eSelectorType.RecParams;
        else if (sender == queryByReferentialAttributesToolStripMenuItem)
            selType = DRQuery.eSelectorType.RefAttributes;
        */
        if (sender == openSavedRecordsToolStripMenuItem)
            selType = DRQuery.eSelectorType.File;
        else if (sender == newTUDirectQueryToolStripMenuItem)
            selType = DRQuery.eSelectorType.TUDirect;
        else if (sender == recordsQueryToolStripMenuItem)
            selType = DRQuery.eSelectorType.DB;
        else if (sender == openDiagnosticsXMLToolStripMenuItem)
            selType = DRQuery.eSelectorType.XMLD;
      try
      {
        //Create and show new query
        CreateNewQuery(selType);
      }
      catch (Exception ex)
      {
        ShowErrorMessage("Failed to open new query window: " + ex.Message);
      }
    }

    private void cascadeToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.LayoutMdi(MdiLayout.Cascade);
    }

    private void titleToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.LayoutMdi(MdiLayout.TileHorizontal);
    }

    private void tileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.LayoutMdi(MdiLayout.TileVertical);
    }

    private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
    {
      //Show about box
      AboutBox aboutForm = new AboutBox(GetDBConnection());
      aboutForm.ShowDialog();
    }

    private void propertiesToolStripMenuItem_Click(object sender, EventArgs e)
    {
      //Show properties form
      frmProperties propForm = new frmProperties();
      propForm.ShowDialog();
      //Update runtime properties
      SetRuntimeProperties();
      //Dispose dialog
      propForm.Dispose();
    }

    /// <summary>
    /// Opens remote file manager for specified target host
    /// </summary>
    private void remoteFileManagerToolStripMenuItem_Click(object sender, EventArgs e)
    {
      //Obtain address of remote host
      String hostAdrr;
      if (UserInput.GetUserInput("Remote host address", out hostAdrr) != DialogResult.OK)
        return; //User cancelled the request
      //Create and display remote file manager
      FileManager rmtFileManager = new FileManager(hostAdrr, Properties.Settings.Default.fileChunkSize);
      rmtFileManager.MdiParent = this;
      rmtFileManager.Show();
    }

    /// <summary>
    /// Creates and displays form for statistical queries
    /// </summary>
    private void statisticalQueryToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (sender == statisticalQueryToolStripMenuItem)
        CreateNewStatisticalQuery(StatsQuery.eStatSelectorType.DB, "");
      else if (sender == openSavedstatisticsToolStripMenuItem)
        CreateNewStatisticalQuery(StatsQuery.eStatSelectorType.File, "");
    }

    /// <summary>
    /// Creates and displays form for querying TU record statistics
    /// </summary>
    private void tuStatisticsToolStripMenuItem_Click(object sender, EventArgs e)
    {
      //obtain DB connection
      SqlConnection dbConnection = GetDBConnection();
      if (dbConnection == null)
      {
        ShowErrorMessage("No database connection specified");
        return;
      }
      //Create and display DRStatistics form
      DRStatistics statsForm = new DRStatistics(dddHelper, dbConnection);
      statsForm.MdiParent = this;
      statsForm.Show();
    }

    private void contentToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Help.ShowHelp(this, "DRView.chm", HelpNavigator.TableOfContents);
    }

    /// <summary>
    /// Opens previously saved query from file
    /// </summary>
    private void openSavedToolStripMenuItem_Click(object sender, EventArgs e)
    {
      try
      {
        //Prepare load file dialog
        dlgOpenFile.Reset();
        dlgOpenFile.DefaultExt = Properties.Resources.DRQFileExtension;
        dlgOpenFile.Filter = Properties.Resources.DRQFilesFilter + "|" + Properties.Resources.StatQFilesFilter;
        //Show file open dialog
        if (dlgOpenFile.ShowDialog() == DialogResult.OK)
          OpenQueryFile(dlgOpenFile.FileName);
      }
      catch (Exception ex)
      {
        ShowErrorMessage("Failed to open saved query: " + ex.Message);
      }
    }
    #endregion //Event handlers

    #region Helper methods
    /// <summary>
    /// Creates new SqlConnection according to settings from configuration file
    /// or as specified by user
    /// </summary>
    /// <returns>SqlConnection object or null if no connection is selected</returns>
    protected SqlConnection GetDBConnection()
    {
      //Obtain connection string using specialised form
      frmDBConnection dbConnForm = new frmDBConnection();
      String connString = dbConnForm.ConnectionString;
      while (connString != String.Empty)
      { //Try to open the connection
        SqlConnection dbConnection = new SqlConnection(connString);
        try
        {
          //Open the connection
          dbConnection.Open();
          //Connection succesfully opened
          dbConnForm.Dispose(); //Dispose the form
          return dbConnection; //Return the created connection
        }
        catch (Exception ex)
        { //Openning of DB connection failed
          //Invalidate connection string in dialog
          frmDBConnection.InvalidateLastConnString(true);
          //Display message box with choice
          String message = String.Format("Connection to the database failed ({0}).\nWould you like to re-enter connection information?", ex.Message);
          DialogResult res = MessageBox.Show(
            message, "Database connection failed", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
          if (res == DialogResult.Yes)
            connString = dbConnForm.ConnectionString; //Retrieve new connection string from dialog
          else
            connString = String.Empty; //No connection specified
        }
      }
      //No connection is selected
      dbConnForm.Dispose(); //Dispose the form
      return null;
    }

    /// <summary>
    /// Returns connection string according to the configuration
    /// or as specified by the user. Empty string is returned, in case of no available DB connection.
    /// </summary>
    /// <returns>Connection string or empty string</returns>
    protected String GetDBConnString()
    {
      //Get connection using standard method
      SqlConnection dbConnection = GetDBConnection();
      if (dbConnection == null)
        return String.Empty; //No connection specified - empty connection string
      else
      { //Connection specified - extract conn string and destroy the connection
        String connString = dbConnection.ConnectionString;
        dbConnection.Close();
        dbConnection.Dispose();
        return connString;
      }
    }

    /// <summary>
    /// Handles all unhandled application exceptions.
    /// Displays message box.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
    {
      String msg;
      msg = String.Format("Fatal error encountered.\nException: {0}", e.Exception.Message);
      if (e.Exception.InnerException != null)
        msg += String.Format("\nInner exception: {0}", e.Exception.InnerException.Message);
      MessageBox.Show(msg, "Fatal error", MessageBoxButtons.OK, MessageBoxIcon.Error);
    }

    /// <summary>
    /// Displays message box with error message
    /// </summary>
    void ShowErrorMessage(String message)
    {
      MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
    }

    /// <summary>
    /// Shows error message according to given exception
    /// </summary>
    void ShowErrorMessage(Exception ex)
    {
      String message = String.Empty;
      Exception tmpEx = ex;
      while (tmpEx != null)
      {
        message += "\n" + tmpEx.Message;
        tmpEx = tmpEx.InnerException;
      }
      ShowErrorMessage(message.Trim());
    }

    /// <summary>
    /// Set properties of runtime objects according tu user Properties
    /// </summary>
    private void SetRuntimeProperties()
    {
      dddHelper.TimeFormat = Properties.Settings.Default.timeFormat;
      dddHelper.TimeFormatGraph = Properties.Settings.Default.timeFormatGraph;
      dddHelper.TimeOffset = (int)Properties.Settings.Default.timeOffset;
      dddHelper.UseLocalTime = Properties.Settings.Default.useLocalTime;
      dddHelper.ConnectionString = GetDBConnString();
      //Enable / disable new query menu
      newToolStripMenuItem.Enabled = ((eDBConnTypes)Properties.Settings.Default.preferredDBConn != eDBConnTypes.None);
    }

    /// <summary>
    /// Creates new query window with given selector type
    /// </summary>
    /// <param name="selectorType">Type of selector to use</param>
    /// <returns>Reference to query window</returns>
    private DRQuery CreateNewQuery(DRQuery.eSelectorType selectorType)
    {
      return CreateNewQuery(selectorType, "");
    }    

    /// <summary>
    /// Creates new query window with given selector type
    /// </summary>
    /// <param name="selectorType">Type of selector to use</param>
    /// <param name="recordFile">Name of file with diagnostic records (for FileSelector only)</param>
    /// <returns>Reference to query window</returns>
    private DRQuery CreateNewQuery(DRQuery.eSelectorType selectorType, String recordFile)
    {
      //Create database connection if necessary
      SqlConnection dbConn = null; 
      if ((selectorType != DRQuery.eSelectorType.File) && (selectorType != DRQuery.eSelectorType.TUDirect && selectorType != DRQuery.eSelectorType.XMLD))
      { //Database connection is necessary
        dbConn = GetDBConnection();
        if (dbConn == null)
        { //Could not obtain databse connection
          throw new Exception("No database connection available");
        }
      }
        
      DRQuery newDRQuery = null;

        if (selectorType != DRQuery.eSelectorType.XMLD)
        {
            //Create query form                
            newDRQuery = new DRQuery(selectorType, dbConn, this.dddHelper, queryNumber, recordFile, viewConfiguration);
        }
        else if (selectorType == DRQuery.eSelectorType.XMLD)
        {
            //the input file is a xml diagnostics that still has to be compiled into a DRView File
            String XMLDiagnosticPath = "";
            do
            {
                //Select Diagnostics file 
                dlgOpenFile.Reset();
                dlgOpenFile.Filter = "All files (*.*)|*.*";
                dlgOpenFile.InitialDirectory = @"C:\";
                if (dlgOpenFile.ShowDialog() == DialogResult.OK)
                    XMLDiagnosticPath = dlgOpenFile.FileName;
                else
                    return null;
            } //Check if selected XML Diagn file is valid: it SHOULD NOT have an extension
            while (XMLDiagnosticPath.Substring(XMLDiagnosticPath.LastIndexOf("_")).LastIndexOf(".") != -1); 

            //Init the parser object
            Parser parser = new Parser();
            parser.filePath = XMLDiagnosticPath;

            parser.Type();

            //searching for the Baseline file

            //Set the basline folder if not set
            if (Properties.Settings.Default.baselineDir == "" || !Directory.Exists(Properties.Settings.Default.baselineDir))
            {
                MessageBox.Show("Selezionare cartella che contiene le baseline...");
                using (FolderBrowserDialog fbd = new FolderBrowserDialog())
                {
                    fbd.SelectedPath = @"C:\";
                    if (fbd.ShowDialog() == DialogResult.OK)
                    {
                        Properties.Settings.Default.baselineDir = fbd.SelectedPath;
                    } else
                    {
                        return null;
                    }
                }
                Properties.Settings.Default.Save();
            }

            string XMLBaselinePath = "";
            string bltmp;
            bool foundLocalBaseline = false;

            string[] listOfBaselines = Directory.GetFiles(Properties.Settings.Default.baselineDir);
            foreach (string bl in listOfBaselines)
            {
                bltmp = bl.Remove(0, Properties.Settings.Default.baselineDir.Length + 1);
                if (bltmp.StartsWith(parser.baselineNameFromDiag))
                    {
                        //Check the version
                        if (bltmp.Substring(parser.baselineNameFromDiag.Length + 15).StartsWith(parser.DiagBlVersion))
                        {
                            //check the extension
                            if (Path.GetExtension(bltmp) == ".xml" || Path.GetExtension(bltmp) == ".csv")
                            {
                                foundLocalBaseline = true;
                                XMLBaselinePath = Properties.Settings.Default.baselineDir + "\\" + bltmp;
                                break;
                            }
                        }
                    }
                    
            }

            if (!foundLocalBaseline)
            {
                MessageBox.Show("File di baseline non trovato. Selezionarne uno.");

                dlgOpenFile.Reset();
                dlgOpenFile.Filter = "XML files (*.xml)|*.xml|Csv files (*.csv)|*.csv";
                dlgOpenFile.InitialDirectory = @"C:\projects\maggio2022\DRView\Input file";
                if (dlgOpenFile.ShowDialog() == DialogResult.OK)
                    XMLBaselinePath = dlgOpenFile.FileName;
                else
                    return null;
            }


            parser.BaselinePath = XMLBaselinePath;


            Cursor.Current = Cursors.WaitCursor;

            //parse the file!
            parser.CheckXML();

            // xml -> xgz
            parser.Compress();

            Cursor.Current = Cursors.Default;

            //Get .xgz path
            string path = parser.docSave.Substring(0, parser.docSave.Length - 4);
            path += ".xgz";

            //new query with drview file 
            newDRQuery = new DRQuery(DRQuery.eSelectorType.File, dbConn, this.dddHelper, queryNumber, path, viewConfiguration);
        }
            

        // Set the parent form of the query form
        newDRQuery.MdiParent = this;
        // Increment query number
        queryNumber++;
        //Set visible views
        newDRQuery.VisibleViews = DRQuery.eVisibleDRViews.MasterSlave;
        // Display the new form. 
        newDRQuery.Show();
        //return form reference
        return newDRQuery;
    }

    /// <summary>
    /// Creates new form for statistical queries with given selector type
    /// </summary>
    /// <param name="selectorType">Type of selector</param>
    /// <param name="statisticFile">Name of file with statistical data (for FileSelector only)</param>
    /// <returns>Reference to query window</returns>
    private StatsQuery CreateNewStatisticalQuery(StatsQuery.eStatSelectorType selectorType, String statisticFile)
    {
      //Create database connection if necessary
      SqlConnection dbConn = null;
      if ((selectorType == StatsQuery.eStatSelectorType.DB))
      { //Database connection is necessary
        dbConn = GetDBConnection();
        if (dbConn == null)
        { //Could not obtain databse connection
          throw new Exception("No database connection available");
        }
      }
      //Create and display DRStatistics form
      StatsQuery statsQueryFrom = new StatsQuery(selectorType, dbConn, dddHelper, queryNumber, statisticFile);
      // Set the parent form of the query form
      statsQueryFrom.MdiParent = this;
      // Increment query number
      queryNumber++;
      //Set visible views
      statsQueryFrom.VisibleViews = StatsQuery.eVisibleStatViews.Table;
      // Display the new form.
      statsQueryFrom.Show();
      //return form reference
      return statsQueryFrom;
    }

    /// <summary>
    /// Method examines all command line arguments and takes appropriate actions based on these arguments
    /// </summary>
    private void ParseCmdLineArgs()
    {
      if (cmdLineArgs == null)
        return; //No command line arguments exist

      //Iterate over all command line arguments
      foreach (String arg in cmdLineArgs)
      {
        //For now, we expect all parameters are file names to open
        try
        {
          String fileName = arg;
          if (!File.Exists(fileName))
            throw new Exception(String.Format("Invalid file name {0} specified as command line argument", fileName));
          else
            switch (Path.GetExtension(fileName).ToLower())
            {
              case ".dr":
              case ".xgz":
                CreateNewQuery(DRQuery.eSelectorType.File, fileName);
                break;
              case ".drstat":
                CreateNewStatisticalQuery(StatsQuery.eStatSelectorType.File, fileName);
                break;
              case ".drq":
              case ".statq":
                OpenQueryFile(fileName);
                break;
              default:
                throw new Exception(String.Format("File {0} has unknown type", fileName));
            }
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.Message, "File open failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
      }
    }

    /// <summary>
    /// Opens query file with given mane
    /// </summary>
    /// <param name="fileName">Name of query file</param>
    private void OpenQueryFile(String fileName)
    {
      //Create formatter
      BinaryFormatter formatter = new BinaryFormatter();
      //Open file stream
      using (FileStream inFile = new FileStream(fileName, FileMode.Open, FileAccess.Read))
      {
        //Determine kind of query - records or statistics
        if (Path.GetExtension(fileName).ToLower() == ".drq")
        {//Diagnostic records query
          //Deserialize selector type
          DRQuery.eSelectorType selType = (DRQuery.eSelectorType)formatter.Deserialize(inFile);
          //Create query form with correct selector type
          DRQuery queryForm = CreateNewQuery(selType);
          //Load query settings
          queryForm.LoadQuerySetting(inFile, formatter);
        }
        else if (Path.GetExtension(fileName).ToLower() == ".statq")
        { //Statistical query
          //Deserialize selector type
          StatsQuery.eStatSelectorType selType = (StatsQuery.eStatSelectorType)formatter.Deserialize(inFile);
          //Create statistical query from with correct selector type
          StatsQuery statQueryForm = CreateNewStatisticalQuery(selType, "");
          //Load query settings
          statQueryForm.LoadQuerySetting(inFile, formatter);
        }
      }
    }

    /// <summary>
    /// Loads configuration from XML document. 
    /// Creates menu items according to given configuration.
    /// </summary>
    private void LoadComponentConfiguration()
    {
      try
      {
        //Create configuration document
        compConfigDoc = new XmlDocument();
        //Load configuration from file
        String cfgFilePath = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), 
          Properties.Settings.Default.compConfigFile);
        compConfigDoc.Load(cfgFilePath);
        //Create view configuration
        viewConfiguration = new ViewConfiguration();
        XmlNodeList viewAttributes = compConfigDoc.DocumentElement.SelectNodes("//ViewAttribute");
        foreach (XmlElement viewAttribute in viewAttributes)
          viewConfiguration.Attributes.Add(
            new ViewConfiguration.ViewAttribute(viewAttribute.GetAttribute("DataField"), 
            viewAttribute.GetAttribute("HeaderText"))
            );
      }
      catch (Exception ex)
      {
        ShowErrorMessage(new Exception("Failed to load configuration", ex));
      }
    }

    /*
    /// <summary>
    /// Creates menu item according to given MenuItem xml element.
    /// Item is created as child of given parent item.
    /// If MenuItem element contains additional MenuItem child elements,
    /// method is called recursively.
    /// </summary>
    /// <param name="itemCfg">Xml configuration element</param>
    /// <param name="parentItem">Parent menu item</param>
    private void CreateMenuItem(XmlElement itemCfg, ToolStripMenuItem parentItem)
    {
      //Create menu item and set attributes
      ToolStripMenuItem menuItem = new ToolStripMenuItem();
      menuItem.Name = itemCfg.GetAttribute("name");
      menuItem.Text = itemCfg.GetAttribute("text");
      menuItem.ToolTipText = itemCfg.GetAttribute("tooltip");
      //Obtain menu item icon from embedded resource
      String iconName = itemCfg.GetAttribute("icon");
      Icon icon = ResourceAccess.GetIcon(iconName);
      if (icon != null)
        menuItem.Image = icon.ToBitmap();
      //Add click handler
      menuItem.Click += new EventHandler(PerformMenuCommand);
      //Add item to parent
      parentItem.DropDownItems.Add(menuItem);
      //Select child items and call recursively
      XmlNodeList childItems = itemCfg.SelectNodes("MenuItem");
      foreach (XmlElement childItem in childItems)
        CreateMenuItem(childItem, menuItem);
    }
    */

    /*
    /// <summary>
    /// Performs command initiated by click on menu item.
    /// Command description is found in component configuration document 
    /// according to the name of menu item.
    /// </summary>
    /// <param name="sender">Menu item which initiated the command</param>
    /// <param name="e">Event arguments</param>
    private void PerformMenuCommand(object sender, EventArgs e)
    {
      try
      {
        //Check type of command sender
        if (sender.GetType() != typeof(ToolStripMenuItem))
          throw new Exception("Invalid type of command sender");
        //Obtain the name of menu item, which initiated the command
        String menuItemName = ((ToolStripMenuItem)sender).Name;
        //Find element defining the command in config document
        String xpathExpr = String.Format("//MenuItem[@name='{0}']/Command", menuItemName);
        XmlElement commandElem = (XmlElement)compConfigDoc.SelectSingleNode(xpathExpr);
        if (commandElem == null)
          throw new Exception("Command not found");
        //Switch action according to command
        String command = commandElem.GetAttribute("cmd");
        switch (command)
        {
          case "OpenForm":
            PerformOpenFormCommand(commandElem);
            break;
          default:
            throw new Exception("Invalid command " + command);
        }
      }
      catch (Exception ex)
      { //Display error
        ShowErrorMessage(ex);
      }
    }
    */

    /*
    /// <summary>
    /// Performs command OpenForm specified by XML element from component configuration file.
    /// </summary>
    /// <param name="commandElement">OpenForm XML element with parameters</param>
    private void PerformOpenFormCommand(XmlElement commandElement)
    {
      try
      {
        //Obtain form type
        Type formType = Type.GetType(commandElement.GetAttribute("type"), true);
        Dictionary<String, Object> constructorParams = CreateParamsDictionary(commandElement);
        //Construct the form
        Form form = (Form)Activator.CreateInstance(formType, constructorParams);
        // Set the parent form
        form.MdiParent = this;
        // Display the new form.
        form.Show();
      }
      catch(Exception ex)
      {
        throw new Exception("Failed to open form", ex);
      }
    }
    */

    /*
    /// <summary>
    /// Method builds dictionary of named parameters which can be passed to consturctors of new forms or components.
    /// Dictionary is build based on the child ConstructorParams/Param elements. 
    /// In cases when parameter value starts with % character, it is replaced by reference to known object, 
    /// otherwise original value string is used as parameter value.
    /// </summary>
    /// <param name="paramNames">Command element with ConstructorParams/Param child elements</param>
    /// <returns>Dictionary of named parameters</returns>
    private Dictionary<String, Object> CreateParamsDictionary(XmlElement commandElement)
    {
      //Create dictionary for named parameters
      Dictionary<String, Object> parameters = new Dictionary<string, object>();
      //Obtain all ConstructorParams/Param child elements
      XmlNodeList parElems = commandElement.SelectNodes("ConstructorParams/Param");
      //Iterate over all param elements
      foreach (XmlElement parElem in parElems)
      { //Obtain parameter name and value
        String parName = parElem.GetAttribute("name");
        String value = parElem.GetAttribute("value");
        //Add parameter to dictionary - replace known values with object references
        switch (value)
        { //switch by parameter value
          case "%dbConnection":
            parameters.Add(parName, GetDBConnection());
            break;
          case "%dddHelper":
            parameters.Add(parName, dddHelper);
            break;
          case "%queryNumber":
            parameters.Add(parName, queryNumber++);
            break;
          case "%recordFile":
            parameters.Add(parName, String.Empty);
            break;
          case "%element":
            XmlElement elem = (XmlElement)commandElement.SelectSingleNode(parElem.GetAttribute("path"));
            parameters.Add(parName, elem);
            break;
          default:
            parameters.Add(parName, value); //Use original string value
            break;
        }
      }
      //Return named parameters dictionary
      return parameters;
    }
    */
    #endregion //Helper methods
  }

}