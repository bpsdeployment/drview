using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using DDDObjects;
using DRSelectorComponents;
using DRViewComponents;
using DRViewComponents.VarTreeNodes;
using DSDiagnosticRecords;


namespace DRView
{
  /// <summary>
  /// Represents dialog specifying filter conditions for selected diagnostic records
  /// Filter is than applied to selector data views
  /// </summary>
  public partial class FilterDialog : Form
  {
    #region Static members
    private static String NumTbTemplate = "0123456789, ";
    #endregion

    #region Members
    public DDDHelper DDDHelper;
    public DRSelectorBase Selector;
    /// <summary>
    /// Attach this handler to the source of NewDataReady event (Selector component)
    /// </summary>
    public NDREventHandler NewDataReadyHandler;
    #endregion //Members

    #region Public properties
    /// <summary>
    /// Filter string for records
    /// </summary>
    public String RecordFilter
    {
      get { return tbRecFilterStr.Text; }
    }

    /// <summary>
    /// Dictionary with selected variables
    /// </summary>
    public VarIDDictionary SelectedVariables
    {
      get { return varTreeView.GetSelectedVarIDs(); }
    }
    #endregion //Public properties

    #region Public Events
    /// <summary>
    /// Event occurs when there is an information message to pass to component owner
    /// </summary>
    public event InfoMessageEventhandler InfoMessage;
    /// <summary>
    /// Event occurs when there is an error encountered during selection process
    /// </summary>
    public event ErrorMessageEventhandler ErrorMessage;
    /// <summary>
    /// Event occurs when component needs to report progress of running operation
    /// </summary>
    public event ReportProgressEventHandler ProgressReport;
    #endregion //Public Events

    #region Construction initialization
    /// <summary>
    /// Dialog constructor, stores passed selector and helper references
    /// </summary>
    public FilterDialog(DRSelectorBase selector, DDDHelper dddHelper)
    {
      InitializeComponent();
      this.DDDHelper = dddHelper;
      this.Selector = selector;
      varTreeView.DDDHelper = dddHelper;
      //Initialize public delegate
      NewDataReadyHandler = OnNewDataReady;
    }
    #endregion //Construction initialization

    #region Event raisers
    /// <summary>
    /// Method raises InfoMessage event
    /// </summary>
    protected void OnInfoMessage(String message)
    {
      if (InfoMessage != null)
        InfoMessage(this, message);
    }

    /// <summary>
    /// Raises ErrorMessage event
    /// </summary>
    /// <param name="error">Exception describing the error</param>
    protected void OnErrorMessage(Exception error)
    {
      if (ErrorMessage != null)
        ErrorMessage(this, error);
    }

    /// <summary>
    /// Method raises InfoMessage event
    /// </summary>
    protected void OnInfoMessage(String message, params Object[] args)
    {
      if (InfoMessage != null)
        InfoMessage(this, String.Format(message, args));
    }

    /// <summary>
    /// Method raises Report progress event
    /// </summary>
    /// <param name="progress">reported progress</param>
    /// <param name="barVisible">visibility of progress bar</param>
    protected void OnReportProgress(int progress, bool barVisible)
    {
      if (ProgressReport != null)
        ProgressReport(this, progress, barVisible);
    }
    #endregion //Event raisers

    #region Helper methods
    /// <summary>
    /// Creates filter string for records view
    /// based on values of visual components
    /// </summary>
    /// <returns>Created filter string</returns>
    private String BuildRecFilter()
    {
      String filter = "";
      String tmpFilter = "";
      //Add parts of filter string based on control values
      //Record title
      tmpFilter = String.Empty;
      foreach (String keyword in tbTitleKeywords.Text.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries))
      {
        if (chbMatchAllKeywords.Checked)
          tmpFilter = ANDString(tmpFilter, String.Format("RecTitle LIKE '%{0}%'", keyword));
        else
          tmpFilter = ORString(tmpFilter, String.Format("RecTitle LIKE '%{0}%'", keyword));
      }
      filter = ANDString(filter, tmpFilter);
      //Times
      if (dtpStartFrom.Checked)
        filter = ANDString(filter, String.Format("StartTime >= #{0}#", dtpStartFrom.Text.Trim()));
      if (dtpStartTo.Checked)
        filter = ANDString(filter, String.Format("StartTime <= #{0}#", dtpStartTo.Text.Trim()));
      //Record type
      tmpFilter = String.Empty;
      if (chbEventRec.Checked)
        tmpFilter = ORString(tmpFilter, "RecordType = 'event'");
      if (chbTraceRec.Checked)
        tmpFilter = ORString(tmpFilter, "RecordType = 'trace'");
      if (chbSnapRec.Checked)
        tmpFilter = ORString(tmpFilter, "RecordType = 'snap'");
      filter = ANDString(filter, tmpFilter);
      //Record sources
      tmpFilter = String.Empty;
      foreach (String keyword in tbRecSources.Text.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries))
        tmpFilter = ORString(tmpFilter, String.Format("Source LIKE '%{0}%'", keyword));
      filter = ANDString(filter, tmpFilter);
      //Vehicle numbers
      tmpFilter = String.Join(", ", tbVehicleNumbers.Text.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries));
      if (tmpFilter != "")
      {
        tmpFilter = String.Format("VehicleNumber IN ({0})", tmpFilter);
        filter = ANDString(filter, tmpFilter);
      }
      //Fault codes
      tmpFilter = String.Join(", ", tbFaultCodes.Text.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries));
      if (tmpFilter != String.Empty)
      {
        tmpFilter = String.Format("FaultCode IN ({0})", tmpFilter);
        filter = ANDString(filter, tmpFilter);
      }
      //Trace or Snap codes
      tmpFilter = String.Join(", ", tbTrcSnpCodes.Text.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries));
      if (tmpFilter != "")
      {
        tmpFilter = String.Format("TrcSnpCode IN ({0})", tmpFilter);
        filter = ANDString(filter, tmpFilter);
      }
      //Fault severities
      tmpFilter = String.Empty;
      if (chbSevA1.Checked)
        tmpFilter = ORString(tmpFilter, "FaultSeverity = 'A1'");
      if (chbSevA.Checked)
        tmpFilter = ORString(tmpFilter, "FaultSeverity = 'A'");
      if (chbSevB1.Checked)
        tmpFilter = ORString(tmpFilter, "FaultSeverity = 'B1'");
      if (chbSevB.Checked)
        tmpFilter = ORString(tmpFilter, "FaultSeverity = 'B'");
      if (chbSevC.Checked)
        tmpFilter = ORString(tmpFilter, "FaultSeverity = 'C'");
      filter = ANDString(filter, tmpFilter);
      //Fault subseverities
      tmpFilter = String.Join(", ", tbFaultSubseverities.Text.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries));
      if (tmpFilter != String.Empty)
      {
        tmpFilter = String.Format("FaultSubSeverity IN ({0})", tmpFilter);
        filter = ANDString(filter, tmpFilter);
      }
      //DDD Versions
      tmpFilter = String.Empty;
      foreach (String keyword in tbDDDVersions.Text.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries))
        tmpFilter = ORString(tmpFilter, String.Format("DDDVersionName LIKE '%{0}%'", keyword));
      filter = ANDString(filter, tmpFilter);
      //TU Names
      tmpFilter = String.Empty;
      foreach (String keyword in tbTUNames.Text.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries))
        tmpFilter = ORString(tmpFilter, String.Format("TUInstanceName LIKE '%{0}%'", keyword));
      filter = ANDString(filter, tmpFilter);
      //Fleet hierarchy IDs
      List<int> itemIDs = fleetHierarchy.GetSelectedIDs();
      if (itemIDs.Count > 0)
      {
        String[] strIDs = new String[itemIDs.Count];
        for(int i = 0; i < itemIDs.Count; i++)
          strIDs[i] = itemIDs[i].ToString();
        tmpFilter = String.Join(", ", strIDs);
        tmpFilter = String.Format("HierarchyItemID IN ({0})", tmpFilter);
        filter = ANDString(filter, tmpFilter);
      }
      //Return filter string
      return filter;
    }

    private String ORString(String part1, String part2)
    {
      if (part2 == "")
        return part1;
      if (part1 == "")
        return part2;
      return (part1 + " OR " + part2);
    }

    private String ANDString(String part1, String part2)
    {
      if (part2 == "")
        return part1;
      if (part1 == "")
        return "(" + part2 + ")";
      return (part1 + " AND (" + part2 + ")");
    }
    #endregion //Helper methods

    #region Event handlers
    /// <summary>
    /// Occurs whenever user changes value of any control in Records group box
    /// </summary>
    private void FilterChanged(object sender, EventArgs e)
    {
      tbRecFilterStr.Text = BuildRecFilter();
    }

    /// <summary>
    /// Enables / disables group boxes with controls
    /// </summary>
    private void chbModifRecFilter_CheckedChanged(object sender, EventArgs e)
    {
      tbRecFilterStr.Enabled = chbModifyFilters.Checked;
      gbRecords.Enabled = !chbModifyFilters.Checked;
    }

    /// <summary>
    /// Method handles NewDataReady event raised by SelectorComponent.
    /// Displays tree of variables for available records.
    /// Displays tree of hierarchy items for available records.
    /// </summary>
    protected virtual void OnNewDataReady(DRSelectorBase sender, NDREventArgs args)
    {
      try
      {
        Selector = sender;
        //Create trees only if not filtered
        if (!args.Filtered)
        {
          //Variable tree
          varTreeView.CreateVarTree(Selector.RecordsView, null);
          varTreeView.CheckAll();
          try
          { //Show hierarchy tree if defined
            if (DDDHelper.HierarchyHelper.GetCurrentFleetHierarchy() != null || 
              DDDHelper.IsDBConnectionSpecified())
              fleetHierarchy.ShowCurrentHierarchy(DDDHelper.HierarchyHelper, Selector.RecordsView);
          }
          catch (Exception ex)
          {
            OnErrorMessage(new Exception("Failed to show current hierarchy in filter dialog", ex));
          }
        }
      }
      catch (Exception ex)
      {
        OnErrorMessage(new Exception("Failed to handle new data in filter dialog", ex));
      }
    }

    /// <summary>
    /// Expand nodes in trees
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void FilterDialog_Shown(object sender, EventArgs e)
    {
      varTreeView.PartExpand();
      fleetHierarchy.ExpandCheckedNodes();
    }

    private void tsbLoadFilter_Click(object sender, EventArgs e)
    {
      try
      {
        if (openFileDialog.ShowDialog() == DialogResult.OK)
        {
          //Open file for reading
          FileStream file = new FileStream(openFileDialog.FileName, FileMode.Open, FileAccess.Read);
          //Create binary formatter for object serialization
          BinaryFormatter formatter = new BinaryFormatter();
          //Load filter status
          LoadFromFile(file, formatter);
          //Close file
          file.Close();
          file.Dispose();
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show("Failed to load filter from file. " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void tsbSaveFilter_Click(object sender, EventArgs e)
    {
      try
      {
        if (saveFileDialog.ShowDialog() == DialogResult.OK)
        {
          //Open file for writing
          FileStream file = new FileStream(saveFileDialog.FileName, FileMode.Create, FileAccess.Write);
          //Create binary formatter for object serialization
          BinaryFormatter formatter = new BinaryFormatter();
          //Save filter status
          SaveToFile(file, formatter);
          //Close file
          file.Close();
          file.Dispose();
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show("Failed to save filter to file. " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void fleetHierarchy_AfterCheck(object sender, TreeViewEventArgs e)
    {
      FilterChanged(this, null);
    }

    /// <summary>
    /// Check whether pressed key represents allowed character.
    /// If not, event is discarded.
    /// Used for text boxes which accept only characters [0-9, ] 
    /// </summary>
    private void NumericTextBox_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (NumTbTemplate.IndexOf(e.KeyChar) == -1 && e.KeyChar != '\b')
        e.Handled = true; //Character is not in template string -> it is invalid
    }
    #endregion //Event handlers

    #region Public methods
    /// <summary>
    /// Saves filter settings to file
    /// </summary>
    /// <param name="file">File stream to save to</param>
    /// <param name="formatter">Formatter for saving objects</param>
    public void SaveToFile(FileStream file, BinaryFormatter formatter)
    {
      //Obtain selected variables
      List<String> selVariables = varTreeView.GetSelectedVarIDs().GetVarIDStrings();
      //Write objects to file
      formatter.Serialize(file, chbMatchAllKeywords.Checked);
      formatter.Serialize(file, tbTitleKeywords.Text);
      formatter.Serialize(file, dtpStartFrom.Value);
      formatter.Serialize(file, dtpStartFrom.Checked);
      formatter.Serialize(file, dtpStartTo.Value);
      formatter.Serialize(file, dtpStartTo.Checked);
      formatter.Serialize(file, chbEventRec.Checked);
      formatter.Serialize(file, chbTraceRec.Checked);
      formatter.Serialize(file, chbSnapRec.Checked);
      formatter.Serialize(file, tbRecSources.Text);
      formatter.Serialize(file, tbVehicleNumbers.Text);
      formatter.Serialize(file, tbFaultCodes.Text);
      formatter.Serialize(file, tbTrcSnpCodes.Text);
      formatter.Serialize(file, chbSevA1.Checked);
      formatter.Serialize(file, chbSevA.Checked);
      formatter.Serialize(file, chbSevB1.Checked);
      formatter.Serialize(file, chbSevB.Checked);
      formatter.Serialize(file, chbSevC.Checked);
      formatter.Serialize(file, tbFaultSubseverities.Text);
      formatter.Serialize(file, tbDDDVersions.Text);
      formatter.Serialize(file, tbTUNames.Text);
      formatter.Serialize(file, selVariables);
      formatter.Serialize(file, fleetHierarchy.GetSelectedNamesXML());
    }

    /// <summary>
    /// Loads filter settings from file
    /// </summary>
    /// <param name="file">File stream to load from</param>
    /// <param name="formatter">Formatter for loading objects</param>
    public void LoadFromFile(FileStream file, BinaryFormatter formatter)
    {
      //Read objects from file
      chbMatchAllKeywords.Checked = (bool)formatter.Deserialize(file);
      tbTitleKeywords.Text = (String)formatter.Deserialize(file);
      dtpStartFrom.Value = (DateTime)formatter.Deserialize(file);
      dtpStartFrom.Checked = (bool)formatter.Deserialize(file);
      dtpStartTo.Value = (DateTime)formatter.Deserialize(file);
      dtpStartTo.Checked = (bool)formatter.Deserialize(file);
      chbEventRec.Checked = (bool)formatter.Deserialize(file);
      chbTraceRec.Checked = (bool)formatter.Deserialize(file);
      chbSnapRec.Checked = (bool)formatter.Deserialize(file);
      tbRecSources.Text = (String)formatter.Deserialize(file);
      tbVehicleNumbers.Text = (String)formatter.Deserialize(file);
      tbFaultCodes.Text = (String)formatter.Deserialize(file);
      tbTrcSnpCodes.Text = (String)formatter.Deserialize(file);
      chbSevA1.Checked = (bool)formatter.Deserialize(file);
      chbSevA.Checked = (bool)formatter.Deserialize(file);
      chbSevB1.Checked = (bool)formatter.Deserialize(file);
      chbSevB.Checked = (bool)formatter.Deserialize(file);
      chbSevC.Checked = (bool)formatter.Deserialize(file);
      tbFaultSubseverities.Text = (String)formatter.Deserialize(file);
      tbDDDVersions.Text = (String)formatter.Deserialize(file);
      tbTUNames.Text = (String)formatter.Deserialize(file);
      List<String> selVariables = (List<String>)formatter.Deserialize(file);
      //Check selected variables
      varTreeView.CheckSelectedVars(selVariables);
      //Check selected hierarchy items
      fleetHierarchy.CheckHierarchyItems((String)formatter.Deserialize(file));
      //Build filter string
      FilterChanged(this, null);
    }
    #endregion //Public methods
  }
}