namespace DRView
{
  partial class CustomQuery
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomQuery));
      this.toolStrip = new System.Windows.Forms.ToolStrip();
      this.tsbSelect = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.tsbShowMessages = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
      this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
      this.tsbSaveQuery = new System.Windows.Forms.ToolStripButton();
      this.tsbSaveRecords = new System.Windows.Forms.ToolStripButton();
      this.tsbExportRecords = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
      this.tsbShowFilterDialog = new System.Windows.Forms.ToolStripButton();
      this.tsbApplyFilter = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
      this.msCustomQuery = new System.Windows.Forms.MenuStrip();
      this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
      this.saveQueryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.saveSelectedDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.exportRecordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.queryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
      this.systemMessagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
      this.defineFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.applyFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
      this.executeQueryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.dlgSaveFile = new System.Windows.Forms.SaveFileDialog();
      this.dlgOpenFile = new System.Windows.Forms.OpenFileDialog();
      this.statusStrip = new System.Windows.Forms.StatusStrip();
      this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
      this.progresBar = new System.Windows.Forms.ToolStripProgressBar();
      this.statusLabelRecords = new System.Windows.Forms.ToolStripStatusLabel();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.panel1 = new System.Windows.Forms.Panel();
      this.splContCustomQuery = new System.Windows.Forms.SplitContainer();
      this.tbcComponents = new System.Windows.Forms.TabControl();
      this.lbMessages = new System.Windows.Forms.ListBox();
      this.DRViewHelpProvider = new System.Windows.Forms.HelpProvider();
      this.drExport = new DRViewComponents.DRExport(this.components);
      this.persistWindow = new Mowog.PersistWindowComponent(this.components);
      this.toolStrip.SuspendLayout();
      this.msCustomQuery.SuspendLayout();
      this.statusStrip.SuspendLayout();
      this.panel1.SuspendLayout();
      this.splContCustomQuery.Panel1.SuspendLayout();
      this.splContCustomQuery.Panel2.SuspendLayout();
      this.splContCustomQuery.SuspendLayout();
      this.SuspendLayout();
      // 
      // toolStrip
      // 
      this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbSelect,
            this.toolStripSeparator1,
            this.tsbShowMessages,
            this.toolStripSeparator8,
            this.toolStripSeparator2,
            this.tsbSaveQuery,
            this.tsbSaveRecords,
            this.tsbExportRecords,
            this.toolStripSeparator5,
            this.tsbShowFilterDialog,
            this.tsbApplyFilter,
            this.toolStripSeparator6});
      this.toolStrip.Location = new System.Drawing.Point(0, 0);
      this.toolStrip.Name = "toolStrip";
      this.toolStrip.Size = new System.Drawing.Size(921, 25);
      this.toolStrip.TabIndex = 4;
      this.toolStrip.Text = "toolStrip1";
      // 
      // tsbSelect
      // 
      this.tsbSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbSelect.Image = ((System.Drawing.Image)(resources.GetObject("tsbSelect.Image")));
      this.tsbSelect.ImageTransparentColor = System.Drawing.Color.White;
      this.tsbSelect.Name = "tsbSelect";
      this.tsbSelect.Size = new System.Drawing.Size(23, 22);
      this.tsbSelect.Text = "Execute query";
      this.tsbSelect.Click += new System.EventHandler(this.tsbSelect_Click);
      // 
      // toolStripSeparator1
      // 
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
      // 
      // tsbShowMessages
      // 
      this.tsbShowMessages.Checked = true;
      this.tsbShowMessages.CheckOnClick = true;
      this.tsbShowMessages.CheckState = System.Windows.Forms.CheckState.Checked;
      this.tsbShowMessages.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbShowMessages.Image = ((System.Drawing.Image)(resources.GetObject("tsbShowMessages.Image")));
      this.tsbShowMessages.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbShowMessages.Name = "tsbShowMessages";
      this.tsbShowMessages.Size = new System.Drawing.Size(23, 22);
      this.tsbShowMessages.Text = "Show messages";
      this.tsbShowMessages.Click += new System.EventHandler(this.tsbShowMessages_Click);
      // 
      // toolStripSeparator8
      // 
      this.toolStripSeparator8.Name = "toolStripSeparator8";
      this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
      // 
      // toolStripSeparator2
      // 
      this.toolStripSeparator2.Name = "toolStripSeparator2";
      this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
      // 
      // tsbSaveQuery
      // 
      this.tsbSaveQuery.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbSaveQuery.Image = ((System.Drawing.Image)(resources.GetObject("tsbSaveQuery.Image")));
      this.tsbSaveQuery.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbSaveQuery.Name = "tsbSaveQuery";
      this.tsbSaveQuery.Size = new System.Drawing.Size(23, 22);
      this.tsbSaveQuery.Text = "saveQuery";
      this.tsbSaveQuery.ToolTipText = "Save query";
      this.tsbSaveQuery.Click += new System.EventHandler(this.saveQueryToolStripMenuItem_Click);
      // 
      // tsbSaveRecords
      // 
      this.tsbSaveRecords.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbSaveRecords.Image = ((System.Drawing.Image)(resources.GetObject("tsbSaveRecords.Image")));
      this.tsbSaveRecords.ImageTransparentColor = System.Drawing.Color.Silver;
      this.tsbSaveRecords.Name = "tsbSaveRecords";
      this.tsbSaveRecords.Size = new System.Drawing.Size(23, 22);
      this.tsbSaveRecords.ToolTipText = "Save records to file";
      this.tsbSaveRecords.Click += new System.EventHandler(this.saveSelectedDataToolStripMenuItem_Click);
      // 
      // tsbExportRecords
      // 
      this.tsbExportRecords.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbExportRecords.Image = ((System.Drawing.Image)(resources.GetObject("tsbExportRecords.Image")));
      this.tsbExportRecords.ImageTransparentColor = System.Drawing.Color.Silver;
      this.tsbExportRecords.Name = "tsbExportRecords";
      this.tsbExportRecords.Size = new System.Drawing.Size(23, 22);
      this.tsbExportRecords.ToolTipText = "Export records to file";
      this.tsbExportRecords.Click += new System.EventHandler(this.exportRecordsToolStripMenuItem_Click);
      // 
      // toolStripSeparator5
      // 
      this.toolStripSeparator5.Name = "toolStripSeparator5";
      this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
      // 
      // tsbShowFilterDialog
      // 
      this.tsbShowFilterDialog.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbShowFilterDialog.Image = ((System.Drawing.Image)(resources.GetObject("tsbShowFilterDialog.Image")));
      this.tsbShowFilterDialog.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbShowFilterDialog.Name = "tsbShowFilterDialog";
      this.tsbShowFilterDialog.Size = new System.Drawing.Size(23, 22);
      this.tsbShowFilterDialog.ToolTipText = "Show filter dialog";
      this.tsbShowFilterDialog.Click += new System.EventHandler(this.tsbShowFilterDialog_Click);
      // 
      // tsbApplyFilter
      // 
      this.tsbApplyFilter.CheckOnClick = true;
      this.tsbApplyFilter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbApplyFilter.Image = ((System.Drawing.Image)(resources.GetObject("tsbApplyFilter.Image")));
      this.tsbApplyFilter.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbApplyFilter.Name = "tsbApplyFilter";
      this.tsbApplyFilter.Size = new System.Drawing.Size(23, 22);
      this.tsbApplyFilter.ToolTipText = "Apply filter";
      this.tsbApplyFilter.Click += new System.EventHandler(this.tsbApplyFilter_Click);
      // 
      // toolStripSeparator6
      // 
      this.toolStripSeparator6.Name = "toolStripSeparator6";
      this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
      // 
      // msCustomQuery
      // 
      this.msCustomQuery.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.queryToolStripMenuItem});
      this.msCustomQuery.Location = new System.Drawing.Point(0, 0);
      this.msCustomQuery.Name = "msCustomQuery";
      this.msCustomQuery.Size = new System.Drawing.Size(921, 24);
      this.msCustomQuery.TabIndex = 5;
      this.msCustomQuery.Text = "menuStrip1";
      this.msCustomQuery.Visible = false;
      // 
      // fileToolStripMenuItem
      // 
      this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator4,
            this.saveQueryToolStripMenuItem,
            this.saveSelectedDataToolStripMenuItem,
            this.exportRecordsToolStripMenuItem});
      this.fileToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.MatchOnly;
      this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
      this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
      this.fileToolStripMenuItem.Text = "&File";
      // 
      // toolStripSeparator4
      // 
      this.toolStripSeparator4.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.toolStripSeparator4.MergeIndex = 5;
      this.toolStripSeparator4.Name = "toolStripSeparator4";
      this.toolStripSeparator4.Size = new System.Drawing.Size(146, 6);
      // 
      // saveQueryToolStripMenuItem
      // 
      this.saveQueryToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveQueryToolStripMenuItem.Image")));
      this.saveQueryToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.saveQueryToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.saveQueryToolStripMenuItem.MergeIndex = 6;
      this.saveQueryToolStripMenuItem.Name = "saveQueryToolStripMenuItem";
      this.saveQueryToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
      this.saveQueryToolStripMenuItem.Text = "Save query";
      this.saveQueryToolStripMenuItem.Click += new System.EventHandler(this.saveQueryToolStripMenuItem_Click);
      // 
      // saveSelectedDataToolStripMenuItem
      // 
      this.saveSelectedDataToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveSelectedDataToolStripMenuItem.Image")));
      this.saveSelectedDataToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Silver;
      this.saveSelectedDataToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.saveSelectedDataToolStripMenuItem.MergeIndex = 7;
      this.saveSelectedDataToolStripMenuItem.Name = "saveSelectedDataToolStripMenuItem";
      this.saveSelectedDataToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
      this.saveSelectedDataToolStripMenuItem.Text = "Save records";
      this.saveSelectedDataToolStripMenuItem.Click += new System.EventHandler(this.saveSelectedDataToolStripMenuItem_Click);
      // 
      // exportRecordsToolStripMenuItem
      // 
      this.exportRecordsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exportRecordsToolStripMenuItem.Image")));
      this.exportRecordsToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Silver;
      this.exportRecordsToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.exportRecordsToolStripMenuItem.MergeIndex = 8;
      this.exportRecordsToolStripMenuItem.Name = "exportRecordsToolStripMenuItem";
      this.exportRecordsToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
      this.exportRecordsToolStripMenuItem.Text = "Export records";
      this.exportRecordsToolStripMenuItem.Click += new System.EventHandler(this.exportRecordsToolStripMenuItem_Click);
      // 
      // queryToolStripMenuItem
      // 
      this.queryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewToolStripMenuItem,
            this.toolStripSeparator7,
            this.defineFilterToolStripMenuItem,
            this.applyFilterToolStripMenuItem,
            this.toolStripSeparator9,
            this.executeQueryToolStripMenuItem});
      this.queryToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.queryToolStripMenuItem.MergeIndex = 1;
      this.queryToolStripMenuItem.Name = "queryToolStripMenuItem";
      this.queryToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
      this.queryToolStripMenuItem.Text = "&Query";
      // 
      // viewToolStripMenuItem
      // 
      this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator3,
            this.systemMessagesToolStripMenuItem});
      this.viewToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.viewToolStripMenuItem.MergeIndex = 1;
      this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
      this.viewToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
      this.viewToolStripMenuItem.Text = "&Show";
      // 
      // toolStripSeparator3
      // 
      this.toolStripSeparator3.Name = "toolStripSeparator3";
      this.toolStripSeparator3.Size = new System.Drawing.Size(163, 6);
      // 
      // systemMessagesToolStripMenuItem
      // 
      this.systemMessagesToolStripMenuItem.CheckOnClick = true;
      this.systemMessagesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("systemMessagesToolStripMenuItem.Image")));
      this.systemMessagesToolStripMenuItem.Name = "systemMessagesToolStripMenuItem";
      this.systemMessagesToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
      this.systemMessagesToolStripMenuItem.Text = "System messages";
      this.systemMessagesToolStripMenuItem.Click += new System.EventHandler(this.tsbShowMessages_Click);
      // 
      // toolStripSeparator7
      // 
      this.toolStripSeparator7.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.toolStripSeparator7.MergeIndex = 2;
      this.toolStripSeparator7.Name = "toolStripSeparator7";
      this.toolStripSeparator7.Size = new System.Drawing.Size(144, 6);
      // 
      // defineFilterToolStripMenuItem
      // 
      this.defineFilterToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("defineFilterToolStripMenuItem.Image")));
      this.defineFilterToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.defineFilterToolStripMenuItem.MergeIndex = 3;
      this.defineFilterToolStripMenuItem.Name = "defineFilterToolStripMenuItem";
      this.defineFilterToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
      this.defineFilterToolStripMenuItem.Text = "&Define filter";
      this.defineFilterToolStripMenuItem.Click += new System.EventHandler(this.tsbShowFilterDialog_Click);
      // 
      // applyFilterToolStripMenuItem
      // 
      this.applyFilterToolStripMenuItem.CheckOnClick = true;
      this.applyFilterToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("applyFilterToolStripMenuItem.Image")));
      this.applyFilterToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.applyFilterToolStripMenuItem.MergeIndex = 4;
      this.applyFilterToolStripMenuItem.Name = "applyFilterToolStripMenuItem";
      this.applyFilterToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
      this.applyFilterToolStripMenuItem.Text = "Apply &filter";
      this.applyFilterToolStripMenuItem.Click += new System.EventHandler(this.tsbApplyFilter_Click);
      // 
      // toolStripSeparator9
      // 
      this.toolStripSeparator9.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.toolStripSeparator9.MergeIndex = 5;
      this.toolStripSeparator9.Name = "toolStripSeparator9";
      this.toolStripSeparator9.Size = new System.Drawing.Size(144, 6);
      // 
      // executeQueryToolStripMenuItem
      // 
      this.executeQueryToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("executeQueryToolStripMenuItem.Image")));
      this.executeQueryToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.White;
      this.executeQueryToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.executeQueryToolStripMenuItem.MergeIndex = 6;
      this.executeQueryToolStripMenuItem.Name = "executeQueryToolStripMenuItem";
      this.executeQueryToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
      this.executeQueryToolStripMenuItem.Text = "Execute query";
      this.executeQueryToolStripMenuItem.Click += new System.EventHandler(this.tsbSelect_Click);
      // 
      // dlgOpenFile
      // 
      this.dlgOpenFile.ShowReadOnly = true;
      // 
      // statusStrip
      // 
      this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel,
            this.progresBar,
            this.statusLabelRecords});
      this.statusStrip.Location = new System.Drawing.Point(0, 617);
      this.statusStrip.Name = "statusStrip";
      this.statusStrip.Size = new System.Drawing.Size(921, 24);
      this.statusStrip.TabIndex = 6;
      this.statusStrip.Text = "statusStrip1";
      // 
      // statusLabel
      // 
      this.statusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                  | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                  | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
      this.statusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
      this.statusLabel.Name = "statusLabel";
      this.statusLabel.Size = new System.Drawing.Size(746, 19);
      this.statusLabel.Spring = true;
      this.statusLabel.Text = "ready";
      this.statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // progresBar
      // 
      this.progresBar.AutoSize = false;
      this.progresBar.Name = "progresBar";
      this.progresBar.Size = new System.Drawing.Size(100, 18);
      this.progresBar.Visible = false;
      // 
      // statusLabelRecords
      // 
      this.statusLabelRecords.AutoSize = false;
      this.statusLabelRecords.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                  | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                  | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
      this.statusLabelRecords.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
      this.statusLabelRecords.Name = "statusLabelRecords";
      this.statusLabelRecords.Size = new System.Drawing.Size(160, 19);
      // 
      // splitter1
      // 
      this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter1.Enabled = false;
      this.splitter1.Location = new System.Drawing.Point(0, 616);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(921, 1);
      this.splitter1.TabIndex = 7;
      this.splitter1.TabStop = false;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.splContCustomQuery);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel1.Location = new System.Drawing.Point(0, 25);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(921, 591);
      this.panel1.TabIndex = 8;
      // 
      // splContCustomQuery
      // 
      this.splContCustomQuery.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splContCustomQuery.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
      this.splContCustomQuery.Location = new System.Drawing.Point(0, 0);
      this.splContCustomQuery.Name = "splContCustomQuery";
      this.splContCustomQuery.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splContCustomQuery.Panel1
      // 
      this.splContCustomQuery.Panel1.Controls.Add(this.tbcComponents);
      this.splContCustomQuery.Panel1MinSize = 50;
      // 
      // splContCustomQuery.Panel2
      // 
      this.splContCustomQuery.Panel2.Controls.Add(this.lbMessages);
      this.splContCustomQuery.Panel2MinSize = 50;
      this.splContCustomQuery.Size = new System.Drawing.Size(921, 591);
      this.splContCustomQuery.SplitterDistance = 538;
      this.splContCustomQuery.SplitterWidth = 3;
      this.splContCustomQuery.TabIndex = 3;
      // 
      // tbcComponents
      // 
      this.tbcComponents.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbcComponents.Location = new System.Drawing.Point(0, 0);
      this.tbcComponents.Name = "tbcComponents";
      this.tbcComponents.Padding = new System.Drawing.Point(3, 3);
      this.tbcComponents.SelectedIndex = 0;
      this.tbcComponents.Size = new System.Drawing.Size(921, 538);
      this.tbcComponents.TabIndex = 2;
      // 
      // lbMessages
      // 
      this.lbMessages.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbMessages.FormattingEnabled = true;
      this.lbMessages.Location = new System.Drawing.Point(0, 0);
      this.lbMessages.Name = "lbMessages";
      this.lbMessages.Size = new System.Drawing.Size(921, 43);
      this.lbMessages.TabIndex = 0;
      // 
      // DRViewHelpProvider
      // 
      this.DRViewHelpProvider.HelpNamespace = "DRView.chm";
      // 
      // drExport
      // 
      this.drExport.DDDHelper = null;
      this.drExport.Selector = null;
      this.drExport.ProgressReport += new DRSelectorComponents.ReportProgressEventHandler(this.ProgressReportEvent);
      this.drExport.InfoMessage += new DRSelectorComponents.InfoMessageEventhandler(this.InfoMessageEvent);
      this.drExport.ErrorMessage += new DRSelectorComponents.ErrorMessageEventhandler(this.ErrorMessageEvent);
      // 
      // persistWindow
      // 
      this.persistWindow.Form = this;
      this.persistWindow.UseLocAppDataDir = true;
      this.persistWindow.XMLFilePath = "DRViewWindowState.xml";
      // 
      // CustomQuery
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(921, 641);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.splitter1);
      this.Controls.Add(this.statusStrip);
      this.Controls.Add(this.toolStrip);
      this.Controls.Add(this.msCustomQuery);
      this.DRViewHelpProvider.SetHelpKeyword(this, "CustomQueryForm.htm");
      this.DRViewHelpProvider.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MainMenuStrip = this.msCustomQuery;
      this.MinimumSize = new System.Drawing.Size(600, 500);
      this.Name = "CustomQuery";
      this.DRViewHelpProvider.SetShowHelp(this, true);
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Diagnostic records query";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CustomQuery_FormClosed);
      this.toolStrip.ResumeLayout(false);
      this.toolStrip.PerformLayout();
      this.msCustomQuery.ResumeLayout(false);
      this.msCustomQuery.PerformLayout();
      this.statusStrip.ResumeLayout(false);
      this.statusStrip.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.splContCustomQuery.Panel1.ResumeLayout(false);
      this.splContCustomQuery.Panel2.ResumeLayout(false);
      this.splContCustomQuery.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ToolStrip toolStrip;
    private System.Windows.Forms.ToolStripButton tsbShowMessages;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.SplitContainer splContCustomQuery;
    private System.Windows.Forms.MenuStrip msCustomQuery;
    private System.Windows.Forms.ToolStripMenuItem queryToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
    private System.Windows.Forms.ToolStripMenuItem saveSelectedDataToolStripMenuItem;
    private System.Windows.Forms.SaveFileDialog dlgSaveFile;
    private System.Windows.Forms.OpenFileDialog dlgOpenFile;
    private System.Windows.Forms.StatusStrip statusStrip;
    private System.Windows.Forms.ToolStripStatusLabel statusLabel;
    private System.Windows.Forms.ToolStripProgressBar progresBar;
    private System.Windows.Forms.Splitter splitter1;
    private System.Windows.Forms.ListBox lbMessages;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.ToolStripButton tsbSaveRecords;
    private System.Windows.Forms.ToolStripMenuItem systemMessagesToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem exportRecordsToolStripMenuItem;
    private DRViewComponents.DRExport drExport;
    private System.Windows.Forms.ToolStripButton tsbExportRecords;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
    private System.Windows.Forms.ToolStripButton tsbShowFilterDialog;
    private System.Windows.Forms.ToolStripButton tsbApplyFilter;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
    private System.Windows.Forms.ToolStripMenuItem defineFilterToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem applyFilterToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
    private System.Windows.Forms.ToolStripStatusLabel statusLabelRecords;
    private System.Windows.Forms.HelpProvider DRViewHelpProvider;
    private System.Windows.Forms.ToolStripButton tsbSelect;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
    private System.Windows.Forms.ToolStripMenuItem executeQueryToolStripMenuItem;
    private System.Windows.Forms.ToolStripButton tsbSaveQuery;
    private System.Windows.Forms.ToolStripMenuItem saveQueryToolStripMenuItem;
    private Mowog.PersistWindowComponent persistWindow;
    private System.Windows.Forms.TabControl tbcComponents;

  }
}