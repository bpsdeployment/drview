namespace DRView
{
  partial class frmMap
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMap));
      this.statusStrip = new System.Windows.Forms.StatusStrip();
      this.tsLabelRecs = new System.Windows.Forms.ToolStripStatusLabel();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.panel1 = new System.Windows.Forms.Panel();
      this.drViewMap = new DRViewComponents.DRViewMap();
      this.DRViewHelpProvider = new System.Windows.Forms.HelpProvider();
      this.persistWindow = new Mowog.PersistWindowComponent(this.components);
      this.statusStrip.SuspendLayout();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // statusStrip
      // 
      this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsLabelRecs});
      this.statusStrip.Location = new System.Drawing.Point(0, 334);
      this.statusStrip.Name = "statusStrip";
      this.statusStrip.Size = new System.Drawing.Size(382, 22);
      this.statusStrip.TabIndex = 1;
      this.statusStrip.Text = "statusStrip1";
      // 
      // tsLabelRecs
      // 
      this.tsLabelRecs.Name = "tsLabelRecs";
      this.tsLabelRecs.Size = new System.Drawing.Size(0, 17);
      // 
      // splitter1
      // 
      this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter1.Location = new System.Drawing.Point(0, 331);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(382, 3);
      this.splitter1.TabIndex = 2;
      this.splitter1.TabStop = false;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.drViewMap);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(382, 331);
      this.panel1.TabIndex = 3;
      // 
      // drViewMap
      // 
      this.drViewMap.CurrFirstRecIndx = 0;
      this.drViewMap.CurrLastRecIndx = 0;
      this.drViewMap.DDDHelper = null;
      this.drViewMap.DisplayData = false;
      this.drViewMap.Dock = System.Windows.Forms.DockStyle.Fill;
      this.drViewMap.FirstStartTime = new System.DateTime(((long)(0)));
      this.DRViewHelpProvider.SetHelpKeyword(this.drViewMap, "MapView.chm");
      this.DRViewHelpProvider.SetHelpNavigator(this.drViewMap, System.Windows.Forms.HelpNavigator.Topic);
      this.drViewMap.InvGPSIconURL = "http://maps.google.com/mapfiles/kml/pal3/icon37.png";
      this.drViewMap.InvGPSText = "Invalid GPS data";
      this.drViewMap.LastStartTime = new System.DateTime(((long)(0)));
      this.drViewMap.Location = new System.Drawing.Point(0, 0);
      this.drViewMap.MaxRecsPerTab = 10;
      this.drViewMap.MaxTabCount = 5;
      this.drViewMap.MinimumSize = new System.Drawing.Size(350, 330);
      this.drViewMap.MoreRecsTitle = "More records, click for details...";
      this.drViewMap.Name = "drViewMap";
      this.DRViewHelpProvider.SetShowHelp(this.drViewMap, true);
      this.drViewMap.ShowTUNames = false;
      this.drViewMap.Size = new System.Drawing.Size(382, 331);
      this.drViewMap.TabCaptionBase = "Records";
      this.drViewMap.TabIndex = 0;
      this.drViewMap.MapViewChange += new DRViewComponents.MapViewChangeEventhandler(this.drViewMap_MapViewChange);
      // 
      // DRViewHelpProvider
      // 
      this.DRViewHelpProvider.HelpNamespace = "DRView.chm";
      // 
      // persistWindow
      // 
      this.persistWindow.Form = this;
      this.persistWindow.UseLocAppDataDir = true;
      this.persistWindow.XMLFilePath = "DRViewWindowState.xml";
      // 
      // frmMap
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(382, 356);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.splitter1);
      this.Controls.Add(this.statusStrip);
      this.DRViewHelpProvider.SetHelpKeyword(this, "MapView.htm");
      this.DRViewHelpProvider.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.KeyPreview = true;
      this.MinimumSize = new System.Drawing.Size(390, 390);
      this.Name = "frmMap";
      this.DRViewHelpProvider.SetShowHelp(this, true);
      this.Text = "MapForm";
      this.Load += new System.EventHandler(this.frmMap_Load);
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMap_FormClosed);
      this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMap_KeyDown);
      this.statusStrip.ResumeLayout(false);
      this.statusStrip.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private DRViewComponents.DRViewMap drViewMap;
    private System.Windows.Forms.StatusStrip statusStrip;
    private System.Windows.Forms.Splitter splitter1;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.ToolStripStatusLabel tsLabelRecs;
    private System.Windows.Forms.HelpProvider DRViewHelpProvider;
    private Mowog.PersistWindowComponent persistWindow;
  }
}