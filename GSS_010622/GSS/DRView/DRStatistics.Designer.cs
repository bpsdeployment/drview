namespace DRView
{
  partial class DRStatistics
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DRStatistics));
      this.label1 = new System.Windows.Forms.Label();
      this.statusStrip = new System.Windows.Forms.StatusStrip();
      this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
      this.slRowCount = new System.Windows.Forms.ToolStripStatusLabel();
      this.slTotalRecCount = new System.Windows.Forms.ToolStripStatusLabel();
      this.btSelect = new System.Windows.Forms.Button();
      this.cmdGetStats = new System.Data.SqlClient.SqlCommand();
      this.msDRStatistics = new System.Windows.Forms.MenuStrip();
      this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.exportStatisticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.dlgSaveFile = new System.Windows.Forms.SaveFileDialog();
      this.CNetHelpProvider = new System.Windows.Forms.HelpProvider();
      this.dgvRecStats = new System.Windows.Forms.DataGridView();
      this.TrainName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.VehicleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.VehicleNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Source = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.RecTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colFirstTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colLastTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.FaultSeverity = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.FaultSubSeverity = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dsRecordStats = new DSRecordStats.DSRecordStats();
      this.timePicker = new DRSelectorComponents.TimePicker();
      this.panHierarchy = new System.Windows.Forms.Panel();
      this.hierarchyTUInstances = new DRSelectorComponents.FleetHierarchy_TUInstances();
      this.btToggleHierarchy = new System.Windows.Forms.Button();
      this.persistWindow = new Mowog.PersistWindowComponent(this.components);
      this.statusStrip.SuspendLayout();
      this.msDRStatistics.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvRecStats)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dsRecordStats)).BeginInit();
      this.panHierarchy.SuspendLayout();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.CNetHelpProvider.SetHelpKeyword(this.label1, "DRStatistics.htm#DRStatistics_label1");
      this.CNetHelpProvider.SetHelpNavigator(this.label1, System.Windows.Forms.HelpNavigator.Topic);
      this.label1.Location = new System.Drawing.Point(1, 3);
      this.label1.Name = "label1";
      this.CNetHelpProvider.SetShowHelp(this.label1, true);
      this.label1.Size = new System.Drawing.Size(90, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Trains or vehicles";
      // 
      // statusStrip
      // 
      this.CNetHelpProvider.SetHelpKeyword(this.statusStrip, "DRStatistics.htm#DRStatistics_statusStrip");
      this.CNetHelpProvider.SetHelpNavigator(this.statusStrip, System.Windows.Forms.HelpNavigator.Topic);
      this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel,
            this.slRowCount,
            this.slTotalRecCount});
      this.statusStrip.Location = new System.Drawing.Point(0, 444);
      this.statusStrip.Name = "statusStrip";
      this.CNetHelpProvider.SetShowHelp(this.statusStrip, true);
      this.statusStrip.Size = new System.Drawing.Size(744, 22);
      this.statusStrip.TabIndex = 1;
      this.statusStrip.Text = "statusStrip1";
      // 
      // statusLabel
      // 
      this.statusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
      this.statusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
      this.statusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.statusLabel.Name = "statusLabel";
      this.statusLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
      this.statusLabel.Size = new System.Drawing.Size(529, 17);
      this.statusLabel.Spring = true;
      this.statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // slRowCount
      // 
      this.slRowCount.AutoSize = false;
      this.slRowCount.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
      this.slRowCount.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
      this.slRowCount.Name = "slRowCount";
      this.slRowCount.Size = new System.Drawing.Size(100, 17);
      this.slRowCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // slTotalRecCount
      // 
      this.slTotalRecCount.AutoSize = false;
      this.slTotalRecCount.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
      this.slTotalRecCount.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
      this.slTotalRecCount.Name = "slTotalRecCount";
      this.slTotalRecCount.Size = new System.Drawing.Size(100, 17);
      this.slTotalRecCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btSelect
      // 
      this.CNetHelpProvider.SetHelpKeyword(this.btSelect, "DRStatistics.htm#DRStatistics_btSelect");
      this.CNetHelpProvider.SetHelpNavigator(this.btSelect, System.Windows.Forms.HelpNavigator.Topic);
      this.btSelect.Location = new System.Drawing.Point(571, 16);
      this.btSelect.Name = "btSelect";
      this.CNetHelpProvider.SetShowHelp(this.btSelect, true);
      this.btSelect.Size = new System.Drawing.Size(75, 23);
      this.btSelect.TabIndex = 4;
      this.btSelect.Text = "Select";
      this.btSelect.UseVisualStyleBackColor = true;
      this.btSelect.Click += new System.EventHandler(this.btSelect_Click);
      // 
      // cmdGetStats
      // 
      this.cmdGetStats.CommandText = "GetRecordStats";
      this.cmdGetStats.CommandTimeout = 60;
      this.cmdGetStats.CommandType = System.Data.CommandType.StoredProcedure;
      this.cmdGetStats.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@TUInstanceID", System.Data.SqlDbType.UniqueIdentifier),
            new System.Data.SqlClient.SqlParameter("@TimeFrom", System.Data.SqlDbType.DateTime),
            new System.Data.SqlClient.SqlParameter("@TimeTo", System.Data.SqlDbType.DateTime),
            new System.Data.SqlClient.SqlParameter("@LanguageID", System.Data.SqlDbType.Int)});
      // 
      // msDRStatistics
      // 
      this.CNetHelpProvider.SetHelpKeyword(this.msDRStatistics, "DRStatistics.htm#DRStatistics_msDRStatistics");
      this.CNetHelpProvider.SetHelpNavigator(this.msDRStatistics, System.Windows.Forms.HelpNavigator.Topic);
      this.msDRStatistics.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
      this.msDRStatistics.Location = new System.Drawing.Point(0, 0);
      this.msDRStatistics.Name = "msDRStatistics";
      this.CNetHelpProvider.SetShowHelp(this.msDRStatistics, true);
      this.msDRStatistics.Size = new System.Drawing.Size(755, 24);
      this.msDRStatistics.TabIndex = 5;
      this.msDRStatistics.Text = "menuStrip1";
      this.msDRStatistics.Visible = false;
      // 
      // fileToolStripMenuItem
      // 
      this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.exportStatisticsToolStripMenuItem});
      this.fileToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.MatchOnly;
      this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
      this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
      this.fileToolStripMenuItem.Text = "&File";
      // 
      // toolStripSeparator1
      // 
      this.toolStripSeparator1.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.toolStripSeparator1.MergeIndex = 3;
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new System.Drawing.Size(152, 6);
      // 
      // exportStatisticsToolStripMenuItem
      // 
      this.exportStatisticsToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.exportStatisticsToolStripMenuItem.MergeIndex = 4;
      this.exportStatisticsToolStripMenuItem.Name = "exportStatisticsToolStripMenuItem";
      this.exportStatisticsToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
      this.exportStatisticsToolStripMenuItem.Text = "Export statistics";
      this.exportStatisticsToolStripMenuItem.Click += new System.EventHandler(this.exportStatisticsToolStripMenuItem_Click);
      // 
      // dlgSaveFile
      // 
      this.dlgSaveFile.DefaultExt = "csv";
      this.dlgSaveFile.FileName = "DRStatistics.csv";
      this.dlgSaveFile.Filter = "CSV files|*.csv";
      // 
      // CNetHelpProvider
      // 
      this.CNetHelpProvider.HelpNamespace = "DRView.chm";
      // 
      // dgvRecStats
      // 
      this.dgvRecStats.AllowUserToAddRows = false;
      this.dgvRecStats.AllowUserToDeleteRows = false;
      this.dgvRecStats.AllowUserToOrderColumns = true;
      this.dgvRecStats.AllowUserToResizeRows = false;
      this.dgvRecStats.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.dgvRecStats.AutoGenerateColumns = false;
      this.dgvRecStats.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
      this.dgvRecStats.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvRecStats.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TrainName,
            this.VehicleName,
            this.VehicleNumber,
            this.Source,
            this.dataGridViewTextBoxColumn6,
            this.RecTitle,
            this.dataGridViewTextBoxColumn7,
            this.colFirstTime,
            this.colLastTime,
            this.FaultSeverity,
            this.FaultSubSeverity});
      this.dgvRecStats.DataMember = "RecordStats";
      this.dgvRecStats.DataSource = this.dsRecordStats;
      this.CNetHelpProvider.SetHelpKeyword(this.dgvRecStats, "DRStatistics.htm#DRStatistics_dgvRecStats");
      this.CNetHelpProvider.SetHelpNavigator(this.dgvRecStats, System.Windows.Forms.HelpNavigator.Topic);
      this.dgvRecStats.Location = new System.Drawing.Point(0, 45);
      this.dgvRecStats.Name = "dgvRecStats";
      this.dgvRecStats.ReadOnly = true;
      this.dgvRecStats.RowHeadersVisible = false;
      this.CNetHelpProvider.SetShowHelp(this.dgvRecStats, true);
      this.dgvRecStats.Size = new System.Drawing.Size(744, 396);
      this.dgvRecStats.TabIndex = 3;
      // 
      // TrainName
      // 
      this.TrainName.DataPropertyName = "TrainName";
      this.TrainName.HeaderText = "Train";
      this.TrainName.Name = "TrainName";
      this.TrainName.ReadOnly = true;
      this.TrainName.Width = 56;
      // 
      // VehicleName
      // 
      this.VehicleName.DataPropertyName = "VehicleName";
      this.VehicleName.HeaderText = "Vehicle";
      this.VehicleName.Name = "VehicleName";
      this.VehicleName.ReadOnly = true;
      this.VehicleName.Width = 67;
      // 
      // VehicleNumber
      // 
      this.VehicleNumber.DataPropertyName = "VehicleNumber";
      this.VehicleNumber.HeaderText = "Vehicle Number";
      this.VehicleNumber.Name = "VehicleNumber";
      this.VehicleNumber.ReadOnly = true;
      this.VehicleNumber.Width = 98;
      // 
      // Source
      // 
      this.Source.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
      this.Source.DataPropertyName = "Source";
      this.Source.HeaderText = "Record Source";
      this.Source.Name = "Source";
      this.Source.ReadOnly = true;
      this.Source.Width = 96;
      // 
      // dataGridViewTextBoxColumn6
      // 
      this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
      this.dataGridViewTextBoxColumn6.DataPropertyName = "FaultCode";
      this.dataGridViewTextBoxColumn6.HeaderText = "Fault Code";
      this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
      this.dataGridViewTextBoxColumn6.ReadOnly = true;
      this.dataGridViewTextBoxColumn6.Width = 77;
      // 
      // RecTitle
      // 
      this.RecTitle.DataPropertyName = "RecTitle";
      this.RecTitle.HeaderText = "Record Title";
      this.RecTitle.Name = "RecTitle";
      this.RecTitle.ReadOnly = true;
      this.RecTitle.Width = 83;
      // 
      // dataGridViewTextBoxColumn7
      // 
      this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
      this.dataGridViewTextBoxColumn7.DataPropertyName = "RecCount";
      this.dataGridViewTextBoxColumn7.HeaderText = "Count";
      this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
      this.dataGridViewTextBoxColumn7.ReadOnly = true;
      this.dataGridViewTextBoxColumn7.Width = 60;
      // 
      // colFirstTime
      // 
      this.colFirstTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
      this.colFirstTime.DataPropertyName = "FirstTime";
      dataGridViewCellStyle1.Format = "yyyy-MM-dd HH:mm:ss.fff";
      this.colFirstTime.DefaultCellStyle = dataGridViewCellStyle1;
      this.colFirstTime.HeaderText = "Time of first";
      this.colFirstTime.Name = "colFirstTime";
      this.colFirstTime.ReadOnly = true;
      this.colFirstTime.Width = 65;
      // 
      // colLastTime
      // 
      this.colLastTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
      this.colLastTime.DataPropertyName = "LastTime";
      dataGridViewCellStyle2.Format = "yyyy-MM-dd HH:mm:ss.fff";
      this.colLastTime.DefaultCellStyle = dataGridViewCellStyle2;
      this.colLastTime.HeaderText = "Time of last";
      this.colLastTime.Name = "colLastTime";
      this.colLastTime.ReadOnly = true;
      this.colLastTime.Width = 65;
      // 
      // FaultSeverity
      // 
      this.FaultSeverity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
      this.FaultSeverity.DataPropertyName = "FaultSeverity";
      this.FaultSeverity.HeaderText = "Severity";
      this.FaultSeverity.Name = "FaultSeverity";
      this.FaultSeverity.ReadOnly = true;
      this.FaultSeverity.Width = 70;
      // 
      // FaultSubSeverity
      // 
      this.FaultSubSeverity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
      this.FaultSubSeverity.DataPropertyName = "FaultSubSeverity";
      this.FaultSubSeverity.HeaderText = "Sub Severity";
      this.FaultSubSeverity.Name = "FaultSubSeverity";
      this.FaultSubSeverity.ReadOnly = true;
      this.FaultSubSeverity.Width = 85;
      // 
      // dsRecordStats
      // 
      this.dsRecordStats.DataSetName = "DSRecordStats";
      this.dsRecordStats.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
      // 
      // timePicker
      // 
      this.timePicker.FromSelected = true;
      this.CNetHelpProvider.SetHelpKeyword(this.timePicker, "DRStatistics.htm#DRStatistics_TimePicker");
      this.CNetHelpProvider.SetHelpNavigator(this.timePicker, System.Windows.Forms.HelpNavigator.Topic);
      this.timePicker.Location = new System.Drawing.Point(102, -2);
      this.timePicker.Name = "timePicker";
      this.timePicker.PredefIntervalIndex = 0;
      this.CNetHelpProvider.SetShowHelp(this.timePicker, true);
      this.timePicker.Size = new System.Drawing.Size(463, 45);
      this.timePicker.TabIndex = 2;
      this.timePicker.TimeFrom = new System.DateTime(2009, 9, 7, 20, 37, 57, 155);
      this.timePicker.TimeTo = new System.DateTime(2009, 9, 7, 20, 37, 57, 155);
      this.timePicker.ToSelected = true;
      // 
      // panHierarchy
      // 
      this.panHierarchy.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.panHierarchy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.panHierarchy.Controls.Add(this.hierarchyTUInstances);
      this.panHierarchy.Location = new System.Drawing.Point(0, 45);
      this.panHierarchy.Name = "panHierarchy";
      this.panHierarchy.Size = new System.Drawing.Size(744, 396);
      this.panHierarchy.TabIndex = 6;
      // 
      // hierarchyTUInstances
      // 
      this.hierarchyTUInstances.Dock = System.Windows.Forms.DockStyle.Fill;
      this.hierarchyTUInstances.Location = new System.Drawing.Point(0, 0);
      this.hierarchyTUInstances.MinimumSize = new System.Drawing.Size(195, 90);
      this.hierarchyTUInstances.Name = "hierarchyTUInstances";
      this.hierarchyTUInstances.Size = new System.Drawing.Size(742, 394);
      this.hierarchyTUInstances.TabIndex = 0;
      // 
      // btToggleHierarchy
      // 
      this.btToggleHierarchy.Location = new System.Drawing.Point(4, 16);
      this.btToggleHierarchy.Name = "btToggleHierarchy";
      this.btToggleHierarchy.Size = new System.Drawing.Size(87, 23);
      this.btToggleHierarchy.TabIndex = 7;
      this.btToggleHierarchy.Text = "Hide <<";
      this.btToggleHierarchy.UseVisualStyleBackColor = true;
      this.btToggleHierarchy.Click += new System.EventHandler(this.btToggleHierarchy_Click);
      // 
      // persistWindow
      // 
      this.persistWindow.Form = this;
      this.persistWindow.UseLocAppDataDir = true;
      this.persistWindow.XMLFilePath = "DRViewWindowState.xml";
      // 
      // DRStatistics
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(744, 466);
      this.Controls.Add(this.btToggleHierarchy);
      this.Controls.Add(this.panHierarchy);
      this.Controls.Add(this.btSelect);
      this.Controls.Add(this.dgvRecStats);
      this.Controls.Add(this.timePicker);
      this.Controls.Add(this.statusStrip);
      this.Controls.Add(this.msDRStatistics);
      this.Controls.Add(this.label1);
      this.CNetHelpProvider.SetHelpKeyword(this, "DRStatistics.htm");
      this.CNetHelpProvider.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MainMenuStrip = this.msDRStatistics;
      this.MinimumSize = new System.Drawing.Size(760, 300);
      this.Name = "DRStatistics";
      this.CNetHelpProvider.SetShowHelp(this, true);
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Record statistics";
      this.Load += new System.EventHandler(this.DRStatistics_Load);
      this.statusStrip.ResumeLayout(false);
      this.statusStrip.PerformLayout();
      this.msDRStatistics.ResumeLayout(false);
      this.msDRStatistics.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvRecStats)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dsRecordStats)).EndInit();
      this.panHierarchy.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.StatusStrip statusStrip;
    private System.Windows.Forms.ToolStripStatusLabel statusLabel;
    private DRSelectorComponents.TimePicker timePicker;
    private System.Windows.Forms.DataGridView dgvRecStats;
    private DSRecordStats.DSRecordStats dsRecordStats;
    private System.Windows.Forms.Button btSelect;
    private System.Data.SqlClient.SqlCommand cmdGetStats;
    private System.Windows.Forms.MenuStrip msDRStatistics;
    private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem exportStatisticsToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.SaveFileDialog dlgSaveFile;
    private System.Windows.Forms.HelpProvider CNetHelpProvider;
    private System.Windows.Forms.ToolStripStatusLabel slRowCount;
    private System.Windows.Forms.ToolStripStatusLabel slTotalRecCount;
    private Mowog.PersistWindowComponent persistWindow;
    private System.Windows.Forms.DataGridViewTextBoxColumn TrainName;
    private System.Windows.Forms.DataGridViewTextBoxColumn VehicleName;
    private System.Windows.Forms.DataGridViewTextBoxColumn VehicleNumber;
    private System.Windows.Forms.DataGridViewTextBoxColumn Source;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
    private System.Windows.Forms.DataGridViewTextBoxColumn RecTitle;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
    private System.Windows.Forms.DataGridViewTextBoxColumn colFirstTime;
    private System.Windows.Forms.DataGridViewTextBoxColumn colLastTime;
    private System.Windows.Forms.DataGridViewTextBoxColumn FaultSeverity;
    private System.Windows.Forms.DataGridViewTextBoxColumn FaultSubSeverity;
    private System.Windows.Forms.Panel panHierarchy;
    private DRSelectorComponents.FleetHierarchy_TUInstances hierarchyTUInstances;
    private System.Windows.Forms.Button btToggleHierarchy;
  }
}