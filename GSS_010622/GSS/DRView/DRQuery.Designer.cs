namespace DRView
{
  partial class DRQuery
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DRQuery));
      this.toolStrip = new System.Windows.Forms.ToolStrip();
      this.tsbSelect = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.tsbShowMessages = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
      this.tsbShowGridView = new System.Windows.Forms.ToolStripButton();
      this.tsbShowTreeView = new System.Windows.Forms.ToolStripButton();
      this.tsbShowVarsView = new System.Windows.Forms.ToolStripButton();
      this.tsbShowStatistics = new System.Windows.Forms.ToolStripButton();
      this.tsbShowGrouped = new System.Windows.Forms.ToolStripButton();
      this.tsbShowMap = new System.Windows.Forms.ToolStripButton();
      this.tsbNewGraph = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
      this.tsbSaveQuery = new System.Windows.Forms.ToolStripButton();
      this.tsbSaveRecords = new System.Windows.Forms.ToolStripButton();
      this.tsbExportRecords = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
      this.tsbShowFilterDialog = new System.Windows.Forms.ToolStripButton();
      this.tsbApplyFilter = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
      this.msDRQuery = new System.Windows.Forms.MenuStrip();
      this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
      this.saveQueryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.saveSelectedDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.exportRecordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.queryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.gridViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.treeViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.variablesViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.statisticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.groupedViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.mapViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.newGraphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
      this.systemMessagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
      this.defineFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.applyFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
      this.executeQueryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.dlgSaveFile = new System.Windows.Forms.SaveFileDialog();
      this.dlgOpenFile = new System.Windows.Forms.OpenFileDialog();
      this.statusStrip = new System.Windows.Forms.StatusStrip();
      this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
      this.progresBar = new System.Windows.Forms.ToolStripProgressBar();
      this.statusLabelRecords = new System.Windows.Forms.ToolStripStatusLabel();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.panel1 = new System.Windows.Forms.Panel();
      this.splContDRQuery = new System.Windows.Forms.SplitContainer();
      this.tbcComponents = new System.Windows.Forms.TabControl();
      this.tabSelector = new System.Windows.Forms.TabPage();
      this.tabVwMasterSlave = new System.Windows.Forms.TabPage();
      this.drViewMS = new DRViewComponents.DRViewMaster_Slave();
      this.tabVwTreeList = new System.Windows.Forms.TabPage();
      this.drViewTreeList = new DRViewComponents.DRViewTreeList();
      this.tabVwVariables = new System.Windows.Forms.TabPage();
      this.drViewVariables = new DRViewComponents.DRViewVariables();
      this.tabVwStatistics = new System.Windows.Forms.TabPage();
      this.drViewStatistics = new DRViewComponents.DRViewStatistics();
      this.tabVwGrouped = new System.Windows.Forms.TabPage();
      this.drViewGrouped = new DRViewComponents.DRViewGrouped();
      this.lbMessages = new System.Windows.Forms.ListBox();
      this.DRViewHelpProvider = new System.Windows.Forms.HelpProvider();
      this.drExport = new DRViewComponents.DRExport(this.components);
      this.persistWindow = new Mowog.PersistWindowComponent(this.components);
      this.toolStrip.SuspendLayout();
      this.msDRQuery.SuspendLayout();
      this.statusStrip.SuspendLayout();
      this.panel1.SuspendLayout();
      this.splContDRQuery.Panel1.SuspendLayout();
      this.splContDRQuery.Panel2.SuspendLayout();
      this.splContDRQuery.SuspendLayout();
      this.tbcComponents.SuspendLayout();
      this.tabVwMasterSlave.SuspendLayout();
      this.tabVwTreeList.SuspendLayout();
      this.tabVwVariables.SuspendLayout();
      this.tabVwStatistics.SuspendLayout();
      this.tabVwGrouped.SuspendLayout();
      this.SuspendLayout();
      // 
      // toolStrip
      // 
      this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbSelect,
            this.toolStripSeparator1,
            this.tsbShowMessages,
            this.toolStripSeparator8,
            this.tsbShowGridView,
            this.tsbShowTreeView,
            this.tsbShowVarsView,
            this.tsbShowStatistics,
            this.tsbShowGrouped,
            this.tsbShowMap,
            this.tsbNewGraph,
            this.toolStripSeparator2,
            this.tsbSaveQuery,
            this.tsbSaveRecords,
            this.tsbExportRecords,
            this.toolStripSeparator5,
            this.tsbShowFilterDialog,
            this.tsbApplyFilter,
            this.toolStripSeparator6});
      this.toolStrip.Location = new System.Drawing.Point(0, 0);
      this.toolStrip.Name = "toolStrip";
      this.toolStrip.Size = new System.Drawing.Size(921, 25);
      this.toolStrip.TabIndex = 4;
      this.toolStrip.Text = "toolStrip1";
      // 
      // tsbSelect
      // 
      this.tsbSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbSelect.Image = ((System.Drawing.Image)(resources.GetObject("tsbSelect.Image")));
      this.tsbSelect.ImageTransparentColor = System.Drawing.Color.White;
      this.tsbSelect.Name = "tsbSelect";
      this.tsbSelect.Size = new System.Drawing.Size(23, 22);
      this.tsbSelect.Text = "Execute query";
      this.tsbSelect.Click += new System.EventHandler(this.tsbSelect_Click);
      // 
      // toolStripSeparator1
      // 
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
      // 
      // tsbShowMessages
      // 
      this.tsbShowMessages.Checked = true;
      this.tsbShowMessages.CheckOnClick = true;
      this.tsbShowMessages.CheckState = System.Windows.Forms.CheckState.Checked;
      this.tsbShowMessages.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbShowMessages.Image = ((System.Drawing.Image)(resources.GetObject("tsbShowMessages.Image")));
      this.tsbShowMessages.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbShowMessages.Name = "tsbShowMessages";
      this.tsbShowMessages.Size = new System.Drawing.Size(23, 22);
      this.tsbShowMessages.Text = "Show messages";
      this.tsbShowMessages.Click += new System.EventHandler(this.tsbShowMessages_Click);
      // 
      // toolStripSeparator8
      // 
      this.toolStripSeparator8.Name = "toolStripSeparator8";
      this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
      // 
      // tsbShowGridView
      // 
      this.tsbShowGridView.Checked = true;
      this.tsbShowGridView.CheckOnClick = true;
      this.tsbShowGridView.CheckState = System.Windows.Forms.CheckState.Checked;
      this.tsbShowGridView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbShowGridView.Image = ((System.Drawing.Image)(resources.GetObject("tsbShowGridView.Image")));
      this.tsbShowGridView.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbShowGridView.Name = "tsbShowGridView";
      this.tsbShowGridView.Size = new System.Drawing.Size(23, 22);
      this.tsbShowGridView.Text = "Show Grid View";
      this.tsbShowGridView.Click += new System.EventHandler(this.tsbShowGridView_Click);
      // 
      // tsbShowTreeView
      // 
      this.tsbShowTreeView.Checked = true;
      this.tsbShowTreeView.CheckOnClick = true;
      this.tsbShowTreeView.CheckState = System.Windows.Forms.CheckState.Checked;
      this.tsbShowTreeView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbShowTreeView.Image = ((System.Drawing.Image)(resources.GetObject("tsbShowTreeView.Image")));
      this.tsbShowTreeView.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbShowTreeView.Name = "tsbShowTreeView";
      this.tsbShowTreeView.Size = new System.Drawing.Size(23, 22);
      this.tsbShowTreeView.Text = "Show Tree View";
      this.tsbShowTreeView.Click += new System.EventHandler(this.tsbShowTreeView_Click);
      // 
      // tsbShowVarsView
      // 
      this.tsbShowVarsView.Checked = true;
      this.tsbShowVarsView.CheckOnClick = true;
      this.tsbShowVarsView.CheckState = System.Windows.Forms.CheckState.Checked;
      this.tsbShowVarsView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbShowVarsView.Image = ((System.Drawing.Image)(resources.GetObject("tsbShowVarsView.Image")));
      this.tsbShowVarsView.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbShowVarsView.Name = "tsbShowVarsView";
      this.tsbShowVarsView.Size = new System.Drawing.Size(23, 22);
      this.tsbShowVarsView.Text = "Show Variables View";
      this.tsbShowVarsView.Click += new System.EventHandler(this.tsbShowVarsView_Click);
      // 
      // tsbShowStatistics
      // 
      this.tsbShowStatistics.Checked = true;
      this.tsbShowStatistics.CheckOnClick = true;
      this.tsbShowStatistics.CheckState = System.Windows.Forms.CheckState.Checked;
      this.tsbShowStatistics.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbShowStatistics.Image = ((System.Drawing.Image)(resources.GetObject("tsbShowStatistics.Image")));
      this.tsbShowStatistics.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbShowStatistics.Name = "tsbShowStatistics";
      this.tsbShowStatistics.Size = new System.Drawing.Size(23, 22);
      this.tsbShowStatistics.Text = "Show Statistics";
      this.tsbShowStatistics.Click += new System.EventHandler(this.tsbShowStatistics_Click);
      // 
      // tsbShowGrouped
      // 
      this.tsbShowGrouped.Checked = true;
      this.tsbShowGrouped.CheckOnClick = true;
      this.tsbShowGrouped.CheckState = System.Windows.Forms.CheckState.Checked;
      this.tsbShowGrouped.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbShowGrouped.Image = ((System.Drawing.Image)(resources.GetObject("tsbShowGrouped.Image")));
      this.tsbShowGrouped.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbShowGrouped.Name = "tsbShowGrouped";
      this.tsbShowGrouped.Size = new System.Drawing.Size(23, 22);
      this.tsbShowGrouped.Text = "Show Grouped View";
      this.tsbShowGrouped.Click += new System.EventHandler(this.tsbShowGrouped_Click);
      // 
      // tsbShowMap
      // 
      this.tsbShowMap.Checked = true;
      this.tsbShowMap.CheckOnClick = true;
      this.tsbShowMap.CheckState = System.Windows.Forms.CheckState.Checked;
      this.tsbShowMap.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbShowMap.Image = ((System.Drawing.Image)(resources.GetObject("tsbShowMap.Image")));
      this.tsbShowMap.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbShowMap.Name = "tsbShowMap";
      this.tsbShowMap.Size = new System.Drawing.Size(23, 22);
      this.tsbShowMap.Text = "Show Map View";
      this.tsbShowMap.Click += new System.EventHandler(this.tsbShowMap_Click);
      // 
      // tsbNewGraph
      // 
      this.tsbNewGraph.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbNewGraph.Image = ((System.Drawing.Image)(resources.GetObject("tsbNewGraph.Image")));
      this.tsbNewGraph.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbNewGraph.Name = "tsbNewGraph";
      this.tsbNewGraph.Size = new System.Drawing.Size(23, 22);
      this.tsbNewGraph.ToolTipText = "New graph";
      this.tsbNewGraph.Click += new System.EventHandler(this.tsbNewGraph_Click);
      // 
      // toolStripSeparator2
      // 
      this.toolStripSeparator2.Name = "toolStripSeparator2";
      this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
      // 
      // tsbSaveQuery
      // 
      this.tsbSaveQuery.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbSaveQuery.Image = ((System.Drawing.Image)(resources.GetObject("tsbSaveQuery.Image")));
      this.tsbSaveQuery.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbSaveQuery.Name = "tsbSaveQuery";
      this.tsbSaveQuery.Size = new System.Drawing.Size(23, 22);
      this.tsbSaveQuery.Text = "saveQuery";
      this.tsbSaveQuery.ToolTipText = "Save query";
      this.tsbSaveQuery.Click += new System.EventHandler(this.saveQueryToolStripMenuItem_Click);
      // 
      // tsbSaveRecords
      // 
      this.tsbSaveRecords.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbSaveRecords.Image = ((System.Drawing.Image)(resources.GetObject("tsbSaveRecords.Image")));
      this.tsbSaveRecords.ImageTransparentColor = System.Drawing.Color.Silver;
      this.tsbSaveRecords.Name = "tsbSaveRecords";
      this.tsbSaveRecords.Size = new System.Drawing.Size(23, 22);
      this.tsbSaveRecords.ToolTipText = "Save records to file";
      this.tsbSaveRecords.Click += new System.EventHandler(this.saveSelectedDataToolStripMenuItem_Click);
      // 
      // tsbExportRecords
      // 
      this.tsbExportRecords.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbExportRecords.Image = ((System.Drawing.Image)(resources.GetObject("tsbExportRecords.Image")));
      this.tsbExportRecords.ImageTransparentColor = System.Drawing.Color.Silver;
      this.tsbExportRecords.Name = "tsbExportRecords";
      this.tsbExportRecords.Size = new System.Drawing.Size(23, 22);
      this.tsbExportRecords.ToolTipText = "Export records to file";
      this.tsbExportRecords.Click += new System.EventHandler(this.exportRecordsToolStripMenuItem_Click);
      // 
      // toolStripSeparator5
      // 
      this.toolStripSeparator5.Name = "toolStripSeparator5";
      this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
      // 
      // tsbShowFilterDialog
      // 
      this.tsbShowFilterDialog.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbShowFilterDialog.Image = ((System.Drawing.Image)(resources.GetObject("tsbShowFilterDialog.Image")));
      this.tsbShowFilterDialog.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbShowFilterDialog.Name = "tsbShowFilterDialog";
      this.tsbShowFilterDialog.Size = new System.Drawing.Size(23, 22);
      this.tsbShowFilterDialog.ToolTipText = "Show filter dialog";
      this.tsbShowFilterDialog.Click += new System.EventHandler(this.tsbShowFilterDialog_Click);
      // 
      // tsbApplyFilter
      // 
      this.tsbApplyFilter.CheckOnClick = true;
      this.tsbApplyFilter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbApplyFilter.Image = ((System.Drawing.Image)(resources.GetObject("tsbApplyFilter.Image")));
      this.tsbApplyFilter.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbApplyFilter.Name = "tsbApplyFilter";
      this.tsbApplyFilter.Size = new System.Drawing.Size(23, 22);
      this.tsbApplyFilter.ToolTipText = "Apply filter";
      this.tsbApplyFilter.Click += new System.EventHandler(this.tsbApplyFilter_Click);
      // 
      // toolStripSeparator6
      // 
      this.toolStripSeparator6.Name = "toolStripSeparator6";
      this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
      // 
      // msDRQuery
      // 
      this.msDRQuery.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.queryToolStripMenuItem});
      this.msDRQuery.Location = new System.Drawing.Point(0, 0);
      this.msDRQuery.Name = "msDRQuery";
      this.msDRQuery.Size = new System.Drawing.Size(921, 24);
      this.msDRQuery.TabIndex = 5;
      this.msDRQuery.Text = "menuStrip1";
      this.msDRQuery.Visible = false;
      this.msDRQuery.MenuActivate += new System.EventHandler(this.msDRQuery_MenuActivate);
      // 
      // fileToolStripMenuItem
      // 
      this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator4,
            this.saveQueryToolStripMenuItem,
            this.saveSelectedDataToolStripMenuItem,
            this.exportRecordsToolStripMenuItem});
      this.fileToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.MatchOnly;
      this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
      this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
      this.fileToolStripMenuItem.Text = "&File";
      // 
      // toolStripSeparator4
      // 
      this.toolStripSeparator4.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.toolStripSeparator4.MergeIndex = 5;
      this.toolStripSeparator4.Name = "toolStripSeparator4";
      this.toolStripSeparator4.Size = new System.Drawing.Size(146, 6);
      // 
      // saveQueryToolStripMenuItem
      // 
      this.saveQueryToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveQueryToolStripMenuItem.Image")));
      this.saveQueryToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.saveQueryToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.saveQueryToolStripMenuItem.MergeIndex = 6;
      this.saveQueryToolStripMenuItem.Name = "saveQueryToolStripMenuItem";
      this.saveQueryToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
      this.saveQueryToolStripMenuItem.Text = "Save query";
      this.saveQueryToolStripMenuItem.Click += new System.EventHandler(this.saveQueryToolStripMenuItem_Click);
      // 
      // saveSelectedDataToolStripMenuItem
      // 
      this.saveSelectedDataToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveSelectedDataToolStripMenuItem.Image")));
      this.saveSelectedDataToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Silver;
      this.saveSelectedDataToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.saveSelectedDataToolStripMenuItem.MergeIndex = 7;
      this.saveSelectedDataToolStripMenuItem.Name = "saveSelectedDataToolStripMenuItem";
      this.saveSelectedDataToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
      this.saveSelectedDataToolStripMenuItem.Text = "Save records";
      this.saveSelectedDataToolStripMenuItem.Click += new System.EventHandler(this.saveSelectedDataToolStripMenuItem_Click);
      // 
      // exportRecordsToolStripMenuItem
      // 
      this.exportRecordsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exportRecordsToolStripMenuItem.Image")));
      this.exportRecordsToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Silver;
      this.exportRecordsToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.exportRecordsToolStripMenuItem.MergeIndex = 8;
      this.exportRecordsToolStripMenuItem.Name = "exportRecordsToolStripMenuItem";
      this.exportRecordsToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
      this.exportRecordsToolStripMenuItem.Text = "Export records";
      this.exportRecordsToolStripMenuItem.Click += new System.EventHandler(this.exportRecordsToolStripMenuItem_Click);
      // 
      // queryToolStripMenuItem
      // 
      this.queryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewToolStripMenuItem,
            this.toolStripSeparator7,
            this.defineFilterToolStripMenuItem,
            this.applyFilterToolStripMenuItem,
            this.toolStripSeparator9,
            this.executeQueryToolStripMenuItem});
      this.queryToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.queryToolStripMenuItem.MergeIndex = 1;
      this.queryToolStripMenuItem.Name = "queryToolStripMenuItem";
      this.queryToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
      this.queryToolStripMenuItem.Text = "&Query";
      // 
      // viewToolStripMenuItem
      // 
      this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gridViewToolStripMenuItem,
            this.treeViewToolStripMenuItem,
            this.variablesViewToolStripMenuItem,
            this.statisticsToolStripMenuItem,
            this.groupedViewToolStripMenuItem,
            this.mapViewToolStripMenuItem,
            this.newGraphToolStripMenuItem,
            this.toolStripSeparator3,
            this.systemMessagesToolStripMenuItem});
      this.viewToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.viewToolStripMenuItem.MergeIndex = 1;
      this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
      this.viewToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
      this.viewToolStripMenuItem.Text = "&Show";
      // 
      // gridViewToolStripMenuItem
      // 
      this.gridViewToolStripMenuItem.CheckOnClick = true;
      this.gridViewToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("gridViewToolStripMenuItem.Image")));
      this.gridViewToolStripMenuItem.Name = "gridViewToolStripMenuItem";
      this.gridViewToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
      this.gridViewToolStripMenuItem.Text = "Grid view";
      this.gridViewToolStripMenuItem.Click += new System.EventHandler(this.tsbShowGridView_Click);
      // 
      // treeViewToolStripMenuItem
      // 
      this.treeViewToolStripMenuItem.CheckOnClick = true;
      this.treeViewToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("treeViewToolStripMenuItem.Image")));
      this.treeViewToolStripMenuItem.Name = "treeViewToolStripMenuItem";
      this.treeViewToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
      this.treeViewToolStripMenuItem.Text = "Tree view";
      this.treeViewToolStripMenuItem.Click += new System.EventHandler(this.tsbShowTreeView_Click);
      // 
      // variablesViewToolStripMenuItem
      // 
      this.variablesViewToolStripMenuItem.CheckOnClick = true;
      this.variablesViewToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("variablesViewToolStripMenuItem.Image")));
      this.variablesViewToolStripMenuItem.Name = "variablesViewToolStripMenuItem";
      this.variablesViewToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
      this.variablesViewToolStripMenuItem.Text = "Variables view";
      this.variablesViewToolStripMenuItem.Click += new System.EventHandler(this.tsbShowVarsView_Click);
      // 
      // statisticsToolStripMenuItem
      // 
      this.statisticsToolStripMenuItem.CheckOnClick = true;
      this.statisticsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("statisticsToolStripMenuItem.Image")));
      this.statisticsToolStripMenuItem.Name = "statisticsToolStripMenuItem";
      this.statisticsToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
      this.statisticsToolStripMenuItem.Text = "Statistics";
      this.statisticsToolStripMenuItem.Click += new System.EventHandler(this.tsbShowStatistics_Click);
      // 
      // groupedViewToolStripMenuItem
      // 
      this.groupedViewToolStripMenuItem.CheckOnClick = true;
      this.groupedViewToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("groupedViewToolStripMenuItem.Image")));
      this.groupedViewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.groupedViewToolStripMenuItem.Name = "groupedViewToolStripMenuItem";
      this.groupedViewToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
      this.groupedViewToolStripMenuItem.Text = "Grouped view";
      this.groupedViewToolStripMenuItem.Click += new System.EventHandler(this.tsbShowGrouped_Click);
      // 
      // mapViewToolStripMenuItem
      // 
      this.mapViewToolStripMenuItem.CheckOnClick = true;
      this.mapViewToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("mapViewToolStripMenuItem.Image")));
      this.mapViewToolStripMenuItem.Name = "mapViewToolStripMenuItem";
      this.mapViewToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
      this.mapViewToolStripMenuItem.Text = "Map view";
      this.mapViewToolStripMenuItem.Click += new System.EventHandler(this.tsbShowMap_Click);
      // 
      // newGraphToolStripMenuItem
      // 
      this.newGraphToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newGraphToolStripMenuItem.Image")));
      this.newGraphToolStripMenuItem.Name = "newGraphToolStripMenuItem";
      this.newGraphToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
      this.newGraphToolStripMenuItem.Text = "New graph";
      this.newGraphToolStripMenuItem.Click += new System.EventHandler(this.tsbNewGraph_Click);
      // 
      // toolStripSeparator3
      // 
      this.toolStripSeparator3.Name = "toolStripSeparator3";
      this.toolStripSeparator3.Size = new System.Drawing.Size(163, 6);
      // 
      // systemMessagesToolStripMenuItem
      // 
      this.systemMessagesToolStripMenuItem.CheckOnClick = true;
      this.systemMessagesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("systemMessagesToolStripMenuItem.Image")));
      this.systemMessagesToolStripMenuItem.Name = "systemMessagesToolStripMenuItem";
      this.systemMessagesToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
      this.systemMessagesToolStripMenuItem.Text = "System messages";
      this.systemMessagesToolStripMenuItem.Click += new System.EventHandler(this.tsbShowMessages_Click);
      // 
      // toolStripSeparator7
      // 
      this.toolStripSeparator7.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.toolStripSeparator7.MergeIndex = 2;
      this.toolStripSeparator7.Name = "toolStripSeparator7";
      this.toolStripSeparator7.Size = new System.Drawing.Size(144, 6);
      // 
      // defineFilterToolStripMenuItem
      // 
      this.defineFilterToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("defineFilterToolStripMenuItem.Image")));
      this.defineFilterToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.defineFilterToolStripMenuItem.MergeIndex = 3;
      this.defineFilterToolStripMenuItem.Name = "defineFilterToolStripMenuItem";
      this.defineFilterToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
      this.defineFilterToolStripMenuItem.Text = "&Define filter";
      this.defineFilterToolStripMenuItem.Click += new System.EventHandler(this.tsbShowFilterDialog_Click);
      // 
      // applyFilterToolStripMenuItem
      // 
      this.applyFilterToolStripMenuItem.CheckOnClick = true;
      this.applyFilterToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("applyFilterToolStripMenuItem.Image")));
      this.applyFilterToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.applyFilterToolStripMenuItem.MergeIndex = 4;
      this.applyFilterToolStripMenuItem.Name = "applyFilterToolStripMenuItem";
      this.applyFilterToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
      this.applyFilterToolStripMenuItem.Text = "Apply &filter";
      this.applyFilterToolStripMenuItem.Click += new System.EventHandler(this.tsbApplyFilter_Click);
      // 
      // toolStripSeparator9
      // 
      this.toolStripSeparator9.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.toolStripSeparator9.MergeIndex = 5;
      this.toolStripSeparator9.Name = "toolStripSeparator9";
      this.toolStripSeparator9.Size = new System.Drawing.Size(144, 6);
      // 
      // executeQueryToolStripMenuItem
      // 
      this.executeQueryToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("executeQueryToolStripMenuItem.Image")));
      this.executeQueryToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.White;
      this.executeQueryToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.executeQueryToolStripMenuItem.MergeIndex = 6;
      this.executeQueryToolStripMenuItem.Name = "executeQueryToolStripMenuItem";
      this.executeQueryToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
      this.executeQueryToolStripMenuItem.Text = "Execute query";
      this.executeQueryToolStripMenuItem.Click += new System.EventHandler(this.tsbSelect_Click);
      // 
      // dlgOpenFile
      // 
      this.dlgOpenFile.ShowReadOnly = true;
      // 
      // statusStrip
      // 
      this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel,
            this.progresBar,
            this.statusLabelRecords});
      this.statusStrip.Location = new System.Drawing.Point(0, 617);
      this.statusStrip.Name = "statusStrip";
      this.statusStrip.Size = new System.Drawing.Size(921, 24);
      this.statusStrip.TabIndex = 6;
      this.statusStrip.Text = "statusStrip1";
      // 
      // statusLabel
      // 
      this.statusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                  | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                  | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
      this.statusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
      this.statusLabel.Name = "statusLabel";
      this.statusLabel.Size = new System.Drawing.Size(746, 19);
      this.statusLabel.Spring = true;
      this.statusLabel.Text = "ready";
      this.statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // progresBar
      // 
      this.progresBar.AutoSize = false;
      this.progresBar.Name = "progresBar";
      this.progresBar.Size = new System.Drawing.Size(100, 18);
      this.progresBar.Visible = false;
      // 
      // statusLabelRecords
      // 
      this.statusLabelRecords.AutoSize = false;
      this.statusLabelRecords.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                  | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                  | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
      this.statusLabelRecords.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
      this.statusLabelRecords.Name = "statusLabelRecords";
      this.statusLabelRecords.Size = new System.Drawing.Size(160, 19);
      // 
      // splitter1
      // 
      this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter1.Enabled = false;
      this.splitter1.Location = new System.Drawing.Point(0, 616);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(921, 1);
      this.splitter1.TabIndex = 7;
      this.splitter1.TabStop = false;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.splContDRQuery);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel1.Location = new System.Drawing.Point(0, 25);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(921, 591);
      this.panel1.TabIndex = 8;
      // 
      // splContDRQuery
      // 
      this.splContDRQuery.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splContDRQuery.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
      this.splContDRQuery.Location = new System.Drawing.Point(0, 0);
      this.splContDRQuery.Name = "splContDRQuery";
      this.splContDRQuery.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splContDRQuery.Panel1
      // 
      this.splContDRQuery.Panel1.Controls.Add(this.tbcComponents);
      this.splContDRQuery.Panel1MinSize = 50;
      // 
      // splContDRQuery.Panel2
      // 
      this.splContDRQuery.Panel2.Controls.Add(this.lbMessages);
      this.splContDRQuery.Panel2MinSize = 50;
      this.splContDRQuery.Size = new System.Drawing.Size(921, 591);
      this.splContDRQuery.SplitterDistance = 538;
      this.splContDRQuery.SplitterWidth = 3;
      this.splContDRQuery.TabIndex = 3;
      // 
      // tbcComponents
      // 
      this.tbcComponents.Controls.Add(this.tabSelector);
      this.tbcComponents.Controls.Add(this.tabVwMasterSlave);
      this.tbcComponents.Controls.Add(this.tabVwTreeList);
      this.tbcComponents.Controls.Add(this.tabVwVariables);
      this.tbcComponents.Controls.Add(this.tabVwStatistics);
      this.tbcComponents.Controls.Add(this.tabVwGrouped);
      this.tbcComponents.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbcComponents.Location = new System.Drawing.Point(0, 0);
      this.tbcComponents.Name = "tbcComponents";
      this.tbcComponents.Padding = new System.Drawing.Point(3, 3);
      this.tbcComponents.SelectedIndex = 0;
      this.tbcComponents.Size = new System.Drawing.Size(921, 538);
      this.tbcComponents.TabIndex = 2;
      // 
      // tabSelector
      // 
      this.DRViewHelpProvider.SetHelpKeyword(this.tabSelector, "Selector.htm");
      this.DRViewHelpProvider.SetHelpNavigator(this.tabSelector, System.Windows.Forms.HelpNavigator.Topic);
      this.tabSelector.Location = new System.Drawing.Point(4, 22);
      this.tabSelector.Name = "tabSelector";
      this.tabSelector.Padding = new System.Windows.Forms.Padding(3);
      this.DRViewHelpProvider.SetShowHelp(this.tabSelector, true);
      this.tabSelector.Size = new System.Drawing.Size(913, 512);
      this.tabSelector.TabIndex = 0;
      this.tabSelector.Text = "Query";
      this.tabSelector.ToolTipText = "Selects records from data source";
      this.tabSelector.UseVisualStyleBackColor = true;
      // 
      // tabVwMasterSlave
      // 
      this.tabVwMasterSlave.Controls.Add(this.drViewMS);
      this.tabVwMasterSlave.Location = new System.Drawing.Point(4, 22);
      this.tabVwMasterSlave.Name = "tabVwMasterSlave";
      this.tabVwMasterSlave.Padding = new System.Windows.Forms.Padding(3);
      this.tabVwMasterSlave.Size = new System.Drawing.Size(913, 511);
      this.tabVwMasterSlave.TabIndex = 1;
      this.tabVwMasterSlave.Text = "Grid view";
      this.tabVwMasterSlave.ToolTipText = "Displays selected records in master-slave grid";
      this.tabVwMasterSlave.UseVisualStyleBackColor = true;
      // 
      // drViewMS
      // 
      this.drViewMS.DDDHelper = null;
      this.drViewMS.DisplayData = true;
      this.drViewMS.Dock = System.Windows.Forms.DockStyle.Fill;
      this.DRViewHelpProvider.SetHelpKeyword(this.drViewMS, "ViewGrid.htm");
      this.DRViewHelpProvider.SetHelpNavigator(this.drViewMS, System.Windows.Forms.HelpNavigator.Topic);
      this.drViewMS.Location = new System.Drawing.Point(3, 3);
      this.drViewMS.Name = "drViewMS";
      this.DRViewHelpProvider.SetShowHelp(this.drViewMS, true);
      this.drViewMS.Size = new System.Drawing.Size(907, 505);
      this.drViewMS.StartTimeColor = System.Drawing.Color.Red;
      this.drViewMS.TabIndex = 0;
      this.drViewMS.ProgressReport += new DRSelectorComponents.ReportProgressEventHandler(this.ProgressReportEvent);
      this.drViewMS.InfoMessage += new DRSelectorComponents.InfoMessageEventhandler(this.InfoMessageEvent);
      this.drViewMS.ErrorMessage += new DRSelectorComponents.ErrorMessageEventhandler(this.ErrorMessageEvent);
      // 
      // tabVwTreeList
      // 
      this.tabVwTreeList.Controls.Add(this.drViewTreeList);
      this.tabVwTreeList.Location = new System.Drawing.Point(4, 22);
      this.tabVwTreeList.Name = "tabVwTreeList";
      this.tabVwTreeList.Padding = new System.Windows.Forms.Padding(3);
      this.tabVwTreeList.Size = new System.Drawing.Size(913, 511);
      this.tabVwTreeList.TabIndex = 2;
      this.tabVwTreeList.Text = "Tree view";
      this.tabVwTreeList.UseVisualStyleBackColor = true;
      // 
      // drViewTreeList
      // 
      this.drViewTreeList.DDDHelper = null;
      this.drViewTreeList.DisplayData = true;
      this.drViewTreeList.Dock = System.Windows.Forms.DockStyle.Fill;
      this.DRViewHelpProvider.SetHelpKeyword(this.drViewTreeList, "ViewTree.htm");
      this.DRViewHelpProvider.SetHelpNavigator(this.drViewTreeList, System.Windows.Forms.HelpNavigator.Topic);
      this.drViewTreeList.Location = new System.Drawing.Point(3, 3);
      this.drViewTreeList.Name = "drViewTreeList";
      this.DRViewHelpProvider.SetShowHelp(this.drViewTreeList, true);
      this.drViewTreeList.Size = new System.Drawing.Size(907, 505);
      this.drViewTreeList.TabIndex = 0;
      this.drViewTreeList.ProgressReport += new DRSelectorComponents.ReportProgressEventHandler(this.ProgressReportEvent);
      this.drViewTreeList.InfoMessage += new DRSelectorComponents.InfoMessageEventhandler(this.InfoMessageEvent);
      this.drViewTreeList.ErrorMessage += new DRSelectorComponents.ErrorMessageEventhandler(this.ErrorMessageEvent);
      // 
      // tabVwVariables
      // 
      this.tabVwVariables.Controls.Add(this.drViewVariables);
      this.tabVwVariables.Location = new System.Drawing.Point(4, 22);
      this.tabVwVariables.Name = "tabVwVariables";
      this.tabVwVariables.Padding = new System.Windows.Forms.Padding(3);
      this.tabVwVariables.Size = new System.Drawing.Size(913, 511);
      this.tabVwVariables.TabIndex = 3;
      this.tabVwVariables.Text = "Variables view";
      this.tabVwVariables.UseVisualStyleBackColor = true;
      // 
      // drViewVariables
      // 
      this.drViewVariables.DDDHelper = null;
      this.drViewVariables.DisplayData = true;
      this.drViewVariables.Dock = System.Windows.Forms.DockStyle.Fill;
      this.DRViewHelpProvider.SetHelpKeyword(this.drViewVariables, "ViewVariables.htm");
      this.DRViewHelpProvider.SetHelpNavigator(this.drViewVariables, System.Windows.Forms.HelpNavigator.Topic);
      this.drViewVariables.Location = new System.Drawing.Point(3, 3);
      this.drViewVariables.Name = "drViewVariables";
      this.DRViewHelpProvider.SetShowHelp(this.drViewVariables, true);
      this.drViewVariables.Size = new System.Drawing.Size(907, 505);
      this.drViewVariables.TabIndex = 0;
      this.drViewVariables.OnGraphVariables += new DRViewComponents.GraphVariablesEventHandler(this.drViewVariables_OnGraphVariables);
      this.drViewVariables.ProgressReport += new DRSelectorComponents.ReportProgressEventHandler(this.ProgressReportEvent);
      this.drViewVariables.InfoMessage += new DRSelectorComponents.InfoMessageEventhandler(this.InfoMessageEvent);
      this.drViewVariables.ErrorMessage += new DRSelectorComponents.ErrorMessageEventhandler(this.ErrorMessageEvent);
      // 
      // tabVwStatistics
      // 
      this.tabVwStatistics.Controls.Add(this.drViewStatistics);
      this.tabVwStatistics.Location = new System.Drawing.Point(4, 22);
      this.tabVwStatistics.Name = "tabVwStatistics";
      this.tabVwStatistics.Padding = new System.Windows.Forms.Padding(3);
      this.tabVwStatistics.Size = new System.Drawing.Size(913, 511);
      this.tabVwStatistics.TabIndex = 4;
      this.tabVwStatistics.Text = "Statistics";
      this.tabVwStatistics.UseVisualStyleBackColor = true;
      // 
      // drViewStatistics
      // 
      this.drViewStatistics.DDDHelper = null;
      this.drViewStatistics.DisplayData = true;
      this.drViewStatistics.Dock = System.Windows.Forms.DockStyle.Fill;
      this.DRViewHelpProvider.SetHelpKeyword(this.drViewStatistics, "ViewStatistics.htm");
      this.DRViewHelpProvider.SetHelpNavigator(this.drViewStatistics, System.Windows.Forms.HelpNavigator.Topic);
      this.drViewStatistics.Location = new System.Drawing.Point(3, 3);
      this.drViewStatistics.Name = "drViewStatistics";
      this.DRViewHelpProvider.SetShowHelp(this.drViewStatistics, true);
      this.drViewStatistics.Size = new System.Drawing.Size(907, 505);
      this.drViewStatistics.TabIndex = 0;
      this.drViewStatistics.ProgressReport += new DRSelectorComponents.ReportProgressEventHandler(this.ProgressReportEvent);
      this.drViewStatistics.InfoMessage += new DRSelectorComponents.InfoMessageEventhandler(this.InfoMessageEvent);
      this.drViewStatistics.ErrorMessage += new DRSelectorComponents.ErrorMessageEventhandler(this.ErrorMessageEvent);
      // 
      // tabVwGrouped
      // 
      this.tabVwGrouped.Controls.Add(this.drViewGrouped);
      this.tabVwGrouped.Location = new System.Drawing.Point(4, 22);
      this.tabVwGrouped.Name = "tabVwGrouped";
      this.tabVwGrouped.Padding = new System.Windows.Forms.Padding(3);
      this.tabVwGrouped.Size = new System.Drawing.Size(913, 511);
      this.tabVwGrouped.TabIndex = 5;
      this.tabVwGrouped.Text = "Grouped view";
      this.tabVwGrouped.UseVisualStyleBackColor = true;
      // 
      // drViewGrouped
      // 
      this.drViewGrouped.DDDHelper = null;
      this.drViewGrouped.DisplayData = true;
      this.drViewGrouped.Dock = System.Windows.Forms.DockStyle.Fill;
      this.DRViewHelpProvider.SetHelpKeyword(this.drViewGrouped, "ViewGraph.htm");
      this.DRViewHelpProvider.SetHelpNavigator(this.drViewGrouped, System.Windows.Forms.HelpNavigator.Topic);
      this.drViewGrouped.Location = new System.Drawing.Point(3, 3);
      this.drViewGrouped.Name = "drViewGrouped";
      this.DRViewHelpProvider.SetShowHelp(this.drViewGrouped, true);
      this.drViewGrouped.Size = new System.Drawing.Size(907, 505);
      this.drViewGrouped.TabIndex = 0;
      this.drViewGrouped.ProgressReport += new DRSelectorComponents.ReportProgressEventHandler(this.ProgressReportEvent);
      this.drViewGrouped.InfoMessage += new DRSelectorComponents.InfoMessageEventhandler(this.InfoMessageEvent);
      this.drViewGrouped.ErrorMessage += new DRSelectorComponents.ErrorMessageEventhandler(this.ErrorMessageEvent);
      // 
      // lbMessages
      // 
      this.lbMessages.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbMessages.FormattingEnabled = true;
      this.lbMessages.Location = new System.Drawing.Point(0, 0);
      this.lbMessages.Name = "lbMessages";
      this.lbMessages.Size = new System.Drawing.Size(921, 43);
      this.lbMessages.TabIndex = 0;
      // 
      // DRViewHelpProvider
      // 
      this.DRViewHelpProvider.HelpNamespace = "DRView.chm";
      // 
      // drExport
      // 
      this.drExport.DDDHelper = null;
      this.drExport.Selector = null;
      this.drExport.ProgressReport += new DRSelectorComponents.ReportProgressEventHandler(this.ProgressReportEvent);
      this.drExport.InfoMessage += new DRSelectorComponents.InfoMessageEventhandler(this.InfoMessageEvent);
      this.drExport.ErrorMessage += new DRSelectorComponents.ErrorMessageEventhandler(this.ErrorMessageEvent);
      // 
      // persistWindow
      // 
      this.persistWindow.Form = this;
      this.persistWindow.UseLocAppDataDir = true;
      this.persistWindow.XMLFilePath = "DRViewWindowState.xml";
      // 
      // DRQuery
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(921, 641);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.splitter1);
      this.Controls.Add(this.statusStrip);
      this.Controls.Add(this.toolStrip);
      this.Controls.Add(this.msDRQuery);
      this.DRViewHelpProvider.SetHelpKeyword(this, "DRQueryForm.htm");
      this.DRViewHelpProvider.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MainMenuStrip = this.msDRQuery;
      this.MinimumSize = new System.Drawing.Size(600, 500);
      this.Name = "DRQuery";
      this.DRViewHelpProvider.SetShowHelp(this, true);
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Diagnostic records query";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DRQuery_FormClosed);
      this.toolStrip.ResumeLayout(false);
      this.toolStrip.PerformLayout();
      this.msDRQuery.ResumeLayout(false);
      this.msDRQuery.PerformLayout();
      this.statusStrip.ResumeLayout(false);
      this.statusStrip.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.splContDRQuery.Panel1.ResumeLayout(false);
      this.splContDRQuery.Panel2.ResumeLayout(false);
      this.splContDRQuery.ResumeLayout(false);
      this.tbcComponents.ResumeLayout(false);
      this.tabVwMasterSlave.ResumeLayout(false);
      this.tabVwTreeList.ResumeLayout(false);
      this.tabVwVariables.ResumeLayout(false);
      this.tabVwStatistics.ResumeLayout(false);
      this.tabVwGrouped.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ToolStrip toolStrip;
    private System.Windows.Forms.ToolStripButton tsbShowMessages;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.ToolStripButton tsbShowGridView;
    private System.Windows.Forms.ToolStripButton tsbShowTreeView;
    private System.Windows.Forms.ToolStripButton tsbShowVarsView;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.SplitContainer splContDRQuery;
    private System.Windows.Forms.TabControl tbcComponents;
    private System.Windows.Forms.TabPage tabSelector;
    private System.Windows.Forms.TabPage tabVwMasterSlave;
    private DRViewComponents.DRViewMaster_Slave drViewMS;
    private System.Windows.Forms.TabPage tabVwTreeList;
    private DRViewComponents.DRViewTreeList drViewTreeList;
    private System.Windows.Forms.TabPage tabVwVariables;
    private DRViewComponents.DRViewVariables drViewVariables;
    private System.Windows.Forms.MenuStrip msDRQuery;
    private System.Windows.Forms.ToolStripMenuItem queryToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem gridViewToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem treeViewToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem variablesViewToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
    private System.Windows.Forms.ToolStripMenuItem saveSelectedDataToolStripMenuItem;
    private System.Windows.Forms.SaveFileDialog dlgSaveFile;
    private System.Windows.Forms.OpenFileDialog dlgOpenFile;
    private System.Windows.Forms.StatusStrip statusStrip;
    private System.Windows.Forms.ToolStripStatusLabel statusLabel;
    private System.Windows.Forms.ToolStripProgressBar progresBar;
    private System.Windows.Forms.Splitter splitter1;
    private System.Windows.Forms.ListBox lbMessages;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.ToolStripButton tsbSaveRecords;
    private System.Windows.Forms.ToolStripMenuItem systemMessagesToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem exportRecordsToolStripMenuItem;
    private DRViewComponents.DRExport drExport;
    private System.Windows.Forms.ToolStripButton tsbExportRecords;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
    private System.Windows.Forms.ToolStripButton tsbShowFilterDialog;
    private System.Windows.Forms.ToolStripButton tsbApplyFilter;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
    private System.Windows.Forms.ToolStripMenuItem defineFilterToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem applyFilterToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
    private System.Windows.Forms.ToolStripStatusLabel statusLabelRecords;
    private System.Windows.Forms.TabPage tabVwStatistics;
    private DRViewComponents.DRViewStatistics drViewStatistics;
    private System.Windows.Forms.ToolStripButton tsbShowStatistics;
    private System.Windows.Forms.ToolStripMenuItem statisticsToolStripMenuItem;
    private System.Windows.Forms.TabPage tabVwGrouped;
    private DRViewComponents.DRViewGrouped drViewGrouped;
    private System.Windows.Forms.ToolStripButton tsbShowGrouped;
    private System.Windows.Forms.ToolStripMenuItem groupedViewToolStripMenuItem;
    private System.Windows.Forms.ToolStripButton tsbNewGraph;
    private System.Windows.Forms.ToolStripMenuItem newGraphToolStripMenuItem;
    private System.Windows.Forms.HelpProvider DRViewHelpProvider;
    private System.Windows.Forms.ToolStripButton tsbSelect;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
    private System.Windows.Forms.ToolStripMenuItem executeQueryToolStripMenuItem;
    private System.Windows.Forms.ToolStripButton tsbSaveQuery;
    private System.Windows.Forms.ToolStripMenuItem saveQueryToolStripMenuItem;
    private System.Windows.Forms.ToolStripButton tsbShowMap;
    private System.Windows.Forms.ToolStripMenuItem mapViewToolStripMenuItem;
    private Mowog.PersistWindowComponent persistWindow;

  }
}