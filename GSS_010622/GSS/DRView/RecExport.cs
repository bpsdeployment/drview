using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DRViewComponents;
using DDDObjects;
using DRSelectorComponents;

namespace DRView
{
  /// <summary>
  /// Dialog providing the possibility to select export method.
  /// </summary>
  public partial class RecExport : Form
  {
    public DDDHelper DDDHelper;
    public DRSelectorBase Selector;
    /// <summary>
    /// Attach this handler to the source of NewDataReady event (Selector component)
    /// </summary>
    public NDREventHandler NewDataReadyHandler;

    /// <summary>
    /// Dialog constructor, stores passed selector and helper references
    /// </summary>
    public RecExport(DRSelectorBase selector, DDDHelper dddHelper)
    {
      InitializeComponent();
      this.DDDHelper = dddHelper;
      this.Selector = selector;
      varTreeView.DDDHelper = dddHelper;
      //Initialize public delegate
      NewDataReadyHandler = OnNewDataReady;
      //Initialize time format
      tbTimeFormat.Text = dddHelper.TimeFormat;
    }

    /// <summary>
    /// Opens save file dialog with predefined filters
    /// </summary>
    private void btSetFileName_Click(object sender, EventArgs e)
    {
      if (rbRecCSV.Checked | rbVarCSV.Checked)
      { //save csv file
        saveFileDialog.Filter = "CSV files|*.csv";
        saveFileDialog.DefaultExt = "csv";
      }
      else
      { //save xml file
        saveFileDialog.Filter = "XML files|*.xml";
        saveFileDialog.DefaultExt = "xml";
      }
      //Display dialog
      DialogResult diagRes = saveFileDialog.ShowDialog();
      if (diagRes == DialogResult.OK)
        tbFileName.Text = saveFileDialog.FileName;
    }

    /// <summary>
    /// Enables Export button, if there is any file name specified
    /// </summary>
    private void tbFileName_TextChanged(object sender, EventArgs e)
    {
      if (tbFileName.Text != "")
        btnExport.Enabled = true;
      else
        btnExport.Enabled = false;
    }

    /// <summary>
    /// Enables / disables groups of controls based on type of export selected
    /// </summary>
    private void ExportTypeChanged(object sender, EventArgs e)
    {
      if (rbRecCSV.Checked)
      {
        gbVariables.Enabled = false;
        gbRecordAttributes.Enabled = true;
        tbSeparator.Enabled = true;
        chbCompleteVarTitle.Enabled = false;
        chbIncludeDDDVer.Enabled = false;
      }
      else if (rbVarCSV.Checked)
      {
        gbVariables.Enabled = true;
        gbRecordAttributes.Enabled = false;
        tbSeparator.Enabled = true;
        chbCompleteVarTitle.Enabled = true;
        chbIncludeDDDVer.Enabled = true;
      }
    }

    /// <summary>
    /// Creates enumeration representing selected records for export
    /// </summary>
    public DRExport.RecordsCSVColumns SelectedRecAttributes()
    {
      DRExport.RecordsCSVColumns ret = DRExport.RecordsCSVColumns.None;
      if (chbTrainName.Checked)
        ret |= DRExport.RecordsCSVColumns.TrainName;
      if (chbVehicleName.Checked)
        ret |= DRExport.RecordsCSVColumns.VehicleName;
      if (chbTUInstance.Checked)
        ret |= DRExport.RecordsCSVColumns.TUInstanceName;
      if (chbTUType.Checked)
        ret |= DRExport.RecordsCSVColumns.TUTypeName;
      if (chbVehicleNumber.Checked)
        ret |= DRExport.RecordsCSVColumns.VehicleNumber;
      if (chbSource.Checked)
        ret |= DRExport.RecordsCSVColumns.Source;
      if (chbDeviceCode.Checked)
        ret |= DRExport.RecordsCSVColumns.DeviceCode;
      if (chbStartTime.Checked)
        ret |= DRExport.RecordsCSVColumns.StartTime;
      if (chbEndTime.Checked)
        ret |= DRExport.RecordsCSVColumns.EndTime;
      if (chbLastModified.Checked)
        ret |= DRExport.RecordsCSVColumns.LastModified;
      if (chbAcknowledgeTime.Checked)
        ret |= DRExport.RecordsCSVColumns.AcknowledgeTime;
      if (chbImportTime.Checked)
        ret |= DRExport.RecordsCSVColumns.ImportTime;
      if (chbDDDVersion.Checked)
        ret |= DRExport.RecordsCSVColumns.DDDVersionName;
      if (chbRecTitle.Checked)
        ret |= DRExport.RecordsCSVColumns.RecTitle;
      if (chbRecName.Checked)
        ret |= DRExport.RecordsCSVColumns.RecordName;
      if (chbFaultCode.Checked)
        ret |= DRExport.RecordsCSVColumns.FaultCode;
      if (chbTrcSnpCode.Checked)
        ret |= DRExport.RecordsCSVColumns.TrcSnpCode;
      if (chbRecordType.Checked)
        ret |= DRExport.RecordsCSVColumns.RecordType;
      if (chbFaultSeverity.Checked)
        ret |= DRExport.RecordsCSVColumns.FaultSeverity;
      if (chbFaultSubSeverity.Checked)
        ret |= DRExport.RecordsCSVColumns.FaultSubSeverity;
      if (chbGPSLatitude.Checked)
        ret |= DRExport.RecordsCSVColumns.GPSLatitude;
      if (chbGPSLongitude.Checked)
        ret |= DRExport.RecordsCSVColumns.GPSLongitude;
      if (chbTypeCount.Checked)
        ret |= DRExport.RecordsCSVColumns.TypeCount;
      return ret;
    }

    /// <summary>
    /// Preselect record attributes according to attributes selectedin view configuration
    /// </summary>
    /// <param name="viewConfiguration"></param>
    public void SelectRecAttributes(ViewConfiguration viewConfiguration)
    {
      foreach(ViewConfiguration.ViewAttribute attribute in viewConfiguration.Attributes)
        switch (attribute.DataField)
        {
          case "TUInstanceName":
            chbTUInstance.Checked = true;
            break;
          case "VehicleNumber":
            chbVehicleNumber.Checked = true;
            break;
          case "Source":
            chbSource.Checked = true;
            break;
          case "DeviceCode":
            chbDeviceCode.Checked = true;
            break;
          case "FaultCode":
            chbFaultCode.Checked = true;
            break;
          case "TrcSnpCode":
            chbTrcSnpCode.Checked = true;
            break;
          case "RecTitle":
            chbRecTitle.Checked = true;
            break;
          case "StartTime":
            chbStartTime.Checked = true;
            break;
          case "EndTime":
            chbEndTime.Checked = true;
            break;
          case "AcknowledgeTime":
            chbAcknowledgeTime.Checked = true;
            break;
          case "FaultSeverity":
            chbFaultSeverity.Checked = true;
            break;
          case "FaultSubSeverity":
            chbFaultSubSeverity.Checked = true;
            break;
          case "VehicleName":
            chbVehicleName.Checked = true;
            break;
          case "TrainName":
            chbTrainName.Checked = true;
            break;
          case "LastModified":
            chbLastModified.Checked = true;
            break;
          case "TUTypeName":
            chbTUType.Checked = true;
            break;
          case "DDDVersionName":
            chbDDDVersion.Checked = true;
            break;
          case "RecordName":
            chbRecName.Checked = true;
            break;
          case "ImportTime":
            chbImportTime.Checked = true;
            break;
          case "RecordType":
            chbRecordType.Checked = true;
            break;
          case "GPSLatitude":
            chbGPSLatitude.Checked = true;
            break;
          case "GPSLongitude":
            chbGPSLongitude.Checked = true;
            break;
        }
    }

    /// <summary>
    /// Method handles NewDataReady event raised by SelectorComponent.
    /// Displays tree of variables for available records
    /// </summary>
    protected virtual void OnNewDataReady(DRSelectorBase sender, NDREventArgs args)
    {
      Selector = sender;
      //Create variable tree
      varTreeView.CreateVarTree(Selector.RecordsView, Selector.VariableFilter);

    }
  }
}