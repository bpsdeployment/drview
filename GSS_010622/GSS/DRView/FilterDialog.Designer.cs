namespace DRView
{
  partial class FilterDialog
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FilterDialog));
      this.gbRecords = new System.Windows.Forms.GroupBox();
      this.tbFaultSubseverities = new System.Windows.Forms.TextBox();
      this.label13 = new System.Windows.Forms.Label();
      this.label11 = new System.Windows.Forms.Label();
      this.chbSevC = new System.Windows.Forms.CheckBox();
      this.chbSevB = new System.Windows.Forms.CheckBox();
      this.chbSevB1 = new System.Windows.Forms.CheckBox();
      this.chbSevA = new System.Windows.Forms.CheckBox();
      this.chbSevA1 = new System.Windows.Forms.CheckBox();
      this.label10 = new System.Windows.Forms.Label();
      this.tbVehicleNumbers = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.tbTrcSnpCodes = new System.Windows.Forms.TextBox();
      this.tbFaultCodes = new System.Windows.Forms.TextBox();
      this.gbRecType = new System.Windows.Forms.GroupBox();
      this.chbSnapRec = new System.Windows.Forms.CheckBox();
      this.chbTraceRec = new System.Windows.Forms.CheckBox();
      this.chbEventRec = new System.Windows.Forms.CheckBox();
      this.tbRecSources = new System.Windows.Forms.TextBox();
      this.tbTUNames = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.chbMatchAllKeywords = new System.Windows.Forms.CheckBox();
      this.tbDDDVersions = new System.Windows.Forms.TextBox();
      this.tbTitleKeywords = new System.Windows.Forms.TextBox();
      this.label8 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.dtpStartTo = new System.Windows.Forms.DateTimePicker();
      this.dtpStartFrom = new System.Windows.Forms.DateTimePicker();
      this.label5 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.gbFilterStrings = new System.Windows.Forms.GroupBox();
      this.label12 = new System.Windows.Forms.Label();
      this.tbRecFilterStr = new System.Windows.Forms.TextBox();
      this.chbModifyFilters = new System.Windows.Forms.CheckBox();
      this.btApply = new System.Windows.Forms.Button();
      this.btCancel = new System.Windows.Forms.Button();
      this.btRemoveFilter = new System.Windows.Forms.Button();
      this.toolStrip1 = new System.Windows.Forms.ToolStrip();
      this.tsbLoadFilter = new System.Windows.Forms.ToolStripButton();
      this.tsbSaveFilter = new System.Windows.Forms.ToolStripButton();
      this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
      this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
      this.DRViewHelpProvider = new System.Windows.Forms.HelpProvider();
      this.tabCtrlHierVars = new System.Windows.Forms.TabControl();
      this.tpFleetHierarchy = new System.Windows.Forms.TabPage();
      this.fleetHierarchy = new DRSelectorComponents.FleetHierarchyTree();
      this.tpVariables = new System.Windows.Forms.TabPage();
      this.varTreeView = new DRViewComponents.VarTreeView();
      this.persistWindow = new Mowog.PersistWindowComponent(this.components);
      this.gbRecords.SuspendLayout();
      this.gbRecType.SuspendLayout();
      this.gbFilterStrings.SuspendLayout();
      this.toolStrip1.SuspendLayout();
      this.tabCtrlHierVars.SuspendLayout();
      this.tpFleetHierarchy.SuspendLayout();
      this.tpVariables.SuspendLayout();
      this.SuspendLayout();
      // 
      // gbRecords
      // 
      this.gbRecords.Controls.Add(this.tbFaultSubseverities);
      this.gbRecords.Controls.Add(this.label13);
      this.gbRecords.Controls.Add(this.label11);
      this.gbRecords.Controls.Add(this.chbSevC);
      this.gbRecords.Controls.Add(this.chbSevB);
      this.gbRecords.Controls.Add(this.chbSevB1);
      this.gbRecords.Controls.Add(this.chbSevA);
      this.gbRecords.Controls.Add(this.chbSevA1);
      this.gbRecords.Controls.Add(this.label10);
      this.gbRecords.Controls.Add(this.tbVehicleNumbers);
      this.gbRecords.Controls.Add(this.label4);
      this.gbRecords.Controls.Add(this.tbTrcSnpCodes);
      this.gbRecords.Controls.Add(this.tbFaultCodes);
      this.gbRecords.Controls.Add(this.gbRecType);
      this.gbRecords.Controls.Add(this.tbRecSources);
      this.gbRecords.Controls.Add(this.tbTUNames);
      this.gbRecords.Controls.Add(this.label1);
      this.gbRecords.Controls.Add(this.label9);
      this.gbRecords.Controls.Add(this.chbMatchAllKeywords);
      this.gbRecords.Controls.Add(this.tbDDDVersions);
      this.gbRecords.Controls.Add(this.tbTitleKeywords);
      this.gbRecords.Controls.Add(this.label8);
      this.gbRecords.Controls.Add(this.label7);
      this.gbRecords.Controls.Add(this.label6);
      this.gbRecords.Controls.Add(this.dtpStartTo);
      this.gbRecords.Controls.Add(this.dtpStartFrom);
      this.gbRecords.Controls.Add(this.label5);
      this.gbRecords.Controls.Add(this.label3);
      this.gbRecords.Controls.Add(this.label2);
      this.gbRecords.Location = new System.Drawing.Point(4, 29);
      this.gbRecords.Name = "gbRecords";
      this.gbRecords.Size = new System.Drawing.Size(298, 334);
      this.gbRecords.TabIndex = 0;
      this.gbRecords.TabStop = false;
      this.gbRecords.Text = "Records";
      // 
      // tbFaultSubseverities
      // 
      this.tbFaultSubseverities.Location = new System.Drawing.Point(6, 268);
      this.tbFaultSubseverities.Name = "tbFaultSubseverities";
      this.tbFaultSubseverities.Size = new System.Drawing.Size(140, 20);
      this.tbFaultSubseverities.TabIndex = 50;
      this.tbFaultSubseverities.TextChanged += new System.EventHandler(this.FilterChanged);
      this.tbFaultSubseverities.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericTextBox_KeyPress);
      // 
      // label13
      // 
      this.label13.AutoSize = true;
      this.label13.Location = new System.Drawing.Point(3, 252);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(97, 13);
      this.label13.TabIndex = 49;
      this.label13.Text = "Fault subseverities:";
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(3, 216);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(80, 13);
      this.label11.TabIndex = 48;
      this.label11.Text = "Fault severities:";
      // 
      // chbSevC
      // 
      this.chbSevC.AutoSize = true;
      this.chbSevC.Location = new System.Drawing.Point(174, 232);
      this.chbSevC.Name = "chbSevC";
      this.chbSevC.Size = new System.Drawing.Size(33, 17);
      this.chbSevC.TabIndex = 47;
      this.chbSevC.Text = "C";
      this.chbSevC.UseVisualStyleBackColor = true;
      this.chbSevC.CheckedChanged += new System.EventHandler(this.FilterChanged);
      // 
      // chbSevB
      // 
      this.chbSevB.AutoSize = true;
      this.chbSevB.Location = new System.Drawing.Point(135, 232);
      this.chbSevB.Name = "chbSevB";
      this.chbSevB.Size = new System.Drawing.Size(33, 17);
      this.chbSevB.TabIndex = 46;
      this.chbSevB.Text = "B";
      this.chbSevB.UseVisualStyleBackColor = true;
      this.chbSevB.CheckedChanged += new System.EventHandler(this.FilterChanged);
      // 
      // chbSevB1
      // 
      this.chbSevB1.AutoSize = true;
      this.chbSevB1.Location = new System.Drawing.Point(90, 232);
      this.chbSevB1.Name = "chbSevB1";
      this.chbSevB1.Size = new System.Drawing.Size(39, 17);
      this.chbSevB1.TabIndex = 45;
      this.chbSevB1.Text = "B1";
      this.chbSevB1.UseVisualStyleBackColor = true;
      this.chbSevB1.CheckedChanged += new System.EventHandler(this.FilterChanged);
      // 
      // chbSevA
      // 
      this.chbSevA.AutoSize = true;
      this.chbSevA.Location = new System.Drawing.Point(51, 232);
      this.chbSevA.Name = "chbSevA";
      this.chbSevA.Size = new System.Drawing.Size(33, 17);
      this.chbSevA.TabIndex = 44;
      this.chbSevA.Text = "A";
      this.chbSevA.UseVisualStyleBackColor = true;
      this.chbSevA.CheckedChanged += new System.EventHandler(this.FilterChanged);
      // 
      // chbSevA1
      // 
      this.chbSevA1.AutoSize = true;
      this.chbSevA1.Location = new System.Drawing.Point(6, 232);
      this.chbSevA1.Name = "chbSevA1";
      this.chbSevA1.Size = new System.Drawing.Size(39, 17);
      this.chbSevA1.TabIndex = 43;
      this.chbSevA1.Text = "A1";
      this.chbSevA1.UseVisualStyleBackColor = true;
      this.chbSevA1.CheckedChanged += new System.EventHandler(this.FilterChanged);
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(149, 137);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(88, 13);
      this.label10.TabIndex = 42;
      this.label10.Text = "Vehicle numbers:";
      // 
      // tbVehicleNumbers
      // 
      this.tbVehicleNumbers.Location = new System.Drawing.Point(152, 153);
      this.tbVehicleNumbers.Name = "tbVehicleNumbers";
      this.tbVehicleNumbers.Size = new System.Drawing.Size(140, 20);
      this.tbVehicleNumbers.TabIndex = 41;
      this.tbVehicleNumbers.TextChanged += new System.EventHandler(this.FilterChanged);
      this.tbVehicleNumbers.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericTextBox_KeyPress);
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(149, 177);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(110, 13);
      this.label4.TabIndex = 40;
      this.label4.Text = "Trace or Snap codes:";
      // 
      // tbTrcSnpCodes
      // 
      this.tbTrcSnpCodes.Location = new System.Drawing.Point(152, 193);
      this.tbTrcSnpCodes.Name = "tbTrcSnpCodes";
      this.tbTrcSnpCodes.Size = new System.Drawing.Size(140, 20);
      this.tbTrcSnpCodes.TabIndex = 39;
      this.tbTrcSnpCodes.TextChanged += new System.EventHandler(this.FilterChanged);
      this.tbTrcSnpCodes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericTextBox_KeyPress);
      // 
      // tbFaultCodes
      // 
      this.tbFaultCodes.Location = new System.Drawing.Point(6, 193);
      this.tbFaultCodes.Name = "tbFaultCodes";
      this.tbFaultCodes.Size = new System.Drawing.Size(140, 20);
      this.tbFaultCodes.TabIndex = 38;
      this.tbFaultCodes.TextChanged += new System.EventHandler(this.FilterChanged);
      this.tbFaultCodes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericTextBox_KeyPress);
      // 
      // gbRecType
      // 
      this.gbRecType.Controls.Add(this.chbSnapRec);
      this.gbRecType.Controls.Add(this.chbTraceRec);
      this.gbRecType.Controls.Add(this.chbEventRec);
      this.gbRecType.Location = new System.Drawing.Point(210, 59);
      this.gbRecType.Name = "gbRecType";
      this.gbRecType.Size = new System.Drawing.Size(82, 75);
      this.gbRecType.TabIndex = 37;
      this.gbRecType.TabStop = false;
      this.gbRecType.Text = "Record type";
      // 
      // chbSnapRec
      // 
      this.chbSnapRec.AutoSize = true;
      this.chbSnapRec.Location = new System.Drawing.Point(6, 54);
      this.chbSnapRec.Name = "chbSnapRec";
      this.chbSnapRec.Size = new System.Drawing.Size(51, 17);
      this.chbSnapRec.TabIndex = 2;
      this.chbSnapRec.Text = "Snap";
      this.chbSnapRec.UseVisualStyleBackColor = true;
      this.chbSnapRec.CheckedChanged += new System.EventHandler(this.FilterChanged);
      // 
      // chbTraceRec
      // 
      this.chbTraceRec.AutoSize = true;
      this.chbTraceRec.Location = new System.Drawing.Point(6, 35);
      this.chbTraceRec.Name = "chbTraceRec";
      this.chbTraceRec.Size = new System.Drawing.Size(54, 17);
      this.chbTraceRec.TabIndex = 1;
      this.chbTraceRec.Text = "Trace";
      this.chbTraceRec.UseVisualStyleBackColor = true;
      this.chbTraceRec.CheckedChanged += new System.EventHandler(this.FilterChanged);
      // 
      // chbEventRec
      // 
      this.chbEventRec.AutoSize = true;
      this.chbEventRec.Location = new System.Drawing.Point(6, 16);
      this.chbEventRec.Name = "chbEventRec";
      this.chbEventRec.Size = new System.Drawing.Size(54, 17);
      this.chbEventRec.TabIndex = 0;
      this.chbEventRec.Text = "Event";
      this.chbEventRec.UseVisualStyleBackColor = true;
      this.chbEventRec.CheckedChanged += new System.EventHandler(this.FilterChanged);
      // 
      // tbRecSources
      // 
      this.tbRecSources.Location = new System.Drawing.Point(6, 153);
      this.tbRecSources.Name = "tbRecSources";
      this.tbRecSources.Size = new System.Drawing.Size(140, 20);
      this.tbRecSources.TabIndex = 36;
      this.tbRecSources.TextChanged += new System.EventHandler(this.FilterChanged);
      // 
      // tbTUNames
      // 
      this.tbTUNames.Location = new System.Drawing.Point(152, 307);
      this.tbTUNames.Name = "tbTUNames";
      this.tbTUNames.Size = new System.Drawing.Size(140, 20);
      this.tbTUNames.TabIndex = 4;
      this.tbTUNames.TextChanged += new System.EventHandler(this.FilterChanged);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(149, 291);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(58, 13);
      this.label1.TabIndex = 3;
      this.label1.Text = "TU Names";
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(3, 137);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(82, 13);
      this.label9.TabIndex = 35;
      this.label9.Text = "Record sources";
      // 
      // chbMatchAllKeywords
      // 
      this.chbMatchAllKeywords.AutoSize = true;
      this.chbMatchAllKeywords.Location = new System.Drawing.Point(223, 16);
      this.chbMatchAllKeywords.Name = "chbMatchAllKeywords";
      this.chbMatchAllKeywords.Size = new System.Drawing.Size(69, 17);
      this.chbMatchAllKeywords.TabIndex = 34;
      this.chbMatchAllKeywords.Text = "Match all";
      this.chbMatchAllKeywords.UseVisualStyleBackColor = true;
      this.chbMatchAllKeywords.CheckedChanged += new System.EventHandler(this.FilterChanged);
      // 
      // tbDDDVersions
      // 
      this.tbDDDVersions.Location = new System.Drawing.Point(6, 307);
      this.tbDDDVersions.Name = "tbDDDVersions";
      this.tbDDDVersions.Size = new System.Drawing.Size(140, 20);
      this.tbDDDVersions.TabIndex = 5;
      this.tbDDDVersions.TextChanged += new System.EventHandler(this.FilterChanged);
      // 
      // tbTitleKeywords
      // 
      this.tbTitleKeywords.Location = new System.Drawing.Point(6, 33);
      this.tbTitleKeywords.Name = "tbTitleKeywords";
      this.tbTitleKeywords.Size = new System.Drawing.Size(286, 20);
      this.tbTitleKeywords.TabIndex = 1;
      this.tbTitleKeywords.TextChanged += new System.EventHandler(this.FilterChanged);
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(3, 17);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(109, 13);
      this.label8.TabIndex = 15;
      this.label8.Text = "Record title keywords";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(5, 105);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(19, 13);
      this.label7.TabIndex = 14;
      this.label7.Text = "<=";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(5, 79);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(19, 13);
      this.label6.TabIndex = 13;
      this.label6.Text = ">=";
      // 
      // dtpStartTo
      // 
      this.dtpStartTo.Checked = false;
      this.dtpStartTo.CustomFormat = " yyyy-MM-dd  HH:mm:ss";
      this.dtpStartTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.dtpStartTo.Location = new System.Drawing.Point(25, 101);
      this.dtpStartTo.MinDate = new System.DateTime(2006, 1, 1, 0, 0, 0, 0);
      this.dtpStartTo.Name = "dtpStartTo";
      this.dtpStartTo.ShowCheckBox = true;
      this.dtpStartTo.Size = new System.Drawing.Size(161, 20);
      this.dtpStartTo.TabIndex = 3;
      this.dtpStartTo.ValueChanged += new System.EventHandler(this.FilterChanged);
      this.dtpStartTo.EnabledChanged += new System.EventHandler(this.FilterChanged);
      // 
      // dtpStartFrom
      // 
      this.dtpStartFrom.Checked = false;
      this.dtpStartFrom.CustomFormat = " yyyy-MM-dd  HH:mm:ss";
      this.dtpStartFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.dtpStartFrom.Location = new System.Drawing.Point(25, 75);
      this.dtpStartFrom.MinDate = new System.DateTime(2006, 1, 1, 0, 0, 0, 0);
      this.dtpStartFrom.Name = "dtpStartFrom";
      this.dtpStartFrom.ShowCheckBox = true;
      this.dtpStartFrom.Size = new System.Drawing.Size(161, 20);
      this.dtpStartFrom.TabIndex = 2;
      this.dtpStartFrom.ValueChanged += new System.EventHandler(this.FilterChanged);
      this.dtpStartFrom.EnabledChanged += new System.EventHandler(this.FilterChanged);
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(3, 60);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(55, 13);
      this.label5.TabIndex = 9;
      this.label5.Text = "Start Time";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(3, 176);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(62, 13);
      this.label3.TabIndex = 6;
      this.label3.Text = "Fault codes";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(3, 291);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(74, 13);
      this.label2.TabIndex = 4;
      this.label2.Text = "DDD Versions";
      // 
      // gbFilterStrings
      // 
      this.gbFilterStrings.Controls.Add(this.label12);
      this.gbFilterStrings.Controls.Add(this.tbRecFilterStr);
      this.gbFilterStrings.Controls.Add(this.chbModifyFilters);
      this.gbFilterStrings.Location = new System.Drawing.Point(4, 369);
      this.gbFilterStrings.Name = "gbFilterStrings";
      this.gbFilterStrings.Size = new System.Drawing.Size(602, 97);
      this.gbFilterStrings.TabIndex = 2;
      this.gbFilterStrings.TabStop = false;
      this.gbFilterStrings.Text = "Filter strings";
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(3, 20);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(47, 13);
      this.label12.TabIndex = 19;
      this.label12.Text = "Records";
      // 
      // tbRecFilterStr
      // 
      this.tbRecFilterStr.Enabled = false;
      this.tbRecFilterStr.Location = new System.Drawing.Point(6, 36);
      this.tbRecFilterStr.Multiline = true;
      this.tbRecFilterStr.Name = "tbRecFilterStr";
      this.tbRecFilterStr.Size = new System.Drawing.Size(589, 52);
      this.tbRecFilterStr.TabIndex = 12;
      // 
      // chbModifyFilters
      // 
      this.chbModifyFilters.AutoSize = true;
      this.chbModifyFilters.Location = new System.Drawing.Point(538, 19);
      this.chbModifyFilters.Name = "chbModifyFilters";
      this.chbModifyFilters.Size = new System.Drawing.Size(57, 17);
      this.chbModifyFilters.TabIndex = 11;
      this.chbModifyFilters.Text = "Modify";
      this.chbModifyFilters.UseVisualStyleBackColor = true;
      this.chbModifyFilters.CheckedChanged += new System.EventHandler(this.chbModifRecFilter_CheckedChanged);
      // 
      // btApply
      // 
      this.btApply.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btApply.Image = ((System.Drawing.Image)(resources.GetObject("btApply.Image")));
      this.btApply.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btApply.Location = new System.Drawing.Point(136, 472);
      this.btApply.Name = "btApply";
      this.btApply.Size = new System.Drawing.Size(79, 23);
      this.btApply.TabIndex = 14;
      this.btApply.Text = "Apply filter";
      this.btApply.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.btApply.UseVisualStyleBackColor = true;
      // 
      // btCancel
      // 
      this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btCancel.Location = new System.Drawing.Point(394, 472);
      this.btCancel.Name = "btCancel";
      this.btCancel.Size = new System.Drawing.Size(82, 23);
      this.btCancel.TabIndex = 16;
      this.btCancel.Text = "Cancel";
      this.btCancel.UseVisualStyleBackColor = true;
      // 
      // btRemoveFilter
      // 
      this.btRemoveFilter.DialogResult = System.Windows.Forms.DialogResult.Ignore;
      this.btRemoveFilter.Location = new System.Drawing.Point(265, 472);
      this.btRemoveFilter.Name = "btRemoveFilter";
      this.btRemoveFilter.Size = new System.Drawing.Size(82, 23);
      this.btRemoveFilter.TabIndex = 15;
      this.btRemoveFilter.Text = "Remove filter";
      this.btRemoveFilter.UseVisualStyleBackColor = true;
      // 
      // toolStrip1
      // 
      this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbLoadFilter,
            this.tsbSaveFilter});
      this.toolStrip1.Location = new System.Drawing.Point(0, 0);
      this.toolStrip1.Name = "toolStrip1";
      this.toolStrip1.Size = new System.Drawing.Size(608, 25);
      this.toolStrip1.TabIndex = 17;
      this.toolStrip1.Text = "toolStrip1";
      // 
      // tsbLoadFilter
      // 
      this.tsbLoadFilter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbLoadFilter.Image = ((System.Drawing.Image)(resources.GetObject("tsbLoadFilter.Image")));
      this.tsbLoadFilter.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbLoadFilter.Name = "tsbLoadFilter";
      this.tsbLoadFilter.Size = new System.Drawing.Size(23, 22);
      this.tsbLoadFilter.Text = "LoadFilter";
      this.tsbLoadFilter.ToolTipText = "Load filter from file";
      this.tsbLoadFilter.Click += new System.EventHandler(this.tsbLoadFilter_Click);
      // 
      // tsbSaveFilter
      // 
      this.tsbSaveFilter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbSaveFilter.Image = ((System.Drawing.Image)(resources.GetObject("tsbSaveFilter.Image")));
      this.tsbSaveFilter.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbSaveFilter.Name = "tsbSaveFilter";
      this.tsbSaveFilter.Size = new System.Drawing.Size(23, 22);
      this.tsbSaveFilter.Text = "Save filter to file";
      this.tsbSaveFilter.Click += new System.EventHandler(this.tsbSaveFilter_Click);
      // 
      // openFileDialog
      // 
      this.openFileDialog.DefaultExt = "flt";
      this.openFileDialog.Filter = "Filter files|*.flt";
      this.openFileDialog.Title = "Open filter file";
      // 
      // saveFileDialog
      // 
      this.saveFileDialog.DefaultExt = "flt";
      this.saveFileDialog.Filter = "Filter files|*.flt";
      this.saveFileDialog.Title = "Save filter file";
      // 
      // DRViewHelpProvider
      // 
      this.DRViewHelpProvider.HelpNamespace = "DRView.chm";
      // 
      // tabCtrlHierVars
      // 
      this.tabCtrlHierVars.Controls.Add(this.tpFleetHierarchy);
      this.tabCtrlHierVars.Controls.Add(this.tpVariables);
      this.tabCtrlHierVars.Location = new System.Drawing.Point(308, 29);
      this.tabCtrlHierVars.Name = "tabCtrlHierVars";
      this.tabCtrlHierVars.SelectedIndex = 0;
      this.tabCtrlHierVars.Size = new System.Drawing.Size(298, 334);
      this.tabCtrlHierVars.TabIndex = 18;
      // 
      // tpFleetHierarchy
      // 
      this.tpFleetHierarchy.Controls.Add(this.fleetHierarchy);
      this.tpFleetHierarchy.Location = new System.Drawing.Point(4, 22);
      this.tpFleetHierarchy.Name = "tpFleetHierarchy";
      this.tpFleetHierarchy.Padding = new System.Windows.Forms.Padding(3);
      this.tpFleetHierarchy.Size = new System.Drawing.Size(290, 308);
      this.tpFleetHierarchy.TabIndex = 0;
      this.tpFleetHierarchy.Text = "Fleet Hierarchy";
      this.tpFleetHierarchy.UseVisualStyleBackColor = true;
      // 
      // fleetHierarchy
      // 
      this.fleetHierarchy.Dock = System.Windows.Forms.DockStyle.Fill;
      this.fleetHierarchy.ImageIndex = 1;
      this.fleetHierarchy.Location = new System.Drawing.Point(3, 3);
      this.fleetHierarchy.Name = "fleetHierarchy";
      this.fleetHierarchy.SelectedImageIndex = 1;
      this.fleetHierarchy.ShowNodeToolTips = true;
      this.fleetHierarchy.Size = new System.Drawing.Size(284, 302);
      this.fleetHierarchy.TabIndex = 0;
      this.fleetHierarchy.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.fleetHierarchy_AfterCheck);
      // 
      // tpVariables
      // 
      this.tpVariables.Controls.Add(this.varTreeView);
      this.tpVariables.Location = new System.Drawing.Point(4, 22);
      this.tpVariables.Name = "tpVariables";
      this.tpVariables.Padding = new System.Windows.Forms.Padding(3);
      this.tpVariables.Size = new System.Drawing.Size(290, 308);
      this.tpVariables.TabIndex = 1;
      this.tpVariables.Text = "Environmental Variables";
      this.tpVariables.UseVisualStyleBackColor = true;
      // 
      // varTreeView
      // 
      this.varTreeView.DDDHelper = null;
      this.varTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
      this.varTreeView.Location = new System.Drawing.Point(3, 3);
      this.varTreeView.Name = "varTreeView";
      this.varTreeView.ShowBits = false;
      this.varTreeView.Size = new System.Drawing.Size(284, 302);
      this.varTreeView.TabIndex = 8;
      this.varTreeView.UseFilter = true;
      // 
      // persistWindow
      // 
      this.persistWindow.Form = this;
      this.persistWindow.UseLocAppDataDir = true;
      this.persistWindow.XMLFilePath = "DRViewWindowState.xml";
      // 
      // FilterDialog
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(608, 497);
      this.Controls.Add(this.tabCtrlHierVars);
      this.Controls.Add(this.toolStrip1);
      this.Controls.Add(this.btRemoveFilter);
      this.Controls.Add(this.btCancel);
      this.Controls.Add(this.btApply);
      this.Controls.Add(this.gbFilterStrings);
      this.Controls.Add(this.gbRecords);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.DRViewHelpProvider.SetHelpKeyword(this, "FilterDialog.htm");
      this.DRViewHelpProvider.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.MaximumSize = new System.Drawing.Size(614, 523);
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(614, 523);
      this.Name = "FilterDialog";
      this.DRViewHelpProvider.SetShowHelp(this, true);
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Filter records";
      this.Shown += new System.EventHandler(this.FilterDialog_Shown);
      this.gbRecords.ResumeLayout(false);
      this.gbRecords.PerformLayout();
      this.gbRecType.ResumeLayout(false);
      this.gbRecType.PerformLayout();
      this.gbFilterStrings.ResumeLayout(false);
      this.gbFilterStrings.PerformLayout();
      this.toolStrip1.ResumeLayout(false);
      this.toolStrip1.PerformLayout();
      this.tabCtrlHierVars.ResumeLayout(false);
      this.tpFleetHierarchy.ResumeLayout(false);
      this.tpVariables.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.GroupBox gbRecords;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.DateTimePicker dtpStartTo;
    private System.Windows.Forms.DateTimePicker dtpStartFrom;
    private System.Windows.Forms.TextBox tbTitleKeywords;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.GroupBox gbFilterStrings;
    private System.Windows.Forms.TextBox tbRecFilterStr;
    private System.Windows.Forms.CheckBox chbModifyFilters;
    private System.Windows.Forms.Button btApply;
    private System.Windows.Forms.Button btCancel;
    private System.Windows.Forms.TextBox tbDDDVersions;
    private System.Windows.Forms.TextBox tbTUNames;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Button btRemoveFilter;
    private System.Windows.Forms.ToolStrip toolStrip1;
    private System.Windows.Forms.ToolStripButton tsbLoadFilter;
    private System.Windows.Forms.ToolStripButton tsbSaveFilter;
    private System.Windows.Forms.OpenFileDialog openFileDialog;
    private System.Windows.Forms.SaveFileDialog saveFileDialog;
    private System.Windows.Forms.CheckBox chbMatchAllKeywords;
    private System.Windows.Forms.TextBox tbRecSources;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.HelpProvider DRViewHelpProvider;
    private DRViewComponents.VarTreeView varTreeView;
    private System.Windows.Forms.TabControl tabCtrlHierVars;
    private System.Windows.Forms.TabPage tpFleetHierarchy;
    private System.Windows.Forms.TabPage tpVariables;
    private DRSelectorComponents.FleetHierarchyTree fleetHierarchy;
    private Mowog.PersistWindowComponent persistWindow;
    private System.Windows.Forms.GroupBox gbRecType;
    private System.Windows.Forms.CheckBox chbSnapRec;
    private System.Windows.Forms.CheckBox chbTraceRec;
    private System.Windows.Forms.CheckBox chbEventRec;
    private System.Windows.Forms.TextBox tbFaultCodes;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox tbTrcSnpCodes;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.TextBox tbVehicleNumbers;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.CheckBox chbSevC;
    private System.Windows.Forms.CheckBox chbSevB;
    private System.Windows.Forms.CheckBox chbSevB1;
    private System.Windows.Forms.CheckBox chbSevA;
    private System.Windows.Forms.CheckBox chbSevA1;
    private System.Windows.Forms.TextBox tbFaultSubseverities;
    private System.Windows.Forms.Label label13;
  }
}