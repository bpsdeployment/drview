﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Il codice è stato generato da uno strumento.
//     Versione runtime:4.0.30319.42000
//
//     Le modifiche apportate a questo file possono provocare un comportamento non corretto e andranno perse se
//     il codice viene rigenerato.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DRView.Properties {
    using System;
    
    
    /// <summary>
    ///   Classe di risorse fortemente tipizzata per la ricerca di stringhe localizzate e così via.
    /// </summary>
    // Questa classe è stata generata automaticamente dalla classe StronglyTypedResourceBuilder.
    // tramite uno strumento quale ResGen o Visual Studio.
    // Per aggiungere o rimuovere un membro, modificare il file con estensione ResX ed eseguire nuovamente ResGen
    // con l'opzione /str oppure ricompilare il progetto VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Restituisce l'istanza di ResourceManager nella cache utilizzata da questa classe.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("DRView.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Esegue l'override della proprietà CurrentUICulture del thread corrente per tutte le
        ///   ricerche di risorse eseguite utilizzando questa classe di risorse fortemente tipizzata.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Cerca una risorsa localizzata di tipo System.Drawing.Icon simile a (Icona).
        /// </summary>
        internal static System.Drawing.Icon AdvancedQuery {
            get {
                object obj = ResourceManager.GetObject("AdvancedQuery", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a csv.
        /// </summary>
        internal static string CSVFileExtension {
            get {
                return ResourceManager.GetString("CSVFileExtension", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a CSV files|*.csv.
        /// </summary>
        internal static string CSVFilesFilter {
            get {
                return ResourceManager.GetString("CSVFilesFilter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una risorsa localizzata di tipo System.Drawing.Icon simile a (Icona).
        /// </summary>
        internal static System.Drawing.Icon DBQuery {
            get {
                object obj = ResourceManager.GetObject("DBQuery", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   Cerca una risorsa localizzata di tipo System.Drawing.Icon simile a (Icona).
        /// </summary>
        internal static System.Drawing.Icon DirectTUQuery {
            get {
                object obj = ResourceManager.GetObject("DirectTUQuery", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a dr.
        /// </summary>
        internal static string DRFileExtension {
            get {
                return ResourceManager.GetString("DRFileExtension", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a diagnostic records files|*.dr.
        /// </summary>
        internal static string DRFilesFilter {
            get {
                return ResourceManager.GetString("DRFilesFilter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a drq.
        /// </summary>
        internal static string DRQFileExtension {
            get {
                return ResourceManager.GetString("DRQFileExtension", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a diagnostic record query files|*.drq.
        /// </summary>
        internal static string DRQFilesFilter {
            get {
                return ResourceManager.GetString("DRQFilesFilter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una risorsa localizzata di tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Export {
            get {
                object obj = ResourceManager.GetObject("Export", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Cerca una risorsa localizzata di tipo System.Drawing.Icon simile a (Icona).
        /// </summary>
        internal static System.Drawing.Icon FileQuery {
            get {
                object obj = ResourceManager.GetObject("FileQuery", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   Cerca una risorsa localizzata di tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap HorizLayout {
            get {
                object obj = ResourceManager.GetObject("HorizLayout", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Cerca una risorsa localizzata di tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap logoUC {
            get {
                object obj = ResourceManager.GetObject("logoUC", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Cerca una risorsa localizzata di tipo System.Drawing.Icon simile a (Icona).
        /// </summary>
        internal static System.Drawing.Icon MainIcon {
            get {
                object obj = ResourceManager.GetObject("MainIcon", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Failed to load current fleet hierarchy from DB. {0}.
        /// </summary>
        internal static string msgFailedToLoadFleetHierarchyFromDB {
            get {
                return ResourceManager.GetString("msgFailedToLoadFleetHierarchyFromDB", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Failed to load runtime DDD objects from  {0}. {1}.
        /// </summary>
        internal static string msgFailedToLoadRuntimeObjFromDir {
            get {
                return ResourceManager.GetString("msgFailedToLoadRuntimeObjFromDir", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Failed to load list of available TU types and instances from DB. {0}.
        /// </summary>
        internal static string msgFailedToLoadTUObjectsFromDB {
            get {
                return ResourceManager.GetString("msgFailedToLoadTUObjectsFromDB", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una risorsa localizzata di tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap openFolder {
            get {
                object obj = ResourceManager.GetObject("openFolder", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Cerca una risorsa localizzata di tipo System.Drawing.Icon simile a (Icona).
        /// </summary>
        internal static System.Drawing.Icon RefAttrQuery {
            get {
                object obj = ResourceManager.GetObject("RefAttrQuery", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a drstat.
        /// </summary>
        internal static string StatFileExtension {
            get {
                return ResourceManager.GetString("StatFileExtension", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a diagnostic records statistic files|*.drstat.
        /// </summary>
        internal static string StatFilesFilter {
            get {
                return ResourceManager.GetString("StatFilesFilter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una risorsa localizzata di tipo System.Drawing.Icon simile a (Icona).
        /// </summary>
        internal static System.Drawing.Icon Statistics {
            get {
                object obj = ResourceManager.GetObject("Statistics", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a statq.
        /// </summary>
        internal static string StatQFileExtension {
            get {
                return ResourceManager.GetString("StatQFileExtension", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a statistical query files|*.statq.
        /// </summary>
        internal static string StatQFilesFilter {
            get {
                return ResourceManager.GetString("StatQFilesFilter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Advanced query.
        /// </summary>
        internal static string strAdvancedQueryTitle {
            get {
                return ResourceManager.GetString("strAdvancedQueryTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Basic Query.
        /// </summary>
        internal static string strBasicQueryTitle {
            get {
                return ResourceManager.GetString("strBasicQueryTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Count limit for records or variable values was reached, some records were not loaded..
        /// </summary>
        internal static string strCountLimitWarning {
            get {
                return ResourceManager.GetString("strCountLimitWarning", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Diagnostic records query.
        /// </summary>
        internal static string strDBQueryTitle {
            get {
                return ResourceManager.GetString("strDBQueryTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Records file.
        /// </summary>
        internal static string strFileQueryTitle {
            get {
                return ResourceManager.GetString("strFileQueryTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a invalid.
        /// </summary>
        internal static string strInvalidTimeFormat {
            get {
                return ResourceManager.GetString("strInvalidTimeFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Records {0} - {1} of {2} ({3} - {4}).
        /// </summary>
        internal static string strMapFormStatus {
            get {
                return ResourceManager.GetString("strMapFormStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Map for query.
        /// </summary>
        internal static string strMapFormTitle {
            get {
                return ResourceManager.GetString("strMapFormTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a ready.
        /// </summary>
        internal static string strReady {
            get {
                return ResourceManager.GetString("strReady", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a {0} records.
        /// </summary>
        internal static string strRecCount {
            get {
                return ResourceManager.GetString("strRecCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Query by referential attributes.
        /// </summary>
        internal static string strRefAttrQueryTitle {
            get {
                return ResourceManager.GetString("strRefAttrQueryTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Statistics file.
        /// </summary>
        internal static string strStatFileTitle {
            get {
                return ResourceManager.GetString("strStatFileTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Statistical Query.
        /// </summary>
        internal static string strStatRecParamsTitle {
            get {
                return ResourceManager.GetString("strStatRecParamsTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a Direct query to Telediagnostic Unit.
        /// </summary>
        internal static string strTUDirectQueryTitle {
            get {
                return ResourceManager.GetString("strTUDirectQueryTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una risorsa localizzata di tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap VertLayout {
            get {
                object obj = ResourceManager.GetObject("VertLayout", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
    }
}
