using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using DRViewComponents;
using BrendanGrant.Helpers.FileAssociation;

namespace DRView
{
  public enum eDBConnTypes
  {
    None = 0,
    FirstConnection = 1,
    SecondConnection = 2
  }

  public partial class frmProperties : Form
  {
    SqlConnectionStringBuilder connStrBuilder1; //Builder class for first connection string
    SqlConnectionStringBuilder connStrBuilder2; //Builder class for second connection string
    
    public frmProperties()
    {
      InitializeComponent();
      connStrBuilder1 = new SqlConnectionStringBuilder();
      connStrBuilder2 = new SqlConnectionStringBuilder();
    }

    /// <summary>
    /// Loads user settings from file and displays in properties form
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Properties_Load(object sender, EventArgs e)
    {
      //Connection type
      eDBConnTypes dbConnType = (eDBConnTypes)Properties.Settings.Default.preferredDBConn;
      chbDefConn1.Checked = false;
      chbDefConn2.Checked = false;
      if (dbConnType == eDBConnTypes.None)
        chbUseDBConnection.Checked = false;
      else
      {
        chbUseDBConnection.Checked = true;
        if (dbConnType == eDBConnTypes.FirstConnection)
          chbDefConn1.Checked = true;
        else
          chbDefConn2.Checked = true;
      }
      //Format time
      tbTimeFromat_TextChanged(this, null);
      nudTimeOffset.Enabled = !chbUseLocTime.Checked;
      //Parse first connection string
      connStrBuilder1.ConnectionString = Properties.Settings.Default.firstDBConnStr;
      gbDBConn1Params.Enabled = chbUseDBConnection.Checked;
      tbDBServerName1.Text = connStrBuilder1.DataSource;
      tbDBName1.Text = connStrBuilder1.InitialCatalog;
      if (connStrBuilder1.IntegratedSecurity)
      {
        rbDBWinAuth1.Checked = true;
        tbDBPassword1.Enabled = false;
        tbDBUser1.Enabled = false;
      }
      else
      {
        rbDBSqlAuth1.Checked = true;
        tbDBUser1.Text = connStrBuilder1.UserID;
        tbDBPassword1.Text = connStrBuilder1.Password;
        tbDBPassword1.Enabled = true;
        tbDBUser1.Enabled = true;
      }
      //Parse second connection string
      connStrBuilder2.ConnectionString = Properties.Settings.Default.secondDBConnStr;
      gbDBConn2Params.Enabled = chbUseDBConnection.Checked;
      tbDBServerName2.Text = connStrBuilder2.DataSource;
      tbDBName2.Text = connStrBuilder2.InitialCatalog;
      if (connStrBuilder2.IntegratedSecurity)
      {
        rbDBWinAuth2.Checked = true;
        tbDBPassword2.Enabled = false;
        tbDBUser2.Enabled = false;
      }
      else
      {
        rbDBSqlAuth2.Checked = true;
        tbDBUser2.Text = connStrBuilder2.UserID;
        tbDBPassword2.Text = connStrBuilder2.Password;
        tbDBPassword2.Enabled = true;
        tbDBUser2.Enabled = true;
      }
      //Check file associations
      CheckFileAssociations();
        tbBaselineDir.Text = Properties.Settings.Default.baselineDir;
    }

    /// <summary>
    /// Saves user settings from properties form to file
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btSave_Click(object sender, EventArgs e)
    {
      //Check if time format is valid
      try
      {
        lbTimeSample.Text = DateTime.Now.ToString(tbTimeFromat.Text);
        lbTimeSampleGraph.Text = DateTime.Now.ToString(tbTimeFormatGraph.Text);
      }
      catch (Exception)
      {
        MessageBox.Show("Invalid time format", "Time format", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }
      //Determine preffered DB connection
      if (!chbUseDBConnection.Checked)
        Properties.Settings.Default.preferredDBConn = (int)eDBConnTypes.None;
      else if (chbDefConn1.Checked)
        Properties.Settings.Default.preferredDBConn = (int)eDBConnTypes.FirstConnection;
      else if (chbDefConn2.Checked)
        Properties.Settings.Default.preferredDBConn = (int)eDBConnTypes.SecondConnection;
      else
        Properties.Settings.Default.preferredDBConn = (int)eDBConnTypes.None;
      //Build first connection string
      connStrBuilder1.ConnectionString = Properties.Settings.Default.firstDBConnStr;
      connStrBuilder1.DataSource = tbDBServerName1.Text;
      connStrBuilder1.InitialCatalog = tbDBName1.Text;
      connStrBuilder1.PersistSecurityInfo = true;
      if (rbDBWinAuth1.Checked)
      {
        connStrBuilder1.IntegratedSecurity = true;
        connStrBuilder1.UserID = String.Empty;
        connStrBuilder1.Password = String.Empty;
      }
      else
      {
        connStrBuilder1.IntegratedSecurity = false;
        connStrBuilder1.UserID = tbDBUser1.Text;
        connStrBuilder1.Password = tbDBPassword1.Text;
        connStrBuilder2.PersistSecurityInfo = true;
      }
      Properties.Settings.Default.firstDBConnStr = connStrBuilder1.ConnectionString;
      //Build second connection string
      connStrBuilder2.ConnectionString = Properties.Settings.Default.secondDBConnStr;
      connStrBuilder2.DataSource = tbDBServerName2.Text;
      connStrBuilder2.InitialCatalog = tbDBName2.Text;
      connStrBuilder2.PersistSecurityInfo = true;
      if (rbDBWinAuth2.Checked)
      {
        connStrBuilder2.IntegratedSecurity = true;
        connStrBuilder2.UserID = String.Empty;
        connStrBuilder2.Password = String.Empty;
      }
      else
      {
        connStrBuilder2.IntegratedSecurity = false;
        connStrBuilder2.UserID = tbDBUser2.Text;
        connStrBuilder2.Password = tbDBPassword2.Text;
        connStrBuilder2.PersistSecurityInfo = true;
      }
      Properties.Settings.Default.secondDBConnStr = connStrBuilder2.ConnectionString;
      //Clear connection string stored in connection dialog
      frmDBConnection.InvalidateLastConnString(false);
      //Save settings to file
      Properties.Settings.Default.Save();
    }

    /// <summary>
    /// Cancel properties changes
    /// </summary>
    private void btCancel_Click(object sender, EventArgs e)
    {
      //Reload last properties values
      Properties.Settings.Default.Reload();
    }

    private void btBrowseRuntimeObjDir_Click(object sender, EventArgs e)
    {
      folderBrowserDialog.SelectedPath = Properties.Settings.Default.runtimeDDDObjDir;
      if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
        Properties.Settings.Default.runtimeDDDObjDir = folderBrowserDialog.SelectedPath;
    }
    private void btBrowseBaselineDir_Click(object sender, EventArgs e)
    {
        folderBrowserDialog.SelectedPath = (Properties.Settings.Default.baselineDir != "") ? Properties.Settings.Default.baselineDir : @"C:\";
        if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                Properties.Settings.Default.baselineDir = folderBrowserDialog.SelectedPath;
                tbBaselineDir.Text = folderBrowserDialog.SelectedPath;
            }
    }

    private void frmProperties_FormClosed(object sender, FormClosedEventArgs e)
    {
      if (e.CloseReason == CloseReason.UserClosing)
        btCancel_Click(sender, e);
    }

    private void tbTimeFromat_TextChanged(object sender, EventArgs e)
    {
      try
      {
        lbTimeSample.Text = DateTime.Now.ToString(tbTimeFromat.Text);
      }
      catch (Exception)
      {
        lbTimeSample.Text = Properties.Resources.strInvalidTimeFormat;
      }
    }

    private void chbUseLocTime_CheckedChanged(object sender, EventArgs e)
    {
      nudTimeOffset.Enabled = !chbUseLocTime.Checked;
    }

    private void tbTimeFormatGraph_TextChanged(object sender, EventArgs e)
    {
      try
      {
        lbTimeSampleGraph.Text = DateTime.Now.ToString(tbTimeFormatGraph.Text);
      }
      catch (Exception)
      {
        lbTimeSampleGraph.Text = Properties.Resources.strInvalidTimeFormat;
      }
    }

    private void chbUseDBConnection_CheckedChanged(object sender, EventArgs e)
    {
      gbDBConn1Params.Enabled = chbUseDBConnection.Checked;
      gbDBConn2Params.Enabled = chbUseDBConnection.Checked;
      chbAskForDBConn.Enabled = chbUseDBConnection.Checked;
      if (!chbUseDBConnection.Checked)
        chbAskForDBConn.Checked = false;
    }

    private void rbDBSqlAuth1_CheckedChanged(object sender, EventArgs e)
    {
      tbDBUser1.Enabled = rbDBSqlAuth1.Checked;
      tbDBPassword1.Enabled = rbDBSqlAuth1.Checked;
    }

    private void rbDBSqlAuth2_CheckedChanged(object sender, EventArgs e)
    {
      tbDBUser2.Enabled = rbDBSqlAuth2.Checked;
      tbDBPassword2.Enabled = rbDBSqlAuth2.Checked;
    }

    private void chbDefConn1_CheckedChanged(object sender, EventArgs e)
    {
      chbDefConn2.Checked = !chbDefConn1.Checked;
    }

    private void chbDefConn2_CheckedChanged(object sender, EventArgs e)
    {
      chbDefConn1.Checked = !chbDefConn2.Checked;
    }

    /// <summary>
    /// Store associations of file types with DRView application
    /// </summary>
    private void btSaveAssoc_Click(object sender, EventArgs e)
    {
      try
      {
        ProgramVerb verb = new ProgramVerb("open", Application.ExecutablePath + " \"%1\"");
        ProgramIcon dataIcon = new ProgramIcon(Application.ExecutablePath, 0);
        ProgramIcon queryIcon = new ProgramIcon(Application.ExecutablePath, 1);
        ProgramIcon statsIcon = new ProgramIcon(Application.ExecutablePath, 2);
        //dr
        ModifyFileAssociation(chbAssocDR.Checked, ".dr", "diag.records", "Diagnostic records", verb, dataIcon);
        //xgz
        ModifyFileAssociation(chbAssocXGZ.Checked, ".xgz", "diag.records", "Diagnostic records", verb, dataIcon);
        //drq
        ModifyFileAssociation(chbAssocDRQ.Checked, ".drq", "diag.records.query", "Diagnostic records query", verb, queryIcon);
        //drstat
        ModifyFileAssociation(chbAssocDRSTAT.Checked, ".drstat", "diag.records.statistics", "Diagnostic records statistics", verb, statsIcon);
        //statq
        ModifyFileAssociation(chbAssocSTATQ.Checked, ".statq", "diag.records.statistics.query", "Statistical query", verb, queryIcon);
      }
      catch (Exception ex)
      {
        MessageBox.Show("Failed to save file associations for DR View: " + ex.Message, "File associations", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    /// <summary>
    /// Check associations of file types with DR View and display the status
    /// </summary>
    private void CheckFileAssociations()
    {
      try
      {
        //dr
        chbAssocDR.Checked = CheckFileAssociation(".dr", "diag.records");
        //xgz
        chbAssocXGZ.Checked = CheckFileAssociation(".xgz", "diag.records");
        //drq
        chbAssocDRQ.Checked = CheckFileAssociation(".drq", "diag.records.query");
        //drstat
        chbAssocDRSTAT.Checked = CheckFileAssociation(".drstat", "diag.records.statistics");
        //statq
        chbAssocSTATQ.Checked = CheckFileAssociation(".statq", "diag.records.statistics.query");
      }
      catch (Exception ex)
      {
        MessageBox.Show("Failed to read file associations for DR View: " + ex.Message, "File associations", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void ModifyFileAssociation(bool requiered, String ext, String progID, String desc, ProgramVerb verb, ProgramIcon icon)
    {
      if (CheckFileAssociation(ext, progID) != requiered)
      {
        if (chbAssocDR.Checked)
          CreateFileAssociation(ext, progID, desc, verb, icon);
        else
          DeleteFileAssociation(ext, progID);
      }
    }

    private void CreateFileAssociation(String ext, String progID, String desc, ProgramVerb verb, ProgramIcon icon)
    {
      FileAssociationInfo fai;
      ProgramAssociationInfo pai;
      //Create association
      pai = new ProgramAssociationInfo(progID);
      if (!pai.Exists)
        pai.Create(desc, verb);
      pai.DefaultIcon = icon;
      fai = new FileAssociationInfo(ext);
      if (!fai.Exists)
        fai.Create(progID);
      else if (!fai.IsValid(ext, progID))
      {
        fai.Delete();
        fai.Create(progID);
      }
    }

    private void DeleteFileAssociation(String ext, String progID)
    {
      FileAssociationInfo fai;
      ProgramAssociationInfo pai;
      //Delete association
      fai = new FileAssociationInfo(ext);
      if (fai.IsValid(ext, progID))
        fai.Delete();
      pai = new ProgramAssociationInfo(progID);
      if (pai.Exists)
        pai.Delete();
    }

    private bool CheckFileAssociation(String ext, String progID)
    {
      FileAssociationInfo fai;
      fai = new FileAssociationInfo(ext);
      return fai.IsValid(ext, progID);
    }

    
    }
}