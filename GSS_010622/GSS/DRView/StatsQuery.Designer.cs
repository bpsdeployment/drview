namespace DRView
{
  partial class StatsQuery
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StatsQuery));
      this.toolStrip = new System.Windows.Forms.ToolStrip();
      this.tsbSelect = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.tsbShowMessages = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
      this.tsbShowTableView = new System.Windows.Forms.ToolStripButton();
      this.tsbShowGraphView = new System.Windows.Forms.ToolStripButton();
      this.tsbShowSums = new System.Windows.Forms.ToolStripButton();
      this.tsbRotateViews = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
      this.tsbSaveQuery = new System.Windows.Forms.ToolStripButton();
      this.tsbSaveStatistics = new System.Windows.Forms.ToolStripButton();
      this.tsbExportStatistics = new System.Windows.Forms.ToolStripButton();
      this.msStatQuery = new System.Windows.Forms.MenuStrip();
      this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
      this.saveQueryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.saveSelectedDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.exportStatisticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.queryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.tableViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.graphViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.showSumToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
      this.systemMessagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
      this.executeQueryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.dlgSaveFile = new System.Windows.Forms.SaveFileDialog();
      this.dlgOpenFile = new System.Windows.Forms.OpenFileDialog();
      this.statusStrip = new System.Windows.Forms.StatusStrip();
      this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.panel1 = new System.Windows.Forms.Panel();
      this.splContDRQuery = new System.Windows.Forms.SplitContainer();
      this.tbcComponents = new System.Windows.Forms.TabControl();
      this.tabSelector = new System.Windows.Forms.TabPage();
      this.tabVwResults = new System.Windows.Forms.TabPage();
      this.splitContainerView = new System.Windows.Forms.SplitContainer();
      this.statViewTable = new StatViewComponents.StatViewTable();
      this.statViewGraph = new StatViewComponents.StatViewGraph();
      this.lbMessages = new System.Windows.Forms.ListBox();
      this.DRViewHelpProvider = new System.Windows.Forms.HelpProvider();
      this.persistWindow = new Mowog.PersistWindowComponent(this.components);
      this.toolStrip.SuspendLayout();
      this.msStatQuery.SuspendLayout();
      this.statusStrip.SuspendLayout();
      this.panel1.SuspendLayout();
      this.splContDRQuery.Panel1.SuspendLayout();
      this.splContDRQuery.Panel2.SuspendLayout();
      this.splContDRQuery.SuspendLayout();
      this.tbcComponents.SuspendLayout();
      this.tabVwResults.SuspendLayout();
      this.splitContainerView.Panel1.SuspendLayout();
      this.splitContainerView.Panel2.SuspendLayout();
      this.splitContainerView.SuspendLayout();
      this.SuspendLayout();
      // 
      // toolStrip
      // 
      this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbSelect,
            this.toolStripSeparator1,
            this.tsbShowMessages,
            this.toolStripSeparator8,
            this.tsbShowTableView,
            this.tsbShowGraphView,
            this.tsbShowSums,
            this.tsbRotateViews,
            this.toolStripSeparator2,
            this.tsbSaveQuery,
            this.tsbSaveStatistics,
            this.tsbExportStatistics});
      this.toolStrip.Location = new System.Drawing.Point(0, 0);
      this.toolStrip.Name = "toolStrip";
      this.toolStrip.Size = new System.Drawing.Size(921, 25);
      this.toolStrip.TabIndex = 4;
      this.toolStrip.Text = "toolStrip1";
      // 
      // tsbSelect
      // 
      this.tsbSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbSelect.Image = ((System.Drawing.Image)(resources.GetObject("tsbSelect.Image")));
      this.tsbSelect.ImageTransparentColor = System.Drawing.Color.White;
      this.tsbSelect.Name = "tsbSelect";
      this.tsbSelect.Size = new System.Drawing.Size(23, 22);
      this.tsbSelect.Text = "Execute query";
      this.tsbSelect.Click += new System.EventHandler(this.tsbSelect_Click);
      // 
      // toolStripSeparator1
      // 
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
      // 
      // tsbShowMessages
      // 
      this.tsbShowMessages.Checked = true;
      this.tsbShowMessages.CheckOnClick = true;
      this.tsbShowMessages.CheckState = System.Windows.Forms.CheckState.Checked;
      this.tsbShowMessages.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbShowMessages.Image = ((System.Drawing.Image)(resources.GetObject("tsbShowMessages.Image")));
      this.tsbShowMessages.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbShowMessages.Name = "tsbShowMessages";
      this.tsbShowMessages.Size = new System.Drawing.Size(23, 22);
      this.tsbShowMessages.Text = "Show messages";
      this.tsbShowMessages.Click += new System.EventHandler(this.tsbShowMessages_Click);
      // 
      // toolStripSeparator8
      // 
      this.toolStripSeparator8.Name = "toolStripSeparator8";
      this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
      // 
      // tsbShowTableView
      // 
      this.tsbShowTableView.Checked = true;
      this.tsbShowTableView.CheckOnClick = true;
      this.tsbShowTableView.CheckState = System.Windows.Forms.CheckState.Checked;
      this.tsbShowTableView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbShowTableView.Image = ((System.Drawing.Image)(resources.GetObject("tsbShowTableView.Image")));
      this.tsbShowTableView.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbShowTableView.Name = "tsbShowTableView";
      this.tsbShowTableView.Size = new System.Drawing.Size(23, 22);
      this.tsbShowTableView.Text = "Show Table";
      this.tsbShowTableView.Click += new System.EventHandler(this.tsbShowTableView_Click);
      // 
      // tsbShowGraphView
      // 
      this.tsbShowGraphView.Checked = true;
      this.tsbShowGraphView.CheckOnClick = true;
      this.tsbShowGraphView.CheckState = System.Windows.Forms.CheckState.Checked;
      this.tsbShowGraphView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbShowGraphView.Image = ((System.Drawing.Image)(resources.GetObject("tsbShowGraphView.Image")));
      this.tsbShowGraphView.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbShowGraphView.Name = "tsbShowGraphView";
      this.tsbShowGraphView.Size = new System.Drawing.Size(23, 22);
      this.tsbShowGraphView.Text = "Show Graph";
      this.tsbShowGraphView.Click += new System.EventHandler(this.tsbShowGraphView_Click);
      // 
      // tsbShowSums
      // 
      this.tsbShowSums.Checked = true;
      this.tsbShowSums.CheckOnClick = true;
      this.tsbShowSums.CheckState = System.Windows.Forms.CheckState.Checked;
      this.tsbShowSums.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbShowSums.Image = ((System.Drawing.Image)(resources.GetObject("tsbShowSums.Image")));
      this.tsbShowSums.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbShowSums.Name = "tsbShowSums";
      this.tsbShowSums.Size = new System.Drawing.Size(23, 22);
      this.tsbShowSums.Text = "Show sum column and row(s)";
      this.tsbShowSums.Click += new System.EventHandler(this.tsbShowSums_Click);
      // 
      // tsbRotateViews
      // 
      this.tsbRotateViews.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbRotateViews.Image = ((System.Drawing.Image)(resources.GetObject("tsbRotateViews.Image")));
      this.tsbRotateViews.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbRotateViews.Name = "tsbRotateViews";
      this.tsbRotateViews.Size = new System.Drawing.Size(23, 22);
      this.tsbRotateViews.Text = "Split horizintal / vertical";
      this.tsbRotateViews.Click += new System.EventHandler(this.tsbRotateViews_Click);
      // 
      // toolStripSeparator2
      // 
      this.toolStripSeparator2.Name = "toolStripSeparator2";
      this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
      // 
      // tsbSaveQuery
      // 
      this.tsbSaveQuery.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbSaveQuery.Image = ((System.Drawing.Image)(resources.GetObject("tsbSaveQuery.Image")));
      this.tsbSaveQuery.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbSaveQuery.Name = "tsbSaveQuery";
      this.tsbSaveQuery.Size = new System.Drawing.Size(23, 22);
      this.tsbSaveQuery.Text = "saveQuery";
      this.tsbSaveQuery.ToolTipText = "Save query";
      this.tsbSaveQuery.Click += new System.EventHandler(this.saveQueryToolStripMenuItem_Click);
      // 
      // tsbSaveStatistics
      // 
      this.tsbSaveStatistics.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbSaveStatistics.Image = ((System.Drawing.Image)(resources.GetObject("tsbSaveStatistics.Image")));
      this.tsbSaveStatistics.ImageTransparentColor = System.Drawing.Color.Silver;
      this.tsbSaveStatistics.Name = "tsbSaveStatistics";
      this.tsbSaveStatistics.Size = new System.Drawing.Size(23, 22);
      this.tsbSaveStatistics.ToolTipText = "Save selected statistical data to file";
      this.tsbSaveStatistics.Click += new System.EventHandler(this.saveSelectedDataToolStripMenuItem_Click);
      // 
      // tsbExportStatistics
      // 
      this.tsbExportStatistics.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbExportStatistics.Image = ((System.Drawing.Image)(resources.GetObject("tsbExportStatistics.Image")));
      this.tsbExportStatistics.ImageTransparentColor = System.Drawing.Color.Silver;
      this.tsbExportStatistics.Name = "tsbExportStatistics";
      this.tsbExportStatistics.Size = new System.Drawing.Size(23, 22);
      this.tsbExportStatistics.ToolTipText = "Export statistics to CSV file";
      this.tsbExportStatistics.Click += new System.EventHandler(this.tsbExportStatistics_Click);
      // 
      // msStatQuery
      // 
      this.msStatQuery.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.queryToolStripMenuItem});
      this.msStatQuery.Location = new System.Drawing.Point(0, 0);
      this.msStatQuery.Name = "msStatQuery";
      this.msStatQuery.Size = new System.Drawing.Size(921, 24);
      this.msStatQuery.TabIndex = 5;
      this.msStatQuery.Text = "menuStrip1";
      this.msStatQuery.Visible = false;
      // 
      // fileToolStripMenuItem
      // 
      this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator4,
            this.saveQueryToolStripMenuItem,
            this.saveSelectedDataToolStripMenuItem,
            this.exportStatisticsToolStripMenuItem});
      this.fileToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.MatchOnly;
      this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
      this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
      this.fileToolStripMenuItem.Text = "&File";
      // 
      // toolStripSeparator4
      // 
      this.toolStripSeparator4.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.toolStripSeparator4.MergeIndex = 5;
      this.toolStripSeparator4.Name = "toolStripSeparator4";
      this.toolStripSeparator4.Size = new System.Drawing.Size(152, 6);
      // 
      // saveQueryToolStripMenuItem
      // 
      this.saveQueryToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveQueryToolStripMenuItem.Image")));
      this.saveQueryToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.saveQueryToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.saveQueryToolStripMenuItem.MergeIndex = 6;
      this.saveQueryToolStripMenuItem.Name = "saveQueryToolStripMenuItem";
      this.saveQueryToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
      this.saveQueryToolStripMenuItem.Text = "Save query";
      this.saveQueryToolStripMenuItem.ToolTipText = "Save query parameters to file";
      this.saveQueryToolStripMenuItem.Click += new System.EventHandler(this.saveQueryToolStripMenuItem_Click);
      // 
      // saveSelectedDataToolStripMenuItem
      // 
      this.saveSelectedDataToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveSelectedDataToolStripMenuItem.Image")));
      this.saveSelectedDataToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Silver;
      this.saveSelectedDataToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.saveSelectedDataToolStripMenuItem.MergeIndex = 7;
      this.saveSelectedDataToolStripMenuItem.Name = "saveSelectedDataToolStripMenuItem";
      this.saveSelectedDataToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
      this.saveSelectedDataToolStripMenuItem.Text = "Save statistics";
      this.saveSelectedDataToolStripMenuItem.ToolTipText = "Save selected statistical data to file";
      this.saveSelectedDataToolStripMenuItem.Click += new System.EventHandler(this.saveSelectedDataToolStripMenuItem_Click);
      // 
      // exportStatisticsToolStripMenuItem
      // 
      this.exportStatisticsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exportStatisticsToolStripMenuItem.Image")));
      this.exportStatisticsToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Silver;
      this.exportStatisticsToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.exportStatisticsToolStripMenuItem.MergeIndex = 8;
      this.exportStatisticsToolStripMenuItem.Name = "exportStatisticsToolStripMenuItem";
      this.exportStatisticsToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
      this.exportStatisticsToolStripMenuItem.Text = "Export statistics";
      this.exportStatisticsToolStripMenuItem.ToolTipText = "Export statistics to CSV file";
      this.exportStatisticsToolStripMenuItem.Click += new System.EventHandler(this.tsbExportStatistics_Click);
      // 
      // queryToolStripMenuItem
      // 
      this.queryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewToolStripMenuItem,
            this.toolStripSeparator7,
            this.executeQueryToolStripMenuItem});
      this.queryToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.queryToolStripMenuItem.MergeIndex = 1;
      this.queryToolStripMenuItem.Name = "queryToolStripMenuItem";
      this.queryToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
      this.queryToolStripMenuItem.Text = "&Query";
      // 
      // viewToolStripMenuItem
      // 
      this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tableViewToolStripMenuItem,
            this.graphViewToolStripMenuItem,
            this.showSumToolStripMenuItem,
            this.toolStripSeparator3,
            this.systemMessagesToolStripMenuItem});
      this.viewToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.viewToolStripMenuItem.MergeIndex = 1;
      this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
      this.viewToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
      this.viewToolStripMenuItem.Text = "&Show";
      // 
      // tableViewToolStripMenuItem
      // 
      this.tableViewToolStripMenuItem.Checked = true;
      this.tableViewToolStripMenuItem.CheckOnClick = true;
      this.tableViewToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
      this.tableViewToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("tableViewToolStripMenuItem.Image")));
      this.tableViewToolStripMenuItem.Name = "tableViewToolStripMenuItem";
      this.tableViewToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
      this.tableViewToolStripMenuItem.Text = "&Table view";
      this.tableViewToolStripMenuItem.Click += new System.EventHandler(this.tsbShowTableView_Click);
      // 
      // graphViewToolStripMenuItem
      // 
      this.graphViewToolStripMenuItem.Checked = true;
      this.graphViewToolStripMenuItem.CheckOnClick = true;
      this.graphViewToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
      this.graphViewToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("graphViewToolStripMenuItem.Image")));
      this.graphViewToolStripMenuItem.Name = "graphViewToolStripMenuItem";
      this.graphViewToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
      this.graphViewToolStripMenuItem.Text = "&Graph view";
      this.graphViewToolStripMenuItem.Click += new System.EventHandler(this.tsbShowGraphView_Click);
      // 
      // showSumToolStripMenuItem
      // 
      this.showSumToolStripMenuItem.Checked = true;
      this.showSumToolStripMenuItem.CheckOnClick = true;
      this.showSumToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
      this.showSumToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("showSumToolStripMenuItem.Image")));
      this.showSumToolStripMenuItem.Name = "showSumToolStripMenuItem";
      this.showSumToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
      this.showSumToolStripMenuItem.Text = "Show &sums";
      this.showSumToolStripMenuItem.ToolTipText = "Show sum column and row(s)";
      this.showSumToolStripMenuItem.Click += new System.EventHandler(this.tsbShowSums_Click);
      // 
      // toolStripSeparator3
      // 
      this.toolStripSeparator3.Name = "toolStripSeparator3";
      this.toolStripSeparator3.Size = new System.Drawing.Size(163, 6);
      // 
      // systemMessagesToolStripMenuItem
      // 
      this.systemMessagesToolStripMenuItem.CheckOnClick = true;
      this.systemMessagesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("systemMessagesToolStripMenuItem.Image")));
      this.systemMessagesToolStripMenuItem.Name = "systemMessagesToolStripMenuItem";
      this.systemMessagesToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
      this.systemMessagesToolStripMenuItem.Text = "System &messages";
      this.systemMessagesToolStripMenuItem.Click += new System.EventHandler(this.tsbShowMessages_Click);
      // 
      // toolStripSeparator7
      // 
      this.toolStripSeparator7.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.toolStripSeparator7.MergeIndex = 2;
      this.toolStripSeparator7.Name = "toolStripSeparator7";
      this.toolStripSeparator7.Size = new System.Drawing.Size(144, 6);
      // 
      // executeQueryToolStripMenuItem
      // 
      this.executeQueryToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("executeQueryToolStripMenuItem.Image")));
      this.executeQueryToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.White;
      this.executeQueryToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
      this.executeQueryToolStripMenuItem.MergeIndex = 6;
      this.executeQueryToolStripMenuItem.Name = "executeQueryToolStripMenuItem";
      this.executeQueryToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
      this.executeQueryToolStripMenuItem.Text = "Execute query";
      this.executeQueryToolStripMenuItem.Click += new System.EventHandler(this.tsbSelect_Click);
      // 
      // dlgOpenFile
      // 
      this.dlgOpenFile.ShowReadOnly = true;
      // 
      // statusStrip
      // 
      this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
      this.statusStrip.Location = new System.Drawing.Point(0, 617);
      this.statusStrip.Name = "statusStrip";
      this.statusStrip.Size = new System.Drawing.Size(921, 24);
      this.statusStrip.TabIndex = 6;
      this.statusStrip.Text = "statusStrip1";
      // 
      // statusLabel
      // 
      this.statusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                  | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                  | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
      this.statusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
      this.statusLabel.Name = "statusLabel";
      this.statusLabel.Size = new System.Drawing.Size(906, 19);
      this.statusLabel.Spring = true;
      this.statusLabel.Text = "ready";
      this.statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // splitter1
      // 
      this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter1.Enabled = false;
      this.splitter1.Location = new System.Drawing.Point(0, 616);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(921, 1);
      this.splitter1.TabIndex = 7;
      this.splitter1.TabStop = false;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.splContDRQuery);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel1.Location = new System.Drawing.Point(0, 25);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(921, 591);
      this.panel1.TabIndex = 8;
      // 
      // splContDRQuery
      // 
      this.splContDRQuery.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splContDRQuery.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
      this.splContDRQuery.Location = new System.Drawing.Point(0, 0);
      this.splContDRQuery.Name = "splContDRQuery";
      this.splContDRQuery.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splContDRQuery.Panel1
      // 
      this.splContDRQuery.Panel1.Controls.Add(this.tbcComponents);
      this.splContDRQuery.Panel1MinSize = 50;
      // 
      // splContDRQuery.Panel2
      // 
      this.splContDRQuery.Panel2.Controls.Add(this.lbMessages);
      this.splContDRQuery.Panel2MinSize = 50;
      this.splContDRQuery.Size = new System.Drawing.Size(921, 591);
      this.splContDRQuery.SplitterDistance = 538;
      this.splContDRQuery.SplitterWidth = 3;
      this.splContDRQuery.TabIndex = 3;
      // 
      // tbcComponents
      // 
      this.tbcComponents.Controls.Add(this.tabSelector);
      this.tbcComponents.Controls.Add(this.tabVwResults);
      this.tbcComponents.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbcComponents.Location = new System.Drawing.Point(0, 0);
      this.tbcComponents.Name = "tbcComponents";
      this.tbcComponents.Padding = new System.Drawing.Point(3, 3);
      this.tbcComponents.SelectedIndex = 0;
      this.tbcComponents.Size = new System.Drawing.Size(921, 538);
      this.tbcComponents.TabIndex = 2;
      // 
      // tabSelector
      // 
      this.DRViewHelpProvider.SetHelpKeyword(this.tabSelector, "StatQuery.htm");
      this.DRViewHelpProvider.SetHelpNavigator(this.tabSelector, System.Windows.Forms.HelpNavigator.Topic);
      this.tabSelector.Location = new System.Drawing.Point(4, 22);
      this.tabSelector.Name = "tabSelector";
      this.tabSelector.Padding = new System.Windows.Forms.Padding(3);
      this.DRViewHelpProvider.SetShowHelp(this.tabSelector, true);
      this.tabSelector.Size = new System.Drawing.Size(913, 512);
      this.tabSelector.TabIndex = 0;
      this.tabSelector.Text = "Query";
      this.tabSelector.ToolTipText = "Selects statistical data from data source";
      this.tabSelector.UseVisualStyleBackColor = true;
      // 
      // tabVwResults
      // 
      this.tabVwResults.Controls.Add(this.splitContainerView);
      this.DRViewHelpProvider.SetHelpKeyword(this.tabVwResults, "StatQueryResult.htm");
      this.DRViewHelpProvider.SetHelpNavigator(this.tabVwResults, System.Windows.Forms.HelpNavigator.Topic);
      this.tabVwResults.Location = new System.Drawing.Point(4, 22);
      this.tabVwResults.Name = "tabVwResults";
      this.tabVwResults.Padding = new System.Windows.Forms.Padding(3);
      this.DRViewHelpProvider.SetShowHelp(this.tabVwResults, true);
      this.tabVwResults.Size = new System.Drawing.Size(913, 512);
      this.tabVwResults.TabIndex = 1;
      this.tabVwResults.Text = "Results";
      this.tabVwResults.ToolTipText = "Displays results of statistical query";
      this.tabVwResults.UseVisualStyleBackColor = true;
      // 
      // splitContainerView
      // 
      this.splitContainerView.BackColor = System.Drawing.Color.Transparent;
      this.splitContainerView.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainerView.Location = new System.Drawing.Point(3, 3);
      this.splitContainerView.Name = "splitContainerView";
      // 
      // splitContainerView.Panel1
      // 
      this.splitContainerView.Panel1.Controls.Add(this.statViewTable);
      // 
      // splitContainerView.Panel2
      // 
      this.splitContainerView.Panel2.Controls.Add(this.statViewGraph);
      this.splitContainerView.Size = new System.Drawing.Size(907, 506);
      this.splitContainerView.SplitterDistance = 449;
      this.splitContainerView.TabIndex = 1;
      // 
      // statViewTable
      // 
      this.statViewTable.BackColor = System.Drawing.Color.Transparent;
      this.statViewTable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.statViewTable.DDDHelper = null;
      this.statViewTable.DisplayData = true;
      this.statViewTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.DRViewHelpProvider.SetHelpKeyword(this.statViewTable, "StatQueryResult.htm");
      this.DRViewHelpProvider.SetHelpNavigator(this.statViewTable, System.Windows.Forms.HelpNavigator.Topic);
      this.statViewTable.Location = new System.Drawing.Point(0, 0);
      this.statViewTable.Name = "statViewTable";
      this.DRViewHelpProvider.SetShowHelp(this.statViewTable, true);
      this.statViewTable.ShowSums = true;
      this.statViewTable.Size = new System.Drawing.Size(449, 506);
      this.statViewTable.TabIndex = 0;
      this.statViewTable.InfoMessage += new StatSelectorComponents.InfoMessageEventhandler(this.InfoMessageEvent);
      this.statViewTable.ErrorMessage += new StatSelectorComponents.ErrorMessageEventhandler(this.ErrorMessageEvent);
      // 
      // statViewGraph
      // 
      this.statViewGraph.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.statViewGraph.DDDHelper = null;
      this.statViewGraph.DisplayData = true;
      this.statViewGraph.Dock = System.Windows.Forms.DockStyle.Fill;
      this.statViewGraph.Location = new System.Drawing.Point(0, 0);
      this.statViewGraph.Name = "statViewGraph";
      this.statViewGraph.Size = new System.Drawing.Size(454, 506);
      this.statViewGraph.TabIndex = 0;
      this.statViewGraph.InfoMessage += new StatSelectorComponents.InfoMessageEventhandler(this.InfoMessageEvent);
      this.statViewGraph.ErrorMessage += new StatSelectorComponents.ErrorMessageEventhandler(this.ErrorMessageEvent);
      // 
      // lbMessages
      // 
      this.lbMessages.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbMessages.FormattingEnabled = true;
      this.lbMessages.Location = new System.Drawing.Point(0, 0);
      this.lbMessages.Name = "lbMessages";
      this.lbMessages.Size = new System.Drawing.Size(921, 43);
      this.lbMessages.TabIndex = 0;
      // 
      // DRViewHelpProvider
      // 
      this.DRViewHelpProvider.HelpNamespace = "DRView.chm";
      // 
      // persistWindow
      // 
      this.persistWindow.Form = this;
      this.persistWindow.UseLocAppDataDir = true;
      this.persistWindow.XMLFilePath = "DRViewWindowState.xml";
      // 
      // StatsQuery
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(921, 641);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.splitter1);
      this.Controls.Add(this.statusStrip);
      this.Controls.Add(this.toolStrip);
      this.Controls.Add(this.msStatQuery);
      this.DRViewHelpProvider.SetHelpKeyword(this, "StatQuery.htm");
      this.DRViewHelpProvider.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MainMenuStrip = this.msStatQuery;
      this.MinimumSize = new System.Drawing.Size(600, 500);
      this.Name = "StatsQuery";
      this.DRViewHelpProvider.SetShowHelp(this, true);
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Statistical query";
      this.toolStrip.ResumeLayout(false);
      this.toolStrip.PerformLayout();
      this.msStatQuery.ResumeLayout(false);
      this.msStatQuery.PerformLayout();
      this.statusStrip.ResumeLayout(false);
      this.statusStrip.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.splContDRQuery.Panel1.ResumeLayout(false);
      this.splContDRQuery.Panel2.ResumeLayout(false);
      this.splContDRQuery.ResumeLayout(false);
      this.tbcComponents.ResumeLayout(false);
      this.tabVwResults.ResumeLayout(false);
      this.splitContainerView.Panel1.ResumeLayout(false);
      this.splitContainerView.Panel2.ResumeLayout(false);
      this.splitContainerView.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ToolStrip toolStrip;
    private System.Windows.Forms.ToolStripButton tsbShowMessages;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.ToolStripButton tsbShowTableView;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.SplitContainer splContDRQuery;
    private System.Windows.Forms.MenuStrip msStatQuery;
    private System.Windows.Forms.ToolStripMenuItem queryToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem tableViewToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
    private System.Windows.Forms.ToolStripMenuItem saveSelectedDataToolStripMenuItem;
    private System.Windows.Forms.SaveFileDialog dlgSaveFile;
    private System.Windows.Forms.OpenFileDialog dlgOpenFile;
    private System.Windows.Forms.StatusStrip statusStrip;
    private System.Windows.Forms.ToolStripStatusLabel statusLabel;
    private System.Windows.Forms.Splitter splitter1;
    private System.Windows.Forms.ListBox lbMessages;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.ToolStripButton tsbSaveStatistics;
    private System.Windows.Forms.ToolStripMenuItem systemMessagesToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem exportStatisticsToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
    private System.Windows.Forms.HelpProvider DRViewHelpProvider;
    private System.Windows.Forms.ToolStripButton tsbSelect;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
    private System.Windows.Forms.ToolStripMenuItem executeQueryToolStripMenuItem;
    private System.Windows.Forms.ToolStripButton tsbSaveQuery;
    private System.Windows.Forms.ToolStripMenuItem saveQueryToolStripMenuItem;
    private Mowog.PersistWindowComponent persistWindow;
    private System.Windows.Forms.TabControl tbcComponents;
    private System.Windows.Forms.TabPage tabSelector;
    private System.Windows.Forms.TabPage tabVwResults;
    private StatViewComponents.StatViewTable statViewTable;
    private System.Windows.Forms.ToolStripButton tsbShowSums;
    private System.Windows.Forms.ToolStripButton tsbExportStatistics;
    private System.Windows.Forms.ToolStripMenuItem showSumToolStripMenuItem;
    private System.Windows.Forms.SplitContainer splitContainerView;
    private StatViewComponents.StatViewGraph statViewGraph;
    private System.Windows.Forms.ToolStripButton tsbShowGraphView;
    private System.Windows.Forms.ToolStripMenuItem graphViewToolStripMenuItem;
    private System.Windows.Forms.ToolStripButton tsbRotateViews;

  }
}