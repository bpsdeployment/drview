namespace DRView
{
  partial class RecExport
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.rbRecCSV = new System.Windows.Forms.RadioButton();
      this.rbVarCSV = new System.Windows.Forms.RadioButton();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.chbIncludeDDDVer = new System.Windows.Forms.CheckBox();
      this.chbCompleteVarTitle = new System.Windows.Forms.CheckBox();
      this.tbSeparator = new System.Windows.Forms.MaskedTextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.tbTimeFormat = new System.Windows.Forms.TextBox();
      this.tbFileName = new System.Windows.Forms.TextBox();
      this.btSetFileName = new System.Windows.Forms.Button();
      this.chbRecTitle = new System.Windows.Forms.CheckBox();
      this.chbStartTime = new System.Windows.Forms.CheckBox();
      this.chbEndTime = new System.Windows.Forms.CheckBox();
      this.chbFaultCode = new System.Windows.Forms.CheckBox();
      this.chbDDDVersion = new System.Windows.Forms.CheckBox();
      this.chbTUType = new System.Windows.Forms.CheckBox();
      this.chbTUInstance = new System.Windows.Forms.CheckBox();
      this.chbTypeCount = new System.Windows.Forms.CheckBox();
      this.chbLastModified = new System.Windows.Forms.CheckBox();
      this.gbRecordAttributes = new System.Windows.Forms.GroupBox();
      this.chbFaultSubSeverity = new System.Windows.Forms.CheckBox();
      this.chbFaultSeverity = new System.Windows.Forms.CheckBox();
      this.chbRecordType = new System.Windows.Forms.CheckBox();
      this.chbTrcSnpCode = new System.Windows.Forms.CheckBox();
      this.chbRecName = new System.Windows.Forms.CheckBox();
      this.chbImportTime = new System.Windows.Forms.CheckBox();
      this.chbAcknowledgeTime = new System.Windows.Forms.CheckBox();
      this.chbDeviceCode = new System.Windows.Forms.CheckBox();
      this.chbVehicleNumber = new System.Windows.Forms.CheckBox();
      this.chbSource = new System.Windows.Forms.CheckBox();
      this.chbVehicleName = new System.Windows.Forms.CheckBox();
      this.chbTrainName = new System.Windows.Forms.CheckBox();
      this.chbGPSLongitude = new System.Windows.Forms.CheckBox();
      this.chbGPSLatitude = new System.Windows.Forms.CheckBox();
      this.btnExport = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
      this.drExport = new DRViewComponents.DRExport(this.components);
      this.DRViewHelpProvider = new System.Windows.Forms.HelpProvider();
      this.gbVariables = new System.Windows.Forms.GroupBox();
      this.varTreeView = new DRViewComponents.VarTreeView();
      this.persistWindow = new Mowog.PersistWindowComponent(this.components);
      this.gbFile = new System.Windows.Forms.GroupBox();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.gbRecordAttributes.SuspendLayout();
      this.gbVariables.SuspendLayout();
      this.gbFile.SuspendLayout();
      this.SuspendLayout();
      // 
      // rbRecCSV
      // 
      this.rbRecCSV.AutoSize = true;
      this.rbRecCSV.Checked = true;
      this.rbRecCSV.Location = new System.Drawing.Point(5, 13);
      this.rbRecCSV.Name = "rbRecCSV";
      this.rbRecCSV.Size = new System.Drawing.Size(170, 17);
      this.rbRecCSV.TabIndex = 0;
      this.rbRecCSV.TabStop = true;
      this.rbRecCSV.Text = "Diagnostic Records to CSV file";
      this.rbRecCSV.UseVisualStyleBackColor = true;
      this.rbRecCSV.CheckedChanged += new System.EventHandler(this.ExportTypeChanged);
      // 
      // rbVarCSV
      // 
      this.rbVarCSV.AutoSize = true;
      this.rbVarCSV.Location = new System.Drawing.Point(5, 30);
      this.rbVarCSV.Name = "rbVarCSV";
      this.rbVarCSV.Size = new System.Drawing.Size(149, 17);
      this.rbVarCSV.TabIndex = 1;
      this.rbVarCSV.Text = "Variable values to CSV file";
      this.rbVarCSV.UseVisualStyleBackColor = true;
      this.rbVarCSV.CheckedChanged += new System.EventHandler(this.ExportTypeChanged);
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.rbVarCSV);
      this.groupBox1.Controls.Add(this.rbRecCSV);
      this.groupBox1.Location = new System.Drawing.Point(7, 8);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(247, 53);
      this.groupBox1.TabIndex = 1;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Export Type";
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.chbIncludeDDDVer);
      this.groupBox2.Controls.Add(this.chbCompleteVarTitle);
      this.groupBox2.Controls.Add(this.tbSeparator);
      this.groupBox2.Controls.Add(this.label2);
      this.groupBox2.Controls.Add(this.label1);
      this.groupBox2.Controls.Add(this.tbTimeFormat);
      this.groupBox2.Location = new System.Drawing.Point(7, 67);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(247, 120);
      this.groupBox2.TabIndex = 2;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Formatting";
      // 
      // chbIncludeDDDVer
      // 
      this.chbIncludeDDDVer.AutoSize = true;
      this.chbIncludeDDDVer.Checked = true;
      this.chbIncludeDDDVer.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chbIncludeDDDVer.Enabled = false;
      this.chbIncludeDDDVer.Location = new System.Drawing.Point(26, 93);
      this.chbIncludeDDDVer.Name = "chbIncludeDDDVer";
      this.chbIncludeDDDVer.Size = new System.Drawing.Size(193, 17);
      this.chbIncludeDDDVer.TabIndex = 5;
      this.chbIncludeDDDVer.Text = "Include DDD version and TU name";
      this.chbIncludeDDDVer.UseVisualStyleBackColor = true;
      // 
      // chbCompleteVarTitle
      // 
      this.chbCompleteVarTitle.AutoSize = true;
      this.chbCompleteVarTitle.Checked = true;
      this.chbCompleteVarTitle.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chbCompleteVarTitle.Enabled = false;
      this.chbCompleteVarTitle.Location = new System.Drawing.Point(4, 76);
      this.chbCompleteVarTitle.Name = "chbCompleteVarTitle";
      this.chbCompleteVarTitle.Size = new System.Drawing.Size(129, 17);
      this.chbCompleteVarTitle.TabIndex = 4;
      this.chbCompleteVarTitle.Text = "Complete variable title";
      this.chbCompleteVarTitle.UseVisualStyleBackColor = true;
      // 
      // tbSeparator
      // 
      this.tbSeparator.Location = new System.Drawing.Point(83, 50);
      this.tbSeparator.Mask = "C";
      this.tbSeparator.Name = "tbSeparator";
      this.tbSeparator.Size = new System.Drawing.Size(18, 20);
      this.tbSeparator.TabIndex = 3;
      this.tbSeparator.Text = ";";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(1, 55);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(79, 13);
      this.label2.TabIndex = 2;
      this.label2.Text = "Field separator:";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(1, 14);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(65, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Time format:";
      // 
      // tbTimeFormat
      // 
      this.tbTimeFormat.Location = new System.Drawing.Point(4, 27);
      this.tbTimeFormat.Name = "tbTimeFormat";
      this.tbTimeFormat.Size = new System.Drawing.Size(136, 20);
      this.tbTimeFormat.TabIndex = 0;
      this.tbTimeFormat.Text = "yyyy-MM-dd HH:mm:ss.fff";
      // 
      // tbFileName
      // 
      this.tbFileName.Location = new System.Drawing.Point(4, 19);
      this.tbFileName.Name = "tbFileName";
      this.tbFileName.Size = new System.Drawing.Size(209, 20);
      this.tbFileName.TabIndex = 3;
      this.tbFileName.TextChanged += new System.EventHandler(this.tbFileName_TextChanged);
      // 
      // btSetFileName
      // 
      this.btSetFileName.Location = new System.Drawing.Point(217, 16);
      this.btSetFileName.Name = "btSetFileName";
      this.btSetFileName.Size = new System.Drawing.Size(24, 23);
      this.btSetFileName.TabIndex = 7;
      this.btSetFileName.Text = "...";
      this.btSetFileName.UseVisualStyleBackColor = true;
      this.btSetFileName.Click += new System.EventHandler(this.btSetFileName_Click);
      // 
      // chbRecTitle
      // 
      this.chbRecTitle.AutoSize = true;
      this.chbRecTitle.Location = new System.Drawing.Point(121, 36);
      this.chbRecTitle.Name = "chbRecTitle";
      this.chbRecTitle.Size = new System.Drawing.Size(84, 17);
      this.chbRecTitle.TabIndex = 0;
      this.chbRecTitle.Text = "Record Title";
      this.chbRecTitle.UseVisualStyleBackColor = true;
      // 
      // chbStartTime
      // 
      this.chbStartTime.AutoSize = true;
      this.chbStartTime.Location = new System.Drawing.Point(4, 138);
      this.chbStartTime.Name = "chbStartTime";
      this.chbStartTime.Size = new System.Drawing.Size(74, 17);
      this.chbStartTime.TabIndex = 1;
      this.chbStartTime.Text = "Start Time";
      this.chbStartTime.UseVisualStyleBackColor = true;
      // 
      // chbEndTime
      // 
      this.chbEndTime.AutoSize = true;
      this.chbEndTime.Location = new System.Drawing.Point(4, 155);
      this.chbEndTime.Name = "chbEndTime";
      this.chbEndTime.Size = new System.Drawing.Size(71, 17);
      this.chbEndTime.TabIndex = 2;
      this.chbEndTime.Text = "End Time";
      this.chbEndTime.UseVisualStyleBackColor = true;
      // 
      // chbFaultCode
      // 
      this.chbFaultCode.AutoSize = true;
      this.chbFaultCode.Location = new System.Drawing.Point(121, 70);
      this.chbFaultCode.Name = "chbFaultCode";
      this.chbFaultCode.Size = new System.Drawing.Size(77, 17);
      this.chbFaultCode.TabIndex = 3;
      this.chbFaultCode.Text = "Fault Code";
      this.chbFaultCode.UseVisualStyleBackColor = true;
      // 
      // chbDDDVersion
      // 
      this.chbDDDVersion.AutoSize = true;
      this.chbDDDVersion.Location = new System.Drawing.Point(121, 19);
      this.chbDDDVersion.Name = "chbDDDVersion";
      this.chbDDDVersion.Size = new System.Drawing.Size(88, 17);
      this.chbDDDVersion.TabIndex = 4;
      this.chbDDDVersion.Text = "DDD Version";
      this.chbDDDVersion.UseVisualStyleBackColor = true;
      // 
      // chbTUType
      // 
      this.chbTUType.AutoSize = true;
      this.chbTUType.Location = new System.Drawing.Point(4, 70);
      this.chbTUType.Name = "chbTUType";
      this.chbTUType.Size = new System.Drawing.Size(68, 17);
      this.chbTUType.TabIndex = 5;
      this.chbTUType.Text = "TU Type";
      this.chbTUType.UseVisualStyleBackColor = true;
      // 
      // chbTUInstance
      // 
      this.chbTUInstance.AutoSize = true;
      this.chbTUInstance.Location = new System.Drawing.Point(4, 53);
      this.chbTUInstance.Name = "chbTUInstance";
      this.chbTUInstance.Size = new System.Drawing.Size(72, 17);
      this.chbTUInstance.TabIndex = 6;
      this.chbTUInstance.Text = "TU Name";
      this.chbTUInstance.UseVisualStyleBackColor = true;
      // 
      // chbTypeCount
      // 
      this.chbTypeCount.AutoSize = true;
      this.chbTypeCount.Location = new System.Drawing.Point(121, 189);
      this.chbTypeCount.Name = "chbTypeCount";
      this.chbTypeCount.Size = new System.Drawing.Size(81, 17);
      this.chbTypeCount.TabIndex = 7;
      this.chbTypeCount.Text = "Type Count";
      this.chbTypeCount.UseVisualStyleBackColor = true;
      // 
      // chbLastModified
      // 
      this.chbLastModified.AutoSize = true;
      this.chbLastModified.Location = new System.Drawing.Point(4, 172);
      this.chbLastModified.Name = "chbLastModified";
      this.chbLastModified.Size = new System.Drawing.Size(89, 17);
      this.chbLastModified.TabIndex = 8;
      this.chbLastModified.Text = "Last Modified";
      this.chbLastModified.UseVisualStyleBackColor = true;
      // 
      // gbRecordAttributes
      // 
      this.gbRecordAttributes.Controls.Add(this.chbFaultSubSeverity);
      this.gbRecordAttributes.Controls.Add(this.chbFaultSeverity);
      this.gbRecordAttributes.Controls.Add(this.chbRecordType);
      this.gbRecordAttributes.Controls.Add(this.chbTrcSnpCode);
      this.gbRecordAttributes.Controls.Add(this.chbRecName);
      this.gbRecordAttributes.Controls.Add(this.chbImportTime);
      this.gbRecordAttributes.Controls.Add(this.chbAcknowledgeTime);
      this.gbRecordAttributes.Controls.Add(this.chbDeviceCode);
      this.gbRecordAttributes.Controls.Add(this.chbVehicleNumber);
      this.gbRecordAttributes.Controls.Add(this.chbSource);
      this.gbRecordAttributes.Controls.Add(this.chbVehicleName);
      this.gbRecordAttributes.Controls.Add(this.chbTrainName);
      this.gbRecordAttributes.Controls.Add(this.chbGPSLongitude);
      this.gbRecordAttributes.Controls.Add(this.chbGPSLatitude);
      this.gbRecordAttributes.Controls.Add(this.chbDDDVersion);
      this.gbRecordAttributes.Controls.Add(this.chbLastModified);
      this.gbRecordAttributes.Controls.Add(this.chbRecTitle);
      this.gbRecordAttributes.Controls.Add(this.chbTypeCount);
      this.gbRecordAttributes.Controls.Add(this.chbStartTime);
      this.gbRecordAttributes.Controls.Add(this.chbTUInstance);
      this.gbRecordAttributes.Controls.Add(this.chbEndTime);
      this.gbRecordAttributes.Controls.Add(this.chbTUType);
      this.gbRecordAttributes.Controls.Add(this.chbFaultCode);
      this.gbRecordAttributes.Location = new System.Drawing.Point(7, 193);
      this.gbRecordAttributes.Name = "gbRecordAttributes";
      this.gbRecordAttributes.Size = new System.Drawing.Size(247, 227);
      this.gbRecordAttributes.TabIndex = 9;
      this.gbRecordAttributes.TabStop = false;
      this.gbRecordAttributes.Text = "Record Attributes";
      // 
      // chbFaultSubSeverity
      // 
      this.chbFaultSubSeverity.AutoSize = true;
      this.chbFaultSubSeverity.Location = new System.Drawing.Point(121, 138);
      this.chbFaultSubSeverity.Name = "chbFaultSubSeverity";
      this.chbFaultSubSeverity.Size = new System.Drawing.Size(107, 17);
      this.chbFaultSubSeverity.TabIndex = 23;
      this.chbFaultSubSeverity.Text = "Fault Subseverity";
      this.chbFaultSubSeverity.UseVisualStyleBackColor = true;
      // 
      // chbFaultSeverity
      // 
      this.chbFaultSeverity.AutoSize = true;
      this.chbFaultSeverity.Location = new System.Drawing.Point(121, 121);
      this.chbFaultSeverity.Name = "chbFaultSeverity";
      this.chbFaultSeverity.Size = new System.Drawing.Size(90, 17);
      this.chbFaultSeverity.TabIndex = 22;
      this.chbFaultSeverity.Text = "Fault Severity";
      this.chbFaultSeverity.UseVisualStyleBackColor = true;
      // 
      // chbRecordType
      // 
      this.chbRecordType.AutoSize = true;
      this.chbRecordType.Location = new System.Drawing.Point(121, 104);
      this.chbRecordType.Name = "chbRecordType";
      this.chbRecordType.Size = new System.Drawing.Size(88, 17);
      this.chbRecordType.TabIndex = 21;
      this.chbRecordType.Text = "Record Type";
      this.chbRecordType.UseVisualStyleBackColor = true;
      // 
      // chbTrcSnpCode
      // 
      this.chbTrcSnpCode.AutoSize = true;
      this.chbTrcSnpCode.Location = new System.Drawing.Point(121, 87);
      this.chbTrcSnpCode.Name = "chbTrcSnpCode";
      this.chbTrcSnpCode.Size = new System.Drawing.Size(122, 17);
      this.chbTrcSnpCode.TabIndex = 20;
      this.chbTrcSnpCode.Text = "Trace or Snap Code";
      this.chbTrcSnpCode.UseVisualStyleBackColor = true;
      // 
      // chbRecName
      // 
      this.chbRecName.AutoSize = true;
      this.chbRecName.Location = new System.Drawing.Point(121, 53);
      this.chbRecName.Name = "chbRecName";
      this.chbRecName.Size = new System.Drawing.Size(92, 17);
      this.chbRecName.TabIndex = 19;
      this.chbRecName.Text = "Record Name";
      this.chbRecName.UseVisualStyleBackColor = true;
      // 
      // chbImportTime
      // 
      this.chbImportTime.AutoSize = true;
      this.chbImportTime.Location = new System.Drawing.Point(4, 206);
      this.chbImportTime.Name = "chbImportTime";
      this.chbImportTime.Size = new System.Drawing.Size(81, 17);
      this.chbImportTime.TabIndex = 18;
      this.chbImportTime.Text = "Import Time";
      this.chbImportTime.UseVisualStyleBackColor = true;
      // 
      // chbAcknowledgeTime
      // 
      this.chbAcknowledgeTime.AutoSize = true;
      this.chbAcknowledgeTime.Location = new System.Drawing.Point(4, 189);
      this.chbAcknowledgeTime.Name = "chbAcknowledgeTime";
      this.chbAcknowledgeTime.Size = new System.Drawing.Size(117, 17);
      this.chbAcknowledgeTime.TabIndex = 17;
      this.chbAcknowledgeTime.Text = "Acknowledge Time";
      this.chbAcknowledgeTime.UseVisualStyleBackColor = true;
      // 
      // chbDeviceCode
      // 
      this.chbDeviceCode.AutoSize = true;
      this.chbDeviceCode.Location = new System.Drawing.Point(4, 121);
      this.chbDeviceCode.Name = "chbDeviceCode";
      this.chbDeviceCode.Size = new System.Drawing.Size(88, 17);
      this.chbDeviceCode.TabIndex = 16;
      this.chbDeviceCode.Text = "Device Code";
      this.chbDeviceCode.UseVisualStyleBackColor = true;
      // 
      // chbVehicleNumber
      // 
      this.chbVehicleNumber.AutoSize = true;
      this.chbVehicleNumber.Location = new System.Drawing.Point(4, 87);
      this.chbVehicleNumber.Name = "chbVehicleNumber";
      this.chbVehicleNumber.Size = new System.Drawing.Size(101, 17);
      this.chbVehicleNumber.TabIndex = 15;
      this.chbVehicleNumber.Text = "Vehicle Number";
      this.chbVehicleNumber.UseVisualStyleBackColor = true;
      // 
      // chbSource
      // 
      this.chbSource.AutoSize = true;
      this.chbSource.Location = new System.Drawing.Point(4, 104);
      this.chbSource.Name = "chbSource";
      this.chbSource.Size = new System.Drawing.Size(60, 17);
      this.chbSource.TabIndex = 14;
      this.chbSource.Text = "Source";
      this.chbSource.UseVisualStyleBackColor = true;
      // 
      // chbVehicleName
      // 
      this.chbVehicleName.AutoSize = true;
      this.chbVehicleName.Location = new System.Drawing.Point(4, 36);
      this.chbVehicleName.Name = "chbVehicleName";
      this.chbVehicleName.Size = new System.Drawing.Size(61, 17);
      this.chbVehicleName.TabIndex = 12;
      this.chbVehicleName.Text = "Vehicle";
      this.chbVehicleName.UseVisualStyleBackColor = true;
      // 
      // chbTrainName
      // 
      this.chbTrainName.AutoSize = true;
      this.chbTrainName.Location = new System.Drawing.Point(4, 19);
      this.chbTrainName.Name = "chbTrainName";
      this.chbTrainName.Size = new System.Drawing.Size(50, 17);
      this.chbTrainName.TabIndex = 11;
      this.chbTrainName.Text = "Train";
      this.chbTrainName.UseVisualStyleBackColor = true;
      // 
      // chbGPSLongitude
      // 
      this.chbGPSLongitude.AutoSize = true;
      this.chbGPSLongitude.Location = new System.Drawing.Point(121, 172);
      this.chbGPSLongitude.Name = "chbGPSLongitude";
      this.chbGPSLongitude.Size = new System.Drawing.Size(98, 17);
      this.chbGPSLongitude.TabIndex = 10;
      this.chbGPSLongitude.Text = "GPS Longitude";
      this.chbGPSLongitude.UseVisualStyleBackColor = true;
      // 
      // chbGPSLatitude
      // 
      this.chbGPSLatitude.AutoSize = true;
      this.chbGPSLatitude.Location = new System.Drawing.Point(121, 155);
      this.chbGPSLatitude.Name = "chbGPSLatitude";
      this.chbGPSLatitude.Size = new System.Drawing.Size(89, 17);
      this.chbGPSLatitude.TabIndex = 9;
      this.chbGPSLatitude.Text = "GPS Latitude";
      this.chbGPSLatitude.UseVisualStyleBackColor = true;
      // 
      // btnExport
      // 
      this.btnExport.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnExport.Enabled = false;
      this.btnExport.Location = new System.Drawing.Point(7, 482);
      this.btnExport.Name = "btnExport";
      this.btnExport.Size = new System.Drawing.Size(75, 23);
      this.btnExport.TabIndex = 12;
      this.btnExport.Text = "Export";
      this.btnExport.UseVisualStyleBackColor = true;
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(179, 482);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 13;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      // 
      // saveFileDialog
      // 
      this.saveFileDialog.SupportMultiDottedExtensions = true;
      // 
      // drExport
      // 
      this.drExport.DDDHelper = null;
      this.drExport.Selector = null;
      // 
      // DRViewHelpProvider
      // 
      this.DRViewHelpProvider.HelpNamespace = "DRView.chm";
      // 
      // gbVariables
      // 
      this.gbVariables.Controls.Add(this.varTreeView);
      this.gbVariables.Enabled = false;
      this.gbVariables.Location = new System.Drawing.Point(260, 8);
      this.gbVariables.Name = "gbVariables";
      this.gbVariables.Size = new System.Drawing.Size(257, 449);
      this.gbVariables.TabIndex = 11;
      this.gbVariables.TabStop = false;
      this.gbVariables.Text = "Variables";
      // 
      // varTreeView
      // 
      this.varTreeView.DDDHelper = null;
      this.varTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
      this.varTreeView.Location = new System.Drawing.Point(3, 16);
      this.varTreeView.Name = "varTreeView";
      this.varTreeView.ShowBits = true;
      this.varTreeView.Size = new System.Drawing.Size(251, 430);
      this.varTreeView.TabIndex = 0;
      this.varTreeView.UseFilter = true;
      // 
      // persistWindow
      // 
      this.persistWindow.Form = this;
      this.persistWindow.UseLocAppDataDir = true;
      this.persistWindow.XMLFilePath = "DRViewWindowState.xml";
      // 
      // gbFile
      // 
      this.gbFile.Controls.Add(this.btSetFileName);
      this.gbFile.Controls.Add(this.tbFileName);
      this.gbFile.Location = new System.Drawing.Point(7, 425);
      this.gbFile.Name = "gbFile";
      this.gbFile.Size = new System.Drawing.Size(247, 51);
      this.gbFile.TabIndex = 14;
      this.gbFile.TabStop = false;
      this.gbFile.Text = "File Name";
      // 
      // RecExport
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(522, 507);
      this.Controls.Add(this.gbFile);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnExport);
      this.Controls.Add(this.gbVariables);
      this.Controls.Add(this.gbRecordAttributes);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.groupBox1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.DRViewHelpProvider.SetHelpKeyword(this, "ExportDialog.htm");
      this.DRViewHelpProvider.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
      this.MaximizeBox = false;
      this.MaximumSize = new System.Drawing.Size(528, 533);
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(528, 533);
      this.Name = "RecExport";
      this.DRViewHelpProvider.SetShowHelp(this, true);
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Diagnostic Records Export";
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.gbRecordAttributes.ResumeLayout(false);
      this.gbRecordAttributes.PerformLayout();
      this.gbVariables.ResumeLayout(false);
      this.gbFile.ResumeLayout(false);
      this.gbFile.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Button btSetFileName;
    private System.Windows.Forms.CheckBox chbTUInstance;
    private System.Windows.Forms.CheckBox chbTUType;
    private System.Windows.Forms.CheckBox chbDDDVersion;
    private System.Windows.Forms.CheckBox chbFaultCode;
    private System.Windows.Forms.CheckBox chbEndTime;
    private System.Windows.Forms.CheckBox chbStartTime;
    private System.Windows.Forms.CheckBox chbRecTitle;
    private System.Windows.Forms.CheckBox chbLastModified;
    private System.Windows.Forms.CheckBox chbTypeCount;
    private System.Windows.Forms.GroupBox gbRecordAttributes;
    private System.Windows.Forms.Button btnExport;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.SaveFileDialog saveFileDialog;
    private DRViewComponents.DRExport drExport;
    public System.Windows.Forms.RadioButton rbRecCSV;
    public System.Windows.Forms.RadioButton rbVarCSV;
    public System.Windows.Forms.TextBox tbTimeFormat;
    public System.Windows.Forms.MaskedTextBox tbSeparator;
    public System.Windows.Forms.CheckBox chbCompleteVarTitle;
    public System.Windows.Forms.CheckBox chbIncludeDDDVer;
    public System.Windows.Forms.TextBox tbFileName;
    private System.Windows.Forms.HelpProvider DRViewHelpProvider;
    private Mowog.PersistWindowComponent persistWindow;
    private System.Windows.Forms.CheckBox chbTrainName;
    private System.Windows.Forms.CheckBox chbGPSLongitude;
    private System.Windows.Forms.CheckBox chbGPSLatitude;
    private System.Windows.Forms.GroupBox gbVariables;
    public DRViewComponents.VarTreeView varTreeView;
    private System.Windows.Forms.CheckBox chbVehicleName;
    private System.Windows.Forms.CheckBox chbSource;
    private System.Windows.Forms.GroupBox gbFile;
    private System.Windows.Forms.CheckBox chbVehicleNumber;
    private System.Windows.Forms.CheckBox chbDeviceCode;
    private System.Windows.Forms.CheckBox chbImportTime;
    private System.Windows.Forms.CheckBox chbAcknowledgeTime;
    private System.Windows.Forms.CheckBox chbFaultSubSeverity;
    private System.Windows.Forms.CheckBox chbFaultSeverity;
    private System.Windows.Forms.CheckBox chbRecordType;
    private System.Windows.Forms.CheckBox chbTrcSnpCode;
    private System.Windows.Forms.CheckBox chbRecName;
  }
}