using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Reflection;
using DDDObjects;
using DRSelectorComponents;
using DRViewComponents;

namespace DRView
{
  public partial class DRQuery : Form
  {
    #region Public enumerations 
    /// <summary>
    /// Enumeration determining the type of DR Selector
    /// </summary>
    public enum eSelectorType
    {
      Basic = 1,
      RecParams = 2,
      RefAttributes = 3,
      File = 4,
      TUDirect = 5,
      DB = 6,
      XMLD = 7
    }

    /// <summary>
    /// Flags determining visible DRView components
    /// </summary>
    [Flags]
    public enum eVisibleDRViews
    {
      None = 0,
      MasterSlave = 2,
      TreeList = 4,
      Variables = 8,
      Statistics = 16,
      Grouped = 32,
      Map = 64
    }

    /// <summary>
    /// Severities of logged messages
    /// </summary>
    public enum eMsgSeverity
    {
      Info,
      Error
    }
    #endregion //Public enumerations 

    #region Private members
    private DRSelectorBase selector; //Selector component
    private InfoMessageEventhandler infoMessageHandler; //Handler for all information messages from components
    private ErrorMessageEventhandler errorMessageHandler; //Handler for all error messages from components
    private RecordCountEventHandler recordCountHandler; //Handler for record count message from selector
    private ReportProgressEventHandler progressReportHandler; //Handler for all progress reports from components
    private NDREventHandler newDataReadyHandler; //Handler for new data ready event from selector
    private eVisibleDRViews visibleViews = eVisibleDRViews.MasterSlave | eVisibleDRViews.TreeList | eVisibleDRViews.Variables | eVisibleDRViews.Statistics | eVisibleDRViews.Grouped | eVisibleDRViews.Map; //Keeps info about visible DRViews
    private RecExport exportDialog;
    private FilterDialog filterDialog;
    private int graphIndex = 1; //Index added to the name of graph tab page
    private eSelectorType selectorType; //Type of selector used on the form
    private frmMap mapForm = null; //Reference to form with map of diagnostic records
    private int queryNumber = 0; //Ordinal number of query window since application start
    private ViewConfiguration viewConfiguration; //Configuration parameters for record views
    #endregion //Private members

    #region Public properties
    /// <summary>
    /// Controls the visibilty of tab pages with different viewers
    /// </summary>
    public eVisibleDRViews VisibleViews
    {
      get { return visibleViews; }
      set { SetVisibleViewers(value); }
    }
    /// <summary>
    /// Configuraton parameters for record views 
    /// </summary>
    public ViewConfiguration ViewConfiguration
    {
      get { return viewConfiguration; }
      set
      { //Store view configuration
        viewConfiguration = value;
        //Update configuration of record views
        drViewMS.ViewConfiguration = value;
        drViewTreeList.ViewConfiguration = value;
        drViewVariables.ViewConfiguration = value;
        drViewStatistics.ViewConfiguration = value;
        drViewGrouped.ViewConfiguration = value;
      }
    }
    #endregion //Public properties

    #region Construction
    public DRQuery()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Constructor, creates and initializes DRSelector and DRViews
    /// </summary>
    /// <param name="selType">Type of selector to display</param>
    /// <param name="dbConnection">Connection to database to use</param>
    /// <param name="dddHelper">DDD Helper object for DDD Specific actions</param>
    /// <param name="visibleViews">Identify visible visualisation objects</param>
    /// <param name="queryNumber">Ordinal number of query window since application start</param>
    /// <param name="recordFile">Name of file with diagnostic records (for FileSelector only)</param>
    /// <param name="viewConfiguration">Configuration parameters for record views</param>
    public DRQuery(
      eSelectorType selType, SqlConnection dbConnection, DDDHelper dddHelper, 
      int queryNumber, String recordFile, ViewConfiguration viewConfiguration)
    {
      //Initialize components
      InitializeComponent();
      //Initialize event handlers
      infoMessageHandler = this.InfoMessageEvent;
      errorMessageHandler = this.ErrorMessageEvent;
      progressReportHandler = this.ProgressReportEvent;
      recordCountHandler = this.RecordCountEvent;
      newDataReadyHandler = this.NewDataReadyEvent;
      //Store view configuration
      ViewConfiguration = viewConfiguration;
      //Store type of selector used
      selectorType = selType;
      //Create selector component
      //And set correct icon
      switch (selType)
      {
        /*
        case eSelectorType.Basic:
          selector = new DRSelectorBasic();
          this.Icon = Properties.Resources.DBQuery;
          this.Text = Properties.Resources.strBasicQueryTitle + " " + queryNumber.ToString();
          break;
        case eSelectorType.RecParams:
          selector = new DRSelectorRecParams();
          this.Icon = Properties.Resources.AdvancedQuery;
          this.Text = Properties.Resources.strAdvancedQueryTitle + " " + queryNumber.ToString();
          break;
        case eSelectorType.RefAttributes:
          selector = new DRSelectorRefAttributes();
          this.Icon = Properties.Resources.RefAttrQuery;
          this.Text = Properties.Resources.strRefAttrQueryTitle + " " + queryNumber.ToString();
          break;
        */
        case eSelectorType.DB:
          selector = new DRSelectorDB();
          this.Icon = Properties.Resources.DBQuery;
          this.Text = Properties.Resources.strDBQueryTitle + " " + queryNumber.ToString();
          break;
        case eSelectorType.File:
          selector = new DRSelectorFile();
          this.Icon = Properties.Resources.FileQuery;
          this.Text = Properties.Resources.strFileQueryTitle + " " + queryNumber.ToString();
          ((DRSelectorFile)selector).FileName = recordFile;
          break;
        case eSelectorType.TUDirect:
          selector = new DRSelectorMDS();
          this.Icon = Properties.Resources.DirectTUQuery;
          this.Text = Properties.Resources.strTUDirectQueryTitle + " " + queryNumber.ToString();
          break;
        default:
          throw new Exception("Unkonwn selector type");
      }
      this.queryNumber = queryNumber;
      //Place selector on form
      this.tabSelector.Controls.Add(selector);
      selector.Dock = DockStyle.Fill;
      selector.Location = new Point(0, 0);
      selector.Name = "drSelector";
      selector.TabIndex = 0;
      selector.CommandTimeout = (int)Properties.Settings.Default.dbCommandTimeoutSec;
      //Set selector properties
      selector.MaxRecCount = (int)Properties.Settings.Default.maxRecorCount;
      selector.ComputeRecTypeCounts = Properties.Settings.Default.computeTypeCount;
      //Set connection object selector
      selector.DBConnection = dbConnection;
      //Create dialog for record exports
      exportDialog = new RecExport(selector, dddHelper);
      exportDialog.SelectRecAttributes(ViewConfiguration);
      //Create dialog for record filtering
      filterDialog = new FilterDialog(selector, dddHelper);
      //Set event handlers for events raised by filter dialog
      filterDialog.ErrorMessage += new DRSelectorComponents.ErrorMessageEventhandler(this.ErrorMessageEvent);
      filterDialog.InfoMessage += new InfoMessageEventhandler(this.InfoMessageEvent);
      filterDialog.ProgressReport += new ReportProgressEventHandler(this.ProgressReportEvent);
      //Set helper objects
      selector.DDDHelper = dddHelper;
      drViewMS.DDDHelper = dddHelper;
      drExport.DDDHelper = dddHelper;
      drViewTreeList.DDDHelper = dddHelper;
      drViewVariables.DDDHelper = dddHelper;
      drViewStatistics.DDDHelper = dddHelper;
      drViewGrouped.DDDHelper = dddHelper;
      //Connect viewers with selector
      selector.NewDataReady += drViewMS.NewDataReadyHandler;
      selector.NewDataReady += drViewTreeList.NewDataReadyHandler;
      selector.NewDataReady += drViewVariables.NewDataReadyHandler;
      selector.NewDataReady += drViewStatistics.NewDataReadyHandler;
      selector.NewDataReady += drViewGrouped.NewDataReadyHandler;
      selector.NewDataReady += exportDialog.NewDataReadyHandler;
      selector.NewDataReady += filterDialog.NewDataReadyHandler;
      selector.NewDataReady += this.newDataReadyHandler;
      //Connect views with selectors SelectRecord event
      selector.OnSelectRecord += drViewMS.SelectRecordHandler;
      selector.OnSelectRecord += drViewTreeList.SelectRecordHandler;
      selector.OnSelectRecord += drViewVariables.SelectRecordHandler;
      selector.OnSelectRecord += drViewStatistics.SelectRecordHandler;
      selector.OnSelectRecord += drViewGrouped.SelectRecordHandler;
      //Connect viewers with helper object
      dddHelper.NewLanguageSelected += drViewMS.NewLanguageHandler;
      dddHelper.NewLanguageSelected += drViewTreeList.NewLanguageHandler;
      dddHelper.NewLanguageSelected += drViewVariables.NewLanguageHandler;
      dddHelper.NewLanguageSelected += drViewStatistics.NewLanguageHandler;
      dddHelper.NewLanguageSelected += drViewGrouped.NewLanguageHandler;
      //Connect selector to Info and Error message handlers
      selector.InfoMessage += this.infoMessageHandler;
      selector.ErrorMessage += this.errorMessageHandler;
      selector.RecordCountMessage += this.recordCountHandler;
      //Set selector for drExport
      drExport.Selector = selector;
      //Set visibility of system messages
      systemMessagesToolStripMenuItem.Checked = Properties.Settings.Default.showSystemMessages;
      tsbShowMessages.Checked = Properties.Settings.Default.showSystemMessages;
      splContDRQuery.Panel2Collapsed = !Properties.Settings.Default.showSystemMessages;
    }
    #endregion //Construction

    #region Event handlers
    /// <summary>
    /// Show / hide log text box
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void tsbShowMessages_Click(object sender, EventArgs e)
    {
      if (sender == tsbShowMessages)
        systemMessagesToolStripMenuItem.Checked = tsbShowMessages.Checked;
      else
        tsbShowMessages.Checked = systemMessagesToolStripMenuItem.Checked;
      splContDRQuery.Panel2Collapsed = !tsbShowMessages.Checked;
    }

    /// <summary>
    /// Show / hide TreeView
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void tsbShowTreeView_Click(object sender, EventArgs e)
    {
      if (sender == tsbShowTreeView)
        treeViewToolStripMenuItem.Checked = tsbShowTreeView.Checked;
      else
        tsbShowTreeView.Checked = treeViewToolStripMenuItem.Checked;
      if (tsbShowTreeView.Checked)
      {
        VisibleViews |= eVisibleDRViews.TreeList;
        tbcComponents.SelectTab(tabVwTreeList);
      }
      else
        VisibleViews = VisibleViews & ~(eVisibleDRViews.TreeList);
    }

    /// <summary>
    /// Show / Hide grid view
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void tsbShowGridView_Click(object sender, EventArgs e)
    {
      if (sender == tsbShowGridView)
        gridViewToolStripMenuItem.Checked = tsbShowGridView.Checked;
      else
        tsbShowGridView.Checked = gridViewToolStripMenuItem.Checked;
      if (tsbShowGridView.Checked)
      {
        VisibleViews |= eVisibleDRViews.MasterSlave;
        tbcComponents.SelectTab(tabVwMasterSlave);
      }
      else
        VisibleViews = VisibleViews & ~(eVisibleDRViews.MasterSlave);
    }

    /// <summary>
    /// Show / Hide variables view
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void tsbShowVarsView_Click(object sender, EventArgs e)
    {
      if (sender == tsbShowVarsView)
        variablesViewToolStripMenuItem.Checked = tsbShowVarsView.Checked;
      else
        tsbShowVarsView.Checked = variablesViewToolStripMenuItem.Checked;
      if (tsbShowVarsView.Checked)
      {
        VisibleViews |= eVisibleDRViews.Variables;
        tbcComponents.SelectTab(tabVwVariables);
      }
      else
        VisibleViews = VisibleViews & ~(eVisibleDRViews.Variables);
    }

    /// <summary>
    /// Show / Hide statistics
    /// </summary>
    private void tsbShowStatistics_Click(object sender, EventArgs e)
    {
      if (sender == tsbShowStatistics)
        statisticsToolStripMenuItem.Checked = tsbShowStatistics.Checked;
      else
        tsbShowStatistics.Checked = statisticsToolStripMenuItem.Checked;
      if (tsbShowStatistics.Checked)
      {
        VisibleViews |= eVisibleDRViews.Statistics;
        tbcComponents.SelectTab(tabVwStatistics);
      }
      else
        VisibleViews = VisibleViews & ~(eVisibleDRViews.Statistics);
    }

    /// <summary>
    /// Show / Hide grouped view
    /// </summary>
    private void tsbShowGrouped_Click(object sender, EventArgs e)
    {
      if (sender == tsbShowGrouped)
        groupedViewToolStripMenuItem.Checked = tsbShowGrouped.Checked;
      else
        tsbShowGrouped.Checked = groupedViewToolStripMenuItem.Checked;
      if (tsbShowGrouped.Checked)
      {
        VisibleViews |= eVisibleDRViews.Grouped;
        tbcComponents.SelectTab(tabVwGrouped);
      }
      else
        VisibleViews = VisibleViews & ~(eVisibleDRViews.Grouped);
    }

    /// <summary>
    /// Show / Hide map view
    /// </summary>
    private void tsbShowMap_Click(object sender, EventArgs e)
    {
      if (sender == tsbShowMap)
        mapViewToolStripMenuItem.Checked = tsbShowMap.Checked;
      else
        tsbShowMap.Checked = mapViewToolStripMenuItem.Checked;
      if (tsbShowMap.Checked)
        VisibleViews |= eVisibleDRViews.Map;
      else
        VisibleViews = VisibleViews & ~(eVisibleDRViews.Map);
    }

    /// <summary>
    /// Presets checked property for menu items
    /// </summary>
    private void msDRQuery_MenuActivate(object sender, EventArgs e)
    {
      gridViewToolStripMenuItem.Checked = tsbShowGridView.Checked;
      treeViewToolStripMenuItem.Checked = tsbShowTreeView.Checked;
      variablesViewToolStripMenuItem.Checked = tsbShowVarsView.Checked;
      statisticsToolStripMenuItem.Checked = tsbShowStatistics.Checked;
      groupedViewToolStripMenuItem.Checked = tsbShowGrouped.Checked;
      mapViewToolStripMenuItem.Checked = tsbShowMap.Checked;
      systemMessagesToolStripMenuItem.Checked = tsbShowMessages.Checked;
    }

    /// <summary>
    /// Handles all error messages generated by components on the form
    /// </summary>
    /// <param name="sender">Sending component</param>
    /// <param name="error">Error in the form of exception</param>
    public void ErrorMessageEvent(object sender, Exception error)
    {
      Exception ex;
      //Log exception to listbox
      LogMsg(error.Message, eMsgSeverity.Error, 0);
      //Log all inner exceptions
      ex = error;
      while ((ex = ex.InnerException) != null)
        LogMsg(ex.Message, eMsgSeverity.Error, 1);
    }

    /// <summary>
    /// Handles all info messages generated by components on the form
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="message"></param>
    public void InfoMessageEvent(object sender, string message)
    {
      //Log message
      LogMsg(message, eMsgSeverity.Info, 0);
    }

    /// <summary>
    /// Set text on status bar label
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="message"></param>
    private void RecordCountEvent(object sender, RecCounEventArgs args)
    {
      statusLabelRecords.Text = String.Format(Properties.Resources.strRecCount, args.RecCount);
      Update(); //Redraw the form
    }

    /// <summary>
    /// Handles all progress reports from components on the form
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="progress"></param>
    /// <param name="showBar"></param>
    public void ProgressReportEvent(Object sender, int progress, bool showBar)
    {
      //Set progess bar visibility
      if (showBar != progresBar.Visible)
        progresBar.Visible = showBar;
      //Set progress bar value
      progress = Math.Max(progresBar.Minimum, progress);
      progress = Math.Min(progresBar.Maximum, progress);
      progresBar.Value = progress;
      //Update form
      Update();
    }

    /// <summary>
    /// Method handles NewDataReady event raised by SelectorComponent.
    /// </summary>
    /// <param name="sender">selector that raised the event</param>
    private void NewDataReadyEvent(DRSelectorBase sender, NDREventArgs args)
    {
      RecordCountEvent(this, new RecCounEventArgs(sender.RecordsView.Count));
      if (args.CountLimitReached)
        MessageBox.Show(Properties.Resources.strCountLimitWarning, "Count limit", MessageBoxButtons.OK, MessageBoxIcon.Warning);
      LogMsg(Properties.Resources.strReady, eMsgSeverity.Info, 0);
    }

    /// <summary>
    /// Saves dataset with selected records to XML file.
    /// </summary>
    private void saveSelectedDataToolStripMenuItem_Click(object sender, EventArgs e)
    {
      //Prepare save file dialog
      dlgSaveFile.Reset();
      dlgSaveFile.DefaultExt = Properties.Resources.DRFileExtension;
      dlgSaveFile.Filter = Properties.Resources.DRFilesFilter;
      //Show file save dialog
      if (dlgSaveFile.ShowDialog() == DialogResult.OK)
        selector.SaveRecordsToFile(dlgSaveFile.FileName);
    }

    /// <summary>
    /// Displays record export dialog and exports records according to user selection
    /// </summary>
    private void exportRecordsToolStripMenuItem_Click(object sender, EventArgs e)
    {
      try
      {
        //Display dialog
        if (exportDialog.ShowDialog() != DialogResult.OK)
          return; //User pressed cancel button
      }
      catch (Exception ex)
      {
        ErrorMessageEvent(this, new Exception("Failed to show export dialog", ex));
      }

      //Export records
      //Determine export method
      if (exportDialog.rbRecCSV.Checked)
        drExport.SaveRecordsToCSV(exportDialog.tbSeparator.Text, exportDialog.tbTimeFormat.Text,
                                  exportDialog.tbFileName.Text, exportDialog.SelectedRecAttributes());
      else if (exportDialog.rbVarCSV.Checked)
        drExport.SaveVarsToCSV(exportDialog.varTreeView.GetSelectedVarIDs(), exportDialog.tbSeparator.Text,
                               exportDialog.tbTimeFormat.Text, exportDialog.tbFileName.Text,
                               exportDialog.chbCompleteVarTitle.Checked, exportDialog.chbIncludeDDDVer.Checked);
    }

    /// <summary>
    /// Displays dialog enabling the user to select filter conditions
    /// </summary>
    private void tsbShowFilterDialog_Click(object sender, EventArgs e)
    {
      //Display dialog
      DialogResult res = filterDialog.ShowDialog();
      if (res == DialogResult.OK)
      {
        selector.ApplyFilter(filterDialog.RecordFilter, filterDialog.SelectedVariables);
        tsbApplyFilter.Checked = true;
        applyFilterToolStripMenuItem.Checked = true;
      }
      else if (res == DialogResult.Ignore)
      {
        selector.RemoveFilter();
        tsbApplyFilter.Checked = false;
        applyFilterToolStripMenuItem.Checked = false;
      }
    }

    /// <summary>
    /// Applies / removes filters for selected records
    /// </summary>
    private void tsbApplyFilter_Click(object sender, EventArgs e)
    {
      if (sender == applyFilterToolStripMenuItem)
        tsbApplyFilter.Checked = applyFilterToolStripMenuItem.Checked;
      else if (sender == tsbApplyFilter)
        applyFilterToolStripMenuItem.Checked = tsbApplyFilter.Checked;
      if (tsbApplyFilter.Checked)
        selector.ApplyFilter(filterDialog.RecordFilter, filterDialog.SelectedVariables);
      else
        selector.RemoveFilter();
    }

    private void DRQuery_FormClosed(object sender, FormClosedEventArgs e)
    {
      //Close the child form with map, if opened
      if (mapForm != null)
        mapForm.Close();
      mapForm = null;      
      this.Dispose();
    }

    /// <summary>
    /// Create new tab with graph component
    /// </summary>
    private void tsbNewGraph_Click(object sender, EventArgs e)
    {
      CreateGraphTab(null);
    }

    /// <summary>
    /// Close and destroy tab with graph component sending this event
    /// </summary>
    private void drViewGraph_OnCloseGraph(DRViewGraph sender, EventArgs args)
    {
      //Find tab page containing graph
      TabPage graphPage = TabPage.GetTabPageOfComponent(sender);
      if (graphPage == null)
        return; //No tab page exists for graph
      //Remove page from tab control
      tbcComponents.TabPages.Remove(graphPage);
      //Dispose graph
      sender.Dispose();
      //Dispose tab page
      graphPage.Dispose();
    }

    /// <summary>
    /// Create new graph tab which displays selected variables
    /// </summary>
    /// <param name="sender">Event sender</param>
    /// <param name="selectedVariables">Variables to display</param>
    private void drViewVariables_OnGraphVariables(object sender, VarIDDictionary selectedVariables)
    {
      CreateGraphTab(selectedVariables);
    }

    /// <summary>
    /// Executes query for diagnostic records by calling selector of the form
    /// </summary>
    private void tsbSelect_Click(object sender, EventArgs e)
    {
      //Load records using selector
      selector.LoadRecordsAsync();
    }

    /// <summary>
    /// Saves complete settings of current diagnostic query into binary file
    /// </summary>
    private void saveQueryToolStripMenuItem_Click(object sender, EventArgs e)
    {
      try
      {
        //Prepare save file dialog
        dlgSaveFile.Reset();
        dlgSaveFile.DefaultExt = Properties.Resources.DRQFileExtension;
        dlgSaveFile.Filter = Properties.Resources.DRQFilesFilter;
        //Show file save dialog
        if (dlgSaveFile.ShowDialog() == DialogResult.OK)
        {
          //Open file for writing
          FileStream file = new FileStream(dlgSaveFile.FileName, FileMode.Create, FileAccess.Write);
          //Create binary formatter for object serialization
          BinaryFormatter formatter = new BinaryFormatter();
          //Save selector type
          formatter.Serialize(file, selectorType);
          //Save selector settings
          selector.SaveSettingsToFile(file, formatter);
          //Save filter settings
          filterDialog.SaveToFile(file, formatter);
          //Save filter applied setting
          formatter.Serialize(file, tsbApplyFilter.Checked);
          //Close file
          file.Close();
          file.Dispose();
        }
      }
      catch (Exception ex)
      {
        ErrorMessageEvent(this, new Exception("Failed to save query", ex));        
      }
    }
    #endregion //Event handlers

    #region Helper methods
    /// <summary>
    /// Sets view visibility according to passed flags
    /// </summary>
    /// <param name="flags">Flags specifying the visibility</param>
    private void SetVisibleViewers(eVisibleDRViews flags)
    {
      //Display/hide MasterSlave view
      if ((visibleViews & eVisibleDRViews.MasterSlave) != (flags & eVisibleDRViews.MasterSlave))
      { //Change visibility of Master/Slave view
        if ((flags & eVisibleDRViews.MasterSlave) == 0)
          tbcComponents.TabPages.Remove(tabVwMasterSlave);
        else
          tbcComponents.TabPages.Add(tabVwMasterSlave);
      }
      drViewMS.DisplayData = (flags & eVisibleDRViews.MasterSlave) > 0;
      tsbShowGridView.Checked = (flags & eVisibleDRViews.MasterSlave) > 0;
      gridViewToolStripMenuItem.Checked = (flags & eVisibleDRViews.MasterSlave) > 0;
      //Display/hide TreeList view
      if ((visibleViews & eVisibleDRViews.TreeList) != (flags & eVisibleDRViews.TreeList))
      { //Change visibility of TreeList view
        if ((flags & eVisibleDRViews.TreeList) == 0)
          tbcComponents.TabPages.Remove(tabVwTreeList);
        else
          tbcComponents.TabPages.Add(tabVwTreeList);
      }
      drViewTreeList.DisplayData = (flags & eVisibleDRViews.TreeList) > 0;
      tsbShowTreeView.Checked = (flags & eVisibleDRViews.TreeList) > 0;
      treeViewToolStripMenuItem.Checked = (flags & eVisibleDRViews.TreeList) > 0;
      //Display/hide Variables view
      if ((visibleViews & eVisibleDRViews.Variables) != (flags & eVisibleDRViews.Variables))
      { //Change visibility of Variables view
        if ((flags & eVisibleDRViews.Variables) == 0)
          tbcComponents.TabPages.Remove(tabVwVariables);
        else
          tbcComponents.TabPages.Add(tabVwVariables);
      }
      drViewVariables.DisplayData = (flags & eVisibleDRViews.Variables) > 0;
      tsbShowVarsView.Checked = (flags & eVisibleDRViews.Variables) > 0;
      variablesViewToolStripMenuItem.Checked = (flags & eVisibleDRViews.Variables) > 0;
      //Display/hide Statistics view
      if ((visibleViews & eVisibleDRViews.Statistics) != (flags & eVisibleDRViews.Statistics))
      { //Change visibility of statistics view
        if ((flags & eVisibleDRViews.Statistics) == 0)
          tbcComponents.TabPages.Remove(tabVwStatistics);
        else
          tbcComponents.TabPages.Add(tabVwStatistics);
      }
      drViewStatistics.DisplayData = (flags & eVisibleDRViews.Statistics) > 0;
      tsbShowStatistics.Checked = (flags & eVisibleDRViews.Statistics) > 0;
      statisticsToolStripMenuItem.Checked = (flags & eVisibleDRViews.Statistics) > 0;
      //Display/hide Grouped view
      if ((visibleViews & eVisibleDRViews.Grouped) != (flags & eVisibleDRViews.Grouped))
      { //Change visibility of grouped view
        if ((flags & eVisibleDRViews.Grouped) == 0)
          tbcComponents.TabPages.Remove(tabVwGrouped);
        else
          tbcComponents.TabPages.Add(tabVwGrouped);
      }
      drViewGrouped.DisplayData = (flags & eVisibleDRViews.Grouped) > 0;
      tsbShowGrouped.Checked = (flags & eVisibleDRViews.Grouped) > 0;
      groupedViewToolStripMenuItem.Checked = (flags & eVisibleDRViews.Grouped) > 0;
      //Display/hide Map view
      if ((visibleViews & eVisibleDRViews.Map) != (flags & eVisibleDRViews.Map))
      { //Change visibility of map view
        if ((flags & eVisibleDRViews.Map) == 0)
        {
          //Stores flags
          this.visibleViews = flags;
          //Close the form with map
          if (mapForm != null)
            mapForm.Close();
          mapForm = null;
        }
        else
        {
          //Create and show the form with map
          mapForm = new frmMap(selector, this, queryNumber);
          mapForm.MdiParent = this.MdiParent;
          mapForm.Show();
        }
      }
      tsbShowMap.Checked = (flags & eVisibleDRViews.Map) > 0;
      mapViewToolStripMenuItem.Checked = (flags & eVisibleDRViews.Map) > 0;
      //Stores flags
      this.visibleViews = flags;
    }

    private void LogMsg(String message, eMsgSeverity severity, int indent)
    {
      //Create header for the string
      String header = DateTime.Now.ToString("HH:mm:ss.fff: ");
      header = header.PadRight(header.Length + indent * 2);
      //Display message
      lbMessages.Items.Add(header + message);
      lbMessages.TopIndex = lbMessages.Items.Count - 1;
      //Set status label
      if (indent == 0)
        statusLabel.Text = message;
      Update(); //Redraw the form
    }

    /// <summary>
    /// Creates new tab page with graph component and adds to tab control
    /// </summary>
    /// <param name="selVariables">variables which shall be displayed in graph</param>
    private void CreateGraphTab(VarIDDictionary selVariables)
    {
      //Create page and graph component
      TabPage graphPage = new TabPage();
      DRViewGraph graph = new DRViewGraph();
      //Set properties of tab page
      graphPage.SuspendLayout();
      tbcComponents.Controls.Add(graphPage);
      graphPage.Controls.Add(graph);
      graphPage.Location = new System.Drawing.Point(4, 22);
      graphPage.Name = "tpGraph" + graphIndex;
      graphPage.Padding = new System.Windows.Forms.Padding(3);
      graphPage.Size = new System.Drawing.Size(913, 513);
      graphPage.Text = "Graph " + graphIndex;
      graphPage.UseVisualStyleBackColor = true;
      graphPage.ResumeLayout();
      //Set properties of graph component
      graph.DDDHelper = selector.DDDHelper;
      graph.DisplayData = true;
      graph.Dock = System.Windows.Forms.DockStyle.Fill;
      graph.Location = new System.Drawing.Point(3, 3);
      graph.Name = "drViewGraph" + graphIndex;
      graph.Size = new System.Drawing.Size(907, 507);
      graph.TabIndex = 0;
      graph.ViewConfiguration = ViewConfiguration;
      graph.InfoMessage += new DRSelectorComponents.InfoMessageEventhandler(this.InfoMessageEvent);
      graph.ProgressReport += new DRSelectorComponents.ReportProgressEventHandler(this.ProgressReportEvent);
      graph.ErrorMessage += new DRSelectorComponents.ErrorMessageEventhandler(this.ErrorMessageEvent);

      selector.NewDataReady += graph.NewDataReadyHandler;
      graph.DDDHelper.NewLanguageSelected += graph.NewLanguageHandler;
      graph.OnCloseGraph += new CloseGraphEventHandler(drViewGraph_OnCloseGraph);

      //Make graph page active
      tbcComponents.SelectedTab = graphPage;
      //Increment index of graph page
      graphIndex++;
      //Initiate graph
      graph.InitiateGraph(this.selector, selVariables);
      //Display selected variables in graph, if any
      if (selVariables != null)
        graph.DisplayVariables();
    }
    #endregion //Helper methods

    #region Public methods
    /// <summary>
    /// Loads previously saved query and filter settings from given file
    /// </summary>
    /// <param name="file">File stream to load from</param>
    /// <param name="formatter">Formatter to use for object deserialization</param>
    public void LoadQuerySetting(FileStream file, BinaryFormatter formatter)
    {
      //Load selector settings
      selector.LoadSettingsFromFile(file, formatter);
      //Load filter settings
      filterDialog.LoadFromFile(file, formatter);
      //Load filter applied status
      bool filtered = (bool)formatter.Deserialize(file);
      //Apply filter if required
      if (filtered)
      {
        selector.ApplyFilter(filterDialog.RecordFilter, filterDialog.SelectedVariables);
        tsbApplyFilter.Checked = true;
        applyFilterToolStripMenuItem.Checked = true;
      }
    }
    #endregion //Public methods

  }
}
