using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Diagnostics;
using System.Windows.Forms;
using System.IO;
using DRView.PersistWindowState;

namespace Mowog
{
	public class PersistWindowComponent : System.ComponentModel.Component
	{
		#region Declarations
		// event info that allows form to persist extra window state data
		public delegate void WindowStateDelegate(object sender, WindowStateInfo WindowInfo);
		public event WindowStateDelegate LoadStateEvent;
		public event WindowStateDelegate SaveStateEvent;

		private Form mParentForm;
		private WindowStateInfo mWindowInfo = new WindowStateInfo();
		private PersistMethods mPersistMethod = PersistMethods.XMLFile;
		private string mRegistryPath = "";
		private string mXMLFilePath = "WindowState.xml";
    private bool mbUseLocAppDataDir = true;
		private static WindowState_DS mWindowState_DS = new WindowState_DS();
		private System.Data.DataRow mWindowStateInfo_DR;
		private static bool mDSNotLoaded = true;
		public enum PersistMethods { Registry, XMLFile, Custom }

		private System.ComponentModel.Container components = null;
		#endregion

		#region Public Properties

		[DefaultValue(PersistMethods.XMLFile)]
		[Description("Method of persisting window state information."), 
		Category("Persist Configuration")]
		public PersistMethods PersistMethod
		{
			get { return this.mPersistMethod; }
			set { this.mPersistMethod = value; }
		}


		[DefaultValue("")]
		[Description("Path for saving XML file info to."), 
		Category("Persist Configuration")]
		public string XMLFilePath
		{
			get { return this.mXMLFilePath; }
			set { this.mXMLFilePath=value; }
		}
		
		
		[DefaultValue("")]
		[Description("Path for saving info to registry."), 
		Category("Persist Configuration")]
		public string RegistryPath
		{
			get { return this.mRegistryPath; }
			set { this.mRegistryPath=value; }
		}

    [DefaultValue(false)]
    [Description("Automatically use local application data directory for xml file"),
    Category("Persist Configuration")]
    public bool UseLocAppDataDir
    {
      get { return mbUseLocAppDataDir; }
      set { this.mbUseLocAppDataDir = value; }
    }
		
		[Browsable(false)]
		public Form Form
		{
			get
			{
				if (this.mParentForm == null)
				{
					if (this.Site.DesignMode)
					{
						IDesignerHost dh = (IDesignerHost)this.GetService(typeof(IDesignerHost));

						if (dh != null)
						{
							Object obj = dh.RootComponent;
							if (obj != null)
							{
								this.mParentForm = (Form)obj;
							}
						}
					}
				}

				return this.mParentForm;
			}

			set
			{
				if (this.mParentForm != null)
					return;

				if (value != null)
				{
					this.mParentForm = value;
					
					// subscribe to parent form's events
					mParentForm.Closing += new System.ComponentModel.CancelEventHandler(OnClosing);
					mParentForm.Resize += new System.EventHandler(OnResize);
					mParentForm.Move += new System.EventHandler(OnMove);
					mParentForm.Load += new System.EventHandler(OnLoad);

					// get initial width and height in case form is never resized
					mWindowInfo.Width = mParentForm.Width;
					mWindowInfo.Height = mParentForm.Height;

					this.mRegistryPath = "Software\\" + this.mParentForm.CompanyName + "\\" + System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
					this.mXMLFilePath = System.IO.Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location).ToString() + "\\WindowStateInfo.xml";
					this.mWindowStateInfo_DR = mWindowState_DS.WindowStateInfo.NewRow();
				}
			}
		}

		
		#endregion

		#region Public Methods

		public void SetWindowInfo(WindowStateInfo WindowInfo)
		{
			this.mWindowInfo = WindowInfo;

			mParentForm.Location = new System.Drawing.Point(mWindowInfo.Left, mWindowInfo.Top);
			mParentForm.Size = new System.Drawing.Size(mWindowInfo.Width, mWindowInfo.Height);
			mParentForm.WindowState = mWindowInfo.WindowState;
		}


		#endregion

		#region Private Form Event Handlers

		private void OnResize(object sender, System.EventArgs e)
		{
			// save width and height
			if(mParentForm.WindowState == FormWindowState.Normal)
			{
				mWindowInfo.Width = mParentForm.Width;
				mWindowInfo.Height = mParentForm.Height;
			}
		}

		
		private void OnMove(object sender, System.EventArgs e)
		{
			// save position
			if(mParentForm.WindowState == FormWindowState.Normal)
			{
				mWindowInfo.Left = mParentForm.Left;
				mWindowInfo.Top= mParentForm.Top;
			}
			// save state
			mWindowInfo.WindowState = mParentForm.WindowState;
		}


		private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// check if we are allowed to save the state as minimized (not normally)
			if(! mWindowInfo.AllowSaveMinimised)
			{
				if(mWindowInfo.WindowState == FormWindowState.Minimized)
					mWindowInfo.WindowState= FormWindowState.Normal;
			}

			switch (this.mPersistMethod)
			{
				case PersistMethods.XMLFile:
					if (this.mXMLFilePath == "")
						throw new Exception("XML File path is empty");

					this.SaveInfoToXMLFile();
					break;
				case PersistMethods.Registry:
					if (this.mRegistryPath == "")
						throw new Exception("Registry File path is empty");

					this.SaveInfoToRegistry();
					break;
				case PersistMethods.Custom:
					// fire SaveState event
					if(SaveStateEvent != null)
						SaveStateEvent(this, this.mWindowInfo);

					break;
			}
		}

		
		private void OnLoad(object sender, System.EventArgs e)
		{
			switch (this.mPersistMethod)
			{
				case PersistMethods.XMLFile:
					if (this.mXMLFilePath == "")
						throw new Exception("XML File path is empty");

					this.LoadInfoFromXMLFile();
					break;
				case PersistMethods.Registry:
					if (this.mRegistryPath == "")
						throw new Exception("Registry File path is empty");

					this.LoadInfoFromRegistry();
					break;
				case PersistMethods.Custom:
					// fire LoadState event
					if(LoadStateEvent != null)
						LoadStateEvent(this, this.mWindowInfo);

					break;
			}
		}

	
		#endregion

		#region Private Support Functions

		private void LoadInfoFromRegistry()
		{
			// attempt to read state from registry
			Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(this.mRegistryPath);
			string KeyPrefix = this.mParentForm.GetType().Namespace+"."+this.mParentForm.GetType().Name;

			if(key != null)
			{
				int left = (int)key.GetValue(KeyPrefix+"_Left", mParentForm.Left);
				int top = (int)key.GetValue(KeyPrefix+"_Top", mParentForm.Top);
				int width = (int)key.GetValue(KeyPrefix+"_Width", mParentForm.Width);
				int height = (int)key.GetValue(KeyPrefix+"_Height", mParentForm.Height);
				FormWindowState windowState = (FormWindowState)key.GetValue(KeyPrefix+"_WindowState", 
					(int)mParentForm.WindowState);

				mParentForm.Location = new System.Drawing.Point(left, top);
				mParentForm.Size = new System.Drawing.Size(width, height);
				mParentForm.WindowState = windowState;

				this.mWindowInfo.Left = mParentForm.Left;
				this.mWindowInfo.Top = mParentForm.Top;
				this.mWindowInfo.Height = mParentForm.Height;
				this.mWindowInfo.Width = mParentForm.Width;
				this.mWindowInfo.WindowState = mParentForm.WindowState;
			}
		}

		
		private void SaveInfoToRegistry()
		{
			// save position, size and state
			Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(this.mRegistryPath);
			string KeyPrefix = this.mParentForm.GetType().Namespace+"."+this.mParentForm.GetType().Name;

			key.SetValue(KeyPrefix+"_Left", mWindowInfo.Left);
			key.SetValue(KeyPrefix+"_Top", mWindowInfo.Top);
			key.SetValue(KeyPrefix+"_Width", mWindowInfo.Width);
			key.SetValue(KeyPrefix+"_Height", mWindowInfo.Height);
			
			key.SetValue(KeyPrefix+"_WindowState", (int)mWindowInfo.WindowState);
		}


		private void LoadInfoFromXMLFile()
		{
			string KeyPrefix = this.mParentForm.GetType().Namespace+"."+this.mParentForm.GetType().Name;
      string fileName = this.mXMLFilePath;
      if (mbUseLocAppDataDir)
        fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), fileName);

      if (System.IO.File.Exists(fileName) && mDSNotLoaded)
			{
        mWindowState_DS.ReadXml(fileName);
        mWindowState_DS.ReadXmlSchema(fileName);
				mDSNotLoaded = false;
			}

			if (mWindowState_DS.WindowStateInfo.Rows.Contains(KeyPrefix))
			{
				this.mWindowStateInfo_DR = mWindowState_DS.WindowStateInfo.Select("FormId='"+KeyPrefix+"'")[0];

				int left = (int)mWindowStateInfo_DR["Left"];
				int top = (int)mWindowStateInfo_DR["Top"];
				int width = (int)mWindowStateInfo_DR["Width"];
				int height = (int)mWindowStateInfo_DR["Height"];
				FormWindowState windowState = (FormWindowState)mWindowStateInfo_DR["WindowState"];

				mParentForm.Location = new System.Drawing.Point(left, top);
				mParentForm.Size = new System.Drawing.Size(width, height);
				mParentForm.WindowState = windowState;

				this.mWindowInfo.Left = mParentForm.Left;
				this.mWindowInfo.Top = mParentForm.Top;
				this.mWindowInfo.Height = mParentForm.Height;
				this.mWindowInfo.Width = mParentForm.Width;
				this.mWindowInfo.WindowState = mParentForm.WindowState;
			}

      if (!System.IO.File.Exists(fileName))
			{
				this.OnClosing(null, null);
				mDSNotLoaded = false;
			}
		}


		private void SaveInfoToXMLFile()
		{
			string KeyPrefix = this.mParentForm.GetType().Namespace+"."+this.mParentForm.GetType().Name;

			if (! mWindowState_DS.WindowStateInfo.Rows.Contains(KeyPrefix))
			{
				mWindowStateInfo_DR["FormId"] = KeyPrefix;
				mWindowState_DS.WindowStateInfo.Rows.Add(this.mWindowStateInfo_DR);
			}

			mWindowStateInfo_DR["Height"] = this.mWindowInfo.Height;
			mWindowStateInfo_DR["Width"] = this.mWindowInfo.Width;
			mWindowStateInfo_DR["Left"] = this.mWindowInfo.Left;
			mWindowStateInfo_DR["Top"] = this.mWindowInfo.Top;
			mWindowStateInfo_DR["WindowState"] = (int)this.mWindowInfo.WindowState;
			mWindowStateInfo_DR.AcceptChanges();
			mWindowState_DS.AcceptChanges();

      string fileName = this.mXMLFilePath;
      if (mbUseLocAppDataDir)
        fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), fileName);

      System.IO.FileInfo FileInfo = new System.IO.FileInfo(fileName);
			if (! FileInfo.Directory.Exists)
				System.IO.Directory.CreateDirectory(FileInfo.Directory.FullName);

      mWindowState_DS.WriteXml(fileName, System.Data.XmlWriteMode.WriteSchema);
		}

		
		#endregion

		#region Creator

		public PersistWindowComponent(System.ComponentModel.IContainer container)
		{
			container.Add(this);
			InitializeComponent();
		}

		public PersistWindowComponent()
		{
			InitializeComponent();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion

		#endregion
	}

	#region WindowStateInfo

	public class WindowStateInfo
	{
		public Int32 Top;
		public Int32 Left;
		public Int32 Height;
		public Int32 Width;
		public FormWindowState WindowState;
		public bool AllowSaveMinimised = false;
	}

	#endregion
}