using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DRView
{
  /// <summary>
  /// Dialog for selection of database connection
  /// </summary>
  public partial class frmDBConnection : Form
  {
    #region Private members
    private SqlConnectionStringBuilder connStringBuilder;
    private static bool alwaysShowDialog = false;
    #endregion

    #region Public static members
    public static String LastConnString = String.Empty;
    #endregion //Public static members

    #region Public properties
    /// <summary>
    /// Connection string selected by the user
    /// </summary>
    public String ConnectionString
    {
      get 
      { 
        //Check if connection has been previously specified
        if (LastConnString == String.Empty)
          LastConnString = GetConnString(); //Get connection specifications (properties or dialog)
        alwaysShowDialog = false; //Reset always show flag
        return LastConnString; //Return specified connection
      }
    }
    #endregion //Public properties

    #region Construction
    public frmDBConnection()
    {
      InitializeComponent();
      //Create connection string builder
      connStringBuilder = new SqlConnectionStringBuilder();
      //Read preferred connection from configuration
      eDBConnTypes connType = (eDBConnTypes)Properties.Settings.Default.preferredDBConn;
      switch (connType)
      {
        case eDBConnTypes.None:
          chbUseDBConn.Checked = false;
          cbConnType.SelectedIndex = 0;
          break;
        case eDBConnTypes.FirstConnection:
          chbUseDBConn.Checked = true;
          cbConnType.SelectedIndex = 0;
          break;
        case eDBConnTypes.SecondConnection:
          chbUseDBConn.Checked = true;
          cbConnType.SelectedIndex = 1;
          break;
      }
    }
    #endregion //Construction

    #region Public static methods
    /// <summary>
    /// Invalidate (empty) last specified connection string - used when DB connection was not succesfull
    /// </summary>
    public static void InvalidateLastConnString(bool alwaysShowDialog)
    {
      LastConnString = String.Empty;
      frmDBConnection.alwaysShowDialog = alwaysShowDialog;
    }
    #endregion //Public static methods

    #region Helper methods
    private void chbUseDBConn_CheckedChanged(object sender, EventArgs e)
    {
      gbDBConn.Enabled = chbUseDBConn.Checked;
    }

    private void ShowConnString()
    {
      tbDBServerName.Text = connStringBuilder.DataSource;
      tbDBName.Text = connStringBuilder.InitialCatalog;
      if (connStringBuilder.IntegratedSecurity)
      {
        rbDBWinAuth.Checked = true;
        tbDBPassword.Enabled = false;
        tbDBUser.Enabled = false;
      }
      else
      {
        rbDBSqlAuth.Checked = true;
        tbDBUser.Text = connStringBuilder.UserID;
        tbDBPassword.Text = connStringBuilder.Password;
        tbDBPassword.Enabled = true;
        tbDBUser.Enabled = true;
      }
    }

    private String GetConnString()
    {
      if (!Properties.Settings.Default.askForDBConn)
      { //Do not ask for DB connection by default
        if (!chbUseDBConn.Checked)
          return String.Empty; //Do not use DB connection
        if (!alwaysShowDialog &&
          (connStringBuilder.IntegratedSecurity || (connStringBuilder.Password != String.Empty)))
          return connStringBuilder.ConnectionString; //Connection string properly specified
      }
      //Display dialog to obtain connection details from the user
      this.ShowDialog();
      //Create connection string from parameters provided by the user
      if (!chbUseDBConn.Checked)
        return String.Empty;
      //Build connection string
      connStringBuilder.Clear();
      connStringBuilder.DataSource = tbDBServerName.Text;
      connStringBuilder.InitialCatalog = tbDBName.Text;
      if (rbDBWinAuth.Checked)
      {
        connStringBuilder.IntegratedSecurity = true;
        connStringBuilder.UserID = String.Empty;
        connStringBuilder.Password = String.Empty;
      }
      else
      {
        connStringBuilder.IntegratedSecurity = false;
        connStringBuilder.UserID = tbDBUser.Text;
        connStringBuilder.Password = tbDBPassword.Text;
        connStringBuilder.PersistSecurityInfo = true;
      }
      return connStringBuilder.ConnectionString;
    }

    private void rbDBSqlAuth_CheckedChanged(object sender, EventArgs e)
    {
      tbDBUser.Enabled = rbDBSqlAuth.Checked;
      tbDBPassword.Enabled = rbDBSqlAuth.Checked;
    }

    private void cbConnType_SelectedIndexChanged(object sender, EventArgs e)
    {
      switch (cbConnType.SelectedIndex)
      {
        case 0:
          gbConnSpec.Enabled = false;
          connStringBuilder.Clear();
          connStringBuilder.ConnectionString = Properties.Settings.Default.firstDBConnStr;
          break;
        case 1:
          gbConnSpec.Enabled = false;
          connStringBuilder.Clear();
          connStringBuilder.ConnectionString = Properties.Settings.Default.secondDBConnStr;
          break;
        case 2:
          gbConnSpec.Enabled = true;
          break;
      }
      ShowConnString();
    }
    #endregion //Helper methods
  }
}
