using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using DDDObjects;
using StatSelectorComponents;

namespace DRView
{
  /// <summary>
  /// Form with statistical query
  /// </summary>
  public partial class StatsQuery : Form
  {
    #region Public enumerations 
    public enum eStatSelectorType
    {
      DB = 1,
      File = 2
    }

    /// <summary>
    /// Flags determining visible StatView components
    /// </summary>
    [Flags]
    public enum eVisibleStatViews
    {
      None = 0,
      Table = 2,
      Graph = 4
    }

    /// <summary>
    /// Severities of logged messages
    /// </summary>
    public enum eMsgSeverity
    {
      Info,
      Error
    }
    #endregion //Public enumerations 

    #region Private members
    private StatSelectorBase selector; //Selector component
    private InfoMessageEventhandler infoMessageHandler; //Handler for all information messages from components
    private ErrorMessageEventhandler errorMessageHandler; //Handler for all error messages from components
    private NDREventHandler newDataReadyHandler; //Handler for new data ready event from selector
    private eVisibleStatViews visibleViews = eVisibleStatViews.Table | eVisibleStatViews.Graph; //Keeps info about visible StatViews
    private eStatSelectorType selectorType; //Type of selector used on the form
    private int queryNumber = 0; //Ordinal number of query window since application start
    #endregion //Private members

    #region Public properties
    /// <summary>
    /// Controls the visibilty of tab pages with different viewers
    /// </summary>
    public eVisibleStatViews VisibleViews
    {
      get { return visibleViews; }
      set { SetVisibleViewers(value); }
    }
    #endregion //Public properties

    #region Construction
    public StatsQuery()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Constructor, creates and initializes DRSelector and DRViews
    /// </summary>
    /// <param name="selType">Type of selector to display</param>
    /// <param name="dbConnection">Connection to database to use</param>
    /// <param name="dddHelper">DDD Helper object for DDD Specific actions</param>
    /// <param name="queryNumber">Ordinal number of query window since application start</param>
    /// <param name="recordFile">Name of file with diagnostic records (for FileSelector only)</param>
    public StatsQuery(eStatSelectorType selType, SqlConnection dbConnection, DDDHelper dddHelper, int queryNumber, String statFile)
    {
      //Initialize components
      InitializeComponent();
      //Initialize event handlers
      infoMessageHandler = this.InfoMessageEvent;
      errorMessageHandler = this.ErrorMessageEvent;
      newDataReadyHandler = this.NewDataReadyEvent;
      //Store type of selector used
      selectorType = selType;
      //Create selector component
      //And set correct icon
      switch (selType)
      {
        case eStatSelectorType.DB:
          selector = new StatSelectorDB();
          this.Icon = Properties.Resources.Statistics;
          this.Text = Properties.Resources.strStatRecParamsTitle + " " + queryNumber.ToString();
          break;
        case eStatSelectorType.File:
          selector = new StatSelectorFile();
          ((StatSelectorFile)selector).FileName = statFile;
          this.Icon = Properties.Resources.Statistics;
          this.Text = Properties.Resources.strStatFileTitle + " " + queryNumber.ToString();
          break;
        default:
          throw new Exception("Unkonwn selector type");
      }
      this.queryNumber = queryNumber;
      //Place selector on form
      this.tabSelector.Controls.Add(selector);
      selector.Dock = DockStyle.Fill;
      selector.Location = new Point(0, 0);
      selector.Name = "statSelector";
      selector.TabIndex = 0;
      selector.CommandTimeout = (int)Properties.Settings.Default.dbCommandTimeoutSec;
      //Set connection object selector
      selector.DBConnection = dbConnection;
      //Set helper objects
      selector.DDDHelper = dddHelper;
      statViewTable.DDDHelper = dddHelper;
      statViewGraph.DDDHelper = dddHelper;
      //Connect viewers with selector
      selector.NewDataReady += statViewTable.NewDataReadyHandler;
      selector.NewDataReady += statViewGraph.NewDataReadyHandler;
      //Connect viewers with helper object
      dddHelper.NewLanguageSelected += statViewTable.NewLanguageHandler;
      dddHelper.NewLanguageSelected += statViewGraph.NewLanguageHandler;
      //Connect selector to Info and Error message handlers
      selector.InfoMessage += this.infoMessageHandler;
      selector.ErrorMessage += this.errorMessageHandler;
      //Set visibility of system messages
      systemMessagesToolStripMenuItem.Checked = Properties.Settings.Default.showSystemMessages;
      tsbShowMessages.Checked = Properties.Settings.Default.showSystemMessages;
      splContDRQuery.Panel2Collapsed = !Properties.Settings.Default.showSystemMessages;
    }
    #endregion //Construction

    #region Event handlers
    /// <summary>
    /// Show / hide log text box
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void tsbShowMessages_Click(object sender, EventArgs e)
    {
      if (sender == tsbShowMessages)
        systemMessagesToolStripMenuItem.Checked = tsbShowMessages.Checked;
      else
        tsbShowMessages.Checked = systemMessagesToolStripMenuItem.Checked;
      splContDRQuery.Panel2Collapsed = !tsbShowMessages.Checked;
    }

    /// <summary>
    /// Show / Hide table view
    /// </summary>
    private void tsbShowTableView_Click(object sender, EventArgs e)
    {
      if (sender == tsbShowTableView)
        tableViewToolStripMenuItem.Checked = tsbShowTableView.Checked;
      else
        tsbShowTableView.Checked = tableViewToolStripMenuItem.Checked;
      statViewTable.DisplayData = tsbShowTableView.Checked;
      splitContainerView.Panel1Collapsed = !tsbShowTableView.Checked;
    }
    
    /// <summary>
    /// Show / Hide graph view
    /// </summary>
    private void tsbShowGraphView_Click(object sender, EventArgs e)
    {
      if (sender == tsbShowGraphView)
        graphViewToolStripMenuItem.Checked = tsbShowGraphView.Checked;
      else
        tsbShowGraphView.Checked = graphViewToolStripMenuItem.Checked;
      statViewGraph.DisplayData = tsbShowGraphView.Checked;
      splitContainerView.Panel2Collapsed = !tsbShowGraphView.Checked;
    }

    /// <summary>
    /// Handles all error messages generated by components on the form
    /// </summary>
    /// <param name="sender">Sending component</param>
    /// <param name="error">Error in the form of exception</param>
    public void ErrorMessageEvent(object sender, Exception error)
    {
      Exception ex;
      //Log exception to listbox
      LogMsg(error.Message, eMsgSeverity.Error, 0);
      //Log all inner exceptions
      ex = error;
      while ((ex = ex.InnerException) != null)
        LogMsg(ex.Message, eMsgSeverity.Error, 1);
    }

    /// <summary>
    /// Handles all info messages generated by components on the form
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="message"></param>
    public void InfoMessageEvent(object sender, string message)
    {
      //Log message
      LogMsg(message, eMsgSeverity.Info, 0);
    }

    /// <summary>
    /// Method handles NewDataReady event raised by SelectorComponent.
    /// </summary>
    /// <param name="sender">selector that raised the event</param>
    private void NewDataReadyEvent(StatSelectorBase sender, NDREventArgs args)
    {
      LogMsg(Properties.Resources.strReady, eMsgSeverity.Info, 0);
    }

    /// <summary>
    /// Saves selected data to file
    /// </summary>
    private void saveSelectedDataToolStripMenuItem_Click(object sender, EventArgs e)
    {
      //Prepare save file dialog
      dlgSaveFile.Reset();
      dlgSaveFile.DefaultExt = Properties.Resources.StatFileExtension;
      dlgSaveFile.Filter = Properties.Resources.StatFilesFilter;
      //Show file save dialog
      if (dlgSaveFile.ShowDialog() == DialogResult.OK)
        selector.SaveStatisticsToFile(dlgSaveFile.FileName);
    }

    private void StatsQuery_FormClosed(object sender, FormClosedEventArgs e)
    {
      this.Dispose();
    }

    /// <summary>
    /// Executes query for diagnostic records by calling selector of the form
    /// </summary>
    private void tsbSelect_Click(object sender, EventArgs e)
    {
      //Load records using selector
      selector.LoadStatisticsAsync();
    }

    /// <summary>
    /// Saves complete settings of current diagnostic query into binary file
    /// </summary>
    private void saveQueryToolStripMenuItem_Click(object sender, EventArgs e)
    {
      try
      {
        //Prepare save file dialog
        dlgSaveFile.Reset();
        dlgSaveFile.DefaultExt = Properties.Resources.StatQFileExtension;
        dlgSaveFile.Filter = Properties.Resources.StatQFilesFilter;
        //Show file save dialog
        if (dlgSaveFile.ShowDialog() == DialogResult.OK)
        {
          //Open file for writing
          FileStream file = new FileStream(dlgSaveFile.FileName, FileMode.Create, FileAccess.Write);
          //Create binary formatter for object serialization
          BinaryFormatter formatter = new BinaryFormatter();
          //Save selector type
          formatter.Serialize(file, selectorType);
          //Save selector settings
          selector.SaveSettingsToFile(file, formatter);
          //Close file
          file.Close();
          file.Dispose();
        }
      }
      catch (Exception ex)
      {
        ErrorMessageEvent(this, new Exception("Failed to save query", ex));        
      }
    }
    
    /// <summary>
    /// Show/hide sums in table view
    /// </summary>
    private void tsbShowSums_Click(object sender, EventArgs e)
    {
      if (sender == tsbShowSums)
        showSumToolStripMenuItem.Checked = tsbShowSums.Checked;
      else
        tsbShowSums.Checked = showSumToolStripMenuItem.Checked;
      statViewTable.ShowSums = tsbShowSums.Checked;
    }

    /// <summary>
    /// Split view horizontally / vertically
    /// </summary>
    private void tsbRotateViews_Click(object sender, EventArgs e)
    {
      if (splitContainerView.Orientation == Orientation.Horizontal)
      {
        splitContainerView.Orientation = Orientation.Vertical;
        splitContainerView.SplitterDistance = splitContainerView.Width / 2;
        tsbRotateViews.Image = Properties.Resources.HorizLayout;
      }
      else
      {
        splitContainerView.Orientation = Orientation.Horizontal;
        splitContainerView.SplitterDistance = splitContainerView.Height / 2;
        tsbRotateViews.Image = Properties.Resources.VertLayout;
      }
    }

    /// <summary>
    /// Export selected statistics to CSV vile
    /// </summary>
    private void tsbExportStatistics_Click(object sender, EventArgs e)
    {
      try
      {
        //Prepare save file dialog
        dlgSaveFile.Reset();
        dlgSaveFile.DefaultExt = Properties.Resources.CSVFileExtension;
        dlgSaveFile.Filter = Properties.Resources.CSVFilesFilter;
        //Show file save dialog
        if (dlgSaveFile.ShowDialog() == DialogResult.OK)
        {
          //Open file for writing
          using (StreamWriter streamWriter = new StreamWriter(dlgSaveFile.FileName, false, Encoding.ASCII))
          {
            statViewTable.ExportToCSVFile(streamWriter);
          }
        }
      }
      catch (Exception ex)
      {
        ErrorMessageEvent(this, new Exception("Failed to export statistics to CSV file", ex));
      }
    }
    #endregion //Event handlers

    #region Helper methods
    /// <summary>
    /// Sets view visibility according to passed flags
    /// </summary>
    /// <param name="flags">Flags specifying the visibility</param>
    private void SetVisibleViewers(eVisibleStatViews flags)
    {
      //Display/hide Table view
      if ((visibleViews & eVisibleStatViews.Table) != (flags & eVisibleStatViews.Table))
      { //Change visibility of Table view
        if ((flags & eVisibleStatViews.Table) == 0)
          tbcComponents.TabPages.Remove(tabVwResults);
        else
          tbcComponents.TabPages.Add(tabVwResults);
      }
      statViewTable.DisplayData = (flags & eVisibleStatViews.Table) > 0;
      statViewGraph.DisplayData = (flags & eVisibleStatViews.Table) > 0;
      //Stores flags
      this.visibleViews = flags;
    }

    private void LogMsg(String message, eMsgSeverity severity, int indent)
    {
      //Create header for the string
      String header = DateTime.Now.ToString("HH:mm:ss.fff: ");
      header = header.PadRight(header.Length + indent * 2);
      //Display message
      lbMessages.Items.Add(header + message);
      lbMessages.TopIndex = lbMessages.Items.Count - 1;
      //Set status label
      if (indent == 0)
        statusLabel.Text = message;
      Update(); //Redraw the form
    }
    #endregion //Helper methods

    #region Public methods
    /// <summary>
    /// Loads previously saved query and filter settings from given file
    /// </summary>
    /// <param name="file">File stream to load from</param>
    /// <param name="formatter">Formatter to use for object deserialization</param>
    public void LoadQuerySetting(FileStream file, BinaryFormatter formatter)
    {
      //Load selector settings
      selector.LoadSettingsFromFile(file, formatter);
    }
    #endregion //Public methods
  }
}
