namespace DRView
{
  partial class SelectDDDSource
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.btSelect = new System.Windows.Forms.Button();
      this.btCancel = new System.Windows.Forms.Button();
      this.grBoxVerSource = new System.Windows.Forms.GroupBox();
      this.label2 = new System.Windows.Forms.Label();
      this.tbUnitAddress = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.tbConnectionString = new System.Windows.Forms.TextBox();
      this.rbTU = new System.Windows.Forms.RadioButton();
      this.rbCustomDBConnection = new System.Windows.Forms.RadioButton();
      this.rbPreferredDBCon = new System.Windows.Forms.RadioButton();
      this.CNetHelpProvider = new System.Windows.Forms.HelpProvider();
      this.persistWindow = new Mowog.PersistWindowComponent(this.components);
      this.grBoxVerSource.SuspendLayout();
      this.SuspendLayout();
      // 
      // btSelect
      // 
      this.btSelect.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.CNetHelpProvider.SetHelpKeyword(this.btSelect, "SelectDDDSource.htm#SelectDDDSource_btSelect");
      this.CNetHelpProvider.SetHelpNavigator(this.btSelect, System.Windows.Forms.HelpNavigator.Topic);
      this.btSelect.Location = new System.Drawing.Point(69, 248);
      this.btSelect.Name = "btSelect";
      this.CNetHelpProvider.SetShowHelp(this.btSelect, true);
      this.btSelect.Size = new System.Drawing.Size(75, 23);
      this.btSelect.TabIndex = 0;
      this.btSelect.Text = "OK";
      this.btSelect.UseVisualStyleBackColor = true;
      // 
      // btCancel
      // 
      this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.CNetHelpProvider.SetHelpKeyword(this.btCancel, "SelectDDDSource.htm#SelectDDDSource_btCancel");
      this.CNetHelpProvider.SetHelpNavigator(this.btCancel, System.Windows.Forms.HelpNavigator.Topic);
      this.btCancel.Location = new System.Drawing.Point(183, 248);
      this.btCancel.Name = "btCancel";
      this.CNetHelpProvider.SetShowHelp(this.btCancel, true);
      this.btCancel.Size = new System.Drawing.Size(75, 23);
      this.btCancel.TabIndex = 1;
      this.btCancel.Text = "Cancel";
      this.btCancel.UseVisualStyleBackColor = true;
      // 
      // grBoxVerSource
      // 
      this.grBoxVerSource.Controls.Add(this.label2);
      this.grBoxVerSource.Controls.Add(this.tbUnitAddress);
      this.grBoxVerSource.Controls.Add(this.label1);
      this.grBoxVerSource.Controls.Add(this.tbConnectionString);
      this.grBoxVerSource.Controls.Add(this.rbTU);
      this.grBoxVerSource.Controls.Add(this.rbCustomDBConnection);
      this.grBoxVerSource.Controls.Add(this.rbPreferredDBCon);
      this.CNetHelpProvider.SetHelpKeyword(this.grBoxVerSource, "SelectDDDSource.htm#SelectDDDSource_grBoxVerSource");
      this.CNetHelpProvider.SetHelpNavigator(this.grBoxVerSource, System.Windows.Forms.HelpNavigator.Topic);
      this.grBoxVerSource.Location = new System.Drawing.Point(12, 12);
      this.grBoxVerSource.Name = "grBoxVerSource";
      this.CNetHelpProvider.SetShowHelp(this.grBoxVerSource, true);
      this.grBoxVerSource.Size = new System.Drawing.Size(310, 216);
      this.grBoxVerSource.TabIndex = 2;
      this.grBoxVerSource.TabStop = false;
      this.grBoxVerSource.Text = "Source";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.CNetHelpProvider.SetHelpKeyword(this.label2, "SelectDDDSource.htm#SelectDDDSource_label2");
      this.CNetHelpProvider.SetHelpNavigator(this.label2, System.Windows.Forms.HelpNavigator.Topic);
      this.label2.Location = new System.Drawing.Point(40, 126);
      this.label2.Name = "label2";
      this.CNetHelpProvider.SetShowHelp(this.label2, true);
      this.label2.Size = new System.Drawing.Size(69, 13);
      this.label2.TabIndex = 6;
      this.label2.Text = "Unit address:";
      // 
      // tbUnitAddress
      // 
      this.CNetHelpProvider.SetHelpKeyword(this.tbUnitAddress, "SelectDDDSource.htm#SelectDDDSource_tbUnitAddress");
      this.CNetHelpProvider.SetHelpNavigator(this.tbUnitAddress, System.Windows.Forms.HelpNavigator.Topic);
      this.tbUnitAddress.Location = new System.Drawing.Point(40, 142);
      this.tbUnitAddress.Name = "tbUnitAddress";
      this.CNetHelpProvider.SetShowHelp(this.tbUnitAddress, true);
      this.tbUnitAddress.Size = new System.Drawing.Size(235, 20);
      this.tbUnitAddress.TabIndex = 5;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.CNetHelpProvider.SetHelpKeyword(this.label1, "SelectDDDSource.htm#SelectDDDSource_label1");
      this.CNetHelpProvider.SetHelpNavigator(this.label1, System.Windows.Forms.HelpNavigator.Topic);
      this.label1.Location = new System.Drawing.Point(40, 62);
      this.label1.Name = "label1";
      this.CNetHelpProvider.SetShowHelp(this.label1, true);
      this.label1.Size = new System.Drawing.Size(92, 13);
      this.label1.TabIndex = 4;
      this.label1.Text = "Connection string:";
      // 
      // tbConnectionString
      // 
      this.CNetHelpProvider.SetHelpKeyword(this.tbConnectionString, "SelectDDDSource.htm#SelectDDDSource_tbConnectionString");
      this.CNetHelpProvider.SetHelpNavigator(this.tbConnectionString, System.Windows.Forms.HelpNavigator.Topic);
      this.tbConnectionString.Location = new System.Drawing.Point(40, 77);
      this.tbConnectionString.Name = "tbConnectionString";
      this.CNetHelpProvider.SetShowHelp(this.tbConnectionString, true);
      this.tbConnectionString.Size = new System.Drawing.Size(235, 20);
      this.tbConnectionString.TabIndex = 3;
      // 
      // rbTU
      // 
      this.rbTU.AutoSize = true;
      this.CNetHelpProvider.SetHelpKeyword(this.rbTU, "SelectDDDSource.htm#SelectDDDSource_rbTU");
      this.CNetHelpProvider.SetHelpNavigator(this.rbTU, System.Windows.Forms.HelpNavigator.Topic);
      this.rbTU.Location = new System.Drawing.Point(6, 103);
      this.rbTU.Name = "rbTU";
      this.CNetHelpProvider.SetShowHelp(this.rbTU, true);
      this.rbTU.Size = new System.Drawing.Size(116, 17);
      this.rbTU.TabIndex = 2;
      this.rbTU.TabStop = true;
      this.rbTU.Text = "Telediagnostic Unit";
      this.rbTU.UseVisualStyleBackColor = true;
      // 
      // rbCustomDBConnection
      // 
      this.rbCustomDBConnection.AutoSize = true;
      this.CNetHelpProvider.SetHelpKeyword(this.rbCustomDBConnection, "SelectDDDSource.htm#SelectDDDSource_rbCustomDBConnection");
      this.CNetHelpProvider.SetHelpNavigator(this.rbCustomDBConnection, System.Windows.Forms.HelpNavigator.Topic);
      this.rbCustomDBConnection.Location = new System.Drawing.Point(6, 42);
      this.rbCustomDBConnection.Name = "rbCustomDBConnection";
      this.CNetHelpProvider.SetShowHelp(this.rbCustomDBConnection, true);
      this.rbCustomDBConnection.Size = new System.Drawing.Size(134, 17);
      this.rbCustomDBConnection.TabIndex = 1;
      this.rbCustomDBConnection.TabStop = true;
      this.rbCustomDBConnection.Text = "Custom DB connection";
      this.rbCustomDBConnection.UseVisualStyleBackColor = true;
      // 
      // rbPreferredDBCon
      // 
      this.rbPreferredDBCon.AutoSize = true;
      this.CNetHelpProvider.SetHelpKeyword(this.rbPreferredDBCon, "SelectDDDSource.htm#SelectDDDSource_rbPreferredDBCon");
      this.CNetHelpProvider.SetHelpNavigator(this.rbPreferredDBCon, System.Windows.Forms.HelpNavigator.Topic);
      this.rbPreferredDBCon.Location = new System.Drawing.Point(6, 19);
      this.rbPreferredDBCon.Name = "rbPreferredDBCon";
      this.CNetHelpProvider.SetShowHelp(this.rbPreferredDBCon, true);
      this.rbPreferredDBCon.Size = new System.Drawing.Size(142, 17);
      this.rbPreferredDBCon.TabIndex = 0;
      this.rbPreferredDBCon.TabStop = true;
      this.rbPreferredDBCon.Text = "Preferred DB connection";
      this.rbPreferredDBCon.UseVisualStyleBackColor = true;
      // 
      // CNetHelpProvider
      // 
      this.CNetHelpProvider.HelpNamespace = "DRView.chm";
      // 
      // persistWindow
      // 
      this.persistWindow.Form = this;
      this.persistWindow.UseLocAppDataDir = true;
      this.persistWindow.XMLFilePath = "DRViewWindowState.xml";
      // 
      // SelectDDDSource
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(336, 279);
      this.Controls.Add(this.grBoxVerSource);
      this.Controls.Add(this.btCancel);
      this.Controls.Add(this.btSelect);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.CNetHelpProvider.SetHelpKeyword(this, "SelectDDDSource.htm");
      this.CNetHelpProvider.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "SelectDDDSource";
      this.CNetHelpProvider.SetShowHelp(this, true);
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "DDD version needed";
      this.grBoxVerSource.ResumeLayout(false);
      this.grBoxVerSource.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btSelect;
    private System.Windows.Forms.Button btCancel;
    private System.Windows.Forms.GroupBox grBoxVerSource;
    private System.Windows.Forms.RadioButton rbPreferredDBCon;
    private System.Windows.Forms.RadioButton rbCustomDBConnection;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox tbUnitAddress;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox tbConnectionString;
    private System.Windows.Forms.RadioButton rbTU;
    private System.Windows.Forms.HelpProvider CNetHelpProvider;
    private Mowog.PersistWindowComponent persistWindow;
  }
}