using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Reflection;
using System.Data.SqlClient;
using System.Data;

namespace DRView
{
  partial class AboutBox : Form
  {
    public AboutBox(SqlConnection dbConnection)
    {
      InitializeComponent();
      
      //Set labels
      this.Text = String.Format("About {0}", AssemblyTitle);
      this.gbProduct.Text = AssemblyProduct;
      this.labelVersion.Text = String.Format("Version {0}", AssemblyVersion);
      this.labelCopyright.Text = AssemblyCopyright;
      this.labelCompanyName.Text = AssemblyCompany;
      this.textBoxDescription.Text = AssemblyDescription;
      ListReferencedAssemblies();
      GetDBVersion(dbConnection);
    }

    #region Assembly Attribute Accessors

    public string AssemblyTitle
    {
      get
      {
        // Get all Title attributes on this assembly
        object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
        // If there is at least one Title attribute
        if (attributes.Length > 0)
        {
          // Select the first one
          AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
          // If it is not an empty string, return it
          if (titleAttribute.Title != "")
            return titleAttribute.Title;
        }
        // If there was no Title attribute, or if the Title attribute was the empty string, return the .exe name
        return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
      }
    }

    public string AssemblyVersion
    {
      get
      {
        return Assembly.GetExecutingAssembly().GetName().Version.ToString();
      }
    }

    public string AssemblyDescription
    {
      get
      {
        // Get all Description attributes on this assembly
        object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
        // If there aren't any Description attributes, return an empty string
        if (attributes.Length == 0)
          return "";
        // If there is a Description attribute, return its value
        return ((AssemblyDescriptionAttribute)attributes[0]).Description;
      }
    }

    public string AssemblyProduct
    {
      get
      {
        // Get all Product attributes on this assembly
        object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
        // If there aren't any Product attributes, return an empty string
        if (attributes.Length == 0)
          return "";
        // If there is a Product attribute, return its value
        return ((AssemblyProductAttribute)attributes[0]).Product;
      }
    }

    public string AssemblyCopyright
    {
      get
      {
        // Get all Copyright attributes on this assembly
        object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
        // If there aren't any Copyright attributes, return an empty string
        if (attributes.Length == 0)
          return "";
        // If there is a Copyright attribute, return its value
        return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
      }
    }

    public string AssemblyCompany
    {
      get
      {
        // Get all Company attributes on this assembly
        object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
        // If there aren't any Company attributes, return an empty string
        if (attributes.Length == 0)
          return "";
        // If there is a Company attribute, return its value
        return ((AssemblyCompanyAttribute)attributes[0]).Company;
      }
    }

    /// <summary>
    /// List all referenced assemblies
    /// </summary>
    public void ListReferencedAssemblies()
    {
      Assembly execAssembly = Assembly.GetExecutingAssembly();
      AssemblyName[] assemblies = execAssembly.GetReferencedAssemblies();
      lvAssemblies.Items.Clear();
      foreach (AssemblyName assembly in assemblies)
      {
        ListViewItem item = new ListViewItem(assembly.Name);
        item.SubItems.Add(assembly.Version.ToString());
        lvAssemblies.Items.Add(item);
      }
      lvAssemblies.Refresh();
    }

    /// <summary>
    /// Display version of connected database
    /// </summary>
    public void GetDBVersion(SqlConnection dbConnection)
    {
      if (dbConnection == null)
        return;

      try
      {
        //Open DB Connection
        if (dbConnection.State != ConnectionState.Open)
        {
          dbConnection.Close();
          dbConnection.Open();
        }
        //Create command and preset parameters
        SqlCommand proc = new SqlCommand("GetDBVersion", dbConnection);
        proc.CommandType = System.Data.CommandType.StoredProcedure;
        proc.Parameters.Add("@DBVersion", SqlDbType.Int).Direction = ParameterDirection.Output;
        proc.Parameters.Add("@UpgradeDate", SqlDbType.DateTime).Direction = ParameterDirection.Output;
        proc.Parameters.Add("@MinReqAppVersion", SqlDbType.Int).Direction = ParameterDirection.Output;
        proc.Parameters.Add("@Comment", SqlDbType.NVarChar, 500).Direction = ParameterDirection.Output;
        //Execute command
        proc.ExecuteNonQuery();
        //Display results
        int ver = (int)proc.Parameters["@DBVersion"].Value;
        lbDBVersion.Text = (ver / 1000000).ToString() + "." + ((ver % 1000000) / 1000).ToString() + "." + (ver % 1000).ToString();
        lbUprgDate.Text = proc.Parameters["@UpgradeDate"].Value.ToString();
        ver = (int)proc.Parameters["@MinReqAppVersion"].Value;
        lbMinRqAppVer.Text = (ver / 1000000).ToString() + "." + ((ver % 1000000) / 1000).ToString() + "." + (ver % 1000).ToString();
        lbDBComment.Text = proc.Parameters["@Comment"].Value as String;
      }
      catch (Exception ex)
      {
        MessageBox.Show("Failed to obtain DB version. " + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }
    #endregion
  }
}
