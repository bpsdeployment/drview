using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace DRView
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    /// <param name="args">Command line arguments passed to the application</param>
    [STAThread]
    static void Main(string[] args)
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);

      //Create and register delegate which accepts all SSL certificates from all servers
      ServicePointManager.ServerCertificateValidationCallback
          += delegate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
      {
        return true;
      };

      Application.Run(new MainForm(args));
    }
  }
}