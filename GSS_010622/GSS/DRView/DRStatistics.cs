using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using DSRecordStats;
using DDDObjects;
using FleetHierarchies;
using DRQueries;

namespace DRView
{
  /// <summary>
  /// Displays statistics of diagnostic records for selected 
  /// TU Instance and time range
  /// </summary>
  public partial class DRStatistics : Form
  {
    private DDDHelper dddHelper = null;
    private SqlConnection dbConnection = null;
    private DataTable dtTUInstances = new DataTable();

    /// <summary>
    /// DDDHelper object
    /// </summary>
    public DDDHelper DDDHelper
    {
      get { return dddHelper; }
      set { dddHelper = value; }
    }

    /// <summary>
    /// Connection to DB
    /// </summary>
    public SqlConnection DBConnection
    {
      get { return dbConnection; }
      set 
      { 
        dbConnection = value;
        cmdGetStats.Connection = dbConnection;
      }
    }

    public DRStatistics(DDDHelper dddHelper, SqlConnection dbConnection)
    {
      InitializeComponent();
      DDDHelper = dddHelper;
      DBConnection = dbConnection;
    }

    private void DRStatistics_Load(object sender, EventArgs e)
    {
      try
      {
        if (dbConnection == null)
          throw new Exception("No DB Connection specified");
        if (dbConnection.State != ConnectionState.Open)
        {
          dbConnection.Close();
          dbConnection.Open();
        }
        RefreshHierarchyAndInstances();
      }
      catch (Exception ex)
      {
        statusLabel.Text = "Failed to select list of Telediagnostic Units: " + ex.Message;
      }
    }

    /// <summary>
    /// Select record statistics from database to data set
    /// </summary>
    private void btSelect_Click(object sender, EventArgs e)
    {
      try
      {
        //Set status labels
        statusLabel.Text = "Selecting record statistics...";
        slRowCount.Text = "";
        slTotalRecCount.Text = "";
        int totalRecCount = 0;
        //Repaint the form
        this.Refresh();
        //Set fromatting for time columns
        colFirstTime.DefaultCellStyle.Format = dddHelper.TimeFormat;
        colLastTime.DefaultCellStyle.Format = dddHelper.TimeFormat;
        //Build string list of selected hierarchy item IDs
        List<int> selIDs = hierarchyTUInstances.FleetHierarchy.GetSelectedIDs();
        String strSelIDs = String.Empty;
        foreach (int itemID in selIDs)
          strSelIDs += itemID.ToString() + " ";
        //Get query text from DB
        String queryText;
        queryText = DRQueryHelper.GetQuery_WebStatistics(
          hierarchyTUInstances.TUInstancesList.GetSelectedInstanceIDs(),
          strSelIDs,
          timePicker.FromSelected ? (DateTime?)dddHelper.UserTimeToUTC(timePicker.TimeFrom) : null,
          timePicker.ToSelected ? (DateTime?)dddHelper.UserTimeToUTC(timePicker.TimeTo) : null,
          null, null, null, null, null, false, null, null, null);
        //Check connection state
        if (dbConnection.State != ConnectionState.Open)
        {
          dbConnection.Close();
          dbConnection.Open();
        }
        //Clear data set
        dsRecordStats.Clear();
        //Load data from DB 
        using (SqlCommand dbCmd = new SqlCommand(queryText, dbConnection))
        { //Create database command
          dbCmd.CommandTimeout = (int)Properties.Settings.Default.dbCommandTimeoutSec;
          dbCmd.CommandType = CommandType.Text;
          using (SqlDataReader reader = dbCmd.ExecuteReader())
          { //Execute the command and obtain data reader
            dsRecordStats.RecordStats.Load(reader); //Load data from reader into the table
          }
        }
        //Fill in missing columns for each row
        try
        {
          totalRecCount = 0;
          foreach (DSRecordStats.DSRecordStats.RecordStatsRow row in dsRecordStats.RecordStats)
          { //iterate over all rows in data table
            FillStatisticsRowAttributes(row);
            //Compute total record count
            if (!row.IsRecCountNull())
              totalRecCount += row.RecCount;
          }
        }
        catch (Exception ex)
        {
          throw new Exception("DDD Helper objects failed", ex);
        }

        //Set status label
        statusLabel.Text = "Record statistics selected";
        slRowCount.Text = dsRecordStats.RecordStats.Count.ToString() + " rows";
        slTotalRecCount.Text = totalRecCount.ToString() + " records";
        //Hide hierarchy tree to display the grid
        HideFleetHierarchy();
      }
      catch (Exception ex)
      {
        statusLabel.Text = "Failed to select record statistics: " + ex.Message;
      }
    }

    private void exportStatisticsToolStripMenuItem_Click(object sender, EventArgs e)
    {
      //open file save dialog
      if (dlgSaveFile.ShowDialog() == DialogResult.OK)
      {
        statusLabel.Text = "Exporting record statistics to file " + dlgSaveFile.FileName;
        this.Update();
        Stream outStream = null;
        StreamWriter writer = null;
        try
        {
          String line;
          //open output file
          outStream = dlgSaveFile.OpenFile();
          writer = new StreamWriter(outStream);
          //Create and write header
          line =
            ResDRStatistics.strTrain + Properties.Settings.Default.defaultCSVSeparator +
            ResDRStatistics.strVehicle + Properties.Settings.Default.defaultCSVSeparator +
            ResDRStatistics.strVehicleNumber + Properties.Settings.Default.defaultCSVSeparator +
            ResDRStatistics.strSource + Properties.Settings.Default.defaultCSVSeparator +
            ResDRStatistics.strFaultCode + Properties.Settings.Default.defaultCSVSeparator +
            ResDRStatistics.strRecordTitle + Properties.Settings.Default.defaultCSVSeparator +
            ResDRStatistics.strCount + Properties.Settings.Default.defaultCSVSeparator +
            ResDRStatistics.strTimeFirst + Properties.Settings.Default.defaultCSVSeparator +
            ResDRStatistics.strTimeLast + Properties.Settings.Default.defaultCSVSeparator +
            ResDRStatistics.strSeverity + Properties.Settings.Default.defaultCSVSeparator +
            ResDRStatistics.strSubSeverity + Properties.Settings.Default.defaultCSVSeparator;
          writer.WriteLine(line);
          //write lines from data set
          foreach (DSRecordStats.DSRecordStats.RecordStatsRow row in dsRecordStats.RecordStats)
          {
            //Create line
            line = "";
            if (!row.IsTrainNameNull())
              line += row.TrainName;
            line += Properties.Settings.Default.defaultCSVSeparator;
            if (!row.IsVehicleNameNull())
              line += row.VehicleName;
            line += Properties.Settings.Default.defaultCSVSeparator;
            if (!row.IsVehicleNumberNull())
              line += row.VehicleNumber.ToString();
            line += Properties.Settings.Default.defaultCSVSeparator;
            if (!row.IsSourceNull())
              line += row.Source;
            line += Properties.Settings.Default.defaultCSVSeparator;
            if (!row.IsFaultCodeNull())
              line += row.FaultCode.ToString();
            line += Properties.Settings.Default.defaultCSVSeparator;
            if (!row.IsRecTitleNull())
              line += row.RecTitle;
            line += Properties.Settings.Default.defaultCSVSeparator;
            if (!row.IsRecCountNull())
              line += row.RecCount.ToString();
            line += Properties.Settings.Default.defaultCSVSeparator;
            if (!row.IsFirstTimeNull())
              line += row.FirstTime.ToString("yyyy-MM-dd HH:mm:ss.fff");
            line += Properties.Settings.Default.defaultCSVSeparator;
            if (!row.IsLastTimeNull())
              line += row.LastTime.ToString("yyyy-MM-dd HH:mm:ss.fff");
            line += Properties.Settings.Default.defaultCSVSeparator;
            if (!row.IsFaultSeverityNull())
              line += row.FaultSeverity.ToString();
            line += Properties.Settings.Default.defaultCSVSeparator;
            if (!row.IsFaultSubSeverityNull())
              line += row.FaultSubSeverity.ToString();
            //Write line
            writer.WriteLine(line);
          }
          //close output file
          writer.Close();
          writer.Dispose();
          outStream.Dispose();
          statusLabel.Text = "Record statistics exported";
        }
        catch (Exception ex)
        {
          statusLabel.Text = "Failed to export statistics: " + ex.Message;
        }
        finally
        {
          if (writer != null)
          {
            writer.Close();
            writer.Dispose();
          }
          if (outStream != null)
          {
            outStream.Dispose();
          }
        }
      }
    }

    #region Helper methods
    /// <summary>
    /// Selects list of all TU Instances and fllet hierarchy from DB
    /// Fills in the trees in components
    /// </summary>
    protected void RefreshHierarchyAndInstances()
    {
      try
      {
        //Set proper connection string to DDD Helper
        if (dbConnection != null)
          dddHelper.ConnectionString = dbConnection.ConnectionString;

        hierarchyTUInstances.FleetHierarchy.ShowCurrentHierarchy(dddHelper.HierarchyHelper, null);
        hierarchyTUInstances.TUInstancesList.ShowTUInstances(dddHelper);
      }
      catch (Exception)
      {
        statusLabel.Text = "Failed to load fleet hierarchy or TU Instances list";
      }
    }

    /// <summary>
    /// Method fills in additional attributes to row in record Statistics table.
    /// It uses data already loaded from DB to obtain configuration objects and fill in the additional attributes
    /// </summary>
    /// <param name="row">Row to be filled (it has to contain data loaded from DB)</param>
    public void FillStatisticsRowAttributes(DSRecordStats.DSRecordStats.RecordStatsRow row)
    {
      DDDVersion ver;
      DDDRecord rec;
      TUInstance inst;

      //Modify times to correct time zone
      if (!row.IsFirstTimeNull())
        row.FirstTime = dddHelper.UTCToUserTime(row.FirstTime);
      if (!row.IsLastTimeNull())
        row.LastTime = dddHelper.UTCToUserTime(row.LastTime);

      //Obtain helper objects
      ver = dddHelper.GetVersion(row.DDDVersionID);
      inst = dddHelper.GetTUInstance(row.TUInstanceID);
      rec = ver.GetRecord(row.RecordDefID);

      //Fill in missing columns using helper objects
      row.TUInstanceName = inst.TUName;
      row.UserVersion = ver.UserVersion;
      row.RecTitle = rec.Title;
      if (rec.GetType() == typeof(DDDEventRecord))
      {
        row.RecordType = "event";
        row.FaultCode = ((DDDEventRecord)rec).FaultCode;
        row.FaultSeverity = ((DDDEventRecord)rec).Severity.ToString();
        row.FaultSubSeverity = ((DDDEventRecord)rec).SubSeverity;
      }
      else if (rec.GetType() == typeof(DDDTraceRecord))
      {
        row.RecordType = "trace";
        row.TrcSnpCode = ((DDDTraceRecord)rec).Code;
      }
      else if (rec.GetType() == typeof(DDDSnapRecord))
      {
        row.RecordType = "snap";
        row.TrcSnpCode = ((DDDSnapRecord)rec).Code;
      }

      //Fill in hierarchy names
      if (!row.IsHierarchyItemIDNull() && row.HierarchyItemID != -1)
      { //Valid hierarchy item ID - obtain names of train and vehicle
        FleetHierarchy hier = dddHelper.HierarchyHelper.ObtainHierarchy(row.LastTime);
        FleetHierarchyItem hierItem = hier.GetHierarchyItem(row.HierarchyItemID);
        FHVehicle vehicle = hierItem.GetVehicle();
        FHTrain train = hierItem.GetTrain();
        if (vehicle != null)
          row.VehicleName = vehicle.Name;
        if (train != null)
          row.TrainName = train.Name;
      }
    }

    /// <summary>
    /// Toggle the state of fleet hierarchy tree
    /// </summary>
    private void btToggleHierarchy_Click(object sender, EventArgs e)
    { //Toggle the state of fleet hierarchy tree
      if (panHierarchy.Visible)
      { //Tree is visible - hide it
        HideFleetHierarchy();
      }
      else
      { //Display hierarchy tree
        panHierarchy.Visible = true;
        btToggleHierarchy.Text = "Hide <<";
      }
    }

    /// <summary>
    /// Hide tree with fleet hierarchy
    /// </summary>
    private void HideFleetHierarchy()
    {
      panHierarchy.Visible = false;
      btToggleHierarchy.Text = "Choose >>";
    }
    #endregion
  }
}