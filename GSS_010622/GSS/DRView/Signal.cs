﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

public class Signal
{
	//Signal in the FamList
	public string idx;
	public string sigMTs;
	public string csvDF;    //Optional
	public string csvCI;    //Optional
	public string csvAS;    //Optional
	public string csvO;     //Optional
	public string binO;     //Optional

	//SignalAnag in the SignalList
	public string sysId;
	public string sigName;
	public string sigSD;
	public string sigLD;    //Optional
	public string sigUM;
	public string sigDT;
	public string sigAS;    //Optional
	public string sigM;
	public string sigB;

	public Signal(XmlNode Signal, XmlNode SignalAnag)
	{
		//Attribute in the Signal
		idx = Signal.Attributes["idx"]?.InnerText;
		sigMTs = Signal.Attributes["sigMTs"]?.InnerText;
		if (csvDF != null)
        {
			csvDF = Signal.Attributes["csvDF"]?.InnerText;
		}
		if (csvCI != null)
		{
			csvCI = Signal.Attributes["csvCI"]?.InnerText;
		}
		if (csvAS != null)
		{
			csvAS = Signal.Attributes["csvAS"]?.InnerText;
		}
		if (csvO != null)
		{
			csvO = Signal.Attributes["csvO"]?.InnerText;
		}
		if (binO != null)
		{
			binO = Signal.Attributes["binO"]?.InnerText;
		}

		//Attribute in the SignalAnag
		sysId = SignalAnag.Attributes["sysId"]?.InnerText;
		sigName = SignalAnag.Attributes["sigName"]?.InnerText;
		sigSD = SignalAnag.Attributes["sigSD"]?.InnerText;
		if (sigLD != null)
		{
			sigLD = SignalAnag.Attributes["sigLD"]?.InnerText;
		}
		sigUM = SignalAnag.Attributes["sigUM"]?.InnerText;
		sigDT = SignalAnag.Attributes["sigDT"]?.InnerText;
		if (sigAS != null)
		{
			sigAS = SignalAnag.Attributes["sigAS"]?.InnerText;
		}
		sigM = SignalAnag.Attributes["sigM"]?.InnerText;
		sigB = SignalAnag.Attributes["sigB"]?.InnerText;
	}

	public Signal(XmlNode Signal, XmlDocument BL)
	{
		//Attribute in the Signal
		idx = Signal.Attributes["idx"]?.InnerText;
		sigMTs = Signal.Attributes["sigMTs"]?.InnerText;
		csvDF = Signal.Attributes["csvDF"]?.InnerText;
		csvCI = Signal.Attributes["csvCI"]?.InnerText;
		csvAS = Signal.Attributes["csvAS"]?.InnerText;
		csvO = Signal.Attributes["csvO"]?.InnerText;
		binO = Signal.Attributes["binO"]?.InnerText;

		XmlNodeList SignalAnagList = BL.GetElementsByTagName("SignalAnag");
		foreach(XmlNode SignalAnagNode in SignalAnagList)
        {
			if(SignalAnagNode.Attributes["idx"]?.InnerText == idx)
            {
				//Attribute in the SignalAnag
				sysId = SignalAnagNode.Attributes["sysId"]?.InnerText;
				sigName = SignalAnagNode.Attributes["sigName"]?.InnerText;
				sigSD = SignalAnagNode.Attributes["sigSD"]?.InnerText;
				sigLD = SignalAnagNode.Attributes["sigLD"]?.InnerText;
				sigUM = SignalAnagNode.Attributes["sigUM"]?.InnerText;
				sigDT = SignalAnagNode.Attributes["sigDT"]?.InnerText;
				sigAS = SignalAnagNode.Attributes["sigAS"]?.InnerText;
				sigM = SignalAnagNode.Attributes["sigM"]?.InnerText;
				sigB = SignalAnagNode.Attributes["sigB"]?.InnerText;

				break;
			}
        }

	}
}