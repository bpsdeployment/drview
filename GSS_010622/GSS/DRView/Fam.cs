﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

public class Fam
{
	public string famId;
	public string famSD;
	public List<Signal> SignalList;
	public Fam(XmlNode node)
	{
		famId = node.Attributes["famId"]?.InnerText;
		famSD = node.Attributes["famSD"]?.InnerText;
	}

	public void SetSignalList(List<Signal> NewSignalList)
    {
		SignalList = NewSignalList;
	}

	public List<Signal> GelSignalList()
    {
		return SignalList;
    }
}

