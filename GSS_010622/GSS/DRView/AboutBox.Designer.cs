namespace DRView
{
  partial class AboutBox
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutBox));
      this.logoPictureBox = new System.Windows.Forms.PictureBox();
      this.okButton = new System.Windows.Forms.Button();
      this.labelCopyright = new System.Windows.Forms.Label();
      this.labelCompanyName = new System.Windows.Forms.Label();
      this.labelVersion = new System.Windows.Forms.Label();
      this.textBoxDescription = new System.Windows.Forms.TextBox();
      this.lvAssemblies = new System.Windows.Forms.ListView();
      this.name = new System.Windows.Forms.ColumnHeader();
      this.version = new System.Windows.Forms.ColumnHeader();
      this.lbDBVersion = new System.Windows.Forms.Label();
      this.lbUprgDate = new System.Windows.Forms.Label();
      this.lbMinRqAppVer = new System.Windows.Forms.Label();
      this.lbDBComment = new System.Windows.Forms.Label();
      this.gbProduct = new System.Windows.Forms.GroupBox();
      this.gbAssemblies = new System.Windows.Forms.GroupBox();
      this.gbDB = new System.Windows.Forms.GroupBox();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.label4 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.CNetHelpProvider = new System.Windows.Forms.HelpProvider();
      ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).BeginInit();
      this.gbProduct.SuspendLayout();
      this.gbAssemblies.SuspendLayout();
      this.gbDB.SuspendLayout();
      this.tableLayoutPanel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // logoPictureBox
      // 
      this.CNetHelpProvider.SetHelpKeyword(this.logoPictureBox, "AboutBox.htm#AboutBox_logoPictureBox");
      this.CNetHelpProvider.SetHelpNavigator(this.logoPictureBox, System.Windows.Forms.HelpNavigator.Topic);
      this.logoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("logoPictureBox.Image")));
      this.logoPictureBox.Location = new System.Drawing.Point(200, 19);
      this.logoPictureBox.Name = "logoPictureBox";
      this.CNetHelpProvider.SetShowHelp(this.logoPictureBox, true);
      this.logoPictureBox.Size = new System.Drawing.Size(186, 139);
      this.logoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.logoPictureBox.TabIndex = 25;
      this.logoPictureBox.TabStop = false;
      // 
      // okButton
      // 
      this.okButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.CNetHelpProvider.SetHelpKeyword(this.okButton, "AboutBox.htm#AboutBox_okButton");
      this.CNetHelpProvider.SetHelpNavigator(this.okButton, System.Windows.Forms.HelpNavigator.Topic);
      this.okButton.Location = new System.Drawing.Point(171, 417);
      this.okButton.Name = "okButton";
      this.CNetHelpProvider.SetShowHelp(this.okButton, true);
      this.okButton.Size = new System.Drawing.Size(75, 23);
      this.okButton.TabIndex = 24;
      this.okButton.Text = "&OK";
      // 
      // labelCopyright
      // 
      this.labelCopyright.AutoSize = true;
      this.CNetHelpProvider.SetHelpKeyword(this.labelCopyright, "AboutBox.htm#AboutBox_labelCopyright");
      this.CNetHelpProvider.SetHelpNavigator(this.labelCopyright, System.Windows.Forms.HelpNavigator.Topic);
      this.labelCopyright.Location = new System.Drawing.Point(3, 19);
      this.labelCopyright.Name = "labelCopyright";
      this.CNetHelpProvider.SetShowHelp(this.labelCopyright, true);
      this.labelCopyright.Size = new System.Drawing.Size(73, 13);
      this.labelCopyright.TabIndex = 30;
      this.labelCopyright.Text = "labelCopyright";
      // 
      // labelCompanyName
      // 
      this.labelCompanyName.AutoSize = true;
      this.CNetHelpProvider.SetHelpKeyword(this.labelCompanyName, "AboutBox.htm#AboutBox_labelCompanyName");
      this.CNetHelpProvider.SetHelpNavigator(this.labelCompanyName, System.Windows.Forms.HelpNavigator.Topic);
      this.labelCompanyName.Location = new System.Drawing.Point(3, 41);
      this.labelCompanyName.Name = "labelCompanyName";
      this.CNetHelpProvider.SetShowHelp(this.labelCompanyName, true);
      this.labelCompanyName.Size = new System.Drawing.Size(101, 13);
      this.labelCompanyName.TabIndex = 31;
      this.labelCompanyName.Text = "labelCompanyName";
      // 
      // labelVersion
      // 
      this.labelVersion.AutoSize = true;
      this.CNetHelpProvider.SetHelpKeyword(this.labelVersion, "AboutBox.htm#AboutBox_labelVersion");
      this.CNetHelpProvider.SetHelpNavigator(this.labelVersion, System.Windows.Forms.HelpNavigator.Topic);
      this.labelVersion.Location = new System.Drawing.Point(3, 64);
      this.labelVersion.Name = "labelVersion";
      this.CNetHelpProvider.SetShowHelp(this.labelVersion, true);
      this.labelVersion.Size = new System.Drawing.Size(64, 13);
      this.labelVersion.TabIndex = 32;
      this.labelVersion.Text = "labelVersion";
      // 
      // textBoxDescription
      // 
      this.textBoxDescription.BackColor = System.Drawing.SystemColors.Control;
      this.CNetHelpProvider.SetHelpKeyword(this.textBoxDescription, "AboutBox.htm#AboutBox_textBoxDescription");
      this.CNetHelpProvider.SetHelpNavigator(this.textBoxDescription, System.Windows.Forms.HelpNavigator.Topic);
      this.textBoxDescription.Location = new System.Drawing.Point(6, 92);
      this.textBoxDescription.Multiline = true;
      this.textBoxDescription.Name = "textBoxDescription";
      this.CNetHelpProvider.SetShowHelp(this.textBoxDescription, true);
      this.textBoxDescription.Size = new System.Drawing.Size(188, 66);
      this.textBoxDescription.TabIndex = 34;
      // 
      // lvAssemblies
      // 
      this.lvAssemblies.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.name,
            this.version});
      this.lvAssemblies.FullRowSelect = true;
      this.lvAssemblies.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
      this.CNetHelpProvider.SetHelpKeyword(this.lvAssemblies, "AboutBox.htm#AboutBox_lvAssemblies");
      this.CNetHelpProvider.SetHelpNavigator(this.lvAssemblies, System.Windows.Forms.HelpNavigator.Topic);
      this.lvAssemblies.Location = new System.Drawing.Point(6, 19);
      this.lvAssemblies.MultiSelect = false;
      this.lvAssemblies.Name = "lvAssemblies";
      this.lvAssemblies.ShowGroups = false;
      this.CNetHelpProvider.SetShowHelp(this.lvAssemblies, true);
      this.lvAssemblies.Size = new System.Drawing.Size(380, 97);
      this.lvAssemblies.TabIndex = 35;
      this.lvAssemblies.UseCompatibleStateImageBehavior = false;
      this.lvAssemblies.View = System.Windows.Forms.View.Details;
      // 
      // name
      // 
      this.name.Text = "Name";
      this.name.Width = 186;
      // 
      // version
      // 
      this.version.Text = "Version";
      this.version.Width = 190;
      // 
      // lbDBVersion
      // 
      this.lbDBVersion.AutoSize = true;
      this.CNetHelpProvider.SetHelpKeyword(this.lbDBVersion, "AboutBox.htm#AboutBox_lbDBVersion");
      this.CNetHelpProvider.SetHelpNavigator(this.lbDBVersion, System.Windows.Forms.HelpNavigator.Topic);
      this.lbDBVersion.Location = new System.Drawing.Point(105, 0);
      this.lbDBVersion.Name = "lbDBVersion";
      this.CNetHelpProvider.SetShowHelp(this.lbDBVersion, true);
      this.lbDBVersion.Size = new System.Drawing.Size(0, 13);
      this.lbDBVersion.TabIndex = 37;
      // 
      // lbUprgDate
      // 
      this.lbUprgDate.AutoSize = true;
      this.CNetHelpProvider.SetHelpKeyword(this.lbUprgDate, "AboutBox.htm#AboutBox_lbUprgDate");
      this.CNetHelpProvider.SetHelpNavigator(this.lbUprgDate, System.Windows.Forms.HelpNavigator.Topic);
      this.lbUprgDate.Location = new System.Drawing.Point(105, 18);
      this.lbUprgDate.Name = "lbUprgDate";
      this.CNetHelpProvider.SetShowHelp(this.lbUprgDate, true);
      this.lbUprgDate.Size = new System.Drawing.Size(0, 13);
      this.lbUprgDate.TabIndex = 38;
      // 
      // lbMinRqAppVer
      // 
      this.lbMinRqAppVer.AutoSize = true;
      this.CNetHelpProvider.SetHelpKeyword(this.lbMinRqAppVer, "AboutBox.htm#AboutBox_lbMinRqAppVer");
      this.CNetHelpProvider.SetHelpNavigator(this.lbMinRqAppVer, System.Windows.Forms.HelpNavigator.Topic);
      this.lbMinRqAppVer.Location = new System.Drawing.Point(105, 54);
      this.lbMinRqAppVer.Name = "lbMinRqAppVer";
      this.CNetHelpProvider.SetShowHelp(this.lbMinRqAppVer, true);
      this.lbMinRqAppVer.Size = new System.Drawing.Size(0, 13);
      this.lbMinRqAppVer.TabIndex = 39;
      // 
      // lbDBComment
      // 
      this.lbDBComment.AutoSize = true;
      this.CNetHelpProvider.SetHelpKeyword(this.lbDBComment, "AboutBox.htm#AboutBox_lbDBComment");
      this.CNetHelpProvider.SetHelpNavigator(this.lbDBComment, System.Windows.Forms.HelpNavigator.Topic);
      this.lbDBComment.Location = new System.Drawing.Point(105, 36);
      this.lbDBComment.Name = "lbDBComment";
      this.CNetHelpProvider.SetShowHelp(this.lbDBComment, true);
      this.lbDBComment.Size = new System.Drawing.Size(0, 13);
      this.lbDBComment.TabIndex = 40;
      // 
      // gbProduct
      // 
      this.gbProduct.Controls.Add(this.textBoxDescription);
      this.gbProduct.Controls.Add(this.logoPictureBox);
      this.gbProduct.Controls.Add(this.labelCopyright);
      this.gbProduct.Controls.Add(this.labelCompanyName);
      this.gbProduct.Controls.Add(this.labelVersion);
      this.CNetHelpProvider.SetHelpKeyword(this.gbProduct, "AboutBox.htm#AboutBox_gbProduct");
      this.CNetHelpProvider.SetHelpNavigator(this.gbProduct, System.Windows.Forms.HelpNavigator.Topic);
      this.gbProduct.Location = new System.Drawing.Point(12, 3);
      this.gbProduct.Name = "gbProduct";
      this.CNetHelpProvider.SetShowHelp(this.gbProduct, true);
      this.gbProduct.Size = new System.Drawing.Size(393, 165);
      this.gbProduct.TabIndex = 41;
      this.gbProduct.TabStop = false;
      this.gbProduct.Text = "Product";
      // 
      // gbAssemblies
      // 
      this.gbAssemblies.Controls.Add(this.lvAssemblies);
      this.CNetHelpProvider.SetHelpKeyword(this.gbAssemblies, "AboutBox.htm#AboutBox_gbAssemblies");
      this.CNetHelpProvider.SetHelpNavigator(this.gbAssemblies, System.Windows.Forms.HelpNavigator.Topic);
      this.gbAssemblies.Location = new System.Drawing.Point(12, 179);
      this.gbAssemblies.Name = "gbAssemblies";
      this.CNetHelpProvider.SetShowHelp(this.gbAssemblies, true);
      this.gbAssemblies.Size = new System.Drawing.Size(393, 126);
      this.gbAssemblies.TabIndex = 42;
      this.gbAssemblies.TabStop = false;
      this.gbAssemblies.Text = "Additional assemblies";
      // 
      // gbDB
      // 
      this.gbDB.Controls.Add(this.tableLayoutPanel1);
      this.CNetHelpProvider.SetHelpKeyword(this.gbDB, "AboutBox.htm#AboutBox_gbDB");
      this.CNetHelpProvider.SetHelpNavigator(this.gbDB, System.Windows.Forms.HelpNavigator.Topic);
      this.gbDB.Location = new System.Drawing.Point(12, 311);
      this.gbDB.Name = "gbDB";
      this.CNetHelpProvider.SetShowHelp(this.gbDB, true);
      this.gbDB.Size = new System.Drawing.Size(393, 100);
      this.gbDB.TabIndex = 43;
      this.gbDB.TabStop = false;
      this.gbDB.Text = "Database";
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 2;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.84211F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.1579F));
      this.tableLayoutPanel1.Controls.Add(this.lbMinRqAppVer, 1, 3);
      this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
      this.tableLayoutPanel1.Controls.Add(this.lbDBVersion, 1, 0);
      this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.lbUprgDate, 1, 1);
      this.tableLayoutPanel1.Controls.Add(this.lbDBComment, 1, 2);
      this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
      this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
      this.CNetHelpProvider.SetHelpKeyword(this.tableLayoutPanel1, "AboutBox.htm#AboutBox_tableLayoutPanel1");
      this.CNetHelpProvider.SetHelpNavigator(this.tableLayoutPanel1, System.Windows.Forms.HelpNavigator.Topic);
      this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 19);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 4;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
      this.CNetHelpProvider.SetShowHelp(this.tableLayoutPanel1, true);
      this.tableLayoutPanel1.Size = new System.Drawing.Size(380, 75);
      this.tableLayoutPanel1.TabIndex = 0;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.CNetHelpProvider.SetHelpKeyword(this.label4, "AboutBox.htm#AboutBox_label4");
      this.CNetHelpProvider.SetHelpNavigator(this.label4, System.Windows.Forms.HelpNavigator.Topic);
      this.label4.Location = new System.Drawing.Point(3, 54);
      this.label4.Name = "label4";
      this.CNetHelpProvider.SetShowHelp(this.label4, true);
      this.label4.Size = new System.Drawing.Size(96, 13);
      this.label4.TabIndex = 44;
      this.label4.Text = "Minimum app. ver.:";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.CNetHelpProvider.SetHelpKeyword(this.label1, "AboutBox.htm#AboutBox_label1");
      this.CNetHelpProvider.SetHelpNavigator(this.label1, System.Windows.Forms.HelpNavigator.Topic);
      this.label1.Location = new System.Drawing.Point(3, 0);
      this.label1.Name = "label1";
      this.CNetHelpProvider.SetShowHelp(this.label1, true);
      this.label1.Size = new System.Drawing.Size(93, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Database version:";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.CNetHelpProvider.SetHelpKeyword(this.label2, "AboutBox.htm#AboutBox_label2");
      this.CNetHelpProvider.SetHelpNavigator(this.label2, System.Windows.Forms.HelpNavigator.Topic);
      this.label2.Location = new System.Drawing.Point(3, 18);
      this.label2.Name = "label2";
      this.CNetHelpProvider.SetShowHelp(this.label2, true);
      this.label2.Size = new System.Drawing.Size(75, 13);
      this.label2.TabIndex = 1;
      this.label2.Text = "Upgrade date:";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.CNetHelpProvider.SetHelpKeyword(this.label3, "AboutBox.htm#AboutBox_label3");
      this.CNetHelpProvider.SetHelpNavigator(this.label3, System.Windows.Forms.HelpNavigator.Topic);
      this.label3.Location = new System.Drawing.Point(3, 36);
      this.label3.Name = "label3";
      this.CNetHelpProvider.SetShowHelp(this.label3, true);
      this.label3.Size = new System.Drawing.Size(54, 13);
      this.label3.TabIndex = 2;
      this.label3.Text = "Comment:";
      // 
      // CNetHelpProvider
      // 
      this.CNetHelpProvider.HelpNamespace = "DRView.chm";
      // 
      // AboutBox
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(418, 446);
      this.Controls.Add(this.gbDB);
      this.Controls.Add(this.gbAssemblies);
      this.Controls.Add(this.gbProduct);
      this.Controls.Add(this.okButton);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.CNetHelpProvider.SetHelpKeyword(this, "AboutBox.htm");
      this.CNetHelpProvider.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "AboutBox";
      this.Padding = new System.Windows.Forms.Padding(9);
      this.CNetHelpProvider.SetShowHelp(this, true);
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "AboutBox";
      ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).EndInit();
      this.gbProduct.ResumeLayout(false);
      this.gbProduct.PerformLayout();
      this.gbAssemblies.ResumeLayout(false);
      this.gbDB.ResumeLayout(false);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button okButton;
    private System.Windows.Forms.PictureBox logoPictureBox;
    private System.Windows.Forms.Label labelCopyright;
    private System.Windows.Forms.Label labelCompanyName;
    private System.Windows.Forms.Label labelVersion;
    private System.Windows.Forms.TextBox textBoxDescription;
    private System.Windows.Forms.ListView lvAssemblies;
    private System.Windows.Forms.ColumnHeader name;
    private System.Windows.Forms.ColumnHeader version;
    private System.Windows.Forms.Label lbDBVersion;
    private System.Windows.Forms.Label lbUprgDate;
    private System.Windows.Forms.Label lbMinRqAppVer;
    private System.Windows.Forms.Label lbDBComment;
    private System.Windows.Forms.GroupBox gbProduct;
    private System.Windows.Forms.GroupBox gbAssemblies;
    private System.Windows.Forms.GroupBox gbDB;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.HelpProvider CNetHelpProvider;
  }
}
