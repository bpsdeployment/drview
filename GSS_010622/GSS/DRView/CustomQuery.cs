using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Reflection;
using DDDObjects;
using DRSelectorComponents;
using DRViewComponents;
using TDManagementForms;

namespace DRView
{
  /// <summary>
  /// Form reresents query for diagnostic records.
  /// Form can be customized using array of named parameters passed to the constructor.
  /// </summary>
  public partial class CustomQuery : Form
  {
    #region Public enumerations 
    /// <summary>
    /// Severities of logged messages
    /// </summary>
    public enum eMsgSeverity
    {
      Info,
      Error
    }
    #endregion //Public enumerations 

    #region Private members
    private DRSelectorBase selector; //Selector component
    private InfoMessageEventhandler infoMessageHandler; //Handler for all information messages from components
    private ErrorMessageEventhandler errorMessageHandler; //Handler for all error messages from components
    private RecordCountEventHandler recordCountHandler; //Handler for record count message from selector
    private ReportProgressEventHandler progressReportHandler; //Handler for all progress reports from components
    private NDREventHandler newDataReadyHandler; //Handler for new data ready event from selector
    private RecExport exportDialog; //Dialog for export of diagnostic records
    private FilterDialog filterDialog; //Dialog for specifying filter conditions
    Dictionary<String, Object> queryParams; //Stored constructor parameters
    #endregion //Private members

    #region Public properties
    #endregion //Public properties

    #region Construction
    public CustomQuery()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Constructor, creates and initializes custom query form
    /// </summary>
    /// <param name="queryParams">Collection of named parameters used to initialize query form and selector</param>
    public CustomQuery(Dictionary<String, Object> queryParams)
    {
      //Initialize components
      InitializeComponent();
      //Store parameters
      this.queryParams = queryParams;
      //Initialize event handlers
      infoMessageHandler = this.InfoMessageEvent;
      errorMessageHandler = this.ErrorMessageEvent;
      progressReportHandler = this.ProgressReportEvent;
      recordCountHandler = this.RecordCountEvent;
      newDataReadyHandler = this.NewDataReadyEvent;
      //Create selector component
      //Obtain selector type
      Type selType = Type.GetType(queryParams["SelectorType"].ToString(), true);
      //Create selector instance - use same constructor parameters
      selector = (DRSelectorBase)Activator.CreateInstance(selType, queryParams);
      //Create selector tab
      CreateNewTab("Query", selector);
      //Set title and icon of query form
      this.Text = (String)queryParams["TitleText"] + " " + queryParams["QueryNumber"].ToString();
      this.Icon = ResourceAccess.GetIcon((String)queryParams["Icon"]);
      //Create dialog for record exports
      exportDialog = new RecExport(selector, (DDDHelper)queryParams["DDDHelper"]);
      //Create dialog for record filtering
      filterDialog = new FilterDialog(selector, (DDDHelper)queryParams["DDDHelper"]);
      //Set event handlers for events raised by filter dialog
      filterDialog.ErrorMessage += new DRSelectorComponents.ErrorMessageEventhandler(this.ErrorMessageEvent);
      filterDialog.InfoMessage += new InfoMessageEventhandler(this.InfoMessageEvent);
      filterDialog.ProgressReport += new ReportProgressEventHandler(this.ProgressReportEvent);
      //Set helper object to export dialog
      drExport.DDDHelper = (DDDHelper)queryParams["DDDHelper"];
      //Connect to selector new data ready event
      selector.NewDataReady += exportDialog.NewDataReadyHandler;
      selector.NewDataReady += filterDialog.NewDataReadyHandler;
      selector.NewDataReady += this.newDataReadyHandler;
      //Connect selector to Info and Error message handlers
      selector.InfoMessage += this.infoMessageHandler;
      selector.ErrorMessage += this.errorMessageHandler;
      selector.RecordCountMessage += this.recordCountHandler;
      //Set selector for drExport
      drExport.Selector = selector;
      //Set visibility of system messages
      bool showSystemMessages = Boolean.Parse((String)queryParams["ShowSystemMessages"]);
      systemMessagesToolStripMenuItem.Checked = showSystemMessages;
      tsbShowMessages.Checked = showSystemMessages;
      splContCustomQuery.Panel2Collapsed = !showSystemMessages;
      //Create toobar buttons and menu items which control associated views
      CreateViewButtonsAndMenus((XmlElement)queryParams["ViewConfiguration"]);
    }
    #endregion //Construction

    #region Event handlers
    /// <summary>
    /// Show / hide log text box
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void tsbShowMessages_Click(object sender, EventArgs e)
    {
      if (sender == tsbShowMessages)
        systemMessagesToolStripMenuItem.Checked = tsbShowMessages.Checked;
      else
        tsbShowMessages.Checked = systemMessagesToolStripMenuItem.Checked;
      splContCustomQuery.Panel2Collapsed = !tsbShowMessages.Checked;
    }

    /// <summary>
    /// Handles all error messages generated by components on the form
    /// </summary>
    /// <param name="sender">Sending component</param>
    /// <param name="error">Error in the form of exception</param>
    public void ErrorMessageEvent(object sender, Exception error)
    {
      Exception ex;
      //Log exception to listbox
      LogMsg(error.Message, eMsgSeverity.Error, 0);
      //Log all inner exceptions
      ex = error;
      while ((ex = ex.InnerException) != null)
        LogMsg(ex.Message, eMsgSeverity.Error, 1);
    }

    /// <summary>
    /// Handles all info messages generated by components on the form
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="message"></param>
    public void InfoMessageEvent(object sender, string message)
    {
      //Log message
      LogMsg(message, eMsgSeverity.Info, 0);
    }

    /// <summary>
    /// Set text on status bar label
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="message"></param>
    private void RecordCountEvent(object sender, RecCounEventArgs args)
    {
      statusLabelRecords.Text = String.Format(Properties.Resources.strRecCount, args.RecCount);
      Update(); //Redraw the form
    }

    /// <summary>
    /// Handles all progress reports from components on the form
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="progress"></param>
    /// <param name="showBar"></param>
    public void ProgressReportEvent(Object sender, int progress, bool showBar)
    {
      //Set progess bar visibility
      if (showBar != progresBar.Visible)
        progresBar.Visible = showBar;
      //Set progress bar value
      progress = Math.Max(progresBar.Minimum, progress);
      progress = Math.Min(progresBar.Maximum, progress);
      progresBar.Value = progress;
      //Update form
      Update();
    }

    /// <summary>
    /// Method handles NewDataReady event raised by SelectorComponent.
    /// </summary>
    /// <param name="sender">selector that raised the event</param>
    private void NewDataReadyEvent(DRSelectorBase sender, NDREventArgs args)
    {
      RecordCountEvent(this, new RecCounEventArgs(sender.RecordsView.Count));
      if (args.CountLimitReached)
        MessageBox.Show(Properties.Resources.strCountLimitWarning, "Count limit", MessageBoxButtons.OK, MessageBoxIcon.Warning);
      LogMsg(Properties.Resources.strReady, eMsgSeverity.Info, 0);
    }

    /// <summary>
    /// Saves dataset with selected records to XML file.
    /// </summary>
    private void saveSelectedDataToolStripMenuItem_Click(object sender, EventArgs e)
    {
      //Prepare save file dialog
      dlgSaveFile.Reset();
      dlgSaveFile.DefaultExt = Properties.Resources.DRFileExtension;
      dlgSaveFile.Filter = Properties.Resources.DRFilesFilter;
      //Show file save dialog
      if (dlgSaveFile.ShowDialog() == DialogResult.OK)
        selector.SaveRecordsToFile(dlgSaveFile.FileName);
    }

    /// <summary>
    /// Displays record export dialog and exports records according to user selection
    /// </summary>
    private void exportRecordsToolStripMenuItem_Click(object sender, EventArgs e)
    {
      try
      {
        //Display dialog
        if (exportDialog.ShowDialog() != DialogResult.OK)
          return; //User pressed cancel button
      }
      catch (Exception ex)
      {
        ErrorMessageEvent(this, new Exception("Failed to show export dialog", ex));
      }

      //Export records
      //Determine export method
      if (exportDialog.rbRecCSV.Checked)
        drExport.SaveRecordsToCSV(exportDialog.tbSeparator.Text, exportDialog.tbTimeFormat.Text,
                                  exportDialog.tbFileName.Text, exportDialog.SelectedRecAttributes());
      else if (exportDialog.rbVarCSV.Checked)
        drExport.SaveVarsToCSV(exportDialog.varTreeView.GetSelectedVarIDs(), exportDialog.tbSeparator.Text,
                               exportDialog.tbTimeFormat.Text, exportDialog.tbFileName.Text,
                               exportDialog.chbCompleteVarTitle.Checked, exportDialog.chbIncludeDDDVer.Checked);
    }

    /// <summary>
    /// Displays dialog enabling the user to select filter conditions
    /// </summary>
    private void tsbShowFilterDialog_Click(object sender, EventArgs e)
    {
      //Display dialog
      DialogResult res = filterDialog.ShowDialog();
      if (res == DialogResult.OK)
      {
        selector.ApplyFilter(filterDialog.RecordFilter, filterDialog.SelectedVariables);
        tsbApplyFilter.Checked = true;
        applyFilterToolStripMenuItem.Checked = true;
      }
      else if (res == DialogResult.Ignore)
      {
        selector.RemoveFilter();
        tsbApplyFilter.Checked = false;
        applyFilterToolStripMenuItem.Checked = false;
      }
    }

    /// <summary>
    /// Applies / removes filters for selected records
    /// </summary>
    private void tsbApplyFilter_Click(object sender, EventArgs e)
    {
      if (sender == applyFilterToolStripMenuItem)
        tsbApplyFilter.Checked = applyFilterToolStripMenuItem.Checked;
      else if (sender == tsbApplyFilter)
        applyFilterToolStripMenuItem.Checked = tsbApplyFilter.Checked;
      if (tsbApplyFilter.Checked)
        selector.ApplyFilter(filterDialog.RecordFilter, filterDialog.SelectedVariables);
      else
        selector.RemoveFilter();
    }

    private void CustomQuery_FormClosed(object sender, FormClosedEventArgs e)
    {
      this.Dispose();
    }

    /// <summary>
    /// Executes query for diagnostic records by calling selector of the form
    /// </summary>
    private void tsbSelect_Click(object sender, EventArgs e)
    {
      //Load records using selector
      selector.LoadRecordsAsync();
    }

    /// <summary>
    /// Saves complete settings of current diagnostic query into binary file
    /// </summary>
    private void saveQueryToolStripMenuItem_Click(object sender, EventArgs e)
    {
      try
      {
        //Prepare save file dialog
        dlgSaveFile.Reset();
        dlgSaveFile.DefaultExt = Properties.Resources.DRQFileExtension;
        dlgSaveFile.Filter = Properties.Resources.DRQFilesFilter;
        //Show file save dialog
        if (dlgSaveFile.ShowDialog() == DialogResult.OK)
        {
          //Open file for writing
          FileStream file = new FileStream(dlgSaveFile.FileName, FileMode.Create, FileAccess.Write);
          //Create binary formatter for object serialization
          BinaryFormatter formatter = new BinaryFormatter();
          //Save selector type
          formatter.Serialize(file, selector.GetType());
          //Save selector settings
          selector.SaveSettingsToFile(file, formatter);
          //Save filter settings
          filterDialog.SaveToFile(file, formatter);
          //Save filter applied setting
          formatter.Serialize(file, tsbApplyFilter.Checked);
          //Close file
          file.Close();
          file.Dispose();
        }
      }
      catch (Exception ex)
      {
        ErrorMessageEvent(this, new Exception("Failed to save query", ex));        
      }
    }

    /// <summary>
    /// Handles click from toolstrip items which control visibility of views
    /// </summary>
    /// <param name="sender">Sending item</param>
    /// <param name="e">Argument</param>
    private void viewToolStripItem_Click(object sender, EventArgs e)
    {
      //Obtain view configuration from sender
      CustomViewConfig viewConfig = null;
      if (sender.GetType() == typeof(ToolStripMenuItem))
        viewConfig = (CustomViewConfig)((ToolStripMenuItem)sender).Tag;
      else if (sender.GetType() == typeof(ToolStripButton))
        viewConfig = (CustomViewConfig)((ToolStripButton)sender).Tag;
      else //Invalid sender type
        throw new Exception("Failed to obtain view configuration");

      /*
      //Create view control
      viewConfig.

      //Create tab with view component
      CreateNewTab();
      */
    }
    #endregion //Event handlers

    #region Helper methods
    private void LogMsg(String message, eMsgSeverity severity, int indent)
    {
      //Create header for the string
      String header = DateTime.Now.ToString("HH:mm:ss.fff: ");
      header = header.PadRight(header.Length + indent * 2);
      //Display message
      lbMessages.Items.Add(header + message);
      lbMessages.TopIndex = lbMessages.Items.Count - 1;
      //Set status label
      if (indent == 0)
        statusLabel.Text = message;
      Update(); //Redraw the form
    }

    /// <summary>
    /// Method creates and displays new tab with given graphical component.
    /// </summary>
    /// <param name="tabName">Name of the tab</param>
    /// <param name="control">Control which shall be displayed on the tab</param>
    private TabPage CreateNewTab(String tabName, Control control)
    {
      try
      {
        //Create page
        TabPage page = new TabPage(tabName);
        page.UseVisualStyleBackColor = true;
        //Add control to page
        page.Controls.Add(control);
        //Add page to tab control
        tbcComponents.Controls.Add(page);
        //Set control dock style
        control.Dock = DockStyle.Fill;
        //Select newly added page
        tbcComponents.SelectedTab = page;
        //Return created page
        return page;
      }
      catch (Exception ex)
      {
        throw new Exception(String.Format("Failed to create tab {0}", tabName), ex);
      }
    }

    /// <summary>
    /// Method creates toolbar buttons and menu items for each configured view type
    /// </summary>
    /// <param name="viewConfig">XML element with view configuration</param>
    private void CreateViewButtonsAndMenus(XmlElement viewsConfigElem)
    {
      //Obtain elements with list of available views
      XmlNodeList viewElems = viewsConfigElem.SelectNodes("AvailableViews/View");
      for(int viewIndex = 0; viewIndex < viewElems.Count; viewIndex++) 
      { //Iterate over all view elements
        XmlElement viewElem = (XmlElement)viewElems[viewIndex];
        //Obtain attributes
        String tooltip = viewElem.GetAttribute("tooltip");
        String text = viewElem.GetAttribute("name");
        Bitmap viewImage = ResourceAccess.GetBitmap(viewElem.GetAttribute("image"));
        bool allowMultiple = Convert.ToBoolean(viewElem.GetAttribute("allowMultiple"));
        //Create toolbar button and add to toolbar
        ToolStripButton viewButton = new ToolStripButton(String.Empty, viewImage, new EventHandler(this.viewToolStripItem_Click));
        viewButton.ToolTipText = tooltip;
        viewButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
        toolStrip.Items.Insert(viewIndex + 4, viewButton);
        //Create menu item and add to menu strip
        ToolStripMenuItem viewMenu = new ToolStripMenuItem(text, viewImage, new EventHandler(this.viewToolStripItem_Click));
        viewMenu.ToolTipText = tooltip;
        viewToolStripMenuItem.DropDownItems.Insert(viewIndex, viewMenu);
        //Create custom view config object
        CustomViewConfig viewConfig = new CustomViewConfig(viewElem, viewButton, viewMenu);
        //Store view config in button tag
        viewButton.Tag = viewConfig;
        //Store view config in menu item tag
        viewMenu.Tag = viewConfig;
      }
    }
    #endregion //Helper methods

    #region Public methods
    /// <summary>
    /// Loads previously saved query and filter settings from given file
    /// </summary>
    /// <param name="file">File stream to load from</param>
    /// <param name="formatter">Formatter to use for object deserialization</param>
    public void LoadQuerySetting(FileStream file, BinaryFormatter formatter)
    {
      //Load selector settings
      selector.LoadSettingsFromFile(file, formatter);
      //Load filter settings
      filterDialog.LoadFromFile(file, formatter);
      //Load filter applied status
      bool filtered = (bool)formatter.Deserialize(file);
      //Apply filter if required
      if (filtered)
      {
        selector.ApplyFilter(filterDialog.RecordFilter, filterDialog.SelectedVariables);
        tsbApplyFilter.Checked = true;
        applyFilterToolStripMenuItem.Checked = true;
      }
    }
    #endregion //Public methods
  }

  /// <summary>
  /// Class represents configuration of one view in custom query form.
  /// </summary>
  class CustomViewConfig
  {
    public XmlElement ViewConfig = null; //XML element with configuration of associated view
    public ToolStripMenuItem MenuItem = null; //Menu item associated with the view
    public ToolStripButton Button = null; //Button associated with the view
    public TabPage ViewPage = null; //TabPage on which view is placed
    public Control View = null; //Control representing the view

    /// <summary>
    /// Standard constructor
    /// </summary>
    /// <param name="viewElem">Element with view configurations</param>
    public CustomViewConfig(XmlElement viewElem, ToolStripButton button, ToolStripMenuItem menuItem)
    { //Store view configuration element
      this.ViewConfig = viewElem;
      //Store tool strip items
      this.Button = button;
      this.MenuItem = menuItem;
    }
  }
}
