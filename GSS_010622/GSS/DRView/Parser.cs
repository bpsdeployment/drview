﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Windows.Forms;
using System.Xml;

//MANCANZA INFO
//Da completare
public class Parser
{
    public string BaselinePath;
    public string docSave;
    public List<Fam> EventList_Fam = new List<Fam>();
    public List<Signal> EventList_Signal = new List<Signal>();
    public string fileContent;
    public string FileName;
    public string filePath;
    public string Name;
    public int NumEvents;
    public string OutputDirectory;
    public string TypeBaseLine;
    public string TypeFile;

    //Variables used to find baseline version from the name of the diagnostics
    public string DiagSysId;
    public string DiagContentId;
    public string DiagBlVersion;
    public string baselineNameFromDiag;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////Constant
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private XmlElement DDX;
    private readonly string BeforeEnd = "0";
    private readonly string BeforeStart = "7";
    private readonly string PastEnd = "0";
    private readonly string PastStart = "6";
    private readonly string SamplingPeriod = "500";

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////General Function
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public Parser(string filePath, string BaselinePath)
    {
        this.filePath = filePath;
        this.BaselinePath = BaselinePath;
    }

    public Parser()
    {
    }

    //It recognizes the type of loco and calls the function to parse the xml
    public void CheckXML()
    {
        XmlDocument doc = new XmlDocument();
        doc.Load(filePath);

        XmlNodeList a = doc.GetElementsByTagName("Header");
        if (a[0].Attributes["diagVer"]?.InnerText != null)
        {
            WriteFileDRView();
        }
        else
        {
            MessageBox.Show("Il file selezionato non è un diagnostico");
        }
    }

    //Compress the DRView.xml file created in .xgz
    public void Compress()
    {
        FileInfo fileToCompress = new FileInfo(docSave);
        using (FileStream originalFileStream = File.Open(docSave, FileMode.Open))//fileToCompress.OpenRead())
        {
            if ((File.GetAttributes(fileToCompress.FullName) & FileAttributes.Hidden) != FileAttributes.Hidden && fileToCompress.Extension != ".gz")
            {
                //remove .xml and insert .xgz
                string compress = fileToCompress.FullName.Remove(fileToCompress.FullName.Length - 4);
                using (FileStream compressedFileStream = File.Create(compress + ".xgz"))
                {
                    using (GZipStream compressionStream = new GZipStream(compressedFileStream, CompressionMode.Compress))
                    {
                        originalFileStream.CopyTo(compressionStream);
                    }
                }
            }
        }
        // MessageBox.Show("File compresso in .XGZ");
    }

    //Function that recognizes the type of locomotive with the "fleet" attribute in the "Header" node and assigns the name to the xml
    public void Type()
    {
        XmlDocument doc = new XmlDocument();
        doc.Load(filePath);

        XmlNodeList headerNode = doc.GetElementsByTagName("Header");
        XmlNodeList diagnosticDataTrasmissionNode = doc.GetElementsByTagName("DiagnosticDataTransmission");

        if (headerNode[0].Attributes["diagVer"] != null)
        {
            /*
            if (headerNode[0].Attributes["fleet"]?.InnerText == "E403")
            {
                TypeFile = "E403";
            }
            else if (headerNode[0].Attributes["fleet"]?.InnerText == "E402B")
            {
                TypeFile = "E402B";
            }
            else if (headerNode[0].Attributes["fleet"]?.InnerText == "E401")
            {
                TypeFile = "E401";
            }
            else if (headerNode[0].Attributes["fleet"]?.InnerText == "MD")
            {
                TypeFile = "MD";
            }
            else if (headerNode[0].Attributes["fleet"]?.InnerText == "TAF")
            {
                TypeFile = "TAF";
            }
            */

            

            if (diagnosticDataTrasmissionNode[0].Attributes["fn"] != null)
            {
                Name = diagnosticDataTrasmissionNode[0].Attributes["fn"]?.InnerText;
            }
            else
            {

                MessageBox.Show("Il file selezionato non è un diagnostico");
                return;
                /*
                //Name if "fn" attribute is missing
                Name = headerNode[0].Attributes["fleet"]?.InnerText + "_DRView.xml";
                int s = 0;

                while (File.Exists(Name))
                {
                    s++;
                    Name = Name + "(" + s.ToString() + ")";
                }
                */
            }

            //Get the fleetID
            TypeFile = Name.Substring(0, Name.IndexOf("_"));
        }
        else
        {
            MessageBox.Show("Il file selezionato non è un diagnostico");
        }

        getInfoFromDiagName(diagnosticDataTrasmissionNode[0].Attributes["fn"]?.InnerText);
        baselineNameFromDiag = $"{TypeFile}___{DiagSysId}_{DiagContentId.Replace("upl","uplBL")}";
    }

    private void getInfoFromDiagName(string fn)
    {
        string tmp = "";

        //analyze the name of the diagnostics file and extract 

        //- the device that generated the diagnostics
        DiagSysId = fn;
        for (int i = 0; i < 5; i++)
        {
            tmp = (DiagSysId.IndexOf("_") == 0) ? DiagSysId.Substring(1) : DiagSysId.Substring(DiagSysId.IndexOf("_"));
            DiagSysId = tmp;
        }
        DiagSysId = DiagSysId.Substring(0, DiagSysId.IndexOf("_"));

        //- the type of records
        DiagContentId = tmp;
        for (int i = 0; i < 2; i++)
        {
            tmp = (DiagContentId.IndexOf("_") == 0) ? DiagContentId.Substring(1) : DiagContentId.Substring(DiagContentId.IndexOf("_"));
            DiagContentId = tmp;
        }
        DiagContentId = DiagContentId.Substring(0, DiagContentId.IndexOf("_"));

        //- the baseline version 
        DiagBlVersion = tmp;
        for (int i = 0; i < 6; i++)
        {
            tmp = (DiagBlVersion.IndexOf("_") == 0) ? DiagBlVersion.Substring(1) : DiagBlVersion.Substring(DiagBlVersion.IndexOf("_"));
            DiagBlVersion = tmp;
        }
        DiagBlVersion = DiagBlVersion.Substring(0, DiagBlVersion.IndexOf("_"));

    }

    //Function that recognizes the type of locomotive with the "fleet" attribute in the "Header" node
    public void TypeBL()
    {
        XmlDocument doc = new XmlDocument();
        doc.Load(BaselinePath);

        XmlNodeList a = doc.GetElementsByTagName("Header");
        if (a[0].Attributes["blVer"] != null)
        {
            if (a[0].Attributes["fleet"]?.InnerText == "E403")
            {
                TypeBaseLine = "E403";
            }
            else if (a[0].Attributes["fleet"]?.InnerText == "E402B")
            {
                TypeBaseLine = "E402B";
            }
            else if (a[0].Attributes["fleet"]?.InnerText == "E401")
            {
                TypeBaseLine = "E401";
            }
            else if (a[0].Attributes["fleet"]?.InnerText == "MD")
            {
                TypeBaseLine = "MD";
            }
            else if (a[0].Attributes["fleet"]?.InnerText == "TAF")
            {
                TypeBaseLine = "TAF";
            }
        }
        else
        {
            MessageBox.Show("Il file selezionato non è una baseline");
        }
    }

    //Function that save all the Fam with all its attributes
    private void ParseFamBL(XmlDocument BL)
    {
        XmlNodeList Fam = BL.GetElementsByTagName("Fam");
        EventList_Fam.Clear();
        foreach (XmlNode FamNode in Fam)
        {
            List<Signal> SignalList = new List<Signal>();
            foreach (XmlNode SignalNode in FamNode.ChildNodes)
            {
                SignalList.Add(new Signal(SignalNode, BL));
            }
            Fam tmpFam = new Fam(FamNode);
            tmpFam.SetSignalList(SignalList);
            EventList_Fam.Add(tmpFam);
        }
    }

    //Function that rewrites all the RepresNames. INT8 -> Int8
    private string ResizeSigDT(String Event_sigDT)
    {
        string tmp;

        if (Event_sigDT == "INT8" || Event_sigDT == "Integer8" || Event_sigDT == "BIT" || Event_sigDT == "Boolean1")
        {
            tmp = "Int8";
        }
        else if (Event_sigDT == "INT16" || Event_sigDT == "Integer16")
        {
            tmp = "Int16";
        }
        else if (Event_sigDT == "INT32" || Event_sigDT == "Integer32")
        {
            tmp = "Int32";
        }
        else if (Event_sigDT == "REAL32")
        {
            tmp = "Real32";
        }
        else if (Event_sigDT == "REAL64")
        {
            tmp = "Real64";
        }
        else if (Event_sigDT == "UINT8" || Event_sigDT == "Unsigned8")
        {
            tmp = "Uint8";
        }
        else if (Event_sigDT == "UINT16" || Event_sigDT == "Unsigned16")
        {
            tmp = "Uint16";
        }
        else if (Event_sigDT == "UINT32" || Event_sigDT == "Unsigned32")
        {
            tmp = "Uint32";
        }
        else if (Event_sigDT == "UINT64" || Event_sigDT == "Unsigned64")
        {
            tmp = "Uint64";
        }
        else
        {
            tmp = Event_sigDT;
        }
        return tmp;
    }

    private  string RewriteTimeStS(string sTs)
    {
        if (sTs.Contains(" "))
        {
            sTs = sTs.Replace(" ", "T");
            sTs = sTs.Replace(".00", "");
        }
        return sTs;
    }

    private string RewriteTimeEtS(string eTs)
    {
        if (eTs.Contains(" "))
        {
            eTs = eTs.Replace(" ", "T");
            eTs = eTs.Replace(".00", "");
        }
        return eTs;
    }
//Function that saves the xml file. Create a folder inside the path where you got the file to be parsed
private void SaveXML(XmlDocument doc)
    {
        //Save XML document
        docSave = Path.GetDirectoryName(filePath);
        if (Directory.Exists(docSave))
        {
            docSave = docSave + "\\Output";
            Directory.CreateDirectory(docSave);
        }
        OutputDirectory = docSave + "\\Output";
        docSave = docSave + "\\" + Name;
        doc.Save(docSave);
       
    }

    //Function that write all the file "DRView", including <DD> and <DDD>
    private void WriteFileDRView()
    {
        XmlDocument docXML = new XmlDocument();
        docXML.Load(filePath);

        //<?xml version="1.0" encoding="utf-8"?>
        XmlDocument DRViewFile = new XmlDocument();

        //MANCANZA INFO
        XmlNode docNode = DRViewFile.CreateXmlDeclaration("1.0", "UTF-8", null);
        DRViewFile.AppendChild(docNode);

        //<DDX>
        DDX = DRViewFile.CreateElement("DDX");
        DRViewFile.AppendChild(DDX);

        //<DD>
        XmlElement DDMessage = DRViewFile.CreateElement("DD");

        //MANCANZA INFO
        //This ID is generated by the ground server when the TU is registered in the ground server database
        //DD.SetAttribute("TUInstanceID", "");
        DDMessage.SetAttribute("TUInstanceID", "6E5B042D-E378-4AA1-8EC6-D26867CC7708");

        //MANCANZA INFO
        //This ID is generated by the ground server when the DDD is registered in the ground server database
        //DD.SetAttribute("DDDVersionID", "");
        DDMessage.SetAttribute("DDDVersionID", "91d952e6-b6ee-be44-ad92-a76a50f0e9f8");

        //MANCANZA INFO
        //Telediagnostic Units with big-endian CPUs sets this attribute to “true”, little-endian TUs sets this attribute to “false”
        DDMessage.SetAttribute("BigEndianData", "true");
        DDX.AppendChild(DDMessage);

        XmlNodeList nodes = docXML.GetElementsByTagName("Evt");
        int i = 0;
        int InstID = 1;
        foreach (XmlNode record in nodes)
        {
            string sTs = record.Attributes["sTs"]?.InnerText;
            string eTs = record.Attributes["eTs"]?.InnerText;

            //Replace the " " values with "T" and Remove the ".00" values in the sTs,eTs string
            sTs = RewriteTimeStS(sTs);
            if(eTs != null)
            {
                eTs = RewriteTimeEtS(eTs);
            }

            //<Record>
            XmlElement Record = DRViewFile.CreateElement("Record");


            string id = record.Attributes["id"]?.InnerText;

            //id = id.Remove(id.Length - 1);

            Record.SetAttribute("RecordDefID", FilterId(id));
            Record.SetAttribute("RecordInstID", InstID.ToString());
            Record.SetAttribute("StartTime", sTs);
            
            if (eTs != null)
            {
                Record.SetAttribute("EndTime", eTs);
            }

            /*
            //NON OBBLIGATORI
            else
            {
                //Record.SetAttribute("EndTime", null);
                Record.SetAttribute("EndTime", DateTime.Now.ToString("yyyy-MM-dd") + "T" + DateTime.Now.ToString("hh:mm:ss"));
            }
            
            //MANCANZA INFO
            Record.SetAttribute("LastModified", DateTime.Now.ToString("yyyy-MM-dd") + "T" + DateTime.Now.ToString("hh:mm:ss"));
            if(record.Attributes["ImportTime"]?.InnerText != null)
            {
                Record.SetAttribute("ImportTime", record.Attributes["ImportTime"]?.InnerText);
            }
            else
            {
                //Record.SetAttribute("ImportTime", null);
                //TEST
                //Bisogna mettergli un tempo bisogna decidere quale
                Record.SetAttribute("ImportTime", DateTime.Now.ToString("yyyy-MM-dd") + "T" + DateTime.Now.ToString("hh:mm:ss"));
            }
            Record.SetAttribute("CompleteData", "1");
            */

            Record.SetAttribute("VehicleNumber", record.Attributes["veh"]?.InnerText);

            if (record.Attributes["sysId"]?.InnerText != null)
            {
                Record.SetAttribute("Source", record.Attributes["sysId"]?.InnerText);
            }

            if (record.Attributes["hexbin"]?.InnerText != null)
            {
                string hexbin = record.Attributes["hexbin"]?.InnerText;

                //Convertion in BASE 64
                byte[] buffEnvData = Encoding.ASCII.GetBytes(hexbin);
                string buffEnvData64Base = System.Convert.ToBase64String(buffEnvData);
                Record.InnerText = buffEnvData64Base;
            }
            else if (record.Attributes["csv"]?.InnerText != null)
            {
                string csv = record.Attributes["csv"]?.InnerText;

                //Convertion in BASE 64
                byte[] buffEnvData = Encoding.ASCII.GetBytes(csv);
                string buffEnvData64Base = Convert.ToBase64String(buffEnvData);

                Record.InnerText = buffEnvData64Base;
            }

            DDMessage.AppendChild(Record);
            /*
            //TEST
            try
            {
                //PASS
                Convert.ToDateTime(Record.GetAttribute("StartTime")).ToUniversalTime();
            }
            catch (Exception ex)
            {
                throw new Exception("StartTime", ex);
            }

            if ((Record.GetAttribute("EndTime")) != null)
            {
                try
                {
                    //PASS
                    if ((Record.GetAttribute("EndTime")) != null)
                        Convert.ToDateTime(Record.GetAttribute("EndTime")).ToUniversalTime();
                }
                catch (Exception ex)
                {
                    throw new Exception("EndTime", ex);
                }
            }

            try
            {
                //PASS
                Convert.ToDateTime(Record.GetAttribute("LastModified")).ToUniversalTime();
            }
            catch (Exception ex)
            {
                throw new Exception("LastModified", ex);
            }

            try
            {
                //PASS
                Convert.ToInt32(Record.GetAttribute("RecordDefID"));
            }
            catch (Exception ex)
            {
                throw new Exception("RecordDefID", ex);
            }

            try
            {
                //PASS
                Convert.ToInt64(Record.GetAttribute("RecordInstID"));
            }
            catch (Exception ex)
            {
                throw new Exception("RecordInstID", ex);
            }

            if (Record.GetAttribute("ImportTime") != null)
            {
                try
                {
                    Convert.ToDateTime(Record.GetAttribute("ImportTime")).ToUniversalTime();
                }
                catch (Exception ex)
                {
                    throw new Exception("ImportTime", ex);
                }
            }

            if (Record.GetAttribute("VehicleNumber") != null)
            {
                try
                {
                    Convert.ToInt16(Record.GetAttribute("VehicleNumber"));
                }
                catch (Exception ex)
                {
                    throw new Exception("VehicleNumber", ex);
                }
            }

            try
            {
                Convert.FromBase64String(Record.InnerText);
            }
            catch (Exception ex)
            {
                throw new Exception("InnerText", ex);
            }
            //END TEST
            */
            i++;
            InstID = InstID + 1;
        }

        XmlDocument docBL = new XmlDocument();
        docBL.Load(BaselinePath);

        //Verify that the BL chosen is the right one
        XmlNodeList HeaderXML = docXML.GetElementsByTagName("Header");
        XmlNodeList HeaderBL = docBL.GetElementsByTagName("Header");

        if (HeaderBL[0].Attributes["blVer"].ToString() != HeaderXML[0].Attributes["diagVer"].ToString())
        {
            string message = "Vuoi continuare?";
            string title = "La Baseline selezionata non è coerente con quella del file XML";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show(message, title, buttons);
            if (result == DialogResult.Yes)
            {
                this.Continue();
            }
            else
            {
                this.Close();
            }

        }

        ParseFamBL(docBL);

        //<DDD>
        XmlElement DDD = DRViewFile.CreateElement("DDD");
        DDX.AppendChild(DDD);
        //<VersionInfo>
        //MANCANZA INFO
        XmlElement VersionInfo = DRViewFile.CreateElement("VersionInfo");

        //MANCANZA INFO
        //name identifying unambiguously the type of target Telediagnostic Unit. This type must be previously defined in database, to which DDD is being imported.
        VersionInfo.SetAttribute("TUTypeName", "IT-CPU/1.0/");
        //VersionInfo.SetAttribute("TUTypeName", "");

        //MANCANZA INFO
        //user defined string identifying the version of DDD. It can have a form of version number e.g. 1.14.58 or any other string form.
        VersionInfo.SetAttribute("DDDVersion", "00061");
        //VersionInfo.SetAttribute("DDDVersion", "");

        //The date is written like this 2013-04-23T06:57:29
        VersionInfo.SetAttribute("CreationDate", DateTime.Now.ToString("yyyy-MM-dd") + "T" + DateTime.Now.ToString("hh:mm:ss"));
        DDD.AppendChild(VersionInfo);

        XmlElement Representations = DRViewFile.CreateElement("Representations");
        DDD.AppendChild(Representations);

        //Function that writes all types of <Representation>
        WriteRappresentations(DRViewFile, Representations);

        XmlElement Variables = DRViewFile.CreateElement("Variables");
        DDD.AppendChild(Variables);

        XmlNodeList Fam = docBL.GetElementsByTagName("Fam");

        foreach (XmlNode FamEvent in Fam)
        {
            XmlNodeList SignalAnag = docBL.GetElementsByTagName("SignalAnag");

            foreach (XmlNode Event in SignalAnag)
            {
                //<Record>
                XmlElement Variable = DRViewFile.CreateElement("Variable");
                string RepresName;
                Variable.SetAttribute("Name", Event.Attributes["sigSD"]?.InnerText);

                //Function that rewrites all the RepresNames. INT8 -> Int8
                RepresName = ResizeSigDT(Event.Attributes["sigDT"]?.InnerText);

                Variable.SetAttribute("RepresName", RepresName);

                //MANCANZA INFO
                Variable.SetAttribute("RefAttrName", "");
                Variables.AppendChild(Variable);

                XmlElement Title = DRViewFile.CreateElement("Title");
                //MANCANZA INFO
                //Sempre inglese??
                Title.SetAttribute("Language", "english");
                Title.SetAttribute("Text", Event.Attributes["sigSD"]?.InnerText);
                Variable.AppendChild(Title);
            }
            XmlElement Trigger = DRViewFile.CreateElement("Trigger");

            //MANCANZA INFO
            Trigger.SetAttribute("Name", FamEvent.Attributes["famId"]?.InnerText);

            XmlElement FamTitle = DRViewFile.CreateElement("Title");
            //Sempre inglese??
            FamTitle.SetAttribute("Language", "english");
            FamTitle.SetAttribute("Text", FamEvent.Attributes["famSD"]?.InnerText);
            Trigger.AppendChild(FamTitle);

            Variables.AppendChild(Trigger);
        }

        XmlElement EventRecords = DRViewFile.CreateElement("EventRecords");
        DDD.AppendChild(EventRecords);

        XmlNodeList EventListBL = docBL.GetElementsByTagName("Event");

        foreach (XmlNode EventBL in EventListBL)
        {
            if (!(bool)(EventBL.Attributes["evtId"]?.InnerText.Contains("R")))
            {
                XmlElement EventRecord = DRViewFile.CreateElement("EventRecord");

                EventRecord.SetAttribute("FaultCode", FilterId(EventBL.Attributes["evtId"]?.InnerText));
                EventRecord.SetAttribute("Name", EventBL.Attributes["evtSD"]?.InnerText);
                //Filter the possible severity values
                EventRecord.SetAttribute("Severity", SeverityFilter(EventBL.Attributes["lv"]?.InnerText));
                EventRecord.SetAttribute("Trigger", EventBL.Attributes["famId"]?.InnerText);

                //MANCANZA INFO
                EventRecord.SetAttribute("TriggerReaction", "LeadingEdge");

                EventRecords.AppendChild(EventRecord);
                XmlElement Title = DRViewFile.CreateElement("Title");

                //MANCANZA INFO
                Title.SetAttribute("Language", "english");
                Title.SetAttribute("Text", EventBL.Attributes["evtSD"]?.InnerText);

                EventRecord.AppendChild(Title);

                //CAMBIARE
                foreach (Fam fam in EventList_Fam)
                {
                    if (fam.famId == EventBL.Attributes["famId"]?.InnerText)
                    {
                        EventList_Signal = fam.GelSignalList();
                        foreach (Signal signal in EventList_Signal)
                        {
                            XmlElement HistorizedInput = DRViewFile.CreateElement("HistorizedInput");

                            //MANCANZA INFO ------->
                            HistorizedInput.SetAttribute("BeforeEnd", BeforeEnd);
                            HistorizedInput.SetAttribute("BeforeStart", BeforeStart);
                            HistorizedInput.SetAttribute("PastEnd", PastEnd);
                            HistorizedInput.SetAttribute("PastStart", PastStart);
                            HistorizedInput.SetAttribute("SamplingPeriod", SamplingPeriod);
                            //<-------- Fino a qua

                            if (signal.sigSD != null)
                            {
                                HistorizedInput.SetAttribute("VarName", signal.sigSD);
                            }
                            else
                            {
                                //MANCANZA INFO
                                HistorizedInput.SetAttribute("VarName", signal.sigName);
                            }
                            EventRecord.AppendChild(HistorizedInput);
                        }
                    }
                }
            }
        }
        //END <EventRecords>
        //END <DDD>

        XmlElement TUInstanceInfo = DRViewFile.CreateElement("TUInstanceInfo");

        //MANCANZA INFO
        //TUInstanceInfo.SetAttribute("Name", "");
        TUInstanceInfo.SetAttribute("Name", "UCV5200");

        //MANCANZA INFO
        //TUInstanceInfo.SetAttribute("InstanceID", "");
        TUInstanceInfo.SetAttribute("InstanceID", "6E5B042D-E378-4AA1-8EC6-D26867CC7708");

        //MANCANZA INFO
        //TUInstanceInfo.SetAttribute("Comment", "");
        TUInstanceInfo.SetAttribute("Comment", "Vehicle Control Unit");

        DDX.AppendChild(TUInstanceInfo);

        XmlElement TUType = DRViewFile.CreateElement("TUType");
        //MANCANZA INFO
        //TUType.SetAttribute("Name", "");
        TUType.SetAttribute("Name", "UCV5200");

        //MANCANZA INFO
        //TUType.SetAttribute("TypeID", "");
        TUType.SetAttribute("TypeID", "12D98697-9C4A-427F-9AB2-CE8D684D96D9");

        //MANCANZA INFO
        //TUType.SetAttribute("Comment", "");
        TUType.SetAttribute("Comment", "Vehicle control unit based on MPC5200");


        TUInstanceInfo.AppendChild(TUType);

        XmlElement DDDVersionInfo = DRViewFile.CreateElement("DDDVersionInfo");
        //MANCANZA INFO
        //DDDVersionInfo.SetAttribute("VersionID", "");
        DDDVersionInfo.SetAttribute("VersionID", "91d952e6-b6ee-be44-ad92-a76a50f0e9f8");

        //MANCANZA INFO
        //DDDVersionInfo.SetAttribute("TUTypeID", "");
        DDDVersionInfo.SetAttribute("TUTypeID", "37c5ebab-2ac7-4262-a738-40ed9b521b98");

        //MANCANZA INFO
        //DDDVersionInfo.SetAttribute("UserVersion", "");
        DDDVersionInfo.SetAttribute("UserVersion", "00061");

        DDDVersionInfo.SetAttribute("CreationDate", DateTime.Now.ToString("yyyy-MM-dd") + "T" + DateTime.Now.ToString("hh:mm:ss"));
        DDDVersionInfo.SetAttribute("ImportDate", DateTime.Now.ToString("yyyy-MM-dd") + "T" + DateTime.Now.ToString("hh:mm:ss"));

        DDX.AppendChild(DDDVersionInfo);

        SaveXML(DRViewFile);

    }

    private void Close()
    {
        //throw new Exception();
        Application.Restart();

    }

    private void Continue()
    {
        //throw new NotImplementedException();
    }

    //Function that writes all types of <Representation>
    private void WriteRappresentations(XmlDocument DRViewFile, XmlNode Representations)
    {

        //MANCANZA INFO
        XmlElement TIMESTAMP = DRViewFile.CreateElement("BinaryRepres");
        TIMESTAMP.SetAttribute("Name", "TIMESTAMP");
        TIMESTAMP.SetAttribute("Length", "22");
        TIMESTAMP.SetAttribute("Format", "ASCII");
        Representations.AppendChild(TIMESTAMP);
      
        /*
        XmlElement BOOL = DRViewFile.CreateElement("SimpleRepres");
        BOOL.SetAttribute("Name", "BOOL");
        BOOL.SetAttribute("DataTypeName", "BIT");
        BOOL.SetAttribute("Divisor", "1");
        Representations.AppendChild(BOOL);
        */

        XmlElement INT8 = DRViewFile.CreateElement("SimpleRepres");
        INT8.SetAttribute("Name", "Int8");
        INT8.SetAttribute("DataTypeName", "I8");
        INT8.SetAttribute("Divisor", "1");
        Representations.AppendChild(INT8);

        XmlElement INT16 = DRViewFile.CreateElement("SimpleRepres");
        INT16.SetAttribute("Name", "Int16");
        INT16.SetAttribute("DataTypeName", "I16");
        INT16.SetAttribute("Divisor", "1");
        Representations.AppendChild(INT16);

        XmlElement INT32 = DRViewFile.CreateElement("SimpleRepres");
        INT32.SetAttribute("Name", "Int32");
        INT32.SetAttribute("DataTypeName", "I32");
        INT32.SetAttribute("Divisor", "1");
        Representations.AppendChild(INT32);

        XmlElement INT64 = DRViewFile.CreateElement("SimpleRepres");
        INT64.SetAttribute("Name", "Int64");
        INT64.SetAttribute("DataTypeName", "I64");
        INT64.SetAttribute("Divisor", "1");
        Representations.AppendChild(INT64);

        XmlElement UINT8 = DRViewFile.CreateElement("SimpleRepres");
        UINT8.SetAttribute("Name", "Uint8");
        UINT8.SetAttribute("DataTypeName", "U8");
        UINT8.SetAttribute("Divisor", "1");
        Representations.AppendChild(UINT8);

        XmlElement UINT16 = DRViewFile.CreateElement("SimpleRepres");
        UINT16.SetAttribute("Name", "Uint16");
        UINT16.SetAttribute("DataTypeName", "U16");
        UINT16.SetAttribute("Divisor", "1");
        Representations.AppendChild(UINT16);

        XmlElement UINT32 = DRViewFile.CreateElement("SimpleRepres");
        UINT32.SetAttribute("Name", "Uint32");
        UINT32.SetAttribute("DataTypeName", "U32");
        UINT32.SetAttribute("Divisor", "1");
        Representations.AppendChild(UINT32);

        XmlElement UINT64 = DRViewFile.CreateElement("SimpleRepres");
        UINT64.SetAttribute("Name", "Uint64");
        UINT64.SetAttribute("DataTypeName", "U64");
        UINT64.SetAttribute("Divisor", "1");
        Representations.AppendChild(UINT64);

        XmlElement REAL32 = DRViewFile.CreateElement("SimpleRepres");
        REAL32.SetAttribute("Name", "Real32");
        REAL32.SetAttribute("DataTypeName", "R32");
        REAL32.SetAttribute("Divisor", "1");
        Representations.AppendChild(REAL32);

        XmlElement REAL64 = DRViewFile.CreateElement("SimpleRepres");
        REAL64.SetAttribute("Name", "Real64");
        REAL64.SetAttribute("DataTypeName", "R64");
        REAL64.SetAttribute("Divisor", "1");
        Representations.AppendChild(REAL64);

        XmlElement CHAR1 = DRViewFile.CreateElement("BinaryRepres");
        CHAR1.SetAttribute("Name", "CHAR1");
        CHAR1.SetAttribute("Length", "1");
        CHAR1.SetAttribute("Format", "ASCII");
        Representations.AppendChild(CHAR1);

        XmlElement CHAR2 = DRViewFile.CreateElement("BinaryRepres");
        CHAR2.SetAttribute("Name", "CHAR2");
        CHAR2.SetAttribute("Length", "2");
        CHAR2.SetAttribute("Format", "ASCII");
        Representations.AppendChild(CHAR2);

        XmlElement CHAR4 = DRViewFile.CreateElement("BinaryRepres");
        CHAR4.SetAttribute("Name", "CHAR4");
        CHAR4.SetAttribute("Length", "4");
        CHAR4.SetAttribute("Format", "ASCII");
        Representations.AppendChild(CHAR4);

        XmlElement CHAR5 = DRViewFile.CreateElement("BinaryRepres");
        CHAR5.SetAttribute("Name", "CHAR5");
        CHAR5.SetAttribute("Length", "5");
        CHAR5.SetAttribute("Format", "ASCII");
        Representations.AppendChild(CHAR5);

        XmlElement CHAR8 = DRViewFile.CreateElement("BinaryRepres");
        CHAR8.SetAttribute("Name", "CHAR8");
        CHAR8.SetAttribute("Length", "8");
        CHAR8.SetAttribute("Format", "ASCII");
        Representations.AppendChild(CHAR8);

        XmlElement CHAR16 = DRViewFile.CreateElement("BinaryRepres");
        CHAR16.SetAttribute("Name", "CHAR16");
        CHAR16.SetAttribute("Length", "16");
        CHAR16.SetAttribute("Format", "ASCII");
        Representations.AppendChild(CHAR16);

        XmlElement CHAR32 = DRViewFile.CreateElement("BinaryRepres");
        CHAR32.SetAttribute("Name", "CHAR32");
        CHAR32.SetAttribute("Length", "32");
        CHAR32.SetAttribute("Format", "ASCII");
        Representations.AppendChild(CHAR32);

    }

    private string SeverityFilter(string lv)
    {
        if (!(lv == "A" || lv == "A1" || lv == "B" || lv == "B1" || lv == "C"))
        {
            lv = "C";
        }

        return lv;
    }
    private string FilterId(string id)
    {
        if (id.Contains("I") || id.Contains("F") || id.Contains("R"))
        {
            id = id.Remove(id.Length - 1);
        }
        return id;
    }
}