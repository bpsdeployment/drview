namespace DRView
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.msMainMenu = new System.Windows.Forms.MenuStrip();
            this.queryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recordsQueryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statisticalQueryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tuStatisticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newTUDirectQueryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openSavedRecordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openSavedstatisticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openSavedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openDiagnosticsXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.propertiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cascadeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.titleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tileVerticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dddHelper = new DDDObjects.DDDHelper(this.components);
            this.DRViewHelpProvider = new System.Windows.Forms.HelpProvider();
            this.dlgOpenFile = new System.Windows.Forms.OpenFileDialog();
            this.persistWindow = new Mowog.PersistWindowComponent(this.components);
            this.msMainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // msMainMenu
            // 
            this.DRViewHelpProvider.SetHelpKeyword(this.msMainMenu, resources.GetString("msMainMenu.HelpKeyword"));
            this.DRViewHelpProvider.SetHelpNavigator(this.msMainMenu, ((System.Windows.Forms.HelpNavigator)(resources.GetObject("msMainMenu.HelpNavigator"))));
            this.msMainMenu.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.msMainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.queryToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.windowToolStripMenuItem,
            this.helpToolStripMenuItem});
            resources.ApplyResources(this.msMainMenu, "msMainMenu");
            this.msMainMenu.MdiWindowListItem = this.windowToolStripMenuItem;
            this.msMainMenu.Name = "msMainMenu";
            this.DRViewHelpProvider.SetShowHelp(this.msMainMenu, ((bool)(resources.GetObject("msMainMenu.ShowHelp"))));
            // 
            // queryToolStripMenuItem
            // 
            this.queryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.newTUDirectQueryToolStripMenuItem,
            this.openSavedRecordsToolStripMenuItem,
            this.openDiagnosticsXMLToolStripMenuItem,
            this.openSavedstatisticsToolStripMenuItem,
            this.openSavedToolStripMenuItem});
            this.queryToolStripMenuItem.Name = "queryToolStripMenuItem";
            resources.ApplyResources(this.queryToolStripMenuItem, "queryToolStripMenuItem");
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.recordsQueryToolStripMenuItem,
            this.statisticalQueryToolStripMenuItem,
            this.tuStatisticsToolStripMenuItem});
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            resources.ApplyResources(this.newToolStripMenuItem, "newToolStripMenuItem");
            // 
            // recordsQueryToolStripMenuItem
            // 
            resources.ApplyResources(this.recordsQueryToolStripMenuItem, "recordsQueryToolStripMenuItem");
            this.recordsQueryToolStripMenuItem.Name = "recordsQueryToolStripMenuItem";
            this.recordsQueryToolStripMenuItem.Click += new System.EventHandler(this.NewQueryWindow_Click);
            // 
            // statisticalQueryToolStripMenuItem
            // 
            resources.ApplyResources(this.statisticalQueryToolStripMenuItem, "statisticalQueryToolStripMenuItem");
            this.statisticalQueryToolStripMenuItem.Name = "statisticalQueryToolStripMenuItem";
            this.statisticalQueryToolStripMenuItem.Click += new System.EventHandler(this.statisticalQueryToolStripMenuItem_Click);
            // 
            // tuStatisticsToolStripMenuItem
            // 
            resources.ApplyResources(this.tuStatisticsToolStripMenuItem, "tuStatisticsToolStripMenuItem");
            this.tuStatisticsToolStripMenuItem.Name = "tuStatisticsToolStripMenuItem";
            this.tuStatisticsToolStripMenuItem.Click += new System.EventHandler(this.tuStatisticsToolStripMenuItem_Click);
            // 
            // newTUDirectQueryToolStripMenuItem
            // 
            resources.ApplyResources(this.newTUDirectQueryToolStripMenuItem, "newTUDirectQueryToolStripMenuItem");
            this.newTUDirectQueryToolStripMenuItem.Name = "newTUDirectQueryToolStripMenuItem";
            this.newTUDirectQueryToolStripMenuItem.Click += new System.EventHandler(this.NewQueryWindow_Click);
            // 
            // openSavedRecordsToolStripMenuItem
            // 
            resources.ApplyResources(this.openSavedRecordsToolStripMenuItem, "openSavedRecordsToolStripMenuItem");
            this.openSavedRecordsToolStripMenuItem.Name = "openSavedRecordsToolStripMenuItem";
            this.openSavedRecordsToolStripMenuItem.Click += new System.EventHandler(this.NewQueryWindow_Click);
            // 
            // openSavedstatisticsToolStripMenuItem
            // 
            resources.ApplyResources(this.openSavedstatisticsToolStripMenuItem, "openSavedstatisticsToolStripMenuItem");
            this.openSavedstatisticsToolStripMenuItem.Name = "openSavedstatisticsToolStripMenuItem";
            this.openSavedstatisticsToolStripMenuItem.Click += new System.EventHandler(this.statisticalQueryToolStripMenuItem_Click);
            // 
            // openSavedToolStripMenuItem
            // 
            resources.ApplyResources(this.openSavedToolStripMenuItem, "openSavedToolStripMenuItem");
            this.openSavedToolStripMenuItem.Name = "openSavedToolStripMenuItem";
            this.openSavedToolStripMenuItem.Click += new System.EventHandler(this.openSavedToolStripMenuItem_Click);
            // 
            // openDiagnosticsXMLToolStripMenuItem
            // 
            this.openDiagnosticsXMLToolStripMenuItem.Image = global::DRView.Properties.Resources.openFolder;
            this.openDiagnosticsXMLToolStripMenuItem.Name = "openDiagnosticsXMLToolStripMenuItem";
            resources.ApplyResources(this.openDiagnosticsXMLToolStripMenuItem, "openDiagnosticsXMLToolStripMenuItem");
            this.openDiagnosticsXMLToolStripMenuItem.Click += new System.EventHandler(this.NewQueryWindow_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.propertiesToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            resources.ApplyResources(this.optionsToolStripMenuItem, "optionsToolStripMenuItem");
            // 
            // propertiesToolStripMenuItem
            // 
            this.propertiesToolStripMenuItem.Name = "propertiesToolStripMenuItem";
            resources.ApplyResources(this.propertiesToolStripMenuItem, "propertiesToolStripMenuItem");
            this.propertiesToolStripMenuItem.Click += new System.EventHandler(this.propertiesToolStripMenuItem_Click);
            // 
            // windowToolStripMenuItem
            // 
            this.windowToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cascadeToolStripMenuItem,
            this.titleToolStripMenuItem,
            this.tileVerticalToolStripMenuItem,
            this.toolStripSeparator1});
            this.windowToolStripMenuItem.Name = "windowToolStripMenuItem";
            resources.ApplyResources(this.windowToolStripMenuItem, "windowToolStripMenuItem");
            // 
            // cascadeToolStripMenuItem
            // 
            this.cascadeToolStripMenuItem.Name = "cascadeToolStripMenuItem";
            resources.ApplyResources(this.cascadeToolStripMenuItem, "cascadeToolStripMenuItem");
            this.cascadeToolStripMenuItem.Click += new System.EventHandler(this.cascadeToolStripMenuItem_Click);
            // 
            // titleToolStripMenuItem
            // 
            this.titleToolStripMenuItem.Name = "titleToolStripMenuItem";
            resources.ApplyResources(this.titleToolStripMenuItem, "titleToolStripMenuItem");
            this.titleToolStripMenuItem.Click += new System.EventHandler(this.titleToolStripMenuItem_Click);
            // 
            // tileVerticalToolStripMenuItem
            // 
            this.tileVerticalToolStripMenuItem.Name = "tileVerticalToolStripMenuItem";
            resources.ApplyResources(this.tileVerticalToolStripMenuItem, "tileVerticalToolStripMenuItem");
            this.tileVerticalToolStripMenuItem.Click += new System.EventHandler(this.tileVerticalToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            resources.ApplyResources(this.helpToolStripMenuItem, "helpToolStripMenuItem");
            // 
            // contentToolStripMenuItem
            // 
            resources.ApplyResources(this.contentToolStripMenuItem, "contentToolStripMenuItem");
            this.contentToolStripMenuItem.Name = "contentToolStripMenuItem";
            this.contentToolStripMenuItem.Click += new System.EventHandler(this.contentToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            resources.ApplyResources(this.aboutToolStripMenuItem, "aboutToolStripMenuItem");
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // dddHelper
            // 
            this.dddHelper.ConnectionString = "";
            this.dddHelper.TimeFormat = "yyyy-MM-dd HH:mm:ss.fff";
            this.dddHelper.TimeFormatGraph = "yy-MM-dd HH:mm:ss";
            this.dddHelper.TimeOffset = 1;
            this.dddHelper.UseLocalTime = true;
            // 
            // DRViewHelpProvider
            // 
            resources.ApplyResources(this.DRViewHelpProvider, "DRViewHelpProvider");
            // 
            // dlgOpenFile
            // 
            this.dlgOpenFile.ShowReadOnly = true;
            // 
            // persistWindow
            // 
            this.persistWindow.Form = this;
            this.persistWindow.UseLocAppDataDir = true;
            this.persistWindow.XMLFilePath = "DRViewWindowState.xml";
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.msMainMenu);
            this.DRViewHelpProvider.SetHelpKeyword(this, resources.GetString("$this.HelpKeyword"));
            this.DRViewHelpProvider.SetHelpNavigator(this, ((System.Windows.Forms.HelpNavigator)(resources.GetObject("$this.HelpNavigator"))));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.msMainMenu;
            this.Name = "MainForm";
            this.DRViewHelpProvider.SetShowHelp(this, ((bool)(resources.GetObject("$this.ShowHelp"))));
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.msMainMenu.ResumeLayout(false);
            this.msMainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.MenuStrip msMainMenu;
    private System.Windows.Forms.ToolStripMenuItem queryToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem windowToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem cascadeToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem titleToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem tileVerticalToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private DDDObjects.DDDHelper dddHelper;
    private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem propertiesToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem tuStatisticsToolStripMenuItem;
    private System.Windows.Forms.HelpProvider DRViewHelpProvider;
    private System.Windows.Forms.ToolStripMenuItem contentToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem openSavedRecordsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem openSavedToolStripMenuItem;
    private System.Windows.Forms.OpenFileDialog dlgOpenFile;
    private System.Windows.Forms.ToolStripMenuItem newTUDirectQueryToolStripMenuItem;
    private Mowog.PersistWindowComponent persistWindow;
    private System.Windows.Forms.ToolStripMenuItem statisticalQueryToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem openSavedstatisticsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem recordsQueryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openDiagnosticsXMLToolStripMenuItem;
    }
}

