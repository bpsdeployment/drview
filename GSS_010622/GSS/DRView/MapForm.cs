using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DDDObjects;
using DRSelectorComponents;

namespace DRView
{
  public partial class frmMap : Form
  {
    #region Private members
    private DRSelectorBase selector;
    private DRQuery query;
    #endregion //Private members

    #region Public properties
    #endregion //Public properties

    #region Construction initialization
    /// <summary>
    /// Constructs the form
    /// DRView on the form is provided to given selector
    /// and parent Query form
    /// </summary>
    /// <param name="selector">Selector component of parent query window</param>
    /// <param name="query">Parent query window</param>
    /// <param name="queryNumber">Ordinal number of parent query window since application start</param>
    public frmMap(DRSelectorBase selector, DRQuery query, int queryNumber)
    {
      InitializeComponent();
      //Store objects
      this.selector = selector;
      this.query = query;
      //Initialize view parameters
      drViewMap.DDDHelper = selector.DDDHelper;
      drViewMap.DisplayData = true;
      //Attach events to selector
      selector.NewDataReady += drViewMap.NewDataReadyHandler;
      selector.OnSelectRecord += drViewMap.SelectRecordHandler;
      selector.DDDHelper.NewLanguageSelected += drViewMap.NewLanguageHandler;
      this.drViewMap.InfoMessage += new DRSelectorComponents.InfoMessageEventhandler(query.InfoMessageEvent);
      this.drViewMap.ProgressReport += new DRSelectorComponents.ReportProgressEventHandler(query.ProgressReportEvent);
      this.drViewMap.ErrorMessage += new DRSelectorComponents.ErrorMessageEventhandler(query.ErrorMessageEvent);
      //Set form title
      this.Text = Properties.Resources.strMapFormTitle + " " + queryNumber.ToString();
    }
    #endregion //Construction initialization

    #region Event handlers
    private void frmMap_FormClosed(object sender, FormClosedEventArgs e)
    {
      //Notify parent query form, that map view has closed
      query.VisibleViews = query.VisibleViews & ~(DRQuery.eVisibleDRViews.Map);
      //Desubscribe event handlers
      selector.NewDataReady -= drViewMap.NewDataReadyHandler;
      selector.OnSelectRecord -= drViewMap.SelectRecordHandler;
      selector.DDDHelper.NewLanguageSelected -= drViewMap.NewLanguageHandler;
      //Dispose the form
      this.Dispose();
    }

    private void frmMap_Load(object sender, EventArgs e)
    {
      //Raise first OnNewDataReady event
      drViewMap.NewDataReadyHandler.Invoke(this.selector, new NDREventArgs(false, false));
    }

    /// <summary>
    /// Raised when information displayed on the map changes
    /// e.g. user selects different batch of records
    /// </summary>
    private void drViewMap_MapViewChange(DRViewComponents.DRViewMap sender)
    {
      tsLabelRecs.Text = String.Format(Properties.Resources.strMapFormStatus, 
        drViewMap.CurrFirstRecIndx, drViewMap.CurrLastRecIndx, selector.RecordsView.Count, 
        drViewMap.FirstStartTime, drViewMap.LastStartTime);
    }

    /// <summary>
    /// Event raised, when key is pressed and form has focus
    /// </summary>
    private void frmMap_KeyDown(object sender, KeyEventArgs e)
    {
      switch (e.KeyCode)
      {
        //Keys which control displayed records
        case Keys.PageUp:
          drViewMap.ChangeDisplayedRecords(DRViewComponents.DRViewMap.eChangeShownRecs.ShowPrevious);
          break;
        case Keys.PageDown:
          drViewMap.ChangeDisplayedRecords(DRViewComponents.DRViewMap.eChangeShownRecs.ShowNext);
          break;
        case Keys.Home:
          drViewMap.ChangeDisplayedRecords(DRViewComponents.DRViewMap.eChangeShownRecs.ShowFirst);
          break;
        case Keys.End:
          drViewMap.ChangeDisplayedRecords(DRViewComponents.DRViewMap.eChangeShownRecs.ShowLast);
          break;
        case Keys.NumPad8:
          drViewMap.PanMap(DRViewComponents.DRViewMap.ePanMapDirection.Up);
          break;
        case Keys.NumPad2:
          drViewMap.PanMap(DRViewComponents.DRViewMap.ePanMapDirection.Down);
          break;
        case Keys.NumPad4:
          drViewMap.PanMap(DRViewComponents.DRViewMap.ePanMapDirection.Left);
          break;
        case Keys.NumPad6:
          drViewMap.PanMap(DRViewComponents.DRViewMap.ePanMapDirection.Right);
          break;
        case Keys.Add:
          drViewMap.ZoomMap(DRViewComponents.DRViewMap.eMapZoomDir.ZoomIn);
          break;
        case Keys.Subtract:
          drViewMap.ZoomMap(DRViewComponents.DRViewMap.eMapZoomDir.ZoomOut);
          break;
      }
      e.Handled = true;
    }
    #endregion //Event handlers
  }
}