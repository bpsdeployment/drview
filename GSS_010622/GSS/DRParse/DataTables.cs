using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DDDObjects;

namespace DRParse
{
  /// <summary>
  /// Class represents data table with parsed variables values,
  /// </summary>
  class ParsedVarsTable
  {
    private DataTable parsedVars; //DataTable object holding parsed values

    /// <summary>
    /// Constructor, initializes data table with columns
    /// </summary>
    public ParsedVarsTable()
    {
      //initialize the data table
      //Create columns for parsedValues table
      DataColumn colTUInstanceID = new DataColumn("TUInstanceID", Type.GetType("System.Guid"));
      DataColumn colRecordInstID = new DataColumn("RecordInstID", Type.GetType("System.UInt64"));
      DataColumn colDDDVersionID = new DataColumn("DDDVersionID", Type.GetType("System.Guid"));
      DataColumn colRecordDefID = new DataColumn("RecordDefID", Type.GetType("System.Int32"));
      DataColumn colInputIndex = new DataColumn("InputIndex", Type.GetType("System.Int32"));
      DataColumn colVariableID = new DataColumn("VariableID", Type.GetType("System.Int32"));
      DataColumn colTimeStamp = new DataColumn("TimeStamp", Type.GetType("System.UInt64"));
      DataColumn colValueIndex = new DataColumn("ValueIndex", Type.GetType("System.Int32"));
      DataColumn colValueArrayIndex = new DataColumn("ValueArrayIndex", Type.GetType("System.Int32"));
      DataColumn colValue = new DataColumn("Value", Type.GetType("System.Object"));
      //Create parsedValues table and add columns;
      parsedVars = new DataTable();
      parsedVars.Columns.Add(colTUInstanceID);
      parsedVars.Columns.Add(colRecordInstID);
      parsedVars.Columns.Add(colDDDVersionID);
      parsedVars.Columns.Add(colRecordDefID);
      parsedVars.Columns.Add(colInputIndex);
      parsedVars.Columns.Add(colVariableID);
      parsedVars.Columns.Add(colTimeStamp);
      parsedVars.Columns.Add(colValueIndex);
      parsedVars.Columns.Add(colValueArrayIndex);
      parsedVars.Columns.Add(colValue);
    }

    //Property providing read only acces to internal data table
    public DataTable ParsedVars
    {
      get { return parsedVars; }
    }

    /// <summary>
    /// Inserts collection of rows into table according to passed structures.
    /// </summary>
    /// <param name="recbinData">Structure containing record instance information</param>
    /// <param name="parsedVals">List of structures with parsed values</param>
    public void InsertData(sRecBinData recbinData, List<sParsedVarValue> parsedVals)
    {
      foreach (sParsedVarValue parsedVal in parsedVals)
        parsedVars.Rows.Add(recbinData.TUInstanceID, recbinData.RecordInstID, recbinData.DDDVersionID, recbinData.RecordDefID, 
                            parsedVal.InputIndex, parsedVal.VariableID, parsedVal.TimeStamp.Ticks, parsedVal.ValueIndex, 
                            parsedVal.ValueArrayIndex, parsedVal.Value);
    }

    //Deletes all rows from data table
    public void ClearTable()
    {
      parsedVars.Rows.Clear();
    }
  }

  /// <summary>
  /// Class represents data table containing reference to parsed diagnostic records
  /// together with the result of parsing (succes or failure reason)
  /// </summary>
  class RecParseStatTable
  {
    private DataTable parsedRecs; //DataTable object holding reference to parsed records

    /// <summary>
    /// Constructor, initializes data table with columns
    /// </summary>
    public RecParseStatTable()
    {
      //Create columns for parsedRecs table
      DataColumn colTUInstanceID = new DataColumn("TUInstanceID", Type.GetType("System.Guid"));
      DataColumn colRecordInstID = new DataColumn("RecordInstID", Type.GetType("System.UInt64"));
      DataColumn colRecordDefID = new DataColumn("RecordDefID", Type.GetType("System.Int32"));
      DataColumn colFailReasonID = new DataColumn("FailReasonID", Type.GetType("System.Int32"));
      //Create parsedRecs table and add columns
      parsedRecs = new DataTable();
      parsedRecs.Columns.Add(colTUInstanceID);
      parsedRecs.Columns.Add(colRecordInstID);
      parsedRecs.Columns.Add(colRecordDefID);
      parsedRecs.Columns.Add(colFailReasonID);
    }

    //Property providing read only acces to internal data table
    public DataTable ParsedRecs
    {
      get { return parsedRecs; }
    }

    //Inserts row referencing successfully parsed record
    public void InsertRecParseResult(Guid TUInstanceID, Int64 RecordInstID, Int32 RecordDefID, eParseStatus ParseStatus)
    {
      parsedRecs.Rows.Add(TUInstanceID, RecordInstID, RecordDefID, (int)ParseStatus);
    }

    //Deletes all rows from data table
    public void ClearTable()
    {
      parsedRecs.Rows.Clear();
    }
  }
}
