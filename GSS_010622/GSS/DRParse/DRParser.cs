using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using DDDObjects;


namespace DRParse
{
  /// <summary>
  /// Class is used for parsing binary data associated with diagnostic records.
  /// Class uses DDDHelper component to load record structures from DB and parse binary data.
  /// Parsed variable values are loaded back to DB using the BulkInsert objects.
  /// </summary>
  public class DRParser
  {
    #region Private members
    private DRParserParams parameters;  //Parser parametrisation
    private ParseResults result;        //Last parsing result
    ParsedVarsTable parsedVars;
    RecParseStatTable recParseStats;
    DDDHelper dddHelper;
    SqlConnection dbConnection;
    #endregion //Private members

    #region Public properties
    /// <summary>
    /// Result of last parsing operation
    /// </summary>
    public ParseResults LastResult
    {
      get { return result; }
    }
    #endregion //Public properties

    #region Construction initialization
    /// <summary>
    /// Constructs new parser object.
    /// Creates DDDHelper and new SqlConnection.
    /// Stores parametrisation.
    /// </summary>
    /// <param name="parameters">Class holding parametrisation of the object</param>
    public DRParser(DRParserParams parameters)
    {
      //First check parameters
      if (parameters.DBConnection == null)
        throw new Exception("Database connection has to be initialized");

      //Store parametrisation info
      this.parameters = parameters;

      //Create DB connection
      dbConnection = new SqlConnection(parameters.DBConnection.ConnectionString);
      //Create DDDHeleper
      dddHelper = new DDDHelper();
      //Assign handler for version needed event
      dddHelper.DDDVersionNeeded += new DDDVersionNeededEventHandler(dddHelper_DDDVersionNeeded);

      //Create data tables for parsed data
      parsedVars = new ParsedVarsTable();
      recParseStats = new RecParseStatTable();
    }
    #endregion //Construction initialization

    #region Helper methods
    /// <summary>
    /// Retrieves and returns data reader positioned at the start of result set with binary variable values from DB.
    /// Executes ProcGetBinRec command passed in parametrisation
    /// </summary>
    /// <param name="parseParams">Parametrisation of parse operation</param>
    /// <returns>Data reader with retrieved binary data</returns>
    private SqlDataReader GetBinaryData(ParseCmdParams parseParams)
    {
      SqlDataReader binDataReader;

      //Execute stored procedure, which returns the record set
      try
      {
        //Set command parameters
        parseParams.ProcGetBinRec.Connection = parameters.DBConnection;
        //Execute the command
        binDataReader = parseParams.ProcGetBinRec.ExecuteReader();
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to read binary data from DB", ex);
      }
      return binDataReader;
    }

    /// <summary>
    /// Method parses all binary records contained in result set
    /// and stores results in internal data tables.
    /// </summary>
    /// <param name="dataReader">Data reader with binary records</param>
    private void ParseBinaryRecords(SqlDataReader dataReader)
    {
      sRecBinData recBinData;
      List<sParsedVarValue> parsedVals;
      eParseStatus parseRes;

      //Iterate over all records in record set
      while (dataReader.Read())
      {
        //Read record to structure
        recBinData.TUInstanceID = dataReader.GetGuid(0);
        recBinData.RecordInstID = dataReader.GetInt64(1);
        recBinData.DDDVersionID = dataReader.GetGuid(2);
        recBinData.RecordDefID = dataReader.GetInt32(3);
        recBinData.StartTime = dataReader.GetDateTime(4);
        if (!dataReader.IsDBNull(5))
          recBinData.EndTime = dataReader.GetDateTime(5);
        else
          recBinData.EndTime = DateTime.MinValue;
        recBinData.BigEndian = dataReader.GetBoolean(6);
        if (!dataReader.IsDBNull(7))
          recBinData.BinData = dataReader.GetSqlBytes(7).Buffer;
        else
          recBinData.BinData = new byte[0];
        //Parse the record
        parseRes = dddHelper.ParseBinRecord(recBinData, out parsedVals);
        //Add results to internal tables
        recParseStats.InsertRecParseResult(recBinData.TUInstanceID, recBinData.RecordInstID, recBinData.RecordDefID, parseRes);
        if (parseRes == eParseStatus.parseSuccessfull)
          parsedVars.InsertData(recBinData, parsedVals);
      }
    }

    /// <summary>
    /// Method performs the bulk copy operation.
    /// </summary>
    /// <param name="parseParams">Parametrisation of parse operation</param>
    private void BulkCopyTables(ParseCmdParams parseParams)
    {
      //Bulk copy the tables into DB
      //Create the bulk copy object
      SqlBulkCopy bcp = new SqlBulkCopy(parameters.DBConnection);
      try
      {
        bcp.BulkCopyTimeout = parameters.BCPTimeout;
        bcp.BatchSize = parameters.BCPBatchSize;
        //Load the parsedVars table
        bcp.DestinationTableName = parseParams.ParsedVarsTableName;
        bcp.WriteToServer(parsedVars.ParsedVars);
        //Load the recParseStats table
        bcp.DestinationTableName = parseParams.RecParseStatTableName;
        bcp.WriteToServer(recParseStats.ParsedRecs);
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to bulk copy parsed data", ex);
      }
      finally
      {
        //Clear tables
        parsedVars.ClearTable();
        recParseStats.ClearTable();
        //Close bulk copy object
        bcp.Close();
      }
    }

    /// <summary>
    /// Returns number of unparsed records in DB.
    /// </summary>
    /// <param name="parseParams">Parametrisation of parse operation</param>
    /// <returns>Number of unparsed records</returns>
    private int GetUnparsedRecCount(ParseCmdParams parseParams)
    {
      try
      {
        //Set command parameters
        parseParams.ProcGetUnprocCount.Connection = parameters.DBConnection;

        //Execute command and return result
        return (int)parseParams.ProcGetUnprocCount.ExecuteScalar();
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to retrieve number of unparsed records", ex);
      }
    }

    /// <summary>
    /// Executes stored DB procedure which processes bulk copied records in DB
    /// </summary>
    /// <param name="parseParams">Parametrisation of parse operation</param>
    private void ProcessParsedReords(ParseCmdParams parseParams)
    {
      try
      {
        //Set command parameters
        parseParams.ProcProcessParsed.Connection = parameters.DBConnection;
        //Execute command
        parseParams.ProcProcessParsed.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to process parsed records in DB", ex);
      }
    }

    /// <summary>
    /// Executes stored DB procedure which deletes all obsolete records
    /// </summary>
    /// <param name="parseParams">Parametrisation of parse operation</param>
    private int DeleteObsoleteRecord(ParseCmdParams parseParams)
    {
      int count = 0;
      try
      {
        //Set command parameters
        parseParams.ProcDeleteObsolete.Connection = parameters.DBConnection;
        //Execute command
        count = parseParams.ProcDeleteObsolete.ExecuteNonQuery();
        return count;
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to delete obsolete records in DB", ex);
      }
    }

    /// <summary>
    /// Handles DDDVersionNeeded event from DDDHelper object.
    /// Always instructs the helper to use DB and supplies DB connection.
    /// </summary>
    void dddHelper_DDDVersionNeeded(DDDHelper sender, DDDVersionNeededEventArgs verNeededArgs)
    {
      verNeededArgs.Source = DDDVersionNeededEventArgs.VersionSource.DB;
      verNeededArgs.DBConnection = dbConnection;
    }
    #endregion //Helper methods

    #region Public methods
    /// <summary>
    /// Method loads unparsed binary records from DB,
    /// parses variable values and bulk copies the values back to DB
    /// </summary>
    /// <param name="parseParams">Parametrisation of parse operation</param>
    /// <returns>Object with results of parsing operation</returns>
    public ParseResults ParseRecords(ParseCmdParams parseParams)
    {
      SqlDataReader dataReader = null;
      int parsedRecCount;
      int parsedValsCount;
      //Store time of parsing start
      Stopwatch totalElepsedTime = Stopwatch.StartNew();
      Stopwatch detailElapsedTime = new Stopwatch();

      //Clear data tables for parsed data
      parsedVars.ClearTable();
      recParseStats.ClearTable();
      //Create new result object
      result = new ParseResults();

      try
      {
        //Check DB connection
        if (parameters.DBConnection.State != ConnectionState.Open)
        { //Connectio closed - reopen
          parameters.DBConnection.Close();
          parameters.DBConnection.Open();
        }
        //Delete obsolete records
        result.DeletedObsoleteRecs = DeleteObsoleteRecord(parseParams);
        //Get number of unparsed records
        result.UnparsedRecs = GetUnparsedRecCount(parseParams);
        while (result.UnparsedRecs > parseParams.UnparsedCountTreshold  //check number of remaining records
               && ((result.IterationCount <= parseParams.MaxIterationCount) || (parseParams.MaxIterationCount == 0)) //check number of allowed iterations
               && ((result.ElapsedTime < parseParams.MaxParseTime) || (parseParams.MaxParseTime.Ticks == 0)) //check allowed parsing time
             )
        {
          try
          {
            detailElapsedTime.Start();
            //Load binary data from DB
            dataReader = GetBinaryData(parseParams);
            result.LoadBinDataTime += detailElapsedTime.Elapsed;
            detailElapsedTime.Reset();

            detailElapsedTime.Start();
            //Parse the document
            ParseBinaryRecords(dataReader);
            result.ParseRecordsTime += detailElapsedTime.Elapsed;
            detailElapsedTime.Reset();
            //Close data reader
            dataReader.Close();

            //Store number of parsed records
            parsedRecCount = recParseStats.ParsedRecs.Rows.Count;
            parsedValsCount = parsedVars.ParsedVars.Rows.Count;

            detailElapsedTime.Start();
            //Bulk copy tables into DB
            BulkCopyTables(parseParams);
            result.BulkCopyTime += detailElapsedTime.Elapsed;
            detailElapsedTime.Reset();

            detailElapsedTime.Start();
            //Process bulk copied records status in DB
            ProcessParsedReords(parseParams);
            result.ProcessParsedRecsTime += detailElapsedTime.Elapsed;
            detailElapsedTime.Reset();

            //Trace the number of parsed records and values
            if (parameters.TraceInfo)
              Trace.TraceInformation("Parsed {0} records with {1} elementary values", parsedRecCount, parsedValsCount);
            //Update result
            result.IterationCount++;
            result.TotalParsedRecs += parsedRecCount;
            result.TotalParsedVals += parsedValsCount;
            result.UnparsedRecs = GetUnparsedRecCount(parseParams);
            result.ElapsedTime = totalElepsedTime.Elapsed;
          }
          catch (Exception ex)
          { //Non fatal execption - clear data tables, log exception and continue if possible
            if (dataReader != null)
              dataReader.Close();
            parsedVars.ClearTable();
            recParseStats.ClearTable();
            result.Errors.Add(ex);
            if (parameters.TraceWarning)
              Trace.TraceWarning("Failed to complete parsing iteration: {0}", ex.Message);
            if (!parameters.ContinueOnError || (result.Errors.Count >= parameters.MaxErrorCount))
              return result; //parsing cannot continue
            //Parsing can continue - check DB connection status
            if (parameters.DBConnection.State != ConnectionState.Open)
            { //Connectio closed - reopen
              parameters.DBConnection.Close();
              parameters.DBConnection.Open();
            }
          } //End of catch - non fatal exception
        } //End of while loop 
      } //End of overall try block
      catch (Exception ex)
      { //Fatal exception - terminate parsing and rethrow the exception
        result.Errors.Add(ex);
        if (parameters.TraceError)
          Trace.TraceError("Parsing batch failed: {0}", ex.Message);
        throw ex;
      }
      //Parsing finished successfully
      result.Success = true;
      return result;
    }
    #endregion //Public methods
  }
}
