using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DRParse
{
  /// <summary>
  /// Class serves as parametrisation for construction of new DRParser object
  /// </summary>
  public class DRParserParams
  {
    /// <summary>
    /// Connection to database
    /// </summary>
    public SqlConnection DBConnection;
    /// <summary>
    /// Timeout in seconds for Bulk Copy operation
    /// </summary>
    public int BCPTimeout = 60;
    /// <summary>
    /// Maximum number of rows bulk copied in one batch (0 -> all rows)
    /// </summary>
    public int BCPBatchSize = 20000;
    /// <summary>
    /// Information messages shall be traced
    /// </summary>
    public bool TraceInfo = false;
    /// <summary>
    /// Warning messages shall be traced
    /// </summary>
    public bool TraceWarning = false;
    /// <summary>
    /// Error messages shall be traced
    /// </summary>
    public bool TraceError = false;
    /// <summary>
    /// Parsing shall continue with, when error occurs (otherwise exception is thrown)
    /// </summary>
    public bool ContinueOnError = true;
    /// <summary>
    /// Maximum number of errors that can occur before parsing is terminated
    /// </summary>
    public int MaxErrorCount = 10;
  }

  /// <summary>
  /// Class serves as parametrisation for one call to Parse method of DRParser object
  /// </summary>
  public class ParseCmdParams
  {
    /// <summary>
    /// Command which returns recordset with binary data
    /// </summary>
    public SqlCommand ProcGetBinRec;
    /// <summary>
    /// Command which returns number of unprocessed records from DB
    /// </summary>
    public SqlCommand ProcGetUnprocCount;
    /// <summary>
    /// Command which processes parsed records bulk copied into DB
    /// </summary>
    public SqlCommand ProcProcessParsed;
    /// <summary>
    /// Command which deletes obsolete parsed records from DB
    /// </summary>
    public SqlCommand ProcDeleteObsolete;
    /// <summary>
    /// Name of target table for parsed variable values (bulk copy)
    /// </summary>
    public String ParsedVarsTableName;
    /// <summary>
    /// Name of target table for record parse status (bulk copy)
    /// </summary>
    public String RecParseStatTableName;
    /// <summary>
    /// Maximum number of parsing iterations (0 = unbounded)
    /// </summary>
    public int MaxIterationCount = 0;
    /// <summary>
    /// Maximum time elapsed by parsing (0 = unbounded)
    /// </summary>
    public TimeSpan MaxParseTime = new TimeSpan(0, 2, 0);
    /// <summary>
    /// Maximum number of records that can be left unparsed
    /// </summary>
    public int UnparsedCountTreshold = 10;
  }

  /// <summary>
  /// Class holds result of a call to Parse method of DRParser object
  /// </summary>
  public class ParseResults
  {
    /// <summary>
    /// Overall parsing success
    /// </summary>
    public bool Success = false;
    /// <summary>
    /// Number of parse iterations processed
    /// </summary>
    public int IterationCount = 0;
    /// <summary>
    /// Time elapsed 
    /// </summary>
    public TimeSpan ElapsedTime = new TimeSpan(0);
    /// <summary>
    /// Total number of parsed records
    /// </summary>
    public int TotalParsedRecs = 0;
    /// <summary>
    /// Total number of parsed record values
    /// </summary>
    public int TotalParsedVals = 0;
    /// <summary>
    /// Number of unparsed records
    /// </summary>
    public int UnparsedRecs = 0;
    /// <summary>
    /// Number of deleted obsolete records
    /// </summary>
    public int DeletedObsoleteRecs = 0;
    /// <summary>
    /// List of errors that occured during parsing (non fatal)
    /// </summary>
    public List<Exception> Errors = new List<Exception>();
    /// <summary>
    /// Total time elapsed by loading of binary data from DB
    /// </summary>
    public TimeSpan LoadBinDataTime = new TimeSpan(0);
    /// <summary>
    /// Total time elapsed by parsing of records
    /// </summary>
    public TimeSpan ParseRecordsTime = new TimeSpan(0);
    /// <summary>
    /// Total time elapsed by bulk copy operation
    /// </summary>
    public TimeSpan BulkCopyTime = new TimeSpan(0);
    /// <summary>
    /// Total time elapsed by processing of parsed records
    /// </summary>
    public TimeSpan ProcessParsedRecsTime = new TimeSpan(0);
  }
}
