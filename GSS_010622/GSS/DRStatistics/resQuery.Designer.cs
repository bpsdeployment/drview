﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Il codice è stato generato da uno strumento.
//     Versione runtime:4.0.30319.42000
//
//     Le modifiche apportate a questo file possono provocare un comportamento non corretto e andranno perse se
//     il codice viene rigenerato.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DRStatistics {
    using System;
    
    
    /// <summary>
    ///   Classe di risorse fortemente tipizzata per la ricerca di stringhe localizzate e così via.
    /// </summary>
    // Questa classe è stata generata automaticamente dalla classe StronglyTypedResourceBuilder.
    // tramite uno strumento quale ResGen o Visual Studio.
    // Per aggiungere o rimuovere un membro, modificare il file con estensione ResX ed eseguire nuovamente ResGen
    // con l'opzione /str oppure ricompilare il progetto VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class resQuery {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal resQuery() {
        }
        
        /// <summary>
        ///   Restituisce l'istanza di ResourceManager nella cache utilizzata da questa classe.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("DRStatistics.resQuery", typeof(resQuery).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Esegue l'override della proprietà CurrentUICulture del thread corrente per tutte le
        ///   ricerche di risorse eseguite utilizzando questa classe di risorse fortemente tipizzata.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a INNER JOIN fGetIntTable(@FaultCodes) fC
        ///    ON (RD.FaultCode = fC.number).
        /// </summary>
        internal static string condFaultCodes {
            get {
                return ResourceManager.GetString("condFaultCodes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a (DR.StartTime &gt;= @From).
        /// </summary>
        internal static string condFromTime {
            get {
                return ResourceManager.GetString("condFromTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a INNER JOIN Hierarchy.fGetSelectedItemsFromNames(@HierarchyNames, @From, @To) HI
        ///    ON (DR.HierarchyItemID = HI.ItemID AND DR.StartTime &gt;= HI.TimeFrom AND (DR.StartTime &lt; HI.TimeTo OR HI.TimeTo IS NULL)).
        /// </summary>
        internal static string condHierItems {
            get {
                return ResourceManager.GetString("condHierItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a (RD.RecordTypeID = @RecType).
        /// </summary>
        internal static string condRecTypeID {
            get {
                return ResourceManager.GetString("condRecTypeID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a INNER JOIN fGetStrTable(@Sources) fS
        ///    ON (DR.Source = fS.item).
        /// </summary>
        internal static string condSources {
            get {
                return ResourceManager.GetString("condSources", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a INNER JOIN fMatchRecTitle(@TitleKeywords, @MatchAllKeywords) fM
        ///    ON (DR.DDDVersionID = fM.VersionID AND DR.RecordDefID = fM.RecordDefID).
        /// </summary>
        internal static string condTitleKeywords {
            get {
                return ResourceManager.GetString("condTitleKeywords", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a (DR.StartTime &lt;= @To).
        /// </summary>
        internal static string condToTime {
            get {
                return ResourceManager.GetString("condToTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a INNER JOIN fGetUniqueIdentifierTable(@TUInstances) TU
        ///    ON (DR.TUInstanceID = TU.number).
        /// </summary>
        internal static string condTUInstances {
            get {
                return ResourceManager.GetString("condTUInstances", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a FROM 
        ///  DiagnosticRecords DR WITH (NOLOCK)
        ///  INNER JOIN RecordDefinitions RD
        ///    ON (DR.DDDVersionID = RD.VersionID AND DR.RecordDefID = RD.RecordDefID)
        ///  INNER JOIN TUInstances TI
        ///    ON (DR.TUInstanceID = TI.TUInstanceID).
        /// </summary>
        internal static string fromPart {
            get {
                return ResourceManager.GetString("fromPart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a GROUP BY.
        /// </summary>
        internal static string groupPart {
            get {
                return ResourceManager.GetString("groupPart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a SELECT 
        ///  count(*) AS RecCount.
        /// </summary>
        internal static string selectPart {
            get {
                return ResourceManager.GetString("selectPart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Cerca una stringa localizzata simile a WHERE.
        /// </summary>
        internal static string wherePart {
            get {
                return ResourceManager.GetString("wherePart", resourceCulture);
            }
        }
    }
}
