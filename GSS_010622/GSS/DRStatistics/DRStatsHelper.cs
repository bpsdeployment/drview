using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DDDObjects;
using DRQueries;

namespace DRStatistics
{
  /// <summary>
  /// Helper class for diagnostic record statistics.
  /// Provides the ability to build statistics queries and process the results.
  /// </summary>
  public class DRStatsHelper
  {
    #region Enumerations
    public enum eStatGroup
    {
      TUInstance,
      TUType,
      FaultCode,
      Source,
      Hour,
      Day,
      Month,
      Year,
      Vehicle,
      Train,
      VehicleNumber,
      DeviceCode,
      FaultSeverity,
      FaultSubSeverity,
      TrcSnpCode,
      RecordType
    }
    #endregion //Enumerations

    #region Private members
    List<DRStatsGroupBase> groups = new List<DRStatsGroupBase>();
    DDDHelper dddHelper;
    #endregion //Private members

    #region Public properties
    /// <summary>
    /// Descriptor of top level statistical group, contains descriptors of child groups
    /// </summary>
    public GroupsDescriptors GroupsDescriptors;
    /// <summary>
    /// Value of top level group, contains values of child groups
    /// </summary>
    public GroupValue GroupValue;
    /// <summary>
    /// DDD Helper object
    /// </summary>
    public DDDHelper DDDHelper
    {
      get { return dddHelper; }
      set { dddHelper = value; }
    }
    #endregion //Public properties

    #region Construction initialization
    /// <summary>
    /// Public constructor
    /// </summary>
    /// <param name="dddHelper">DDD Helper object</param>
    public DRStatsHelper(DDDHelper dddHelper)
    {
      this.dddHelper = dddHelper;
      InitiateGroupHelpers();
    }
    #endregion //Construction initialization

    #region Public methods
    /// <summary>
    /// Add new group to list of groups
    /// </summary>
    /// <param name="groupType">Type of the group</param>
    public void AddGroup(eStatGroup groupType)
    {
      groups.Add(DRStatsGroupBase.CreateGroup(groupType, dddHelper));
    }
    /// <summary>
    /// Clears all groups from the list
    /// </summary>
    public void ClearGroups()
    {
      groups.Clear();
    }
    /// <summary>
    /// Creates and returns statistical query with configured groups.
    /// Conditions may be specified using query object.
    /// </summary>
    /// <returns>Query object</returns>
    public DRQueryHelper CreateStatsQuery()
    {
      //Create new query object
      DRQueryHelper query = new DRQueryHelper();
      //Add groups
      foreach (DRStatsGroupBase group in groups)
        group.AddColumnsToQuery(query);
      //Return query
      return query;
    }

    /// <summary>
    /// Reads results of statistical queries from database.
    /// Creates group helper classes according to the result and specified statistical groups.
    /// </summary>
    /// <param name="reader">Data reader containg result set from statistical query</param>
    /// <returns>number of results (rows) read from the result</returns>
    public int ReadQueryResults(SqlDataReader reader)
    {
      int readCount = 0;
      //Initiate helpers
      InitiateGroupHelpers();
      try
      {
        while (reader.Read())
        {
          //Obtain group value object
          GroupValue val = GroupValue;
          if (groups.Count > 0)
          {
            String groupID;
            int groupIndex;
            for (int i = 0; i < groups.Count; i++)
            {
              groupID = groups[i].GetIDFromRow(reader);
              groupIndex = GroupsDescriptors.GetGroupIndex(i, groupID);
              if (groupIndex == -1)
                groupIndex = GroupsDescriptors.AddGroup(i, groupID, groups[i].GetTitleFromRow(reader));
              val = val.GetChildGroup(groupIndex);
            }
          }
          //Add value to current group value
          int count = reader.GetInt32(0);
          val.Value += count;
          //Increase number of read results
          readCount++;
        }
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to read results of statistical query", ex);
      }
      return readCount;
    }
    #endregion //Public methods

    #region Helper methods
    /// <summary>
    /// Initiates GroupDescriptor and GroupValue memebers
    /// </summary>
    private void InitiateGroupHelpers()
    {
      GroupsDescriptors = new GroupsDescriptors();
      GroupValue = new GroupValue();
      GroupValue.Value = 0;
    }
    #endregion //Helper methods
  }

  #region Helper classes
  /// <summary>
  /// Descriptor of one statistics group. Contains group ID and title and
  /// sorted list of child descriptors.
  /// </summary>
  [Serializable]
  public class GroupsDescriptors
  {
    /// <summary>
    /// Structure representing pair of group title and index
    /// </summary>
    [Serializable]
    private struct sTitleIndx
    {
      public int Index;
      public String Title;
      public sTitleIndx(int indx, String tit)
      {
        Index = indx;
        Title = tit;
      }
    }
    /// <summary>
    /// List (for each group level) of sorted lists of group IDs and titles - index pairs
    /// </summary>
    private List<SortedList<String, sTitleIndx>> groups = new List<SortedList<String, sTitleIndx>>();
    /// <summary>
    /// Retrieves index of group with specified level and groupID
    /// </summary>
    /// <param name="groupLevel">Level (in GROUP BY clause) of the group</param>
    /// <param name="groupID">ID of the group</param>
    /// <returns>Index of given group</returns>
    public int GetGroupIndex(int groupLevel, String groupID)
    {
      if (groupLevel >= groups.Count)
        return -1;
      SortedList<String, sTitleIndx> groupPairs = groups[groupLevel];
      if (groupPairs == null)
        return -1;
      sTitleIndx groupPair;
      if (groupPairs.TryGetValue(groupID, out groupPair))
        return groupPair.Index;
      else
        return -1;
    }
    /// <summary>
    /// Adds group with specified level and group ID into internal lists.
    /// </summary>
    /// <param name="groupLevel">Level of the group</param>
    /// <param name="groupID">ID of the group</param>
    /// <param name="groupTitle">Title of the group</param>
    /// <returns>Index of added group</returns>
    public int AddGroup(int groupLevel, String groupID, String groupTitle)
    {
      while (groupLevel >= groups.Count)
        groups.Add(new SortedList<String, sTitleIndx>());
      SortedList<String, sTitleIndx> groupPairs = groups[groupLevel];
      sTitleIndx groupPair;
      if (groupPairs.TryGetValue(groupID, out groupPair))
      {
        groupPair.Title = groupTitle;
        return groupPair.Index;
      }
      else
      {
        groupPair = new sTitleIndx(groupPairs.Count, groupTitle);
        groupPairs.Add(groupID, groupPair);
        return groupPair.Index;
      }
    }
    /// <summary>
    /// Returns number of group leveles for current query
    /// </summary>
    /// <returns>number of group levels</returns>
    public int GetGroupLevelsCount()
    {
      return groups.Count;
    }
    /// <summary>
    /// Returns number of groups in given level
    /// </summary>
    /// <param name="groupLevel">level</param>
    /// <returns>Number of groups</returns>
    public int GetGroupCount(int groupLevel)
    {
      if (groupLevel < groups.Count && groupLevel >= 0)
        return groups[groupLevel].Count;
      else
        return 0;
    }
    /// <summary>
    /// Returns title of group with given level and 
    /// index in sorted list of groups on given level.
    /// </summary>
    /// <param name="groupLevel">Level of the group</param>
    /// <param name="groupOrderIndex">Index in sorted list of groups</param>
    /// <returns>Title of the group</returns>
    public String GetGroupTitle(int groupLevel, int groupOrderIndex)
    {
      if (groupLevel >= groups.Count || groupLevel < 0)
        return String.Empty;
      if (groupOrderIndex >= groups[groupLevel].Count || groupOrderIndex < 0)
        return String.Empty;
      return groups[groupLevel].Values[groupOrderIndex].Title;
    }
    /// <summary>
    /// Returns data index of group with given level and 
    /// index in sorted list of groups on given level.
    /// </summary>
    /// <param name="groupLevel">Level of the group</param>
    /// <param name="groupOrderIndex">Index in sorted list of groups</param>
    /// <returns>Data index of the group</returns>
    public int GetGroupIndex(int groupLevel, int groupOrderIndex)
    {
      if (groupLevel >= groups.Count || groupLevel < 0)
        return -1;
      if (groupOrderIndex >= groups[groupLevel].Count || groupOrderIndex < 0)
        return -1;
      return groups[groupLevel].Values[groupOrderIndex].Index;
    }
  }
  /// <summary>
  /// Represents value of one statistical group.
  /// May contain child group values.
  /// </summary>
  [Serializable]
  public class GroupValue
  {
    /// <summary>
    /// Value of the group
    /// </summary>
    public int Value = 0;
    /// <summary>
    /// List of child group values
    /// </summary>
    List<GroupValue> ChildGroups = null;
    /// <summary>
    /// Obtains child group with given index.
    /// If group does not exists yet, it is created.
    /// </summary>
    /// <param name="index">Index of requested group</param>
    /// <returns>Group value</returns>
    public GroupValue GetChildGroup(int index)
    {
      if (ChildGroups == null)
        ChildGroups = new List<GroupValue>();
      while (index >= ChildGroups.Count)
        ChildGroups.Add(new GroupValue());
      return ChildGroups[index];
    }
  }
  #endregion //Helper classes
}

