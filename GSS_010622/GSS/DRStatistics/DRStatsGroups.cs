using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DDDObjects;
using DRQueries;
using FleetHierarchies;

namespace DRStatistics
{
  #region DRStatsGroupBase class
  /// <summary>
  /// Base class representing group of records in statistics
  /// </summary>
  abstract class DRStatsGroupBase
  {
    //Stored reference to DDD helper object
    protected DDDHelper dddHelper;

    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="dddHelper">DDD helper object</param>
    public DRStatsGroupBase(DDDHelper dddHelper)
    {
      this.dddHelper = dddHelper;
    }

    /// <summary>
    /// Adds all necessary columns to given statistical query.
    /// Columns are added to both SELECT and GROUP BY clause.
    /// </summary>
    /// <param name="query">Stat query to which columns are added</param>
    public abstract void AddColumnsToQuery(DRQueryHelper query);

    /// <summary>
    /// Returns string ID based on values in current row of columns added by this group.
    /// ID is intended to be used as unique identifier of the resulting group.
    /// </summary>
    /// <param name="reader">Data reader from which values of current row are read</param>
    /// <returns>Unique ID of the group represented by this row</returns>
    public abstract String GetIDFromRow(SqlDataReader reader);

    /// <summary>
    /// Returns title of the group represented by values of columns in current row
    /// of given data reader.
    /// </summary>
    /// <param name="reader">Data reader from which values of current row are read</param>
    /// <returns>Title of the group represented by this group</returns>
    public abstract String GetTitleFromRow(SqlDataReader reader);

    /// <summary>
    /// Creates group object according to given group type
    /// </summary>
    /// <param name="groupType">Requested type of the group</param>
    /// <returns>Group object</returns>
    public static DRStatsGroupBase CreateGroup(DRStatsHelper.eStatGroup groupType, DDDHelper dddHelper)
    {
      switch (groupType)
      {
        case DRStatsHelper.eStatGroup.TUInstance:
          return new DRStatsGroupTUInstance(dddHelper);
        case DRStatsHelper.eStatGroup.TUType:
          return new DRStatsGroupTUType(dddHelper);
        case DRStatsHelper.eStatGroup.FaultCode:
          return new DRStatsGroupFaultCode(dddHelper);
        case DRStatsHelper.eStatGroup.Source:
          return new DRStatsGroupSource(dddHelper);
        case DRStatsHelper.eStatGroup.Hour:
          return new DRStatsGroupHour(dddHelper);
        case DRStatsHelper.eStatGroup.Day:
          return new DRStatsGroupDay(dddHelper);
        case DRStatsHelper.eStatGroup.Month:
          return new DRStatsGroupMonth(dddHelper);
        case DRStatsHelper.eStatGroup.Year:
          return new DRStatsGroupYear(dddHelper);
        case DRStatsHelper.eStatGroup.Vehicle:
          return new DRStatsGroupVehicle(dddHelper);
        case DRStatsHelper.eStatGroup.Train:
          return new DRStatsGroupTrain(dddHelper);
        case DRStatsHelper.eStatGroup.VehicleNumber:
          return new DRStatsGroupVehicleNumber(dddHelper);
        case DRStatsHelper.eStatGroup.DeviceCode:
          return new DRStatsGroupDeviceCode(dddHelper);
        case DRStatsHelper.eStatGroup.FaultSeverity:
          return new DRStatsGroupFaultSeverity(dddHelper);
        case DRStatsHelper.eStatGroup.FaultSubSeverity:
          return new DRStatsGroupFaultSubSeverity(dddHelper);
        case DRStatsHelper.eStatGroup.TrcSnpCode:
          return new DRStatsGroupTrcSnpCode(dddHelper);
        case DRStatsHelper.eStatGroup.RecordType:
          return new DRStatsGroupRecordType(dddHelper);
      }
      throw new Exception("Unknown group type: " + groupType.ToString());
    }
  }
  #endregion //DRStatsGroupBase class

  #region DRStatsGroupTUInstance
  class DRStatsGroupTUInstance : DRStatsGroupBase
  {
    protected int tuInstIDColIndx;

    public DRStatsGroupTUInstance(DDDHelper dddHelper)
      : base(dddHelper)
    { }

    public override void AddColumnsToQuery(DRQueryHelper query)
    {
      query.AddStatGroupColumn("DR.TUInstanceID");
      tuInstIDColIndx = query.AddStatSelectColumn("DR.TUInstanceID");
    }

    public override string GetIDFromRow(SqlDataReader reader)
    {
      return reader.GetGuid(tuInstIDColIndx).ToString();
    }

    public override string GetTitleFromRow(SqlDataReader reader)
    {
      Guid instID = reader.GetGuid(tuInstIDColIndx);
      return dddHelper.GetTUInstance(instID).TUName;
    }
  }
  #endregion //DRStatsGroupTUInstance

  #region DRStatsGroupTUType
  class DRStatsGroupTUType : DRStatsGroupBase
  {
    protected int tuTypeIDColIndx;

    public DRStatsGroupTUType(DDDHelper dddHelper)
      : base(dddHelper)
    { }

    public override void AddColumnsToQuery(DRQueryHelper query)
    {
      query.AddStatGroupColumn("TI.TUTypeID");
      tuTypeIDColIndx = query.AddStatSelectColumn("TI.TUTypeID");
    }

    public override string GetIDFromRow(SqlDataReader reader)
    {
      return reader.GetGuid(tuTypeIDColIndx).ToString();
    }

    public override string GetTitleFromRow(SqlDataReader reader)
    {
      Guid instID = reader.GetGuid(tuTypeIDColIndx);
      return dddHelper.GetTUType(instID).TypeName;
    }
  }
  #endregion //DRStatsGroupTUType

  #region DRStatsGroupFaultCode
  class DRStatsGroupFaultCode : DRStatsGroupBase
  {
    protected int dddVerIDColIndx;
    protected int recDefIDColIndx;

    public DRStatsGroupFaultCode(DDDHelper dddHelper)
      : base(dddHelper)
    { }

    public override void AddColumnsToQuery(DRQueryHelper query)
    {
      query.AddStatGroupColumn("DR.DDDVersionID");
      query.AddStatGroupColumn("DR.RecordDefID");
      dddVerIDColIndx = query.AddStatSelectColumn("DR.DDDVersionID");
      recDefIDColIndx = query.AddStatSelectColumn("DR.RecordDefID");
    }

    public override string GetIDFromRow(SqlDataReader reader)
    {
      DDDVersion dddVer = dddHelper.GetVersion(reader.GetGuid(dddVerIDColIndx));
      DDDRecord dddRec = dddVer.GetRecord(reader.GetInt32(recDefIDColIndx));
      return dddVer.UserVersion + "_" + dddRec.Name;
    }

    public override string GetTitleFromRow(SqlDataReader reader)
    {
      DDDVersion dddVer = dddHelper.GetVersion(reader.GetGuid(dddVerIDColIndx));
      DDDRecord dddRec = dddVer.GetRecord(reader.GetInt32(recDefIDColIndx));
      String title = String.Empty;
      if (dddRec.GetType() == typeof(DDDEventRecord))
        title = ((DDDEventRecord)dddRec).FaultCode.ToString() + ": ";
      title += dddRec.Title;
      return title;
    }
  }
  #endregion //DRStatsGroupFaultCode

  #region DRStatsGroupSource
  class DRStatsGroupSource : DRStatsGroupBase
  {
    protected int sourceColIndx;

    public DRStatsGroupSource(DDDHelper dddHelper)
      : base(dddHelper)
    { }

    public override void AddColumnsToQuery(DRQueryHelper query)
    {
      query.AddStatGroupColumn("DR.Source");
      sourceColIndx = query.AddStatSelectColumn("DR.Source");
    }

    public override string GetIDFromRow(SqlDataReader reader)
    {
      if (reader.IsDBNull(sourceColIndx))
        return String.Empty;
      else
        return reader.GetString(sourceColIndx).Trim();
    }

    public override string GetTitleFromRow(SqlDataReader reader)
    {
      return GetIDFromRow(reader);
    }
  }
  #endregion //DRStatsGroupSource

  #region DRStatsGroupHour
  class DRStatsGroupHour : DRStatsGroupBase
  {
    protected int timeColIndx;

    public DRStatsGroupHour(DDDHelper dddHelper)
      : base(dddHelper)
    { }

    public override void AddColumnsToQuery(DRQueryHelper query)
    {
      query.AddStatGroupColumn("YEAR(DR.StartTime)*1000000+MONTH(DR.StartTime)*10000+DAY(DR.StartTime)*100+DATEPART(hh, DR.StartTime)");
      timeColIndx = query.AddStatSelectColumn("YEAR(DR.StartTime)*1000000+MONTH(DR.StartTime)*10000+DAY(DR.StartTime)*100+DATEPART(hh, DR.StartTime) Hour");
    }

    public override string GetIDFromRow(SqlDataReader reader)
    {
      int val = reader.GetInt32(timeColIndx);
      return val.ToString();
    }

    public override string GetTitleFromRow(SqlDataReader reader)
    {
      int val = reader.GetInt32(timeColIndx);
      String title = (val / 1000000).ToString("0000") + "-";
      val = val % 1000000;
      title += (val / 10000).ToString("00") + "-";
      val = val % 10000;
      title += (val / 100).ToString("00") + " ";
      val = val % 100;
      title += val.ToString("00") + ":00";
      return title;
    }
  }
  #endregion //DRStatsGroupHour

  #region DRStatsGroupDay
  class DRStatsGroupDay : DRStatsGroupBase
  {
    protected int timeColIndx;

    public DRStatsGroupDay(DDDHelper dddHelper)
      : base(dddHelper)
    { }

    public override void AddColumnsToQuery(DRQueryHelper query)
    {
      query.AddStatGroupColumn("YEAR(DR.StartTime)*10000+MONTH(DR.StartTime)*100+DAY(DR.StartTime)");
      timeColIndx = query.AddStatSelectColumn("YEAR(DR.StartTime)*10000+MONTH(DR.StartTime)*100+DAY(DR.StartTime) Day");
    }

    public override string GetIDFromRow(SqlDataReader reader)
    {
      int val = reader.GetInt32(timeColIndx);
      return val.ToString();
    }

    public override string GetTitleFromRow(SqlDataReader reader)
    {
      int val = reader.GetInt32(timeColIndx);
      String title = (val / 10000).ToString("0000") + "-";
      val = val % 10000;
      title += (val / 100).ToString("00") + "-";
      val = val % 100;
      title += val.ToString("00");
      return title;
    }
  }
  #endregion //DRStatsGroupDay

  #region DRStatsGroupMonth
  class DRStatsGroupMonth : DRStatsGroupBase
  {
    protected int timeColIndx;

    public DRStatsGroupMonth(DDDHelper dddHelper)
      : base(dddHelper)
    { }

    public override void AddColumnsToQuery(DRQueryHelper query)
    {
      query.AddStatGroupColumn("YEAR(DR.StartTime)*100+MONTH(DR.StartTime)");
      timeColIndx = query.AddStatSelectColumn("YEAR(DR.StartTime)*100+MONTH(DR.StartTime) Month");
    }

    public override string GetIDFromRow(SqlDataReader reader)
    {
      int val = reader.GetInt32(timeColIndx);
      return val.ToString();
    }

    public override string GetTitleFromRow(SqlDataReader reader)
    {
      int val = reader.GetInt32(timeColIndx);
      String title = (val / 100).ToString("0000") + "-";
      val = val % 100;
      title += val.ToString("00");
      return title;
    }
  }
  #endregion //DRStatsGroupMonth

  #region DRStatsGroupYear
  class DRStatsGroupYear : DRStatsGroupBase
  {
    protected int timeColIndx;

    public DRStatsGroupYear(DDDHelper dddHelper)
      : base(dddHelper)
    { }

    public override void AddColumnsToQuery(DRQueryHelper query)
    {
      query.AddStatGroupColumn("YEAR(DR.StartTime)");
      timeColIndx = query.AddStatSelectColumn("YEAR(DR.StartTime) Year");
    }

    public override string GetIDFromRow(SqlDataReader reader)
    {
      int val = reader.GetInt32(timeColIndx);
      return val.ToString();
    }

    public override string GetTitleFromRow(SqlDataReader reader)
    {
      int val = reader.GetInt32(timeColIndx);
      return val.ToString("0000");
    }
  }
  #endregion //DRStatsGroupYear

  #region DRStatsGroupTrain
  class DRStatsGroupTrain : DRStatsGroupBase
  {
    protected int hierItemIDColIndx;

    public DRStatsGroupTrain(DDDHelper dddHelper)
      : base(dddHelper)
    { }

    public override void AddColumnsToQuery(DRQueryHelper query)
    {
      query.AddStatGroupColumn("DR.HierarchyItemID");
      hierItemIDColIndx = query.AddStatSelectColumn("DR.HierarchyItemID TrainID");
    }

    public override string GetIDFromRow(SqlDataReader reader)
    {
      int hierItemID;
      if (reader.IsDBNull(hierItemIDColIndx))
        return String.Empty;
      else
        hierItemID = reader.GetInt32(hierItemIDColIndx);
      FleetHierarchy hierarchy = dddHelper.HierarchyHelper.ObtainCurrentHierarchy();
      FleetHierarchyItem hierItem = hierarchy.GetHierarchyItem(hierItemID);
      FHTrain train = hierItem.GetTrain();
      if (train != null)
        return train.Name;
      return hierItem.Name;
    }

    public override string GetTitleFromRow(SqlDataReader reader)
    {
      return GetIDFromRow(reader);
    }
  }
  #endregion //DRStatsGroupTrain

  #region DRStatsGroupVehicle
  class DRStatsGroupVehicle : DRStatsGroupBase
  {
    protected int hierItemIDColIndx;

    public DRStatsGroupVehicle(DDDHelper dddHelper)
      : base(dddHelper)
    { }

    public override void AddColumnsToQuery(DRQueryHelper query)
    {
      query.AddStatGroupColumn("DR.HierarchyItemID");
      hierItemIDColIndx = query.AddStatSelectColumn("DR.HierarchyItemID VehicleID");
    }

    public override string GetIDFromRow(SqlDataReader reader)
    {
      int hierItemID;
      if (reader.IsDBNull(hierItemIDColIndx))
        return String.Empty;
      else
        hierItemID = reader.GetInt32(hierItemIDColIndx);
      FleetHierarchy hierarchy = dddHelper.HierarchyHelper.ObtainCurrentHierarchy();
      FleetHierarchyItem hierItem = hierarchy.GetHierarchyItem(hierItemID);
      FHVehicle vehicle = hierItem.GetVehicle();
      if (vehicle != null)
        return vehicle.Name;
      return hierItem.Name;
    }

    public override string GetTitleFromRow(SqlDataReader reader)
    {
      return GetIDFromRow(reader);
    }
  }
  #endregion //DRStatsGroupVehicle

  #region DRStatsGroupVehicleNumber
  class DRStatsGroupVehicleNumber : DRStatsGroupBase
  {
    protected int sourceColIndx;

    public DRStatsGroupVehicleNumber(DDDHelper dddHelper)
      : base(dddHelper)
    { }

    public override void AddColumnsToQuery(DRQueryHelper query)
    {
      query.AddStatGroupColumn("DR.VehicleNumber");
      sourceColIndx = query.AddStatSelectColumn("DR.VehicleNumber");
    }

    public override string GetIDFromRow(SqlDataReader reader)
    {
      if (reader.IsDBNull(sourceColIndx))
        return String.Empty;
      else
        return reader.GetInt16(sourceColIndx).ToString();
    }

    public override string GetTitleFromRow(SqlDataReader reader)
    {
      return GetIDFromRow(reader);
    }
  }
  #endregion //DRStatsGroupVehicleNumber

  #region DRStatsGroupDeviceCode
  class DRStatsGroupDeviceCode : DRStatsGroupBase
  {
    protected int sourceColIndx;

    public DRStatsGroupDeviceCode(DDDHelper dddHelper)
      : base(dddHelper)
    { }

    public override void AddColumnsToQuery(DRQueryHelper query)
    {
      query.AddStatGroupColumn("DR.DeviceCode");
      sourceColIndx = query.AddStatSelectColumn("DR.DeviceCode");
    }

    public override string GetIDFromRow(SqlDataReader reader)
    {
      if (reader.IsDBNull(sourceColIndx))
        return String.Empty;
      else
        return reader.GetInt16(sourceColIndx).ToString();
    }

    public override string GetTitleFromRow(SqlDataReader reader)
    {
      return GetIDFromRow(reader);
    }
  }
  #endregion //DRStatsGroupDeviceCode

  #region DRStatsGroupFaultSeverity
  class DRStatsGroupFaultSeverity : DRStatsGroupBase
  {
    protected int sourceColIndx;

    public DRStatsGroupFaultSeverity(DDDHelper dddHelper)
      : base(dddHelper)
    { }

    public override void AddColumnsToQuery(DRQueryHelper query)
    {
      query.AddStatGroupColumn("RD.SeverityID");
      sourceColIndx = query.AddStatSelectColumn("RD.SeverityID");
    }

    public override string GetIDFromRow(SqlDataReader reader)
    {
      if (reader.IsDBNull(sourceColIndx))
        return String.Empty;
      else
        return reader.GetInt32(sourceColIndx).ToString();
    }

    public override string GetTitleFromRow(SqlDataReader reader)
    {
      if (reader.IsDBNull(sourceColIndx))
        return String.Empty;
      else
        return ((SeverityType)reader.GetInt32(sourceColIndx)).ToString();
    }
  }
  #endregion //DRStatsGroupFaultSeverity

  #region DRStatsGroupFaultSubSeverity
  class DRStatsGroupFaultSubSeverity : DRStatsGroupBase
  {
    protected int sourceColIndx;

    public DRStatsGroupFaultSubSeverity(DDDHelper dddHelper)
      : base(dddHelper)
    { }

    public override void AddColumnsToQuery(DRQueryHelper query)
    {
      query.AddStatGroupColumn("RD.SubSeverity");
      sourceColIndx = query.AddStatSelectColumn("RD.SubSeverity");
    }

    public override string GetIDFromRow(SqlDataReader reader)
    {
      if (reader.IsDBNull(sourceColIndx))
        return String.Empty;
      else
        return reader.GetInt32(sourceColIndx).ToString();
    }

    public override string GetTitleFromRow(SqlDataReader reader)
    {
      return GetIDFromRow(reader);
    }
  }
  #endregion //DRStatsGroupFaultSubSeverity

  #region DRStatsGroupTrcSnpCode
  class DRStatsGroupTrcSnpCode : DRStatsGroupBase
  {
    protected int dddVerIDColIndx;
    protected int recDefIDColIndx;

    public DRStatsGroupTrcSnpCode(DDDHelper dddHelper)
      : base(dddHelper)
    { }

    public override void AddColumnsToQuery(DRQueryHelper query)
    {
      query.AddStatGroupColumn("DR.DDDVersionID");
      query.AddStatGroupColumn("DR.RecordDefID");
      dddVerIDColIndx = query.AddStatSelectColumn("DR.DDDVersionID");
      recDefIDColIndx = query.AddStatSelectColumn("DR.RecordDefID");
    }

    public override string GetIDFromRow(SqlDataReader reader)
    {
      DDDVersion dddVer = dddHelper.GetVersion(reader.GetGuid(dddVerIDColIndx));
      DDDRecord dddRec = dddVer.GetRecord(reader.GetInt32(recDefIDColIndx));
      return dddVer.UserVersion + "_" + dddRec.Name;
    }

    public override string GetTitleFromRow(SqlDataReader reader)
    {
      DDDVersion dddVer = dddHelper.GetVersion(reader.GetGuid(dddVerIDColIndx));
      DDDRecord dddRec = dddVer.GetRecord(reader.GetInt32(recDefIDColIndx));
      String title = String.Empty;
      if (dddRec.GetType() == typeof(DDDTraceRecord))
        title = ((DDDTraceRecord)dddRec).Code.ToString() + ": ";
      else if (dddRec.GetType() == typeof(DDDSnapRecord))
        title = ((DDDSnapRecord)dddRec).Code.ToString() + ": ";
      title += dddRec.Title;
      return title;
    }
  }
  #endregion //DRStatsGroupTrcSnpCode

  #region DRStatsGroupRecordType
  class DRStatsGroupRecordType : DRStatsGroupBase
  {
    protected int sourceColIndx;

    public DRStatsGroupRecordType(DDDHelper dddHelper)
      : base(dddHelper)
    { }

    public override void AddColumnsToQuery(DRQueryHelper query)
    {
      query.AddStatGroupColumn("RD.RecordTypeID");
      sourceColIndx = query.AddStatSelectColumn("RD.RecordTypeID");
    }

    public override string GetIDFromRow(SqlDataReader reader)
    {
      if (reader.IsDBNull(sourceColIndx))
        return String.Empty;
      else
        return reader.GetInt32(sourceColIndx).ToString();
    }

    public override string GetTitleFromRow(SqlDataReader reader)
    {
      if (!reader.IsDBNull(sourceColIndx))
      {
        int recTypeID = reader.GetInt32(sourceColIndx);
        switch(recTypeID)
        {
          case 1:
            return "event";
          case 2:
            return "trace";
          case 3:
            return "snap";
        }
      }
      return String.Empty;
    }
  }
  #endregion //DRStatsGroupRecordType
}
