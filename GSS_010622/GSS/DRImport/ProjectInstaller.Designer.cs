namespace DRImport
{
  partial class ProjectInstaller
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.DRImportProcessInstaler = new System.ServiceProcess.ServiceProcessInstaller();
      this.DRImportServiceInstaller = new System.ServiceProcess.ServiceInstaller();
      // 
      // DRImportProcessInstaler
      // 
      this.DRImportProcessInstaler.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
      this.DRImportProcessInstaler.Password = null;
      this.DRImportProcessInstaler.Username = null;
      // 
      // DRImportServiceInstaller
      // 
      this.DRImportServiceInstaller.Description = "Imoprts Telediagnostica diagnostic records into CDDB";
      this.DRImportServiceInstaller.DisplayName = "TD DR Import";
      this.DRImportServiceInstaller.ServiceName = "TDDRImport";
      // 
      // ProjectInstaller
      // 
      this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.DRImportProcessInstaler,
            this.DRImportServiceInstaller});

    }

    #endregion

    private System.ServiceProcess.ServiceProcessInstaller DRImportProcessInstaler;
    private System.ServiceProcess.ServiceInstaller DRImportServiceInstaller;
  }
}