using System;
using System.Collections.Generic;
using System.Text;
using DDDObjects;

namespace DRQueries
{
  #region Public enums
  /// <summary>
  /// Name of column, on which results are sorted
  /// </summary>
  public enum eSortColumn
  {
    /// <summary>
    /// Sort by FaultCode
    /// </summary>
    FaultCode = 1,
    /// <summary>
    /// Sort by RecordTitle
    /// </summary>
    RecordTitle = 2,
    /// <summary>
    /// Sort by Source
    /// </summary>
    Source = 3,
    /// <summary>
    /// Sort by StartTime
    /// </summary>
    StartTime = 4,
    /// <summary>
    /// Sort by EndTime
    /// </summary>
    EndTime = 5,
    /// <summary>
    /// Sort by LastModified
    /// </summary>
    LastModified = 6,
    /// <summary>
    /// Sort by FaultCode
    /// </summary>
    ImportTime = 7,
    /// <summary>
    /// Sort by FaultCode
    /// </summary>
    AcknowledgeTime = 8,
    /// <summary>
    /// Sort by VehicleNumber
    /// </summary>
    VehicleNumber = 9,
    /// <summary>
    /// Sort by DeviceCode
    /// </summary>
    DeviceCode = 10,
    /// <summary>
    /// Sort by EventSeverity
    /// </summary>
    EventSeverity = 11,
    /// <summary>
    /// Sort by EventSubSeverity
    /// </summary>
    EventSubSeverity = 12,
    /// <summary>
    /// Sort by TrcSnpCode
    /// </summary>
    TrcSnpCode = 13
  }
  /// <summary>
  /// Sort order of the query result
  /// </summary>
  public enum eSortOrder
  {
    /// <summary>
    /// Sort Ascending
    /// </summary>
    Ascending = 1,
    /// <summary>
    /// Sort Descending
    /// </summary>
    Descending = 2
  }
  #endregion //#region Public enums

  /// <summary>
  /// Class provides methods used for building queries 
  /// for diagnostic records from diagnostic database.
  /// Queries may be created with various filter parameters.
  /// </summary>
  public class DRQueryHelper
  {
    #region Private statistical query parameters
    //Column names or expressions added to select list of statistical query
    private List<String> statSelectCols = new List<string>();
    //Column names or expressions used to group the result of statistical query
    private List<String> statGroupCols = new List<string>();
    #endregion

    #region Public query parameters
    /// <summary>
    /// List of TUInstance IDs separated by ListSeparators
    /// </summary>
    public String TUInstanceIDs = String.Empty;
    /// <summary>
    /// List of hierarchy item IDs separated by ListSeparators
    /// </summary>
    public String HierarchyItemIDs = String.Empty;
    /// <summary>
    /// Minimum inclusive start time value or null
    /// </summary>
    public DateTime? StartTimeFrom = null;
    /// <summary>
    /// Maximum inclusive start time value or null
    /// </summary>
    public DateTime? StartTimeTo = null;
    /// <summary>
    /// Flags indicating desired record type (evt, trc, snp)
    /// </summary>
    public eRecordCategory? RecordTypes = null;
    /// <summary>
    /// List of record sources separated by ListSeparators
    /// </summary>
    public String Sources = String.Empty;
    /// <summary>
    /// List of fault codes separated by ListSeparators
    /// </summary>
    public String FaultCodes = String.Empty;
    /// <summary>
    /// List of title keywords separated by ListSeparators
    /// </summary>
    public String TitleKeywords = String.Empty;
    /// <summary>
    /// Match all title keywords in each selected record
    /// </summary>
    public bool MatchAllKeywords = false;
    /// <summary>
    /// List of event severities separated by ListSeparators
    /// </summary>
    public String Severities = String.Empty;
    /// <summary>
    /// List of event sub severities separated by ListSeparators
    /// </summary>
    public String SubSeverities = String.Empty;
    /// <summary>
    /// List of vehicle numbers separated by ListSeparators
    /// </summary>
    public String VehicleNumbers = String.Empty;
    /// <summary>
    /// List of trace or snap codes separated by ListSeparators
    /// </summary>
    public String TrcSnpCodes = String.Empty;

    /// <summary>
    /// Maximum number of selected rows
    /// </summary>
    public int? MaximumRows = null;
    /// <summary>
    /// Column on which result set is sorted
    /// </summary>
    public eSortColumn? SortColumn = null;
    /// <summary>
    /// Sort order of the result set
    /// </summary>
    public eSortOrder SortOrder = eSortOrder.Ascending;
    #endregion

    #region Public web query properties
    /// <summary>
    /// Index of first returned row (query result sorted by selected column)
    /// </summary>
    public int? StartRowIndex = null;
    #endregion

    #region Public statistical query properties
    /// <summary>
    /// Holds name of the file for StatSelectorFile
    /// </summary>
    public String FileName; 
    #endregion

    #region Public properties
    /// <summary>
    /// Array of strings used as separators in lists
    /// </summary>
    public String[] ListSeparators = new String[] {",", " ", ", ", ";"};
    #endregion

    #region Public methods
    /// <summary>
    /// Method returns query string which selects total number of records
    /// satisfying query parameters.
    /// </summary>
    /// <returns>Query string</returns>
    public String GetQuery_Count()
    {
      String query;
      query = GetSELECTPart_COUNT();
      query = JoinQUERYParts(query, GetFROMPart());
      query = JoinQUERYParts(query, GetWHEREPart());
      return query;
    }
    /// <summary>
    /// Method returns query string which selects records
    /// satisfying query parameters. Total number of selected
    /// records is limited by MaximumRows parameter (if defined).
    /// </summary>
    /// <returns></returns>
    public String GetQuery_RecordsApp()
    {
      String query;
      query = GetSELECTPart_RecordsApp();
      query = JoinQUERYParts(query, GetFROMPart());
      query = JoinQUERYParts(query, GetWHEREPart());
      query = JoinQUERYParts(query, GetORDERBYPart());
      return query;
    }
    /// <summary>
    /// Returns query string which selects records satysfying query parameters.
    /// Query is intended for web applications - it uses paging mechanism.
    /// All records satysfying query parameters are sorted in givem sort order. 
    /// First returned record is number StartRowIndex from the beginning of sorted 
    /// record set, maximum number of returned records is MaximumRows.
    /// Query does NOT select referential attributes.
    /// </summary>
    /// <returns>Query string</returns>
    public String GetQuery_RecordsPaging()
    {
      String query;
      //Obtain query which creates and fills tmp table
      query = GetQuery_PagingTmpTable();
      query += "\n";
      //Append query selecting records based on tmp table
      //Add select with columns for web query
      query = JoinQUERYParts(query, GetSELECTPart_RecordsPaging());
      //Add from part
      String fromPart = "FROM\n";
      fromPart += "  @TempRecIDs AS TMP\n";
      fromPart += "  INNER JOIN DiagnosticRecords DR\n";
      fromPart += "    ON (DR.TUInstanceID = TMP.TUInstanceID  AND DR.RecordInstID = TMP.RecordInstID  AND DR.RecordDefID = TMP.RecordDefID)\n";
      fromPart += "WHERE\n";
      fromPart += String.Format("  TMP.ID >= {0}\n", StartRowIndex.Value);
      fromPart += "ORDER BY\n";
      fromPart += "  TMP.ID\n";
      query = JoinQUERYParts(query, fromPart);
      //Return the query
      return query;
    }
    /// <summary>
    /// Returns query string which deletes records from database based on filter criteria,
    /// together with values of environmental attributes.
    /// Number of deleted records is limited to MaximumRows.
    /// </summary>
    /// <returns>Query string</returns>
    public String GetQuery_DeleteRecords()
    {
      String query;
      StartRowIndex = 0;
      //Obtain query which creates and fills tmp table
      query = GetQuery_PagingTmpTable();
      query += "\n";
      //Append transaction start
      query += "BEGIN TRANSACTION\n";
      //Delete values of Referential attributes
      query += "DELETE FROM RefAttributesValues\n";
      query += "FROM\n";
      query += "  RefAttributesValues RAV\n";
      query += "  INNER JOIN @TempRecIDs TMP\n";
      query += "    ON (RAV.TUInstanceID = TMP.TUInstanceID AND RAV.RecordInstID = TMP.RecordInstID AND RAV.RecordDefID = TMP.RecordDefID)\n";
      //Delete diagnostic records
      query += "DELETE FROM DiagnosticRecords\n";
      query += "FROM\n";
      query += "  DiagnosticRecords DR\n";
      query += "  INNER JOIN @TempRecIDs TMP\n";
      query += "    ON (DR.TUInstanceID = TMP.TUInstanceID AND DR.RecordInstID = TMP.RecordInstID AND DR.RecordDefID = TMP.RecordDefID)\n";
      //Commit transaction
      query += "COMMIT TRANSACTION\n";
      return query;
    }
    /// <summary>
    /// Returns query which selects records statistics for web application.
    /// Selec columns are fixed, only WHERE part of the query is constructed dynamically.
    /// </summary>
    /// <returns>Query string</returns>
    public String GetQuery_WebStatistics()
    {
      String query;
      //Create fixed select part
      query = "SELECT\n";
      query += "  DR.TUInstanceID [TUInstanceID], DR.DDDVersionID	[DDDVersionID], DR.RecordDefID	[RecordDefID],\n";
      query += "  DR.VehicleNumber [VehicleNumber], DR.Source [Source], DR.HierarchyItemID [HierarchyItemID], MIN(DR.StartTime)	[FirstTime], MAX(DR.StartTime) [LastTime],\n";
      query += "  COUNT(DR.RecordInstID) [RecCount], MIN(DR.RecordInstID) [FirstID], MAX(DR.RecordInstID) [LastID]\n";
      //Append dynamic from and where parts
      query = JoinQUERYParts(query, GetFROMPart());
      query = JoinQUERYParts(query, GetWHEREPart());
      //Append fixed GROUP BY and ORDER BY part
      query += "\nGROUP BY\n";
      query += "  DR.TUInstanceID, DR.HierarchyItemID, DR.DDDVersionID, DR.VehicleNumber, DR.Source, DR.RecordDefID\n";
      query += "ORDER BY\n";
      query += "  HierarchyItemID, VehicleNumber, Source, RecordDefID\n";
      //Return query string
      return query;
    }
    #endregion

    #region Public methods for statistical queries
    /// <summary>
    /// Adds column to select part of the statistical query
    /// </summary>
    /// <param name="column">Column name or expression</param>
    /// <returns>index of column in result</returns>
    public int AddStatSelectColumn(String column)
    {
      statSelectCols.Add(column);
      return statSelectCols.Count; //There is always count(*) as first column
    }

    /// <summary>
    /// Adds column to group by part of the statistical query
    /// </summary>
    /// <param name="column">Column name or expression</param>
    /// <returns>index of column in the group by clause</returns>
    public int AddStatGroupColumn(String column)
    {
      statGroupCols.Add(column);
      return statGroupCols.Count - 1;
    }

    /// <summary>
    /// Method returns query string which selects statistics of diagnostic records.
    /// Only records satisfying query parameters are included into the statistics.
    /// Records are grouped according to defined qroup columns.
    /// Count for each group is selected.
    /// </summary>
    /// <returns>Query string</returns>
    public String GetQuery_Statistical()
    {
      String query;
      query = GetSELECTPart_StatisticalQuery();
      query = JoinQUERYParts(query, GetFROMPart());
      query = JoinQUERYParts(query, GetWHEREPart());
      query = JoinQUERYParts(query, GetGROUPBYPart_StatisticalQuery());
      return query;
    }
    #endregion

    #region Helper methods
    /// <summary>
    /// Returns query which creates and fills tmp table for paging query.
    /// </summary>
    /// <returns>Query string</returns>
    private String GetQuery_PagingTmpTable()
    {
      StringBuilder query = new StringBuilder();
      //Add nocount directive
      query.AppendLine("SET NOCOUNT ON");
      //Append declaration of TMP table
      query.AppendLine("DECLARE @TempRecIDs TABLE (ID int IDENTITY, TUInstanceID uniqueidentifier, RecordInstID bigint, RecordDefID int)");
      //Append INSERT statement
      query.AppendLine("INSERT INTO @TempRecIDs (TUInstanceID, RecordInstID, RecordDefID)");
      //Append SELECT with row number limitations (if defined)
      if (StartRowIndex.HasValue && MaximumRows.HasValue)
        query.AppendLine(String.Format("SELECT TOP ({0})", StartRowIndex.Value + MaximumRows.Value));
      else
        query.AppendLine("SELECT");
      //Append columns to be selected
      query.AppendLine("  DR.TUInstanceID, DR.RecordInstID, DR.RecordDefID");
      //Append FROM, WHERE and ORDER BY parts to the SELECT query
      query.AppendLine(GetFROMPart());
      query.AppendLine(GetWHEREPart());
      query.AppendLine(GetORDERBYPart());
      //End nocount directive
      query.AppendLine("SET NOCOUNT OFF");
      //Return resulting query
      return query.ToString();
    }
    /// <summary>
    /// Returns SELECT part of the query, 
    /// which selects total number of records satisfying filter criteria.
    /// </summary>
    /// <returns>SELECT part of the query</returns>
    private String GetSELECTPart_COUNT()
    {
      return "SELECT\n  COUNT(*)";
    }

    /// <summary>
    /// Returns SELECT part of the query, which select diagnostic records for win application.
    /// Number of returned rows is limited by MaximumRows parameter (if defined)
    /// </summary>
    /// <returns>SELECT part of the query</returns>
    private String GetSELECTPart_RecordsApp()
    {
      String selPart = String.Empty;
      //Check if number of rows shall be limited
      if (MaximumRows.HasValue)
        selPart = String.Format("SELECT TOP({0})\n", MaximumRows.Value);
      else
        selPart = "SELECT\n";
      //Add columns to be selected
      selPart += "  DR.TUInstanceID, DR.RecordInstID, DR.DDDVersionID, DR.RecordDefID,\n";
      selPart += "  DR.StartTime, DR.EndTime, DR.LastModified, 0 [ParseStatusID],\n";
      selPart += "  DR.BinaryData, DR.BigEndianData, DR.Source, DR.HierarchyItemID,\n";
      selPart += "  DR.ImportTime, DR.AcknowledgeTime, DR.VehicleNumber, DR.DeviceCode";
      return selPart;
    }

    /// <summary>
    /// Returns SELECT part of the query, which selects diagnostic records for web application.
    /// </summary>
    /// <returns>SELECT part of the query</returns>
    private String GetSELECTPart_RecordsPaging()
    {
      String selPart = String.Empty;
      selPart = "SELECT\n";
      //Add columns to be selected
      selPart += "  DR.TUInstanceID, DR.RecordInstID, DR.DDDVersionID, DR.RecordDefID,\n";
      selPart += "  DR.StartTime, DR.EndTime, DR.LastModified, DR.Source, DR.HierarchyItemID,\n";
      selPart += "  DR.ImportTime, DR.AcknowledgeTime, DR.VehicleNumber, DR.DeviceCode,\n";
      selPart += "  DR.BigEndianData, DR.BinaryData";
      return selPart;
    }

    /// <summary>
    /// Returns SELECT part of the statistical query
    /// Select always contains COUNT(*) as first column and than additiona columns 
    /// according to statSelectCols list
    /// </summary>
    /// <returns>SELECT part of the query</returns>
    private String GetSELECTPart_StatisticalQuery()
    {
      String selPart = String.Empty;
      //Add fixed part
      selPart = "SELECT\n";
      selPart += "  COUNT(*) AS RecCount";
      //Add columns according to the list
      foreach (String col  in statSelectCols)
        selPart += ",\n  " + col;
      return selPart;
    }

    /// <summary>
    /// Returns GROUP BY part of the statistical query.
    /// GROUP BY part contains columns according to statGroupCols list
    /// </summary>
    /// <returns>GROUP BY part of the query</returns>
    private String GetGROUPBYPart_StatisticalQuery()
    {
      String groupPart = String.Empty;
      //Check if any group columns exist
      if (statGroupCols.Count == 0)
        return groupPart; //Return empty string
      //Add fixed part
      groupPart = "GROUP BY\n  ";
      //Add columns according to the list
      groupPart += String.Join(", ", statGroupCols.ToArray());
      //Return group part
      return groupPart;
    }

    /// <summary>
    /// Get WHERE part of the query
    /// </summary>
    /// <returns>WHERE part</returns>
    private String GetWHEREPart()
    {
      #region Declarations
      String wherePart = String.Empty;
      String literal = String.Empty;
      String[] parts = null;
      #endregion

      #region TUInstanceIDs
      if (TUInstanceIDs != String.Empty)
      { //list of TUInstanceIDs
        parts = TUInstanceIDs.Split(ListSeparators, StringSplitOptions.RemoveEmptyEntries);
        literal = "DR.TUInstanceID IN ('" + String.Join("', '", parts) + "')";
        wherePart = AppendWHERELiteral(wherePart, literal);
      }
      #endregion
      #region HierarchyItemIDs
      if (HierarchyItemIDs != String.Empty)
      { //list of HierarchyItemIDs
        parts = HierarchyItemIDs.Split(ListSeparators, StringSplitOptions.RemoveEmptyEntries);
        literal = "DR.HierarchyItemID IN (" + String.Join(", ", parts) + ")";
        wherePart = AppendWHERELiteral(wherePart, literal);
      }
      #endregion
      #region StartTimeFrom
      if (StartTimeFrom.HasValue)
      { 
        literal = "DR.StartTime >= '" + StartTimeFrom.Value.ToString(@"yyyy-MM-dd HH\:mm\:ss") + "'";
        wherePart = AppendWHERELiteral(wherePart, literal);
      }
      #endregion
      #region StartTimeTo
      if (StartTimeTo.HasValue)
      {
        literal = "DR.StartTime <= '" + StartTimeTo.Value.ToString(@"yyyy-MM-dd HH\:mm\:ss") + "'";
        wherePart = AppendWHERELiteral(wherePart, literal);
      }
      #endregion
      #region RecordType
      if (RecordTypes.HasValue)
      {
        literal = String.Empty;
        if ((RecordTypes.Value & eRecordCategory.Event) > 0)
          literal = JoinOR(literal, "RD.RecordTypeID = 1", String.Empty);
        if ((RecordTypes.Value & eRecordCategory.Trace) > 0)
          literal = JoinOR(literal, "RD.RecordTypeID = 2", String.Empty);
        if ((RecordTypes.Value & eRecordCategory.Snap) > 0)
          literal = JoinOR(literal, "RD.RecordTypeID = 3", String.Empty);
        wherePart = AppendWHERELiteral(wherePart, literal);
      }
      #endregion
      #region Sources
      if (Sources != String.Empty)
      { //list of Sources
        parts = Sources.Split(ListSeparators, StringSplitOptions.RemoveEmptyEntries);
        literal = String.Empty;
        foreach (String part in parts)
          literal = JoinOR(literal, String.Format("DR.Source LIKE '%{0}%'", part), String.Empty);
        wherePart = AppendWHERELiteral(wherePart, literal);
      }
      #endregion
      #region FaultCodes
      if (FaultCodes != String.Empty)
      { //list of FaultCodes
        parts = FaultCodes.Split(ListSeparators, StringSplitOptions.RemoveEmptyEntries);
        literal = "RD.FaultCode IN (" + String.Join(", ", parts) + ")";
        wherePart = AppendWHERELiteral(wherePart, literal);
      }
      #endregion
      #region TitleKeywords
      if (TitleKeywords != String.Empty)
      { //list of TitleKeywords
        parts = TitleKeywords.Split(ListSeparators, StringSplitOptions.RemoveEmptyEntries);
        literal = String.Empty;
        foreach (String part in parts)
        {
          if (MatchAllKeywords)
            literal = JoinAND(literal, String.Format("RI.Text LIKE '%{0}%'", part), String.Empty);
          else
            literal = JoinOR(literal, String.Format("RI.Text LIKE '%{0}%'", part), String.Empty);
        }
        wherePart = AppendWHERELiteral(wherePart, literal);
      }
      #endregion
      #region Severities
      if (Severities != String.Empty)
      { //List of record severities
        parts = Severities.Split(ListSeparators, StringSplitOptions.RemoveEmptyEntries);
        literal = "RD.SeverityID IN (" + String.Join(", ", parts) + ")";
        wherePart = AppendWHERELiteral(wherePart, literal);
      }
      #endregion 
      #region SubSeverities
      if (SubSeverities != String.Empty)
      { //list of SubSeverities
        parts = SubSeverities.Split(ListSeparators, StringSplitOptions.RemoveEmptyEntries);
        literal = "RD.SubSeverity IN (" + String.Join(", ", parts) + ")";
        wherePart = AppendWHERELiteral(wherePart, literal);
      }
      #endregion
      #region VehicleNumbers
      if (VehicleNumbers != String.Empty)
      { //list of VehicleNumbers
        parts = VehicleNumbers.Split(ListSeparators, StringSplitOptions.RemoveEmptyEntries);
        literal = "DR.VehicleNumber IN (" + String.Join(", ", parts) + ")";
        wherePart = AppendWHERELiteral(wherePart, literal);
      }
      #endregion
      #region TrcSnpCodes
      if (TrcSnpCodes != String.Empty)
      { //list of TrcSnpCodes
        parts = TrcSnpCodes.Split(ListSeparators, StringSplitOptions.RemoveEmptyEntries);
        literal = "RD.TrcSnpCode IN (" + String.Join(", ", parts) + ")";
        wherePart = AppendWHERELiteral(wherePart, literal);
      }
      #endregion

      //Return the WHERE part
      return wherePart;
    }

    /// <summary>
    /// Method returns FROM part of the query
    /// </summary>
    /// <returns>From part of the query</returns>
    private String GetFROMPart()
    {
      bool bUseRD = false; //use RecordDefinitions table
      bool bUseRI = false; //use RepresItems table
      bool bUseTI = false; //use TUInstances table
      String fromPart = "FROM";
      fromPart += "\n  DiagnosticRecords DR WITH (NOLOCK)"; //Always use diagnostic records table
      //Check if RI and RD table has to be joined
      if (TitleKeywords != String.Empty || 
        (SortColumn.HasValue && SortColumn.Value == eSortColumn.RecordTitle))
      {
        bUseRD = true;
        bUseRI = true;
      }
      //Check if RD table has to be joined
      if (RecordTypes.HasValue || FaultCodes != String.Empty ||
          Severities != String.Empty || SubSeverities != String.Empty || TrcSnpCodes != String.Empty ||
          (SortColumn.HasValue &&
            (SortColumn.Value == eSortColumn.EventSeverity || SortColumn.Value == eSortColumn.EventSubSeverity ||
             SortColumn.Value == eSortColumn.FaultCode || SortColumn.Value == eSortColumn.TrcSnpCode)) ||
          statGroupCols.Contains("RD.SeverityID") || statGroupCols.Contains("RD.SubSeverity") ||
          statGroupCols.Contains("RD.RecordTypeID"))
        bUseRD = true;
      //Check if TI table has to be joined
      if (statGroupCols.Contains("TI.TUTypeID"))
        bUseTI = true;
      //Join RD table if necessary
      if (bUseRD)
      {
        fromPart += "\n  INNER JOIN RecordDefinitions RD";
        fromPart += "\n    ON (DR.DDDVersionID = RD.VersionID AND DR.RecordDefID = RD.RecordDefID)";
      }
      //Join RI table if necessary
      if (bUseRI)
      {
        fromPart += "\n  INNER JOIN RepresItems RI";
        fromPart += "\n    ON (RD.VersionID = RI.VersionID AND RD.TitleRepresID = RI.RepresID)";
      }
      //Join TI table if necessary
      if (bUseTI)
      {
        fromPart += "\n  INNER JOIN TUInstances TI";
        fromPart += "\n    ON (DR.TUInstanceID = TI.TUInstanceID)";
      }
      //Return FROM part
      return fromPart;
    }
    /// <summary>
    /// Method returns ORDER BY part of the query
    /// </summary>
    /// <returns>ORDER BY part of the query</returns>
    private String GetORDERBYPart()
    {
      if (!SortColumn.HasValue)
        return String.Empty; //no order by specified
      //Prepare ORDER BY
      String orderPart = "ORDER BY\n  ";
      //Select column on which data shall be ordered
      switch (SortColumn.Value)
      {
        case eSortColumn.AcknowledgeTime:
          orderPart += "DR.AcknowledgeTime";
          break;
        case eSortColumn.DeviceCode:
          orderPart += "DR.DeviceCode";
          break;
        case eSortColumn.EndTime:
          orderPart += "DR.EndTime";
          break;
        case eSortColumn.EventSeverity:
          orderPart += "RD.SeverityID";
          break;
        case eSortColumn.EventSubSeverity:
          orderPart += "RD.SubSeverity";
          break;
        case eSortColumn.FaultCode:
          orderPart += "RD.FaultCode";
          break;
        case eSortColumn.ImportTime:
          orderPart += "DR.ImportTime";
          break;
        case eSortColumn.LastModified:
          orderPart += "DR.LastModified";
          break;
        case eSortColumn.RecordTitle:
          orderPart += "RI.Text";
          break;
        case eSortColumn.Source:
          orderPart += "DR.Source";
          break;
        case eSortColumn.StartTime:
          orderPart += "DR.StartTime";
          break;
        case eSortColumn.TrcSnpCode:
          orderPart += "RD.TrcSnpCode";
          break;
        case eSortColumn.VehicleNumber:
          orderPart += "DR.VehicleNumber";
          break;
      }
      //Select proper order
      switch (SortOrder)
      {
        case eSortOrder.Ascending:
          orderPart += " ASC";
          break;
        case eSortOrder.Descending:
          orderPart += " DESC";
          break;
      }
      //Return order part
      return orderPart;
    }
    /// <summary>
    /// Retrieves ID of sort column from enumeration based on column name
    /// </summary>
    /// <param name="columnName">Name of the column on which to sort</param>
    /// <returns>Value from eSortColumn</returns>
    private eSortColumn GetSortColumnID(String columnName)
    {
      switch (columnName)
      {
        case "AcknowledgeTime":
        case "Acknowledge Time":
          return eSortColumn.AcknowledgeTime;
        case "DeviceCode":
        case "Device Code":
          return eSortColumn.DeviceCode;
        case "EndTime":
        case "End Time":
          return eSortColumn.EndTime;
        case "FaultSeverity":
        case "Fault Severity":
          return eSortColumn.EventSeverity;
        case "FaultSubSeverity":
        case "Fault Subseverity":
          return eSortColumn.EventSubSeverity;
        case "FaultCode":
        case "Fault Code":
          return eSortColumn.FaultCode;
        case "ImportTime":
        case "Import Time":
          return eSortColumn.ImportTime;
        case "LastModified":
        case "Last Modified":
          return eSortColumn.LastModified;
        case "RecordTitle":
        case "Record Title":
          return eSortColumn.RecordTitle;
        case "Source":
        case "Record Source":
          return eSortColumn.Source;
        case "StartTime":
        case "Start Time":
          return eSortColumn.StartTime;
        case "TrcSnpCode":
        case "Trace or Snap Code":
          return eSortColumn.TrcSnpCode;
        case "VehicleNumber":
        case "Vehicle Number":
          return eSortColumn.VehicleNumber;
        default:
          return eSortColumn.StartTime;
      }
    }

    /// <summary>
    /// Join two literals by AND, insert separator before AND
    /// </summary>
    /// <param name="lit1">literal 1</param>
    /// <param name="lit2">literal 2</param>
    /// <param name="separator">separator inserted before AND</param>
    /// <returns>resulting expression</returns>
    private String JoinAND(String lit1, String lit2, String separator)
    {
      if (lit1 == String.Empty)
        return lit2;
      else if (lit2 == String.Empty)
        return lit1;
      else
        return "(" + lit1 + ") " + separator + "AND (" + lit2 + ")";
    }
    /// <summary>
    /// Join two literals by OR, insert separator before OR
    /// </summary>
    /// <param name="lit1">literal 1</param>
    /// <param name="lit2">literal 2</param>
    /// <param name="separator">separator inserted before OR</param>
    /// <returns>resulting expression</returns>
    private String JoinOR(String lit1, String lit2, String separator)
    {
      if (lit1 == String.Empty)
        return lit2;
      else if (lit2 == String.Empty)
        return lit1;
      else
        return "(" + lit1 + ") " + separator + "OR (" + lit2 + ")";
    }
    /// <summary>
    /// Append additional literal to WHERE part.
    /// </summary>
    /// <param name="wherePart">Original where part</param>
    /// <param name="literal">New literal</param>
    /// <returns>New where part</returns>
    private String AppendWHERELiteral(String wherePart, String literal)
    {
      //Check if WHERE part is empty
      if (wherePart == String.Empty)
        return "WHERE\n      (" + literal + ")";
      else
        return wherePart + "\n  AND (" + literal + ")";
    }
    /// <summary>
    /// Joins two parts of the query together
    /// </summary>
    /// <param name="part1">First part of the query</param>
    /// <param name="part2">Second part of the query</param>
    /// <returns>Joined query</returns>
    private String JoinQUERYParts(String part1, String part2)
    {
      if (part2 == String.Empty)
        return part1;
      else
        return part1 + "\n" + part2;
    }
    #endregion

    #region Publis static methods
    /// <summary>
    /// Method obtains text for query which determines total number of records satysfying given criteria
    /// </summary>
    public static String GetQuery_Count(
            String TUInstanceIDs, String HierarchyItemIDs, 
      DateTime? StartTimeFrom, DateTime? StartTimeTo,
      String Sources, String VehicleNumbers, String FaultCodes, String TrcSnpCodes, 
      String TitleKeywords, bool MatchAllKeywords,
      int? RecordTypes, String Severities, String SubSeverities)
    {
      //Create instance of this class
      DRQueryHelper helper = new DRQueryHelper();
      //Set query parameters
      #region Set query parameters
      if (TUInstanceIDs == null)
        helper.TUInstanceIDs = String.Empty;
      else
        helper.TUInstanceIDs = TUInstanceIDs;
      if (HierarchyItemIDs == null)
        helper.HierarchyItemIDs = String.Empty;
      else
        helper.HierarchyItemIDs = HierarchyItemIDs;
      helper.StartTimeFrom = StartTimeFrom;
      helper.StartTimeTo = StartTimeTo;
      if (Sources == null)
        helper.Sources = String.Empty;
      else
        helper.Sources = Sources;
      if (VehicleNumbers == null)
        helper.VehicleNumbers = String.Empty;
      else
        helper.VehicleNumbers = VehicleNumbers;
      if (FaultCodes == null)
        helper.FaultCodes = String.Empty;
      else
        helper.FaultCodes = FaultCodes;
      if (TrcSnpCodes == null)
        helper.TrcSnpCodes = String.Empty;
      else
        helper.TrcSnpCodes = TrcSnpCodes;
      if (TitleKeywords == null)
        helper.TitleKeywords = String.Empty;
      else
        helper.TitleKeywords = TitleKeywords;
      helper.MatchAllKeywords = MatchAllKeywords;
      helper.RecordTypes = (eRecordCategory?)RecordTypes;
      if (Severities == null)
        helper.Severities = String.Empty;
      else
        helper.Severities = Severities;
      if (SubSeverities == null)
        helper.SubSeverities = String.Empty;
      else
        helper.SubSeverities = SubSeverities;
      #endregion
      //Return query string
      return helper.GetQuery_Count();
    }

    /// <summary>
    /// Method obtains query string for paging query.
    /// </summary>
    public static String GetQuery_RecordsPaging(
      String TUInstanceIDs, String HierarchyItemIDs, 
      DateTime? StartTimeFrom, DateTime? StartTimeTo,
      String Sources, String VehicleNumbers, String FaultCodes, String TrcSnpCodes, 
      String TitleKeywords, bool MatchAllKeywords,
      int? RecordTypes, String Severities, String SubSeverities, 
      int StartRowIndex, int MaximumRows, 
      String SortColumnName, eSortOrder SortOrder)
    {
      //Create instance of this class
      DRQueryHelper helper = new DRQueryHelper();
      //Set query parameters
      #region Set query parameters
      if (TUInstanceIDs == null)
        helper.TUInstanceIDs = String.Empty;
      else
        helper.TUInstanceIDs = TUInstanceIDs;
      if (HierarchyItemIDs == null)
        helper.HierarchyItemIDs = String.Empty;
      else
        helper.HierarchyItemIDs = HierarchyItemIDs;
      helper.StartTimeFrom = StartTimeFrom;
      helper.StartTimeTo = StartTimeTo;
      if (Sources == null)
        helper.Sources = String.Empty;
      else
        helper.Sources = Sources;
      if (VehicleNumbers == null)
        helper.VehicleNumbers = String.Empty;
      else
        helper.VehicleNumbers = VehicleNumbers;
      if (FaultCodes == null)
        helper.FaultCodes = String.Empty;
      else
        helper.FaultCodes = FaultCodes;
      if (TrcSnpCodes == null)
        helper.TrcSnpCodes = String.Empty;
      else
        helper.TrcSnpCodes = TrcSnpCodes;
      if (TitleKeywords == null)
        helper.TitleKeywords = String.Empty;
      else
        helper.TitleKeywords = TitleKeywords;
      helper.MatchAllKeywords = MatchAllKeywords;
      helper.RecordTypes = (eRecordCategory?)RecordTypes;
      if (Severities == null)
        helper.Severities = String.Empty;
      else
        helper.Severities = Severities;
      if (SubSeverities == null)
        helper.SubSeverities = String.Empty;
      else
        helper.SubSeverities = SubSeverities;
      helper.StartRowIndex = StartRowIndex;
      helper.MaximumRows = MaximumRows;
      helper.SortColumn = helper.GetSortColumnID(SortColumnName);
      helper.SortOrder = SortOrder;
      #endregion
      //Return query string
      return helper.GetQuery_RecordsPaging();
    }

    /// <summary>
    /// Method obtains string for query which deletes diagnostic records.
    /// </summary>
    public static String GetQuery_DeleteRecords(
      String TUInstanceIDs, String HierarchyItemIDs,
      DateTime? StartTimeFrom, DateTime? StartTimeTo,
      String Sources, String VehicleNumbers, String FaultCodes, String TrcSnpCodes,
      String TitleKeywords, bool MatchAllKeywords,
      int? RecordTypes, String Severities, String SubSeverities,
      int MaximumRows,
      eSortColumn SortColumn, eSortOrder SortOrder)
    {
      //Create instance of this class
      DRQueryHelper helper = new DRQueryHelper();
      //Set query parameters
      #region Set query parameters
      if (TUInstanceIDs == null)
        helper.TUInstanceIDs = String.Empty;
      else
        helper.TUInstanceIDs = TUInstanceIDs;
      if (HierarchyItemIDs == null)
        helper.HierarchyItemIDs = String.Empty;
      else
        helper.HierarchyItemIDs = HierarchyItemIDs;
      helper.StartTimeFrom = StartTimeFrom;
      helper.StartTimeTo = StartTimeTo;
      if (Sources == null)
        helper.Sources = String.Empty;
      else
        helper.Sources = Sources;
      if (VehicleNumbers == null)
        helper.VehicleNumbers = String.Empty;
      else
        helper.VehicleNumbers = VehicleNumbers;
      if (FaultCodes == null)
        helper.FaultCodes = String.Empty;
      else
        helper.FaultCodes = FaultCodes;
      if (TrcSnpCodes == null)
        helper.TrcSnpCodes = String.Empty;
      else
        helper.TrcSnpCodes = TrcSnpCodes;
      if (TitleKeywords == null)
        helper.TitleKeywords = String.Empty;
      else
        helper.TitleKeywords = TitleKeywords;
      helper.MatchAllKeywords = MatchAllKeywords;
      helper.RecordTypes = (eRecordCategory?)RecordTypes;
      if (Severities == null)
        helper.Severities = String.Empty;
      else
        helper.Severities = Severities;
      if (SubSeverities == null)
        helper.SubSeverities = String.Empty;
      else
        helper.SubSeverities = SubSeverities;
      helper.StartRowIndex = 0;
      helper.MaximumRows = MaximumRows;
      helper.SortColumn = SortColumn;
      helper.SortOrder = SortOrder;
      #endregion
      //Return query string
      return helper.GetQuery_DeleteRecords();
    }

    /// <summary>
    /// Method obtains query string for application query.
    /// </summary>
    public static String GetQuery_RecordsApp(
      String TUInstanceIDs, String HierarchyItemIDs,
      DateTime? StartTimeFrom, DateTime? StartTimeTo,
      String Sources, String VehicleNumbers, String FaultCodes, String TrcSnpCodes,
      String TitleKeywords, bool MatchAllKeywords,
      eRecordCategory? RecordTypes, String Severities, String SubSeverities,
      int MaximumRows,
      String SortColumnName, eSortOrder SortOrder)
    {
      //Create instance of this class
      DRQueryHelper helper = new DRQueryHelper();
      //Set query parameters
      #region Set query parameters
      if (TUInstanceIDs == null)
        helper.TUInstanceIDs = String.Empty;
      else
        helper.TUInstanceIDs = TUInstanceIDs;
      if (HierarchyItemIDs == null)
        helper.HierarchyItemIDs = String.Empty;
      else
        helper.HierarchyItemIDs = HierarchyItemIDs;
      helper.StartTimeFrom = StartTimeFrom;
      helper.StartTimeTo = StartTimeTo;
      if (Sources == null)
        helper.Sources = String.Empty;
      else
        helper.Sources = Sources;
      if (VehicleNumbers == null)
        helper.VehicleNumbers = String.Empty;
      else
        helper.VehicleNumbers = VehicleNumbers;
      if (FaultCodes == null)
        helper.FaultCodes = String.Empty;
      else
        helper.FaultCodes = FaultCodes;
      if (TrcSnpCodes == null)
        helper.TrcSnpCodes = String.Empty;
      else
        helper.TrcSnpCodes = TrcSnpCodes;
      if (TitleKeywords == null)
        helper.TitleKeywords = String.Empty;
      else
        helper.TitleKeywords = TitleKeywords;
      helper.MatchAllKeywords = MatchAllKeywords;
      helper.RecordTypes = RecordTypes;
      if (Severities == null)
        helper.Severities = String.Empty;
      else
        helper.Severities = Severities;
      if (SubSeverities == null)
        helper.SubSeverities = String.Empty;
      else
        helper.SubSeverities = SubSeverities;
      helper.MaximumRows = MaximumRows;
      helper.SortColumn = helper.GetSortColumnID(SortColumnName);
      helper.SortOrder = SortOrder;
      #endregion
      //Return query string
      return helper.GetQuery_RecordsApp();
    }

    /// <summary>
    /// Method obtains statistical query for web application.
    /// </summary>
    public static String GetQuery_WebStatistics(
      String TUInstanceIDs, String HierarchyItemIDs,
      DateTime? StartTimeFrom, DateTime? StartTimeTo,
      String Sources, String VehicleNumbers, String FaultCodes, String TrcSnpCodes,
      String TitleKeywords, bool MatchAllKeywords,
      int? RecordTypes, String Severities, String SubSeverities)
    {
      //Create instance of this class
      DRQueryHelper helper = new DRQueryHelper();
      //Set query parameters
      #region Set query parameters
      if (TUInstanceIDs == null)
        helper.TUInstanceIDs = String.Empty;
      else
        helper.TUInstanceIDs = TUInstanceIDs;
      if (HierarchyItemIDs == null)
        helper.HierarchyItemIDs = String.Empty;
      else
        helper.HierarchyItemIDs = HierarchyItemIDs;
      helper.StartTimeFrom = StartTimeFrom;
      helper.StartTimeTo = StartTimeTo;
      if (Sources == null)
        helper.Sources = String.Empty;
      else
        helper.Sources = Sources;
      if (VehicleNumbers == null)
        helper.VehicleNumbers = String.Empty;
      else
        helper.VehicleNumbers = VehicleNumbers;
      if (FaultCodes == null)
        helper.FaultCodes = String.Empty;
      else
        helper.FaultCodes = FaultCodes;
      if (TrcSnpCodes == null)
        helper.TrcSnpCodes = String.Empty;
      else
        helper.TrcSnpCodes = TrcSnpCodes;
      if (TitleKeywords == null)
        helper.TitleKeywords = String.Empty;
      else
        helper.TitleKeywords = TitleKeywords;
      helper.MatchAllKeywords = MatchAllKeywords;
      helper.RecordTypes = (eRecordCategory?)RecordTypes;
      if (Severities == null)
        helper.Severities = String.Empty;
      else
        helper.Severities = Severities;
      if (SubSeverities == null)
        helper.SubSeverities = String.Empty;
      else
        helper.SubSeverities = SubSeverities;
      #endregion
      //Return query string
      return helper.GetQuery_WebStatistics();
    }
    #endregion
  }
}
