using System;
using System.Collections.Generic;
using System.Text;

namespace DDDObjects
{
    /// <summary>
    /// Prototype class for representats of elementary data types.
    /// Used in representations for formating and parsing
    /// </summary>
    [Serializable]
    public class DDDTypeBase
    {
        /// <summary>
        /// Formats passed value according to specified format string.
        /// </summary>
        /// <param name="value">Value which has to be convertible to derived class data type</param>
        /// <param name="formatString">Formating string used to format the value</param>
        /// <param name="divisor">Constant used for conversion to correct physical unit</param>
        /// <param name="bias">Constant added to raw value</param>
        /// <returns>Value formated value</returns>
        public virtual String FormatValue(Object value, String formatString, double divisor, double bias)
        {
            //Dummy formatting
            return value.ToString();
        }

        /// <summary>
        /// Formats given value as hex word.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <returns>Value formatted as string</returns>
        public virtual String FormatHexValue(Object value)
        {
            return "";
        }

        /// <summary>
        /// Formats given value as series of 1s and 0s, each group of 4 bits separated by specified separator.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <param name="separator">Used to separate each 4 bits</param>
        /// <returns>Value formatted as string</returns>
        public virtual String FormatBinValue(Object value, String separator)
        {
            return "";
        }

        /// <summary>
        /// Extracts value as correctly typed object from binary buffer at given location.
        /// </summary>
        /// <param name="recBinData">Structure with binary data and record instance information.</param>
        /// <param name="dataOffset">Offset to binary buffer.</param>
        /// <param name="value">Output parameter containing extracted value.</param>
        /// <returns>Status of parsing operation</returns>
        public virtual eParseStatus ParseBinValue(sRecBinData recBinData, int dataOffset, out Object value)
        {
            //Virtual prototype - return success
            value = null;
            return eParseStatus.parseSuccessfull;
        }

        /// <summary>
        /// Applies divisor to given value.
        /// In case of divisor being 0 or 1, original object value is returned.
        /// In case of valid divisor, scaled value is returned as double
        /// </summary>
        /// <param name="value">Value to scale</param>
        /// <param name="divisor">Divisor to use for scaling</param>
        /// <param name="bias">Bias added to raw value</param>
        /// <returns>Scaled value as double or original object</returns>
        public virtual Object ScaleValue(Object value, double divisor, double bias)
        {
            if (divisor == 1 && bias == 0)
                return value; //divisor and bias not specified - return original value
            return (Convert.ToDouble(value) / divisor + bias); //apply the divisor and bias, return result as double
        }

        /// <summary>
        /// Rotates two bytes at specified location in buffer
        /// </summary>
        /// <param name="binData">buffer</param>
        /// <param name="offset">location in buffer</param>
        protected byte[] Rotate2(byte[] binData, int offset)
        {
            byte[] buff = new byte[2];
            buff[0] = binData[offset + 1];
            buff[1] = binData[offset];
            return buff;
        }

        /// <summary>
        /// Rotates four bytes at specified location in buffer
        /// </summary>
        /// <param name="binData">buffer</param>
        /// <param name="offset">location in buffer</param>
        protected byte[] Rotate4(byte[] binData, int offset)
        {
            byte[] buff = new byte[4];
            buff[0] = binData[offset + 3];
            buff[1] = binData[offset + 2];
            buff[2] = binData[offset + 1];
            buff[3] = binData[offset];
            return buff;
        }

        /// <summary>
        /// Rotates eight bytes at specified location in buffer
        /// </summary>
        /// <param name="binData">buffer</param>
        /// <param name="offset">location in buffer</param>
        protected byte[] Rotate8(byte[] binData, int offset)
        {
            byte[] buff = new byte[8];
            buff[0] = binData[offset + 7];
            buff[1] = binData[offset + 6];
            buff[2] = binData[offset + 5];
            buff[3] = binData[offset + 4];
            buff[4] = binData[offset + 3];
            buff[5] = binData[offset + 2];
            buff[6] = binData[offset + 1];
            buff[7] = binData[offset];
            return buff;
        }
    }

    /// <summary>
    /// Represents BIT data type for parsing and formatting operations
    /// </summary>
    [Serializable]
    public class DDDTypeBOOL : DDDTypeBase
    {
        /// <summary>
        /// Formats passed value according to specified format string.
        /// </summary>
        /// <param name="value">Value which has to be convertible to derived class data type</param>
        /// <param name="formatString">Formating string used to format the value</param>
        /// <param name="divisor">Constant used for conversion to correct physical unit</param>
        /// <param name="bias">Constant added to raw value</param>
        /// <returns>Value formated value</returns>
        public override String FormatValue(Object value, String formatString, double divisor, double bias)
        {
            int val = Convert.ToInt32(value);
            if (val != 0)
                return DDDHelper.BoolTrueSymbol;
            else
                return DDDHelper.BoolFalseSymbol;
        }

        /// <summary>
        /// Extracts value as correctly typed object from binary buffer at given location.
        /// </summary>
        /// <param name="recBinData">Structure with binary data and record instance information.</param>
        /// <param name="dataOffset">Offset to binary buffer.</param>
        /// <param name="value">Output parameter containing extracted value.</param>
        /// <returns>Status of parsing operation</returns>
        public override eParseStatus ParseBinValue(sRecBinData recBinData, int dataOffset, out Object value)
        {
            value = null;
            //Check buffer length
            if (recBinData.BinData.Length < (dataOffset + 1))
                return eParseStatus.binaryBufferTooSmall;
            //Convert value
            value = BitConverter.ToBoolean(recBinData.BinData, dataOffset);
            return eParseStatus.parseSuccessfull;
        }
    }

    /// <summary>
    /// Represents U8 data type for parsing and formatting operations
    /// </summary>
    [Serializable]
    public class DDDTypeNibble : DDDTypeBase
    {
        /// <summary>
        /// Formats passed value according to specified format string.
        /// </summary>
        /// <param name="value">Value which has to be convertible to derived class data type</param>
        /// <param name="formatString">Formating string used to format the value</param>
        /// <param name="divisor">Constant used for conversion to correct physical unit</param>
        /// <param name="bias">Constant added to raw value</param>
        /// <returns>Value formated value</returns>
        public override String FormatValue(Object value, String formatString, double divisor, double bias)
        {
            byte val = Convert.ToByte(value);
            val &= 0x0F;
            if (divisor != 1 || bias != 0)
            {
                if (formatString == "")
                    return ((double)val / divisor + bias).ToString("0.###");
                else
                    return ((double)val / divisor + bias).ToString(formatString);
            }
            else
                return val.ToString(formatString);
        }

        /// <summary>
        /// Extracts value as correctly typed object from binary buffer at given location.
        /// </summary>
        /// <param name="recBinData">Structure with binary data and record instance information.</param>
        /// <param name="dataOffset">Offset to binary buffer.</param>
        /// <param name="value">Output parameter containing extracted value.</param>
        /// <returns>Status of parsing operation</returns>
        public override eParseStatus ParseBinValue(sRecBinData recBinData, int dataOffset, out Object value)
        {
            value = null;
            //Check buffer length
            if (recBinData.BinData.Length < (dataOffset + 1))
                return eParseStatus.binaryBufferTooSmall;
            //Convert value
            byte val = (byte)recBinData.BinData[dataOffset];
            val &= 0x0F;
            value = val;
            return eParseStatus.parseSuccessfull;
        }

        /// <summary>
        /// Formats given value as hex word.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <returns>Value formatted as string</returns>
        public override string FormatHexValue(object value)
        {
            byte val = Convert.ToByte(value);
            val &= 0x0F;
            return val.ToString("X1");
        }

        /// <summary>
        /// Formats given value as series of 1s and 0s, each group of 4 bits separated by specified separator.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <param name="separator">Used to separate each 4 bits</param>
        /// <returns>Value formatted as string</returns>
        public override string FormatBinValue(object value, string separator)
        {
            byte val = Convert.ToByte(value);
            val &= 0x0F;
            String res = separator;
            for (UInt64 i = 1; i < 0x0F; i = i * 2)
            {
                if ((val & i) > 0)
                    res = "1" + res;
                else
                    res = "0" + res;
                if ((res.Length % 5) == 0)
                    res = separator + res;
            }
            return res;
        }
    }

    /// <summary>
    /// Represents U8 data type for parsing and formatting operations
    /// </summary>
    [Serializable]
    public class DDDTypeNibbleH : DDDTypeBase
    {
        /// <summary>
        /// Formats passed value according to specified format string.
        /// </summary>
        /// <param name="value">Value which has to be convertible to derived class data type</param>
        /// <param name="formatString">Formating string used to format the value</param>
        /// <param name="divisor">Constant used for conversion to correct physical unit</param>
        /// <param name="bias">Constant added to raw value</param>
        /// <returns>Value formated value</returns>
        public override String FormatValue(Object value, String formatString, double divisor, double bias)
        {
            byte val = Convert.ToByte(value);
            //val &= 0xF0;
            val = (byte)(val >> 4);
            if (divisor != 1 || bias != 0)
            {
                if (formatString == "")
                    return ((double)val / divisor + bias).ToString("0.###");
                else
                    return ((double)val / divisor + bias).ToString(formatString);
            }
            else
                return val.ToString(formatString);
        }

        /// <summary>
        /// Extracts value as correctly typed object from binary buffer at given location.
        /// </summary>
        /// <param name="recBinData">Structure with binary data and record instance information.</param>
        /// <param name="dataOffset">Offset to binary buffer.</param>
        /// <param name="value">Output parameter containing extracted value.</param>
        /// <returns>Status of parsing operation</returns>
        public override eParseStatus ParseBinValue(sRecBinData recBinData, int dataOffset, out Object value)
        {
            value = null;
            //Check buffer length
            if (recBinData.BinData.Length < (dataOffset + 1))
                return eParseStatus.binaryBufferTooSmall;
            //Convert value
            byte val = (byte)recBinData.BinData[dataOffset];
            //val &= 0xF0;
            val = (byte)(val >> 4);
            value = val;
            return eParseStatus.parseSuccessfull;
        }

        /// <summary>
        /// Formats given value as hex word.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <returns>Value formatted as string</returns>
        public override string FormatHexValue(object value)
        {
            byte val = Convert.ToByte(value);
            //val &= 0xF0;
            val = (byte)(val >> 4);
            return val.ToString("X1");
        }

        /// <summary>
        /// Formats given value as series of 1s and 0s, each group of 4 bits separated by specified separator.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <param name="separator">Used to separate each 4 bits</param>
        /// <returns>Value formatted as string</returns>
        public override string FormatBinValue(object value, string separator)
        {
            byte val = Convert.ToByte(value);
            //val &= 0xF0;
            val = (byte)(val >> 4);
            String res = separator;
            for (UInt64 i = 1; i < 0x0F; i = i * 2)
            {
                if ((val & i) > 0)
                    res = "1" + res;
                else
                    res = "0" + res;
                if ((res.Length % 5) == 0)
                    res = separator + res;
            }
            return res;
        }
    }

    /// <summary>
    /// Represents U8 data type for parsing and formatting operations
    /// </summary>
    [Serializable]
    public class DDDTypeU8 : DDDTypeBase
    {
        /// <summary>
        /// Formats passed value according to specified format string.
        /// </summary>
        /// <param name="value">Value which has to be convertible to derived class data type</param>
        /// <param name="formatString">Formating string used to format the value</param>
        /// <param name="divisor">Constant used for conversion to correct physical unit</param>
        /// <param name="bias">Constant added to raw value</param>
        /// <returns>Value formated value</returns>
        public override String FormatValue(Object value, String formatString, double divisor, double bias)
        {
            byte val = Convert.ToByte(value);
            if (divisor != 1 || bias != 0)
            {
                if (formatString == "")
                    return ((double)val / divisor + bias).ToString("0.###");
                else
                    return ((double)val / divisor + bias).ToString(formatString);
            }
            else
                return val.ToString(formatString);
        }

        /// <summary>
        /// Extracts value as correctly typed object from binary buffer at given location.
        /// </summary>
        /// <param name="recBinData">Structure with binary data and record instance information.</param>
        /// <param name="dataOffset">Offset to binary buffer.</param>
        /// <param name="value">Output parameter containing extracted value.</param>
        /// <returns>Status of parsing operation</returns>
        public override eParseStatus ParseBinValue(sRecBinData recBinData, int dataOffset, out Object value)
        {
            value = null;
            //Check buffer length
            if (recBinData.BinData.Length < (dataOffset + 1))
                return eParseStatus.binaryBufferTooSmall;
            //Convert value
            value = (Int32)recBinData.BinData[dataOffset];
            return eParseStatus.parseSuccessfull;
        }

        /// <summary>
        /// Formats given value as hex word.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <returns>Value formatted as string</returns>
        public override string FormatHexValue(object value)
        {
            byte val = Convert.ToByte(value);
            return val.ToString("X2");
        }

        /// <summary>
        /// Formats given value as series of 1s and 0s, each group of 4 bits separated by specified separator.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <param name="separator">Used to separate each 4 bits</param>
        /// <returns>Value formatted as string</returns>
        public override string FormatBinValue(object value, string separator)
        {
            byte val = Convert.ToByte(value);
            String res = separator;
            for (UInt64 i = 1; i < 0xFF; i = i * 2)
            {
                if ((val & i) > 0)
                    res = "1" + res;
                else
                    res = "0" + res;
                if ((res.Length % 5) == 0)
                    res = separator + res;
            }
            return res;
        }
    }

    /// <summary>
    /// Represents U16 data type for parsing and formatting operations
    /// </summary>
    [Serializable]
    public class DDDTypeU16 : DDDTypeBase
    {
        /// <summary>
        /// Formats passed value according to specified format string.
        /// </summary>
        /// <param name="value">Value which has to be convertible to derived class data type</param>
        /// <param name="formatString">Formating string used to format the value</param>
        /// <param name="divisor">Constant used for conversion to correct physical unit</param>
        /// <param name="bias">Constant added to raw value</param>
        /// <returns>Value formated value</returns>
        public override String FormatValue(Object value, String formatString, double divisor, double bias)
        {
            UInt16 val = Convert.ToUInt16(value);
            if (divisor != 1 || bias != 0)
            {
                if (formatString == "")
                    return ((double)val / divisor + bias).ToString("0.###");
                else
                    return ((double)val / divisor + bias).ToString(formatString);
            }
            else
                return val.ToString(formatString);
        }

        /// <summary>
        /// Extracts value as correctly typed object from binary buffer at given location.
        /// </summary>
        /// <param name="recBinData">Structure with binary data and record instance information.</param>
        /// <param name="dataOffset">Offset to binary buffer.</param>
        /// <param name="value">Output parameter containing extracted value.</param>
        /// <returns>Status of parsing operation</returns>
        public override eParseStatus ParseBinValue(sRecBinData recBinData, int dataOffset, out Object value)
        {
            value = null;
            //Check buffer length
            if (recBinData.BinData.Length < (dataOffset + 2))
                return eParseStatus.binaryBufferTooSmall;
            //Rotate and convert value
            if (recBinData.BigEndian)
                value = (Int32)BitConverter.ToUInt16(Rotate2(recBinData.BinData, dataOffset), 0);
            else
                value = (Int32)BitConverter.ToUInt16(recBinData.BinData, dataOffset);
            return eParseStatus.parseSuccessfull;
        }

        /// <summary>
        /// Formats given value as hex word.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <returns>Value formatted as string</returns>
        public override string FormatHexValue(object value)
        {
            UInt16 val = Convert.ToUInt16(value);
            return val.ToString("X4");
        }

        /// <summary>
        /// Formats given value as series of 1s and 0s, each group of 4 bits separated by specified separator.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <param name="separator">Used to separate each 4 bits</param>
        /// <returns>Value formatted as string</returns> 
        public override string FormatBinValue(object value, string separator)
        {
            UInt16 val = Convert.ToUInt16(value);
            String res = separator;
            for (UInt64 i = 1; i < 0xFFFF; i = i * 2)
            {
                if ((val & i) > 0)
                    res = "1" + res;
                else
                    res = "0" + res;
                if ((res.Length % 5) == 0)
                    res = separator + res;
            }
            return res;
        }
    }

    /// <summary>
    /// Represents U32 data type for parsing and formatting operations
    /// </summary>
    [Serializable]
    public class DDDTypeU32 : DDDTypeBase
    {
        /// <summary>
        /// Formats passed value according to specified format string.
        /// </summary>
        /// <param name="value">Value which has to be convertible to derived class data type</param>
        /// <param name="formatString">Formating string used to format the value</param>
        /// <param name="divisor">Constant used for conversion to correct physical unit</param>
        /// <param name="bias">Constant added to raw value</param>
        /// <returns>Value formated value</returns>
        public override String FormatValue(Object value, String formatString, double divisor, double bias)
        {
            UInt32 val = (UInt32)Convert.ToInt32(value);
            if (divisor != 1 || bias != 0)
            {
                if (formatString == "")
                    return ((double)val / divisor + bias).ToString("0.###");
                else
                    return ((double)val / divisor + bias).ToString(formatString);
            }
            else
                return val.ToString(formatString);
        }

        /// <summary>
        /// Extracts value as correctly typed object from binary buffer at given location.
        /// </summary>
        /// <param name="recBinData">Structure with binary data and record instance information.</param>
        /// <param name="dataOffset">Offset to binary buffer.</param>
        /// <param name="value">Output parameter containing extracted value.</param>
        /// <returns>Status of parsing operation</returns>
        public override eParseStatus ParseBinValue(sRecBinData recBinData, int dataOffset, out Object value)
        {
            value = null;
            //Check buffer length
            if (recBinData.BinData.Length < (dataOffset + 4))
                return eParseStatus.binaryBufferTooSmall;
            //Rotate and convert value
            if (recBinData.BigEndian)
                value = (Int32)BitConverter.ToUInt32(Rotate4(recBinData.BinData, dataOffset), 0);
            else
                value = (Int32)BitConverter.ToUInt32(recBinData.BinData, dataOffset);
            return eParseStatus.parseSuccessfull;
        }

        /// <summary>
        /// Formats given value as hex word.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <returns>Value formatted as string</returns>
        public override string FormatHexValue(object value)
        {
            UInt32 val = (UInt32)Convert.ToInt32(value);
            return val.ToString("X8");
        }

        /// <summary>
        /// Formats given value as series of 1s and 0s, each group of 4 bits separated by specified separator.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <param name="separator">Used to separate each 4 bits</param>
        /// <returns>Value formatted as string</returns>public override string 
        public override string FormatBinValue(object value, string separator)
        {
            UInt32 val = (UInt32)Convert.ToInt32(value);
            String res = separator;
            for (UInt64 i = 1; i < 0xFFFFFFFF; i = i * 2)
            {
                if ((val & i) > 0)
                    res = "1" + res;
                else
                    res = "0" + res;
                if ((res.Length % 5) == 0)
                    res = separator + res;
            }
            return res;
        }
    }

    /// <summary>
    /// Represents U64 data type for parsing and formatting operations
    /// </summary>
    [Serializable]
    public class DDDTypeU64 : DDDTypeBase
    {
        /// <summary>
        /// Formats passed value according to specified format string.
        /// </summary>
        /// <param name="value">Value which has to be convertible to derived class data type</param>
        /// <param name="formatString">Formating string used to format the value</param>
        /// <param name="divisor">Constant used for conversion to correct physical unit</param>
        /// <param name="bias">Constant added to raw value</param>
        /// <returns>Value formated value</returns>
        public override String FormatValue(Object value, String formatString, double divisor, double bias)
        {
            UInt64 val = (UInt64)Convert.ToInt64(value);
            if (divisor != 1 || bias != 0)
            {
                if (formatString == "")
                    return ((double)val / divisor + bias).ToString("0.###");
                else
                    return ((double)val / divisor + bias).ToString(formatString);
            }
            else
                return val.ToString(formatString);
        }

        /// <summary>
        /// Extracts value as correctly typed object from binary buffer at given location.
        /// </summary>
        /// <param name="recBinData">Structure with binary data and record instance information.</param>
        /// <param name="dataOffset">Offset to binary buffer.</param>
        /// <param name="value">Output parameter containing extracted value.</param>
        /// <returns>Status of parsing operation</returns>
        public override eParseStatus ParseBinValue(sRecBinData recBinData, int dataOffset, out Object value)
        {
            value = null;
            //Check buffer length
            if (recBinData.BinData.Length < (dataOffset + 8))
                return eParseStatus.binaryBufferTooSmall;
            //Rotate and convert value
            if (recBinData.BigEndian)
                value = (Int64)BitConverter.ToUInt64(Rotate8(recBinData.BinData, dataOffset), 0);
            else
                value = (Int64)BitConverter.ToUInt64(recBinData.BinData, dataOffset);
            return eParseStatus.parseSuccessfull;
        }

        /// <summary>
        /// Formats given value as hex word.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <returns>Value formatted as string</returns>
        public override string FormatHexValue(object value)
        {
            UInt64 val = (UInt64)Convert.ToInt64(value);
            return val.ToString("X16");
        }

        /// <summary>
        /// Formats given value as series of 1s and 0s, each group of 4 bits separated by specified separator.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <param name="separator">Used to separate each 4 bits</param>
        /// <returns>Value formatted as string</returns>public override string 
        public override string FormatBinValue(object value, string separator)
        {
            UInt64 val = (UInt64)Convert.ToInt64(value);
            String res = separator;
            for (UInt64 i = 1; i < UInt64.MaxValue; i = i * 2)
            {
                if ((val & i) > 0)
                    res = "1" + res;
                else
                    res = "0" + res;
                if ((res.Length % 5) == 0)
                    res = separator + res;
                if (i == 0x8000000000000000)
                    break;
            }
            return res;
        }
    }

    /// <summary>
    /// Represents I8 data type for parsing and formatting operations
    /// </summary>
    [Serializable]
    public class DDDTypeI8 : DDDTypeBase
    {
        /// <summary>
        /// Formats passed value according to specified format string.
        /// </summary>
        /// <param name="value">Value which has to be convertible to derived class data type</param>
        /// <param name="formatString">Formating string used to format the value</param>
        /// <param name="divisor">Constant used for conversion to correct physical unit</param>
        /// <param name="bias">Constant added to raw value</param>
        /// <returns>Value formated value</returns>
        public override String FormatValue(Object value, String formatString, double divisor, double bias)
        {
            SByte val = Convert.ToSByte(value);
            if (divisor != 1 || bias != 0)
            {
                if (formatString == "")
                    return ((double)val / divisor + bias).ToString("0.###");
                else
                    return ((double)val / divisor + bias).ToString(formatString);
            }
            else
                return val.ToString(formatString);
        }

        /// <summary>
        /// Extracts value as correctly typed object from binary buffer at given location.
        /// </summary>
        /// <param name="recBinData">Structure with binary data and record instance information.</param>
        /// <param name="dataOffset">Offset to binary buffer.</param>
        /// <param name="value">Output parameter containing extracted value.</param>
        /// <returns>Status of parsing operation</returns>
        public override eParseStatus ParseBinValue(sRecBinData recBinData, int dataOffset, out Object value)
        {
            value = null;
            //Check buffer length
            if (recBinData.BinData.Length < (dataOffset + 1))
                return eParseStatus.binaryBufferTooSmall;
            //Convert value
            value = (SByte)recBinData.BinData[dataOffset];
            return eParseStatus.parseSuccessfull;
        }

        /// <summary>
        /// Formats given value as hex word.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <returns>Value formatted as string</returns>
        public override string FormatHexValue(object value)
        {
            byte val = Convert.ToByte(value);
            return val.ToString("X2");
        }

        /// <summary>
        /// Formats given value as series of 1s and 0s, each group of 4 bits separated by specified separator.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <param name="separator">Used to separate each 4 bits</param>
        /// <returns>Value formatted as string</returns>public override string 
        public override string FormatBinValue(object value, string separator)
        {
            byte val = Convert.ToByte(value);
            String res = separator;
            for (UInt64 i = 1; i < 0xFF; i = i * 2)
            {
                if ((val & i) > 0)
                    res = "1" + res;
                else
                    res = "0" + res;
                if ((res.Length % 5) == 0)
                    res = separator + res;
            }
            return res;
        }
    }

    /// <summary>
    /// Represents I16 data type for parsing and formatting operations
    /// </summary>
    [Serializable]
    public class DDDTypeI16 : DDDTypeBase
    {
        /// <summary>
        /// Formats passed value according to specified format string.
        /// </summary>
        /// <param name="value">Value which has to be convertible to derived class data type</param>
        /// <param name="formatString">Formating string used to format the value</param>
        /// <param name="divisor">Constant used for conversion to correct physical unit</param>
        /// <param name="bias">Constant added to raw value</param>
        /// <returns>Value formated value</returns>
        public override String FormatValue(Object value, String formatString, double divisor, double bias)
        {
            Int16 val = Convert.ToInt16(value);
            if (divisor != 1 || bias != 0)
            {
                if (formatString == "")
                    return ((double)val / divisor + bias).ToString("0.###");
                else
                    return ((double)val / divisor + bias).ToString(formatString);
            }
            else
                return val.ToString(formatString);
        }

        /// <summary>
        /// Extracts value as correctly typed object from binary buffer at given location.
        /// </summary>
        /// <param name="recBinData">Structure with binary data and record instance information.</param>
        /// <param name="dataOffset">Offset to binary buffer.</param>
        /// <param name="value">Output parameter containing extracted value.</param>
        /// <returns>Status of parsing operation</returns>
        public override eParseStatus ParseBinValue(sRecBinData recBinData, int dataOffset, out Object value)
        {
            value = null;
            //Check buffer length
            if (recBinData.BinData.Length < (dataOffset + 2))
                return eParseStatus.binaryBufferTooSmall;
            //Rotate and convert value
            if (recBinData.BigEndian)
                value = BitConverter.ToInt16(Rotate2(recBinData.BinData, dataOffset), 0);
            else
                value = BitConverter.ToInt16(recBinData.BinData, dataOffset);
            return eParseStatus.parseSuccessfull;
        }

        /// <summary>
        /// Formats given value as hex word.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <returns>Value formatted as string</returns>
        public override string FormatHexValue(object value)
        {
            UInt16 val = Convert.ToUInt16(value);
            return val.ToString("X4");
        }

        /// <summary>
        /// Formats given value as series of 1s and 0s, each group of 4 bits separated by specified separator.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <param name="separator">Used to separate each 4 bits</param>
        /// <returns>Value formatted as string</returns>public override string 
        public override string FormatBinValue(object value, string separator)
        {
            UInt16 val = Convert.ToUInt16(value);
            String res = separator;
            for (UInt64 i = 1; i < 0xFFFF; i = i * 2)
            {
                if ((val & i) > 0)
                    res = "1" + res;
                else
                    res = "0" + res;
                if ((res.Length % 5) == 0)
                    res = separator + res;
            }
            return res;
        }
    }

    /// <summary>
    /// Represents I32 data type for parsing and formatting operations
    /// </summary>
    [Serializable]
    public class DDDTypeI32 : DDDTypeBase
    {
        /// <summary>
        /// Formats passed value according to specified format string.
        /// </summary>
        /// <param name="value">Value which has to be convertible to derived class data type</param>
        /// <param name="formatString">Formating string used to format the value</param>
        /// <param name="divisor">Constant used for conversion to correct physical unit</param>
        /// <param name="bias">Constant added to raw value</param>
        /// <returns>Value formated value</returns>
        public override String FormatValue(Object value, String formatString, double divisor, double bias)
        {
            Int32 val = Convert.ToInt32(value);
            if (divisor != 1 || bias != 0)
            {
                if (formatString == "")
                    return ((double)val / divisor + bias).ToString("0.###");
                else
                    return ((double)val / divisor + bias).ToString(formatString);
            }
            else
                return val.ToString(formatString);
        }

        /// <summary>
        /// Extracts value as correctly typed object from binary buffer at given location.
        /// </summary>
        /// <param name="recBinData">Structure with binary data and record instance information.</param>
        /// <param name="dataOffset">Offset to binary buffer.</param>
        /// <param name="value">Output parameter containing extracted value.</param>
        /// <returns>Status of parsing operation</returns>
        public override eParseStatus ParseBinValue(sRecBinData recBinData, int dataOffset, out Object value)
        {
            value = null;
            //Check buffer length
            if (recBinData.BinData.Length < (dataOffset + 4))
                return eParseStatus.binaryBufferTooSmall;
            //Rotate and convert value
            if (recBinData.BigEndian)
                value = BitConverter.ToInt32(Rotate4(recBinData.BinData, dataOffset), 0);
            else
                value = BitConverter.ToInt32(recBinData.BinData, dataOffset);
            return eParseStatus.parseSuccessfull;
        }

        /// <summary>
        /// Formats given value as hex word.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <returns>Value formatted as string</returns>
        public override string FormatHexValue(object value)
        {
            UInt32 val = Convert.ToUInt32(value);
            return val.ToString("X8");
        }

        /// <summary>
        /// Formats given value as series of 1s and 0s, each group of 4 bits separated by specified separator.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <param name="separator">Used to separate each 4 bits</param>
        /// <returns>Value formatted as string</returns>public override string 
        public override string FormatBinValue(object value, string separator)
        {
            UInt32 val = Convert.ToUInt32(value);
            String res = separator;
            for (UInt64 i = 1; i < 0xFFFFFFFF; i = i * 2)
            {
                if ((val & i) > 0)
                    res = "1" + res;
                else
                    res = "0" + res;
                if ((res.Length % 5) == 0)
                    res = separator + res;
            }
            return res;
        }
    }

    /// <summary>
    /// Represents I64 data type for parsing and formatting operations
    /// </summary>
    [Serializable]
    public class DDDTypeI64 : DDDTypeBase
    {
        /// <summary>
        /// Formats passed value according to specified format string.
        /// </summary>
        /// <param name="value">Value which has to be convertible to derived class data type</param>
        /// <param name="formatString">Formating string used to format the value</param>
        /// <param name="divisor">Constant used for conversion to correct physical unit</param>
        /// <param name="bias">Constant added to raw value</param>
        /// <returns>Value formated value</returns>
        public override String FormatValue(Object value, String formatString, double divisor, double bias)
        {
            Int64 val = Convert.ToInt64(value);
            if (divisor != 1 || bias != 0)
            {
                if (formatString == "")
                    return ((double)val / divisor + bias).ToString("0.###");
                else
                    return ((double)val / divisor + bias).ToString(formatString);
            }
            else
                return val.ToString(formatString);
        }

        /// <summary>
        /// Extracts value as correctly typed object from binary buffer at given location.
        /// </summary>
        /// <param name="recBinData">Structure with binary data and record instance information.</param>
        /// <param name="dataOffset">Offset to binary buffer.</param>
        /// <param name="value">Output parameter containing extracted value.</param>
        /// <returns>Status of parsing operation</returns>
        public override eParseStatus ParseBinValue(sRecBinData recBinData, int dataOffset, out Object value)
        {
            value = null;
            //Check buffer length
            if (recBinData.BinData.Length < (dataOffset + 8))
                return eParseStatus.binaryBufferTooSmall;
            //Rotate and convert value
            if (recBinData.BigEndian)
                value = BitConverter.ToInt64(Rotate8(recBinData.BinData, dataOffset), 0);
            else
                value = BitConverter.ToInt64(recBinData.BinData, dataOffset);
            return eParseStatus.parseSuccessfull;
        }

        /// <summary>
        /// Formats given value as hex word.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <returns>Value formatted as string</returns>
        public override string FormatHexValue(object value)
        {
            UInt64 val = Convert.ToUInt64(value);
            return val.ToString("X16");
        }

        /// <summary>
        /// Formats given value as series of 1s and 0s, each group of 4 bits separated by specified separator.
        /// </summary>
        /// <param name="value">Value to format</param>
        /// <param name="separator">Used to separate each 4 bits</param>
        /// <returns>Value formatted as string</returns>public override string 
        public override string FormatBinValue(object value, string separator)
        {
            UInt64 val = Convert.ToUInt64(value);
            String res = separator;
            for (UInt64 i = 1; i < UInt64.MaxValue; i = i * 2)
            {
                if ((val & i) > 0)
                    res = "1" + res;
                else
                    res = "0" + res;
                if ((res.Length % 5) == 0)
                    res = separator + res;
                if (i == 0x8000000000000000)
                    break;
            }
            return res;
        }
    }

    /// <summary>
    /// Represents R32 data type for parsing and formatting operations
    /// </summary>
    [Serializable]
    public class DDDTypeR32 : DDDTypeBase
    {
        /// <summary>
        /// Formats passed value according to specified format string.
        /// </summary>
        /// <param name="value">Value which has to be convertible to derived class data type</param>
        /// <param name="formatString">Formating string used to format the value</param>
        /// <param name="divisor">Constant used for conversion to correct physical unit</param>
        /// <param name="bias">Constant added to raw value</param>
        /// <returns>Value formated value</returns>
        public override String FormatValue(Object value, String formatString, double divisor, double bias)
        {
            Single val = Convert.ToSingle(value);
            if (divisor != 1 || bias != 0)
                val = (Single)((double)val / divisor + bias);
            return val.ToString(formatString);
        }

        /// <summary>
        /// Extracts value as correctly typed object from binary buffer at given location.
        /// </summary>
        /// <param name="recBinData">Structure with binary data and record instance information.</param>
        /// <param name="dataOffset">Offset to binary buffer.</param>
        /// <param name="value">Output parameter containing extracted value.</param>
        /// <returns>Status of parsing operation</returns>
        public override eParseStatus ParseBinValue(sRecBinData recBinData, int dataOffset, out Object value)
        {
            value = null;
            //Check buffer length
            if (recBinData.BinData.Length < (dataOffset + 4))
                return eParseStatus.binaryBufferTooSmall;
            //Rotate and convert value
            if (recBinData.BigEndian)
                value = BitConverter.ToSingle(Rotate4(recBinData.BinData, dataOffset), 0);
            else
                value = BitConverter.ToSingle(recBinData.BinData, dataOffset);
            return eParseStatus.parseSuccessfull;
        }
    }

    /// <summary>
    /// Represents R64 data type for parsing and formatting operations
    /// </summary>
    [Serializable]
    public class DDDTypeR64 : DDDTypeBase
    {
        /// <summary>
        /// Formats passed value according to specified format string.
        /// </summary>
        /// <param name="value">Value which has to be convertible to derived class data type</param>
        /// <param name="formatString">Formating string used to format the value</param>
        /// <param name="divisor">Constant used for conversion to correct physical unit</param>
        /// <param name="bias">Constant added to raw value</param>
        /// <returns>Value formated value</returns>
        public override String FormatValue(Object value, String formatString, double divisor, double bias)
        {
            Double val = Convert.ToDouble(value);
            if (divisor != 1 || bias != 0)
                val = val / divisor + bias;
            return val.ToString(formatString);
        }

        /// <summary>
        /// Extracts value as correctly typed object from binary buffer at given location.
        /// </summary>
        /// <param name="recBinData">Structure with binary data and record instance information.</param>
        /// <param name="dataOffset">Offset to binary buffer.</param>
        /// <param name="value">Output parameter containing extracted value.</param>
        /// <returns>Status of parsing operation</returns>
        public override eParseStatus ParseBinValue(sRecBinData recBinData, int dataOffset, out Object value)
        {
            value = null;
            //Check buffer length
            if (recBinData.BinData.Length < (dataOffset + 8))
                return eParseStatus.binaryBufferTooSmall;
            //Rotate and convert value
            if (recBinData.BigEndian)
                value = BitConverter.ToDouble(Rotate8(recBinData.BinData, dataOffset), 0);
            else
                value = BitConverter.ToDouble(recBinData.BinData, dataOffset);
            return eParseStatus.parseSuccessfull;
        }
    }
}
