using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using RefAttrExpressions;

namespace DDDObjects
{
  /// <summary>
  /// Enumeration lists possible origins for DDDVersion object.
  /// </summary>
  public enum DDDVersionOrigin
  {
    /// <summary>
    /// Unknown version origin
    /// </summary>
    Unknonwn = 0, 
    /// <summary>
    /// Version was loaded from DB
    /// </summary>
    DB = 1, 
    /// <summary>
    /// Version was loaded from file
    /// </summary>
    File = 2 
  }
  
  /// <summary>
  /// Class represents one DDD version
  /// with all records, variables,....
  /// </summary>
  [Serializable]
  public class DDDVersion
  {
    /// <summary>
    /// Unique version identifier
    /// </summary>
    public Guid VersionID;

    /// <summary>
    /// User defined string representing the version
    /// </summary>
    public String UserVersion;

    /// <summary>
    /// User comment for the version
    /// </summary>
    public String Comment;

    /// <summary>
    /// Origin of Version object
    /// </summary>
    public DDDVersionOrigin VersionOrigin;

    /// <summary>
    /// Evaluator of referential attributes expressions
    /// </summary>
    public RefAttrEvaluator RefAttrEvaluator;

    /// <summary>
    /// Dictionary of all known records
    /// </summary>
    public Dictionary<int, DDDRecord> Records
    {
      get { return records; }
    }

    private Dictionary<int, DDDRecord> records; //Map of diagnostic records (RecordID is the key)
    private Dictionary<int, DDDVariable> variables; //Map of defined enviromental variables
    private Dictionary<int, DDDRepresentation> representations; //Map of defined representations
    private List<DDDRefAttrMap> refAttrMappings; //List of mappings between referential attributes and environmental variables
    private Dictionary<int, String> deviceCodes; //Dictionary of device codes and device names defined in DDD Version

    /// <summary>
    /// Private constructor, initializes dictionaries
    /// </summary>
    private DDDVersion()
    {
      records = new Dictionary<int, DDDRecord>();
      variables = new Dictionary<int, DDDVariable>();
      representations = new Dictionary<int, DDDRepresentation>();
      refAttrMappings = new List<DDDRefAttrMap>();
      deviceCodes = new Dictionary<int, string>();
    }

    /// <summary>
    /// Creates new object from XML element
    /// </summary>
    /// <param name="verElement">Element containing data for new DDD version</param>
    /// <returns>Created DDD version object</returns>
    /// <exception cref="DDDObjectException">Version creation failed (some attributes missing)</exception>
    public static DDDVersion CreateFromDDDObjXML(XmlElement verElement)
    {
      try
      {
        DDDVersion ver = new DDDVersion();
        ver.VersionID = new Guid(verElement.GetAttribute("VersionID"));
        ver.UserVersion = verElement.GetAttribute("UserVersion");
        ver.Comment = verElement.GetAttribute("Comment");
        String attribute = verElement.GetAttribute("Origin");
        if (attribute != "")
          ver.VersionOrigin = (DDDVersionOrigin)(DDDVersionOrigin.Parse(typeof(DDDVersionOrigin), attribute));
        else
          ver.VersionOrigin = DDDVersionOrigin.Unknonwn;

        return ver;
      }
      catch (Exception ex)
      {
        throw new DDDObjectException(ex, "Failed to create DDDVersion object");
      }
    }

    /// <summary>
    /// Add new record object to DDD version
    /// </summary>
    /// <param name="ID">ID of record definition (ordinal number in DDD)</param>
    /// <param name="rec">Record object (e.g. created from XML)</param>
    public void AddRecord(int ID, DDDRecord rec)
    {
      records.Add(ID, rec);
    }
    /// <summary>
    /// Add new variable object to DDD version
    /// </summary>
    /// <param name="ID">ID of variable in DDD)</param>
    /// <param name="var">Variable object (e.g. created from XML)</param>
    public void AddVariable(int ID, DDDVariable var)
    {
      variables.Add(ID, var);
    }
    /// <summary>
    /// Add new representation object to DDD version
    /// </summary>
    /// <param name="ID">ID of representation definition</param>
    /// <param name="rep">Representation object (e.g. created from XML)</param>
    public void AddRepresentation(int ID, DDDRepresentation rep)
    {
      representations.Add(ID, rep);
    }
    /// <summary>
    /// Returns record object from repository based on specified ID
    /// </summary>
    /// <param name="ID">ID of reguested record</param>
    /// <returns>DDDRecord object with matching ID</returns>
    /// <exception cref="DDDObjectException">Thrown when record with specified ID does not exist.</exception>
    public DDDRecord GetRecord(int ID)
    {
      DDDRecord rec;
      if (records.TryGetValue(ID, out rec))
        return rec;
      else
        throw new DDDObjectException("Record with ID {0} does not exist", ID);
    }

    /// <summary>
    /// Returns event record object with given fault code.
    /// </summary>
    /// <param name="faultCode">Fault code of requested record</param>
    /// <returns>DDDEventRecord with specified fault code</returns>
    /// <exception cref="DDDObjectException">Thrown when record with specified fault code does not exist.</exception>
    public DDDEventRecord GetRecordByFaultCode(int faultCode)
    {
      foreach (KeyValuePair<int, DDDRecord> recPair in records)
        if (recPair.Value.GetType() == typeof(DDDEventRecord)
            && ((DDDEventRecord)recPair.Value).FaultCode == faultCode)
          return (DDDEventRecord)recPair.Value;
      throw new DDDObjectException("Event record with fault code {0} does not exist", faultCode);
    }

    /// <summary>
    /// Returns variable object from repository based on specified ID
    /// </summary>
    /// <param name="ID">ID of reguested variable</param>
    /// <returns>DDDVariable object with matching ID</returns>
    /// <exception cref="DDDObjectException">Thrown when variable with specified ID does not exist.</exception>
    public DDDVariable GetVariable(int ID)
    {
      DDDVariable var;
      if (variables.TryGetValue(ID, out var))
        return var;
      else
        throw new DDDObjectException("Variable with ID {0} does not exist", ID);
    }

    /// <summary>
    /// Obtains DDD Variable object using given complete variable name 
    /// (Name path from parent, separated by separator)
    /// </summary>
    /// <param name="completeName">Complete variable name starting from top level parent</param>
    /// <param name="separator">Separator used in variable name</param>
    /// <returns>DDDVariable object according to given nama</returns>
    public DDDVariable GetVariableByName(String completeName, String separator)
    {
      //Obtain parts of variable name
      String[] nameParts = completeName.Split(new String[] { separator }, StringSplitOptions.RemoveEmptyEntries);
      //Check for minimum name length
      if (nameParts.Length < 1)
        throw new Exception("Invalid variable name " + completeName);

      //Find top level variable registered in DDD Version
      DDDVariable topLevelVar = null;
      foreach (KeyValuePair<int, DDDVariable> idVarPair in variables)
        if (idVarPair.Value.Name == nameParts[0] && idVarPair.Value.ParentVariable == null)
          topLevelVar = idVarPair.Value;
      if (topLevelVar == null)
        throw new Exception(String.Format("Top level {0} variable not found", nameParts[0]));

      if (nameParts.Length == 1)
        return topLevelVar; //Variable found

      //Search for child variables for each additional name part
      DDDVariable parentVar = topLevelVar;
      for (int i = 1; i < nameParts.Length; i++)
      {
        if (parentVar.GetType() != typeof(DDDStructure))
          throw new Exception(String.Format("Variable with name {0} not found", completeName));
        //Search child variables on this level
        DDDVariable var = null;
        foreach(DDDVariable chilVar in ((DDDStructure)parentVar).ChildVars)
          if (chilVar.Name == nameParts[i])
          {
            var = chilVar;
            break;
          }
        if (var == null)
          throw new Exception(String.Format("Variable with name {0} not found", completeName));
        parentVar = var;
      }
      //Return found variable
      return parentVar;
    }

    /// <summary>
    /// Returns elementary variable object from repository based on specified ID
    /// </summary>
    /// <param name="ID">ID of reguested elementary variable</param>
    /// <returns>DDDElemVar object with matching ID</returns>
    /// <exception cref="DDDObjectException">Thrown when elementary variable with specified ID does not exist.</exception>
    public DDDElemVar GetElementaryVariable(int ID)
    {
      DDDVariable var;
      if (variables.TryGetValue(ID, out var))
        if (var.GetType() == typeof(DDDElemVar))
          return (DDDElemVar)var;
      //Elementary variable not found, throw an exception
      throw new DDDObjectException("Elementary variable with ID {0} does not exist", ID);
    }

    /// <summary>
    /// Returns representation object from repository based on specified ID
    /// </summary>
    /// <param name="ID">ID of reguested representation</param>
    /// <returns>DDDRepresentation object with matching ID</returns>
    /// <exception cref="DDDObjectException">Thrown when representation with specified ID does not exist.</exception>
    public DDDRepresentation GetRepresentation(int ID)
    {
      DDDRepresentation repres;
      if (representations.TryGetValue(ID, out repres))
        return repres;
      else
        throw new DDDObjectException("Representation with ID {0} does not exist", ID);
    }

    /// <summary>
    /// Method prepares language - dependent properties for selected language.
    /// </summary>
    /// <param name="currentLang">selected language</param>
    /// <param name="fallbackLang">language to use if selected language is not available</param>
    /// <returns>True if all properties are defined for specified language</returns>
    public bool SetCurrentLanguage(Languages currentLang, Languages fallbackLang)
    {
      bool result = true;
      //Set language for all representations
      foreach(KeyValuePair<int, DDDRepresentation> repres in representations)
        result &= repres.Value.SetCurrentLanguage(currentLang, fallbackLang);
      //Set language for all variables
      foreach(KeyValuePair<int, DDDVariable> var in variables)
        result &= var.Value.SetCurrentLanguage(currentLang, fallbackLang);
      //Set language for all records
      foreach (KeyValuePair<int, DDDRecord> rec in records)
        result &= rec.Value.SetCurrentLanguage(currentLang, fallbackLang);
      return result;
    }

    /// <summary>
    /// Reads elements describing mapping of referential attributes to elementary variables
    /// as defined for this version
    /// </summary>
    /// <param name="DDDObjdDoc">XML document with DDD objects</param>
    public void ReadRefAttrMappings(XmlDocument DDDObjdDoc)
    {
      try
      {
        XmlNodeList elemList;
        //Clear old referential attributes mappings
        refAttrMappings.Clear();
        //Create new evaluator for referential attributes expressions
        RefAttrEvaluator = new RefAttrEvaluator("VER" + VersionID.ToString("N"));

        //Select all referential attributes mappings
        elemList = DDDObjdDoc.SelectNodes("//RefAttrMap");
        foreach (XmlElement refAttrMapElem in elemList)
        {
          DDDRefAttrMap refAttrMap = new DDDRefAttrMap();
          refAttrMap.MappingIndex = Convert.ToInt32(refAttrMapElem.GetAttribute("MappingIndex"));
          refAttrMap.RefAttrID = Convert.ToInt32(refAttrMapElem.GetAttribute("RefAttrID"));
          if (refAttrMapElem.GetAttribute("VariableID") != "")
            refAttrMap.VariableID = Convert.ToInt32(refAttrMapElem.GetAttribute("VariableID"));
          refAttrMap.Expression = refAttrMapElem.GetAttribute("Expression");
          //Check if expression is defined and add it to the evaluator
          if (refAttrMap.Expression != "")
            RefAttrEvaluator.AddRefAttrExpression(refAttrMap.MappingIndex, refAttrMap.Expression);
          //Add mapping to internal list
          refAttrMappings.Add(refAttrMap);
        }
        //Compile the evaluator of expressions if necessary
        if (RefAttrEvaluator.GetRefAttrExprCount() > 0)
          RefAttrEvaluator.Compile();
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to read mappings of referential attributes", ex);
      }
    }

    /// <summary>
    /// Finds mapping of referential attributes to elementary variables (and their offset)
    /// for all inputs of all records.
    /// Uses internal varIDRefAttrMap dictionary.
    /// </summary>
    public void FindRefAttrMappings()
    {
      foreach (KeyValuePair<int, DDDRecord> recPair in records)
        recPair.Value.FindRefAttrMappings(refAttrMappings, RefAttrEvaluator);
    }

    /// <summary>
    /// Add one mapping of device code to device name
    /// </summary>
    /// <param name="deviceCode">Numeric code of the device</param>
    /// <param name="deviceName">Name of the device</param>
    public void AddDeviceCode(int deviceCode, String deviceName)
    {
      //Check if code already exists
      if (deviceCodes.ContainsKey(deviceCode))
        deviceCodes[deviceCode] = deviceName; //Replace with new name
      else
        deviceCodes.Add(deviceCode, deviceName); //Add new device code and name
    }

    /// <summary>
    /// Retrieves name of the device based on device code from device codes mapping
    /// </summary>
    /// <param name="deviceCode">Code of the requested device</param>
    /// <returns>Name of the device</returns>
    public String GetDeviceName(int deviceCode)
    {
      String name;
      if (deviceCodes.TryGetValue(deviceCode, out name))
        return name;
      else
        return null;
    }
  }
}
