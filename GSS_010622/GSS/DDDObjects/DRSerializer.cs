using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Collections.Generic;
using FleetHierarchies;

namespace DDDObjects
{
  /// <summary>
  /// Class provides the posibility to transfer diagnostic records through byte stream together with necessary DDD helper objects
  /// </summary>
  public class DRSerializer : DRSerializerBase
  {
    #region Private members
    BinaryFormatter binFormatter;
    #endregion //Private members

    #region Construction, initialization
    /// <summary>
    /// Creates new diagnostic record serializer object
    /// </summary>
    /// <param name="dddHelper">Helper class for DDD objects</param>
    /// <param name="stream">Stream to read from or write to</param>
    /// <param name="output">Indicates that records will be written into the stream</param>
    public DRSerializer(DDDHelper dddHelper, Stream stream, bool output)
      : base(dddHelper, stream, output)
    {
      binFormatter = new BinaryFormatter();
    }
    #endregion //Construction, initialization

    #region Public methods
    /// <summary>
    /// Writes given record into the stream, registers used DDDVersion and TUInstance
    /// </summary>
    /// <param name="record">Record to write</param>
    public override void WriteRecordToStream(DiagnosticRecord record)
    {
      RememberUsedDDDObj(record, false);
      //Serialize record
      binFormatter.Serialize(zipStream, record);
    }

    /// <summary>
    /// Writes all used DDD objects into the stream preceded by special flag
    /// </summary>
    public override void WriteUsedDDDObjects()
    {
      //Create and write flag record
      DiagnosticRecord record = new DiagnosticRecord();
      record.RecordDefID = -1;
      binFormatter.Serialize(zipStream, record);
      //Serialize DDDVersions
      binFormatter.Serialize(zipStream, usedDDDVer);
      //Serialize TUInstances
      binFormatter.Serialize(zipStream, usedTUInst);
      //Serialize fleet hierarchies
      binFormatter.Serialize(zipStream, usedHierVersions);
    }

    /// <summary>
    /// Writes one record read from given SqlDataReader into the stream
    /// </summary>
    /// <param name="reader">Reader with result set from database</param>
    /// <returns>Returns true, if record was read from DB and written into the stream</returns>
    public bool WriteDBRecordToStream(SqlDataReader reader)
    {
      DiagnosticRecord record = ReadDBRecord(reader);
      if (record == null)
        return false;
      WriteRecordToStream(record);
      return true;
    }

    /// <summary>
    /// Deserializes next record from given stream, remembers used DDD objects
    /// </summary>
    /// <returns>Deserialized diagnostic record</returns>
    public override DiagnosticRecord ReadRecordFromStream()
    {
      DiagnosticRecord record;
      try
      {
        record = (DiagnosticRecord)binFormatter.Deserialize(zipStream);
      }
      catch (SerializationException)
      {
        return null;
      }
      if (record.RecordDefID == -1)
        return null;
      else
      {
        //Remember used version and TUInst 
        RememberUsedDDDObj(record, true);
        return record;
      }
    }

    /// <summary>
    /// Reads stored DDD objects from stream. In case not all necessary objects are available in the stream, exception is thrown.
    /// </summary>
    public override void ReadUsedDDDObjects()
    {
      try
      {
        //Deserialize and store all available DDD objects
        Dictionary<Guid, DDDVersion> dddVersions = (Dictionary<Guid, DDDVersion>)binFormatter.Deserialize(zipStream);
        Dictionary<Guid, TUInstance> tuInstances = (Dictionary<Guid, TUInstance>)binFormatter.Deserialize(zipStream);
        Dictionary<int, FleetHierarchy> fleetHierarchies = (Dictionary<int, FleetHierarchy>)binFormatter.Deserialize(zipStream);
        DDDHelper.StoreObjects(dddVersions, tuInstances, fleetHierarchies);
      }
      catch (Exception)
      {
      }
      CheckUsedDDDObjAvailable();
    }
    #endregion //Public methods

    #region Helper methods
    private DiagnosticRecord ReadDBRecord(SqlDataReader reader)
    {
      if (!reader.Read())
        return null;
      DiagnosticRecord record = new DiagnosticRecord();
      record.TUInstanceID = reader.GetGuid(0);
      record.RecordInstID = reader.GetInt64(1);
      record.DDDVersionID = reader.GetGuid(2);
      record.RecordDefID = reader.GetInt32(3);

      record.StartTime = reader.GetDateTime(4);
      if (!reader.IsDBNull(5))
        record.EndTime = reader.GetDateTime(5);
      else
        record.EndTime = DateTime.MinValue;
      record.LastModified = reader.GetDateTime(6);
      record.ParseStatusID = reader.GetInt32(7);

      if (!reader.IsDBNull(8))
        record.BinData = reader.GetSqlBinary(8).Value;
      else
        record.BinData = null;
      record.BigEndian = reader.GetBoolean(9);
      if (!reader.IsDBNull(10))
        record.Source = reader.GetString(10);
      else
        record.Source = "";
      if (!reader.IsDBNull(11))
        record.HierarchyItemID = reader.GetInt32(11);
      else
        record.HierarchyItemID = -1;

      if (!reader.IsDBNull(12))
        record.ImportTime = reader.GetDateTime(12);
      else
        record.ImportTime = DateTime.MinValue;
      if (!reader.IsDBNull(13))
        record.AcknowledgeTime = reader.GetDateTime(13);
      else
        record.AcknowledgeTime = DateTime.MinValue;
      if (!reader.IsDBNull(14))
        record.VehicleNumber = reader.GetInt16(14);
      else
        record.VehicleNumber = -1;
      if (!reader.IsDBNull(15))
        record.DeviceCode = reader.GetInt16(15);
      else
        record.DeviceCode = -1;

      return record;
    }
    #endregion //Helper methods
  }
}