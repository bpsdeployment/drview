using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using DDDObjects;

namespace FleetHierarchies
{
  /// <summary>
  /// Helper class for fleet hierarchies.
  /// Can contain various versions of hierarchy tree.
  /// Provides methods to access hierarchy items.
  /// </summary>
  public class FleetHierarchyHelper
  {
    #region Private members
    DDDHelper dddHelper = null; //DDDHelper object
    SqlCommand dbCommand = null;
    Dictionary<String, int> itemIDs = new Dictionary<string, int>();
    Dictionary<int, String> itemNames = new Dictionary<int, string>();
    Dictionary<int, FleetHierarchy> verHierarchies = new Dictionary<int, FleetHierarchy>();
    SortedList<TimeRange, FleetHierarchy> rangeHierrchies = new SortedList<TimeRange, FleetHierarchy>();
    int maxItemID = -1;
    int maxVersionID = -1;
    #endregion //Private members

    #region Construction initialization
    /// <summary>
    /// Hierarchy helper constructor
    /// </summary>
    /// <param name="dddHelper">parent DDD Helper object</param>
    public FleetHierarchyHelper(DDDHelper dddHelper)
    {
      //Store parent helper object
      this.dddHelper = dddHelper;
      //Prepare SQL command
      dbCommand = new SqlCommand("Hierarchy.GetFleetHerarchy");
      dbCommand.CommandType = CommandType.StoredProcedure;
      dbCommand.Parameters.Add("@maxKnownItemID", SqlDbType.Int);
      dbCommand.Parameters.Add("@maxKnownVersionID", SqlDbType.Int);
      dbCommand.Parameters.Add("@reqVersionID", SqlDbType.Int);
      dbCommand.Parameters.Add("@reqValidTime", SqlDbType.DateTime);
      dbCommand.Parameters.Add("@hierarchy", SqlDbType.Xml, 2147483647).Direction = ParameterDirection.Output;
      dbCommand.Parameters.Add("@validFrom", SqlDbType.DateTime).Direction = ParameterDirection.Output;
      dbCommand.Parameters.Add("@validTo", SqlDbType.DateTime).Direction = ParameterDirection.Output;
      dbCommand.Parameters.Add("@versionID", SqlDbType.Int).Direction = ParameterDirection.Output;
    }
    #endregion //Construction initialization

    #region Public methods
    /// <summary>
    /// Load current version of fleet hierarchy from DB
    /// if not already loaded
    /// </summary>
    public void LoadCurrentHierarchy()
    {
      LoadHierarchyFromDB(null, null, maxVersionID);
    }

    /// <summary>
    /// Reloads current version of fleet hierarchy from DB
    /// even if version with the same ID is already loaded
    /// </summary>
    public void ReloadCurrentHierarchy()
    {
      LoadHierarchyFromDB(null, null, null);
    }

    /// <summary>
    /// Force reloads current version of fleet hierarchy from DB
    /// </summary>
    public void ForceReloadCurrentHierarchy()
    {
        itemIDs.Clear();
        itemNames.Clear();
        LoadHierarchyFromDB(null, null, null);
    }

    /// <summary>
    /// Obtain current version of fleet hierarchy from internal repository or DB.
    /// Throws exception if no current hierarchy is available.
    /// </summary>
    /// <returns>FleetHierarchy object with highest known version</returns>
    public FleetHierarchy ObtainCurrentHierarchy()
    {
      try
      {
        if (maxVersionID < 0 || !verHierarchies.ContainsKey(maxVersionID) || verHierarchies[maxVersionID] == null)
          LoadCurrentHierarchy();

        return verHierarchies[maxVersionID];
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to obtain current hierarchy version", ex);
      }
    }

    /// <summary>
    /// Obtain specified version of hierarchy from repository or DB.
    /// Throws exception if specified version is not available.
    /// </summary>
    /// <param name="versionID">ID of version to obtain</param>
    /// <returns>Hierarchy object</returns>
    public FleetHierarchy ObtainHierarchy(int versionID)
    {
      try
      {
        if (!verHierarchies.ContainsKey(versionID))
          LoadHierarchyFromDB(versionID, null, maxVersionID);

        return verHierarchies[versionID];
      }
      catch (Exception ex)
      {
        throw new Exception(String.Format("Failed to obtain hierarchy version {0}", versionID), ex);
      }
    }

    /// <summary>
    /// Obtains hierarchy version from repository or DB which is valid for given time.
    /// Throws exception if specified version is not available.
    /// </summary>
    /// <param name="validTime">Time when the version has to be valid</param>
    /// <returns>Version object</returns>
    public FleetHierarchy ObtainHierarchy(DateTime validTime)
    {
      try
      {
        TimeRange range = new TimeRange(validTime, validTime);
        if (!rangeHierrchies.ContainsKey(range))
          LoadHierarchyFromDB(null, validTime, maxVersionID);
        
        return rangeHierrchies[range];
      }
      catch (Exception ex)
      {
        throw new Exception(String.Format("Failed to obtain hierarchy valid at {0}", validTime), ex);
      }
    }

    /// <summary>
    /// Get current version of fleet hierarchy from repository.
    /// </summary>
    /// <returns>Hierarchy object or null</returns>
    public FleetHierarchy GetCurrentFleetHierarchy()
    {
      if (!verHierarchies.ContainsKey(maxVersionID))
        return null;
      return verHierarchies[maxVersionID];
    }

    /// <summary>
    /// Get fleet hierarchy with specified version from repository
    /// </summary>
    /// <param name="versionID">ID of version to retrieve</param>
    /// <returns>Hierarchy object or null</returns>
    public FleetHierarchy GetHierarchy(int versionID)
    {
      if (!verHierarchies.ContainsKey(versionID))
        return null;
      return verHierarchies[versionID];
    }

    /// <summary>
    /// Get fleet hierarchy valid at specified time from repository
    /// </summary>
    /// <param name="validTime">Time, when version has to be valid</param>
    /// <returns>Hierarchy object or null</returns>
    public FleetHierarchy GetHierarchy(DateTime validTime)
    {
      TimeRange range = new TimeRange(validTime, validTime);
      if (!rangeHierrchies.ContainsKey(range))
        return null;
      return rangeHierrchies[range];
    }

    /// <summary>
    /// Get name of hierarchy item with specified ID.
    /// </summary>
    /// <param name="itemID">ID of requested item</param>
    /// <returns>Item name or null</returns>
    public String GetItemName(int itemID)
    {
      String itemName;
      if (!itemNames.TryGetValue(itemID, out itemName))
        return null;
      return itemName;
    }

    /// <summary>
    /// Gets ID of given hierarchy item.
    /// </summary>
    /// <param name="itemName">Name of requested hierarchy item</param>
    /// <returns>Item ID or null</returns>
    public int GetItemID(String itemName)
    {
      int itemID;
      if (!itemIDs.TryGetValue(itemName, out itemID))
        return -1;
      return itemID;
    }

    /// <summary>
    /// Adds fleet hierarchy version to internal repository
    /// </summary>
    /// <param name="hierarchy"></param>
    public void AddHierarchyVersion(FleetHierarchy hierarchy)
    {
      //Check if version is not already in repository
      if (verHierarchies.ContainsKey(hierarchy.VersionID))
        verHierarchies[hierarchy.VersionID] = hierarchy; //Replace existing hierarchy
      else
        verHierarchies.Add(hierarchy.VersionID, hierarchy); //Add new to repository
      //Update maximum version ID
      maxVersionID = Math.Max(maxVersionID, hierarchy.VersionID);
      //Add version to time range repository
      TimeRange validRange = new TimeRange(hierarchy.ValidFrom, hierarchy.ValidTo);
      if (!rangeHierrchies.ContainsKey(validRange))
        rangeHierrchies.Add(validRange, hierarchy); //Add to repository
      else if (rangeHierrchies[validRange] != null && rangeHierrchies[validRange].VersionID <= hierarchy.VersionID)
      { //Valid range already exists in repository, but with lower or same version ID, remove it and replace by new one
        //Delete all equal time ranges from repository
        while(rangeHierrchies.ContainsKey(validRange))
          rangeHierrchies.Remove(validRange);
        //Add hierarchy to the repository
        rangeHierrchies.Add(validRange, hierarchy);
      }
      //Check item names, add new ones to internal repository
      foreach (KeyValuePair<String, FleetHierarchyItem> itemPair in hierarchy.GetHierarchyItemNamesDictionary())
        if (!itemIDs.ContainsKey(itemPair.Key) && itemPair.Value.ItemID != -1)
          itemIDs.Add(itemPair.Key, itemPair.Value.ItemID);
      //Check item IDs, add new ones to internal repository
      foreach (KeyValuePair<int, FleetHierarchyItem> itemPair in hierarchy.GetHierarchyItemIDsDictionary())
        if (!itemNames.ContainsKey(itemPair.Key) && itemPair.Key != -1)
          itemNames.Add(itemPair.Key, itemPair.Value.Name);
    }
    #endregion //Public methods

    #region Helper methods
    /// <summary>
    /// Loads all available unknown hierarchy items and IDs from given data reader.
    /// Strores items in repository.
    /// </summary>
    /// <param name="reader">SQL data reader with hierarchy items</param>
    private void LoadHierarchyItems(SqlDataReader reader)
    {
      //Read all rows
      while (reader.Read())
        if (!itemIDs.ContainsKey(reader.GetString(0)))
        { //Add item to repositories
          itemIDs.Add(reader.GetString(0), reader.GetInt32(2));
          itemNames.Add(reader.GetInt32(2), reader.GetString(0));
          //Set max item ID
          maxItemID = Math.Max(maxItemID, reader.GetInt32(2));
        }
    }

    /// <summary>
    /// Load hierarchy version from DB acording to given parameters.
    /// If versionID is not null, specified version is loaded.
    /// If versionID is null and validTime is not, highest version valid at given time is loaded.
    /// If both versionID and validTime are null, latest version is loaded.
    /// </summary>
    private void LoadHierarchyFromDB(int? versionID, DateTime? validTime, int? maxKnownVersionID)
    {
      try
      {
        using (SqlConnection dbConnection = dddHelper.GetDBConnection())
        {
          dbCommand.Connection = dbConnection;
          //Prepare SQL command parameters
          dbCommand.Parameters["@maxKnownItemID"].Value = maxItemID;
          dbCommand.Parameters["@maxKnownVersionID"].Value = maxKnownVersionID;
          dbCommand.Parameters["@reqVersionID"].Value = versionID;
          dbCommand.Parameters["@reqValidTime"].Value = validTime;

          //Execute command and obtain reader for hierarchy items
          using (SqlDataReader reader = dbCommand.ExecuteReader())
          {
            LoadHierarchyItems(reader);
          }
          //Create Hierarchy from returned XML document
          if (dbCommand.Parameters["@hierarchy"].Value != DBNull.Value)
          { //Hierarchy version is returned, build object
            //Create XML document
            XmlDocument doc = new XmlDocument();
            doc.LoadXml((String)dbCommand.Parameters["@hierarchy"].Value);
            //Set root element attributes
            if (dbCommand.Parameters["@validFrom"].Value != DBNull.Value)
              doc.DocumentElement.SetAttribute("validFrom", ((DateTime)dbCommand.Parameters["@validFrom"].Value).ToString("s"));
            else
              doc.DocumentElement.SetAttribute("validFrom", DateTime.MinValue.ToString("s"));
            if (dbCommand.Parameters["@validTo"].Value != DBNull.Value)
              doc.DocumentElement.SetAttribute("validTo", ((DateTime)dbCommand.Parameters["@validTo"].Value).ToString("s"));
            else
              doc.DocumentElement.SetAttribute("validTo", DateTime.MaxValue.ToString("s"));
            doc.DocumentElement.SetAttribute("versionID", dbCommand.Parameters["@versionID"].Value.ToString());
            //Set itemID attribute for each element (hierarchy item) in document
            SetXmlItemID(doc.DocumentElement);
            //Build hierarchy object
            FleetHierarchy hierarchy = new FleetHierarchy(doc);
            //Add hierarchy to repository
            AddHierarchyVersion(hierarchy);
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to load version of fleet hierarchy from DB", ex);
      }
    }

    /// <summary>
    /// Sets itemID attribute for given element (hierarchy item) and all children
    /// according to item name (name attribute). Called recursively.
    /// </summary>
    /// <param name="itemElem">Element for which itemID attribute is set</param>
    private void SetXmlItemID(XmlElement itemElem)
    {
      //Find id in repository by name
      int itemID;
      if (itemIDs.TryGetValue(itemElem.GetAttribute("name"), out itemID))
        itemElem.SetAttribute("itemID", itemID.ToString()); //Set itemID attribute
      //Call recursively for all child nodes
      foreach (XmlElement childItem in itemElem.ChildNodes)
        SetXmlItemID(childItem);
    }
    #endregion //Helper methods

    #region Public static helpers
    /// <summary>
    /// Returns XML string with selected fleet hierarchy item names.
    /// </summary>
    /// <param name="selectedNames">Selected fleet items in the form name paths in hierarchy tree (path separator /, item separated by ;)</param>
    /// <returns>XML string with selected names</returns>
    public static String GetHierarchyNamesXml(String selectedNames)
    {
      //Check for empty list
      if (selectedNames == null || selectedNames == String.Empty)
        return String.Empty;
      //Split into individual item name paths
      String[] items = selectedNames.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
      //Prepare XML document
      XmlDocument doc = new XmlDocument();
      doc.AppendChild(doc.CreateElement("items"));
      XmlElement itemElem;
      foreach (String item in items)
      { //Iterate over all item name paths
        //Create element with item name
        itemElem = doc.CreateElement("item");
        itemElem.SetAttribute("name", item.Substring(item.LastIndexOf("/") + 1)); //Separate only the last name from the path
        doc.DocumentElement.AppendChild(itemElem);
      }
      return doc.InnerXml;
    }
    #endregion //Public static helpers
  }

  #region TimeRange class
  /// <summary>
  /// Class representing time range with comparisons.
  /// </summary>
  internal class TimeRange : IComparable<TimeRange>
  {
    public DateTime timeFrom;
    public DateTime timeTo;

    /// <summary>
    /// Construct time range object
    /// </summary>
    public TimeRange(DateTime timeFrom, DateTime timeTo)
    {
      if (timeFrom > timeTo)
        throw new Exception(String.Format("Invalid time range from {0} to {1}", timeFrom, timeTo));
      this.timeFrom = timeFrom;
      this.timeTo = timeTo;
    }

    /// <summary>
    /// Compare to another time range
    /// </summary>
    public int CompareTo(TimeRange range)
    {
      if (range.timeFrom < timeFrom && range.timeTo < timeTo)
        return 1; //This greater than range
      if (range.timeFrom > timeFrom && range.timeTo > timeTo)
        return -1; //This smaller than range
      return 0; //Same
    }
  }
  #endregion //TimeRange class
}
