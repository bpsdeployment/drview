using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using FleetHierarchies;

namespace DDDObjects
{
  #region Delegate definitions
  /// <summary>
  /// Delegate represents method which sets current language for the visualisation component
  /// </summary>
  /// <param name="language">Currently selected language</param>
  public delegate void NewLanguageSelectedEventHandler(Languages language);

  /// <summary>
  /// Delegate for methods which handles DDDVersionNeeded event. Method has to decide
  /// which source shall be used and fill in appropriate parameters.
  /// </summary>
  /// <param name="sender">Sender of the event</param>
  /// <param name="verNeededArgs">Parameters specifying version origin</param>
  [System.Obsolete("Use ConnectionString property of DDDHelper to specify DB connection instead of version needed event")]
  public delegate void DDDVersionNeededEventHandler(DDDHelper sender, DDDVersionNeededEventArgs verNeededArgs);
  #endregion //Delegate definitions
  
  /// <summary>
  /// Helper class which wraps all necessary operations performed on DDD objects
  /// </summary>
  public partial class DDDHelper : Component
  {
    #region Private members
    private Languages selectedLanguage;
    private Languages fallbackLanguage;
    private DDDRepository dddRepository;
    private Dictionary<Guid, TUType> tuTypes;
    private Dictionary<Guid, TUInstance> tuInstances;
    private Dictionary<int, String> refAttributes;
    private String timeFormat = "yyyy-MM-dd HH:mm:ss.fff";
    private String timeFormatGraph = "yy-MM-dd HH:mm:ss";
    private int timeOffset = 1;
    private bool bUseLocTime = true;
    private String connString = String.Empty; //Connection string to DB
    private FleetHierarchyHelper hierarchyHelper = null; //Helper object for fleet hierarchy
    #endregion //Private members

    #region Static members
    /// <summary>
    /// String that is used to represent True value for boolean variables
    /// </summary>
    public static String BoolTrueSymbol = "1";
    /// <summary>
    /// String that is used to represent False value for boolean variables
    /// </summary>
    public static String BoolFalseSymbol = "0";
    /// <summary>
    /// String that is used to separate parts of title for variables
    /// </summary>
    public static String TitleSeparator = ".";
    #endregion //Static members

    #region Public events
    /// <summary>
    /// Event handler for event raised when new language is selected as current language
    /// </summary>
    public event NewLanguageSelectedEventHandler NewLanguageSelected;
    #endregion //Public events

    #region Public properties
    /// <summary>
    /// Currently selecte language used for titles
    /// </summary>
    public Languages CurrentLanguage
    {
      get { return selectedLanguage; }
    }

    /// <summary>
    /// Format string used for time formatting
    /// </summary>
    public String TimeFormat
    {
      get { return timeFormat; }
      set { timeFormat = value; }
    }

    /// <summary>
    /// Format string used for time formatting in graphs
    /// </summary>
    public String TimeFormatGraph
    {
      get { return timeFormatGraph; }
      set { timeFormatGraph = value; }
    }

    /// <summary>
    /// Offse from UTC time. All time values are converted using this offset.
    /// Time offset is used only when bUseLocTime is false.
    /// </summary>
    public int TimeOffset
    {
      get { return timeOffset; }
      set { timeOffset = value; }
    }

    /// <summary>
    /// Local system time zone shall be used for converting time values.
    /// </summary>
    public bool UseLocalTime
    {
      get { return bUseLocTime; }
      set { bUseLocTime = value; }
    }

    /// <summary>
    /// Dictionary containing all TUInstance objects loaded from database
    /// </summary>
    public Dictionary<Guid, TUInstance> TUInstances
    {
      get { return tuInstances; }
    }

    /// <summary>
    /// Connection string to CDDB database, used to load configuration data
    /// </summary>
    public String ConnectionString
    {
      get { return connString; }
      set { connString = value; }
    }

    /// <summary>
    /// Helper object for fleet hierarchy
    /// </summary>
    public FleetHierarchyHelper HierarchyHelper
    {
      get { return hierarchyHelper; }
    }
    #endregion //Public properties

    #region Construction, initialization
    /// <summary>
    /// Default constructor
    /// </summary>
    public DDDHelper()
    {
      InitializeComponent();
      Initialize();
    }

    /// <summary>
    /// Standard constructor
    /// </summary>
    /// <param name="container">Container for helper component</param>
    public DDDHelper(IContainer container)
    {
      container.Add(this);
      InitializeComponent();
      Initialize();
    }

    /// <summary>
    /// Initializes instance data
    /// </summary>
    private void Initialize()
    {
      selectedLanguage = Languages.english;
      fallbackLanguage = Languages.Default;
      dddRepository = new DDDRepository();
      tuTypes = new Dictionary<Guid, TUType>();
      tuInstances = new Dictionary<Guid, TUInstance>();
      refAttributes = new Dictionary<int, string>();
      hierarchyHelper = new FleetHierarchyHelper(this);
    }
    #endregion //Construction, initialization

    #region Public methods
    /// <summary>
    /// Method prepares language dependent properties for all DDDVersions
    /// (titles of records, variables, representations...).
    /// </summary>
    /// <param name="currentLang">selected language</param>
    /// <param name="fallbackLang">language to use if selected language is not available</param>
    /// <returns>True if all properties are defined for specified language</returns>
    public bool SetCurrentLanguage(Languages currentLang, Languages fallbackLang)
    {
      bool res;
      this.selectedLanguage = currentLang;
      this.fallbackLanguage = fallbackLang;
      res = dddRepository.SetCurrentLanguage(currentLang, fallbackLang);
      //Raise NewLanguageSelected event
      if (NewLanguageSelected != null)
        NewLanguageSelected(currentLang);
      return res;
    }

    /// <summary>
    /// Loads specified DDD Version from DB and creates tree of DDD objects.
    /// Version is added to the repository and current language is set.
    /// </summary>
    /// <param name="versionID">Version identification</param>
    /// <returns>Created DDDVersion object</returns>
    private DDDVersion LoadDDDVersionFromDB(Guid versionID)
    {
      SqlCommand getDDDProc = new SqlCommand();
      XmlReader dddReader;
      XmlDocument dddDoc = new XmlDocument();
      XmlNode dddElement;
      DDDVersion version;
      try
      {
        using (SqlConnection dbConnection = GetDBConnection())
        {
          //Set command parameters
          getDDDProc.Connection = dbConnection;
          getDDDProc.CommandType = CommandType.StoredProcedure;
          getDDDProc.CommandText = "GetDDDObjectsXML";
          getDDDProc.Parameters.Add("@VersionID", SqlDbType.UniqueIdentifier).Value = versionID;
          //Execute command and obtain XmlReader with result
          dddReader = getDDDProc.ExecuteXmlReader();
          //Create Xml document with version description
          //First create root element - result from DB is XML fragment
          dddElement = dddDoc.CreateElement("DDDObjects");
          dddDoc.AppendChild(dddElement);
          //Append result from DB to root element
          while ((dddElement = dddDoc.ReadNode(dddReader)) != null)
            dddDoc.DocumentElement.AppendChild(dddElement);
          dddReader.Close();
          //Create DDDVersion object from the document
          version = DDDRepository.CreateObjectsFromDDDObjXML(dddDoc);
          //Add version to repository
          AddDDDVersion(version);
        }
      }
      catch (Exception ex)
      {
        throw new DDDObjectException(ex, "Failed to load DDD Version {0} from DB", versionID);
      }
      finally
      { //Clean up
        getDDDProc.Dispose();
      }
      return version;
  }

  /// <summary>
  /// Forces to load information about all known TUTypes and TUInstances from database
  /// and creates corresponding objects
  /// </summary>
  public void ForceLoadTUObjectsFromDB()
  {
      tuTypes.Clear();
      tuInstances.Clear();
      LoadTUObjectsFromDB();
  }

  /// <summary>
  /// Loads information about all known TUTypes and TUInstances from database
  /// and creates corresponding objects
  /// </summary>
  public void LoadTUObjectsFromDB()
  {
      SqlCommand getTUProc = new SqlCommand();
      XmlReader tuReader;
      XmlDocument tuDoc = new XmlDocument();
      XmlNode tuElement;
      XmlNodeList elemList;
      TUInstance instance;
      TUType type;
      try
      {
        using (SqlConnection dbConnection = GetDBConnection())
        {
          //Set command parameters
          getTUProc.Connection = dbConnection;
          getTUProc.CommandType = CommandType.StoredProcedure;
          getTUProc.CommandText = "GetTUObjectsXML";
          //Execute command and obtain XmlReader with result
          tuReader = getTUProc.ExecuteXmlReader();
          //Create Xml document with objects descriptions
          //First create root element - result from DB is XML fragment
          tuElement = tuDoc.CreateElement("TUObjects");
          tuDoc.AppendChild(tuElement);
          //Append result from DB to root element
          while ((tuElement = tuDoc.ReadNode(tuReader)) != null)
            tuDoc.DocumentElement.AppendChild(tuElement);
          tuReader.Close();
          //Create TUType objects from the document
          elemList = tuDoc.SelectNodes("//TUType");
          foreach (XmlElement typeElem in elemList)
          {
            type = TUType.CreateFromTUObjXML(typeElem);
            AddTUType(type);
          }
          //Create TUInstance objects from the document
          elemList = tuDoc.SelectNodes("//TUInstance");
          foreach (XmlElement instanceElem in elemList)
          {
            instance = TUInstance.CreateFromTUObjXML(instanceElem, this);
            AddTUInstance(instance);
          }
        }
      }
      catch (Exception ex)
      {
        throw new DDDObjectException(ex, "Failed to load TU objects from DB");
      }
      finally
      { //Clean up
        getTUProc.Dispose();
      }
    }

    /// <summary>
    /// Loads list of all referntial attributes from DB and stores in internal array.
    /// </summary>
    public void LoadRefAttrListFromDB()
    {
      SqlCommand getRefAttrProc = new SqlCommand();
      SqlDataReader refAttrReader = null;
      try
      {
        using (SqlConnection dbConnection = GetDBConnection())
        {
          //Set command parameters
          getRefAttrProc.Connection = dbConnection;
          getRefAttrProc.CommandType = CommandType.StoredProcedure;
          getRefAttrProc.CommandText = "GetRefAttributesList";
          //Execute command and obtain data reader with result
          refAttrReader = getRefAttrProc.ExecuteReader();
          //Iterate over all rows read from database
          int refAttrID;
          String refAttrName;
          while (refAttrReader.Read())
          {
            refAttrID = refAttrReader.GetInt32(0);
            refAttrName = refAttrReader.GetString(1);
            if (refAttributes.ContainsKey(refAttrID))
              refAttributes[refAttrID] = refAttrName;
            else
              refAttributes.Add(refAttrID, refAttrName);
          }
        }
      }
      catch (Exception ex)
      {
        throw new DDDObjectException(ex, "Failed to load list of Referential Attributes from DB");
      }
      finally
      { //Clean up
        if (refAttrReader != null)
          refAttrReader.Dispose();
        getRefAttrProc.Dispose();
      }
    }

    /// <summary>
    /// Returns specified DDDVersion object.
    /// If version does not exist in repository, tries to load it from DB using stored connection string.
    /// </summary>
    /// <param name="versionID">ID of required version</param>
    /// <returns>Object representing specified DDD version</returns>
    public DDDVersion GetVersion(Guid versionID)
    {
      try
      { //Try to find version in repository
        return dddRepository.GetVersion(versionID);
      }
      catch (Exception)
      { } //Version does not exist in repository
      //Try to load version from database
      return LoadDDDVersionFromDB(versionID);
    }

    /// <summary>
    /// Checks if version with given ID exists in the repostiory.
    /// </summary>
    /// <param name="versionID">ID to check</param>
    /// <returns>True if specified version exists in repository</returns>
    public bool CheckVersionExists(Guid versionID)
    {
      return dddRepository.VersionDictionary.ContainsKey(versionID);
    }

    /// <summary>
    /// Returns specified DDDRecord object from repository
    /// </summary>
    /// <param name="versionID">DDD Version identification</param>
    /// <param name="recordDefID">Record identification</param>
    /// <returns>Record object</returns>
    public DDDRecord GetRecord(Guid versionID, int recordDefID)
    {
      return GetVersion(versionID).GetRecord(recordDefID);
    }

    /// <summary>
    /// Returns specified DDDVariable object from repository
    /// </summary>
    /// <param name="versionID">DDD Version identification</param>
    /// <param name="variableID">Variable identification</param>
    /// <returns>Variable object</returns>
    public DDDVariable GetVariable(Guid versionID, int variableID)
    {
      return GetVersion(versionID).GetVariable(variableID);
    }

    /// <summary>
    /// Returns elementary variable object from repository based on specified ID
    /// </summary>
    /// <param name="versionID">DDD Version identification</param>
    /// <param name="variableID">ID of requested elementary variable</param>
    /// <returns>DDDElemVar object with matching ID</returns>
    public DDDElemVar GetElementaryVariable(Guid versionID, int variableID)
    {
      return GetVersion(versionID).GetElementaryVariable(variableID);
    }


    /// <summary>
    /// Returns specified DDDRepresentation object from repository
    /// </summary>
    /// <param name="versionID">DDD Version identification</param>
    /// <param name="represID">Representation identification</param>
    /// <returns>Representation object</returns>
    public DDDRepresentation GetRepresentation(Guid versionID, int represID)
    {
      return GetVersion(versionID).GetRepresentation(represID);
    }

    /// <summary>
    /// Returns title of specified DDDRecord object in selected language
    /// </summary>
    /// <param name="versionID">DDD Version identification</param>
    /// <param name="recordDefID">Record identification</param>
    /// <returns>Title of the record</returns>
    public String GetRecordTitle(Guid versionID, int recordDefID)
    {
      return GetVersion(versionID).GetRecord(recordDefID).Title;
    }

    /// <summary>
    /// Returns title of specified DDDVariable object in selected language
    /// </summary>
    /// <param name="versionID">DDD Version identification</param>
    /// <param name="variableID">Variable identification</param>
    /// <returns>Title of the variable</returns>
    public String GetVariableTitle(Guid versionID, int variableID)
    {
      return GetVersion(versionID).GetVariable(variableID).Title;
    }
    /// <summary>
    /// Adds new TUType object to the repository
    /// </summary>
    /// <param name="type">TUType object to add</param>
    public void AddTUType(TUType type)
    {
      TUType tmpType;
      //Check if type already exists
      if (tuTypes.TryGetValue(type.TUTypeID, out tmpType))
        tuTypes.Remove(type.TUTypeID); //Remove old type
      tuTypes.Add(type.TUTypeID, type);
    }
    /// <summary>
    /// Retrieves TU Type object from repository
    /// </summary>
    /// <param name="ID">ID of TU Type</param>
    /// <returns>TUType object with specified ID</returns>
    public TUType GetTUType(Guid ID)
    {
      TUType type;
      if (tuTypes.TryGetValue(ID, out type))
        return type;
      else
        throw new DDDObjectException("TU Type with ID {0} does not exist", ID);
    }

    /// <summary>
    /// Check if TU Type exists in repository
    /// </summary>
    /// <param name="ID">ID of the type</param>
    /// <returns>True if the type exists in repository</returns>
    public bool CheckTypeExists(Guid ID)
    {
      return tuTypes.ContainsKey(ID);
    }

    /// <summary>
    /// Retrieves TU Type object from repository
    /// If not found in repository, tries to load TU objects from db
    /// using stored db connection string
    /// </summary>
    /// <param name="ID">ID of requested TU Type</param>
    /// <returns>TUType object with specified ID</returns>
    public TUType GetTUTypeDB(Guid ID)
    {
      try
      { //Try to obtain TU type from repository
        return GetTUType(ID);
      }
      catch(Exception)
      {}
      //TU Type not found in repository, try to load from DB
      LoadTUObjectsFromDB();
      //Return type from repository
      return GetTUType(ID);
    }

    /// <summary>
    /// Adds new TUInstance object to the repository
    /// </summary>
    /// <param name="instance">TUInstance object to add</param>
    public void AddTUInstance(TUInstance instance)
    {
      TUInstance tmpInstance;
      //Check if instance already exists
      if (tuInstances.TryGetValue(instance.TUInstanceID, out tmpInstance))
        tuInstances.Remove(instance.TUInstanceID); //Remove old instance
      tuInstances.Add(instance.TUInstanceID, instance);
    }
    /// <summary>
    /// Retrieves TU Instance object from repository
    /// </summary>
    /// <param name="ID">ID of TU Instance</param>
    /// <returns>TUInstance object with specified ID</returns>
    public TUInstance GetTUInstance(Guid ID)
    {
      TUInstance instance;
      if (tuInstances.TryGetValue(ID, out instance))
        return instance;
      else
        throw new DDDObjectException("TU Instance with ID {0} does not exist", ID);
    }

    /// <summary>
    /// Check if TU Instance exists in repository
    /// </summary>
    /// <param name="ID">ID of the instance</param>
    /// <returns>True if the instance exists in repository</returns>
    public bool CheckInstanceExists(Guid ID)
    {
      return tuInstances.ContainsKey(ID);
    }

    /// <summary>
    /// Retrieves TU Instance object from repository
    /// If not found in repository, tries to load TU objects from db
    /// using stored db connection string
    /// </summary>
    /// <param name="ID">ID of requested TU Instance</param>
    /// <returns>TUInstance object with specified ID</returns>
    public TUInstance GetTUInstanceDB(Guid ID)
    {
      try
      { //Try to obtain TU instance from repository
        return GetTUInstance(ID);
      }
      catch (Exception)
      { }
      //TU Instance not found in repository, try to load from DB
      LoadTUObjectsFromDB();
      //Return type from repository
      return GetTUInstance(ID);
    }

    /// <summary>
    /// Adds new DDD version object to internal repository.
    /// If version already exists in repository, no action is performed.
    /// </summary>
    /// <param name="version">DDD version object to add</param>
    public void AddDDDVersion(DDDVersion version)
    {
      if (!dddRepository.VersionDictionary.ContainsKey(version.VersionID))
      { //Version does not exist yet
        //Set language
        version.SetCurrentLanguage(selectedLanguage, fallbackLanguage);
        //Add to repository
        dddRepository.AddVersion(version);
      }
    }

    /// <summary>
    /// Parses block of binary data of one record instance
    /// </summary>
    /// <param name="recBinData">Structure with binary data and instance information</param>
    /// <param name="parsedValues">List of parsed values. New list is created and returned.</param>
    /// <returns>Result of parsing operation</returns>
    public eParseStatus ParseBinRecord(sRecBinData recBinData, out List<sParsedVarValue> parsedValues)
    {
      parsedValues = null;
      try
      {
        return GetRecord(recBinData.DDDVersionID, recBinData.RecordDefID).ParseBinData(recBinData, out parsedValues);
      }
      catch (Exception)
      { //Record object could not be found
        return eParseStatus.unknownRecType;
      }
    }

    /// <summary>
    /// Parses values of referential attributes from block of binary data for one record
    /// </summary>
    /// <param name="recBinData">Structure with binary data and instance information</param>
    /// <param name="parsedValues">List of parsed values. New list is created and returned.</param>
    /// <returns>Result of parsing operation</returns>
    public eParseStatus ParseBinRecordRefAttr(sRecBinData recBinData, out List<sParsedRefAttrValue> parsedValues)
    {
      parsedValues = null;
      try
      {
        DDDVersion ver = GetVersion(recBinData.DDDVersionID);
        DDDRecord rec = ver.GetRecord(recBinData.RecordDefID);
        return rec.ParseRefAttributes(recBinData, ver.RefAttrEvaluator, out parsedValues);
      }
      catch (Exception)
      { //Record object could not be found
        return eParseStatus.unknownRecType;
      }
    }

    /// <summary>
    /// Saves DDDVersion object with whole graph of child objects to file
    /// </summary>
    /// <param name="VersionID">Version identification</param>
    /// <param name="directory">Path to which version object shall be stored</param>
    public void SaveDDDVersionToFile(Guid VersionID, String directory)
    {
      //Obtain version from repository
      DDDVersion version = dddRepository.GetVersion(VersionID);
      //Create formatter
      BinaryFormatter formatter = new BinaryFormatter();
      //Create file name
      String fileName = "DDD_" + version.VersionID.ToString() + ".ser";
      fileName = Path.Combine(directory, fileName);
      //Open file stream
      FileStream outFile = new FileStream(fileName, FileMode.Create, FileAccess.Write);
      //Serialize version to file
      formatter.Serialize(outFile, version);
      //Close stream
      outFile.Close();
      outFile.Dispose();
    }

    /// <summary>
    /// Loads complete DDDVersion object form file
    /// </summary>
    /// <param name="fileName">Name of file with version</param>
    /// <returns>Loaded DDDVersion object</returns>
    public DDDVersion LoadDDDVersionFromFile(String fileName)
    {
      //Create formatter
      BinaryFormatter formatter = new BinaryFormatter();
      //Open file stream
      FileStream inFile = new FileStream(fileName, FileMode.Open, FileAccess.Read);
      //Deserialize version
      DDDVersion version = (DDDVersion)formatter.Deserialize(inFile);
      //Close stream
      inFile.Close();
      inFile.Dispose();
      //Store version in repository
      AddDDDVersion(version);
      //Return version object
      return version;
    }

    /// <summary>
    /// Saves all available DDDVersions to specified directory
    /// </summary>
    /// <param name="directory">Path to target directory</param>
    /// <param name="originMask">Save only versions of specified origin, when unkonw, saves all versions</param>
    public void SaveAllDDDVersions(String directory, DDDVersionOrigin originMask)
    {
      Dictionary<Guid, DDDVersion> verDict = dddRepository.VersionDictionary;
      foreach (KeyValuePair<Guid, DDDVersion> versionPair in verDict)
        if ((originMask == DDDVersionOrigin.Unknonwn) || (versionPair.Value.VersionOrigin == originMask))
          SaveDDDVersionToFile(versionPair.Key, directory);
    }

    /// <summary>
    /// Loads all saved DDD versions from specified directory
    /// </summary>
    /// <param name="directory">Path to source directory</param>
    public void LoadDDDVersionsFromDir(String directory)
    {
      String[] files;
      //Obtain list of version files
      files = Directory.GetFiles(directory, "DDD*.ser", SearchOption.TopDirectoryOnly);
      //Load all files
      foreach (String file in files)
        LoadDDDVersionFromFile(file);
    }

    /// <summary>
    /// Saves all available TUType and TUInstance objects to file on disk
    /// </summary>
    /// <param name="directory">Target directory</param>
    public void SaveTUObjectsToFile(String directory)
    {
      //Create object array with tuTypes and TUInstances dictionary
      Object[] tuObjects = new Object[2];
      tuObjects[0] = tuTypes;
      tuObjects[1] = tuInstances;
      //Create formatter
      BinaryFormatter formatter = new BinaryFormatter();
      //Create file name
      String fileName = "TUObjects.ser";
      fileName = Path.Combine(directory, fileName);
      //Open file stream
      FileStream outFile = new FileStream(fileName, FileMode.Create, FileAccess.Write);
      //Serialize object array
      formatter.Serialize(outFile, tuObjects);
      //Close stream
      outFile.Close();
      outFile.Dispose();
    }

    /// <summary>
    /// Check if specified directory contains file TUObjects.ser 
    /// and deserializes tuTypes and tuInstances dictionaries from this file.
    /// </summary>
    /// <param name="directory">Source direcory</param>
    /// <returns>True if deserialization succeeds, false when file does not exist</returns>
    public bool LoadTUObjectsFromFile(String directory)
    {
      //Create formatter
      BinaryFormatter formatter = new BinaryFormatter();
      //Create file name
      String fileName = "TUObjects.ser";
      fileName = Path.Combine(directory, fileName);
      //Check if file exists
      if (!File.Exists(fileName))
        return false;
      //Open file stream
      FileStream inFile = new FileStream(fileName, FileMode.Open, FileAccess.Read);
      //Deserialize object array from file
      Object[] tuObjects = (Object[])formatter.Deserialize(inFile);
      //Store dictionaries
      tuTypes = (Dictionary<Guid, TUType>)tuObjects[0];
      tuInstances = (Dictionary<Guid, TUInstance>)tuObjects[1];
      //Close stream
      inFile.Close();
      inFile.Dispose();
      return true;
    }
    
    /// <summary>
    /// Stores in repository all passed objects (DDDVersions, TUInstance, TUType, FleetHierarchies), which are not there yet
    /// </summary>
    /// <param name="dddVersions">Versions to store</param>
    /// <param name="tuObjects">TU Instances (and TUTypes) to store</param>
    /// <param name="fleetHierarchies">Fleet hierarchies to store</param>
    public void StoreObjects(
      Dictionary<Guid, DDDVersion> dddVersions, 
      Dictionary<Guid, TUInstance> tuObjects, 
      Dictionary<int, FleetHierarchy> fleetHierarchies)
    {
      //Store all unknown DDDVersions
      foreach (KeyValuePair<Guid, DDDVersion> verPair in dddVersions)
        AddDDDVersion(verPair.Value);
      //Store all TUInstances and TUTypes
      foreach (KeyValuePair<Guid, TUInstance> instPair in tuObjects)
        if (!tuInstances.ContainsKey(instPair.Key))
        {//instance does not exist in repository
          //Check if TUType is already known
          if (tuTypes.ContainsKey(instPair.Value.TUTypeID))
            instPair.Value.TUTypeObject = GetTUType(instPair.Value.TUTypeID); //Known TUType, use stored TUType object
          else
            AddTUType(instPair.Value.TUTypeObject); //Unknown TUType, store type referenced by TUInstance
          //Store TUInstance object
          AddTUInstance(instPair.Value);
        }
      //Store fleet hierarchies in hierarchy helper
      foreach (KeyValuePair<int, FleetHierarchy> hierPair in fleetHierarchies)
        HierarchyHelper.AddHierarchyVersion(hierPair.Value);
    }

    /// <summary>
    /// Obtains connection to CDDB database from internal connection string.
    /// In case of invalid connection string or connection failure, exception is thrown.
    /// </summary>
    /// <returns>DB Connection object</returns>
    public SqlConnection GetDBConnection()
    {
      if (ConnectionString == null || ConnectionString == String.Empty)
        throw new Exception("No connection to DB specified");
      try
      {
        SqlConnection dbConn = new SqlConnection(ConnectionString);
        dbConn.Open();
        return dbConn;
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to open connection to DB", ex);
      }
    }

    /// <summary>
    /// Return true if connection to DB is specified
    /// </summary>
    public bool IsDBConnectionSpecified()
    {
      return (ConnectionString != null) && (ConnectionString != String.Empty);
    }
    #endregion //Public methods

    #region Public helpers
    /// <summary>
    /// Formats name of TUInstance togehter with user DDD version in square brackets
    /// </summary>
    /// <param name="tuInstance">TUInstance object used in source name</param>
    /// <param name="dddVersion">DDDVersion object used in source name</param>
    /// <returns>Formatted source name</returns>
    public String FormatSourceName(TUInstance tuInstance, DDDVersion dddVersion)
    {
      return "[" + tuInstance.TUName + ", " + dddVersion.UserVersion + "]";
    }

    /// <summary>
    /// Converts time value from UTC time to time zone specified by user.
    /// (Either by UseLocalTime or by TimeOffset)
    /// </summary>
    /// <param name="time">UTC time to convert</param>
    /// <returns>Converted local time</returns>
    public DateTime UTCToUserTime(DateTime time)
    {
      if (time == DateTime.MinValue)
        return time;
      if (UseLocalTime)
        return time.ToLocalTime();
      else
        return time.AddHours(TimeOffset);
    }

    /// <summary>
    /// Converts time value from time zone specified by uset to UTC time.
    /// (Either by UseLocalTime or by TimeOffset)
    /// </summary>
    /// <param name="time">Local time to convert</param>
    /// <returns>Converted UTC time</returns>
    public DateTime UserTimeToUTC(DateTime time)
    {
      if (time == DateTime.MinValue)
        return time;
      if (UseLocalTime)
        return time.ToUniversalTime();
      else
        return time.AddHours(-TimeOffset);
    }

    /// <summary>
    /// Formats time value using user specified format string
    /// </summary>
    /// <param name="time">Time value to format</param>
    /// <returns>Formatted time (date)</returns>
    public String FormatTime(DateTime time)
    {
      return time.ToString(TimeFormat);
    }
    #endregion //Public helpers
  }

  #region DDDVersionNeededEventArgs
  /// <summary>
  /// Arguments passed by DDDHelper to DDDVerNeeded event.
  /// Handler of the event has to decide, which source for the version to use
  /// and fill in appropriate parameters.
  /// </summary>
  [System.Obsolete("Use ConnectionString property of DDDHelper to specify DB connection instead of version needed event")]
  public class DDDVersionNeededEventArgs : EventArgs
  {
    /// <summary>
    /// Types of sources for new DDDVersions
    /// </summary>
    public enum VersionSource
    {
      /// <summary>
      /// No source available
      /// </summary>
      None = 0,
      /// <summary>
      /// Use database connection
      /// </summary>
      DB = 1,
      /// <summary>
      /// Load from previously serialized file
      /// </summary>
      SerFile = 2,
      /// <summary>
      /// Load from DDD XML document (not supported yet)
      /// </summary>
      DDDDocument = 3,
      /// <summary>
      /// Use directly passed DDDVersion object
      /// </summary>
      Object = 4
    }

    /// <summary>
    /// Identifier of needed DDDVersion
    /// </summary>
    public Guid DDDVersionID;

    /// <summary>
    /// Source from which DDDVersion object shall be created
    /// </summary>
    public VersionSource Source = VersionSource.None;

    /// <summary>
    /// Connection to database from which to load DDDVersion
    /// </summary>
    public SqlConnection DBConnection = null;

    /// <summary>
    /// Name of serialized file from which to load DDDVersion
    /// </summary>
    public String FileName = null;

    /// <summary>
    /// XML document containing DDDVersion to parse
    /// </summary>
    public XmlDocument DDDDocument = null;

    /// <summary>
    /// Object containing requested DDDVersion
    /// </summary>
    public DDDVersion VersionObject = null;

    /// <summary>
    /// Source, where version was already searched
    /// </summary>
    public VersionSource SourceAlreadyTried = VersionSource.None;
  }
  #endregion //DDDVersionNeededEventArgs
}
