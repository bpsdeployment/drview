using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace DDDObjects
{
  /// <summary>
  /// Transforms DDD document into DDDObjects document, which can be processed by DDDRepository object.
  /// Values of all IDs are made up by autoincrement counter.
  /// </summary>
  class DDDTransform
  {
    #region Private members
    private Dictionary<String, int> represDataSizeMap; //maps representation to the size of its data
    private Dictionary<String, int> represAlignMap; //maps representation to the alignement of its data
    private Dictionary<String, int> represNameIDMap; //maps all representation names to their ids
    private Dictionary<String, int> varNameIDMap; //maps names of variables to their IDs
    private AutoincrementCounter counter; //ID counter
    #endregion //Private members

    #region Construction
    /// <summary>
    /// Default constructor
    /// </summary>
    public DDDTransform()
    {
    }
    #endregion //Construction

    /// <summary>
    /// Transforms input DDD Document to DDDObjects document parsable by DDDVersion object.
    /// All IDs are made up by autoincrement counter, thus the result is not exactly the same
    /// as when parsing in DB
    /// </summary>
    /// <param name="VersionID">Identification of transformed DDD version</param>
    /// <param name="DDDDocument">DDD Document</param>
    /// <returns>DDDObjects document</returns>
    public XmlDocument TransformDDD(Guid VersionID, XmlDocument DDDDocument)
    {
      #region Declarations
      //Prepare helper classes
      counter = new AutoincrementCounter();
      represDataSizeMap = new Dictionary<string, int>();
      represAlignMap = new Dictionary<string, int>();
      represNameIDMap = new Dictionary<string, int>();
      varNameIDMap = new Dictionary<string, int>();
      //Create output document
      XmlDocument outDoc = new XmlDocument();
      //Create root element
      XmlElement outRootElem = outDoc.CreateElement("DDDObjects");
      outDoc.AppendChild(outRootElem);
      //Prepare variables
      XmlDocument inDoc = DDDDocument;
      XmlElement outElem;
      XmlElement inElem;
      XmlNodeList elemList;
      #endregion //Declarations

      #region DDDVersion
      //DDDVersion element
      inElem = (XmlElement)inDoc.SelectSingleNode("//VersionInfo");
      outElem = outDoc.CreateElement("DDDVersion");
      outElem.SetAttribute("VersionID", VersionID.ToString());
      outElem.SetAttribute("UserVersion", inElem.GetAttribute("DDDVersion"));
      outElem.SetAttribute("Origin", "File");
      outRootElem.AppendChild(outElem);
      #endregion //DDDVersion

      #region DeviceCodes
      //DeviceCodes elements
      elemList = inDoc.SelectNodes("//DeviceCode");
      foreach (XmlElement elem in elemList)
      {
        outElem = outDoc.CreateElement("DeviceCode");
        //Copy attributes
        foreach (XmlAttribute attribute in elem.Attributes)
          outElem.SetAttribute(attribute.Name, attribute.Value);
        outRootElem.AppendChild(outElem);
      }
      #endregion //DDDVersion

      #region Representations
      //SimpleRepres
      elemList = inDoc.SelectNodes("//SimpleRepres");
      foreach(XmlElement elem in elemList)
      {
        outElem = outDoc.CreateElement(elem.LocalName);
        //Copy attributes
        foreach (XmlAttribute attribute in elem.Attributes)
          outElem.SetAttribute(attribute.Name, attribute.Value);
        //Modify attributes
        outElem.RemoveAttribute("DataTypeName");
        outElem.SetAttribute("RepresID", counter.Count.ToString());
        int DataTypeID = (int)ElemDataType.Parse(typeof(ElemDataType), elem.GetAttribute("DataTypeName"));
        outElem.SetAttribute("DataTypeID", DataTypeID.ToString());
        represDataSizeMap.Add(outElem.GetAttribute("Name"), DDDRepository.GetElemTypeByteLength((ElemDataType)DataTypeID));
        represAlignMap.Add(outElem.GetAttribute("Name"), DDDRepository.GetElemTypeByteLength((ElemDataType)DataTypeID));
        represNameIDMap.Add(outElem.GetAttribute("Name"), counter.LastCount);
        outRootElem.AppendChild(outElem);
      }
      //BinaryRepres
      elemList = inDoc.SelectNodes("//BinaryRepres");
      foreach (XmlElement elem in elemList)
      {
        outElem = outDoc.CreateElement(elem.LocalName);
        //Copy attributes
        foreach (XmlAttribute attribute in elem.Attributes)
          outElem.SetAttribute(attribute.Name, attribute.Value);
        //Modify attributes
        outElem.SetAttribute("RepresID", counter.Count.ToString());
        outElem.SetAttribute("DataTypeID", ((int)(ElemDataType.U8)).ToString());
        represDataSizeMap.Add(outElem.GetAttribute("Name"), Convert.ToInt32(outElem.GetAttribute("Length")));
        represAlignMap.Add(outElem.GetAttribute("Name"), 1); //Binary representations always have alignement 1
        represNameIDMap.Add(outElem.GetAttribute("Name"), counter.LastCount);
        outRootElem.AppendChild(outElem);
      }
      //EnumRepres and BitsetRepres
      elemList = inDoc.SelectNodes("//EnumRepres | //BitsetRepres");
      foreach (XmlElement elem in elemList)
      {
        outElem = outDoc.CreateElement(elem.LocalName);
        //Copy attributes
        foreach (XmlAttribute attribute in elem.Attributes)
          outElem.SetAttribute(attribute.Name, attribute.Value);
        //Modify attributes
        outElem.RemoveAttribute("DataTypeName");
        outElem.SetAttribute("RepresID", counter.Count.ToString());
        int DataTypeID = (int)ElemDataType.Parse(typeof(ElemDataType), elem.GetAttribute("DataTypeName"));
        outElem.SetAttribute("DataTypeID", DataTypeID.ToString());
        represDataSizeMap.Add(outElem.GetAttribute("Name"), DDDRepository.GetElemTypeByteLength((ElemDataType)DataTypeID));
        represAlignMap.Add(outElem.GetAttribute("Name"), DDDRepository.GetElemTypeByteLength((ElemDataType)DataTypeID));
        represNameIDMap.Add(outElem.GetAttribute("Name"), counter.LastCount);
        //EnumItems
        XmlNodeList enumItems = elem.SelectNodes("EnumItem | BitRepres");
        foreach (XmlElement enumItem in enumItems)
        {
          XmlElement newEnumItem = outDoc.CreateElement(enumItem.LocalName);
          newEnumItem.SetAttribute("Title", enumItem.GetAttribute("Text"));
          if (enumItem.GetAttribute("Value") != "")
            newEnumItem.SetAttribute("Value", enumItem.GetAttribute("Value"));
          if (enumItem.GetAttribute("Mask") != "")
            newEnumItem.SetAttribute("Mask", enumItem.GetAttribute("Mask"));
          int languageID = (int)Languages.Parse(typeof(Languages), enumItem.GetAttribute("Language"));
          newEnumItem.SetAttribute("LanguageID", languageID.ToString());
          outElem.AppendChild(newEnumItem);
        }
        outRootElem.AppendChild(outElem);
      }
      #endregion //Representations

      #region Variables
      //Variables
      elemList = inDoc.SelectNodes("//Variables/Variable | //Variables/Structure | //Variables/Trigger");
      foreach (XmlElement elem in elemList)
      {
        int size = 0;
        int alignement = 0;
        int parentOffset = 0;
        int topLevelOffset = 0;
        Object ownerVarIndex = null;
        Object ownerVarID = null;
        int varID = counter.Count;
        //Align variable
        AlignVar(elem, out size, out alignement);
        //Offset variable
        OffsetVar(elem, ref parentOffset, ref topLevelOffset, ref ownerVarIndex, 0);
        //Add to document with all children
        AppendVar(elem, outDoc, outRootElem, varID, varID, ownerVarID);
        //Add top level to map
        varNameIDMap.Add(elem.GetAttribute("Name"), varID);
      }
      #endregion //Variables

      #region Records
      //Records
      List<XmlElement> inputList = new List<XmlElement>(); //List with input elements
      elemList = inDoc.SelectNodes("//EventRecord | //TraceRecord | //SnapRecord");
      int recordDefID = 0;
      foreach (XmlElement elem in elemList)
      {
        String attribute;
        XmlElement recElement = outDoc.CreateElement("Record");
        recElement.SetAttribute("RecordDefID", recordDefID.ToString());
        //Record type
        switch (elem.LocalName)
        {
          case "EventRecord":
            recElement.SetAttribute("RecordTypeID", "1");
            break;
          case "TraceRecord":
            recElement.SetAttribute("RecordTypeID", "2");
            break;
          case "SnapRecord":
            recElement.SetAttribute("RecordTypeID", "3");
            break;
        }
        recElement.SetAttribute("Name", elem.GetAttribute("Name"));
        attribute = elem.GetAttribute("Severity");
        if (attribute != "")
          recElement.SetAttribute("SeverityID", ((int)(SeverityType.Parse(typeof(SeverityType), attribute))).ToString());
        int triggerVarID = 0;
        varNameIDMap.TryGetValue(elem.GetAttribute("Trigger"), out triggerVarID);
        recElement.SetAttribute("TriggerVarID", triggerVarID.ToString());
        //Trigger type
        attribute = elem.GetAttribute("TriggerReaction");
        if (attribute != "")
          recElement.SetAttribute("TriggerTypeID", ((int)(TriggerSensitivity.Parse(typeof(TriggerSensitivity), attribute))).ToString());
        //Additional attributes
        attribute = elem.GetAttribute("LastRecs");
        if (attribute != "")
          recElement.SetAttribute("LastRecs", attribute);
        attribute = elem.GetAttribute("FaultCode");
        if (attribute != "")
          recElement.SetAttribute("FaultCode", attribute);
        attribute = elem.GetAttribute("SubSeverity");
        if (attribute != "")
          recElement.SetAttribute("SubSeverity", attribute);
        attribute = elem.GetAttribute("Code");
        if (attribute != "")
          recElement.SetAttribute("Code", attribute); 
        recElement.SetAttribute("Comment", elem.GetAttribute("Comment"));
        //Append title elements
        XmlNodeList titles = elem.SelectNodes("Title");
        foreach (XmlElement title in titles)
        {
          XmlElement newTitle = outDoc.CreateElement("Title");
          int languageID = (int)Languages.Parse(typeof(Languages), title.GetAttribute("Language"));
          newTitle.SetAttribute("LanguageID", languageID.ToString());
          newTitle.SetAttribute("Text", title.GetAttribute("Text"));
          recElement.AppendChild(newTitle);
        }
        //Add record to document
        outRootElem.AppendChild(recElement);
        //Create input elements
        XmlNodeList inputs = elem.SelectNodes("Input | HistorizedInput");
        int inputIndex = 0;
        foreach (XmlElement input in inputs)
        {
          XmlElement newInput = outDoc.CreateElement("Input");
          newInput.SetAttribute("RecordDefID", recordDefID.ToString());
          newInput.SetAttribute("InputIndex", inputIndex.ToString());
          inputIndex++;
          int variableID;
          varNameIDMap.TryGetValue(input.GetAttribute("VarName"), out variableID);
          newInput.SetAttribute("VariableID", variableID.ToString());
          attribute = input.GetAttribute("BeforeStart");
          if (attribute != "")
            newInput.SetAttribute("BeforeStart", attribute);
          attribute = input.GetAttribute("PastStart");
          if (attribute != "")
            newInput.SetAttribute("PastStart", attribute);
          attribute = input.GetAttribute("BeforeEnd");
          if (attribute != "")
            newInput.SetAttribute("BeforeEnd", attribute);
          attribute = input.GetAttribute("PastEnd");
          if (attribute != "")
            newInput.SetAttribute("PastEnd", attribute);
          attribute = input.GetAttribute("SamplingPeriod");
          if (attribute != "")
            newInput.SetAttribute("SamplingPeriodms", attribute);
          //Add input to list
          inputList.Add(newInput);
        }
        recordDefID++; //Increment record ID
      }
      //Append all inputs elements to document
      foreach (XmlElement input in inputList)
        outRootElem.AppendChild(input);
      #endregion //Records

      return outDoc;
    }

    #region Var helper methods
    private void AlignVar(XmlElement varElem, out int size, out int alignement)
    {
      if (varElem.LocalName == "Variable")
      { //Simple variable - get size from data type
        represDataSizeMap.TryGetValue(varElem.GetAttribute("RepresName"), out size);
        represAlignMap.TryGetValue(varElem.GetAttribute("RepresName"), out alignement);
      }
      else if (varElem.LocalName == "Trigger")
      { //Trigger variable - always bool
        size = 1;
        alignement = 1;
      }
      else
      { //Structure
        size = 0;
        alignement = 1;
        XmlNodeList childVars = varElem.SelectNodes("Variable | Structure");
        int childSize;
        int childAlignement;
        //Compute size and alignement from all child variables
        foreach (XmlElement chilVar in childVars)
        { 
          AlignVar(chilVar, out childSize, out childAlignement);
          //round up size to child alignement
          size = ((size + childAlignement - 1) / childAlignement) * childAlignement;
          size += childSize;
          alignement = Math.Max(alignement, childAlignement);
        }
        //Round up size according to alignement
        size = ((size + alignement - 1) / alignement) * alignement;
      }
      //Add size and alignement attributes
      varElem.SetAttribute("Size", size.ToString());
      varElem.SetAttribute("Alignement", alignement.ToString());
    }

    private void OffsetVar(XmlElement varElem, ref int parentOffset, ref int topLevelOffset, ref Object ownerVarIndex, int nestLevel)
    {
      int size = Convert.ToInt32(varElem.GetAttribute("Size"));
      int alignement = Convert.ToInt32(varElem.GetAttribute("Alignement"));
      //Round up offsets to alignement
      parentOffset = ((parentOffset + alignement - 1) / alignement) * alignement;
      topLevelOffset = ((topLevelOffset + alignement - 1) / alignement) * alignement;
      //Create attributes
      varElem.SetAttribute("ParentOffset", parentOffset.ToString());
      varElem.SetAttribute("TopLevelOffset", topLevelOffset.ToString());
      if (ownerVarIndex != null)
        varElem.SetAttribute("OwnerVarIndex", ownerVarIndex.ToString());
      varElem.SetAttribute("NestLevel", nestLevel.ToString());
      //Call method for each child
      int childParOffs = 0;
      int childTopOffs = topLevelOffset;
      Object childOwnerVarIndex = new Int32();
      XmlNodeList childVars = varElem.SelectNodes("Variable | Structure");
      foreach (XmlElement chilVar in childVars)
        OffsetVar(chilVar, ref childParOffs, ref childTopOffs, ref childOwnerVarIndex, nestLevel+1);
      //Add size to offsets
      parentOffset += size;
      topLevelOffset += size;
      if (ownerVarIndex != null)
        ownerVarIndex = (int)ownerVarIndex + 1;
    }

    private void AppendVar(XmlElement srcVarElem, XmlDocument outDocument, XmlElement outRootElem, int varID, int topLevelVarID, Object ownerVarID)
    {
      String attribute;
      //Create variable element and set appropriate attributes
      XmlElement outVar = outDocument.CreateElement("Variable");
      outVar.SetAttribute("VariableID", varID.ToString());
      if (ownerVarID != null)
        outVar.SetAttribute("OwnerVariableID", ownerVarID.ToString());
      attribute = srcVarElem.GetAttribute("OwnerVarIndex");
      if (attribute != "")
        outVar.SetAttribute("OwnerVarIndex", attribute);
      outVar.SetAttribute("Name", srcVarElem.GetAttribute("Name"));
      int represID;
      if (represNameIDMap.TryGetValue(srcVarElem.GetAttribute("RepresName"), out represID))
        outVar.SetAttribute("RepresID", represID.ToString());
      outVar.SetAttribute("TopLevelVarID", topLevelVarID.ToString());
      outVar.SetAttribute("NestLevel", srcVarElem.GetAttribute("NestLevel"));
      if (srcVarElem.LocalName != "Trigger")
        outVar.SetAttribute("Size", srcVarElem.GetAttribute("Size"));
      outVar.SetAttribute("Alignement", srcVarElem.GetAttribute("Alignement"));
      outVar.SetAttribute("ParentOffset", srcVarElem.GetAttribute("ParentOffset"));
      outVar.SetAttribute("TopLevelOffset", srcVarElem.GetAttribute("TopLevelOffset"));
      outVar.SetAttribute("Comment", srcVarElem.GetAttribute("Comment"));
      //Append title elements
      XmlNodeList titles = srcVarElem.SelectNodes("Title");
      foreach(XmlElement title in titles)
      {
        XmlElement newTitle = outDocument.CreateElement("Title");
        int languageID = (int)Languages.Parse(typeof(Languages), title.GetAttribute("Language"));
        newTitle.SetAttribute("LanguageID", languageID.ToString());
        newTitle.SetAttribute("Text", title.GetAttribute("Text"));
        outVar.AppendChild(newTitle);
      }
      //Append variable element to document
      outRootElem.AppendChild(outVar);
      //Recursively call the method for all child variables
      XmlNodeList childVars = srcVarElem.SelectNodes("Variable | Structure");
      foreach (XmlElement chilVar in childVars)
        AppendVar(chilVar, outDocument, outRootElem, counter.Count, topLevelVarID, varID);
    }
    #endregion //Var helper methods
  }

  #region Counter class
  /// <summary>
  /// Represents autoincremental counter
  /// </summary>
  internal class AutoincrementCounter
  {
    protected int counter = 0;
    public int Count
    {
      get { return ++counter; }
      set { counter = value; }
    }

    public int LastCount
    {
      get { return counter; }
    }
  }
  #endregion //Counter class
}
