<%@ Application Language="C#" %>

<script runat="server">

  void Application_Start(object sender, EventArgs e) 
  {
    //Code that runs on application startup
    //Create logger object
    CreateLogger();
    TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
    try
    {
      //Log application start
      logger.LogText("");
      logger.LogText(1, "", "---------------------------");
      logger.LogText(1, "", "DRQuery application started");
      //Create and store DDDHelepr object
      DDDObjects.DDDHelper dddHelper = new DDDObjects.DDDHelper();
      dddHelper.ConnectionString = ConfigurationManager.ConnectionStrings["CDDBConnectionString"].ConnectionString;
      Application["DDDHelper"] = dddHelper;
      //Load set of known telediagnostic units from DB
      dddHelper.LoadTUObjectsFromDB();
      //Load current hierarchy from DB
      dddHelper.HierarchyHelper.LoadCurrentHierarchy();
      //Store time of last TU load in application state
      Application["LastTURefTime"] = DateTime.Now;
      //Create object handling custom configuration of the grid
      String confFile = ConfigurationManager.AppSettings["customGridColConfig"];
      if (confFile != "")
      {
        CustomConfiguration customColConf = new CustomConfiguration(confFile);
        Application["CustomConfiguration"] = customColConf;
      }
      //Create object handling security configuration
      confFile = ConfigurationManager.AppSettings["securityConfig"];
      SecurityHelper securityHelper = new SecurityHelper(confFile);
      Application["SecurityHelper"] = securityHelper;
    }
    catch (Exception ex)
    { //Log exception
      logger.LogText(1, "", "Application failed to start");
      logger.LogException(2, "", ex);
      throw ex;
    }
  }

  void Application_End(object sender, EventArgs e) 
  {
    //Code that runs on application shutdown
    //Log application end
    TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
    logger.LogText(1, "", "DRQuery application stopped");
  }
      
  void Application_Error(object sender, EventArgs e) 
  { 
    // Code that runs when an unhandled error occurs
    Exception ex = Server.GetLastError();
    Session["Exception"] = ex;
    Server.ClearError();
    Response.Redirect("Error.aspx");
  }

  void Session_Start(object sender, EventArgs e) 
  {
    // Code that runs when a new session is started
    try
    {
      //Compute time from last refresh of TUs
      TimeSpan tuRefDelay = DateTime.Now - (DateTime)Application["LastTURefTime"];
      if (tuRefDelay.TotalMinutes > 10)
      {
        //Obtain logger object
        TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
        //Obtain DDDHelper
        DDDObjects.DDDHelper dddHelper = (DDDObjects.DDDHelper)Application["DDDHelper"];
        //Load set of known telediagnostic units from DB
        dddHelper.LoadTUObjectsFromDB();
        logger.LogText(2, "Global", "Reloaded list of TU Instances from DB");
        //Reload fleet hierarchy
        dddHelper.HierarchyHelper.ReloadCurrentHierarchy();
        logger.LogText(2, "Global", "Reloaded current fleet hierarchy from DB");
        //Store time of last TU load in application state
        Application["LastTURefTime"] = DateTime.Now;
      }
    }
    catch (Exception ex)
    {
      throw new Exception("Failed to load list of TUs from DB", ex);
    }
  }

  void Session_End(object sender, EventArgs e) 
  {
      // Code that runs when a session ends. 
      // Note: The Session_End event is raised only when the sessionstate mode
      // is set to InProc in the Web.config file. If session mode is set to StateServer 
      // or SQLServer, the event is not raised.

  }

  /// <summary>
  /// Returns new connection to database based on CDDBConnectionString from configuration file
  /// </summary>
  /// <returns></returns>
  System.Data.SqlClient.SqlConnection GetDBConnection()
  {
    return new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["CDDBConnectionString"].ConnectionString);
  }
  
  /// <summary>
  /// Creates logger object and stores in application state
  /// </summary>
  private void CreateLogger()
  {
    //Create Application logger
    TDManagementForms.Logger loggerApp = new TDManagementForms.Logger();
    loggerApp.FileName = ConfigurationManager.AppSettings["LogFileName"];
    loggerApp.FileVerbosity = Convert.ToInt32(ConfigurationManager.AppSettings["MaxLogVerbosity"]);
    loggerApp.MaxFileLength = Convert.ToInt32(ConfigurationManager.AppSettings["MaxLogSize"]);
    loggerApp.MaxHistoryFiles = Convert.ToInt32(ConfigurationManager.AppSettings["MaxLogHistoryFiles"]);
    loggerApp.LogToFile = true;
    Application["Logger"] = loggerApp;
  }
</script>
