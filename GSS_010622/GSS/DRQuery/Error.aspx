<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="Error.aspx.cs"
  Inherits="Error" Title="Application error" %>

<asp:Content ID="cntError" ContentPlaceHolderID="contentPH" runat="Server">
  <div style="margin: 5px">
    <h5>
      Fatal error occured during request processing:</h5>
    <asp:BulletedList ID="blErrors" runat="server" BulletStyle="Disc">
    </asp:BulletedList>
  </div>
</asp:Content>
