<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="StatisticsView.aspx.cs"
  Inherits="StatisticsView" Title="Statistics of diagnostic records" %>

<asp:Content ID="cntStatisticsView" ContentPlaceHolderID="contentPH" runat="Server">
  <div style="margin: 5px">
    <p align="right">
      <asp:Button ID="btQuery" runat="server" OnClick="btQuery_Click" Text="Modify query" />
    </p>
    <table cellpadding="0" cellspacing="0" style="width: 100%">
      <tr>
        <td>
          <h4 style="margin-bottom: 8px;">
            Diagnostic Records Statistics
          </h4>
        </td>
        <td style="text-align: right;">
          <h4 style="font-size: small; margin-bottom: 5px;">
            <asp:Label ID="lbTotalCount" runat="server" Text="Label"></asp:Label>
            records,
            <asp:Label ID="lbRecTypes" runat="server" Text="Label"></asp:Label>
            record types from
            <asp:Label ID="lbFrom" runat="server" Text="Label"></asp:Label>
            to
            <asp:Label ID="lbTo" runat="server" Text="Label"></asp:Label>
          </h4>
        </td>
      </tr>
    </table>
    <asp:ObjectDataSource ID="StatisticsDataSource" runat="server" MaximumRowsParameterName=""
      OldValuesParameterFormatString="original_{0}" OnObjectCreated="StatisticsDataSource_ObjectCreated"
      SelectMethod="GetRecordsStatistics" StartRowIndexParameterName="" TypeName="BLL.StatisticsBLL"
      OnSelected="StatisticsDataSource_Selected" OnSelecting="StatisticsDataSource_Selecting">
      <SelectParameters>
        <asp:QueryStringParameter Name="TUInstanceIDs" QueryStringField="TUInstanceIDs" Type="String" />
        <asp:QueryStringParameter Name="HierarchyItemIDs" QueryStringField="HierarchyItemIDs" Type="String"/>
        <asp:QueryStringParameter DefaultValue="" Name="StartTimeFrom" QueryStringField="StartTimeFrom" Type="DateTime" />
        <asp:QueryStringParameter DefaultValue="" Name="StartTimeTo" QueryStringField="StartTimeTo" Type="DateTime" />
        <asp:QueryStringParameter DefaultValue="" Name="Sources" QueryStringField="Sources" Type="String" />
        <asp:QueryStringParameter DefaultValue="" Name="VehicleNumbers" QueryStringField="VehicleNumbers" Type="String" />
        <asp:QueryStringParameter DefaultValue="" Name="FaultCodes" QueryStringField="FaultCodes" Type="String" />
        <asp:QueryStringParameter DefaultValue="" Name="TrcSnpCodes" QueryStringField="TrcSnpCodes" Type="String" />
        <asp:QueryStringParameter DefaultValue="" Name="TitleKeywords" QueryStringField="TitleKeywords" Type="String" />
        <asp:QueryStringParameter DefaultValue="" Name="MatchAllKeywords" QueryStringField="MatchAllKeywords" Type="Boolean" />
        <asp:QueryStringParameter DefaultValue="0" Name="RecordTypes" QueryStringField="RecordTypes"
          Type="Int32" />
        <asp:QueryStringParameter DefaultValue="" Name="Severities" QueryStringField="Severities" Type="String" />
        <asp:QueryStringParameter DefaultValue="" Name="SubSeverities" QueryStringField="SubSeverities" Type="String" />
        <asp:Parameter Direction="Output" Name="totalRecCount" Type="Int32" />
        <asp:Parameter Direction="Output" Name="recTypeCount" Type="Int32" />
      </SelectParameters>
    </asp:ObjectDataSource>
    <asp:GridView ID="gvRecords" runat="server" AutoGenerateColumns="False" BackColor="White"
      BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" DataKeyNames="TUInstanceID, HierarchyItemID, DDDVersionID, VehicleNumber, Source, RecordDefID"
      DataSourceID="StatisticsDataSource" ForeColor="Black" GridLines="Both" PageSize="20"
      AllowSorting="True">
      <Columns>
        <asp:BoundField DataField="TrainName" HeaderText="Train" SortExpression="TrainName" />
        <asp:BoundField DataField="VehicleName" HeaderText="Vehicle" SortExpression="VehicleName" />
        <asp:BoundField DataField="VehicleNumber" HeaderText="Vehicle Number" SortExpression="VehicleNumber" />
        <asp:BoundField DataField="Source" HeaderText="Record Source" SortExpression="Source" />
        <asp:BoundField DataField="FaultCode" HeaderText="Fault Code" SortExpression="FaultCode" />
        <asp:BoundField DataField="RecTitle" HeaderText="Record Title" SortExpression="RecTitle" />
        <asp:BoundField DataField="RecCount" HeaderText="Count" ReadOnly="True" SortExpression="RecCount" />
        <asp:BoundField DataField="FirstTime" HeaderText="Time of first" ReadOnly="True"
          SortExpression="FirstTime" DataFormatString="{0:yyyy-MM-dd HH:mm:ss}" HtmlEncode="False" />
        <asp:BoundField DataField="LastTime" HeaderText="Time of last" ReadOnly="True" SortExpression="LastTime"
          DataFormatString="{0:yyyy-MM-dd HH:mm:ss}" HtmlEncode="False" />
        <asp:BoundField DataField="FaultSeverity" HeaderText="Severity" SortExpression="FaultSeverity" />
        <asp:BoundField DataField="FaultSubSeverity" HeaderText="Sub Severity" SortExpression="FaultSubSeverity" />
      </Columns>
      <FooterStyle BackColor="#CCCCCC" />
      <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="Black" />
      <PagerStyle BackColor="#FFDDDD" ForeColor="Blue" HorizontalAlign="Center" />
      <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
      <AlternatingRowStyle BackColor="#DDDDFF" BorderColor="White" BorderStyle="Solid"
        BorderWidth="2px" />
      <RowStyle BackColor="#DDFFDD" BorderColor="White" BorderStyle="Solid" BorderWidth="2px" />
    </asp:GridView>
  </div>
</asp:Content>
