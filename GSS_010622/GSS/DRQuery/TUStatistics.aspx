<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="TUStatistics.aspx.cs"
  Inherits="TUStatistics" Title="Telediagnostic Units Statistics" %>

<asp:Content ID="cntTUStatistics" ContentPlaceHolderID="contentPH" runat="Server">
  <div style="margin: 5px">
    <table cellpadding="0" cellspacing="0" style="width: 100%">
      <tr>
        <td style="height: 28px">
          <h4 style="margin-bottom: 8px;">
            Telediagnostic Units Statistics
          </h4>
        </td>
        <td style="text-align: right; height: 28px;">
          <h4 style="font-size: small; margin-bottom: 5px;">
            Total
            <asp:Label ID="lbTotalRecCount" runat="server" Text="Label"></asp:Label>
            records in database,
            <asp:Label ID="lbIntervalRecCount" runat="server" Text="Label"></asp:Label>
            records in selected time range
          </h4>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <h5>
            Time range: 
          <asp:DropDownList ID="ddlTimeRange" runat="server" Width="200px" AutoPostBack="True">
            <asp:ListItem Value="1">Last day</asp:ListItem>
            <asp:ListItem Value="2">Last 2 days</asp:ListItem>
            <asp:ListItem Value="3">Last 3 days</asp:ListItem>
            <asp:ListItem Value="4">Last 4 days</asp:ListItem>
            <asp:ListItem Selected="True" Value="7">Last week</asp:ListItem>
            <asp:ListItem Value="14">Last 2 weeks</asp:ListItem>
            <asp:ListItem Value="30">Last month</asp:ListItem>
            <asp:ListItem Value="60">Last 2 months</asp:ListItem>
            <asp:ListItem Value="120">Last 4 months</asp:ListItem>
            <asp:ListItem Value="180">Last 6 months</asp:ListItem>
          </asp:DropDownList></h5>
        </td>
      </tr>
    </table>
    <br />
    <asp:ObjectDataSource ID="StatisticsDataSource" runat="server" MaximumRowsParameterName=""
      OldValuesParameterFormatString="original_{0}" OnObjectCreated="StatisticsDataSource_ObjectCreated"
      SelectMethod="GetTUStatistics" StartRowIndexParameterName="" TypeName="BLL.StatisticsBLL"
      OnSelected="StatisticsDataSource_Selected" OnSelecting="StatisticsDataSource_Selecting">
      <SelectParameters>
        <asp:ControlParameter ControlID="ddlTimeRange" Name="rangeDays" PropertyName="SelectedValue"
          Type="Int32" />
        <asp:Parameter Direction="Output" Name="totalRecCount" Type="Int32" />
        <asp:Parameter Direction="Output" Name="intervalRecCount" Type="Int32" />
      </SelectParameters>
    </asp:ObjectDataSource>
    <asp:GridView ID="gvRecords" runat="server" AutoGenerateColumns="False" BackColor="White"
      BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" DataSourceID="StatisticsDataSource"
      ForeColor="Black" PageSize="20" AllowSorting="True">
      <Columns>
        <asp:BoundField DataField="TUName" HeaderText="Name" SortExpression="TUName" />
        <asp:BoundField DataField="RecCount" HeaderText="Record Count" SortExpression="RecCount" />
        <asp:BoundField DataField="LastRecTime" HeaderText="Time of last record" SortExpression="LastRecTime"
          DataFormatString="{0:yyyy-MM-dd HH:mm:ss}" HtmlEncode="False" />
        <asp:BoundField DataField="IPAddrUpdTime" HeaderText="Last online" SortExpression="IPAddrUpdTime"
          DataFormatString="{0:yyyy-MM-dd HH:mm:ss}" HtmlEncode="False" />
      </Columns>
      <FooterStyle BackColor="#CCCCCC" />
      <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="Black" />
      <PagerStyle BackColor="#FFDDDD" ForeColor="Blue" HorizontalAlign="Center" />
      <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
      <AlternatingRowStyle BackColor="#DDDDFF" BorderColor="White" BorderStyle="Solid"
        BorderWidth="2px" />
      <RowStyle BackColor="#DDFFDD" BorderColor="White" BorderStyle="Solid" BorderWidth="2px" />
    </asp:GridView>
    <br />
    <p align="right">
      &nbsp;</p>
  </div>
</asp:Content>
