using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DDDObjects;
using BLL;
using ZetaLibWeb;

public partial class StatisticsView : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    //Sort records in grid
    //gvRecords.Sort("FaultCode, Source", SortDirection.Ascending);
  }

  /// <summary>
  /// Handler for any errors in page not handled in code by catch blocks.
  /// It is actually page-level exception handler for all types of exceptions.
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  private void Page_Error(object sender, EventArgs e)
  {
    Exception ex = Server.GetLastError();
    //Log exception
    //Obtain logger object
    TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
    //Log operation
    logger.LogText(1, "StatsView", "Page error occured for user {0}", HttpContext.Current.User.Identity.Name);
    logger.LogException(2, "StatsView", ex);
    //Store in session end redirect to error page
    Session["Exception"] = ex;
    Response.Redirect("~/Error.aspx");
  }

  protected void StatisticsDataSource_ObjectCreated(object sender, ObjectDataSourceEventArgs e)
  {
    //Assign DDDHelper to BLL object
    StatisticsBLL statBLL = (StatisticsBLL)e.ObjectInstance;
    statBLL.DDDHelper = (DDDHelper)Application["DDDHelper"];
    //Assign logger to BLL object
    TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
    statBLL.Logger = logger;
  }
  protected void btQuery_Click(object sender, EventArgs e)
  {
    //Redirect back to query page with original parameters
    QueryString queryString = new QueryString(Page);
    queryString.BeforeUrl = "Query.aspx";
    Response.Redirect(queryString.All);
  }
  protected void StatisticsDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
  {
    //Display name of telediagnostic unit and time range
    lbFrom.Text = Request.QueryString["StartTimeFrom"];
    lbTo.Text = Request.QueryString["StartTimeTo"];
    //Display record counts
    if (e.OutputParameters["totalRecCount"] != null)
      lbTotalCount.Text = e.OutputParameters["totalRecCount"].ToString();
    if (e.OutputParameters["recTypeCount"] != null)
      lbRecTypes.Text = e.OutputParameters["recTypeCount"].ToString();
  }
  protected void StatisticsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
  {
    //Log select operation
    if (!e.ExecutingSelectCount)
    { //Record select operation
      //Obtain logger object
      TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
      //Log the operation
      String strParams = String.Empty;
      foreach (DictionaryEntry param in e.InputParameters)
        if (param.Value != null)
          strParams += String.Format("{0}: {1}; ", param.Key, param.Value);
      logger.LogText(3, "StatsView", "User {0} selecting recod statistics with parameters:", HttpContext.Current.User.Identity.Name);
      logger.LogText(4, "StatsView", strParams);
    }
  }
}
