using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Collections.Generic;

/// <summary>
/// Provides helper methods to manage the security of this web application
/// </summary>
public class SecurityHelper
{
  /// <summary>
  /// Dictionary which stores hierarchy XPath expression for each configured role
  /// </summary>
  private Dictionary<String, String> roleHierPaths;

  /// <summary>
  /// Constructor of the helper
  /// </summary>
  /// <param name="cfgDocPath">Path to security configuration document</param>
  public SecurityHelper(String cfgDocPath)
  {
    //Create and load xml configuration document
    XmlDocument cfgDoc = new XmlDocument();
    cfgDoc.Load(cfgDocPath);
    //Extract information about access of roles to fleet hierarchy
    //Create dictionary
    roleHierPaths = new Dictionary<string, string>();
    XmlNodeList roleElems = cfgDoc.SelectNodes("//Role");
    foreach (XmlElement roleElem in roleElems)
    { //Iterate over all specified role elements
      //Add role to dictionary
      roleHierPaths[roleElem.GetAttribute("name")] = roleElem.GetAttribute("path");
    }
  }

  /// <summary>
  /// Filters given fleet hierarchy XML document for given set of user roles.
  /// For now, shortest XPath expression is found and used for the filtering.
  /// If role is not in the dictionary empty XML document is returned.
  /// </summary>
  /// <param name="hierDoc">XML document with fleet hierarchy to be filtered</param>
  /// <param name="roles">Roles in which user is authorized</param>
  /// <returns>Filtered fleet hierarchy</returns>
  public XmlDocument FilterFleetHierarchy(XmlDocument hierDoc, String[] roles)
  {
    //Check if any roles were specified
    if (roles == null || roles.Length == 0)
      return hierDoc; //No roles specified - no security
    //Create output document
    XmlDocument filteredHierDoc = new XmlDocument();
    //Import root element from original hierarchy
    filteredHierDoc.AppendChild(filteredHierDoc.ImportNode(hierDoc.DocumentElement, false));
    //First find shortest XPath expression
    String xpath = String.Empty;
    String roleFilt;
    foreach (String role in roles)
      if (roleHierPaths.TryGetValue(role, out roleFilt))
        if (xpath == String.Empty || xpath.Length > roleFilt.Length)
          xpath = roleFilt;
    //Test if any expression has been found
    if (xpath == String.Empty)
      return filteredHierDoc; //Return filtered hierarchy with only root element
    //Obtain filtered elements from original document
    XmlNodeList filteredElems = hierDoc.SelectNodes(xpath);
    //Append filtered elements to filtered hierarchy
    foreach (XmlNode filteredElem in filteredElems)
      filteredHierDoc.DocumentElement.AppendChild(filteredHierDoc.ImportNode(filteredElem, true));
    //Return filtered document
    return filteredHierDoc;
  }
}
