using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace StatisticsTableAdapters
{
  /// <summary>
  /// Partial class adds public properties to statistics table adapter
  /// </summary>
  public partial class RecordsStatisticsTableAdapter
  {
    public int CommandTimeout
    {
      set
      {
        foreach (SqlCommand cmd in CommandCollection)
          cmd.CommandTimeout = value;
      }
    }
  }

  /// <summary>
  /// Partial class adds public properties to statistics table adapter
  /// </summary>
  public partial class TUStatisticsTableAdapter
  {
    public int CommandTimeout
    {
      set
      {
        foreach (SqlCommand cmd in CommandCollection)
          cmd.CommandTimeout = value;
      }
    }
  }

}
