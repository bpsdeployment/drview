using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace DiagnosticDataTableAdapters
{
  /// <summary>
  /// Partial class for table adapter
  /// Defines additional public properties
  /// </summary>
  public partial class DiagnosticRecordsTableAdapter
  {
    public int CommandTimeout
    {
      set
      {
        foreach (SqlCommand cmd in CommandCollection)
          cmd.CommandTimeout = value;
      }
    }
  }
}