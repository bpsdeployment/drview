using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Provides static method for sorting of items in list controls (ComboBox, ListBox)
/// </summary>
public class ListControlSort
{
  #region sorting methods
  public static void SortByValue(ListControl combo)
  {    
    SortCombo(combo, new ComboValueComparer());
  } 
  public static void SortByText(ListControl combo)
  {    
    SortCombo(combo, new ComboTextComparer());
  }
  #endregion 

  #region Private Methods - for internal use
  private static void SortCombo(ListControl combo, IComparer comparer)
  {    
    int i;    
    if (combo.Items.Count <= 1)        
      return;    
    ArrayList arrItems=new ArrayList();    
    for (i=0; i<combo.Items.Count; i++)    
    {        
      ListItem item=combo.Items[i];        
      arrItems.Add(item);    
    }    
    arrItems.Sort(comparer);    
    combo.Items.Clear();    
    for (i=0; i<arrItems.Count; i++)    
    {        
      combo.Items.Add((ListItem) arrItems[i]);    
    }
  } 

  #region Combo Comparers
  /// <summary>
  /// compare list items by their value
  /// </summary>
  private class ComboValueComparer : IComparer
  {    
    public enum SortOrder    
    {        
      Ascending=1,        
      Descending=-1    
    }     
    
    private int _modifier;     
    
    public ComboValueComparer()    
    {        
      _modifier = (int) SortOrder.Ascending;    
    }     

    public ComboValueComparer(SortOrder order)    
    {        
      _modifier = (int) order;    
    }     

    //sort by value    
    public int Compare(Object o1, Object o2)    
    {        
      ListItem cb1=(ListItem) o1;        
      ListItem cb2=(ListItem) o2;        
      return cb1.Value.CompareTo(cb2.Value)*_modifier;    
    }
  } //end class ComboValueComparer 

  /// <summary>
  /// compare list items by their text.
  /// </summary>
  private class ComboTextComparer : IComparer
  {    
    public enum SortOrder    
    {        
      Ascending=1,        
      Descending=-1    
    }     

    private int _modifier;     

    public ComboTextComparer()    
    {        
      _modifier = (int) SortOrder.Ascending;    
    }     

    public ComboTextComparer(SortOrder order)    
    {        
      _modifier = (int) order;    
    }     

    //sort by value    
    public int Compare(Object o1, Object o2)    
    {        
      ListItem cb1=(ListItem) o1;        
      ListItem cb2=(ListItem) o2;        
      return cb1.Text.CompareTo(cb2.Text)*_modifier;    
    }
  } //end class ComboTextComparer
  #endregion

  #endregion
}
