using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Xml;
using System.Globalization;

/// <summary>
/// Handles reading of configuration and formating for custom columns.
/// Columns may then be displayed in resulting grid.
/// </summary>
public class CustomConfiguration
{
  #region Private members
  private int? refAttrID0 = null; //ID of 1. referential attribute
  private int? refAttrID1 = null; //ID of 2. referential attribute
  private int? refAttrID2 = null; //ID of 3. referential attribute
  private int? refAttrID3 = null; //ID of 4. referential attribute
  List<DataTableColumn> dataTableColumns; //List of data table columns
  List<GridColumn> gridColumns; //List of all grid columns
  NumberFormatInfo numFormatInfo; //Format info used to format the numbers
  #endregion //Private members

  #region Public properties
  /// <summary>
  /// ID of 1. referential attribute to use in a query
  /// Represents GPS latitude
  /// </summary>
  public int? RefAttrID0 { get { return refAttrID0; } }

  /// <summary>
  /// ID of 2. referential attribute to use in a query
  /// Represents GPS longitude
  /// </summary>
  public int? RefAttrID1 { get { return refAttrID1; } }

  /// <summary>
  /// ID of 3. referential attribute to use in a query
  /// Custom attribute
  /// </summary>
  public int? RefAttrID2 { get { return refAttrID2; } }

  /// <summary>
  /// ID of 4. referential attribute to use in a query
  /// Custom attribute
  /// </summary>
  public int? RefAttrID3 { get { return refAttrID3; } }

  /// <summary>
  /// List of data table columns
  /// </summary>
  public List<DataTableColumn> DataTableColumns { get { return dataTableColumns; } }

  /// <summary>
  /// List of all grid columns
  /// </summary>
  public List<GridColumn> GridColumns { get { return gridColumns; } }
  #endregion //Public properties

  #region Public map properties
  /// <summary>
  /// ID of referential attribute with latitude
  /// </summary>
  public int? LatRefAttrID { get { return refAttrID0; } }
  /// <summary>
  /// ID of referential attribute with longitude
  /// </summary>
  public int? LngRefAttrID { get { return refAttrID1; } }
  /// <summary>
  /// title displayed on map marker with more than one diagnostic record
  /// </summary>
  public String MoreRecsTitle = "";
  /// <summary>
  /// maximum number of records displayed on one marker tab
  /// </summary>
  public int MaxMarkerTabRecs = 10;
  /// <summary>
  /// title for marker showing records with invalid GPS data
  /// </summary>
  public String InvalidGPSMarkerTitle = ""; 
  /// <summary>
  /// text displayed instead of lat and lng for invalid GPS data
  /// </summary>
  public String InvalidGPSText = "";
  /// <summary>
  /// text used as base for marker info window tab title (index of tab is appended)
  /// </summary>
  public String MarkerTabCaptionBase = "";
  /// <summary>
  /// URL where icon for invalid GPS data marker can be found
  /// </summary>
  public String InvalidGPSIconURL = "";
  /// <summary>
  /// URL where icon for marker with selected record can be found
  /// </summary>
  public String SelectedMarkerIconURL = "";
  /// <summary>
  /// minimum distance between two displayed markers
  /// </summary>
  public double MarkerMinDistance = 100;
  #endregion //Public map properties

  #region Construction initialization
  /// <summary>
  /// Creates the object from XML configuration document
  /// </summary>
  /// <param name="confDoc">Configuration document</param>
  public CustomConfiguration(XmlDocument confDoc)
  {
    Initialize(confDoc);
  }

  /// <summary>
  /// Creates the object from XML configuration document
  /// </summary>
  /// <param name="confDocFile">Path to the file with configuration document</param>
  public CustomConfiguration(String confDocFile)
  {
    XmlDocument confDoc = new XmlDocument();
    confDoc.Load(confDocFile);
    Initialize(confDoc);
  }

  /// <summary>
  /// Initializes the object from XML configuration document
  /// </summary>
  /// <param name="confDoc">Configuration document</param>
  private void Initialize(XmlDocument confDoc)
  {
    //Create number format info
    numFormatInfo = new NumberFormatInfo();
    numFormatInfo.NumberDecimalSeparator = ".";
    //Read IDs of used referential attributes
    XmlElement refAttrElem = (XmlElement)confDoc.SelectSingleNode("//RefAttrColumns");
    if (refAttrElem.GetAttribute("RefAttrID2") != "")
      refAttrID2 = Convert.ToInt32(refAttrElem.GetAttribute("RefAttrID2"));
    if (refAttrElem.GetAttribute("RefAttrID3") != "")
      refAttrID3 = Convert.ToInt32(refAttrElem.GetAttribute("RefAttrID3"));
    //Create lists of data table and grid columns
    XmlElement dataColsElem = (XmlElement)confDoc.SelectSingleNode("//DataTableColumns");
    CreateDataTableColumns(dataColsElem);
    XmlElement gridColsElem = (XmlElement)confDoc.SelectSingleNode("//GridColumns");
    CreateGridColumns(gridColsElem);
    //Read configuration of map
    XmlElement mapElem = (XmlElement)confDoc.SelectSingleNode("//MapConfig");
    if (mapElem.GetAttribute("LatRefAttrID") != "")
      refAttrID0 = Convert.ToInt32(mapElem.GetAttribute("LatRefAttrID"));
    if (mapElem.GetAttribute("LngRefAttrID") != "")
      refAttrID1 = Convert.ToInt32(mapElem.GetAttribute("LngRefAttrID"));
    MoreRecsTitle = mapElem.GetAttribute("MoreRecsTitle");
    if (mapElem.GetAttribute("MaxMarkerTabRecs") != "")
      MaxMarkerTabRecs = Convert.ToInt32(mapElem.GetAttribute("MaxMarkerTabRecs"));
    InvalidGPSMarkerTitle = mapElem.GetAttribute("InvalidGPSMarkerTitle");
    InvalidGPSText = mapElem.GetAttribute("InvalidGPSText");
    MarkerTabCaptionBase = mapElem.GetAttribute("MarkerTabCaptionBase");
    InvalidGPSIconURL = mapElem.GetAttribute("InvalidGPSIconURL");
    SelectedMarkerIconURL = mapElem.GetAttribute("SelectedMarkerIconURL");
    if (mapElem.GetAttribute("MarkerMinDistance") != "")
      MarkerMinDistance = Convert.ToDouble(mapElem.GetAttribute("MarkerMinDistance"));
  }
  #endregion //Construction initialization

  #region Public methods
  /// <summary>
  /// Returns list of formatted string values.
  /// One for each DataTableColumn
  /// </summary>
  public List<String> GetFormattedValues(double? lat, double? lng, double? par2, double? par3, String recTitle)
  {
    List<String> values = new List<string>();
    foreach (DataTableColumn col in dataTableColumns)
      values.Add(col.FormatValue(numFormatInfo, lat, lng, par2, par3, recTitle));
    return values;
  }
  #endregion //Public methods

  #region Helper methods
  /// <summary>
  /// Creates list of data table columns from configuration element
  /// </summary>
  private void CreateDataTableColumns(XmlElement columnsElement)
  {
    //Create list for columns
    dataTableColumns = new List<DataTableColumn>();
    //Iterate over all child elements, create table column objects
    foreach (XmlElement colElem in columnsElement.GetElementsByTagName("*"))
    {
      DataTableColumn col = new DataTableColumn(colElem);
      dataTableColumns.Add(col);
    }
  }

  /// <summary>
  /// Creates list of all grid columns from configuration element
  /// </summary>
  private void CreateGridColumns(XmlElement columnsElement)
  {
    //Create list for columns
    gridColumns = new List<GridColumn>();
    //Iterate over all child elements, create grid column objects
    foreach (XmlElement colElem in columnsElement.GetElementsByTagName("*"))
    {
      GridColumn col;
      if (colElem.Name == "BoundColumn")
        col = new BoundGridColumn(colElem);
      else if (colElem.Name == "HyperlinkColumn")
        col = new HypelinkGridColumn(colElem);
      else
        continue;
      gridColumns.Add(col);
    }
  }
  #endregion //Helper methods
}

#region DataTableColumnClass
/// <summary>
/// Represents one column in DataTable created from additional query columns using expressions
/// </summary>
public class DataTableColumn
{
  public String Name; //Name of the column
  private bool usePar0 = false;
  private bool usePar1 = false;
  private bool usePar2 = false;
  private bool usePar3 = false;
  private String Expression;
  private bool EmptyForNullParams = false;
  private bool TreatZeroAsNull = false;

  /// <summary>
  /// Constructs the object from configuration XML element
  /// </summary>
  public DataTableColumn(XmlElement colElement)
  {
    //Read column attributes
    Name = colElement.GetAttribute("Name");
    Expression = colElement.GetAttribute("FormatExpression");
    if (colElement.GetAttribute("EmptyForNullParams") != "")
      EmptyForNullParams = Convert.ToBoolean(colElement.GetAttribute("EmptyForNullParams"));
    if (colElement.GetAttribute("TreatZeroAsNull") != "")
      TreatZeroAsNull = Convert.ToBoolean(colElement.GetAttribute("TreatZeroAsNull"));
    //Search for used parameters
    if (Expression.Contains("{0"))
      usePar0 = true;
    if (Expression.Contains("{1"))
      usePar1 = true;
    if (Expression.Contains("{2"))
      usePar2 = true;
    if (Expression.Contains("{3"))
      usePar3 = true;
  }

  /// <summary>
  /// Format the value for table column from additional query columns 
  /// </summary>
  /// <returns></returns>
  public String FormatValue(NumberFormatInfo numFormatInfo, double? par0, double? par1, double? par2, double? par3, String recTitle)
  {
    if (EmptyForNullParams && (
        (usePar0 && (!par0.HasValue || (par0.Value == 0 && TreatZeroAsNull))) ||
        (usePar1 && (!par1.HasValue || (par1.Value == 0 && TreatZeroAsNull))) ||
        (usePar2 && (!par2.HasValue || (par2.Value == 0 && TreatZeroAsNull))) ||
        (usePar3 && (!par3.HasValue || (par3.Value == 0 && TreatZeroAsNull))))
       )
      return ""; //Empty expression shall be returned
    //Returned formatted expression
    return String.Format(numFormatInfo, Expression, par0, par1, par2, par3, recTitle);
  }
}
#endregion //DataTableColumnClass

#region Grid column classes
/// <summary>
/// Represents one column in the grid defined by the configuration
/// </summary>
public class GridColumn
{
  /// <summary>
  /// Text displayed in header
  /// </summary>
  public String HeaderText;

  /// <summary>
  /// Standard constructor from configuration element
  /// </summary>
  public GridColumn(XmlElement colElement)
  {
    HeaderText = colElement.GetAttribute("HeaderText");
  }
}

public class BoundGridColumn : GridColumn
{
  /// <summary>
  /// Name of data field to which column is bound
  /// </summary>
  public String DataField;
  /// <summary>
  /// Sort expression used to sort data on this column
  /// </summary>
  public String SortExpression;
  /// <summary>
  /// Format string for data in this column
  /// </summary>
  public String DataFormatString;
  /// <summary>
  /// HTML encode the data in this column
  /// </summary>
  public bool HtmlEncode = true;

  /// <summary>
  /// Standard constructor from configuration element
  /// </summary>
  public BoundGridColumn(XmlElement colElement)
    : base(colElement)
  {
    DataField = colElement.GetAttribute("DataField");
    SortExpression = colElement.GetAttribute("SortExpression");
    DataFormatString = colElement.GetAttribute("DataFormatString");
    String attribute;
    if ((attribute = colElement.GetAttribute("HtmlEncode")) != String.Empty)
      HtmlEncode = Boolean.Parse(attribute);
  }
}

public class HypelinkGridColumn : GridColumn
{
  /// <summary>
  /// Name of data field to which column is bound
  /// </summary>
  public String DataTextField;

  /// <summary>
  /// Name of field, which creates the URL of the hyperlink
  /// </summary>
  public String[] DataNavigateUrlFields;

  /// <summary>
  /// Format string used for formatting of text in the cell
  /// </summary>
  public String DataTextFormatString;

  /// <summary>
  /// Format string used for formatting of the URL
  /// </summary>
  public String DataNavigateUrlFormatString;

  /// <summary>
  /// Name of target frame or window for the hyperlink
  /// </summary>
  public String Target;

  /// <summary>
  /// Standard constructor from configuration element
  /// </summary>
  public HypelinkGridColumn(XmlElement colElement)
    : base(colElement)
  {
    DataTextField = colElement.GetAttribute("DataTextField");
    DataNavigateUrlFields = colElement.GetAttribute("DataNavigateUrlFields").Split(new String[] { "," }, StringSplitOptions.RemoveEmptyEntries);
    DataTextFormatString = colElement.GetAttribute("DataTextFormatString");
    DataNavigateUrlFormatString = colElement.GetAttribute("DataNavigateUrlFormatString");
    Target = colElement.GetAttribute("Target");
  }
}
#endregion //Grid column classes
