using System;
using System.Data;
using System.Configuration;

/// <summary>
/// Represents one GPS point defined by its latitude and longitude
/// Implements equality comparison and hashing algorithm
/// </summary>
public class GPSPoint : IEquatable<GPSPoint>
{
  public double Latitude;
  public double Longitude;
  public double MinDistance = 0;

  /// <summary>
  /// Default constructor
  /// </summary>
  public GPSPoint()
  { }

  /// <summary>
  /// Constructor with initialization
  /// </summary>
  public GPSPoint(double lat, double lon)
  {
    this.Latitude = lat;
    this.Longitude = lon;
  }

  /// <summary>
  /// Constructor with initialization
  /// </summary>
  public GPSPoint(double lat, double lon, double minDistance)
  {
    this.Latitude = lat;
    this.Longitude = lon;
    this.MinDistance = minDistance;
  }

  public override bool Equals(object obj)
  {
    if (obj == null || GetType() != obj.GetType())
      return false;

    return Equals((GPSPoint)obj);
  }

  public bool Equals(GPSPoint point)
  {
    //Check if minimum distance for comparison is defined
    if (MinDistance == 0)
      return ((point.Latitude == Latitude) && (point.Longitude == Longitude));

    //Check computed distance with specified minimum distance
    return (ComputeDistance(point.Latitude, point.Longitude) <= MinDistance &&
            GetHashCode() == point.GetHashCode());
  }

  public override int GetHashCode()
  { //Return hash code of lat and lng rounded to int numbers
    return Math.Round(Latitude, 0).GetHashCode() ^ Math.Round(Longitude, 0).GetHashCode();
  }

  private double ComputeDistance(double lat, double lng)
  {
    //Compute distance between the two points
    double lat1 = lat * Math.PI / 180;
    double lat2 = Latitude * Math.PI / 180;
    double lng1 = lng * Math.PI / 180;
    double lng2 = Longitude * Math.PI / 180;
    double distance;
    if (lat1 == lat2 && lng1 == lng2)
      distance = 0;
    else
    {
      double tmp = Math.Sin(lat1) * Math.Sin(lat2) +
                   Math.Cos(lat1) * Math.Cos(lat2) * Math.Cos(lng1 - lng2);
      if (tmp > 1)
        distance = 3963100 * Math.Acos(1);
      else
        distance = 3963100 * Math.Acos(tmp);
    }
    return distance;
  }
}

