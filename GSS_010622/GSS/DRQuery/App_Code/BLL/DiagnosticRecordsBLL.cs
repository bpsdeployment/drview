using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.Diagnostics;
using System.Globalization;
using System.Data.SqlClient;
using DiagnosticDataTableAdapters;
using DDDObjects;
using DRQueries;
using FleetHierarchies;

namespace BLL
{
  /// <summary>
  /// Provides access to diagnostic records table through bussiness logic
  /// </summary>
  [System.ComponentModel.DataObject]
  public class DiagnosticRecordsBLL
  {
    #region Private members
    private DiagnosticRecordsTableAdapter recordsAdapter = null;
    private DDDHelper dddHelper = null;
    private CustomConfiguration custConf = null;
    private TDManagementForms.Logger logger = null; //Logger object
    #endregion //Private members

    #region Public properties
    public DiagnosticRecordsTableAdapter Adapter
    {
      get
      {
        if (recordsAdapter == null)
          recordsAdapter = new DiagnosticRecordsTableAdapter();
        return recordsAdapter;
      }
    }

    public DDDHelper DDDHelper
    {
      get { return dddHelper; }
      set { dddHelper = value; }
    }

    public CustomConfiguration CustomConfiguration
    {
      get { return custConf; }
      set { custConf = value; }
    }

    /// <summary>
    /// Logger object
    /// </summary>
    public TDManagementForms.Logger Logger
    {
      get { return logger; }
      set { logger = value; }
    }
    #endregion //Public properties

    #region Public methods
    /// <summary>
    /// Returns data table with set of diagnostic records corresponding with given set of parameters
    /// </summary>
    /// <returns></returns>
    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public DiagnosticData.DiagnosticRecordsDataTable GetDiagnosticRecords(
      String TUInstanceIDs, String HierarchyItemIDs,
      DateTime? StartTimeFrom, DateTime? StartTimeTo,
      String Sources, String VehicleNumbers, String FaultCodes, String TrcSnpCodes, 
      String TitleKeywords, bool MatchAllKeywords,
      int? RecordTypes, String Severities, String SubSeverities, 
      int StartRowIndex, int MaximumRows, 
      String SortExpression, bool SortOrderDesc,
      out String RecMarkersXML)
    {
      //Decalre table
      DiagnosticData.DiagnosticRecordsDataTable table = new DiagnosticData.DiagnosticRecordsDataTable();
      //Start stopwatch
      Stopwatch stopwatch = Stopwatch.StartNew();
      //Convert times to UTC
      if (StartTimeFrom.HasValue)
        StartTimeFrom = DDDHelper.UserTimeToUTC((DateTime)StartTimeFrom);
      if (StartTimeTo.HasValue)
        StartTimeTo = DDDHelper.UserTimeToUTC((DateTime)StartTimeTo);
      //Convert sort order parameter
      eSortOrder sortOrder = eSortOrder.Ascending;
      if (SortOrderDesc)
        sortOrder = eSortOrder.Descending;
      //Get text of DB query
      String queryText;
      queryText = DRQueryHelper.GetQuery_RecordsPaging(
        TUInstanceIDs, HierarchyItemIDs, StartTimeFrom, StartTimeTo, Sources, VehicleNumbers, FaultCodes,
        TrcSnpCodes, TitleKeywords, MatchAllKeywords, RecordTypes, Severities, SubSeverities,
        StartRowIndex, MaximumRows, SortExpression, sortOrder);

      //Load data from DB using DB connection
      using (SqlConnection dbConnection = new SqlConnection(
        ConfigurationManager.ConnectionStrings["CDDBConnectionString"].ConnectionString))
      { //Create connection to DB
        dbConnection.Open(); //Open the connection
        using (SqlCommand dbCmd = new SqlCommand(queryText, dbConnection))
        { //Create database command
          dbCmd.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["dbQueryTimeoutSec"]);
          dbCmd.CommandType = CommandType.Text;
          using (SqlDataReader reader = dbCmd.ExecuteReader())
          { //Execute the command and obtain data reader
            table.Load(reader); //Load data from reader into the table
          }
        }
      }
      //Store select time, restart stopwatch
      long selectTime = stopwatch.ElapsedMilliseconds;
      stopwatch.Reset(); stopwatch.Start();

      //If configured, add custom columns to data table
      int origColCount = table.Columns.Count;
      if (CustomConfiguration != null)
        foreach (DataTableColumn custCol in CustomConfiguration.DataTableColumns)
          table.Columns.Add(custCol.Name, typeof(String));
      //Fill in missing columns for each row
      try
      {
        foreach (DiagnosticData.DiagnosticRecordsRow row in table)
          FillDiagRecRowAttributes(row, dddHelper, CustomConfiguration, origColCount);
      }
      catch (Exception ex)
      {
        throw new Exception("DDD Helper objects failed", ex);
      }
      //Create XML for diagnostic records markers (map positions) and return as output String parameter
      RecMarkersXML = GetMarkerXmlFromDataTable(table).OuterXml;
      //Log elapsed times
      long procTime = stopwatch.ElapsedMilliseconds;
      Logger.LogText(5, "DiagRecBLL", "Select operation {0} ms, processing time {1} ms", selectTime, procTime);
      //Return data table
      return table;
    }

    /// <summary>
    /// Returns total number of available records from database
    /// </summary>
    /// <returns></returns>
    public int GetTotalRecords(
      String TUInstanceIDs, String HierarchyItemIDs,
      DateTime? StartTimeFrom, DateTime? StartTimeTo,
      String Sources, String VehicleNumbers, String FaultCodes, String TrcSnpCodes,
      String TitleKeywords, bool MatchAllKeywords,
      int? RecordTypes, String Severities, String SubSeverities,
      int StartRowIndex, int MaximumRows,
      String SortExpression, bool SortOrderDesc,
      out String RecMarkersXML)
    {
      //Start stopwatch
      Stopwatch stopwatch = Stopwatch.StartNew();
      RecMarkersXML = null;
      //Convert times to UTC
      if (StartTimeFrom.HasValue)
        StartTimeFrom = DDDHelper.UserTimeToUTC((DateTime)StartTimeFrom);
      if (StartTimeTo.HasValue)
        StartTimeTo = DDDHelper.UserTimeToUTC((DateTime)StartTimeTo);
      //Get text of DB query
      String queryText;
      queryText = DRQueryHelper.GetQuery_Count(
          TUInstanceIDs, HierarchyItemIDs, StartTimeFrom, StartTimeTo, Sources, VehicleNumbers, FaultCodes,
          TrcSnpCodes, TitleKeywords, MatchAllKeywords, RecordTypes, Severities, SubSeverities);
      //Execute query to obtain record count
      Object count;
      using (SqlConnection dbConnection = new SqlConnection(
        ConfigurationManager.ConnectionStrings["CDDBConnectionString"].ConnectionString))
      { //Create connection to DB
        dbConnection.Open(); //Open the connection
        using (SqlCommand dbCmd = new SqlCommand(queryText, dbConnection))
        { //Create database command
          dbCmd.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["dbQueryTimeoutSec"]);
          dbCmd.CommandType = CommandType.Text;
          //Execute command
          count = dbCmd.ExecuteScalar();
        }
      }
      //Log elapsed times
      long selTime = stopwatch.ElapsedMilliseconds;
      Logger.LogText(5, "DiagRecBLL", "Select count operation {0} ms", selTime);
      //Return count
      if (count == null)
        return 0;
      else
        return (int)count;
    }

    /// <summary>
    /// Method deletes records from database according to given criteria
    /// </summary>
    public void DeleteRecords(
      String TUInstanceIDs, String HierarchyItemIDs,
      DateTime? StartTimeFrom, DateTime? StartTimeTo,
      String Sources, String VehicleNumbers, String FaultCodes, String TrcSnpCodes,
      String TitleKeywords, bool MatchAllKeywords,
      int? RecordTypes, String Severities, String SubSeverities,
      int MaximumRows)
    {
      //Start stopwatch
      Stopwatch stopwatch = Stopwatch.StartNew();
      //Convert times to UTC
      if (StartTimeFrom.HasValue)
        StartTimeFrom = DDDHelper.UserTimeToUTC((DateTime)StartTimeFrom);
      if (StartTimeTo.HasValue)
        StartTimeTo = DDDHelper.UserTimeToUTC((DateTime)StartTimeTo);
      //Get text of DB query
      String queryText;
      queryText = DRQueryHelper.GetQuery_DeleteRecords(
          TUInstanceIDs, HierarchyItemIDs, StartTimeFrom, StartTimeTo, Sources, VehicleNumbers, FaultCodes,
          TrcSnpCodes, TitleKeywords, MatchAllKeywords, RecordTypes, Severities, SubSeverities,
          MaximumRows, eSortColumn.StartTime, eSortOrder.Ascending); //Records are sorted by StartTime in ascending order
      //Execute query to delete records
      using (SqlConnection dbConnection = new SqlConnection(
        ConfigurationManager.ConnectionStrings["CDDBConnectionString"].ConnectionString))
      { //Create connection to DB
        dbConnection.Open(); //Open the connection
        using (SqlCommand dbCmd = new SqlCommand(queryText, dbConnection))
        { //Create database command
          dbCmd.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["dbQueryTimeoutSec"]);
          dbCmd.CommandType = CommandType.Text;
          //Execute command
          dbCmd.ExecuteScalar();
        }
      }
      //Log elapsed times
      long delTime = stopwatch.ElapsedMilliseconds;
      Logger.LogText(5, "DiagRecBLL", "Delete records operation {0} ms", delTime);
    }

    /// <summary>
    /// Returns data table with values of enviromental variables for simple inputs of given record
    /// </summary>
    /// <returns>data table</returns>
    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public DataTable GetHistEnvDataValues(Guid TUInstanceID, long RecordInstID, int RecordDefID, Guid DDDVersionID)
    {
      //Output parameters
      DateTime? startTime = null;
      DateTime? endTime = null;
      bool? bigEndian = null;
      try
      {
        //First obtain enviromental data as byte array
        byte[] binData = (byte[])Adapter.GetBinData(TUInstanceID, RecordInstID, DDDVersionID, RecordDefID, ref bigEndian, ref startTime, ref endTime);
        if (binData == null)
          return null;
        //Create structure for parser
        sRecBinData sData = new sRecBinData();
        sData.BinData = binData;
        sData.DDDVersionID = DDDVersionID;
        sData.RecordDefID = RecordDefID;
        sData.RecordInstID = RecordInstID;
        sData.TUInstanceID = TUInstanceID;
        sData.BigEndian = (bool)bigEndian;
        sData.StartTime = (DateTime)startTime;
        if (endTime.HasValue)
          sData.EndTime = (DateTime)endTime;
        else
          sData.EndTime = DateTime.MinValue;

        //Parse record, obtain list of elementary values
        List<sParsedVarValue> parsedVals;
        if (DDDHelper.ParseBinRecord(sData, out parsedVals) != eParseStatus.parseSuccessfull)
          throw new Exception("Parsing of binary data failed");

        //Create and return data table
        return GetHistoryDataTable(parsedVals, DDDVersionID, RecordDefID);
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to obtain values of environmental variables", ex);
      }
    }
    #endregion //Public methods

    #region Helper methods
    /// <summary>
    /// Creates and returns data table from list of parsed values
    /// </summary>
    /// <returns>Data table with history values</returns>
    private DataTable GetHistoryDataTable(List<sParsedVarValue> ParsedVals, Guid DDDVersionID, int RecordDefID)
    {
      try
      {
        //Create maps for timestamps and variable ids
        Dictionary<DateTime, int> mapTimeToColIndx = new Dictionary<DateTime, int>();
        SortedList<DateTime, int> timeStamps = new SortedList<DateTime, int>();
        Dictionary<int, int> mapVarToRowIndx = new Dictionary<int, int>();
        //Create data table for values
        DataTable valTable = new DataTable("VarValues");
        //Add column for variable names
        valTable.Columns.Add(ConfigurationManager.AppSettings["valTableNameColName"], typeof(String));
        //Create columns for each timestamp in parsed values
        int colIndx;
        //Iterate over all parsed values - locate all timestamps
        foreach (sParsedVarValue varValue in ParsedVals)
          if (!timeStamps.TryGetValue(varValue.TimeStamp, out colIndx))
            timeStamps.Add(varValue.TimeStamp, -1);
        //Create columns for each timestamp
        foreach (DateTime timeStamp in timeStamps.Keys)
        { //Iterate over all existing timestamps, add column for each
          String colTitle =
            dddHelper.UTCToUserTime(timeStamp).ToString(ConfigurationManager.AppSettings["valTableTimeFormat"]);
          DataColumn column = valTable.Columns.Add(colTitle, typeof(String));
          colIndx = valTable.Columns.IndexOf(column);
          mapTimeToColIndx.Add(timeStamp, colIndx);
        }
        //Obtain helper objects for record
        DDDRecord dddRecord = DDDHelper.GetRecord(DDDVersionID, RecordDefID);
        DDDVersion dddVersion = DDDHelper.GetVersion(DDDVersionID);
        DDDElemVar dddVariable;
        int rowIndx;
        //Iterate over parsed values, add into data table
        foreach (sParsedVarValue varValue in ParsedVals)
        {
          //Obtain variable helper object
          dddVariable = (DDDElemVar)dddVersion.GetVariable(varValue.VariableID);

          //Get column index - created before
          colIndx = mapTimeToColIndx[varValue.TimeStamp];
          //Get row index
          if (!mapVarToRowIndx.TryGetValue(varValue.VariableID, out rowIndx))
          { //Add row(s) for variable (bits)
            if (!dddVariable.IsBitSet())
            { //Simple variable - add one row
              valTable.Rows.Add(valTable.NewRow());
              rowIndx = valTable.Rows.Count - 1;
              mapVarToRowIndx.Add(varValue.VariableID, rowIndx);
              //Set variable name
              valTable.Rows[rowIndx][0] = dddVariable.FormatVarTitle(true, 0, true);
            }
            else
            { //Bitset variable - add one row for each bit
              DDDBitsetRepres bitsetRepres = (DDDBitsetRepres)dddVariable.Representation;
              //To map add only first row - represents complete bitset
              rowIndx = valTable.Rows.Count;
              mapVarToRowIndx.Add(varValue.VariableID, rowIndx);
              //Add rows for bits
              foreach (long mask in bitsetRepres.GetUsedBitMasks())
              {
                DataRow row = valTable.NewRow();
                valTable.Rows.Add(row);
                //Set bit name
                row[0] = dddVariable.FormatVarTitle(true, mask, true);
              }
            }
          }

          //Set value for found row(s) and column
          if (!dddVariable.IsBitSet())
            //Set value
            valTable.Rows[rowIndx][colIndx] = dddVariable.FormatValue(varValue.Value);
          else
          { //bitset - one value for each bit
            DDDBitsetRepres bitsetRepres = (DDDBitsetRepres)dddVariable.Representation;
            foreach (long mask in bitsetRepres.GetUsedBitMasks())
              valTable.Rows[rowIndx++][colIndx] = bitsetRepres.FormatBitValue(varValue.Value, mask);
          }
        }
        return valTable;
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to create history data table from parsed values", ex);
      }
    }

    /// <summary>
    /// Creates xml document describing markers for diagnostic records selected in given data table.
    /// Method expects that value of latitude and longitude is in RefAttrVal0 and 1 column.
    /// Markers may be displayed on the map.
    /// </summary>
    /// <param name="table">Table with selected diagnostic records</param>
    /// <returns>Markers XML document</returns>
    private XmlDocument GetMarkerXmlFromDataTable(DiagnosticData.DiagnosticRecordsDataTable table)
    {
      try
      {
        //Determine minimum distance between markers
        double minDist = 100;
        if (CustomConfiguration != null)
          minDist = CustomConfiguration.MarkerMinDistance;
        //Build dictionary of GPS points together with list of associated row indexes
        Dictionary<GPSPoint, List<int>> markers = new Dictionary<GPSPoint, List<int>>();
        for (int i = 0; i < table.Rows.Count; i++)
        { //Iterate over all rows in given records table
          GPSPoint point;
          if (table[i].IsLatitudeNull() || table[i].IsLongitudeNull() ||
              table[i].Latitude == 0 || table[i].Longitude == 0)
            point = new GPSPoint(0, 0, minDist); //Invalid GPS coordinates
          else
            point = new GPSPoint(table[i].Latitude, table[i].Longitude, minDist);
          if (!markers.ContainsKey(point))
            markers.Add(point, new List<int>());
          markers[point].Add(i);
        }

        //Now create the document
        NumberFormatInfo numFormat = new NumberFormatInfo();
        numFormat.NumberDecimalSeparator = ".";
        //Create XML document
        XmlDocument doc = new XmlDocument();
        //Create document element
        doc.AppendChild(doc.CreateElement("markers"));
        //Set attributes of document element
        doc.DocumentElement.SetAttribute("maxmarkertabrecs", CustomConfiguration.MaxMarkerTabRecs.ToString());
        doc.DocumentElement.SetAttribute("invalidgpstext", CustomConfiguration.InvalidGPSText);
        doc.DocumentElement.SetAttribute("markertabcaptionbase", CustomConfiguration.MarkerTabCaptionBase);
        doc.DocumentElement.SetAttribute("invalidgpsiconurl", CustomConfiguration.InvalidGPSIconURL);
        doc.DocumentElement.SetAttribute("selectedmarkericonurl", CustomConfiguration.SelectedMarkerIconURL);
        //Iterate over all GPS points
        int markerIndex = 0;
        foreach (KeyValuePair<GPSPoint, List<int>> marker in markers)
        {
          //Create marker element
          XmlElement markerElem = doc.CreateElement("marker");
          markerElem.SetAttribute("lat", marker.Key.Latitude.ToString(numFormat));
          markerElem.SetAttribute("lng", marker.Key.Longitude.ToString(numFormat));
          markerElem.SetAttribute("markerindex", markerIndex.ToString());
          //Iterate over all rows associated with this point, append elements
          String title = "";
          foreach (int i in marker.Value)
          {//Create recInfo element and add to marker element
            //Create elemnt and set attributes
            XmlElement recElem = doc.CreateElement("recinfo");
            recElem.SetAttribute("starttime", table[i].StartTime.ToString(DDDHelper.TimeFormat));
            title = table[i].RecTitle;
            recElem.InnerText = " ";
            recElem.SetAttribute("title", title);
            recElem.SetAttribute("recordindex", i.ToString());
            markerElem.AppendChild(recElem);
          }
          //Set title to marker element
          if (marker.Key.Latitude == 0 && marker.Key.Longitude == 0)
            markerElem.SetAttribute("title", CustomConfiguration.InvalidGPSMarkerTitle);
          else if (marker.Value.Count > 1)
            markerElem.SetAttribute("title", CustomConfiguration.MoreRecsTitle);
          else
            markerElem.SetAttribute("title", title);
          //Append marker elemnt to document
          doc.DocumentElement.AppendChild(markerElem);
          markerIndex++;
        }
        //return the document
        return doc;
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to obtain XML from selected records", ex);
      }
    }
    #endregion
    
    #region Public static methods
    /// <summary>
    /// Method fills in additional attributes to row from DIagnosticRecords table.
    /// It uses data already loaded from DB to obtain configuration objects and fill in the additional attributes
    /// </summary>
    /// <param name="row">Row to be filled (it has to contain data loaded from DB)</param>
    /// <param name="dddHelper">DDD Helper object used to obtain configuration data</param>
    /// <param name="customConfiguration">Custom configuration used to fill in custom added columns</param>
    /// <param name="firstCustColIndex">Index of first custom column in the table</param>
    public static void FillDiagRecRowAttributes(DiagnosticData.DiagnosticRecordsRow row, DDDHelper dddHelper, CustomConfiguration customConfiguration, int firstCustColIndex)
    {
      DDDVersion ver;
      DDDRecord rec;
      TUInstance inst;

      //Modify times to correct time zone
      row.StartTime = dddHelper.UTCToUserTime(row.StartTime);
      if (!row.IsEndTimeNull())
        row.EndTime = dddHelper.UTCToUserTime(row.EndTime);
      row.LastModified = dddHelper.UTCToUserTime(row.LastModified);
      if (!row.IsAcknowledgeTimeNull())
        row.AcknowledgeTime = dddHelper.UTCToUserTime(row.AcknowledgeTime);

      //Obtain helper objects
      ver = dddHelper.GetVersion(row.DDDVersionID);
      inst = dddHelper.GetTUInstance(row.TUInstanceID);
      rec = ver.GetRecord(row.RecordDefID);

      //Fill in missing columns using helper objects
      row.TUInstanceName = inst.TUName;
      row.TUTypeName = inst.TUTypeObject.TypeName;
      row.UserVersion = ver.UserVersion;
      row.RecTitle = rec.Title;
      row.RecordName = rec.Name;
      if (rec.GetType() == typeof(DDDEventRecord))
      {
        row.RecordType = "event";
        row.FaultCode = ((DDDEventRecord)rec).FaultCode;
        row.FaultSeverity = ((DDDEventRecord)rec).Severity.ToString();
        row.FaultSubSeverity = ((DDDEventRecord)rec).SubSeverity;
      }
      else if (rec.GetType() == typeof(DDDTraceRecord))
      {
        row.RecordType = "trace";
        row.TrcSnpCode = ((DDDTraceRecord)rec).Code;
      }
      else if (rec.GetType() == typeof(DDDSnapRecord))
      {
        row.RecordType = "snap";
        row.TrcSnpCode = ((DDDSnapRecord)rec).Code;
      }

      //Fill in hierarchy names
      if (!row.IsHierarchyItemIDNull() && row.HierarchyItemID != -1)
      { //Valid hierarchy item ID - obtain names of train and vehicle
        FleetHierarchy hier = dddHelper.HierarchyHelper.ObtainHierarchy(row.StartTime);
        FleetHierarchyItem hierItem = hier.GetHierarchyItem(row.HierarchyItemID);
        FHVehicle vehicle = hierItem.GetVehicle();
        FHTrain train = hierItem.GetTrain();
        if (vehicle != null)
          row.VehicleName = vehicle.Name;
        if (train != null)
          row.TrainName = train.Name;
      }

      //Parse values of referential attributes
      ParseRefAttrVals(row, dddHelper, customConfiguration);
      //In case of custom grid configuration - obtain values of custom columns
      if (customConfiguration != null)
      { //Custom grid configuration is used
        double? lat = null; double? lng = null; double? par2 = null; double? par3 = null;
        if (!row.IsLatitudeNull())
          lat = row.Latitude;
        if (!row.IsLongitudeNull())
          lng = row.Longitude;
        if (!row.IsRefAttrVal2Null())
          par2 = row.RefAttrVal2;
        if (!row.IsRefAttrVal3Null())
          par3 = row.RefAttrVal3;
        List<String> vals = customConfiguration.GetFormattedValues(
          lat, lng, par2, par3, row.RecTitle);
        for(int i = 0; i < vals.Count; i++)
          row[firstCustColIndex + i] = vals[i];
      }
    }

    /// <summary>
    /// Parses values of referential attributes for given record row.
    /// Updates ref attr values in the row
    /// </summary>
    private static void ParseRefAttrVals(
      DiagnosticData.DiagnosticRecordsRow row, 
      DDDHelper dddHelper, 
      CustomConfiguration customConfiguration)
    {
      try
      {
        //Check for nonempty env data
        if (row.IsBinaryDataNull() || row.BinaryData.Length == 0)
          return; //Empty or no env. data
        //Create parsing structure
        sRecBinData recBinData;
        List<sParsedRefAttrValue> parsedValues;
        eParseStatus parseStat;
        //Create structure with record binary data
        recBinData.TUInstanceID = row.TUInstanceID;
        recBinData.RecordInstID = row.RecordInstID;
        recBinData.DDDVersionID = row.DDDVersionID;
        recBinData.RecordDefID = row.RecordDefID;
        recBinData.StartTime = row.StartTime;
        if (!row.IsEndTimeNull())
          recBinData.EndTime = row.EndTime;
        else
          recBinData.EndTime = DateTime.MinValue;
        recBinData.BigEndian = row.BigEndianData;
        recBinData.BinData = row.BinaryData;
        //Call parse method of DDDHelper
        parseStat = dddHelper.ParseBinRecordRefAttr(recBinData, out parsedValues);
        //Check parsing result
        if (parseStat == eParseStatus.parseSuccessfull)
        { //Parsing successfull
          foreach (sParsedRefAttrValue refAttrVal in parsedValues)
            if (customConfiguration.LatRefAttrID.HasValue && customConfiguration.LatRefAttrID == refAttrVal.RefAttrID)
              row.Latitude = Convert.ToDouble(refAttrVal.Value);
            else if (customConfiguration.LngRefAttrID.HasValue && customConfiguration.LngRefAttrID == refAttrVal.RefAttrID)
              row.Longitude = Convert.ToDouble(refAttrVal.Value);
            else if (customConfiguration.RefAttrID2.HasValue && customConfiguration.RefAttrID2 == refAttrVal.RefAttrID)
              row.RefAttrVal2 = Convert.ToDouble(refAttrVal.Value);
            else if (customConfiguration.RefAttrID3.HasValue && customConfiguration.RefAttrID3 == refAttrVal.RefAttrID)
              row.RefAttrVal3 = Convert.ToDouble(refAttrVal.Value);
        }
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to obtain values of referential attributes", ex);
      }
    }
    #endregion
  }
}
