using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Data.SqlClient;
using StatisticsTableAdapters;
using DDDObjects;
using DRQueries;
using FleetHierarchies;

namespace BLL
{
  /// <summary>
  /// Provides access to records statistics table through bussiness logic
  /// </summary>
  [System.ComponentModel.DataObject]
  public class StatisticsBLL
  {
    #region Private members
    private RecordsStatisticsTableAdapter statisticsAdapter = null;
    private TUStatisticsTableAdapter tuStatsAdapter = null;
    private DDDHelper dddHelper = null;
    private TDManagementForms.Logger logger = null; //Logger object
    #endregion //Private members

    #region Public properties
    public RecordsStatisticsTableAdapter Adapter
    {
      get
      {
        if (statisticsAdapter == null)
        {
          statisticsAdapter = new RecordsStatisticsTableAdapter();
          statisticsAdapter.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["dbQueryTimeoutSec"]);
        }
        return statisticsAdapter;
      }
    }

    public TUStatisticsTableAdapter TUStatsAdapter
    {
      get
      {
        if (tuStatsAdapter == null)
        {
          tuStatsAdapter = new TUStatisticsTableAdapter();
          tuStatsAdapter.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["dbQueryTimeoutSec"]);
        }
        return tuStatsAdapter;
      }
    }

    public DDDHelper DDDHelper
    {
      get { return dddHelper; }
      set { dddHelper = value; }
    }

    /// <summary>
    /// Logger object
    /// </summary>
    public TDManagementForms.Logger Logger
    {
      get { return logger; }
      set { logger = value; }
    }
    #endregion //Public properties

    #region Public methods
    /// <summary>
    /// Returns data table with statistics of diagnostic records corresponding with given set of parameters
    /// </summary>
    /// <returns></returns>
    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public Statistics.RecordsStatisticsDataTable GetRecordsStatistics(
      String TUInstanceIDs, String HierarchyItemIDs,
      DateTime? StartTimeFrom, DateTime? StartTimeTo,
      String Sources, String VehicleNumbers, String FaultCodes, String TrcSnpCodes, 
      String TitleKeywords, bool MatchAllKeywords,
      int? RecordTypes, String Severities, String SubSeverities, 
      out int totalRecCount, out int recTypeCount)
    {
      //Declare table
      Statistics.RecordsStatisticsDataTable table = new Statistics.RecordsStatisticsDataTable();
      //Start stopwatch
      Stopwatch stopwatch = Stopwatch.StartNew();
      //Convert times to UTC
      if (StartTimeFrom.HasValue)
        StartTimeFrom = DDDHelper.UserTimeToUTC((DateTime)StartTimeFrom);
      if (StartTimeTo.HasValue)
        StartTimeTo = DDDHelper.UserTimeToUTC((DateTime)StartTimeTo);
      //Get text of DB query
      String queryText;
      queryText = DRQueryHelper.GetQuery_WebStatistics(
        TUInstanceIDs, HierarchyItemIDs, StartTimeFrom, StartTimeTo, Sources, VehicleNumbers, FaultCodes,
        TrcSnpCodes, TitleKeywords, MatchAllKeywords, RecordTypes, Severities, SubSeverities);
      //Load data from DB using DB connection
      using (SqlConnection dbConnection = new SqlConnection(
        ConfigurationManager.ConnectionStrings["CDDBConnectionString"].ConnectionString))
      { //Create connection to DB
        dbConnection.Open(); //Open the connection
        using (SqlCommand dbCmd = new SqlCommand(queryText, dbConnection))
        { //Create database command
          dbCmd.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["dbQueryTimeoutSec"]);
          dbCmd.CommandType = CommandType.Text;
          using (SqlDataReader reader = dbCmd.ExecuteReader())
          { //Execute the command and obtain data reader
            table.Load(reader); //Load data from reader into the table
          }
        }
      }
      //Store select time, restart stopwatch
      long selectTime = stopwatch.ElapsedMilliseconds;
      stopwatch.Reset(); stopwatch.Start();

      //Fill in missing columns for each row
      try
      {
        totalRecCount = 0;
        foreach (Statistics.RecordsStatisticsRow row in table)
        { //iterate over all rows in data table
          FillStatisticsRowAttributes(row, dddHelper);
          //Compute total record count
          if (!row.IsRecCountNull())
            totalRecCount += row.RecCount;
        }
        //Assign record type count
        recTypeCount = table.Count;
      }
      catch (Exception ex)
      {
        throw new Exception("DDD Helper objects failed", ex);
      }
      //Log elapsed times
      long procTime = stopwatch.ElapsedMilliseconds;
      Logger.LogText(5, "StatisticsBLL", "Record statistics select operation {0} ms, processing time {1} ms", selectTime, procTime);
      return table;
    }

    /// <summary>
    /// Returns data table with statistics of diagnostic records for each TU and given time range
    /// </summary>
    /// <returns></returns>
    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public Statistics.TUStatisticsDataTable GetTUStatistics(int rangeDays, out int totalRecCount, out int intervalRecCount)
    {
      Statistics.TUStatisticsDataTable table;
      //Start stopwatch
      Stopwatch stopwatch = Stopwatch.StartNew();
      //Obtain table with statistics loaded from DB
      int? TotalDBRecCount;
      int? IntervalRecCount;
      table = TUStatsAdapter.GetData(rangeDays, out TotalDBRecCount, out IntervalRecCount);
      //Set output parameters
      if (TotalDBRecCount.HasValue)
        totalRecCount = TotalDBRecCount.Value;
      else
        totalRecCount = 0;
      if (IntervalRecCount.HasValue)
        intervalRecCount = IntervalRecCount.Value;
      else
        intervalRecCount = 0;

      //Modify times according to time zone
      foreach (Statistics.TUStatisticsRow row in table)
      { //iterate over all rows in data table
        //Modify times to correct time zone
        if (!row.IsLastRecTimeNull())
          row.LastRecTime = dddHelper.UTCToUserTime(row.LastRecTime);
      }
      //Log elapsed times
      long selTime = stopwatch.ElapsedMilliseconds;
      Logger.LogText(5, "StatisticsBLL", "TUStatistics select operation {0} ms", selTime);
      return table;
    }
    #endregion //Public methods

    #region Helper methods
    /// <summary>
    /// Method fills in additional attributes to row from Statistics table.
    /// It uses data already loaded from DB to obtain configuration objects and fill in the additional attributes
    /// </summary>
    /// <param name="row">Row to be filled (it has to contain data loaded from DB)</param>
    /// <param name="dddHelper">DDD Helper object used to obtain configuration data</param>
    public static void FillStatisticsRowAttributes(Statistics.RecordsStatisticsRow row, DDDHelper dddHelper)
    {
      DDDVersion ver;
      DDDRecord rec;
      TUInstance inst;

      //Modify times to correct time zone
      if (!row.IsFirstTimeNull())
        row.FirstTime = dddHelper.UTCToUserTime(row.FirstTime);
      if (!row.IsLastTimeNull())
        row.LastTime = dddHelper.UTCToUserTime(row.LastTime);

      //Obtain helper objects
      ver = dddHelper.GetVersion(row.DDDVersionID);
      inst = dddHelper.GetTUInstance(row.TUInstanceID);
      rec = ver.GetRecord(row.RecordDefID);

      //Fill in missing columns using helper objects
      row.TUInstanceName = inst.TUName;
      row.UserVersion = ver.UserVersion;
      row.RecTitle = rec.Title;
      if (rec.GetType() == typeof(DDDEventRecord))
      {
        row.RecordType = "event";
        row.FaultCode = ((DDDEventRecord)rec).FaultCode;
        row.FaultSeverity = ((DDDEventRecord)rec).Severity.ToString();
        row.FaultSubSeverity = ((DDDEventRecord)rec).SubSeverity;
      }
      else if (rec.GetType() == typeof(DDDTraceRecord))
      {
        row.RecordType = "trace";
        row.TrcSnpCode = ((DDDTraceRecord)rec).Code;
      }
      else if (rec.GetType() == typeof(DDDSnapRecord))
      {
        row.RecordType = "snap";
        row.TrcSnpCode = ((DDDSnapRecord)rec).Code;
      }

      //Fill in hierarchy names
      if (!row.IsHierarchyItemIDNull() && row.HierarchyItemID != -1)
      { //Valid hierarchy item ID - obtain names of train and vehicle
        FleetHierarchy hier = dddHelper.HierarchyHelper.ObtainHierarchy(row.LastTime);
        FleetHierarchyItem hierItem = hier.GetHierarchyItem(row.HierarchyItemID);
        FHVehicle vehicle = hierItem.GetVehicle();
        FHTrain train = hierItem.GetTrain();
        if (vehicle != null)
          row.VehicleName = vehicle.Name;
        if (train != null)
          row.TrainName = train.Name;
      }
    }
    #endregion
  }
}
