using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Xml;
using DDDObjects;
using DRQueries;
using ZetaLibWeb;


public partial class DownloadBinRecords : System.Web.UI.Page
{
  /// <summary>
  /// Called when page is loaded to be sent to the client
  /// </summary>
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      //Clear response stream
      Response.Clear();
      //Obtain logger object
      TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
      try
      {
        //Log operation
        LogDownloadOperation();
        //Load records from DB and serialize to output stream
        using (SqlConnection dbConn =
          new SqlConnection(ConfigurationManager.ConnectionStrings["CDDBConnectionString"].ConnectionString))
        { //Create and open DB connection
          dbConn.Open();
          using (SqlCommand dbCmd = new SqlCommand(GetDRExportQuery(), dbConn))
          { //Create DB Command
            dbCmd.CommandType = CommandType.Text;
            dbCmd.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["dbQueryTimeoutSec"]);
            using (SqlDataReader reader = dbCmd.ExecuteReader())
            { //Obtain reader with result set
              //Clear response stream
              Response.Clear();
              //Set response properties
              Response.ContentType = "application/octet-stream";
              Response.AddHeader("Content-Disposition", "attachment; filename=" + "Records.dr");
              //Create record serializer object
              DRSerializer recSerializer = new DRSerializer((DDDHelper)Application["DDDHelper"], Response.OutputStream, true);
              //Write to response stream record by record
              int writtenCount = 0;
              while (recSerializer.WriteDBRecordToStream(reader))
              {
                writtenCount++;
                if (writtenCount % 200 == 0)
                  Response.Flush(); //Flush response each 200 records
              }
              //All records written, write used DDD objects
              if (Request.QueryString["IncludeDDDObjects"] != null && Boolean.Parse(Request.QueryString["IncludeDDDObjects"]))
                recSerializer.WriteUsedDDDObjects();
              //Close zip stream
              recSerializer.CloseZipStream();
            }
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to send diagnostic data as binary file", ex);
      }
      //Close response
      Response.Flush();
      Response.End();
      //Log end of operation
      logger.LogText(3, "DownloadBinRecords", "Download for user {0} finished", HttpContext.Current.User.Identity.Name);
    }
  }

  /// <summary>
  /// Handler for any errors in page not handled in code by catch blocks.
  /// It is actually page-level exception handler for all types of exceptions.
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  private void Page_Error(object sender, EventArgs e)
  {
    Exception ex = Server.GetLastError();
    //Log exception
    //Obtain logger object
    TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
    //Log operation
    logger.LogText(1, "DownloadBinRecords", "Page error occured for user {0}", HttpContext.Current.User.Identity.Name);
    logger.LogException(2, "DownloadBinRecords", ex);
    //Store in session end redirect to error page
    Session["Exception"] = ex;
    Response.Redirect("~/Error.aspx");
  }

  /// <summary>
  /// Log parameters of download operation
  /// </summary>
  protected void LogDownloadOperation()
  {
    //Obtain logger object
    TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
    //Log the operation
    logger.LogText(3, "DownloadBinRecords", "User {0} downloading recods with parameters:", HttpContext.Current.User.Identity.Name);
    logger.LogText(4, "DownloadBinRecords", "TUInstanceIDs: {0}, HierarchyItemPaths: {1}, StartTimeFrom: {2}, StartTimeTo: {3}, Sources: {4}, VehicleNumbers: {5}",
      (Request.QueryString["TUInstanceIDs"] != null) ? Request.QueryString["TUInstanceIDs"] : "-",
      (Request.QueryString["HierarchyItemPaths"] != null) ? Request.QueryString["HierarchyItemPaths"] : "-",
      (Request.QueryString["StartTimeFrom"] != null) ? Request.QueryString["StartTimeFrom"] : "-",
      (Request.QueryString["StartTimeTo"] != null) ? Request.QueryString["StartTimeTo"] : "-",
      (Request.QueryString["Sources"] != null) ? Request.QueryString["Sources"] : "-",
      (Request.QueryString["VehicleNumbers"] != null) ? Request.QueryString["VehicleNumbers"] : "-");
    logger.LogText(4, "DownloadBinRecords", "FaultCodes: {0}, TrcSnpCodes: {1}, TitleKeywords: {2}, MatchAllKeywords: {3}, RecordTypes: {4}, Severities: {5}, SubSeverities: {6}, MaxRecords: {7}",
      (Request.QueryString["FaultCodes"] != null) ? Request.QueryString["FaultCodes"] : "-",
      (Request.QueryString["TrcSnpCodes"] != null) ? Request.QueryString["TrcSnpCodes"] : "-",
      (Request.QueryString["TitleKeywords"] != null) ? Request.QueryString["TitleKeywords"] : "-",
      (Request.QueryString["MatchAllKeywords"] != null) ? Request.QueryString["MatchAllKeywords"] : "-",
      (Request.QueryString["RecordTypes"] != null) ? Request.QueryString["RecordTypes"] : "-",
      (Request.QueryString["Severities"] != null) ? Request.QueryString["Severities"] : "-",
      (Request.QueryString["SubSeverities"] != null) ? Request.QueryString["SubSeverities"] : "-",
      (Request.QueryString["MaxRecords"] != null) ? Request.QueryString["MaxRecords"] : "-",
      (Request.QueryString["IncludeDDDObjects"] != null) ? Request.QueryString["IncludeDDDObjects"] : "-"
    );
  }

  /// <summary>
  /// Returns query which selects diagnostic records from DB in format suitable 
  /// for export of diagnostic records into binary file.
  /// Query parameters are extracted from URL string
  /// </summary>
  private String GetDRExportQuery()
  {
    String queryText = DRQueryHelper.GetQuery_RecordsApp(
      Request.QueryString["TUInstanceIDs"], 
      Request.QueryString["HierarchyItemIDs"],
      (Request.QueryString["StartTimeFrom"] != null) ? (DateTime?)Convert.ToDateTime(Request.QueryString["StartTimeFrom"]) : null,
      (Request.QueryString["StartTimeTo"] != null) ? (DateTime?)Convert.ToDateTime(Request.QueryString["StartTimeTo"]) : null,
      Request.QueryString["Sources"],
      Request.QueryString["VehicleNumbers"],
      Request.QueryString["FaultCodes"],
      Request.QueryString["TrcSnpCodes"],
      Request.QueryString["TitleKeywords"],
      (Request.QueryString["MatchAllKeywords"] != null) ? (Boolean)Convert.ToBoolean(Request.QueryString["MatchAllKeywords"]) : false,
      (Request.QueryString["RecordTypes"] != null) ? (eRecordCategory?)Convert.ToInt32(Request.QueryString["RecordTypes"]) : null,
      Request.QueryString["Severities"],
      Request.QueryString["SubSeverities"],
     (Request.QueryString["MaxRecords"] != null) ? Convert.ToInt32(Request.QueryString["MaxRecords"]) : 0,
      "StartTime",
      eSortOrder.Descending);
    return queryText;
  }

}
