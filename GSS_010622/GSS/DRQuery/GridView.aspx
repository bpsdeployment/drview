﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="GridView.aspx.cs"
  Inherits="_Default" Title="Grid view of diagnostic records" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
  Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="cntGridView" ContentPlaceHolderID="contentPH" runat="Server">
  <!--AJAX script manager-->
  <asp:ScriptManager ID="ScriptManager" runat="server" />
  <!--Register event handler for page loaded event-->

  <script type="text/javascript">
    try { Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageLoaded); }
    catch(err)
    {}
  </script>

  <div style="margin: 5px">
    <!--Panel with map-->
    <asp:UpdatePanel ID="updPanMap" runat="server" UpdateMode="Conditional">
      <ContentTemplate>
        <asp:Panel ID="panMapGlobal" runat="server" BorderStyle="Solid" BorderWidth="2px"
          BorderColor="Black" Height="400px" Width="400px" Style="margin: 0px;">
          <asp:Panel ID="panMapHead" runat="server" BackColor="LightGray" Width="100%" Height="20px">
            <!--Heading for map part of the page-->
            <h4 style="margin: 0px">
              <table width="100%">
                <tr>
                  <td width="100%">
                    Map</td>
                  <td>
                    <asp:ImageButton ID="imgCloseMap" runat="server" ImageUrl="~/Pictures/Cross.bmp"
                      OnClick="imgCloseMap_Click" ImageAlign="Top" /></td>
                </tr>
              </table>
            </h4>
          </asp:Panel>
          <!--Panel (created as collapsible) with map-->
          <asp:Panel ID="panMap" runat="server" BackColor="LightBlue" Width="100%" Height="380px">
          </asp:Panel>
          <!--Dummy update panel used for event showing markers-->
          <asp:UpdatePanel ID="updPanMarkers" runat="server" UpdateMode="Conditional">
            <Triggers>
              <asp:AsyncPostBackTrigger ControlID="gvRecords" EventName="PageIndexChanged" />
            </Triggers>
            <ContentTemplate>
            </ContentTemplate>
          </asp:UpdatePanel>
        </asp:Panel>
        <!--AJAX control managing the resizibility of map panel-->
        <ajaxToolkit:ResizableControlExtender ID="resizeMap" runat="server" TargetControlID="panMapGlobal"
          HandleCssClass="handleImage" MinimumHeight="400" MinimumWidth="400" MaximumHeight="800"
          MaximumWidth="800" OnClientResize="ResizeMapWindow" OnClientResizing="ResizeMap"
          HandleOffsetX="16" HandleOffsetY="16" />
        <!--AJAX control managing the dragability of map panel-->
        <ajaxToolkit:DragPanelExtender ID="dragMap" runat="server" TargetControlID="panMapGlobal"
          DragHandleID="panMapHead" />
      </ContentTemplate>
      <Triggers>
      </Triggers>
    </asp:UpdatePanel>
    <p align="right">
      <asp:Button ID="btQuery" runat="server" OnClick="btQuery_Click" Text="Modify query" /></p>
    <!--Heading of table with diagnostic records-->
    <!--Contains drop down for record counts-->
    <table style="width: 100%" cellpadding="0" cellspacing="0">
      <tr>
        <td>
          <h4 style="margin-bottom: 5px;">
            <asp:ImageButton ID="imgExpandRecords" runat="server" ImageUrl="~/Pictures/expand_blue.jpg" />
            Diagnostic records&nbsp;
            <asp:ImageButton ID="btShowMap" runat="server" ImageUrl="~/Pictures/showmap.bmp"
              OnClick="btShowMap_Click" ToolTip="Show records on map" /></h4>
        </td>
        <td style="width: 400px; text-align: right;">
          <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
              <h4 style="font-size: small; margin-bottom: 8px;">
                Records
                <asp:Label ID="lbRecRange" runat="server" Text="Label"></asp:Label>
                of about
                <asp:Label ID="lbRecCount" runat="server" Text="Label"></asp:Label>
                . Page size
                <asp:DropDownList ID="ddlPageSize" runat="server" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"
                  AutoPostBack="True">
                  <asp:ListItem>10</asp:ListItem>
                  <asp:ListItem Selected="True">20</asp:ListItem>
                  <asp:ListItem>30</asp:ListItem>
                  <asp:ListItem>40</asp:ListItem>
                  <asp:ListItem>50</asp:ListItem>
                  <asp:ListItem>60</asp:ListItem>
                  <asp:ListItem>70</asp:ListItem>
                  <asp:ListItem>80</asp:ListItem>
                  <asp:ListItem>90</asp:ListItem>
                  <asp:ListItem>100</asp:ListItem>
                </asp:DropDownList>
              </h4>
            </ContentTemplate>
            <Triggers>
              <asp:AsyncPostBackTrigger ControlID="gvRecords" EventName="PageIndexChanged" />
              <asp:PostBackTrigger ControlID="ddlPageSize" />
            </Triggers>
          </asp:UpdatePanel>
        </td>
      </tr>
    </table>
    <asp:Panel ID="panDiagRecs" runat="server">
      <!--Update progress of diagnostic records table-->
      <asp:UpdateProgress ID="updProgDiagRecs" runat="server" AssociatedUpdatePanelID="updPanDiagRecs"
        DisplayAfter="100">
        <ProgressTemplate>
          <img src="Pictures/loading.gif" />
          <b>Updating diagnostic records...</b>
        </ProgressTemplate>
      </asp:UpdateProgress>
      <!--Update panel with data source and grid view of diagnostic records-->
      <asp:UpdatePanel ID="updPanDiagRecs" runat="server">
        <ContentTemplate>
          <asp:ObjectDataSource ID="RecordsDataSource" runat="server" EnablePaging="True" MaximumRowsParameterName="MaximumRows"
            OldValuesParameterFormatString="original_{0}" OnObjectCreated="RecordsDataSource_ObjectCreated"
            SelectCountMethod="GetTotalRecords" SelectMethod="GetDiagnosticRecords" StartRowIndexParameterName="StartRowIndex"
            TypeName="BLL.DiagnosticRecordsBLL" OnSelected="RecordsDataSource_Selected" OnSelecting="RecordsDataSource_Selecting">
            <SelectParameters>
              <asp:QueryStringParameter Name="TUInstanceIDs" QueryStringField="TUInstanceIDs" Type="String" />
              <asp:QueryStringParameter Name="HierarchyItemIDs" QueryStringField="HierarchyItemIDs" Type="String"/>
              <asp:QueryStringParameter DefaultValue="" Name="StartTimeFrom" QueryStringField="StartTimeFrom" Type="DateTime" />
              <asp:QueryStringParameter DefaultValue="" Name="StartTimeTo" QueryStringField="StartTimeTo" Type="DateTime" />
              <asp:QueryStringParameter DefaultValue="" Name="Sources" QueryStringField="Sources" Type="String" />
              <asp:QueryStringParameter DefaultValue="" Name="VehicleNumbers" QueryStringField="VehicleNumbers" Type="String" />
              <asp:QueryStringParameter DefaultValue="" Name="FaultCodes" QueryStringField="FaultCodes" Type="String" />
              <asp:QueryStringParameter DefaultValue="" Name="TrcSnpCodes" QueryStringField="TrcSnpCodes" Type="String" />
              <asp:QueryStringParameter DefaultValue="" Name="TitleKeywords" QueryStringField="TitleKeywords" Type="String" />
              <asp:QueryStringParameter DefaultValue="" Name="MatchAllKeywords" QueryStringField="MatchAllKeywords" Type="Boolean" />
              <asp:QueryStringParameter DefaultValue="0" Name="RecordTypes" QueryStringField="RecordTypes"
                Type="Int32" />
              <asp:QueryStringParameter DefaultValue="" Name="Severities" QueryStringField="Severities" Type="String" />
              <asp:QueryStringParameter DefaultValue="" Name="SubSeverities" QueryStringField="SubSeverities" Type="String" />
              <asp:Parameter Name="StartRowIndex" Type="Int32" />
              <asp:Parameter Name="MaximumRows" Type="Int32" />
              <asp:SessionParameter DefaultValue="StartTime" Name="SortExpression" SessionField="SortExpression"
                Type="String" />
              <asp:SessionParameter DefaultValue="1" Name="SortOrderDesc" SessionField="SortOrder"
                Type="Boolean" />
              <asp:Parameter ConvertEmptyStringToNull="False" Direction="Output" Name="RecMarkersXML"
                Type="Object" />
            </SelectParameters>
          </asp:ObjectDataSource>
          <asp:GridView ID="gvRecords" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            DataKeyNames="TUInstanceID,RecordInstID,RecordDefID,DDDVersionID" DataSourceID="RecordsDataSource"
            PageSize="20" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px"
            CellPadding="3" ForeColor="Black" OnDataBound="gvRecords_DataBound" OnPageIndexChanged="gvRecords_PageIndexChanged"
            OnSelectedIndexChanged="gvRecords_SelectedIndexChanged" AllowSorting="True" OnSorting="gvRecords_Sorting">
            <Columns>
              <asp:CommandField ShowSelectButton="True" />
            </Columns>
            <FooterStyle BackColor="#CCCCCC" />
            <SelectedRowStyle BackColor="#0055FF" Font-Bold="True" ForeColor="Black" />
            <PagerStyle BackColor="#FFDDDD" ForeColor="Blue" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="#DDDDFF" BorderColor="White" BorderStyle="Solid"
              BorderWidth="2px" />
            <RowStyle BackColor="#DDFFDD" BorderColor="White" BorderStyle="Solid" BorderWidth="2px" />
          </asp:GridView>
          <!--Hidden field holding index of selected record-->
          <asp:HiddenField ID="hfSelectedIndex" runat="server" Value="-1" />
          <!--div holding markers XML-->
          <xml id="markersXml">
            <asp:Xml ID="xmlMarkers" runat="server" />
          </xml>
        </ContentTemplate>
        <Triggers>
          <asp:AsyncPostBackTrigger ControlID="gvRecords" EventName="PageIndexChanged" />
        </Triggers>
      </asp:UpdatePanel>
    </asp:Panel>
    <!--Extender managing the collapsibility of the diagnostic records panel-->
    <ajaxToolkit:CollapsiblePanelExtender runat="server" ID="collapseDiagRecs" CollapseControlID="imgExpandRecords"
      ExpandControlID="imgExpandRecords" TargetControlID="panDiagRecs" ImageControlID="imgExpandRecords"
      ExpandedImage="Pictures/collapse_blue.jpg" CollapsedImage="Pictures/expand_blue.jpg"
      SuppressPostBack="true" Collapsed="false" />
    <br />
    <div>
      <!--Environmental variables part of the page-->
      <h4>
        <asp:ImageButton ID="imgExpandEnvVars" runat="server" ImageUrl="~/Pictures/expand_blue.jpg" />
        Environmental variables
      </h4>
      <!--Panel (created as collapsible), which contains data source and grid with environmental variables-->
      <asp:Panel ID="panEnvVars" runat="server" Height="0px">
        <!--Displays update progress for environmental variables-->
        <asp:UpdateProgress ID="updProgEnvVars" runat="server" AssociatedUpdatePanelID="updPanEnvVars">
          <ProgressTemplate>
            <img src="Pictures/loading.gif" />
            <b>Updating environmental data...</b>
          </ProgressTemplate>
        </asp:UpdateProgress>
        <!--Update panel with data source and grid view with environmental variables-->
        <asp:UpdatePanel ID="updPanEnvVars" runat="server" UpdateMode="Conditional">
          <Triggers>
            <asp:AsyncPostBackTrigger ControlID="gvRecords" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="gvRecords" EventName="PageIndexChanged" />
          </Triggers>
          <ContentTemplate>
            <asp:ObjectDataSource ID="VariablesDataSource" runat="server" OldValuesParameterFormatString="original_{0}"
              OnObjectCreated="VariablesDataSource_ObjectCreated" SelectMethod="GetHistEnvDataValues"
              TypeName="BLL.DiagnosticRecordsBLL">
              <SelectParameters>
                <asp:ControlParameter ControlID="gvRecords" Name="TUInstanceID" PropertyName="SelectedDataKey.Values[0]"
                  Type="Object" />
                <asp:ControlParameter ControlID="gvRecords" Name="RecordInstID" PropertyName="SelectedDataKey.Values[1]"
                  Type="Int64" />
                <asp:ControlParameter ControlID="gvRecords" Name="RecordDefID" PropertyName="SelectedDataKey.Values[2]"
                  Type="Int32" />
                <asp:ControlParameter ControlID="gvRecords" Name="DDDVersionID" PropertyName="SelectedDataKey.Values[3]"
                  Type="Object" />
              </SelectParameters>
            </asp:ObjectDataSource>
            <asp:GridView ID="gvVariables" runat="server" BackColor="White" BorderColor="#999999"
              BorderStyle="Solid" BorderWidth="1px" CellPadding="3" DataSourceID="VariablesDataSource"
              ForeColor="Black">
              <FooterStyle BackColor="#CCCCCC" />
              <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="Black" />
              <PagerStyle BackColor="#FFDDDD" ForeColor="Blue" HorizontalAlign="Center" />
              <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
              <AlternatingRowStyle BackColor="#DDDDFF" BorderColor="White" BorderStyle="Solid"
                BorderWidth="2px" />
              <RowStyle BackColor="#DDFFDD" BorderColor="White" BorderStyle="Solid" BorderWidth="2px" />
            </asp:GridView>
          </ContentTemplate>
        </asp:UpdatePanel>
      </asp:Panel>
    </div>
    <!--Extender managing the collapsibility of the env vars panel-->
    <ajaxToolkit:CollapsiblePanelExtender runat="server" ID="colapseEnvVars" CollapseControlID="imgExpandEnvVars"
      ExpandControlID="imgExpandEnvVars" TargetControlID="panEnvVars" ImageControlID="imgExpandEnvVars"
      ExpandedImage="Pictures/collapse_blue.jpg" CollapsedImage="Pictures/expand_blue.jpg"
      SuppressPostBack="true" Collapsed="true" />
  </div>
</asp:Content>
