using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Xml;
using DDDObjects;
using DRQueries;
using ZetaLibWeb;

public partial class Query : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    //Obtain DDDHelper from application state
    DDDHelper dddHelper = (DDDHelper)Application["DDDHelper"];
    if (!Page.IsPostBack)
    {//No postback 
      //Fill in TUInstances from helper
      ddlTUInstances.Items.Clear();
      foreach (KeyValuePair<Guid, TUInstance> tuInstPair in dddHelper.TUInstances)
        ddlTUInstances.Items.Add(new ListItem(tuInstPair.Value.TUName, tuInstPair.Key.ToString()));
      ListControlSort.SortByText(ddlTUInstances);
      //Add item representing all instances
      ddlTUInstances.Items.Insert(0, new ListItem("All", ""));
      //Assign filtered fleet hierarchy XML to XML data source
      if (Session["FleetHierarchy"] == null)
      { //Fleet hierarchy has to be retrieved and filtered
        //Obtain securtiy helper
        SecurityHelper securityHelper = (SecurityHelper)Application["SecurityHelper"];
        //Obtain hierarchy document
        XmlDocument hierDoc = dddHelper.HierarchyHelper.ObtainCurrentHierarchy().GetXMLDocument();
        //Filter the document
        XmlDocument filteredHierDoc = securityHelper.FilterFleetHierarchy(hierDoc, Roles.GetRolesForUser());
        //Save filtered hierarchy XML string in session
        Session["FleetHierarchy"] = filteredHierDoc.InnerXml;
      }
      xmldsFleetHierarchy.Data = (String)Session["FleetHierarchy"];
      tvHierarchy.DataBind();

      //Set values of filter controls according to query string
      SetFiltersAccordingToQueryString();

      //Set visibility of panel for deleting records
      panDeleteRecords.Visible = User.IsInRole("Administrators");
      panTUInstances.Visible = panDeleteRecords.Visible;
    }
  }

  /// <summary>
  /// Handler for any errors in page not handled in code by catch blocks.
  /// It is actually page-level exception handler for all types of exceptions.
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  private void Page_Error(object sender, EventArgs e)
  {
    Exception ex = Server.GetLastError();
    //Log exception
    //Obtain logger object
    TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
    //Log operation
    logger.LogText(1, "Query", "Page error occured for user {0}", HttpContext.Current.User.Identity.Name);
    logger.LogException(2, "Query", ex);
    //Store in session end redirect to error page
    Session["Exception"] = ex;
    Response.Redirect("~/Error.aspx");
  }

  protected void btQuery_Click(object sender, EventArgs e)
  {
    //Redirect to page with grid view of selected records
    Response.Redirect(CreateQueryString("GridView.aspx").All);
  }

  protected void ddlTimeRange_SelectedIndexChanged(object sender, EventArgs e)
  {
    if (Convert.ToDouble(ddlTimeRange.SelectedValue) != -1)
    { //Set time values according to range
      //Obtain DDDHelper from application state
      DDDHelper dddHelper = (DDDHelper)Application["DDDHelper"];
      //Set time fields
      DateTime now = DateTime.UtcNow;
      Double dayOffset = -1 * Convert.ToDouble(ddlTimeRange.SelectedValue);
      tbTimeFrom.Text = dddHelper.UTCToUserTime(now.AddDays(dayOffset)).ToString(ConfigurationManager.AppSettings["dateTimeFormat"]);
      tbTimeTo.Text = dddHelper.UTCToUserTime(now).ToString(ConfigurationManager.AppSettings["dateTimeFormat"]);
    }
  }
  protected void btStatistics_Click(object sender, EventArgs e)
  {
    //Redirect to page with statistics view of selected records
    Response.Redirect(CreateQueryString("StatisticsView.aspx").All);
  }
  protected void btDeleteRecords_Click(object sender, EventArgs e)
  {
    //Redirect to page where records may be deleted
    Response.Redirect(CreateQueryString("~/Admin/DeleteRecords.aspx").All);
  }
  protected void btDownloadBinRecords_Click(object sender, EventArgs e)
  {
    //Redirect to page which writes records selected from DB into response stream
    Response.Redirect(CreateQueryString("DownloadBinRecords.aspx").All);
  }
  protected void btDownloadCSVRecords_Click(object sender, EventArgs e)
  {
    //Redirect to page which writes records selected from DB into response stream
    Response.Redirect(CreateQueryString("DownloadCSVRecords.aspx").All);
  }
  protected void btDownloadStatistics_Click(object sender, EventArgs e)
  {
    //Redirect to page which writes statistics selected from DB into response stream as CSV file
    Response.Redirect(CreateQueryString("DownloadStatistics.aspx").All);
  }
  /// <summary>
  /// Creates query string with all parameters filled by the user and given URL
  /// </summary>
  /// <param name="baseURL">URL of target site</param>
  /// <returns></returns>
  protected QueryString CreateQueryString(String baseURL)
  {
    QueryString queryString = new QueryString(baseURL);
    queryString["TUInstanceIDs"] = ddlTUInstances.SelectedValue.Trim();
    queryString["TUInstanceName"] = ddlTUInstances.SelectedItem.Text;
    queryString["HierarchyItemIDs"] = GetSelectedHierarchyItemIDs().Trim();
    queryString["HierarchyItemPaths"] = GetSelectedHierarchyItemValuePaths().Trim();
    queryString["StartTimeFrom"] = tbTimeFrom.Text.Trim();
    queryString["StartTimeTo"] = tbTimeTo.Text.Trim();
    queryString["TimeRangeValue"] = ddlTimeRange.SelectedValue;
    queryString["RecordTypes"] = GetRecordTypes().Trim();
    queryString["Sources"] = tbRecSource.Text.Trim();
    queryString["FaultCodes"] = tbFaultCode.Text.Trim();
    queryString["TitleKeywords"] = tbTitleKeywords.Text.Trim();
    queryString["MatchAllKeywords"] = chbMatchAllKeywords.Checked.ToString();
    queryString["Severities"] = GetFaultSeverities();
    queryString["SubSeverities"] = tbSubSeverities.Text.Trim();
    queryString["VehicleNumbers"] = tbVehicleNumbers.Text.Trim();
    queryString["TrcSnpCodes"] = tbTrcSnpCodes.Text.Trim();
    queryString["MaxRecords"] = tbMaxRecords.Text.Trim();
    queryString["IncludeDDDObjects"] = chbIncludeDDDObj.Checked.ToString();
    return queryString;
  }

  /// <summary>
  /// Sets values of all filter controls according to current query string
  /// </summary>
  private void SetFiltersAccordingToQueryString()
  {
    //Set time fields
    ddlTimeRange_SelectedIndexChanged(this, null);

    //Set values of controls based on parameters in QueryString
    if (Request.QueryString["TUInstanceIDs"] != null)
      ddlTUInstances.SelectedValue = Request.QueryString["TUInstanceIDs"];
    //Check selected hierarchy items
    CheckHierarchyNodes(Request.QueryString["HierarchyItemPaths"]);
    //Times
    if (Request.QueryString["TimeRangeValue"] != null)
      ddlTimeRange.SelectedValue = Request.QueryString["TimeRangeValue"];
    //Set time fields
    ddlTimeRange_SelectedIndexChanged(this, null);
    if (Request.QueryString["TimeRangeValue"] == "-1")
    {
      if (Request.QueryString["StartTimeFrom"] != null)
        tbTimeFrom.Text = Request.QueryString["StartTimeFrom"];
      else
        tbTimeFrom.Text = String.Empty;
    }
    if (Request.QueryString["TimeRangeValue"] == "-1")
    {
      if (Request.QueryString["StartTimeTo"] != null)
        tbTimeTo.Text = Request.QueryString["StartTimeTo"];
      else
        tbTimeTo.Text = String.Empty;
    }
    //Record types (EVT, TRC...)
    if (Request.QueryString["RecordTypes"] != null)
    {
      int recTypeMask = Int32.Parse(Request.QueryString["RecordTypes"]);
      foreach (ListItem chbItem in chblRecType.Items)
        if ((recTypeMask & Int32.Parse(chbItem.Value)) > 0)
          chbItem.Selected = true;
        else
          chbItem.Selected = false;
    }
    if (Request.QueryString["Sources"] != null)
      tbRecSource.Text = Request.QueryString["Sources"];
    if (Request.QueryString["FaultCodes"] != null)
      tbFaultCode.Text = Request.QueryString["FaultCodes"];
    if (Request.QueryString["TitleKeywords"] != null)
      tbTitleKeywords.Text = Request.QueryString["TitleKeywords"];
    if (Request.QueryString["MatchAllKeywords"] != null)
      chbMatchAllKeywords.Checked = bool.Parse(Request.QueryString["MatchAllKeywords"]);
    //Record severities
    if (Request.QueryString["Severities"] != null)
    {
      String strSeverities = Request.QueryString["Severities"];
      foreach (ListItem chbItem in chblFaultSeverities.Items)
        if (strSeverities.Contains(chbItem.Value))
          chbItem.Selected = true;
        else
          chbItem.Selected = false;
    }
    if (Request.QueryString["SubSeverities"] != null)
      tbSubSeverities.Text = Request.QueryString["SubSeverities"];
    if (Request.QueryString["VehicleNumbers"] != null)
      tbVehicleNumbers.Text = Request.QueryString["VehicleNumbers"];
    if (Request.QueryString["TrcSnpCodes"] != null)
      tbTrcSnpCodes.Text = Request.QueryString["TrcSnpCodes"];
  }

  /// <summary>
  /// Returns flags indicating selected types of records, based on selected checkboxes
  /// </summary>
  /// <returns>Flags converted to string (or empty string)</returns>
  private String GetRecordTypes()
  {
    int recTypeMask = 0;
    foreach (ListItem chbItem in chblRecType.Items)
      if (chbItem.Selected)
        recTypeMask += Int32.Parse(chbItem.Value);
    if (recTypeMask == 0)
      return String.Empty;
    else
      return recTypeMask.ToString();
  }

  /// <summary>
  /// Returns list of selecte fault severities based on selected check boxes.
  /// Empty string in case no severity is selected.
  /// </summary>
  /// <returns>List of severities</returns>
  private String GetFaultSeverities()
  {
    String severities = String.Empty;
    foreach (ListItem chbItem in chblFaultSeverities.Items)
      if (chbItem.Selected)
        severities += chbItem.Value + " ";
    return severities.Trim();
  }

  /// <summary>
  /// Obtains list of selected hierarchy item IDs in the form of string, separated by ;
  /// </summary>
  /// <returns>String with selected item IDs</returns>
  private String GetSelectedHierarchyItemIDs()
  {
    String strItemIDs = String.Empty;
    foreach (TreeNode hierarchyNode in tvHierarchy.CheckedNodes)
    { //Iterate over all checked nodes
      if (strItemIDs != String.Empty)
        strItemIDs += ";"; //Append separator
      strItemIDs += hierarchyNode.Value; //Append node id
      //Add IDs of all childe nodes
      foreach (TreeNode node in hierarchyNode.ChildNodes)
        strItemIDs = AddChildNodeID(node, strItemIDs);
    }
    //Return list of IDs
    return strItemIDs;
  }

  /// <summary>
  /// Add ID of this node and all child nodes to string list
  /// </summary>
  /// <param name="hierarchyNode">Node from TreeView</param>
  /// <param name="strItemIDs">List of Node IDs</param>
  /// <returns>Resulting list of node </returns>
  private String AddChildNodeID(TreeNode hierarchyNode, String strItemIDs)
  {
    strItemIDs += ";" + hierarchyNode.Value;
    //Call recursively for all child nodes
    foreach (TreeNode node in hierarchyNode.ChildNodes)
      strItemIDs = AddChildNodeID(node, strItemIDs);
    return strItemIDs;
  }

  /// <summary>
  /// Obtains list of selected hierarchy items value paths in the form of string, separated by ;
  /// </summary>
  /// <returns>String with selected items value paths</returns>
  private String GetSelectedHierarchyItemValuePaths()
  {
    String strItems = String.Empty;
    foreach (TreeNode item in tvHierarchy.CheckedNodes)
    {
      if (strItems != String.Empty)
        strItems += ";";
      strItems += item.ValuePath;
    }
    return strItems;
  }

  /// <summary>
  /// Checkes nodes in fleet hierarchy tree view
  /// according to list of selected hierarchy items value paths from query string
  /// </summary>
  /// <param name="selectedItems">List of selected hierarchy items value paths from query string (value paths to selected items)</param>
  private void CheckHierarchyNodes(String selectedItems)
  {
    //Check for empty
    if (selectedItems == null || selectedItems == String.Empty)
      return;

    //Split to item value paths
    String[] items = selectedItems.Split(new String[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
    //For each item, check appropriate node
    foreach (String item in items)
    {
      TreeNode node = tvHierarchy.FindNode(item);
      if (node != null)
      {
        node.Checked = true; //Check node
        //Expand the path to the node
        TreeNode parentNode = node;
        while((parentNode = parentNode.Parent) != null)
          parentNode.Expand();
      }
    }
  }
}

