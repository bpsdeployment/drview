﻿
// JScript File
//CONSTANTS
var maxRecsPerTab = 10;
var tabCaptionBase = "records ";
var invGPSIconURL = "http://maps.google.com/mapfiles/kml/pal3/icon37.png";
var selectedMarkerIconURL = "http://maps.google.com/mapfiles/kml/pal3/icon32.png";
var invGPSTex = "Invalid GPS data";
//CONSTANTS

var map = null; //Object representing the map
var invGPSMarker = null; //Marker which displays rcords without valid GPS data
var invGPSMarkerIcon = null; //Icon for above marker
var selectedMarkerIcon = null; //Icon for marker containing selected diagnostic record
var markers = null; //Array of marker objects
var markerManager = null; //Manager of markers displayed on the map
var markerElements = null; //Array of marker XML elements, which are displayed
var selectedRecIndex = -1; //Index of currently selected record

//Handles event raised after each request for page update completes
function pageLoaded(sender, args)
{
  try
  {
    var panelsUpdated = args.get_panelsUpdated();
    var panelsCreated = args.get_panelsCreated();
    //Scan all created update panels
    for (i=0; i < panelsCreated.length; i++)
      if (panelsCreated[i].id == "ctl00_contentPH_updPanMarkers")
      { //Update panel with map was created - create map and show markers
        loadMap(); 
        showMarkers(); 
        return;
      }
    //Scan all updated update panels for panel with markers XML
    for (i=0; i < panelsUpdated.length; i++)
      if (panelsUpdated[i].id == "ctl00_contentPH_updPanMarkers")
        showMarkers(); //Update panel with map was updated - show new markers
    //Scan all updated update panels for panel with diagnostic records
    for (i=0; i < panelsUpdated.length; i++)
      if (panelsUpdated[i].id == "ctl00_contentPH_updPanDiagRecs")
        showSelectedRecInfoTab(); //Show info window for marker with selected record
  }
  catch(err)
  {
    return;
  }
}

//Load the map in mapHolder rectangle
//Add necessary event handlers
function loadMap()
{
  //check for browser compatibility
  if (!GBrowserIsCompatible())
    return;

  //Create map object, add necessary controls
  map = new GMap2(document.getElementById('ctl00_contentPH_panMap'));
  map.addControl(new GZoomControl(), new GControlPosition(G_ANCHOR_TOP_LEFT, new GSize(10,10)));
  map.addControl(new GLargeMapControl(), new GControlPosition(G_ANCHOR_TOP_LEFT, new GSize(10,30)));
  map.addControl(new GMapTypeControl());
  map.setCenter(new GLatLng(0, 0), 8);
    
  //Parse XML document with global attributes included in defined DIV
  var xmlHolder = document.getElementById('markersXml');
  if (xmlHolder != null)
  {
    var xmlDocument = GXml.parse(xmlHolder.innerHTML);
    if (xmlDocument != null && xmlDocument.documentElement != null)
    {//Read global informations from document  
      if (xmlDocument.documentElement.getAttribute("maxmarkertabrecs") != "")
        maxRecsPerTab = parseInt(xmlDocument.documentElement.getAttribute("maxmarkertabrecs"));
      tabCaptionBase = xmlDocument.documentElement.getAttribute("markertabcaptionbase");
      invGPSIconURL = xmlDocument.documentElement.getAttribute("invalidgpsiconurl");
      invGPSTex = xmlDocument.documentElement.getAttribute("invalidgpstext");
      selectedMarkerIconURL = xmlDocument.documentElement.getAttribute("selectedmarkericonurl");
    }
  }

  //Create icon for invalid GPS marker
  invGPSMarkerIcon = new GIcon();
  invGPSMarkerIcon.image = invGPSIconURL;
  invGPSMarkerIcon.iconSize=new GSize(32,32);
  invGPSMarkerIcon.shadowSize=new GSize(56,32);
  invGPSMarkerIcon.iconAnchor=new GPoint(16,32);
  invGPSMarkerIcon.infoWindowAnchor=new GPoint(16,0);
  
  //Create icon for marker with selected record
  selectedMarkerIcon = new GIcon();
  selectedMarkerIcon.image = selectedMarkerIconURL;
  selectedMarkerIcon.iconSize=new GSize(32,32);
  selectedMarkerIcon.shadowSize=new GSize(56,32);
  selectedMarkerIcon.iconAnchor=new GPoint(16,32);
  selectedMarkerIcon.infoWindowAnchor=new GPoint(16,0);
   
  //Add event listner to map on click
  GEvent.addListener(map, "click", function(marker, point) {showMarkerInfoWindow(marker);});
  //Add event listener to map moveend event
  GEvent.addListener(map, "moveend", function() {placeInvGPSMarker();});
  //Add event listener to map zoomend event
  GEvent.addListener(map, "zoomend", function(oldLevel, newLevel) {placeInvGPSMarker();});
}

//show markers described in given XML document
function showMarkers()
{
  //check for browser compatibility
  if (!GBrowserIsCompatible())
    return;
  
  //Remove all existing markers from map
  //Remove markers defined in marker manager
  if (markerManager)
    markerManager.clearMarkers();
  markerManager = new MarkerManager(map);
  //Remove invalid GPS marker
  if (invGPSMarker)
    map.removeOverlay(invGPSMarker);
  invGPSMarker = null;
  //Parse XML markers data from XML included in defined DIV
  var xmlHolder = document.getElementById('markersXml');
  if (xmlHolder == null)
    return; //No xml for markers was specified
  var xmlDocument = GXml.parse(xmlHolder.innerHTML);
  if (xmlDocument == null || xmlDocument.documentElement == null)
    return; //Failed to parse content of XML
  //Read global informations from document  
  if (xmlDocument.documentElement.getAttribute("maxmarkertabrecs") != "")
    maxRecsPerTab = parseInt(xmlDocument.documentElement.getAttribute("maxmarkertabrecs"));
  tabCaptionBase = xmlDocument.documentElement.getAttribute("markertabcaptionbase");
  invGPSIconURL = xmlDocument.documentElement.getAttribute("invalidgpsiconurl");
  invGPSTex = xmlDocument.documentElement.getAttribute("invalidgpstext");
  
  //Extract array of marker elements from the XMl
  markerElements = xmlDocument.documentElement.getElementsByTagName("marker");
  
  //Declare array of markers representing diagnostic records
  markers = new Array(markerElements.length);
  //Declare bounds which will contain all the points on the map
  var mapBounds;
  
  //Create marker for each item in arrays
  for (var i = 0; i < markerElements.length; i++) 
  {
    var lat = parseFloat(markerElements[i].getAttribute("lat"));
    var lng = parseFloat(markerElements[i].getAttribute("lng"));
    var title = markerElements[i].getAttribute("title");
    var markerIndex = parseInt(markerElements[i].getAttribute("markerindex"));
    if (lat == 0 && lng == 0) 
    { //Invalid GPS coordinates, create special marker and add to the map directly
      invGPSMarker = new GMarker(map.getCenter(), { icon: invGPSMarkerIcon, title: title });      
      //Store index in markerElements array into marker
      invGPSMarker.ArrayIndex = markerIndex;
      //Add marker directly to the map
      map.addOverlay(invGPSMarker);      
      //Add new marker into the array
      markers[markerIndex] = invGPSMarker;
    }
    else 
    { //Valid GPS coordinates, create regular marker
      //Create position for marker
      var posn = new GLatLng(lat, lng);
      //Create marker
      var marker = new GMarker(posn, { title: title });
      //Add point to map bounds
      if (mapBounds)
        mapBounds.extend(posn);
      else 
        mapBounds = new GLatLngBounds(posn, posn);
      //Store index in markerElements array into marker
      marker.ArrayIndex = markerIndex;
      //Add new marker into the array
      markers[markerIndex] = marker;
      //Add marker into the marker manager
      markerManager.addMarker(marker, 0);    
    }
  }
  //Display markers on map
  markerManager.refresh();
  //Center map between the markers
  if (mapBounds)
  {
    var zoomLevel = map.getBoundsZoomLevel(mapBounds)-1;
    if (zoomLevel > 10)
      zoomLevel = 10;
    map.setCenter(mapBounds.getCenter(), zoomLevel);
  }
}

//Correct coordinates of marker for records with invalid GPS data
//Marker is displayed in bottom left (SW) corner of the map
function placeInvGPSMarker () 
{
  if (invGPSMarker)
  { //Marker is defined - correct coordinates
    var mapSW   = map.getBounds().getSouthWest();
    var mapSpan = map.getBounds().toSpan();
    //Compute position 
    var markerPoint = new GLatLng(mapSW.lat()+0.01*mapSpan.lat(), mapSW.lng()+0.5*mapSpan.lng());
    //Place marker
    invGPSMarker.setPoint(markerPoint);
  }
}

//Displays info window over diagnostic record marker
//Window contains description of events...
function showMarkerInfoWindow(marker)
{
  //Display info for diag rec marker, if clicked
  if (marker && (marker.ArrayIndex != null)) 
  {
    //Index in markerElements array
    var i = marker.ArrayIndex;
    //Create text representing GPS coordinates
    var gpsText;
    if (marker == invGPSMarker)
      gpsText = "<b>" + invGPSTex + "</b>"; //marker with invalid GPS data
    else
      gpsText = "<b>" + marker.getPoint().toString() + "</b>"; //valid GPS data
      
    //Obtain list of all recInfo elements defined for marker
    var recInfos = markerElements[i].getElementsByTagName("recinfo");
    //Declare temporary variables  
    var infoTabs = []; //Array of info tabs for marker
    var currText = "<table>"; //Current text for diagnostic records
    var recsInCurrTab = 0; //Number of records in currently created tab
    var selectedTab = 0; //Index of tab, which contains selected record
    //Iterate over all defined recInfos
    for (var j = 0; j < recInfos.length; j++)
    {
      if (recsInCurrTab >= maxRecsPerTab)
      { //Too many records, create new tab
        currText += "</table>";
        //Create tab with current text, add to tabs array
        infoTabs.push(new GInfoWindowTab(tabCaptionBase + infoTabs.length.toString(), gpsText + currText));
        recsInCurrTab = 0;
        currText = "<table>";
      }
      //Add text for current record
      var currRecIndx = recInfos[j].getAttribute("recordindex");
      if (selectedRecIndex == currRecIndx)
      { //Highlight currently selected record
        currText += "<tr style=\"background-color:#0055FF; font-weight:bold\">"; 
        //Store index of tab with selected record
        selectedTab = infoTabs.length;
      }
      else
        currText += "<tr>";
      currText += "<td>" + recInfos[j].getAttribute("starttime") + "</td><td>";
      if (selectedRecIndex != currRecIndx)
        currText += "<a href=\"javascript:__doPostBack('ctl00$contentPH$gvRecords','Select$" + currRecIndx.toString() + "')\">";
      currText += recInfos[j].getAttribute("title");
      if (selectedRecIndex != currRecIndx)
        currText += "</a>";
      currText += "</td></tr>";
      recsInCurrTab++;
    }
    currText += "</table>";
    //Create tab with current text, add to tabs array
    infoTabs.push(new GInfoWindowTab(tabCaptionBase + infoTabs.length.toString(), gpsText + currText));
    //Open info tabs window over the marker
    marker.openInfoWindowTabsHtml(infoTabs, {selectedTab: selectedTab});
  }
}

//Function finds marker with selected record 
//and calls showMarkerInfoWindow for this tab
function showSelectedRecInfoTab()
{
  try
  {
    //First close currently open info window
    map.closeInfoWindow();
    //Reset index of currently selected record
    selectedRecIndex = -1;
    if (markers == null)
      return; //No markers were created
    //obtain from hidden field index of selected record
    var field = document.getElementById('ctl00_contentPH_hfSelectedIndex');
    if (field == null || field.value == null)
      return;
    selectedRecIndex = parseInt(field.value);
    if (selectedRecIndex < 0)
      return;
    //find in array of marker elements one containing the selected record
    var markerIndex = -1;
    for (var i = 0; (i < markerElements.length) && (markerIndex < 0); i++) 
    { //iterate over all marker elements
      //Obtain list of all recInfo elements defined for marker
      var recInfos = markerElements[i].getElementsByTagName("recinfo");
      for (var j = 0; j < recInfos.length; j++)
      { //Iterate over all defined recInfos
        if (parseInt(recInfos[j].getAttribute("recordindex")) == selectedRecIndex)
        { //corresponding recor found, store marker index and break from the loop
          markerIndex = parseInt(markerElements[i].getAttribute("markerindex"));
          break;
        }
      }
    }
    if (markerIndex < 0 || markerIndex > markers.length)
      return; //no sensible corresponding marker was found
    //Select marker from the array
    var marker = markers[markerIndex];
    //Show info window with highlighted record
    showMarkerInfoWindow(marker)
  }
  catch(err)
  {
    return;
  }
}

//Handles the resizing of map "window"
function ResizeMapWindow()
{
  try
  {
    //Set height of map part of the "window"
    var globHeight = parseInt(document.getElementById('ctl00_contentPH_panMapGlobal').style.height);
    var headHeight = parseInt(document.getElementById('ctl00_contentPH_panMapHead').style.height);
    document.getElementById('ctl00_contentPH_panMap').style.height = (globHeight - headHeight) + "px";
  }
  catch(err)
  {}
}

//Notifyies GMap of resizing, when resize process is finished
function ResizeMap()
{
  try
  {
    //Set height of map part of the "window"
    var globHeight = parseInt(document.getElementById('ctl00_contentPH_panMapGlobal').style.height);
    var headHeight = parseInt(document.getElementById('ctl00_contentPH_panMapHead').style.height);
    document.getElementById('ctl00_contentPH_panMap').style.height = (globHeight - headHeight) + "px";
    //Notify google map
    map.checkResize();
    //Place invisible marker
    placeInvGPSMarker();
  }
  catch(err)
  {}
}

