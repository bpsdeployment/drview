using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DDDObjects;
using BLL;
using ZetaLibWeb;

public partial class Admin_DeleteRecords : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    { //Get number of records
      int recCount = GetRecCount();
      lRecCount.Text = recCount.ToString();
      //Check record count, enable/disable delete button
      if (recCount > 0)
        btDelete.Enabled = true;
      else
        btDelete.Enabled = false;
      //Display query parameters
      DisplayParameters();
    }
  }
  /// <summary>
  /// Handler for any errors in page not handled in code by catch blocks.
  /// It is actually page-level exception handler for all types of exceptions.
  /// </summary>
  private void Page_Error(object sender, EventArgs e)
  {
    Exception ex = Server.GetLastError();
    //Log exception
    //Obtain logger object
    TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
    //Log operation
    logger.LogText(1, "DeleteRecords", "Page error occured for user {0}", HttpContext.Current.User.Identity.Name);
    logger.LogException(2, "DeleteRecords", ex);
    //Store in session end redirect to error page
    Session["Exception"] = ex;
    Response.Redirect("~/Error.aspx");
  }

  protected void btCancel_Click(object sender, EventArgs e)
  { //User cancelled delete operation
    //Redirect back to query page with original parameters
    QueryString queryString = new QueryString(Page);
    queryString.BeforeUrl = "~/Query.aspx";
    Response.Redirect(queryString.All);
  }
  protected void btDelete_Click(object sender, EventArgs e)
  { //Run delete operation
    String infoText = String.Empty;
    try
    {
      //Log delete operation
      LogDeleteOperation();
      //Obtain maximum number of deleted records
      int maxRecords = Int32.Parse(lRecCount.Text);
      //Create BLL object
      DiagnosticRecordsBLL diagRecBLL = new DiagnosticRecordsBLL();
      diagRecBLL.DDDHelper = (DDDHelper)Application["DDDHelper"];
      diagRecBLL.Logger = (TDManagementForms.Logger)Application["Logger"];
      //Delete records by calling BLL method
      diagRecBLL.DeleteRecords(
        Request.QueryString["TUInstanceIDs"], 
        Request.QueryString["HierarchyItemIDs"],
        (Request.QueryString["StartTimeFrom"] != null) ? (DateTime?)Convert.ToDateTime(Request.QueryString["StartTimeFrom"]) : null,
        (Request.QueryString["StartTimeTo"] != null) ? (DateTime?)Convert.ToDateTime(Request.QueryString["StartTimeTo"]) : null,
        Request.QueryString["Sources"],
        Request.QueryString["VehicleNumbers"],
        Request.QueryString["FaultCodes"],
        Request.QueryString["TrcSnpCodes"],
        Request.QueryString["TitleKeywords"],
        (Request.QueryString["MatchAllKeywords"] != null) ? (Boolean)Convert.ToBoolean(Request.QueryString["MatchAllKeywords"]) : false,
        (Request.QueryString["RecordTypes"] != null) ? (int?)Convert.ToInt32(Request.QueryString["RecordTypes"]) : null,
        Request.QueryString["Severities"],
        Request.QueryString["SubSeverities"],
        maxRecords);
      infoText = "Records were deleted from databse";
    }
    catch (Exception ex)
    {
      infoText = "Failed to delete records: " + ex.Message;
    }
    finally
    { //Display panel with info text
      lInfoMessage.Text = infoText;
      panDeleteRecord.Visible = false;
      panInfoMessage.Visible = true;
      //Log result of the operation
      //Obtain logger object
      TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
      logger.LogText(3, "DeleteRecords", "User {0} result of delete operation: {1}", HttpContext.Current.User.Identity.Name, infoText);
    }
  }
  /// <summary>
  /// Method retrieves number of records to be deleted
  /// </summary>
  protected int GetRecCount()
  {
    try
    {
      //Create BLL object
      DiagnosticRecordsBLL diagRecBLL = new DiagnosticRecordsBLL();
      diagRecBLL.DDDHelper = (DDDHelper)Application["DDDHelper"];
      diagRecBLL.Logger = (TDManagementForms.Logger)Application["Logger"];
      //Retrieve count by calling BLL method
      String tmpString;
      return diagRecBLL.GetTotalRecords(
        Request.QueryString["TUInstanceIDs"], 
        Request.QueryString["HierarchyItemIDs"],
        (Request.QueryString["StartTimeFrom"] != null) ? (DateTime?)Convert.ToDateTime(Request.QueryString["StartTimeFrom"]) : null,
        (Request.QueryString["StartTimeTo"] != null) ? (DateTime?)Convert.ToDateTime(Request.QueryString["StartTimeTo"]) : null,
        Request.QueryString["Sources"],
        Request.QueryString["VehicleNumbers"],
        Request.QueryString["FaultCodes"],
        Request.QueryString["TrcSnpCodes"],
        Request.QueryString["TitleKeywords"],
        (Request.QueryString["MatchAllKeywords"] != null) ? (Boolean)Convert.ToBoolean(Request.QueryString["MatchAllKeywords"]) : false,
        (Request.QueryString["RecordTypes"] != null) ? (int?)Convert.ToInt32(Request.QueryString["RecordTypes"]) : null,
        Request.QueryString["Severities"],
        Request.QueryString["SubSeverities"],
        0,0,String.Empty, false, out tmpString);
    }
    catch (Exception ex)
    {
      throw new Exception("Failed to obtain number of records matching filter criteria", ex);
    }
  }
  /// <summary>
  /// Method displays query parameters
  /// </summary>
  protected void DisplayParameters()
  { //Display parameters in labels
    lTUInstanceIDs.Text =
      (Request.QueryString["TUInstanceIDs"] != null) ? Request.QueryString["TUInstanceIDs"] : "-";
    lHierarchyItems.Text =
      (Request.QueryString["HierarchyItemPaths"] != null) ? Request.QueryString["HierarchyItemPaths"] : "-";
    lStartTimeFrom.Text =
      (Request.QueryString["StartTimeFrom"] != null) ? Request.QueryString["StartTimeFrom"] : "-";
    lStartTimeTo.Text =
      (Request.QueryString["StartTimeTo"] != null) ? Request.QueryString["StartTimeTo"] : "-";
    lSources.Text =
      (Request.QueryString["Sources"] != null) ? Request.QueryString["Sources"] : "-";
    lVehicleNumbers.Text =
      (Request.QueryString["VehicleNumbers"] != null) ? Request.QueryString["VehicleNumbers"] : "-";
    lFaultCodes.Text =
      (Request.QueryString["FaultCodes"] != null) ? Request.QueryString["FaultCodes"] : "-";
    lTrcSnpCodes.Text =
      (Request.QueryString["TrcSnpCodes"] != null) ? Request.QueryString["TrcSnpCodes"] : "-";
    lTitleKeywords.Text =
      (Request.QueryString["TitleKeywords"] != null) ? Request.QueryString["TitleKeywords"] : "-";
    lMatchAllKeywords.Text =
      (Request.QueryString["MatchAllKeywords"] != null) ? Request.QueryString["MatchAllKeywords"] : "-";
    lRecordTypes.Text =
      (Request.QueryString["RecordTypes"] != null) ? Request.QueryString["RecordTypes"] : "-";
    lSeverities.Text =
      (Request.QueryString["Severities"] != null) ? Request.QueryString["Severities"] : "-";
    lSubSeverities.Text =
      (Request.QueryString["SubSeverities"] != null) ? Request.QueryString["SubSeverities"] : "-";
  }

  /// <summary>
  /// Log parameters of delete operation
  /// </summary>
  protected void LogDeleteOperation()
  {
    //Obtain logger object
    TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
    //Log the operation
    logger.LogText(3, "DeleteRecords", "User {0} deleting recods with parameters:", HttpContext.Current.User.Identity.Name);
    logger.LogText(4, "DeleteRecords", "TUInstanceIDs: {0}, HierarchyItemPaths: {1}, StartTimeFrom: {2}, StartTimeTo: {3}, Sources: {4}, VehicleNumbers: {5}",
      (Request.QueryString["TUInstanceIDs"] != null) ? Request.QueryString["TUInstanceIDs"] : "-",
      (Request.QueryString["HierarchyItemPaths"] != null) ? Request.QueryString["HierarchyItemPaths"] : "-",
      (Request.QueryString["StartTimeFrom"] != null) ? Request.QueryString["StartTimeFrom"] : "-",
      (Request.QueryString["StartTimeTo"] != null) ? Request.QueryString["StartTimeTo"] : "-",
      (Request.QueryString["Sources"] != null) ? Request.QueryString["Sources"] : "-",
      (Request.QueryString["VehicleNumbers"] != null) ? Request.QueryString["VehicleNumbers"] : "-");
    logger.LogText(4, "DeleteRecords", "FaultCodes: {0}, TrcSnpCodes: {1}, TitleKeywords: {2}, MatchAllKeywords: {3}, RecordTypes: {4}, Severities: {5}, SubSeverities: {6}, maxRecords: {7}",
      (Request.QueryString["FaultCodes"] != null) ? Request.QueryString["FaultCodes"] : "-",
      (Request.QueryString["TrcSnpCodes"] != null) ? Request.QueryString["TrcSnpCodes"] : "-",
      (Request.QueryString["TitleKeywords"] != null) ? Request.QueryString["TitleKeywords"] : "-",
      (Request.QueryString["MatchAllKeywords"] != null) ? Request.QueryString["MatchAllKeywords"] : "-",
      (Request.QueryString["RecordTypes"] != null) ? Request.QueryString["RecordTypes"] : "-",
      (Request.QueryString["Severities"] != null) ? Request.QueryString["Severities"] : "-",
      (Request.QueryString["SubSeverities"] != null) ? Request.QueryString["SubSeverities"] : "-",
      Int32.Parse(lRecCount.Text));
  }
}
