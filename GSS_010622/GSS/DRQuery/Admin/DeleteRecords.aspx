<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="DeleteRecords.aspx.cs"
  Inherits="Admin_DeleteRecords" Title="Delete records from database" %>

<asp:Content ID="cntDelRecords" ContentPlaceHolderID="contentPH" runat="Server">
  <div style="margin: 5px">
      <h4>
        Delete records from database</h4>
    <asp:Panel ID="panDeleteRecord" runat="server" DefaultButton="btCancel" Width="100%">
      <h5>
        Do you want to delete
        <asp:Label ID="lRecCount" runat="server" />
        records from database?</h5>
      <br />
      <table>
        <tr>
          <td>
            <asp:Button ID="btDelete" runat="server" Text="Delete" OnClick="btDelete_Click" />
          </td>
          <td>
            <asp:Button ID="btCancel" runat="server" Text="Cancel" OnClick="btCancel_Click" />
          </td>
        </tr>
      </table>
    </asp:Panel>
    <asp:Panel ID="panInfoMessage" runat="server" DefaultButton="btQuery" Width="100%"
      Visible="false">
      <h5><asp:Label ID="lInfoMessage" runat="server" /></h5>
      <br />
      <asp:Button ID="btQuery" runat="server" Text="Go to query page" OnClick="btCancel_Click" />
    </asp:Panel>
    <br />
    <h4>
      Parameters
    </h4>
    <table cellpadding="3" cellspacing="0" border="1" bordercolor="black">
      <tr>
        <td>
          Telediagnostic unit IDs</td>
        <td>
          <asp:Label ID="lTUInstanceIDs" runat="server" /></td>
      </tr>
      <tr>
        <td>
          Hierarchy items</td>
        <td>
          <asp:Label ID="lHierarchyItems" runat="server" /></td>
      </tr>
      <tr>
        <td>
          Time from</td>
        <td>
          <asp:Label ID="lStartTimeFrom" runat="server" /></td>
      </tr>
      <tr>
        <td>
          Time to</td>
        <td>
          <asp:Label ID="lStartTimeTo" runat="server" /></td>
      </tr>
      <tr>
        <td>
          Title keywords</td>
        <td>
          <asp:Label ID="lTitleKeywords" runat="server" /></td>
      </tr>
      <tr>
        <td>
          Match all keywords</td>
        <td>
          <asp:Label ID="lMatchAllKeywords" runat="server" /></td>
      </tr>
      <tr>
        <td>
          Fault codes</td>
        <td>
          <asp:Label ID="lFaultCodes" runat="server" /></td>
      </tr>
      <tr>
        <td>
          Vehicle numbers</td>
        <td>
          <asp:Label ID="lVehicleNumbers" runat="server" /></td>
      </tr>
      <tr>
        <td>
          Record sources</td>
        <td>
          <asp:Label ID="lSources" runat="server" /></td>
      </tr>
      <tr>
        <td>
          Record type mask</td>
        <td>
          <asp:Label ID="lRecordTypes" runat="server" /></td>
      </tr>
      <tr>
        <td>
          Fault severities</td>
        <td>
          <asp:Label ID="lSeverities" runat="server" /></td>
      </tr>
      <tr>
        <td>
          Fault subseverities</td>
        <td>
          <asp:Label ID="lSubSeverities" runat="server" /></td>
      </tr>
      <tr>
        <td>
          Trace or Snap codes</td>
        <td>
          <asp:Label ID="lTrcSnpCodes" runat="server" /></td>
      </tr>
    </table>
  </div>
</asp:Content>
