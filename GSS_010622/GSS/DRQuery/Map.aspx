<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Map.aspx.cs" Inherits="DataView_Map" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">
<head id="mapHead" runat="server">
  <title>Map of diagnostic records</title>
  <style type="text/css">
      v\:* {
        behavior:url(#default#VML);
      }
    </style>
  <link href="~/Default.css" rel="stylesheet" type="text/css" />
  <link href="~/calendar/calendar.css" rel="stylesheet" type="text/css" />
  <style type="text/css">
   <!--
   .style7 {font-size: 16px; font-weight: bold; color: #FFFFFF;}
   .style8 {color: #FFFFFF}
   .style9 {font-family: "Courier New", Courier, monospace}
   -->
  </style>
</head>
<body style="margin: 0px" onload="loadMapAndMarkers()" onunload="unloadMap()">
  <form id="frmMap" runat="server">
    <!--AJAX script manager-->
    <asp:ScriptManager ID="ScriptManager" runat="server" />
    <!--Include scripts for maps-->

    <script src="Map/markermanager.js" type="text/javascript"></script>

    <script src="Map/dragzoom.js" type="text/javascript"></script>

    <script src="Map/diagRecMap.js" type="text/javascript"></script>

    <div style="margin: 5px">
      <!--Panel with the records map window-->
      <asp:Panel ID="panMap" runat="server" BorderStyle="Solid" BorderWidth="2px" BorderColor="Black"
        Height="600px" Width="600px" Style="margin: 0px;" BackColor="LightBlue">
      </asp:Panel>
      <!--AJAX control managing the resizibility of map panel-->
      <ajaxToolkit:ResizableControlExtender ID="resizeMap" runat="server" TargetControlID="panMap"
        HandleCssClass="handleImage" MinimumHeight="400" MinimumWidth="400" MaximumHeight="1200"
        MaximumWidth="1200" OnClientResizing="ResizeMap" HandleOffsetX="16" HandleOffsetY="16" />
      <br />
      <button id="btSetQueryGPSParams" type="button" onclick="setQueryGPSParams()" title="Use current map borders as restriction for GPS position in query filter">
        Set query position restriction</button>
    </div>
  </form>

  <script language="javascript">
    //Store function pointers in document - they can be called easilly from different window
    document.showMarkers = showMarkers;
    document.selectRecord = showSelectedRecInfoTab;
    document.setMapBounds = setMapBounds;
  </script>

</body>
</html>
