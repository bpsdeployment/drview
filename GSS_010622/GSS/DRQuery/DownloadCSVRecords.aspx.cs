using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Xml;
using System.Collections.Generic;
using DDDObjects;
using DRQueries;
using BLL;
using ZetaLibWeb;


public partial class DownloadCSVRecords : System.Web.UI.Page
{
  /// <summary>
  /// Called when page is loaded to be sent to the client
  /// </summary>
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      //Clear response stream
      Response.Clear();
      //Obtain logger object
      TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
      try
      {
        //Log operation
        LogDownloadOperation();
        //Load records from DB and serialize to output stream
        using (SqlConnection dbConn =
          new SqlConnection(ConfigurationManager.ConnectionStrings["CDDBConnectionString"].ConnectionString))
        { //Create and open DB connection
          dbConn.Open();
          using (SqlCommand dbCmd = new SqlCommand(GetDRExportQuery(), dbConn))
          { //Create DB Command
            dbCmd.CommandType = CommandType.Text;
            dbCmd.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["dbQueryTimeoutSec"]);
            using (SqlDataReader reader = dbCmd.ExecuteReader())
            { //Obtain reader with result set
              //Clear response stream
              Response.Clear();
              //Set response properties
              Response.ContentType = "application/octet-stream";
              Response.AddHeader("Content-Disposition", "attachment; filename=" + "Records.csv");
              //Obtain DDD Helper and custom configuration objects
              DDDHelper dddHelper = (DDDHelper)Application["DDDHelper"];
              CustomConfiguration customConfiguration = null;
              if (Application["CustomConfiguration"] != null)
                customConfiguration = (CustomConfiguration)Application["CustomConfiguration"];
              //Create fake data table for diagnostic records
              DiagnosticData.DiagnosticRecordsDataTable table = new DiagnosticData.DiagnosticRecordsDataTable();
              //If configured, add custom columns to data table
              int origColCount = table.Columns.Count;
              if (customConfiguration != null)
                foreach (DataTableColumn custCol in customConfiguration.DataTableColumns)
                  table.Columns.Add(custCol.Name, typeof(String));
              //Obtain indexes of columns configured for export and create header text
              List<int> columnIndexes = new List<int>();
              String headerText = String.Empty;
              foreach (GridColumn column in customConfiguration.GridColumns)
              {
                headerText += "\"" + column.HeaderText + "\";";
                if (column.GetType() == typeof(BoundGridColumn))
                  columnIndexes.Add(table.Columns[((BoundGridColumn)column).DataField].Ordinal);
                else if (column.GetType() == typeof(HypelinkGridColumn))
                  columnIndexes.Add(table.Columns[((HypelinkGridColumn)column).DataTextField].Ordinal);
              }
              headerText += "\n";
              //Declare row for diagnostic record
              DiagnosticData.DiagnosticRecordsRow row;
              //Write header into output stream
              Response.Write(headerText);
              //Write to response stream record by record
              int writtenCount = 0;
              String textRow;
              while (reader.Read())
              { //Read records one by one from DB
                //Create new row
                row = table.NewDiagnosticRecordsRow();
                //Fill row attributes by data read from DB
                FillRowFromReader(row, reader);
                //Fill in addtitiona attributes attributes
                DiagnosticRecordsBLL.FillDiagRecRowAttributes(row, dddHelper, customConfiguration, origColCount);
                //Create text row from columns specified in custom configuration
                textRow = String.Empty;
                foreach (int columnIndex in columnIndexes)
                { //Iterate over all columns specified in custom configuration
                  if (row.IsNull(columnIndex))
                    textRow += ";";
                  else
                  {
                    if (row[columnIndex].GetType() == typeof(DateTime))
                      textRow += "\"" + ((DateTime)(row[columnIndex])).ToString("yyyy-MM-dd HH:mm:ss") + "\";";
                    else
                      textRow += "\"" + row[columnIndex].ToString() + "\";";
                  }
                }
                //Append end of line
                textRow += "\n";
                //Write text row to the stream
                Response.Write(textRow);
                //Check for number of written records, flush the response if necessary
                writtenCount++;
                if (writtenCount % 200 == 0)
                  Response.Flush(); //Flush response each 200 records
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to send diagnostic data as CSV file", ex);
      }
      //Close response
      Response.Flush();
      Response.End();
      //Log end of operation
      logger.LogText(3, "DownloadCSVRecords", "Download for user {0} finished", HttpContext.Current.User.Identity.Name);
    }
  }

  /// <summary>
  /// Handler for any errors in page not handled in code by catch blocks.
  /// It is actually page-level exception handler for all types of exceptions.
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  private void Page_Error(object sender, EventArgs e)
  {
    Exception ex = Server.GetLastError();
    //Log exception
    //Obtain logger object
    TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
    //Log operation
    logger.LogText(1, "DownloadCSVRecords", "Page error occured for user {0}", HttpContext.Current.User.Identity.Name);
    logger.LogException(2, "DownloadCSVRecords", ex);
    //Store in session end redirect to error page
    Session["Exception"] = ex;
    Response.Redirect("~/Error.aspx");
  }

  /// <summary>
  /// Log parameters of download operation
  /// </summary>
  protected void LogDownloadOperation()
  {
    //Obtain logger object
    TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
    //Log the operation
    logger.LogText(3, "DownloadCSVRecords", "User {0} downloading recods with parameters:", HttpContext.Current.User.Identity.Name);
    logger.LogText(4, "DownloadCSVRecords", "TUInstanceIDs: {0}, HierarchyItemPaths: {1}, StartTimeFrom: {2}, StartTimeTo: {3}, Sources: {4}, VehicleNumbers: {5}",
      (Request.QueryString["TUInstanceIDs"] != null) ? Request.QueryString["TUInstanceIDs"] : "-",
      (Request.QueryString["HierarchyItemPaths"] != null) ? Request.QueryString["HierarchyItemPaths"] : "-",
      (Request.QueryString["StartTimeFrom"] != null) ? Request.QueryString["StartTimeFrom"] : "-",
      (Request.QueryString["StartTimeTo"] != null) ? Request.QueryString["StartTimeTo"] : "-",
      (Request.QueryString["Sources"] != null) ? Request.QueryString["Sources"] : "-",
      (Request.QueryString["VehicleNumbers"] != null) ? Request.QueryString["VehicleNumbers"] : "-");
    logger.LogText(4, "DownloadCSVRecords", "FaultCodes: {0}, TrcSnpCodes: {1}, TitleKeywords: {2}, MatchAllKeywords: {3}, RecordTypes: {4}, Severities: {5}, SubSeverities: {6}, MaxRecords: {7}",
      (Request.QueryString["FaultCodes"] != null) ? Request.QueryString["FaultCodes"] : "-",
      (Request.QueryString["TrcSnpCodes"] != null) ? Request.QueryString["TrcSnpCodes"] : "-",
      (Request.QueryString["TitleKeywords"] != null) ? Request.QueryString["TitleKeywords"] : "-",
      (Request.QueryString["MatchAllKeywords"] != null) ? Request.QueryString["MatchAllKeywords"] : "-",
      (Request.QueryString["RecordTypes"] != null) ? Request.QueryString["RecordTypes"] : "-",
      (Request.QueryString["Severities"] != null) ? Request.QueryString["Severities"] : "-",
      (Request.QueryString["SubSeverities"] != null) ? Request.QueryString["SubSeverities"] : "-",
      (Request.QueryString["MaxRecords"] != null) ? Request.QueryString["MaxRecords"] : "-"
    );
  }

  /// <summary>
  /// Returns query which selects diagnostic records from DB in format suitable 
  /// for web application.
  /// Query parameters are extracted from URL string
  /// </summary>
  private String GetDRExportQuery()
  {
    //Obtain custom configuration
    CustomConfiguration customConfiguration = null;
    if (Application["CustomConfiguration"] != null)
       customConfiguration = (CustomConfiguration)Application["CustomConfiguration"];
    String queryText = DRQueryHelper.GetQuery_RecordsPaging(
      Request.QueryString["TUInstanceIDs"], 
      Request.QueryString["HierarchyItemIDs"],
      (Request.QueryString["StartTimeFrom"] != null) ? (DateTime?)Convert.ToDateTime(Request.QueryString["StartTimeFrom"]) : null,
      (Request.QueryString["StartTimeTo"] != null) ? (DateTime?)Convert.ToDateTime(Request.QueryString["StartTimeTo"]) : null,
      Request.QueryString["Sources"],
      Request.QueryString["VehicleNumbers"],
      Request.QueryString["FaultCodes"],
      Request.QueryString["TrcSnpCodes"],
      Request.QueryString["TitleKeywords"],
      (Request.QueryString["MatchAllKeywords"] != null) ? (Boolean)Convert.ToBoolean(Request.QueryString["MatchAllKeywords"]) : false,
      (Request.QueryString["RecordTypes"] != null) ? (int?)Convert.ToInt32(Request.QueryString["RecordTypes"]) : null,
      Request.QueryString["Severities"],
      Request.QueryString["SubSeverities"],
      0,
     (Request.QueryString["MaxRecords"] != null) ? Convert.ToInt32(Request.QueryString["MaxRecords"]) : 0,
      "StartTime",
      eSortOrder.Descending);
    return queryText;
  }

  /// <summary>
  /// Fills compulsory row attributes using data from DB
  /// </summary>
  /// <param name="row">Row to fill</param>
  /// <param name="reader">Reader containing data from DB</param>
  private void FillRowFromReader(DiagnosticData.DiagnosticRecordsRow row, SqlDataReader reader)
  {
    row.TUInstanceID = reader.GetGuid(0);
    row.RecordInstID = reader.GetInt64(1);
    row.DDDVersionID = reader.GetGuid(2);
    row.RecordDefID = reader.GetInt32(3);
    row.StartTime = reader.GetDateTime(4);
    if (!reader.IsDBNull(5))
      row.EndTime = reader.GetDateTime(5);
    if (!reader.IsDBNull(6))
      row.LastModified = reader.GetDateTime(6);
    if (!reader.IsDBNull(7))
      row.Source = reader.GetString(7);
    if (!reader.IsDBNull(8))
      row.HierarchyItemID = reader.GetInt32(8);
    if (!reader.IsDBNull(9))
      row.ImportTime = reader.GetDateTime(9);
    if (!reader.IsDBNull(10))
      row.AcknowledgeTime = reader.GetDateTime(10);
    if (!reader.IsDBNull(11))
      row.VehicleNumber = reader.GetInt16(11);
    if (!reader.IsDBNull(12))
      row.DeviceCode = reader.GetInt16(12);
    if (!reader.IsDBNull(13))
      row.BigEndianData = reader.GetBoolean(13);
    if (!reader.IsDBNull(14))
      row.BinaryData = reader.GetSqlBinary(14).Value;
  }
}
