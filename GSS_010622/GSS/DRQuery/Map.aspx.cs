using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class DataView_Map : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    //Include JScripts 
    IncludeMapScripts();
  }

  protected void IncludeMapScripts()
  {
    //Obtain client script manager and register scripts for Google map api support
    ClientScriptManager scriptMan = Page.ClientScript;
    if (!scriptMan.IsClientScriptIncludeRegistered("GMap"))
      scriptMan.RegisterClientScriptInclude("GMap", "http://maps.google.com/maps?file=api&v=2&key=" + ConfigurationManager.AppSettings["GoogleApiKey"]);
  }
}
