using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Collections;
using System.Xml;
using BLL;
using DDDObjects;
using ZetaLibWeb;
using System.Drawing;

public partial class _Default : System.Web.UI.Page 
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      //Initialize record sorting order (if necessary)
      if (Session["SortExpression"] == null)
        Session["SortExpression"] = "StartTime";
      //Store sort order
      if (Session["SortOrder"] == null)
        Session["SortOrder"] = 1;
      //Determine from query string prameter, if map shall be shown
      if (Session["bShowMap"] == null)
        Session["bShowMap"] = false; //Default is no
      if (Request.QueryString["ShowMap"] != null)
        Session["bShowMap"] = Boolean.Parse(Request.QueryString["ShowMap"]);
      if (Application["CustomConfiguration"] != null)
        ConfigureRecordsGrid((CustomConfiguration)Application["CustomConfiguration"]);
      //Set total record count to null - it will be selected from DB
      Session["TotalRecCount"] = null;
    }
    //Include scripts for map
    IncludeMapScripts();
  }

  /// <summary>
  /// Handler for any errors in page not handled in code by catch blocks.
  /// It is actually page-level exception handler for all types of exceptions.
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  private void Page_Error(object sender, EventArgs e)
  {
    Exception ex = Server.GetLastError();
    //Log exception
    //Obtain logger object
    TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
    //Log operation
    logger.LogText(1, "GridView", "Page error occured for user {0}", HttpContext.Current.User.Identity.Name);
    logger.LogException(2, "GridView", ex);
    //Store in session end redirect to error page
    Session["Exception"] = ex;
    Response.Redirect("~/Error.aspx");
  }

  protected void RecordsDataSource_ObjectCreated(object sender, ObjectDataSourceEventArgs e)
  { //Set object properties when created
    DiagnosticRecordsBLL diagRecBLL = (DiagnosticRecordsBLL)e.ObjectInstance;
    //Store DDD Helper
    diagRecBLL.DDDHelper = (DDDHelper)Application["DDDHelper"];
    //Store custom configuration
    if (Application["CustomConfiguration"] != null)
      diagRecBLL.CustomConfiguration = (CustomConfiguration)Application["CustomConfiguration"];
    //Store logger
    TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
    diagRecBLL.Logger = logger;
  }
  protected void btQuery_Click(object sender, EventArgs e)
  {
    QueryString queryString = new QueryString(Page);
    queryString.BeforeUrl = "Query.aspx";
    Response.Redirect(queryString.All);
  }
  protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
  {
    gvRecords.PageSize = Int32.Parse(ddlPageSize.SelectedValue);
    //gvRecords.DataBind();
  }
  protected void gvRecords_DataBound(object sender, EventArgs e)
  {
    int recCount = 0;
    if (Session["TotalRecCount"] != null)
      recCount = (int)Session["TotalRecCount"];
    else
      recCount = gvRecords.PageSize * gvRecords.PageCount;
    lbRecCount.Text = recCount.ToString();
    int minRec = (gvRecords.PageSize * gvRecords.PageIndex) + 1;
    int maxRec = gvRecords.PageSize * (gvRecords.PageIndex + 1);
    minRec = Math.Min(recCount, minRec);
    maxRec = Math.Min(recCount, maxRec);
    lbRecRange.Text = minRec.ToString() + " - " + maxRec.ToString();
  }
  protected void gvRecords_PageIndexChanged(object sender, EventArgs e)
  {
    int recCount = 0;
    if (Session["TotalRecCount"] != null)
      recCount = (int)Session["TotalRecCount"];
    else
      recCount = gvRecords.PageSize * gvRecords.PageCount;
    int minRec = (gvRecords.PageSize * gvRecords.PageIndex) + 1;
    int maxRec = gvRecords.PageSize * (gvRecords.PageIndex + 1);
    minRec = Math.Min(recCount, minRec);
    maxRec = Math.Min(recCount, maxRec);
    lbRecRange.Text = minRec.ToString() + " - " + maxRec.ToString();
  }
  protected void VariablesDataSource_ObjectCreated(object sender, ObjectDataSourceEventArgs e)
  {
    DiagnosticRecordsBLL diagRecBLL = (DiagnosticRecordsBLL)e.ObjectInstance;
    diagRecBLL.DDDHelper = (DDDHelper)Application["DDDHelper"];
  }

  /// <summary>
  /// Add custom configured columns to grid with diagnostic records
  /// </summary>
  /// <param name="gridConfig">Configuration object</param>
  protected void ConfigureRecordsGrid(CustomConfiguration gridConfig)
  {
    foreach (GridColumn custCol in gridConfig.GridColumns)
    { //Iterate over all custom configured grid columns
      if (custCol.GetType() == typeof(BoundGridColumn))
      { //Data bound column
        BoundGridColumn boundCol = (BoundGridColumn)custCol;
        BoundField field = new BoundField();
        field.HeaderText = boundCol.HeaderText;
        field.DataField = boundCol.DataField;
        field.SortExpression = boundCol.SortExpression;
        field.DataFormatString = boundCol.DataFormatString;
        field.HtmlEncode = boundCol.HtmlEncode;
        gvRecords.Columns.Add(field);
      }
      else if (custCol.GetType() == typeof(HypelinkGridColumn))
      { //Hyperlink custom column
        HypelinkGridColumn hypCol = (HypelinkGridColumn)custCol;
        HyperLinkField field = new HyperLinkField();
        field.HeaderText = hypCol.HeaderText;
        field.DataTextField = hypCol.DataTextField;
        field.DataTextFormatString = hypCol.DataTextFormatString;
        field.DataNavigateUrlFields = hypCol.DataNavigateUrlFields;
        field.DataNavigateUrlFormatString = hypCol.DataNavigateUrlFormatString;
        field.Target = hypCol.Target;
        gvRecords.Columns.Add(field);
      }
      else
        continue; //Unknown column type
    }
  }
  /// <summary>
  /// Event raised after the select operation of records data source has completed.
  /// Event is also raised after SelectCount operation.
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  protected void RecordsDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
  {
    if ((e.Exception != null) && !e.ExceptionHandled)
    { //Exception was thrown by BLL object - rethrow the exception
      throw e.Exception;
    }
    else if (e.OutputParameters.Contains("RecMarkersXML") && e.OutputParameters["RecMarkersXML"] != null)
    { // Extract value of output parameter with markers XML and set to hidden field
      xmlMarkers.DocumentContent = (String)e.OutputParameters["RecMarkersXML"];
      //Store XML also in session stae for later use
      Session["RecMarkersXML"] = (String)e.OutputParameters["RecMarkersXML"];
    }
    else if (e.ReturnValue.GetType() == typeof(int))
      Session["TotalRecCount"] = (int)e.ReturnValue;  //Select count operation finished, store record count
  }
  //Raised when index of selected record has changed in diagnostic records grid
  protected void gvRecords_SelectedIndexChanged(object sender, EventArgs e)
  {
    //Store index of selected record in hidden field
    hfSelectedIndex.Value = gvRecords.SelectedIndex.ToString();
  }
  protected void btShowMap_Click(object sender, ImageClickEventArgs e)
  {
    //Store markers XML into document from session state
    if (Session["RecMarkersXML"] != null)
      xmlMarkers.DocumentContent = (String)Session["RecMarkersXML"];
    //Store map state in session state
    Session["bShowMap"] = true;
    IncludeMapScripts();
  }
  protected void imgCloseMap_Click(object sender, ImageClickEventArgs e)
  {
    //Store map state in session state
    Session["bShowMap"] = false;
    IncludeMapScripts();
  }
  protected void IncludeMapScripts()
  {
    if ((bool)Session["bShowMap"])
    { //Map shall be shown
      //Obtain client script manager and register scripts for Google map api support
      ClientScriptManager scriptMan = Page.ClientScript;
      if (!scriptMan.IsClientScriptIncludeRegistered("GMap"))
        scriptMan.RegisterClientScriptInclude("GMap", "http://maps.google.com/maps?file=api&v=2&key=" + ConfigurationManager.AppSettings["GoogleApiKey"]);
      //Register include with new MarkerManager from GMaps utility project
      if (!scriptMan.IsClientScriptIncludeRegistered("GMapUtil"))
        scriptMan.RegisterClientScriptInclude("GMapUtil", "http://gmaps-utility-library.googlecode.com/svn/trunk/markermanager/release/src/markermanager.js");
      //Register include with new zoom control for map
      if (!scriptMan.IsClientScriptIncludeRegistered("GZoom"))
        scriptMan.RegisterClientScriptInclude("GZoom", "Map/gzoom.js");
      //Register include with script for map control
      if (!scriptMan.IsClientScriptIncludeRegistered("MapScript"))
        scriptMan.RegisterClientScriptInclude("MapScript", "Map/mapScripts.js");
      panMapGlobal.Visible = true;
    }
    else
      panMapGlobal.Visible = false; //Hide panel with map
  }
  /// <summary>
  /// Reverse sorting according to time
  /// </summary>
  protected void gvRecords_Sorting(object sender, GridViewSortEventArgs e)
  {
    //Event fired when sorting of record grid is requiered
    //Determine sort order
    if (Session["SortOrder"] == null)
      Session["SortOrder"] = 0;
    if ((String)Session["SortExpression"] == e.SortExpression)
    {
      if ((int)Session["SortOrder"] == 0)
        Session["SortOrder"] = 1;
      else
        Session["SortOrder"] = 0;
    }
    else
      Session["SortOrder"] = 0;
    //Store sort expression
    Session["SortExpression"] = e.SortExpression;
    //Cancel the event
    e.Cancel = true;
    //Bind to data source with correct sorting parameter
    gvRecords.DataBind();
  }
  /// <summary>
  /// Operation raised whenever user is selecting records
  /// </summary>
  protected void RecordsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
  {
    //Log select operation
    if (!e.ExecutingSelectCount)
    { //Record select operation
      //Obtain logger object
      TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
      //Log the operation
      String strParams = String.Empty;
      foreach (DictionaryEntry param in e.InputParameters)
        if (param.Value != null)
          strParams += String.Format("{0}: {1}; ", param.Key, param.Value);
      logger.LogText(3, "GridView", "User {0} selecting recods with parameters:", HttpContext.Current.User.Identity.Name);
      logger.LogText(4, "GridView", strParams);

      if (Session["TotalRecCount"] != null)
      { //Total row count has already been retrieved, no need to do it again
        e.Arguments.TotalRowCount = (int)Session["TotalRecCount"];
        e.Arguments.RetrieveTotalRowCount = false;
      }
    }
  }
}
