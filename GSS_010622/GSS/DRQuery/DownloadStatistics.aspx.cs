using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Xml;
using System.Collections.Generic;
using DDDObjects;
using DRQueries;
using BLL;
using ZetaLibWeb;


public partial class DownloadStatistics : System.Web.UI.Page
{
  /// <summary>
  /// Called when page is loaded to be sent to the client
  /// </summary>
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      //Clear response stream
      Response.Clear();
      //Obtain logger object
      TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
      try
      {
        //Log operation
        LogDownloadOperation();
        //Load records from DB and serialize to output stream
        using (SqlConnection dbConn =
          new SqlConnection(ConfigurationManager.ConnectionStrings["CDDBConnectionString"].ConnectionString))
        { //Create and open DB connection
          dbConn.Open();
          using (SqlCommand dbCmd = new SqlCommand(GetStatisticsExportQuery(), dbConn))
          { //Create DB Command
            dbCmd.CommandType = CommandType.Text;
            dbCmd.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["dbQueryTimeoutSec"]);
            using (SqlDataReader reader = dbCmd.ExecuteReader())
            { //Obtain reader with result set
              //Clear response stream
              Response.Clear();
              //Set response properties
              Response.ContentType = "application/octet-stream";
              Response.AddHeader("Content-Disposition", "attachment; filename=" + "Statistics.csv");
              //Obtain DDD Helper and custom configuration objects
              DDDHelper dddHelper = (DDDHelper)Application["DDDHelper"];
              //Create fake data table for record statistics
              Statistics.RecordsStatisticsDataTable table = new Statistics.RecordsStatisticsDataTable();
              //Write table header to the output stream
              String headerText = "\"Train\";\"Vehicle\";\"Vehicle Number\";\"Record Source\";\"Fault Code\";\"Record Title\";\"Count\";\"Time of first\";\"Time of last\";\"Severity\";\"Sub Severity\";\n";
              //Declare row for statistic record
              Statistics.RecordsStatisticsRow row;
              //Write header into output stream
              Response.Write(headerText);
              //Write to response stream record by record
              int writtenCount = 0;
              String textRow;
              while (reader.Read())
              { //Read records one by one from DB
                //Create new row
                row = table.NewRecordsStatisticsRow();
                //Fill row attributes by data read from DB
                FillRowFromReader(row, reader);
                //Fill in addtitiona attributes
                StatisticsBLL.FillStatisticsRowAttributes(row, dddHelper);
                //Create text row from exported columns
                textRow = String.Empty;
                if (!row.IsTrainNameNull())
                  textRow += "\"" + row.TrainName + "\";";
                else
                  textRow += "\"\";";
                if (!row.IsVehicleNameNull())
                  textRow += "\"" + row.VehicleName + "\";";
                else
                  textRow += "\"\";";
                if (!row.IsVehicleNumberNull())
                  textRow += "\"" + row.VehicleNumber + "\";";
                else
                  textRow += "\"\";";
                if (!row.IsSourceNull())
                  textRow += "\"" + row.Source + "\";";
                else
                  textRow += "\"\";";
                if (!row.IsFaultCodeNull())
                  textRow += "\"" + row.FaultCode + "\";";
                else
                  textRow += "\"\";";
                if (!row.IsRecTitleNull())
                  textRow += "\"" + row.RecTitle + "\";";
                else
                  textRow += "\"\";";
                if (!row.IsRecCountNull())
                  textRow += "\"" + row.RecCount + "\";";
                else
                  textRow += "\"\";";
                if (!row.IsFirstTimeNull())
                  textRow += "\"" + row.FirstTime + "\";";
                else
                  textRow += "\"\";";
                if (!row.IsLastTimeNull())
                  textRow += "\"" + row.LastTime + "\";";
                else
                  textRow += "\"\";";
                if (!row.IsFaultSeverityNull())
                  textRow += "\"" + row.FaultSeverity + "\";";
                else
                  textRow += "\"\";";
                if (!row.IsFaultSubSeverityNull())
                  textRow += "\"" + row.FaultSubSeverity + "\";";
                else
                  textRow += "\"\";";
                //Append end of line
                textRow += "\n";
                //Write text row to the stream
                Response.Write(textRow);
                //Check for number of written records, flush the response if necessary
                writtenCount++;
                if (writtenCount % 200 == 0)
                  Response.Flush(); //Flush response each 200 records
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to send record statistics data as CSV file", ex);
      }
      //Close response
      Response.Flush();
      Response.End();
      //Log end of operation
      logger.LogText(3, "DownloadStatistics", "Download for user {0} finished", HttpContext.Current.User.Identity.Name);
    }
  }

  /// <summary>
  /// Handler for any errors in page not handled in code by catch blocks.
  /// It is actually page-level exception handler for all types of exceptions.
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  private void Page_Error(object sender, EventArgs e)
  {
    Exception ex = Server.GetLastError();
    //Log exception
    //Obtain logger object
    TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
    //Log operation
    logger.LogText(1, "DownloadStatistics", "Page error occured for user {0}", HttpContext.Current.User.Identity.Name);
    logger.LogException(2, "DownloadStatistics", ex);
    //Store in session end redirect to error page
    Session["Exception"] = ex;
    Response.Redirect("~/Error.aspx");
  }

  /// <summary>
  /// Log parameters of download operation
  /// </summary>
  protected void LogDownloadOperation()
  {
    //Obtain logger object
    TDManagementForms.Logger logger = (TDManagementForms.Logger)Application["Logger"];
    //Log the operation
    logger.LogText(3, "DownloadStatistics", "User {0} downloading statistics with parameters:", HttpContext.Current.User.Identity.Name);
    logger.LogText(4, "DownloadStatistics", "TUInstanceIDs: {0}, HierarchyItemPaths: {1}, StartTimeFrom: {2}, StartTimeTo: {3}, Sources: {4}, VehicleNumbers: {5}",
      (Request.QueryString["TUInstanceIDs"] != null) ? Request.QueryString["TUInstanceIDs"] : "-",
      (Request.QueryString["HierarchyItemPaths"] != null) ? Request.QueryString["HierarchyItemPaths"] : "-",
      (Request.QueryString["StartTimeFrom"] != null) ? Request.QueryString["StartTimeFrom"] : "-",
      (Request.QueryString["StartTimeTo"] != null) ? Request.QueryString["StartTimeTo"] : "-",
      (Request.QueryString["Sources"] != null) ? Request.QueryString["Sources"] : "-",
      (Request.QueryString["VehicleNumbers"] != null) ? Request.QueryString["VehicleNumbers"] : "-");
    logger.LogText(4, "DownloadStatistics", "FaultCodes: {0}, TrcSnpCodes: {1}, TitleKeywords: {2}, MatchAllKeywords: {3}, RecordTypes: {4}, Severities: {5}, SubSeverities: {6}",
      (Request.QueryString["FaultCodes"] != null) ? Request.QueryString["FaultCodes"] : "-",
      (Request.QueryString["TrcSnpCodes"] != null) ? Request.QueryString["TrcSnpCodes"] : "-",
      (Request.QueryString["TitleKeywords"] != null) ? Request.QueryString["TitleKeywords"] : "-",
      (Request.QueryString["MatchAllKeywords"] != null) ? Request.QueryString["MatchAllKeywords"] : "-",
      (Request.QueryString["RecordTypes"] != null) ? Request.QueryString["RecordTypes"] : "-",
      (Request.QueryString["Severities"] != null) ? Request.QueryString["Severities"] : "-",
      (Request.QueryString["SubSeverities"] != null) ? Request.QueryString["SubSeverities"] : "-"
    );
  }

  /// <summary>
  /// Returns query which selects records statistics from DB in format suitable 
  /// for web application.
  /// Query parameters are extracted from URL string
  /// </summary>
  private String GetStatisticsExportQuery()
  {
    String queryText = DRQueryHelper.GetQuery_WebStatistics(
      Request.QueryString["TUInstanceIDs"], 
      Request.QueryString["HierarchyItemIDs"],
      (Request.QueryString["StartTimeFrom"] != null) ? (DateTime?)Convert.ToDateTime(Request.QueryString["StartTimeFrom"]) : null,
      (Request.QueryString["StartTimeTo"] != null) ? (DateTime?)Convert.ToDateTime(Request.QueryString["StartTimeTo"]) : null,
      Request.QueryString["Sources"],
      Request.QueryString["VehicleNumbers"],
      Request.QueryString["FaultCodes"],
      Request.QueryString["TrcSnpCodes"],
      Request.QueryString["TitleKeywords"],
      (Request.QueryString["MatchAllKeywords"] != null) ? (Boolean)Convert.ToBoolean(Request.QueryString["MatchAllKeywords"]) : false,
      (Request.QueryString["RecordTypes"] != null) ? (int?)Convert.ToInt32(Request.QueryString["RecordTypes"]) : null,
      Request.QueryString["Severities"],
      Request.QueryString["SubSeverities"]);
    return queryText;
  }

  /// <summary>
  /// Fills compulsory row attributes using data from DB
  /// </summary>
  /// <param name="row">Row to fill</param>
  /// <param name="reader">Reader containing data from DB</param>
  private void FillRowFromReader(Statistics.RecordsStatisticsRow row, SqlDataReader reader)
  {
    row.TUInstanceID = reader.GetGuid(0);
    row.DDDVersionID = reader.GetGuid(1);
    row.RecordDefID = reader.GetInt32(2);
    if (!reader.IsDBNull(3))
      row.VehicleNumber = reader.GetInt16(3);
    if (!reader.IsDBNull(4))
      row.Source = reader.GetString(4);
    if (!reader.IsDBNull(5))
      row.HierarchyItemID = reader.GetInt32(5);
    if (!reader.IsDBNull(6))
      row.FirstTime = reader.GetDateTime(6);
    if (!reader.IsDBNull(7))
      row.LastTime = reader.GetDateTime(7);
    if (!reader.IsDBNull(8))
      row.RecCount = reader.GetInt32(8);
    if (!reader.IsDBNull(9))
      row.FirstID = reader.GetInt64(9);
    if (!reader.IsDBNull(10))
      row.FirstID = reader.GetInt64(10);
  }
}
