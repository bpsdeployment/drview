using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DRMailerTasksTableAdapters;
using DDDObjects;

namespace BLL
{
  /// <summary>
  /// Provides access to table with definitions of DRMailer tasks
  /// </summary>
  [System.ComponentModel.DataObject]
  public class DRMailerTaskDefsBLL
  {
    #region Private members
    private DRMailerTaskDefsTableAdapter taskDefsAdapter = null;
    private DRMailerTaskHistoryTableAdapter taskHistAdapter = null;
    private DDDHelper dddHelper = null;
    #endregion //Private members

    #region Public properties
    public DRMailerTaskDefsTableAdapter Adapter
    {
      get
      {
        if (taskDefsAdapter == null)
          taskDefsAdapter = new DRMailerTaskDefsTableAdapter();
        return taskDefsAdapter;
      }
    }

    public DRMailerTaskHistoryTableAdapter HistoryAdapter
    {
      get
      {
        if (taskHistAdapter == null)
          taskHistAdapter = new DRMailerTaskHistoryTableAdapter();
        return taskHistAdapter;
      }
    }

    public DDDHelper DDDHelper
    {
      set { dddHelper = value; }
    }
    #endregion //Public properties

    #region Public methods
    /// <summary>
    /// Returns data table with set of defined DRMailer tasks
    /// </summary>
    /// <returns></returns>
    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public DRMailerTasks.DRMailerTaskDefsDataTable GetTaskDefinitions()
    {
      DRMailerTasks.DRMailerTaskDefsDataTable table;
      //Obtain table with values loaded from DB
      table = Adapter.GetData(null);
      //Convert all times from UTC to local
      foreach (DRMailerTasks.DRMailerTaskDefsRow row in table)
      {
        if (!row.IsLastSuccRunNull())
          row.LastSuccRun = dddHelper.UTCToUserTime(row.LastSuccRun);
        row.StartTime = dddHelper.UTCToUserTime(row.StartTime);
      }
      //Return table
      return table;
    }

    /// <summary>
    /// Returns data row with definition of one DRMailer task
    /// </summary>
    /// <returns></returns>
    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
    public DRMailerTasks.DRMailerTaskDefsRow GetTaskDefinition(String TaskName)
    {
      DRMailerTasks.DRMailerTaskDefsDataTable table;
      DRMailerTasks.DRMailerTaskDefsRow row;
      //Obtain table with task definition
      table = Adapter.GetData(TaskName);
      //Obtain first row
      if (table.Count > 0)
        row = (DRMailerTasks.DRMailerTaskDefsRow)table.Rows[0];
      else
        return null;
      //Convert times to local
      if (!row.IsLastSuccRunNull())
        row.LastSuccRun = dddHelper.UTCToUserTime(row.LastSuccRun);
      row.StartTime = dddHelper.UTCToUserTime(row.StartTime);
      //Return row
      return row;
    }

    /// <summary>
    /// Inserts definition of new DRMailer task
    /// </summary>
    /// <returns></returns>
    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert)]
    public void InsertNewTask(String TaskName, int? Period, DateTime? StartTime, int? MaxDelay, String Recipients, String Subject, 
                              String TUInstances, int? Interval, int? Delay, int? RecTypeMask, String Sources, 
                              String FaultCodes, String TitleKeywords, bool? MatchAllKeywords, int? MaxRecords,
                              int? MaxFileLengthBytes, bool? SendEmptyFile, String FileNameFormat, String Comment, bool? Enabled)
    {
      //Convert start time to UTC
      if (StartTime.HasValue)
        StartTime = (DateTime?)dddHelper.UserTimeToUTC(StartTime.Value);
      //Insert
      Adapter.Insert(TaskName, Period, StartTime, MaxDelay, Recipients, Subject, TUInstances, Interval, Delay, 
                     RecTypeMask, Sources, FaultCodes, TitleKeywords, MatchAllKeywords, MaxRecords, 
                     MaxFileLengthBytes, SendEmptyFile, FileNameFormat, Comment, Enabled);
    }

    /// <summary>
    /// Updates definition of one DRMailer task
    /// </summary>
    /// <returns></returns>
    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update)]
    public void UpdateTask(String TaskName, String NewTaskName, int? Period, DateTime? StartTime, int? MaxDelay, String Recipients, String Subject,
                           String TUInstances, int? Interval, int? Delay, int? RecTypeMask, String Sources,
                           String FaultCodes, String TitleKeywords, bool? MatchAllKeywords, int? MaxRecords,
                           int? MaxFileLengthBytes, bool? SendEmptyFile, String FileNameFormat, String Comment, bool? Enabled)
    {
      //Convert start time to UTC
      if (StartTime.HasValue)
        StartTime = (DateTime?)dddHelper.UserTimeToUTC(StartTime.Value);
      //Update
      Adapter.Update(TaskName, NewTaskName, Period, StartTime, MaxDelay, Recipients, Subject, TUInstances, Interval, Delay,
                     RecTypeMask, Sources, FaultCodes, TitleKeywords, MatchAllKeywords, MaxRecords,
                     MaxFileLengthBytes, SendEmptyFile, FileNameFormat, Comment, Enabled);
    }

    /// <summary>
    /// Deletes definition of specified task
    /// </summary>
    /// <returns></returns>
    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete)]
    public void DeleteTask(String TaskName)
    {
      Adapter.Delete(TaskName);
    }

    /// <summary>
    /// Returns data table with history of one DRMailer task
    /// </summary>
    /// <returns></returns>
    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select)]
    public DRMailerTasks.DRMailerTaskHistoryDataTable GetTaskHistory(String TaskName, int? MaxRows)
    {
      DRMailerTasks.DRMailerTaskHistoryDataTable table;
      //Obtain table from table adapter
      table = HistoryAdapter.GetData(TaskName, MaxRows); 
      //Convert all times to local time
      foreach (DRMailerTasks.DRMailerTaskHistoryRow row in table)
      {
        row.EndTime = dddHelper.UTCToUserTime(row.EndTime);
        row.ExecTime = dddHelper.UTCToUserTime(row.ExecTime);
        row.StartTime = dddHelper.UTCToUserTime(row.StartTime);
      }
      //Return table 
      return table;
    }
    #endregion //Public methods
  }
}
