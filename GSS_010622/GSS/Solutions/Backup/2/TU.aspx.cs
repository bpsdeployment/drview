using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;

public partial class TU : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
      HttpWebRequest newReq = (HttpWebRequest)WebRequest.Create("http://svoboda-ntb/TDManagement/Default.aspx");
      newReq.Method = Request.HttpMethod;
      newReq.ContentType = Request.ContentType;
      newReq.ContentLength = Request.ContentLength;
      if (Request.ContentLength > 0)
      {
        Byte[] inpContent = new byte[Request.InputStream.Length];
        Request.InputStream.Read(inpContent, 0, (int)(Request.InputStream.Length));
        Stream reqStream = newReq.GetRequestStream();
        reqStream.Write(inpContent, 0, inpContent.Length);
        reqStream.Close();
      }
      HttpWebResponse newResp = (HttpWebResponse)newReq.GetResponse();
      Response.ClearContent();
      Stream respStream = newResp.GetResponseStream();
      Byte[] outContent = new byte[1000];
      int noRead;
      int offset = 0;
      while ((noRead = respStream.Read(outContent, 0, outContent.Length)) > 0)
      {
        Response.OutputStream.Write(outContent, 0, noRead);
        offset += noRead;
      }
      respStream.Close();
      Response.Flush();
      //End the response - response contains the file only
      Response.End();
    }
}
