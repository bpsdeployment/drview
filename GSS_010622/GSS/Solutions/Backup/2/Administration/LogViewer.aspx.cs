using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ZetaLibWeb;
using System.IO;
using System.Text;

public partial class Administration_LogViewer : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {
      using (FileStream stream = new FileStream(ddlLogFile.SelectedValue, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
      {
        using (StreamReader reader = new StreamReader(stream))
        {
          //Determine maximum requested size
          long maxSize = 0;
          if (tbMaxSize.Text != String.Empty)
            maxSize = (long)(UInt32.Parse(tbMaxSize.Text) * 1024);
          if (maxSize > 0 && reader.BaseStream.Length > maxSize)
          { //Need to seek, from the end
            reader.BaseStream.Seek(-1 * maxSize, SeekOrigin.End);
            reader.ReadLine();
          }

          litLog.Text = "<pre>";
          litLog.Text += reader.ReadToEnd();
          litLog.Text += "</pre>";
        }
      }
    }
    catch (Exception ex)
    {
      litLog.Text = String.Format("<b>Failed to show log file \"{0}\"</b><br/>", ddlLogFile.SelectedValue);
      Exception tmpEx = ex;
      while (tmpEx != null)
      {
        litLog.Text += tmpEx.Message + "<br/>";
        tmpEx = tmpEx.InnerException;
      }
    }
  }

}
