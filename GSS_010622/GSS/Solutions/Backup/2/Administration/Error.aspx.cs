using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Administration_Error : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
      //Display error information stored in session state
      Exception ex = (Exception)Session["Exception"];
      Exception innerEx = ex;
      if (ex != null)
        Session.Remove("Exception");
      while (innerEx != null)
      {
        if (innerEx.GetType() != typeof(HttpUnhandledException))
          blErrors.Items.Add(innerEx.Message);
        innerEx = innerEx.InnerException;
      }
    }
}
