using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DDDObjects;

public partial class Administration_DRStats : System.Web.UI.Page
{
  /// <summary>
  /// Handler for any errors in page not handled in code by catch blocks.
  /// It is actually page-level exception handler for all types of exceptions.
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  private void Page_Error(object sender, EventArgs e)
  {
    Exception ex = Server.GetLastError();
    //Store in session end redirect to error page
    Session["Exception"] = ex;
    Response.Redirect("~/Administration/Error.aspx");
  }
  
  protected void Page_Load(object sender, EventArgs e)
  {
    //Decode query string
    String query = Server.UrlDecode(this.ClientQueryString);
    //Check if query string is valid
    if (query != "" &&  query.StartsWith("TUInstanceID="))
    {//Set session parameters
      DateTime timeTo = DateTime.UtcNow;
      DateTime timeFrom = DateTime.UtcNow.AddDays(-1);
      Session["TimeTo"] = timeTo.ToString("yyyy-MM-dd HH:mm:ss.fff");
      Session["TimeFrom"] = timeFrom.ToString("yyyy-MM-dd HH:mm:ss.fff");
      Session["TUInstanceID"] = query.Substring(13);
    }
    else if (!IsPostBack)
    { //Remove old session states
      Session.Remove("TimeTo");
      Session.Remove("TimeFrom");
      Session.Remove("TUInstanceID");
    }
  }

  protected void btSelect_Click(object sender, EventArgs e)
  {
    DateTime timeTo = DateTime.UtcNow;
    DateTime timeFrom = DateTime.UtcNow.AddDays(Convert.ToDouble(ddlTimeRange.SelectedValue) * -1);
    Session["TimeTo"] = timeTo.ToString("yyyy-MM-dd HH:mm:ss.fff");
    Session["TimeFrom"] = timeFrom.ToString("yyyy-MM-dd HH:mm:ss.fff");
    Session["TUInstanceID"] = ddlTUInstances.SelectedValue;
  }

  protected void gvRecStats_DataBound(object sender, EventArgs e)
  {
    if ((Session["TimeTo"] != null) && (Session["TimeFrom"] != null) && (Session["TUInstanceID"] != null))
    {
      //Obtain session and application state variables
      DateTime timeTo = DateTime.Parse((String)Session["TimeTo"]);
      DateTime timeFrom = DateTime.Parse((String)Session["TimeFrom"]);
      ListItem selItem = ddlTUInstances.Items.FindByValue((String)Session["TUInstanceID"]);
      DDDHelper dddHelper = (DDDHelper)Application["DDDHelper"];
      //Convert times to local client time 
      timeTo = dddHelper.UTCToUserTime(timeTo);
      timeFrom = dddHelper.UTCToUserTime(timeFrom);
      if (selItem == null)
        return;
      ddlTUInstances.SelectedIndex = ddlTUInstances.Items.IndexOf(selItem);
      labelTimes.Text = selItem.Text + " (" + timeFrom.ToString("yyyy-MM-dd HH:mm") + " - " + timeTo.ToString("yyyy-MM-dd HH:mm") + ")";
      labelTimes.Visible = true;
    }
    else
      labelTimes.Visible = false;
  }
  protected void gvRecStats_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    DDDHelper dddHelper = (DDDHelper)Application["DDDHelper"];
    //Convert times to client local time
    if (e.Row.Cells.Count < 5)
      return;
    DateTime time;
    if (DateTime.TryParse(e.Row.Cells[3].Text, out time))
      e.Row.Cells[3].Text = dddHelper.UTCToUserTime(time).ToString("yyyy-MM-dd HH:mm:ss");
    if (DateTime.TryParse(e.Row.Cells[4].Text, out time))
      e.Row.Cells[4].Text = dddHelper.UTCToUserTime(time).ToString("yyyy-MM-dd HH:mm:ss");
  }
  protected void dsDRStats_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
  {
    if (e != null && e.Command != null && Session["QueryTimeout"] != null)
      e.Command.CommandTimeout = (int)Session["QueryTimeout"];
  }
}
