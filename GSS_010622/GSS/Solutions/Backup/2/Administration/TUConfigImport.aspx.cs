using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Schema;
using System.IO;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

/// <summary>
/// Class representing the page for import of new TU Configuration
/// </summary>
public partial class Administration_TUConfigImport : System.Web.UI.Page
{
  private bool xmlDocumentValid;

  protected void Page_Load(object sender, EventArgs e)
  {

  }

  /// <summary>
  /// Event handler for btUpload.
  /// Button used for upload of TU Configuration files.
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  protected void btUpload_Click(object sender, EventArgs e)
  {//Uploads TU configuration files to server
    String path = (String)Session["SessionDir"];
    lbImportMessages.Items.Clear();
    try
    {
      DeleteOldFiles(path); //delete leftover files
      UploadFiles(path); //upload files to target directory
      btInsertToDB.Enabled = true;
    }
    catch (Exception ex)
    { //Error occured during import - display error message
      lbImportMessages.Items.Add(ex.Message);
      lbError.Text = Resources.ConfigImport.msgUploadError + ex.Message;
      lbError.Visible = true;
      btInsertToDB.Enabled = false;
    }
  }

  /// <summary>
  /// Uploads TU Configuration files specified by page FileUpload components from the client to 
  /// specified server directory.
  /// </summary>
  /// <param name="path">Target path for uploaded files</param>
  protected void UploadFiles(String path)
  { //Uploads files specified by page contronls into specified path
    String fileName;
    //Uploading DDD file
    fileName = Resources.ConfigImport.DDDFileName;
    if (fileUploadDDD.HasFile)
    { //DDD file is specified
      lbImportMessages.Items.Add(Resources.ConfigImport.msgUploadingFile + fileUploadDDD.FileName);
      if (fileUploadDDD.FileName.ToLower() != fileName.ToLower())
        throw new Exception(Resources.ConfigImport.msgInvalidFileName + fileName); //Wrong file name
      fileUploadDDD.SaveAs(path + fileUploadDDD.FileName); //Upload file
      String xsdPath = ConfigurationManager.AppSettings["XSDFilesDir"];
      ValidateXMLFile(path + fileUploadDDD.FileName, xsdPath + Resources.ConfigImport.DDDXSDFileName); //Validate file
      lbImportMessages.Items.Add(Resources.ConfigImport.msgFileUploadSucc);
    }
    else //Error - DDD file is mandatory
      throw new Exception(Resources.ConfigImport.msgDDDFileMissing);

    //Uploading tasks file
    fileName = Resources.ConfigImport.TaskFileName;
    if (fileUploadTask.HasFile)
    {
      lbImportMessages.Items.Add(Resources.ConfigImport.msgUploadingFile + fileUploadTask.FileName);
      if (fileUploadTask.FileName != fileName)
        throw new Exception(Resources.ConfigImport.msgInvalidFileName + fileName); //Wrong file name
      fileUploadTask.SaveAs(path + fileUploadTask.FileName); //Upload file
      lbImportMessages.Items.Add(Resources.ConfigImport.msgFileUploadSucc);
    }
    else //Warning Task file is not mandatory
      lbImportMessages.Items.Add(Resources.ConfigImport.msgFileMissingWarning + fileName);

    //Uploading libraries file
    fileName = Resources.ConfigImport.BinaryFileName;
    if (fileUploadBinaryLibraries.HasFile)
    {
      lbImportMessages.Items.Add(Resources.ConfigImport.msgUploadingFile + fileUploadBinaryLibraries.FileName);
      if (fileUploadBinaryLibraries.FileName != fileName)
        throw new Exception(Resources.ConfigImport.msgInvalidFileName + fileName); //Wrong file name
      fileUploadBinaryLibraries.SaveAs(path + fileUploadBinaryLibraries.FileName); //Upload file
      lbImportMessages.Items.Add(Resources.ConfigImport.msgFileUploadSucc);
    }
    else //Warning libraries file is not mandatory
      lbImportMessages.Items.Add(Resources.ConfigImport.msgFileMissingWarning + fileName);

    //Uploading source code file
    fileName = Resources.ConfigImport.SourceFileName;
    if (fileUploadSourceCodes.HasFile)
    {
      lbImportMessages.Items.Add(Resources.ConfigImport.msgUploadingFile + fileUploadSourceCodes.FileName);
      if (fileUploadSourceCodes.FileName != fileName)
        throw new Exception(Resources.ConfigImport.msgInvalidFileName + fileName); //Wrong file name
      fileUploadSourceCodes.SaveAs(path + fileUploadSourceCodes.FileName); //Upload file
      lbImportMessages.Items.Add(Resources.ConfigImport.msgFileUploadSucc);
    }
    else //Warning source code file is not mandatory
      lbImportMessages.Items.Add(Resources.ConfigImport.msgFileMissingWarning + fileName);
  }

  /// <summary>
  /// Deletes previously uploaded TU Configuration files from specified directory.
  /// </summary>
  /// <param name="path">Path to the upload directory</param>
  protected void DeleteOldFiles(String path)
  { //Delete any leftover uploaded files from target directory
    try
    {
      if (File.Exists(path + Resources.ConfigImport.DDDFileName))
        File.Delete(path + Resources.ConfigImport.DDDFileName);
      if (File.Exists(path + Resources.ConfigImport.TaskFileName))
        File.Delete(path + Resources.ConfigImport.TaskFileName);
      if (File.Exists(path + Resources.ConfigImport.BinaryFileName))
        File.Delete(path + Resources.ConfigImport.BinaryFileName);
      if (File.Exists(path + Resources.ConfigImport.SourceFileName))
        File.Delete(path + Resources.ConfigImport.SourceFileName);
    }
    catch (Exception)
    { }
  }

  /// <summary>
  /// Validates specified XML file against specified XSD file.
  /// Writes any warnings or errors into the lbImportMessages list box
  /// </summary>
  /// <param name="xmlFileName">Full name of the XML file to validate</param>
  /// <param name="xsdFileName">Full name of the XSD file</param>
  protected void ValidateXMLFile(String xmlFileName, String xsdFileName)
  { //Validates specified XML file against the schema
    xmlDocumentValid = true;
    FileInfo xmlFileInfo = new FileInfo(xmlFileName);
    lbImportMessages.Items.Add(Resources.ConfigImport.msgValidatingFile + xmlFileInfo.Name);
    XmlReaderSettings readerSettings = new XmlReaderSettings();
    readerSettings.Schemas.Add("", xsdFileName); //Add schema to XML reader
    readerSettings.ValidationType = ValidationType.Schema; //Set validation during read
    readerSettings.ValidationEventHandler += new ValidationEventHandler(xmlValidationEventHandler); //Set handler for validation errors

    XmlReader reader = XmlReader.Create(xmlFileName, readerSettings);
    while (reader.Read()) { } //Read xml file and validate
    reader.Close(); //Close the reader
    if (xmlDocumentValid) //check validity flag
      lbImportMessages.Items.Add(Resources.ConfigImport.msgFileValid);
    else
      throw new Exception(Resources.ConfigImport.msgFileInvalid);
    return;
  }

  /// <summary>
  /// Event handler for XML validation .
  /// Events are raised for each error or warning that occures during the validation.
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  void xmlValidationEventHandler(object sender, ValidationEventArgs e)
  { //Handles error calls during XML file validation
    if (e.Severity == XmlSeverityType.Warning)
    {
      lbImportMessages.Items.Add("Warning: " + e.Message);
    }
    else if (e.Severity == XmlSeverityType.Error)
    {
      lbImportMessages.Items.Add("Error: " + e.Message);
      xmlDocumentValid = false;
    }
  }

  /// <summary>
  /// Inserts uploaded files into the database using the ImportTUConfig procedure
  /// Event handler for button btInsertToDB
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  protected void btInsertToDB_Click(object sender, EventArgs e)
  { 
    Guid VersionID;
    btInsertToDB.Enabled = false;
    try
    {
      lbImportMessages.Items.Add(Resources.ConfigImport.msgDBImportStart);
      //Import TU COnfig files into DB
      VersionID = RunImportProcedure();
      //Insert Version info document into DB
      InsertDDDVerInfo(VersionID);
      //Log import result
      lbImportMessages.Items.Add(Resources.ConfigImport.msgDBImportSucc);
      lbImportMessages.Items.Add(Resources.ConfigImport.msgNewDDDID + VersionID.ToString());
      btSaveVersionInfo.Enabled = true;
    }
    catch (Exception ex)
    {
      lbImportMessages.Items.Add(ex.Message);
      lbImportMessages.Items.Add(Resources.ConfigImport.msgDBImportFail);
      lbError.Text = Resources.ConfigImport.msgUploadError + ex.Message;
      lbError.Visible = true;
    }
    finally
    {
      DeleteOldFiles((String)Session["SessionDir"]);
    }
  }

  /// <summary>
  /// Runs DB stored procedure for TU Config import.
  /// Procedure takes TU config files and MD5 signatures as parameters
  /// </summary>
  /// <returns>Returns VersionID of new TU Config</returns>
  protected Guid RunImportProcedure()
  {
    SqlCommand insertCommand = new SqlCommand();
    SqlParameter procParameter;
    String path = (String)Session["SessionDir"];
    byte[] fileSignature;
    Guid VersionID;

    //Get session DB Connection
    insertCommand.Connection = (SqlConnection)Session["DBConnection"];
    insertCommand.CommandText = "ImportTUConfig";
    insertCommand.CommandType = CommandType.StoredProcedure;
    insertCommand.CommandTimeout = (int)Session["QueryTimeout"];
    //Set command parameters
    procParameter = insertCommand.Parameters.Add("@DDDFileName", SqlDbType.NVarChar);
    procParameter.Value = Resources.ConfigImport.DDDFileName;
    procParameter = insertCommand.Parameters.Add("@DDDFile", SqlDbType.Image);
    procParameter.Value = GetFileAsBuffer(path, Resources.ConfigImport.DDDFileName, out fileSignature);
    procParameter = insertCommand.Parameters.Add("@DDDFileXml", SqlDbType.Xml);
    procParameter.Value = GetFileAsXmlString(path, Resources.ConfigImport.DDDFileName);
    procParameter = insertCommand.Parameters.Add("@DDDSignature", SqlDbType.Binary, 16);
    procParameter.Value = fileSignature;

    if (File.Exists(path + Resources.ConfigImport.TaskFileName))
    {
      procParameter = insertCommand.Parameters.Add("@TaskFileName", SqlDbType.NVarChar);
      procParameter.Value = Resources.ConfigImport.TaskFileName;
      procParameter = insertCommand.Parameters.Add("@TaskFile", SqlDbType.Image);
      procParameter.Value = GetFileAsBuffer(path, Resources.ConfigImport.TaskFileName, out fileSignature);
      procParameter = insertCommand.Parameters.Add("@TaskSignature", SqlDbType.Binary, 16);
      procParameter.Value = fileSignature;
    }

    if (File.Exists(path + Resources.ConfigImport.BinaryFileName))
    {
      procParameter = insertCommand.Parameters.Add("@LibraryFileName", SqlDbType.NVarChar);
      procParameter.Value = Resources.ConfigImport.BinaryFileName;
      procParameter = insertCommand.Parameters.Add("@LibraryFile", SqlDbType.Image);
      procParameter.Value = GetFileAsBuffer(path, Resources.ConfigImport.BinaryFileName, out fileSignature);
      procParameter = insertCommand.Parameters.Add("@LibrarySignature", SqlDbType.Binary, 16);
      procParameter.Value = fileSignature;
    }

    if (File.Exists(path + Resources.ConfigImport.SourceFileName))
    {
      procParameter = insertCommand.Parameters.Add("@SourceFileName", SqlDbType.NVarChar);
      procParameter.Value = Resources.ConfigImport.SourceFileName;
      procParameter = insertCommand.Parameters.Add("@SourceFile", SqlDbType.Image);
      procParameter.Value = GetFileAsBuffer(path, Resources.ConfigImport.SourceFileName, out fileSignature);
      procParameter = insertCommand.Parameters.Add("@SourceSignature", SqlDbType.Binary, 16);
      procParameter.Value = fileSignature;
    }

    procParameter = insertCommand.Parameters.Add("@DDDVersionID", SqlDbType.UniqueIdentifier);
    procParameter.Direction = ParameterDirection.Output;

    XmlReader verInfoReader;
    XmlDocument verInfoDocument;
  
    //Execute the command and recieve DDDVersionInfo xml document back
    verInfoReader = insertCommand.ExecuteXmlReader();
    //Store new VersionID
    VersionID = (Guid)procParameter.Value;
    //Create the XML document with version info
    verInfoDocument = new XmlDocument();
    verInfoDocument.Load(verInfoReader);
    //Write the document to session directory
    XmlTextWriter verInfoWriter = new XmlTextWriter(path + Resources.ConfigImport.DDDVerInfFileName, System.Text.Encoding.UTF8);
    verInfoWriter.Formatting = Formatting.Indented;
    verInfoWriter.WriteStartDocument();
    verInfoDocument.WriteTo(verInfoWriter);
    verInfoWriter.Close();
    return VersionID;
  }

  /// <summary>
  /// Inserts DDD version information document to DB.
  /// Document is stored in session directory.
  /// </summary>
  /// <param name="VersionID">Identifier of DDD version</param>
  protected void InsertDDDVerInfo(Guid VersionID)
  {
    SqlCommand insertCommand = new SqlCommand();
    SqlParameter procParameter;
    String path = (String)Session["SessionDir"];
    byte[] fileSignature;

    //Get session DB Connection
    insertCommand.Connection = (SqlConnection)Session["DBConnection"];
    insertCommand.CommandType = CommandType.StoredProcedure;
    insertCommand.CommandText = "ImportDDDRelatedFile";
    insertCommand.CommandTimeout = (int)Session["QueryTimeout"];
    //Set command parameters
    procParameter = insertCommand.Parameters.Add("@VersionID", SqlDbType.UniqueIdentifier);
    procParameter.Value = VersionID;
    procParameter = insertCommand.Parameters.Add("@FileName", SqlDbType.NVarChar, 50);
    procParameter.Value = Resources.ConfigImport.DDDVerInfFileName;
    procParameter = insertCommand.Parameters.Add("@FileContent", SqlDbType.Image);
    procParameter.Value = GetFileAsBuffer(path, Resources.ConfigImport.DDDVerInfFileName, out fileSignature);
    insertCommand.ExecuteNonQuery();
  }

  /// <summary>
  /// Loads the file from disk into memory buffer and computes its MD5 signature
  /// </summary>
  /// <param name="path">Path to the file location</param>
  /// <param name="fileName">Name of the file</param>
  /// <param name="MD5Signature">Buffer for MD5 signature</param>
  /// <returns>Content of the file as byte array</returns>
  protected byte[] GetFileAsBuffer(String path, String fileName, out byte[] MD5Signature)
  {
    FileInfo fileInfo;
    FileStream fileStream;
    byte[] buffer;

    //Create a file stream from an existing file.
    fileInfo = new FileInfo(path + fileName);
    fileStream = fileInfo.OpenRead();

    //Read file into memory buffer
    buffer = new byte[fileStream.Length];
    fileStream.Read(buffer, 0, (int)fileStream.Length);
    fileStream.Close();
    fileStream.Dispose();

    //Compute MD5 signature
    MD5 md5 = new MD5CryptoServiceProvider();
    MD5Signature = md5.ComputeHash(buffer);

    return buffer;
  }

  /// <summary>
  /// Loads text file from disk into the String object.
  /// </summary>
  /// <param name="path">Path to the file</param>
  /// <param name="fileName">File name</param>
  /// <returns>Content of the file as String</returns>
  protected String GetFileAsXmlString(String path, String fileName)
  {
    String fileContent;
    //Create and load file into xml document
    XmlDocument xmlDoc = new XmlDocument();
    xmlDoc.Load(path + fileName);
    //Remove xml declaration if present
    if (xmlDoc.FirstChild.GetType() == typeof(XmlDeclaration))
      xmlDoc.RemoveChild(xmlDoc.FirstChild);
    //Return xml as string
    fileContent = xmlDoc.OuterXml;
    return fileContent;
  }

  /// <summary>
  /// Writes DDD Version info file into the HttpResponse - thus forcing the SaveAs dialog to
  /// apear in client browser
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  protected void btSaveVersionInfo_Click(object sender, EventArgs e)
  {
    btSaveVersionInfo.Enabled = false;
    try
    {
      //Get version info file name
      String fullName = (String)Session["SessionDir"] + Resources.ConfigImport.DDDVerInfFileName;
      //check if the file exists
      if (!File.Exists(fullName))
        return; //Version info file does not exist - nothing to send

      // set the http content type to "APPLICATION/OCTET-STREAM
      Response.ContentType = "APPLICATION/OCTET-STREAM";
      //Clear the response - only the file will be included
      Response.Clear();

      // initialize the http content-disposition header to
      // indicate a file attachment
      String disHeader = "Attachment; Filename=\"" + Resources.ConfigImport.DDDVerInfFileName + "\"";
      Response.AppendHeader("Content-Disposition", disHeader);

      // transfer the file byte-by-byte to the response object
      Response.TransmitFile(fullName);
      Response.Flush();
      //End the response - response contains the file only
      Response.End();
    }
    catch (Exception ex)
    {
      lbImportMessages.Items.Add(ex.Message);
      lbError.Text = Resources.ConfigImport.msgUploadError + ex.Message;
      lbError.Visible = true;
    }
  }

  /// <summary>
  /// Retrieves DDDVariables document using stored procedure 
  /// and stores this document back into the ImportedFiles table.
  /// Document will be used for parsing of uploaded records.
  /// This is the only way I know to store the document in table.
  /// </summary>
  /// <param name="VersionID">Identifier of DDD version</param>

  /// <summary>
  /// Handler for any errors in page not handled in code by catch blocks.
  /// It is actually page-level exception handler for all types of exceptions.
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  private void Page_Error(object sender, EventArgs e)
  {
    //Page error occured - display the text in response
    Response.Write(Resources.Resource.pageErrorText + Server.GetLastError().Message);
    Server.ClearError();
  }
}

