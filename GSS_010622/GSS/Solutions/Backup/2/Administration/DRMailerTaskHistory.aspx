<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="DRMailerTaskHistory.aspx.cs"
  Inherits="Administration_DRMailerTaskHistory" Title="Execution history of DR Mailer task" %>

<asp:Content ID="cntDRMailerTaskHist" ContentPlaceHolderID="contentPH" runat="Server">
  <div style="margin: 5px">
    <table cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td style="height: 36px;">
          <h4 style="margin-bottom: 0px;">
            <asp:Label ID="lTaskname" runat="server" Text="Task"></asp:Label>
            execution history
          </h4>
        </td>
        <td style="height: 36px; width: 200px; text-align: right;">
          <h4 style="font-size: small; margin-bottom: 0px;">
            Last
            <asp:DropDownList ID="ddlMaxRows" runat="server" AutoPostBack="True" Font-Size="X-Small"
              Width="40px">
              <asp:ListItem>10</asp:ListItem>
              <asp:ListItem Selected="True">20</asp:ListItem>
              <asp:ListItem>30</asp:ListItem>
              <asp:ListItem>40</asp:ListItem>
              <asp:ListItem>50</asp:ListItem>
              <asp:ListItem>60</asp:ListItem>
              <asp:ListItem>70</asp:ListItem>
              <asp:ListItem>80</asp:ListItem>
              <asp:ListItem>90</asp:ListItem>
              <asp:ListItem>100</asp:ListItem>
            </asp:DropDownList>
            records
          </h4>
        </td>
      </tr>
    </table>
    <asp:ObjectDataSource ID="dsTaskhistory" runat="server" OldValuesParameterFormatString="original_{0}"
      SelectMethod="GetTaskHistory" TypeName="BLL.DRMailerTaskDefsBLL" OnObjectCreated="dsTaskhistory_ObjectCreated">
      <SelectParameters>
        <asp:QueryStringParameter ConvertEmptyStringToNull="False" DefaultValue="" Name="TaskName"
          QueryStringField="TaskName" Type="String" />
        <asp:ControlParameter ControlID="ddlMaxRows" Name="MaxRows" PropertyName="SelectedValue"
          Type="Int32" />
      </SelectParameters>
    </asp:ObjectDataSource>
    <asp:GridView ID="gvTaskHistory" runat="server" AllowSorting="True" AutoGenerateColumns="False"
      BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3"
      DataSourceID="dsTaskhistory" ForeColor="Black" OnRowDataBound="gvTaskHistory_RowDataBound"
      Font-Names="Tahoma" Font-Size="Small">
      <FooterStyle BackColor="#CCCCCC" />
      <Columns>
        <asp:BoundField DataField="ExecTime" DataFormatString="{0:yyyy-MM-dd HH:mm:ss}" HeaderText="Execution time"
          SortExpression="ExecTime" HtmlEncode="False" />
        <asp:CheckBoxField DataField="Succeeded" HeaderText="Succeeded" SortExpression="Succeeded">
          <ItemStyle HorizontalAlign="Center" />
        </asp:CheckBoxField>
        <asp:BoundField DataField="StartTime" DataFormatString="{0:yyyy-MM-dd HH:mm:ss}"
          HeaderText="Query from" SortExpression="StartTime" HtmlEncode="False" />
        <asp:BoundField DataField="EndTime" DataFormatString="{0:yyyy-MM-dd HH:mm:ss}" HeaderText="Query to"
          SortExpression="EndTime" HtmlEncode="False" />
        <asp:BoundField DataField="RecCount" HeaderText="Selected records" SortExpression="RecCount" />
        <asp:CheckBoxField DataField="RecOverload" HeaderText="Maximum exceeded" SortExpression="RecOverload">
          <ItemStyle HorizontalAlign="Center" />
        </asp:CheckBoxField>
        <asp:BoundField DataField="Recipients" HeaderText="Mail recipients" SortExpression="Recipients" />
        <asp:BoundField DataField="ErrorMessage" HeaderText="Error message" SortExpression="ErrorMessage" />
      </Columns>
      <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
      <PagerStyle BackColor="#FFDDDD" ForeColor="Blue" HorizontalAlign="Center" />
      <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
      <AlternatingRowStyle BackColor="#DDDDFF" BorderColor="White" BorderStyle="Solid"
        BorderWidth="2px" />
      <RowStyle BackColor="#DDFFDD" BorderColor="White" BorderStyle="Solid" BorderWidth="2px" />
    </asp:GridView>
  </div>
</asp:Content>
