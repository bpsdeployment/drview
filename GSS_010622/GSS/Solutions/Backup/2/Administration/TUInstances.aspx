<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="TUInstances.aspx.cs"
  Inherits="Administration_TUTypes" Title="TU Instances" %>

<asp:Content ID="cntTUInstances" runat="server" ContentPlaceHolderID="contentPH">
  <table cellspacing="5" style="width: 100%">
    <tr>
      <td>
        <h4>
          Filter</h4>
      </td>
    </tr>
    <tr>
      <td style="width: 100px">
        <h5>
          TU Type</h5>
        <asp:DropDownList ID="ddlTUTypes" runat="server" AutoPostBack="True" DataSourceID="dsTUTypes"
          DataTextField="TypeName" DataValueField="TUTypeID" Width="300px" OnDataBound="ddlTUTypes_DataBound">
        </asp:DropDownList>
        <asp:SqlDataSource ID="dsTUTypes" runat="server" ConnectionString="<%$ ConnectionStrings:CDDBConnectionString %>"
          SelectCommand="SELECT [TypeName], [TUTypeID] FROM [TUTypes] ORDER BY [TypeName]"
          CacheDuration="20" CacheExpirationPolicy="Sliding"></asp:SqlDataSource>
      </td>
    </tr>
    <tr>
      <td>
        <h5>
          Running DDD Version</h5>
        <asp:DropDownList ID="ddlDDDVersions" runat="server" DataSourceID="dsDDDVersions"
          DataTextField="UserVersion" DataValueField="VersionID" Width="300px" AutoPostBack="True"
          OnDataBound="ddlDDDVersions_DataBound">
        </asp:DropDownList><br />
        <asp:SqlDataSource ID="dsDDDVersions" runat="server" CacheDuration="20" CacheExpirationPolicy="Sliding"
          ConnectionString="<%$ ConnectionStrings:CDDBConnectionString %>" SelectCommand="SELECT DDDVersions.UserVersion, DDDVersions.VersionID, DDDVersions.TUTypeID, TUTypes.TypeName FROM DDDVersions INNER JOIN TUTypes ON DDDVersions.TUTypeID = TUTypes.TUTypeID ORDER BY DDDVersions.UserVersion"
          FilterExpression="(IIF('{0}' = 'All',true,TUTypeID = '{0}'))">
          <FilterParameters>
            <asp:ControlParameter ControlID="ddlTUTypes" DefaultValue="" Name="TUTypeID" PropertyName="SelectedValue" />
          </FilterParameters>
        </asp:SqlDataSource>
      </td>
    </tr>
  </table>
  <br />
  <table border="0" cellpadding="0" cellspacing="5" style="width: 100%; height: 100%;
    font-size: 12pt; font-family: Times New Roman;">
    <tr>
      <td colspan="2">
        <h4>
          Telediagnostic Unit Instances</h4>
      </td>
    </tr>
    <tr style="font-size: 12pt">
      <td style="width: 85%; height: 70%;">
        <asp:GridView ID="gvTUInstances" runat="server" AllowPaging="True" AllowSorting="True"
          BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3"
          DataSourceID="dsTUInstances" ForeColor="Black" AutoGenerateColumns="False" Width="100%"
          DataKeyNames="TUInstanceID,TUTypeID,VersionID,VersionIDConf" OnDataBound="gvTUInstances_DataBound"
          OnSelectedIndexChanged="gvTUInstances_SelectedIndexChanged" OnRowDataBound="gvTUInstances_RowDataBound"
          Font-Names="Tahoma" Font-Size="Small">
          <FooterStyle BackColor="#CCCCCC" />
          <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="TUName" HeaderText="Name" SortExpression="TUName" />
            <asp:BoundField DataField="TypeName" HeaderText="Type" SortExpression="TypeName" />
            <asp:BoundField DataField="UICID" HeaderText="UIC Number" SortExpression="UICID" />
            <asp:BoundField DataField="Comment" HeaderText="Comment" SortExpression="Comment" />
            <asp:BoundField DataField="UserVersion" HeaderText="Running DDD" SortExpression="UserVersion" />
            <asp:BoundField DataField="UserVerConf" HeaderText="Configured DDD" SortExpression="UserVerConf" />
            <asp:HyperLinkField DataNavigateUrlFields="IPAddrWiFi" DataNavigateUrlFormatString="http://{0}"
              DataTextField="IPAddrWiFi" HeaderText="IP addr. WiFi" SortExpression="IPAddrWiFi" />
            <asp:HyperLinkField DataNavigateUrlFields="IPAddrGPRS" DataNavigateUrlFormatString="http://{0}"
              DataTextField="IPAddrGPRS" HeaderText="IP addr. GPRS" SortExpression="IPAddrGPRS" />
            <asp:BoundField DataField="IPAddrUpdTime" HeaderText="Last IP upd. time" SortExpression="IPAddrUpdTime" DataFormatString="{0:yyyy-MM-dd HH:mm:ss}" HtmlEncode="False" />
            <asp:BoundField DataField="TUName" HeaderText="TUName" SortExpression="TUName" Visible="False" />
            <asp:HyperLinkField DataNavigateUrlFields="TUInstanceID" DataNavigateUrlFormatString="DRStats.aspx?TUInstanceID={0}"
              ShowHeader="False" Text="Rec. stat." />
          </Columns>
          <SelectedRowStyle Font-Bold="True"/>
          <PagerStyle BackColor="#FFDDDD" ForeColor="Blue" HorizontalAlign="Center" />
          <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
          <AlternatingRowStyle BackColor="#DDDDFF" BorderColor="White" BorderStyle="Solid"
            BorderWidth="2px" />
          <RowStyle BackColor="#DDFFDD" BorderColor="White" BorderStyle="Solid" BorderWidth="2px" />
        </asp:GridView>
        <asp:SqlDataSource ID="dsTUInstances" runat="server" ConnectionString="<%$ ConnectionStrings:CDDBConnectionString %>"
          SelectCommand="SELECT TUInstances.TUName, TUTypes.TypeName, TUInstances.UICID, TUInstances.Comment, VerRunning.UserVersion, TUInstances.IPAddrGPRS, TUInstances.IPAddrWiFi, TUInstances.IPAddrUpdTime, VerRunning.VersionID, TUInstances.TUInstanceID, TUInstances.TUTypeID, VerConf.UserVersion AS UserVerConf, VerConf.VersionID AS VersionIDConf FROM TUInstances INNER JOIN TUTypes ON TUTypes.TUTypeID = TUInstances.TUTypeID LEFT OUTER JOIN DDDVersions AS VerRunning ON TUInstances.DDDVersionID = VerRunning.VersionID LEFT OUTER JOIN DDDVersions AS VerConf ON TUInstances.ConfVersionID = VerConf.VersionID ORDER BY TUInstances.TUName"
          CacheDuration="60" CacheExpirationPolicy="Sliding" FilterExpression="(IIF('{0}' = 'All',true,TUTypeID =  '{0}')) AND (IIF('{1}' = 'All',true, VersionID = '{1}'))"
          UpdateCommand="UPDATE TUInstances SET TUTypeID = @TUTypeID, TUName = @TUName, UICID = @UICID, Comment = @Comment WHERE (TUInstanceID = @TUInstanceID);&#13;&#10;"
          DeleteCommand="DeleteTUInstance" InsertCommand="INSERT INTO TUInstances(TUInstanceID, TUTypeID, TUName, UICID, Comment) VALUES (NEWID(), @TUTypeID, @TUName, @UICID, @Comment)"
          DeleteCommandType="StoredProcedure">
          <FilterParameters>
            <asp:ControlParameter ControlID="ddlTUTypes" Name="TUTypeID" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="ddlDDDVersions" Name="VersionID" PropertyName="SelectedValue" />
          </FilterParameters>
          <UpdateParameters>
            <asp:ControlParameter ControlID="ddlDetailTUType" Name="TUTypeID" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="tbDetailName" Name="TUName" PropertyName="Text" />
            <asp:ControlParameter ControlID="tbDetailUICNo" Name="UICID" PropertyName="Text" />
            <asp:ControlParameter ControlID="tbDetailComment" Name="Comment" PropertyName="Text" />
            <asp:ControlParameter ControlID="gvTUInstances" Name="TUInstanceID" PropertyName="SelectedValue" />
          </UpdateParameters>
          <DeleteParameters>
            <asp:ControlParameter ControlID="gvTUInstances" Name="TUInstanceID" PropertyName="SelectedValue" />
          </DeleteParameters>
          <InsertParameters>
            <asp:ControlParameter ControlID="ddlDetailTUType" Name="TUTypeID" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="tbDetailName" Name="TUName" PropertyName="Text" />
            <asp:ControlParameter ControlID="tbDetailUICNo" Name="UICID" PropertyName="Text" />
            <asp:ControlParameter ControlID="tbDetailComment" Name="Comment" PropertyName="Text" />
          </InsertParameters>
        </asp:SqlDataSource>
      </td>
    </tr>
  </table>
  <br />
  <table cellspacing="5" style="width: 100%">
    <tr>
      <td colspan="4">
        <h4>
          Selected Instance Details</h4>
      </td>
    </tr>
    <tr>
      <td style="width: 160px">
        <h5>
          Name</h5>
      </td>
      <td style="width: 300px">
        <asp:TextBox ID="tbDetailName" runat="server" Width="300px"></asp:TextBox></td>
      <td style="width: 70px">
        &nbsp;</td>
      <td>
        <asp:RegularExpressionValidator ID="regularValName" runat="server" ControlToValidate="tbDetailName"
          Display="None" ErrorMessage="Name has to be maximum 50 characters long" ValidationExpression="(.){1,50}"></asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator ID="requireValName" runat="server" ControlToValidate="tbDetailName"
          Display="None" ErrorMessage="Name is required."></asp:RequiredFieldValidator></td>
    </tr>
    <tr>
      <td style="width: 160px">
        <h5>
          Type</h5>
      </td>
      <td style="width: 300px">
        <asp:DropDownList ID="ddlDetailTUType" runat="server" DataSourceID="dsTUTypes" DataTextField="TypeName"
          DataValueField="TUTypeID" AutoPostBack="True" Width="306px">
        </asp:DropDownList></td>
      <td style="width: 70px">
        </td>
      <td>
      </td>
    </tr>
    <tr>
      <td style="width: 160px">
        <h5>
          UIC Number</h5>
      </td>
      <td style="width: 300px">
        <asp:TextBox ID="tbDetailUICNo" runat="server" Width="300px"></asp:TextBox></td>
      <td style="width: 70px">
        </td>
      <td>
        <asp:CompareValidator ID="compareValUICNo" runat="server" ControlToValidate="tbDetailUICNo"
          Display="None" ErrorMessage="UIC Number has to be valid integer number." Operator="DataTypeCheck"
          Type="Integer"></asp:CompareValidator></td>
    </tr>
    <tr>
      <td style="width: 160px">
        <h5>
          Comment</h5>
      </td>
      <td style="width: 300px">
        <asp:TextBox ID="tbDetailComment" runat="server" Width="300px"></asp:TextBox></td>
      <td style="width: 70px">
        </td>
      <td>
        <asp:RegularExpressionValidator ID="regulaValComment" runat="server" ControlToValidate="tbDetailComment"
          Display="None" ErrorMessage="Comment has to be maximum 255 characters long" ValidationExpression=".{0,255}"></asp:RegularExpressionValidator></td>
    </tr>
    <tr>
      <td colspan="4">
        <asp:Button ID="btUpdate" runat="server" Enabled="False" OnClick="btUpdate_Click"
          Text="Update" Width="70px" />
        <asp:Button ID="btNew" runat="server" Text="New" Width="70px" OnClick="btNew_Click" />
        <asp:Button ID="btDelete" runat="server" Enabled="False" Text="Delete" Width="70px"
          OnClick="btDelete_Click" /></td>
    </tr>
  </table>
  <br />
  <table cellspacing="5" style="width: 100%">
    <tr>
      <td colspan="4">
        <h4>
          Selected Instance Configuration</h4>
      </td>
    </tr>
    <tr>
      <td style="width: 160px">
        <h5>
          Running DDD Version</h5>
      </td>
      <td style="width: 300px">
        <asp:TextBox ID="tbRunningDDD" runat="server" ReadOnly="True" Width="300px"></asp:TextBox></td>
      <td style="width: 70px">
      </td>
      <td>
      </td>
    </tr>
    <tr>
      <td style="width: 160px">
        <h5>
          Configured DDD Version</h5>
      </td>
      <td style="width: 300px">
        <asp:DropDownList ID="ddlConfiguredDDD" runat="server" DataSourceID="dsConfiguredDDD"
          DataTextField="UserVersion" DataValueField="VersionID" OnDataBound="ddlConfiguredDDD_DataBound"
          Width="306px" OnSelectedIndexChanged="ddlConfiguredDDD_SelectedIndexChanged">
        </asp:DropDownList></td>
      <td style="width: 70px">
        <asp:Button ID="btExportConf" runat="server" Enabled="False" Text="Export" Width="70px" OnClick="btExportConf_Click" /></td>
      <td>
      </td>
    </tr>
    <tr>
      <td colspan="4">
        <asp:SqlDataSource ID="dsConfiguredDDD" runat="server" CacheDuration="20" CacheExpirationPolicy="Sliding"
          ConnectionString="<%$ ConnectionStrings:CDDBConnectionString %>" SelectCommand="SELECT DDDVersions.UserVersion, DDDVersions.VersionID, DDDVersions.TUTypeID, TUTypes.TypeName FROM DDDVersions INNER JOIN TUTypes ON DDDVersions.TUTypeID = TUTypes.TUTypeID ORDER BY DDDVersions.UserVersion"
          FilterExpression="(IIF('{0}' = 'All',true,TUTypeID = '{0}'))">
          <FilterParameters>
            <asp:ControlParameter ControlID="ddlDetailTUType" DefaultValue="" Name="TUTypeID"
              PropertyName="SelectedValue" />
          </FilterParameters>
        </asp:SqlDataSource>
        <asp:Button ID="btDwnlTUInstInfo" runat="server" Enabled="False" EnableTheming="True"
          OnClick="btDwnlTUInstInfo_Click" Text="Download Info Document" />
        <br />
        <br />
        <asp:ValidationSummary ID="valSummary" runat="server" Width="100%" />
      </td>
    </tr>
  </table>  
  <div style="margin: 5px">
    <h5>
      <asp:Label ID="lbError" runat="server" EnableViewState="False" ForeColor="Red" Text="Label"
        Visible="False" Width="100%"></asp:Label>
    </h5>
  </div>
</asp:Content>
