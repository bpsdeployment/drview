using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DDDObjects;
using ZetaLibWeb;
using BLL;

public partial class Administration_DRMailerTasks : System.Web.UI.Page
{
  /// <summary>
  /// Handler for any errors in page not handled in code by catch blocks.
  /// It is actually page-level exception handler for all types of exceptions.
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  private void Page_Error(object sender, EventArgs e)
  {
    Server.Transfer("~/Administration/Error.aspx");
  }
  
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {//No postback 
      //Obtain DDDHelper from application state
      DDDHelper dddHelper = (DDDHelper)Application["DDDHelper"];
      //Fill in TUInstances from helper
      chblTUInstances.Items.Clear();
      foreach (KeyValuePair<Guid, TUInstance> tuInstPair in dddHelper.TUInstances)
        chblTUInstances.Items.Add(new ListItem(tuInstPair.Value.TUName, tuInstPair.Key.ToString()));
      //Associate confirm dialog with Delete task button
      btDelete.Attributes.Add("onclick", "return confirm('Delete selected task?');");
    }
  }

  protected void dsTaskDefs_ObjectCreated(object sender, ObjectDataSourceEventArgs e)
  {
    ((DRMailerTaskDefsBLL)e.ObjectInstance).DDDHelper = (DDDHelper)Application["DDDHelper"];
  }

  protected void gvTaskDefs_SelectedIndexChanged(object sender, EventArgs e)
  {
    //Create bussiness logic object
    DRMailerTaskDefsBLL tasksBLL = new DRMailerTaskDefsBLL();
    tasksBLL.DDDHelper = (DDDHelper)Application["DDDHelper"];
    //Obtain details of selected task
    DRMailerTasks.DRMailerTaskDefsRow taskRow = tasksBLL.GetTaskDefinition(gvTaskDefs.SelectedDataKey.Value.ToString());
    if (taskRow == null)
      throw new Exception(String.Format("Selected task {0} not found.", gvTaskDefs.SelectedDataKey));
    //Set visual object properties based on row values
    lTaskName.Text = taskRow.TaskName;
    tbTaskName.Text = taskRow.TaskName;
    tbSubject.Text = taskRow.Subject;
    if (!taskRow.IsCommentNull())
      tbComment.Text = taskRow.Comment;
    else
      tbComment.Text = "";
    tbStartTime.Text = taskRow.StartTime.ToString("yyyy-MM-dd HH:mm:ss");
    SelectDDLItem(ddlPeriod, taskRow.Period.ToString());
    SelectDDLItem(ddlInterval, taskRow.Interval.ToString());
    SelectDDLItem(ddlDelay, taskRow.Delay.ToString());
    chbEnabled.Checked = taskRow.Enabled;
    tbMailRecipients.Text = taskRow.Recipients;
    if (!taskRow.IsFaultCodesNull())
      tbFaultCodes.Text = taskRow.FaultCodes;
    else
      tbFaultCodes.Text = "";
    if (!taskRow.IsSourcesNull())
      tbSources.Text = taskRow.Sources;
    else
      tbSources.Text = "";
    if (!taskRow.IsTitleKeywordsNull())
      tbTitleKeywords.Text = taskRow.TitleKeywords;
    else
      tbTitleKeywords.Text = "";
    if (!taskRow.IsMatchAllKeywordsNull())
      chbMatchAllKeywords.Checked = taskRow.MatchAllKeywords;
    else
      chbMatchAllKeywords.Checked = false;
    if (!taskRow.IsMaxRecordsNull())
      tbMaxRecords.Text = taskRow.MaxRecords.ToString();
    else
      tbMaxRecords.Text = "";
    if (!taskRow.IsMaxFileLengthBytesNull())
      tbMaxFileLength.Text = taskRow.MaxFileLengthBytes.ToString();
    else
      tbMaxFileLength.Text = "";
    tbFileName.Text = taskRow.FileNameFormat;
    chbSendEmptyFile.Checked = taskRow.SendEmptyFile;
    eRecordCategory recCategories = 0;
    if (!taskRow.IsRecTypeMaskNull())
      recCategories = (eRecordCategory)taskRow.RecTypeMask;
    chblRecCategories.Items.FindByValue(((int)eRecordCategory.Event).ToString()).Selected = ((recCategories & eRecordCategory.Event) > 0);
    chblRecCategories.Items.FindByValue(((int)eRecordCategory.Snap).ToString()).Selected = ((recCategories & eRecordCategory.Snap) > 0);
    chblRecCategories.Items.FindByValue(((int)eRecordCategory.Trace).ToString()).Selected = ((recCategories & eRecordCategory.Trace) > 0);
    //Select TUInstances
    String tuInstances = taskRow.TUInstances.ToUpper();
    foreach (ListItem item in chblTUInstances.Items)
      if (tuInstances.Contains(item.Value.ToUpper()))
        item.Selected = true;
      else
        item.Selected = false;
    //Enable buttons
    btUpdate.Enabled = true;
    btDelete.Enabled = true;
    btTaskHistory.Enabled = true;
  }

  protected void btUpdate_Click(object sender, EventArgs e)
  {
    //Call update method
    dsTaskDefs.Update();
  }

  protected void btInsert_Click(object sender, EventArgs e)
  {
    //Call insert method
    dsTaskDefs.Insert();
  }

  protected void btDelete_Click(object sender, EventArgs e)
  {
    //Call delete method
    dsTaskDefs.Delete();
  }

  protected void dsTaskDefs_Updating(object sender, ObjectDataSourceMethodEventArgs e)
  {
    //Set parameters for the operation
    SetTaskDefParameters(e);
  }

  protected void dsTaskDefs_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
  {
    //Set parameters for the operation
    SetTaskDefParameters(e);
  }

  protected void btTaskHistory_Click(object sender, EventArgs e)
  {
    //Redirect to page displaying task history
    //Specify task name in query parameter
    QueryString target = new QueryString("DRMailerTaskHistory.aspx");
    target["TaskName"] = (String)gvTaskDefs.SelectedValue;
    Response.Redirect(target.All);
  }

  protected void gvTaskDefs_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    if (e.Row.RowIndex >= 0)
    {
      DataRowView rowView = (DataRowView)e.Row.DataItem;
      DRMailerTasks.DRMailerTaskDefsRow row = (DRMailerTasks.DRMailerTaskDefsRow)rowView.Row;
      if (row.IsLastFailedNull())
        e.Row.BackColor = System.Drawing.Color.LightGoldenrodYellow;
      else if (row.LastFailed)
        e.Row.BackColor = System.Drawing.Color.LightCoral;
      else
        e.Row.BackColor = System.Drawing.Color.LightGreen;
    }
  }

  #region Helper methods
  /// <summary>
  /// Selects item with specified value in the drop down list box.
  /// </summary>
  private void SelectDDLItem(DropDownList list, String value)
  {
    try { list.SelectedValue = value; }
    catch (ArgumentOutOfRangeException) 
    { 
      list.Items.Add(new ListItem(value, value));
      list.SelectedValue = value;
    }
  }

  /// <summary>
  /// Sets parameters for input or update operations on task def data source
  /// </summary>
  private void SetTaskDefParameters(ObjectDataSourceMethodEventArgs e)
  {
    //Specify update parameters which could not be specified by expression binding
    e.InputParameters["MaxDelay"] = (int)(e.InputParameters["Period"]) * 2;
    String tuInstances = "";
    foreach (ListItem item in chblTUInstances.Items)
      if (item.Selected)
        tuInstances += item.Value + " ";
    tuInstances = tuInstances.Trim();
    e.InputParameters["TUInstances"] = tuInstances;
    int recCat = 0;
    foreach (ListItem item in chblRecCategories.Items)
      if (item.Selected)
        recCat += Int32.Parse(item.Value);
    e.InputParameters["RecTypeMask"] = recCat;
    //!!Rest of the parameters is specified by expression binding in aspx code!!!
  }
  #endregion //Helper methods
}
