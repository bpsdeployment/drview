<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="DRStats.aspx.cs"
  Inherits="Administration_DRStats" Title="Diagnostic records statistics" %>

<asp:Content ID="cntDRStats" ContentPlaceHolderID="contentPH" runat="Server">
  <table cellspacing="5" style="width: 100%">
    <tr>
      <td>
        <h4>
          Filter
        </h4>
      </td>
    </tr>
    <tr>
      <td>
        <h5>
          Telediagnostic Unit
          <br />
          <asp:DropDownList ID="ddlTUInstances" runat="server" DataSourceID="dsTUInstance"
            DataTextField="TUName" DataValueField="TUInstanceID" Width="300px">
          </asp:DropDownList>
          <asp:SqlDataSource ID="dsTUInstance" runat="server" ConnectionString="<%$ ConnectionStrings:CDDBConnectionString %>"
            SelectCommand="SELECT TUInstanceID, TUName FROM TUInstances"></asp:SqlDataSource>
        </h5>
      </td>
    </tr>
    <tr>
      <td>
        <h5>
          Time range
          <br />
          <asp:DropDownList ID="ddlTimeRange" runat="server" Width="300px">
            <asp:ListItem Selected="True" Value="1">Last day</asp:ListItem>
            <asp:ListItem Value="2">Last 2 days</asp:ListItem>
            <asp:ListItem Value="3">Last 3 days</asp:ListItem>
            <asp:ListItem Value="4">Last 4 days</asp:ListItem>
            <asp:ListItem Value="7">Last week</asp:ListItem>
            <asp:ListItem Value="14">Last 2 weeks</asp:ListItem>
            <asp:ListItem Value="30">Last month</asp:ListItem>
            <asp:ListItem Value="60">Last 2 months</asp:ListItem>
            <asp:ListItem Value="120">Last 4 months</asp:ListItem>
            <asp:ListItem Value="180">Last 6 months</asp:ListItem>
          </asp:DropDownList></h5>
      </td>
    </tr>
    <tr>
      <td>
        <asp:Button ID="btSelect" runat="server" OnClick="btSelect_Click" Text="Select" OnClientClick="var now = new Date(); ctl00_contentPH_TimeZoneFiled.value = now.getTimezoneOffset();" />
      </td>
    </tr>
  </table>
  <br />
  <table cellspacing="5" style="width: 100%; height: 100%">
    <tr>
      <td valign="middle">
        <h4>
          <asp:Label ID="Label2" runat="server" Text="Record statistics" Width="151px"></asp:Label>
          <asp:Label ID="labelTimes" runat="server" Font-Size="Small" Visible="False" Width="576px"></asp:Label>
        </h4>
      </td>
    </tr>
    <tr>
      <td style="width: 100%; height: 100%" valign="top">
        <asp:GridView ID="gvRecStats" runat="server" AllowSorting="True" AutoGenerateColumns="False"
          BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3"
          DataSourceID="dsDRStats" ForeColor="Black" Font-Names="Tahoma" Font-Size="Small"
          OnDataBound="gvRecStats_DataBound" OnRowDataBound="gvRecStats_RowDataBound">
          <FooterStyle BackColor="#CCCCCC" />
          <Columns>
            <asp:BoundField DataField="Title" HeaderText="Record" SortExpression="Title" />
            <asp:BoundField DataField="FaultCode" HeaderText="Fault code" SortExpression="FaultCode" />
            <asp:BoundField DataField="RecCount" HeaderText="Count" SortExpression="RecCount" />
            <asp:BoundField DataField="FirstTime" HeaderText="Time of first" SortExpression="FirstTime" />
            <asp:BoundField DataField="LastTime" HeaderText="Time of last" SortExpression="LastTime" />
          </Columns>
          <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
          <PagerStyle BackColor="#FFDDDD" ForeColor="Blue" HorizontalAlign="Center" />
          <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
          <AlternatingRowStyle BackColor="#DDDDFF" BorderColor="White" BorderStyle="Solid"
            BorderWidth="2px" />
          <RowStyle BackColor="#DDFFDD" BorderColor="White" BorderStyle="Solid" BorderWidth="2px" />
        </asp:GridView>
        <asp:SqlDataSource ID="dsDRStats" runat="server" ConnectionString="<%$ ConnectionStrings:CDDBConnectionString %>"
          SelectCommand="GetRecordStats" SelectCommandType="StoredProcedure" OnSelecting="dsDRStats_Selecting">
          <SelectParameters>
            <asp:SessionParameter Name="TUInstanceID" SessionField="TUInstanceID" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="TimeFrom" SessionField="TimeFrom" Type="DateTime" />
            <asp:SessionParameter DefaultValue="" Name="TimeTo" SessionField="TimeTo" Type="DateTime" />
            <asp:Parameter DefaultValue="1" Name="LanguageID" Type="Int32" />
          </SelectParameters>
        </asp:SqlDataSource>
        <asp:HiddenField ID="TimeZoneFiled" runat="server" Value="-1" />
      </td>
    </tr>
  </table>
</asp:Content>
