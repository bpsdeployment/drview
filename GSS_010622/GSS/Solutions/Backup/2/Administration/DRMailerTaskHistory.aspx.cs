using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ZetaLibWeb;
using BLL;
using DDDObjects;

public partial class Administration_DRMailerTaskHistory : System.Web.UI.Page
{
  /// <summary>
  /// Handler for any errors in page not handled in code by catch blocks.
  /// It is actually page-level exception handler for all types of exceptions.
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="e"></param>
  private void Page_Error(object sender, EventArgs e)
  {
    Server.Transfer("~/Administration/Error.aspx");
  }
  
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack)
    {
      if (Request.QueryString["TaskName"] != null)
        lTaskname.Text = Request.QueryString["TaskName"];
    }
  }
  protected void gvTaskHistory_RowDataBound(object sender, GridViewRowEventArgs e)
  {
    if (e.Row.RowIndex >= 0)
    {
      DataRowView rowView = (DataRowView)e.Row.DataItem;
      DRMailerTasks.DRMailerTaskHistoryRow row = (DRMailerTasks.DRMailerTaskHistoryRow)rowView.Row;
      if (row.Succeeded)
        e.Row.BackColor = System.Drawing.Color.LightGreen;
      else
        e.Row.BackColor = System.Drawing.Color.LightCoral;
    }
  }
  protected void dsTaskhistory_ObjectCreated(object sender, ObjectDataSourceEventArgs e)
  {
    ((DRMailerTaskDefsBLL)e.ObjectInstance).DDDHelper = (DDDHelper)Application["DDDHelper"];
  }
}
