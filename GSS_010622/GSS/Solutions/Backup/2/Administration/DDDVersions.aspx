<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="DDDVersions.aspx.cs"
  Inherits="Administration_DDDVersions" Title="DDD Versions" %>

<asp:Content ID="cntDDDVersions" ContentPlaceHolderID="contentPH" runat="Server">
  <table cellspacing="5" style="width: 100%">
    <tr>
      <td colspan="4">
        <h4>
          Filter</h4>
      </td>
    </tr>
    <tr>
      <td colspan="4">
        <h5>
          TU Type</h5>
        <asp:DropDownList ID="ddlTUTypes" runat="server" Width="300px" AutoPostBack="True"
          DataSourceID="dsTUTypes" DataTextField="TypeName" DataValueField="TypeName" OnDataBound="ddlTUTypes_DataBound"
          OnSelectedIndexChanged="ddlTUTypes_SelectedIndexChanged" CssClass="td" CausesValidation="True">
        </asp:DropDownList>
        <asp:SqlDataSource ID="dsTUTypes" runat="server" ConnectionString="<%$ ConnectionStrings:CDDBConnectionString %>"
          SelectCommand="SELECT [TypeName] FROM [TUTypes]"></asp:SqlDataSource>
      </td>
    </tr>
    <tr>
      <td colspan="4">
        <h5>
          User Version</h5>
        <asp:DropDownList ID="ddlUserVersions" runat="server" Width="300px" AutoPostBack="True"
          DataSourceID="dsUserVersions" DataTextField="UserVersion" DataValueField="UserVersion"
          Enabled="False" OnDataBound="ddlUserVersions_DataBound" CssClass="td" CausesValidation="True">
        </asp:DropDownList>
        <asp:SqlDataSource ID="dsUserVersions" runat="server" ConnectionString="<%$ ConnectionStrings:CDDBConnectionString %>"
          FilterExpression="TypeName = '{0}'" SelectCommand="SELECT DDDVersions.UserVersion, TUTypes.TypeName FROM DDDVersions INNER JOIN TUTypes ON DDDVersions.TUTypeID = TUTypes.TUTypeID">
          <FilterParameters>
            <asp:ControlParameter ControlID="ddlTUTypes" DefaultValue="" Name="TUTypeName" PropertyName="SelectedValue" />
          </FilterParameters>
        </asp:SqlDataSource>
      </td>
    </tr>
    <tr>
      <td colspan="4">
        <asp:CheckBox ID="chbUseDateFilter" runat="server" Text=" Date filter" ToolTip="Use date filter"
          Width="300px" AutoPostBack="True" Font-Names="Tahoma" Font-Size="Small" CssClass="td" CausesValidation="True" />
        <br />
        <asp:RadioButtonList ID="rblDateFilter" runat="server" AutoPostBack="True" Font-Bold="False"
          CssClass="td" Font-Names="Tahoma" Font-Size="Small" Width="300px" CausesValidation="True">
          <asp:ListItem Value="CreationDate" Selected="True">Creation date</asp:ListItem>
          <asp:ListItem Value="ImportDate">Import date</asp:ListItem>
        </asp:RadioButtonList>
      </td>
    </tr>
    <tr>
      <td>
        <h5>
          From</h5>
        <asp:TextBox ID="tbFrom" runat="server" ForeColor="Black" Width="100px"></asp:TextBox>
        <asp:RegularExpressionValidator ID="valFrom" runat="server" ControlToValidate="tbFrom"
          Display="Dynamic" ErrorMessage="Invalid date format (yyyy-mm-dd)" ValidationExpression="\d\d\d\d-[01]\d-[0-3]\d">
        </asp:RegularExpressionValidator>
      </td>
      <td>
        <br />
        <img src="../Pictures/calendar.gif" id="btFrom" onmouseover="this.style.background='black';"
          onmouseout="this.style.background=''" />
      </td>
      <td>
        <h5>
          To</h5>
        <asp:TextBox ID="tbTo" runat="server" ForeColor="Black" Width="100px"></asp:TextBox>
        <asp:RegularExpressionValidator ID="valTo" runat="server" ControlToValidate="tbTo"
          Display="Dynamic" ErrorMessage="Invalid date format (yyyy-mm-dd)" ValidationExpression="\d\d\d\d-[01]\d-[0-3]\d">
        </asp:RegularExpressionValidator>
      </td>
      <td width="100%">
        <br />
        <img src="../Pictures/calendar.gif" id="btTo" onmouseover="this.style.background='black';"
          onmouseout="this.style.background=''" />
      </td>
    </tr>
  </table>

  <script language="JavaScript" src="calendar/calendar.js"></script>

  <script language="JavaScript" src="calendar/calendar-en.js"></script>

  <script language="JavaScript" src="calendar/calendar-setup.js"></script>

  <script language="JavaScript">
    Calendar.setup({
        inputField     :    "ctl00$contentPH$tbFrom",
        ifFormat       :    "%Y-%m-%d",
        button         :    "btFrom",
        showsTime      :    false,
        timeFormat     :    "24",
        singleClick    :    true
    });
  </script>

  <script language="JavaScript">
    Calendar.setup({
        inputField     :    "ctl00$contentPH$tbTo",
        ifFormat       :    "%Y-%m-%d",
        button         :    "btTo",
        showsTime      :    false,
        timeFormat     :    "24",
        singleClick    :    true
    });
  </script>

  <br />
  <table cellspacing="5" style="width: 100%; font-family: Tahoma; height: 100%">
    <tr>
      <td>
        <h4>
          Diagnostic Data Descriptor Versions</h4>
    </tr>
    <tr>
      <td style="width: 100%" valign="top">
        <asp:GridView ID="gvDDDVersions" runat="server" AllowPaging="True" AllowSorting="True"
          AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="Solid"
          BorderWidth="1px" CellPadding="3" DataKeyNames="VersionID" DataSourceID="dsDDDVersions"
          ForeColor="Black" GridLines="Vertical" Width="100%" OnDataBound="gvDDDVersions_DataBound"
          OnSelectedIndexChanged="gvDDDVersions_SelectedIndexChanged" Font-Names="Tahoma"
          Font-Size="Small" Height="100%">
          <FooterStyle BackColor="#CCCCCC" />
          <Columns>
            <asp:CommandField ShowSelectButton="True">
              <ItemStyle ForeColor="Blue" />
            </asp:CommandField>
            <asp:BoundField DataField="TypeName" HeaderText="TU Type" SortExpression="TypeName" />
            <asp:BoundField DataField="UserVersion" HeaderText="DDD Version" SortExpression="UserVersion" />
            <asp:BoundField DataField="CreationDate" HeaderText="Creation date" SortExpression="CreationDate" DataFormatString="{0:yyyy-MM-dd HH:mm:ss}" HtmlEncode="False" />
            <asp:BoundField DataField="ImportDate" HeaderText="Import date" SortExpression="ImportDate" DataFormatString="{0:yyyy-MM-dd HH:mm:ss}" HtmlEncode="False" />
            <asp:BoundField DataField="Comment" HeaderText="Comment" SortExpression="Comment" />
            <asp:BoundField DataField="VersionID" HeaderText="VersionID" ReadOnly="True" SortExpression="VersionID"
              Visible="False" />
          </Columns>
          <SelectedRowStyle Font-Bold="True"/>
          <PagerStyle BackColor="#FFDDDD" ForeColor="Blue" HorizontalAlign="Center" />
          <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
          <AlternatingRowStyle BackColor="#DDDDFF" BorderColor="White" BorderStyle="Solid"
            BorderWidth="2px" />
          <RowStyle BackColor="#DDFFDD" BorderColor="White" BorderStyle="Solid" BorderWidth="2px" />
        </asp:GridView>
        <span style="font-size: 10pt; font-family: Tahoma"><strong></strong></span>
        <asp:SqlDataSource ID="dsDDDVersions" runat="server" ConnectionString="<%$ ConnectionStrings:CDDBConnectionString %>"
          FilterExpression="(TypeName LIKE '{0}') AND (UserVersion LIKE '{1}') AND IIF('{5}',{2} >= #{3}#,true) AND IIF('{5}',{2} <= #{4}#,true)"
          SelectCommand="SELECT TUTypes.TypeName, DDDVersions.UserVersion, DDDVersions.CreationDate, DDDVersions.Comment, DDDVersions.ImportDate, DDDVersions.VersionID FROM DDDVersions INNER JOIN TUTypes ON DDDVersions.TUTypeID = TUTypes.TUTypeID">
          <FilterParameters>
            <asp:ControlParameter ControlID="ddlTUTypes" Name="TUTypeName" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="ddlUserVersions" Name="UserVersion" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="rblDateFilter" Name="DateFilter" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="tbFrom" Name="DateFrom" PropertyName="Text" />
            <asp:ControlParameter ControlID="tbTo" Name="DateTo" PropertyName="Text" />
            <asp:ControlParameter ControlID="chbUseDateFilter" Name="UseDateFilter" PropertyName="Checked" />
          </FilterParameters>
        </asp:SqlDataSource>
        <br />
        <asp:Panel ID="panDDDCommands" runat="server" Enabled="False" Height="100%" Width="100%">
          <h5>
            TU Configuration Files</h5>
          <asp:DropDownList ID="ddlConfigFiles" runat="server" DataSourceID="dsConfigFiles"
            DataTextField="FileName" DataValueField="FileName" Width="306px" CssClass="td">
          </asp:DropDownList>
          <asp:Button ID="btDownloadFile" runat="server" Text="Download file" OnClick="btDownloadFile_Click" />
          <asp:SqlDataSource ID="dsConfigFiles" runat="server" ConnectionString="<%$ ConnectionStrings:CDDBConnectionString %>"
            FilterExpression="VersionID = '{0}'" SelectCommand="SELECT ImportedFiles.FileName, DDDVersions.VersionID FROM DDDVersions INNER JOIN ImportedFiles ON DDDVersions.VersionID = ImportedFiles.VersionID">
            <FilterParameters>
              <asp:ControlParameter ControlID="gvDDDVersions" Name="VersionID" PropertyName="SelectedValue" />
            </FilterParameters>
          </asp:SqlDataSource>
          <br />
          <br />
          <asp:Button ID="dbDeleteVersion" runat="server" ForeColor="Red" OnClick="dbDeleteVersion_Click"
            Text="Delete DDD Version" /><br />
        </asp:Panel>
        <br />
        <asp:Label ID="lbError" runat="server" EnableViewState="False" ForeColor="Red" Height="8px"
          Text="Label" Visible="False" Width="100%"></asp:Label><br />
        <br />
        <asp:Panel ID="panDeleteVersion" runat="server" EnableViewState="False" Height="50px"
          Visible="False" Width="100%">
          <asp:Label ID="lbDeleteWarning" runat="server" Font-Bold="True" Font-Names="Tahoma"
            ForeColor="Red" Text="Label" Width="100%"></asp:Label><br />
          <br />
          <asp:Button ID="btDeleteCancel" runat="server" Font-Bold="True" ForeColor="Red" OnClick="btDeleteCancel_Click1"
            Text="Cancel" />&nbsp; &nbsp;
          <asp:Button ID="btDeleteConfirm" runat="server" Font-Bold="True" ForeColor="Red"
            OnClick="btDeleteConfirm_Click" Text="Delete" /></asp:Panel>
      </td>
    </tr>
  </table>
</asp:Content>
