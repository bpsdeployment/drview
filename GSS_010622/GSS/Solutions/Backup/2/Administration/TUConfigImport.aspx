<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="TUConfigImport.aspx.cs"
  Inherits="Administration_TUConfigImport" Title="TU Configuration import" %>

<asp:Content ID="cntConfigImport" ContentPlaceHolderID="contentPH" runat="Server">
  <table style="width: 100%" cellspacing="5">
    <tr>
      <td>
        <h4>
          Telediagnostic Unit configuration import</h4>
      </td>
    </tr>
  </table>
  <table width="100%" cellspacing="5">
    <tr>
      <td style="width: 50%">
        <h5>
          DDD File</h5>
        <asp:FileUpload ID="fileUploadDDD" runat="server" Width="100%" /></td>
      <td style="width: 50%">
        <h5>
          Task files (zip archive)</h5>
        <asp:FileUpload ID="fileUploadTask" runat="server" Width="100%" /></td>
    </tr>
    <tr>
      <td style="width: 50%">
        <h5>
          Execution block libraries (zip archive)</h5>
        <asp:FileUpload ID="fileUploadBinaryLibraries" runat="server" Width="100%" /></td>
      <td style="width: 50%">
        <h5>
          Execution block source codes (zip archive)</h5>
        <asp:FileUpload ID="fileUploadSourceCodes" runat="server" Width="100%" /></td>
    </tr>
    <tr>
      <td style="width: 50%">
        <asp:Button ID="btUpload" runat="server" OnClick="btUpload_Click" Text="Upload files"
          Width="74px" />
        <asp:Button ID="btInsertToDB" runat="server" Text="Insert into DB" Enabled="False"
          EnableViewState="False" OnClick="btInsertToDB_Click" />
        <asp:Button ID="btSaveVersionInfo" runat="server" OnClick="btSaveVersionInfo_Click"
          Text="Save VersionInfo" Enabled="False" EnableViewState="False" /></td>
      <td style="width: 50%">
      </td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2" style="height: 100%">
        <h5>
          Import progress messages</h5>
        <asp:ListBox ID="lbImportMessages" runat="server" Width="100%" Height="250px" Font-Names="Courier New"
          ForeColor="Blue"></asp:ListBox></td>
    </tr>
    <tr>
    </tr>
  </table>
  <asp:Label ID="lbError" runat="server" EnableViewState="False" ForeColor="Red" Text="Label"
    Visible="False" Width="100%" Height="8px"></asp:Label>
</asp:Content>
