using System;
using System.Collections.Generic;
using System.Text;

namespace DDDObjects
{
  /// <summary>
  /// Structure holding binary data and additional information for parsing 
  /// of one record
  /// </summary>
  public struct sRecBinData
  {
    /// <summary>
    /// Identification of telediagnostic unit
    /// </summary>
    public Guid TUInstanceID;
    /// <summary>
    /// Identification of record instance
    /// </summary>
    public Int64 RecordInstID;
    /// <summary>
    /// Identification of DDD version
    /// </summary>
    public Guid DDDVersionID;
    /// <summary>
    /// Identification of record definition inside of DDD version
    /// </summary>
    public int RecordDefID;
    /// <summary>
    /// Record Start time
    /// </summary>
    public DateTime StartTime;
    /// <summary>
    /// Record end time (or DateTime.MinValue)
    /// </summary>
    public DateTime EndTime;
    /// <summary>
    /// Indicates byte order of binary data
    /// </summary>
    public bool BigEndian;
    /// <summary>
    /// Enviromental data in binary form
    /// </summary>
    public byte[] BinData;
  }

  /// <summary>
  /// Structure holding one parsed value of one elementary variable from one diagnostic record
  /// </summary>
  public struct sParsedVarValue
  {
    /// <summary>
    /// Ordinal index of record input
    /// </summary>
    public int InputIndex;
    /// <summary>
    /// ID of variable
    /// </summary>
    public int VariableID;
    /// <summary>
    /// Value timestamp
    /// </summary>
    public DateTime TimeStamp;
    /// <summary>
    /// Overall index of value for historized inputs
    /// </summary>
    public int ValueIndex;
    /// <summary>
    /// Index of array for historized inputs (0-BeforeStart, 1-PastStart...)
    /// </summary>
    public int ValueArrayIndex;
    /// <summary>
    /// Value of variable
    /// </summary>
    public Object Value;
  }

  /// <summary>
  /// Structure holding one parsed value of one referential attribute
  /// </summary>
  public struct sParsedRefAttrValue
  {
    /// <summary>
    /// ID of referential attribute
    /// </summary>
    public int RefAttrID;
    /// <summary>
    /// Value of referential attribute
    /// </summary>
    public Object Value;
  }

  /// <summary>
  /// Enumeration listing possible results of 
  /// binary data parsing operation
  /// </summary>
  public enum eParseStatus
  {
    /// <summary>
    /// record not parsed yet
    /// </summary>
    notParsed = 0,        
    /// <summary>
    /// record parsed successfully
    /// </summary>
    parseSuccessfull = 1, 
    /// <summary>
    /// no specific reason
    /// </summary>
    unknownReason = 2,    
    /// <summary>
    /// record type not defined in DDD Version
    /// </summary>
    unknownRecType = 3,   
    /// <summary>
    /// binary buffer is too small to contain variable value
    /// </summary>
    binaryBufferTooSmall = 5, 
    /// <summary>
    /// unspecified error occured in binary value conversion
    /// </summary>
    binValConversionErr = 6 
  }

  /// <summary>
  /// Definition of system referential attributes
  /// </summary>
  public enum eSystemRefAttributes
  {
    /// <summary>
    /// Latitude from GPS
    /// </summary>
    gpsLatitude = 6,
    /// <summary>
    /// Longitude from GPS
    /// </summary>
    gpsLongitude = 7
  }
}
