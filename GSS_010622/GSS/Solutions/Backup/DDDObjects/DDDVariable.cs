using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Globalization;

namespace DDDObjects
{
  /// <summary>
  /// Abstract class, represents one elementary or structured variable
  /// </summary>
  [Serializable]
  public class DDDVariable
  {
    /// <summary>
    /// ID of variable in DDD version
    /// </summary>
    public int VariableID;

    /// <summary>
    /// ID of parent variable
    /// </summary>
    public int OwnerVariableID;

    /// <summary>
    /// Index of variable inside parent variable
    /// </summary>
    public int OwnerVarIndex;

    /// <summary>
    /// Variable name
    /// </summary>
    public String Name;

    /// <summary>
    /// Identification of variable representation
    /// </summary>
    public int RepresID;

    /// <summary>
    /// Identification of top levele variable in structure
    /// </summary>
    public int TopLevelVarID;

    /// <summary>
    /// Level of nesting inside of structure
    /// </summary>
    public int NestLevel;

    /// <summary>
    /// Size of variable in bytes
    /// </summary>
    public int Size;

    /// <summary>
    /// Alignement of variable in bytes
    /// </summary>
    public int Alignement;

    /// <summary>
    /// Byte offset among parent strucutre
    /// </summary>
    public int ParentOffset;

    /// <summary>
    /// Byte offset among top level structure
    /// </summary>
    public int TopLevelOffset;

    /// <summary>
    /// User comment for variable
    /// </summary>
    public String Comment;

    /// <summary>
    /// Parent variable object
    /// </summary>
    public DDDStructure ParentVariable;

    /// <summary>
    /// Array of titles, one for each language
    /// </summary>
    private String[] titles;
    /// <summary>
    /// Title for currently selected language
    /// </summary>
    private String currentTitle;

    /// <summary>
    /// Variable title in selected language
    /// </summary>
    public String Title
    {
      get { return currentTitle; }
    }

    /// <summary>
    /// Default protected constructor
    /// </summary>
    protected DDDVariable()
    {
      titles = new String[DDDRepository.LanguageCount];
      currentTitle = "";
    }

    /// <summary>
    /// Creates new object from XML element
    /// </summary>
    /// <param name="varElement">Variable element from DDDObjects XML file</param>
    /// <param name="dddVer">DDDVersion object to which variable belongs</param>
    /// <returns>new DDDVariable object</returns>
    public static DDDVariable CreateFromDDDObjXML(XmlElement varElement, DDDVersion dddVer)
    {
      try
      {
        DDDVariable var;
        if (varElement.GetAttribute("RepresID") != "")
          var = new DDDElemVar(); //Variable is elementary
        else if (varElement.GetAttribute("Size") != "")
          var = new DDDStructure(); //Variable is structure
        else
          var = new DDDTriggerVar(); //Variable is trigger
        
        //Initialize variable
        var.InitializeFromDDDObjXML(varElement, dddVer);

        return var;
      }
      catch (Exception ex)
      {
        throw new DDDObjectException(ex, "Failed to create DDDVariable object");
      }
    }

    /// <summary>
    /// Initializes instance data from XML element
    /// </summary>
    /// <param name="varElement">xml element from DDDObjects document</param>
    /// <param name="dddVer">DDDVersion object to which variable belongs</param>
    protected virtual void InitializeFromDDDObjXML(XmlElement varElement, DDDVersion dddVer)
    {
      //Initialize values of all attributes
      String attribute;
      VariableID = Convert.ToInt32(varElement.GetAttribute("VariableID"));
      if ((attribute = varElement.GetAttribute("OwnerVariableID")) != "")
        OwnerVariableID = Convert.ToInt32(attribute);
      if ((attribute = varElement.GetAttribute("OwnerVarIndex")) != "")
        OwnerVarIndex = Convert.ToInt32(attribute);
      Name = varElement.GetAttribute("Name");
      if ((attribute = varElement.GetAttribute("RepresID")) != "")
        RepresID = Convert.ToInt32(attribute);
      TopLevelVarID = Convert.ToInt32(varElement.GetAttribute("TopLevelVarID"));
      NestLevel = Convert.ToInt32(varElement.GetAttribute("NestLevel"));
      if ((attribute = varElement.GetAttribute("Size")) != "")
        Size = Convert.ToInt32(attribute);
      if ((attribute = varElement.GetAttribute("Alignement")) != "")
        Alignement = Convert.ToInt32(attribute);
      if ((attribute = varElement.GetAttribute("ParentOffset")) != "")
        ParentOffset = Convert.ToInt32(attribute);
      if ((attribute = varElement.GetAttribute("TopLevelOffset")) != "")
        TopLevelOffset = Convert.ToInt32(attribute);
      Comment = varElement.GetAttribute("Comment");
      
      //Create array of titles
      //Select all title elements
      XmlNodeList titleElems = varElement.SelectNodes("./Title");
      Languages langID;
      String text;
      //Walk through and add to dicitonary each element
      foreach (XmlElement title in titleElems)
      {
        //Select attributes
        langID = (Languages)Convert.ToInt32(title.GetAttribute("LanguageID"));
        text = title.GetAttribute("Text");
        //Check language boundary
        if ((Int32)langID >= DDDRepository.LanguageCount)
          throw new DDDObjectException("Unknown language ID {0}", langID);
        //Add title to array
        titles[(Int32)langID] = text;
      }

      //Add to parent variable - if exists
      if ((OwnerVariableID != 0) && (OwnerVariableID != VariableID))
      { //Parent variable exists
        DDDStructure parent = dddVer.GetVariable(OwnerVariableID) as DDDStructure;
        parent.AddChild(this);
        ParentVariable = parent;
      }
      else
        ParentVariable = null;
    }

    /// <summary>
    /// Method selects all elementary variables inside of variable instance
    /// </summary>
    /// <returns>List of all elementary variables</returns>
    public virtual List<DDDElemVar> GetElementaryVars()
    {
      return new List<DDDElemVar>();
    }

    /// <summary>
    /// Method prepares language - dependent properties for selected language.
    /// </summary>
    /// <param name="currentLang">selected language</param>
    /// <param name="fallbackLang">language to use if selected language is not available</param>
    /// <returns>True if all properties are defined for specified language</returns>
    public virtual bool SetCurrentLanguage(Languages currentLang, Languages fallbackLang)
    {
      //check if specified language exists
      if (titles[(int)currentLang] != null)
      {
        currentTitle = titles[(int)currentLang];
        return true;
      }
      else if (titles[(int)fallbackLang] != null)
        currentTitle = titles[(int)fallbackLang];
      else if (titles[(int)Languages.Default] != null)
        currentTitle = titles[(int)Languages.Default];
      else
        currentTitle = Name;
      return false;
    }

    /// <summary>
    /// Obtains complete variable title consisting of titles of all parent variables, separated by "."
    /// </summary>
    /// <returns>complete variable title</returns>
    public String GetCompleteVarTitle()
    {
      String completeTit = this.Title;
      if (ParentVariable != null)
        completeTit = ParentVariable.GetCompleteVarTitle() + DDDHelper.TitleSeparator + completeTit;
      return completeTit;
    }

    /// <summary>
    /// Obtains complete variable title consisting of titles of all parent variables, separated by "."
    /// Appends name of bit specified by mask at the end, if bit exists
    /// </summary>
    /// <param name="mask">Mask of bit in bitset</param>
    /// <returns>Complete variable title</returns>
    public String GetCompleteVarTitle(Int64 mask)
    {
      String completeTit = GetCompleteVarTitle();
      if ((mask != 0) &&
           (this.GetType() == typeof(DDDElemVar)) &&
           (((DDDElemVar)this).Representation.GetType() == typeof(DDDBitsetRepres)))
      {
        DDDBitsetRepres bitsetRepres = ((DDDElemVar)this).Representation as DDDBitsetRepres;
        completeTit += DDDHelper.TitleSeparator + bitsetRepres.GetBitTitle(mask);
      }
      return completeTit;
    }

    /// <summary>
    /// Formats variable title according to parameters.
    /// </summary>
    /// <param name="complete">Create complete title including titles of all parents</param>
    /// <param name="mask">Sprcifies bit in bitset</param>
    /// <param name="unit">Include unit symbol in square brackets</param>
    /// <returns>Formatted title</returns>
    public String FormatVarTitle(bool complete, Int64 mask, bool unit)
    {
      String title = "";
      if (complete)
        title = GetCompleteVarTitle(mask);
      else
      { //Simple title
        if (mask == 0)
          title = Title;
        else if ((this.GetType() == typeof(DDDElemVar)) &&
                  (((DDDElemVar)this).Representation.GetType() == typeof(DDDBitsetRepres)))
        {
          DDDBitsetRepres bitsetRepres = ((DDDElemVar)this).Representation as DDDBitsetRepres;
          title = bitsetRepres.GetBitTitle(mask);
        }
      }
      if (unit &&
          (this.GetType() == typeof(DDDElemVar)) &&
          (((DDDElemVar)this).Representation.GetType() == typeof(DDDSimpleRepres)))
      {
        DDDSimpleRepres simpleRepres = ((DDDElemVar)this).Representation as DDDSimpleRepres;
        if (simpleRepres.UnitSymbol != "")
          title += " [" + simpleRepres.UnitSymbol + "]";
      }
      return title;
    }

    /// <summary>
    /// Method returns true if variable is elementary bitset variable
    /// </summary>
    /// <returns>True if variable is bitset</returns>
    public bool  IsBitSet()
    {
      return ( (this.GetType() == typeof(DDDElemVar)) && 
               (((DDDElemVar)this).Representation.GetType() == typeof(DDDBitsetRepres)) );
    }

    /// <summary>
    /// Returns unique string identification of specified variable
    /// </summary>
    /// <param name="TUInstanceID">Identification of TU Instance</param>
    /// <param name="DDDVersionID">identification of DDD version</param>
    /// <param name="VariableID">identification of variable</param>
    /// <returns>String uniquely identifying variable</returns>
    public static String GetVarIDString(Guid TUInstanceID, Guid DDDVersionID, int VariableID)
    {
      return TUInstanceID.ToString() + "." + DDDVersionID.ToString() + "." + VariableID.ToString("00000"); ;
    }
  }

  /// <summary>
  /// Represents structured variable
  /// </summary>
  [Serializable]
  public class DDDStructure : DDDVariable
  {
    /// <summary>
    /// List of children variables
    /// </summary>
    public List<DDDVariable> ChildVars;

    /// <summary>
    /// Standard public constructor
    /// </summary>
    public DDDStructure()
    {
      ChildVars = new List<DDDVariable>();
    }

    /// <summary>
    /// Adds child variable to structure
    /// </summary>
    /// <param name="var">Child variable (elementary or structure) to add</param>
    public void AddChild(DDDVariable var)
    {
      ChildVars.Add(var);
    }

    /// <summary>
    /// Method selects all elementary variables inside of variable instance
    /// </summary>
    /// <returns>List of all elementary variables</returns>
    public override List<DDDElemVar> GetElementaryVars()
    {
      List<DDDElemVar> elemVars = new List<DDDElemVar>();
      foreach (DDDVariable var in ChildVars)
        elemVars.AddRange(var.GetElementaryVars());
      return elemVars;
    }
  }

  /// <summary>
  /// Represents elementary variable
  /// </summary>
  [Serializable]
  public class DDDElemVar : DDDVariable
  {
    /// <summary>
    /// Representation associated with variable
    /// </summary>
    public DDDRepresentation Representation;

    /// <summary>
    /// Standard public constructor
    /// </summary>
    public DDDElemVar()
    {
    }

    /// <summary>
    /// Initializes instance data from XML element
    /// </summary>
    /// <param name="varElement">xml element from DDDObjects document</param>
    /// <param name="dddVer">DDDVersion object to which variable belongs</param>
    protected override void InitializeFromDDDObjXML(XmlElement varElement, DDDVersion dddVer)
    {
      //Call parent initialization
      base.InitializeFromDDDObjXML(varElement, dddVer);

      //Obtain correct representation 
      Representation = dddVer.GetRepresentation(RepresID);
    }

    /// <summary>
    /// Method selects all elementary variables inside of variable instance
    /// </summary>
    /// <returns>List of all elementary variables</returns>
    public override List<DDDElemVar> GetElementaryVars()
    {
      List<DDDElemVar> elemVars = new List<DDDElemVar>();
      elemVars.Add(this);
      return elemVars;
    }

    /// <summary>
    /// Returns unit symbol defined for variable or empty string
    /// </summary>
    /// <returns>Physical unit symbol for variable or empty string.</returns>
    public String GetUnitSymbol()
    {
      if (Representation.GetType() == typeof(DDDSimpleRepres))
        return ((DDDSimpleRepres)Representation).UnitSymbol;
      else
        return "";
    }

    /// <summary>
    /// Method returns string with formated numerical value according to associated representation.
    /// If variable is associated with bitset representation, method returns value formated as series of 1s and 0s.
    /// </summary>
    /// <param name="value">Value to format. Value has to be convertible to elementary type of this variable.</param>
    /// <returns>Value formatted according to representation</returns>
    public String FormatValue(Object value)
    {
      /*
      if (value.GetType() == typeof(String))
      {
        String strVal = (String)value;
        strVal = strVal.Replace(".", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator);
        strVal = strVal.Replace(",", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator);
        return Representation.FormatValue(strVal);
      }
      */
      return Representation.FormatValue(value);
    }

    /// <summary>
    /// Parses part of binary data block which represents value of variable.
    /// Parsed value is added to parsedValues list
    /// </summary>
    /// <param name="recBinData">Structure with binary data and record instance information</param>
    /// <param name="parsedValues">List into which parsed value is added</param>
    /// <param name="dataOffset">Offset of variable in binary block. Offset is expected to have correct alignement</param>
    /// <param name="inputIndex">Index of input into which variable belongs</param>
    /// <param name="timeStamp">TimeStamp for variable value</param>
    /// <param name="valueIndex">Overall index in array of input's samples</param>
    /// <param name="valueArrayIndex">Identification of one of four historized input's arrays</param>
    /// <returns>Result of parsing operation</returns>
    public eParseStatus ParseBinData(sRecBinData recBinData, ref List<sParsedVarValue> parsedValues, int dataOffset,
                                              int inputIndex, DateTime timeStamp, int valueIndex, int valueArrayIndex)
    {
      eParseStatus parseRes;
      //Create new parsed value structure
      sParsedVarValue parsedVal = new sParsedVarValue();      
      //Fill given parameters
      parsedVal.InputIndex = inputIndex;
      parsedVal.TimeStamp = timeStamp;
      parsedVal.ValueIndex = valueIndex;
      parsedVal.ValueArrayIndex = valueArrayIndex;
      parsedVal.VariableID = VariableID;
      //Obtain parsed value
      try
      {
        parseRes = Representation.ParseBinValue(recBinData, dataOffset + TopLevelOffset, out parsedVal.Value);
      }
      catch (Exception)
      { return eParseStatus.binValConversionErr; }
      //Add parsed value to list
      parsedValues.Add(parsedVal);
      return parseRes;
    }

    /// <summary>
    /// Parses part of binary data block which represents value of variable.
    /// Value is used for referential attributes.
    /// Only values convertible to floats are used.
    /// Divisor constant is applied to the value after parsing.
    /// In case of value, which is not convertible to float, NotParsed result is returned and parsedVal is assigned NaN value.
    /// </summary>
    /// <param name="recBinData">Structure with binary data and record instance information</param>
    /// <param name="dataOffset">Offset of variable in binary block. Offset is expected to have correct alignement</param>
    /// <param name="parsedVal">Output parsed float value with applied divisor constant.</param>
    /// <returns>Result of parsing operation</returns>
    public eParseStatus ParseRefAttrData(sRecBinData recBinData, int dataOffset, out float parsedVal)
    {
      eParseStatus parseRes;
      Object value;
      parsedVal = Single.NaN;
      //Obtain parsed value
      try
      {
        if (Representation.GetType() == typeof(DDDBinaryRepres))
          return eParseStatus.notParsed; //No sense parsing binary representation - result is not convertible to float
        parseRes = Representation.ParseBinValue(recBinData, dataOffset + TopLevelOffset, out value);
        if (parseRes == eParseStatus.parseSuccessfull)
        {
          parsedVal = Convert.ToSingle(value); //Convert value to float
          if (Representation.GetType() == typeof(DDDSimpleRepres))
          { //For simple representation, apply the divisor
            DDDSimpleRepres rep = (DDDSimpleRepres)Representation;
            if (rep.Divisor != 0)
              parsedVal = (float)(parsedVal / rep.Divisor);
            parsedVal += (float)rep.Bias;
          }
        }
      }
      catch (InvalidCastException)
      { return eParseStatus.notParsed; }
      catch (Exception)
      { return eParseStatus.binValConversionErr; }
      return parseRes;
    }

    /// <summary>
    /// Scales given variable value using a divisor defined for the representation (SimpleRepres)
    /// or returns original value in case of other representations.
    /// In case of scaling, when divisor is not 1 or 0, result is returned as double
    /// </summary>
    /// <param name="value">Value to scale</param>
    /// <returns>Scaled value as double or original object</returns>
    public virtual Object ScaleValue(Object value)
    {
      return Representation.ScaleValue(value); //Scaled variable value
    }
  }

  /// <summary>
  /// Represents trigger variable
  /// </summary>
  [Serializable]
  public class DDDTriggerVar : DDDVariable
  {
    /// <summary>
    /// Standard public constructor
    /// </summary>
    public DDDTriggerVar()
    {
    }
  }
}
