using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization;
using FleetHierarchies;

namespace DDDObjects
{
  /// <summary>
  /// Base class providing the posibility to read and write diagnostic records and DDD objects
  /// to (from) GZIPed stream. Virtual class, must be overriden.
  /// </summary>
  public class DRSerializerBase
  {
    /// <summary>
    /// Class used to hold one instance of diagnostic record
    /// </summary>
    [Serializable]
    public class DiagnosticRecord
    {
      /// <summary>
      /// Identifier of telediagnostic unit
      /// </summary>
      public Guid TUInstanceID;
      /// <summary>
      /// Identifier of record instance
      /// </summary>
      public long RecordInstID;
      /// <summary>
      /// Identifier of DDD Version
      /// </summary>
      public Guid DDDVersionID;
      /// <summary>
      /// Identifier of record definition inside of DDD
      /// </summary>
      public int RecordDefID;
      /// <summary>
      /// Record Strt time
      /// </summary>
      public DateTime StartTime;
      /// <summary>
      /// Record End time
      /// </summary>
      public DateTime EndTime;
      /// <summary>
      /// Time of last record modification in TU
      /// </summary>
      public DateTime LastModified;
      /// <summary>
      /// Status of record parsing operation
      /// </summary>
      public int ParseStatusID;
      /// <summary>
      /// Flag of byte order (endianes)
      /// </summary>
      public bool BigEndian;
      /// <summary>
      /// Identifier of source device for record
      /// </summary>
      public String Source;
      /// <summary>
      /// ID of fleet hierarchy item associated with this record
      /// </summary>
      public int HierarchyItemID;
      /// <summary>
      /// Time of the import of DR into DB
      /// </summary>
      public DateTime ImportTime;
      /// <summary>
      /// Time, when record was acknowledged by the driver
      /// </summary>
      public DateTime AcknowledgeTime;
      /// <summary>
      /// Number of the vehicle in trainset from which record has originated
      /// </summary>
      public Int16 VehicleNumber;
      /// <summary>
      /// Code of the device which produced the record
      /// </summary>
      public Int16 DeviceCode;
      /// <summary>
      /// Enviromental data in binary form
      /// </summary>
      public Object BinData;
    }

    #region Protected members
    /// <summary>
    /// Helper class for DDDObjects
    /// </summary>
    protected DDDHelper DDDHelper;
    /// <summary>
    /// Stream for input/output operations
    /// </summary>
    protected Stream stream;
    /// <summary>
    /// Compressed stream opened over given stream.
    /// </summary>
    protected GZipStream zipStream;
    /// <summary>
    /// Dictionary used for storing used TUInstance objects
    /// </summary>
    protected Dictionary<Guid, TUInstance> usedTUInst = new Dictionary<Guid, TUInstance>();
    /// <summary>
    /// Dictionary used for storing used DDD Versions objects
    /// </summary>
    protected Dictionary<Guid, DDDVersion> usedDDDVer = new Dictionary<Guid, DDDVersion>();
    /// <summary>
    /// Dictionary storing used fleet hierarchies
    /// </summary>
    protected Dictionary<int, FleetHierarchy> usedHierVersions = new Dictionary<int, FleetHierarchy>();
    #endregion //Protected members

    #region Construction, initialization
    /// <summary>
    /// Creates new diagnostic record serializer object
    /// </summary>
    /// <param name="dddHelper">Helper class for DDD objects</param>
    /// <param name="stream">Stream to read from or write to</param>
    /// <param name="output">Indicates that records will be written into the stream</param>
    protected DRSerializerBase(DDDHelper dddHelper, Stream stream, bool output)
    {
      this.DDDHelper = dddHelper;
      this.stream = stream;
      CompressionMode compMode = output ? CompressionMode.Compress : CompressionMode.Decompress;
      this.zipStream = new GZipStream(stream, compMode, true);
    }
    #endregion //Construction, initialization

    #region Public methods
    /// <summary>
    /// Writes given record into the stream, registers used DDDVersion and TUInstance
    /// </summary>
    /// <param name="record">Record to write</param>
    public virtual void WriteRecordToStream(DiagnosticRecord record)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Writes all used DDD objects into the stream preceded by special flag
    /// </summary>
    public virtual void WriteUsedDDDObjects()
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Writes record created from given parameters into the stream
    /// </summary>
    /// <param name="TUInstanceID">Identifier of telediagnostic unit</param>
    /// <param name="RecordInstID">Identifier of record instance</param>
    /// <param name="DDDVersionID">Identifier of DDD version</param>
    /// <param name="RecordDefID">Identifier of record definition inside of DDD</param>
    /// <param name="StartTime">Record StartTime</param>
    /// <param name="EndTime">Record EndTime (or DateTime.MinValue)</param>
    /// <param name="LastModified">Time of last record modification in TU</param>
    /// <param name="ParseStatusID">Result status of parsing operation</param>
    /// <param name="BigEndian">Byte order flag for binary data</param>
    /// <param name="Source">Identifier of record source in vehicle</param>
    /// <param name="HierarchyItemID">ID of associated fleet hierarchy item</param>
    /// <param name="ImportTime">Time of record import into DB</param>
    /// <param name="AcknowledgeTime">Time of record acknowledge by the driver</param>
    /// <param name="VehicleNumber">Number of vehicle where record was produced</param>
    /// <param name="DeviceCode">Code of device which produced the record</param>
    /// <param name="BinData">Environmental data in binary form</param>
    public void WriteRecordToStream(Guid TUInstanceID, long RecordInstID, Guid DDDVersionID, int RecordDefID,
                                    DateTime StartTime, DateTime EndTime, DateTime LastModified, int ParseStatusID,
                                    bool BigEndian, String Source, int HierarchyItemID,
                                    DateTime ImportTime, DateTime AcknowledgeTime, Int16 VehicleNumber, Int16 DeviceCode,
                                    Object BinData)
    {
      DiagnosticRecord rec = new DiagnosticRecord();
      rec.TUInstanceID = TUInstanceID; rec.RecordInstID = RecordInstID; rec.DDDVersionID = DDDVersionID; rec.RecordDefID = RecordDefID;
      rec.StartTime = StartTime; rec.EndTime = EndTime; rec.LastModified = LastModified; rec.ParseStatusID = ParseStatusID;
      rec.BigEndian = BigEndian; rec.Source = Source; rec.HierarchyItemID = HierarchyItemID;
      rec.ImportTime = ImportTime; rec.AcknowledgeTime = AcknowledgeTime; rec.VehicleNumber = VehicleNumber; rec.DeviceCode = DeviceCode;
      rec.BinData = BinData;
      WriteRecordToStream(rec);
    }

    /// <summary>
    /// Deserializes next record from given stream
    /// </summary>
    /// <returns>Deserialized diagnostic record</returns>
    public virtual DiagnosticRecord ReadRecordFromStream()
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Reads stored DDD objects from stream. In case not all necessary objects are available in the stream, exception is thrown.
    /// Objects are stored in DDDHelper class.
    /// </summary>
    public virtual void ReadUsedDDDObjects()
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Closes compressed stream created over the passed stream object
    /// </summary>
    public void CloseZipStream()
    {
      zipStream.Flush();
      zipStream.Close();
    }
    #endregion //Public methods

    #region Protected helper methods
    /// <summary>
    /// Checks if all used DDDObjects in read records are available in DDDHelper.
    /// If not, exception is thrown.
    /// </summary>
    protected void CheckUsedDDDObjAvailable()
    {
      try
      {
        //Check if all necessary DDD objects are available
        foreach (KeyValuePair<Guid, DDDVersion> verPair in usedDDDVer)
          DDDHelper.GetVersion(verPair.Key);
        foreach (KeyValuePair<Guid, TUInstance> tuPair in usedTUInst)
          DDDHelper.GetTUInstance(tuPair.Key);
        //Hierarchy versions are not checked
      }
      catch (Exception)
      {
        throw new Exception("Some DDD objects are not available");
      }
    }

    /// <summary>
    /// Stores used DDD objects for given record.
    /// Directories with stored objects can be later used to either check if all necessary versions are available or to write used objects into the stream.
    /// </summary>
    /// <param name="record">Record referencing used objects</param>
    /// <param name="identifiersOnly">Only identifiers, not objects references are stored (used for reading of records)</param>
    protected void RememberUsedDDDObj(DiagnosticRecord record, bool identifiersOnly)
    {
      FleetHierarchy hierarchy = DDDHelper.HierarchyHelper.GetHierarchy(record.StartTime);
      if (identifiersOnly)
      {
        //Remember used version and TUInst 
        if (!usedDDDVer.ContainsKey(record.DDDVersionID))
          usedDDDVer.Add(record.DDDVersionID, null);
        if (!usedTUInst.ContainsKey(record.TUInstanceID))
          usedTUInst.Add(record.TUInstanceID, null);
        if ((hierarchy != null) && !usedHierVersions.ContainsKey(hierarchy.VersionID))
          usedHierVersions.Add(hierarchy.VersionID, null);
      }
      else
      {
        //Remember used version and TUInst 
        if (!usedDDDVer.ContainsKey(record.DDDVersionID))
          usedDDDVer.Add(record.DDDVersionID, DDDHelper.GetVersion(record.DDDVersionID));
        if (!usedTUInst.ContainsKey(record.TUInstanceID))
          usedTUInst.Add(record.TUInstanceID, DDDHelper.GetTUInstance(record.TUInstanceID));
        if ((hierarchy != null) && !usedHierVersions.ContainsKey(hierarchy.VersionID))
          usedHierVersions.Add(hierarchy.VersionID, hierarchy);
      }
    }
    #endregion //Protected helper methods
  }
}
