using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace DDDObjects
{
  /// <summary>
  /// Exception class thrown by all DDD objects
  /// </summary>
  class DDDObjectException : Exception
  {
    public DDDObjectException(String message, params Object[] args) 
      : base(String.Format(message, args))
    {}
    public DDDObjectException(Exception inner, String message, params Object[] args)
      : base(String.Format(message, args), inner)
    { }
  }

  /// <summary>
  /// Enumeration of all available languages
  /// </summary>
  public enum Languages
  {
    /// <summary>
    /// Default system language
    /// </summary>
    Default = 0,
    /// <summary>
    /// English language
    /// </summary>
    english = 1,
    /// <summary>
    /// Italian language
    /// </summary>
    italian = 2,
    /// <summary>
    /// German language
    /// </summary>
    german = 3,
    /// <summary>
    /// Czech language
    /// </summary>
    czech = 4
  }

  /// <summary>
  /// Enumeration representing all available elementary data types
  /// </summary>
  public enum ElemDataType
  {
    /// <summary>
    /// Boolean data (TRUE / FALSE), 1 byte
    /// </summary>
    BIT = 1,
    /// <summary>
    /// Signed integer, 1 byte
    /// </summary>
    I8 = 2,
    /// <summary>
    /// Signed integer, 2 bytes
    /// </summary>
    I16 = 3,
    /// <summary>
    /// Signed integer, 4 bytes
    /// </summary>
    I32 = 4,
    /// <summary>
    /// Signed integer, 8 bytes
    /// </summary>
    I64 = 5,
    /// <summary>
    /// Unsigned integer, 1 byte
    /// </summary>
    U8 = 6,
    /// <summary>
    /// Unsigned integer, 2 bytes
    /// </summary>
    U16 = 7,
    /// <summary>
    /// Unsigned integer, 4 bytes
    /// </summary>
    U32 = 8,
    /// <summary>
    /// Unsigned integer, 8 bytes
    /// </summary>
    U64 = 9,
    /// <summary>
    /// Floating point number, 4 bytes
    /// </summary>
    R32 = 10,
    /// <summary>
    /// Floating point number, 8 bytes
    /// </summary>
    R64 = 11
  }

  /// <summary>
  /// Contains dictionary of DDDVersion objects (Trees of DDD objects)
  /// </summary>
  public class DDDRepository
  {
    /// <summary>
    /// Number of languages defined in the system
    /// </summary>
    public const int LanguageCount = 5;
    private Dictionary<Guid, DDDVersion> dddVersions;

    /// <summary>
    /// Dictionary of available DDD versions
    /// </summary>
    public Dictionary<Guid, DDDVersion> VersionDictionary
    {
      get { return dddVersions; }
    }

    /// <summary>
    /// Returns bit size of specified elementary data type
    /// </summary>
    /// <param name="elemType">Elementary data type</param>
    /// <returns>Bit size</returns>
    public static int GetElemTypeBitLength(ElemDataType elemType)
    {
      switch (elemType)
      {
        case ElemDataType.BIT:
          return 1;
        case ElemDataType.U8:
        case ElemDataType.I8:
          return 8;
        case ElemDataType.U16:
        case ElemDataType.I16:
          return 16;
        case ElemDataType.U32:
        case ElemDataType.I32:
        case ElemDataType.R32:
          return 32;
        case ElemDataType.U64:
        case ElemDataType.I64:
        case ElemDataType.R64:
          return 64;
      }
      return 0;
    }

    /// <summary>
    /// Returns byte size of elementary data type
    /// </summary>
    /// <param name="elemType">Elementary data type</param>
    /// <returns>Byte size</returns>
    public static int GetElemTypeByteLength(ElemDataType elemType)
    {
      switch (elemType)
      {
        case ElemDataType.BIT:
        case ElemDataType.U8:
        case ElemDataType.I8:
          return 1;
        case ElemDataType.U16:
        case ElemDataType.I16:
          return 2;
        case ElemDataType.U32:
        case ElemDataType.I32:
        case ElemDataType.R32:
          return 4;
        case ElemDataType.U64:
        case ElemDataType.I64:
        case ElemDataType.R64:
          return 8;
      }
      return 0;
    }

    /// <summary>
    /// Default public constructor
    /// </summary>
    public DDDRepository()
    {
      dddVersions = new Dictionary<Guid, DDDVersion>();
    }

    /// <summary>
    /// Creates new tree of objects representing complete DDD version (including records, variables...)
    /// from DDDObjects XML document
    /// </summary>
    /// <param name="DDDObjdDoc">DDDObjects XML document containing desription of all objects from one DDD version</param>
    /// <returns>DDDVersion object representing the root of object hierarchy</returns>
    public static DDDVersion CreateObjectsFromDDDObjXML(XmlDocument DDDObjdDoc)
    {
      XmlNodeList elemList;
      XmlElement elem;

      //Create DDDVersion object
      DDDVersion version;
      elem = (XmlElement)DDDObjdDoc.SelectSingleNode("//DDDVersion");
      version = DDDVersion.CreateFromDDDObjXML(elem);
      //Add referential attributes mapping
      version.ReadRefAttrMappings(DDDObjdDoc);
      //Read device codes mapping
      elemList = DDDObjdDoc.SelectNodes("//DeviceCode");
      foreach (XmlElement devCodeElem in elemList)
      {
        int deviceCode = Convert.ToInt32(devCodeElem.GetAttribute("Code"));
        String deviceName = devCodeElem.GetAttribute("Name");
        version.AddDeviceCode(deviceCode, deviceName);
      }
      //Create Simple Representation objects
      DDDSimpleRepres simpleRepres;
      elemList = DDDObjdDoc.SelectNodes("//SimpleRepres");
      foreach(XmlElement simpleRepElem in elemList)
      {
        simpleRepres = DDDSimpleRepres.CreateFromDDDObjXML(simpleRepElem);
        version.AddRepresentation(simpleRepres.RepresID, simpleRepres);
      }
      //
      //Create Binary Representation objects
      DDDBinaryRepres binaryRepres;
      elemList = DDDObjdDoc.SelectNodes("//BinaryRepres");
      foreach (XmlElement binaryRepElem in elemList)
      {
        binaryRepres = DDDBinaryRepres.CreateFromDDDObjXML(binaryRepElem);
        version.AddRepresentation(binaryRepres.RepresID, binaryRepres);
      }
      //Create Enum Representation objects
      DDDEnumRepres enumRepres;
      elemList = DDDObjdDoc.SelectNodes("//EnumRepres");
      foreach (XmlElement enumRepElem in elemList)
      {
        enumRepres = DDDEnumRepres.CreateFromDDDObjXML(enumRepElem);
        version.AddRepresentation(enumRepres.RepresID, enumRepres);
      }
      //Create Bitset Representation objects
      DDDBitsetRepres bitsetRepres;
      elemList = DDDObjdDoc.SelectNodes("//BitsetRepres");
      foreach (XmlElement bitsetRepElem in elemList)
      {
        bitsetRepres = DDDBitsetRepres.CreateFromDDDObjXML(bitsetRepElem);
        version.AddRepresentation(bitsetRepres.RepresID, bitsetRepres);
      }
      //Create Variable objects
      DDDVariable variable;
      elemList = DDDObjdDoc.SelectNodes("//Variable");
      foreach (XmlElement variableElem in elemList)
      {
        variable = DDDVariable.CreateFromDDDObjXML(variableElem, version);
        version.AddVariable(variable.VariableID, variable);
      }
      //Create record objects
      DDDRecord record;
      elemList = DDDObjdDoc.SelectNodes("//Record");
      foreach (XmlElement recordElem in elemList)
      {
        record = DDDRecord.CreateFromDDDObjXML(recordElem, version);
        version.AddRecord(record.RecordDefID, record);
      }
      //Create input objects
      DDDInput input;
      elemList = DDDObjdDoc.SelectNodes("//Input");
      foreach (XmlElement inputElem in elemList)
        input = DDDInput.CreateFromDDDObjXML(inputElem, version);

      //Find mappings of elementary variables to referential attributes for this version
      version.FindRefAttrMappings();

      return version;
    }

    /// <summary>
    /// Creates new tree of objects representing complete DDD version (including records, variables...)
    /// from DDD XML document.
    /// This tree of objects may differ from tree created from DDDObjects document obtained from DB.
    /// </summary>
    /// <param name="DDDDoc">DDD XML document in original form</param>
    /// <param name="DDDVersionID">Identification of DDD version</param>
    /// <returns>DDDVersion object representing the root of object hierarchy</returns>
    public static DDDVersion CreateObjectsFromDDDXML(XmlDocument DDDDoc, Guid DDDVersionID)
    {
      try
      {
        //Create transfom object
        DDDTransform transform = new DDDTransform();
        //Transform the document to ddd objects document
        XmlDocument dddObjects = transform.TransformDDD(DDDVersionID, DDDDoc);
        //Create DDDVersion
        DDDVersion version = DDDRepository.CreateObjectsFromDDDObjXML(dddObjects);
        //Return the version
        return version;
      }
      catch (Exception ex)
      {
        throw new DDDObjectException(ex, "Failed to parse DDD document");
      }
    }

    /// <summary>
    /// Adds new DDDVersion object to the repository
    /// </summary>
    /// <param name="version">Object to add</param>
    public void AddVersion(DDDVersion version)
    {
      DDDVersion tmpVersion;
      //Check if version already exists
      if (dddVersions.TryGetValue(version.VersionID, out tmpVersion))
        dddVersions.Remove(version.VersionID); //Remove old version
      dddVersions.Add(version.VersionID, version);
    }
    /// <summary>
    /// Retrieves DDD Version object from repository
    /// </summary>
    /// <param name="ID">ID of the version</param>
    /// <returns>DDDVersion object with specified ID</returns>
    public DDDVersion GetVersion(Guid ID)
    {
      DDDVersion version;
      if (dddVersions.TryGetValue(ID, out version))
        return version;
      else
        throw new DDDObjectException("Version with ID {0} does not exist", ID);
    }

    /// <summary>
    /// Method prepares language - dependent properties for selected language.
    /// </summary>
    /// <param name="currentLang">selected language</param>
    /// <param name="fallbackLang">language to use if selected language is not available</param>
    /// <returns>True if all properties are defined for specified language</returns>
    public bool SetCurrentLanguage(Languages currentLang, Languages fallbackLang)
    {
      bool result = true;
      //Set language for all DDDVersions
      foreach (KeyValuePair<Guid, DDDVersion> version in dddVersions)
        result &= version.Value.SetCurrentLanguage(currentLang, fallbackLang);
      return result;
    }
  }


}
