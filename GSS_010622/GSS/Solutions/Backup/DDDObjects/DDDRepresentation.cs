using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Xml;
using System.Globalization;

namespace DDDObjects
{
  /// <summary>
  /// Parent virtual class for all representation classes.
  /// </summary>
  [Serializable]
  public class DDDRepresentation
  {
    /// <summary>
    /// Name of the representation
    /// </summary>
    public String Name;

    /// <summary>
    /// Data type associated with the representation
    /// </summary>
    public ElemDataType DataType;

    /// <summary>
    /// Object representing associated data type
    /// </summary>
    public DDDTypeBase DataTypeObj;

    /// <summary>
    /// Identification of the representation
    /// </summary>
    public int RepresID;

    /// <summary>
    /// User comment
    /// </summary>
    public String Comment;

    /// <summary>
    /// Default private constructor
    /// </summary>
    protected DDDRepresentation()
    {
    }

    /// <summary>
    /// Initializes instance data from XML element
    /// </summary>
    /// <param name="repElement">xml element from DDDObjects document</param>
    protected virtual void InitializeFromDDDObjXML(XmlElement repElement)
    {
      Name = repElement.GetAttribute("Name");
      DataType = (ElemDataType)Convert.ToInt32(repElement.GetAttribute("DataTypeID"));
      RepresID = Convert.ToInt32(repElement.GetAttribute("RepresID"));
      Comment = repElement.GetAttribute("Comment");
      DataTypeObj = DDDRepresentation.CreateDDDType(DataType);
    }

    /// <summary>
    /// Method prepares language - dependent properties for selected language.
    /// </summary>
    /// <param name="currentLang">selected language</param>
    /// <param name="fallbackLang">language to use if selected language is not available</param>
    /// <returns>True if all properties are defined for specified language</returns>
    public virtual bool SetCurrentLanguage(Languages currentLang, Languages fallbackLang)
    {
      return true;
    }

    /// <summary>
    /// Method returns string with formated numerical value.
    /// </summary>
    /// <param name="value">Value to format. Value has to be convertible to elementary type of this representation.</param>
    /// <returns>Value formatted according to format string</returns>
    public virtual String FormatValue(Object value)
    {
      return value.ToString();
    }

    /// <summary>
    /// Returns array of Mask - Bit value pairs for specified value and current language
    /// </summary>
    /// <param name="value">Value of variable. Value has to be convertible to Int64 data type</param>
    /// <returns>array of mask-value pairs</returns>
    public virtual KeyValuePair<Int64, String>[] GetMasksAndValues(Object value)
    {
      //Dummy implementation
      return new KeyValuePair<long, string>[0];
    }

    /// <summary>
    /// Cretes object representing elementary data type
    /// </summary>
    /// <param name="dataType">Elementary data type</param>
    /// <returns>DDDType object for specified elem. data type</returns>
    public static DDDTypeBase CreateDDDType(ElemDataType dataType)
    {
      switch (dataType)
      {
        case ElemDataType.BIT:
          return new DDDTypeBOOL();
        case ElemDataType.I8:
          return new DDDTypeI8();
        case ElemDataType.I16:
          return new DDDTypeI16();
        case ElemDataType.I32:
          return new DDDTypeI32();
        case ElemDataType.I64:
          return new DDDTypeI64();
        case ElemDataType.U8:
          return new DDDTypeU8();
        case ElemDataType.U16:
          return new DDDTypeU16();
        case ElemDataType.U32:
          return new DDDTypeU32();
        case ElemDataType.U64:
          return new DDDTypeU64();
        case ElemDataType.R32:
          return new DDDTypeR32();
        case ElemDataType.R64:
          return new DDDTypeR64();
      }
      throw new DDDObjectException("Unkonwn elementary data type {0}", dataType);
    }

    /// <summary>
    /// Extracts value as correctly typed object from binary buffer at given location.
    /// </summary>
    /// <param name="recBinData">Structure with binary data and record instance information.</param>
    /// <param name="dataOffset">Offset to binary buffer.</param>
    /// <param name="value">Output parameter containing extracted value.</param>
    /// <returns>Status of parsing operation</returns>
    public virtual eParseStatus ParseBinValue(sRecBinData recBinData, int dataOffset, out Object value)
    {
      //Call DataType implementation of ParseBinValue method
      return DataTypeObj.ParseBinValue(recBinData, dataOffset, out value);
    }

    /// <summary>
    /// Scales given value using a divisor defined for the representation (SimpleRepres)
    /// or returns original value in case of other representations.
    /// In case of scaling, when divisor is not 1 or 0, result is returned as double
    /// </summary>
    /// <param name="value">Value to scale</param>
    /// <returns>Scaled value as double or original object</returns>
    public virtual Object ScaleValue(Object value)
    {
      return value; //Return original value
    }
  }

  /// <summary>
  /// Simple representation of numeric data type (formatted numerical value with optional unit symbol)
  /// </summary>
  [Serializable]
  public class DDDSimpleRepres : DDDRepresentation
  {
    /// <summary>
    /// Physical units symbol
    /// </summary>
    public String UnitSymbol;

    /// <summary>
    /// Divisor constant - applied to value before formatting
    /// </summary>
    public Double Divisor;

    /// <summary>
    /// Bias constant - added to value before formatting
    /// </summary>
    public Double Bias;

    /// <summary>
    /// Format string
    /// </summary>
    public String Format;

    /// <summary>
    /// Protected constructor
    /// </summary>
    protected DDDSimpleRepres()
    {
    }

    /// <summary>
    /// Creates and initializes new simple representation object from XML element
    /// </summary>
    /// <param name="repElement">XML element describing the representation</param>
    /// <returns>simple representation object</returns>
    public static DDDSimpleRepres CreateFromDDDObjXML(XmlElement repElement)
    {
      try
      {
        DDDSimpleRepres rep = new DDDSimpleRepres();
        rep.InitializeFromDDDObjXML(repElement);
        return rep;
      }
      catch (Exception ex)
      {
        throw new DDDObjectException(ex, "Failed to create simple representation object");
      }
    }

    /// <summary>
    /// Initializes instance data from XML element
    /// </summary>
    /// <param name="repElement">xml element from DDDObjects document</param>
    protected override void InitializeFromDDDObjXML(XmlElement repElement)
    {
      //Call base class initialization
      base.InitializeFromDDDObjXML(repElement);
      String attribute;
      UnitSymbol = repElement.GetAttribute("UnitSymbol");
      //Divisor parameter
      Divisor = 0;
      if ((attribute = repElement.GetAttribute("Divisor")) != "")
      {
        attribute = attribute.Replace(".", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator);
        attribute = attribute.Replace(",", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator);
        Divisor = Convert.ToDouble(attribute);
      }
      if (Divisor == 0)
        Divisor = 1;
      //Bias parameter
      if ((attribute = repElement.GetAttribute("Bias")) != "")
      {
        attribute = attribute.Replace(".", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator);
        attribute = attribute.Replace(",", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator);
        Bias = Convert.ToDouble(attribute);
      }
      else
        Bias = 0;
      Format = repElement.GetAttribute("Format");
    }

    /// <summary>
    /// Method returns string with formated numerical value.
    /// </summary>
    /// <param name="value">Value to format. Value has to be convertible to elementary type of this representation.</param>
    /// <returns>Value formatted according to format string</returns>
    public override String FormatValue(Object value)
    {
      return DataTypeObj.FormatValue(value, Format, Divisor, Bias);
    }

    /// <summary>
    /// Scales given value using a divisor defined for the representation.
    /// In case of divisor being 1 or 0, original object is returned, 
    /// otherwise result is returned as double.
    /// </summary>
    /// <param name="value">Value to scale</param>
    /// <returns>Scaled value as double or original object</returns>
    public override object ScaleValue(object value)
    {
      return DataTypeObj.ScaleValue(value, Divisor, Bias);
    }
  }

  /// <summary>
  /// Representation of binary data. Formats the data according to format string.
  /// </summary>
  [Serializable]
  public class DDDBinaryRepres : DDDRepresentation
  {
    /// <summary>
    /// Format string, supported values are: ASCII, UTF8, UTF7, UNICODE, BINARY
    /// </summary>
    public String Format;

    /// <summary>
    /// Length of binary data
    /// </summary>
    public int Length;

    /// <summary>
    /// Protected constructor
    /// </summary>
    protected DDDBinaryRepres()
    {
    }

    /// <summary>
    /// Creates and initializes new binary representation object from XML element
    /// </summary>
    /// <param name="repElement">XML element describing the representation</param>
    /// <returns>binary representation object</returns>
    public static DDDBinaryRepres CreateFromDDDObjXML(XmlElement repElement)
    {
      try
      {
        DDDBinaryRepres rep = new DDDBinaryRepres();
        rep.InitializeFromDDDObjXML(repElement);
        return rep;
      }
      catch (Exception ex)
      {
        throw new DDDObjectException(ex, "Failed to create binary representation object");
      }
    }

    /// <summary>
    /// Initializes instance data from XML element
    /// </summary>
    /// <param name="repElement">xml element from DDDObjects document</param>
    protected override void InitializeFromDDDObjXML(XmlElement repElement)
    {
      //Call base class initialization
      base.InitializeFromDDDObjXML(repElement);
      String attribute;
      attribute = repElement.GetAttribute("Length");
      Length = Convert.ToInt32(attribute);
      Format = repElement.GetAttribute("Format");
    }

    /// <summary>
    /// Method returns string with formated binary value.
    /// </summary>
    /// <param name="value">Value to format. Value has to be convertible to array of bytes.</param>
    /// <returns>Value formatted according to format string</returns>
    public override String FormatValue(Object value)
    {
      //Convert value to array of bytes
      byte[] binVal = (byte[])value;
      char[] trimChar = new char[1];
      trimChar[0] = '\0';
      String result = Format;
      //Format string according to requested formatting
      switch (Format.ToUpper())
      {
        case "ASCII": //Format buffer as ASCII characters
          result = Encoding.ASCII.GetString(binVal).TrimEnd(trimChar);
          break;
        case "UTF8": //Format buffer as UTF8 characters
          result = Encoding.UTF8.GetString(binVal).TrimEnd(trimChar);
          break;
        case "UTF7": //Format buffer as UTF7 characters
          result = Encoding.UTF7.GetString(binVal).TrimEnd(trimChar);
          break;
        case "UNICODE": //Format buffer as UTF16 characters (assumes little endian byte order)
          result = Encoding.Unicode.GetString(binVal).TrimEnd(trimChar);
          break;
        case "BIN":
        case "BINARY":
          result = BitConverter.ToString(binVal);
          break;
      }
      return result;
    }

    /// <summary>
    /// Extracts value as array of bytes from binary buffer at given location.
    /// </summary>
    /// <param name="recBinData">Structure with binary data and record instance information.</param>
    /// <param name="dataOffset">Offset to binary buffer.</param>
    /// <param name="value">Output parameter containing extracted value.</param>
    /// <returns>Status of parsing operation</returns>
    public override eParseStatus ParseBinValue(sRecBinData recBinData, int dataOffset, out Object value)
    {
      value = null;
      //Check buffer length
      if (recBinData.BinData.Length < (dataOffset + Length))
        return eParseStatus.binaryBufferTooSmall;
      //Extract value as array of bytes
      value = new byte[Length];
      Array.Copy(recBinData.BinData, dataOffset, (byte[])value, 0, Length);
      return eParseStatus.parseSuccessfull;
    }
  }

  /// <summary>
  /// Represents enumerations - different string is assigned to each possible value of integer variable.
  /// </summary>
  [Serializable]
  public class DDDEnumRepres : DDDRepresentation
  {
    /// <summary>
    /// Array of value sets, one for each defined language
    /// </summary>
    private Dictionary<Int64, String>[] valueSets;

    /// <summary>
    /// Current value set, according to selected language
    /// </summary>
    private Dictionary<Int64, String> currentValueSet;

    /// <summary>
    /// Protected constructor
    /// </summary>
    protected DDDEnumRepres()
    {
      valueSets = new Dictionary<long,string>[DDDRepository.LanguageCount];
    }

    /// <summary>
    /// Creates and initializes new enum representation object from XML element
    /// </summary>
    /// <param name="repElement">XML element describing the representation</param>
    /// <returns>enum representation object</returns>
    public static DDDEnumRepres CreateFromDDDObjXML(XmlElement repElement)
    {
      try
      {
        DDDEnumRepres rep = new DDDEnumRepres();
        rep.InitializeFromDDDObjXML(repElement);
        return rep;
      }
      catch (Exception ex)
      {
        throw new DDDObjectException(ex, "Failed to create enumeration representation object");
      }
    }

    /// <summary>
    /// Initializes instance data from XML element
    /// </summary>
    /// <param name="repElement">xml element from DDDObjects document</param>
    protected override void InitializeFromDDDObjXML(XmlElement repElement)
    {
      //Call base class initialization
      base.InitializeFromDDDObjXML(repElement);

      //Create Dictionaries with enumeration values - one for each language
      //Select all enum elements
      XmlNodeList enumItems = repElement.SelectNodes("./EnumItem");
      Languages langID;
      Int64 value;
      String title;
      //Walk through and add to dicitonary each element
      foreach (XmlElement enumItem in enumItems)
      {
        //Select attributes
        langID = (Languages)Convert.ToInt32(enumItem.GetAttribute("LanguageID"));
        value = Convert.ToInt64(enumItem.GetAttribute("Value"));
        title = enumItem.GetAttribute("Title");
        //Check language boundary
        if ((Int32)langID >= DDDRepository.LanguageCount)
          throw new DDDObjectException("Unknown language ID {0}", langID);
        //Check if dictionary for languge exists
        if (valueSets[(Int32)langID] == null)
          valueSets[(Int32)langID] = new Dictionary<long, string>(); //Create new dictionary for specified language
        //Add value to dictionary
        valueSets[(Int32)langID].Add(value, title);
      }
    }

    /// <summary>
    /// Method prepares language - dependent properties for selected language.
    /// </summary>
    /// <param name="currentLang">selected language</param>
    /// <param name="fallbackLang">language to use if selected language is not available</param>
    /// <returns>True if all properties are defined for specified language</returns>
    public override bool SetCurrentLanguage(Languages currentLang, Languages fallbackLang)
    {
      //check if specified language exists
      if (valueSets[(int)currentLang] != null)
      {
        currentValueSet = valueSets[(int)currentLang];
        return true;
      }
      else if (valueSets[(int)fallbackLang] != null)
        currentValueSet = valueSets[(int)fallbackLang];
      else if (valueSets[(int)Languages.Default] != null)
        currentValueSet = valueSets[(int)Languages.Default];
      else
      {
        currentValueSet = null;
        foreach (Dictionary<Int64, String> valueSet in valueSets)
          if (valueSet != null)
          {
            currentValueSet = valueSet;
            break;
          }
      }
      return false;
    }

    /// <summary>
    /// Method returns string from enumeration collection according to passed value.
    /// </summary>
    /// <param name="value">Value to format. Value has to be convertible to Int64 type.</param>
    /// <returns>String from enumeration collection for currently selecte language</returns>
    public override String FormatValue(Object value)
    {
      Int64 iVal = Convert.ToInt64(value);
      String retStr = "";
      if ((currentValueSet != null) && currentValueSet.TryGetValue(iVal, out retStr))
        return retStr;
      else
        return iVal.ToString();
    }
  }

  /// <summary>
  /// Represents bitset variables - integer data displayed as set of named bits
  /// </summary>
  [Serializable]
  public class DDDBitsetRepres : DDDRepresentation
  {
    /// <summary>
    /// Array of bit names lists, one for each defined language
    /// </summary>
    private SortedList<Int64, String>[] bitNamesSets;
    /// <summary>
    /// Current bit names list according to selected language
    /// </summary>
    private SortedList<Int64, String> currentBitNameSet;

    /// <summary>
    /// Protected constructor
    /// </summary>
    protected DDDBitsetRepres()
    {
      bitNamesSets = new SortedList<long,string>[DDDRepository.LanguageCount];
    }

    /// <summary>
    /// Creates and initializes new bitset representation object from XML element
    /// </summary>
    /// <param name="repElement">XML element describing the representation</param>
    /// <returns>bitset representation object</returns>
    public static DDDBitsetRepres CreateFromDDDObjXML(XmlElement repElement)
    {
      try
      {
        DDDBitsetRepres rep = new DDDBitsetRepres();
        rep.InitializeFromDDDObjXML(repElement);
        return rep;
      }
      catch (Exception ex)
      {
        throw new DDDObjectException(ex, "Failed to create bitset representation object");
      }
    }

    /// <summary>
    /// Initializes instance data from XML element
    /// </summary>
    /// <param name="repElement">xml element from DDDObjects document</param>
    protected override void InitializeFromDDDObjXML(XmlElement repElement)
    {
      //Call base class initialization
      base.InitializeFromDDDObjXML(repElement);

      //Create Sorted lists with bit names - one for each language
      //Select all bitrepres elements
      XmlNodeList bitItems = repElement.SelectNodes("./BitRepres");
      Languages langID;
      Int64 mask;
      String title;
      //Walk through and add to dicitonary each element
      foreach (XmlElement bitRepres in bitItems)
      {
        //Select attributes
        langID = (Languages)Convert.ToInt32(bitRepres.GetAttribute("LanguageID"));
        mask = Convert.ToInt64(bitRepres.GetAttribute("Mask"));
        title = bitRepres.GetAttribute("Title");
        //Check language boundary
        if ((Int32)langID >= DDDRepository.LanguageCount)
          throw new DDDObjectException("Unknown language ID {0}", langID);
        //Check if sorted list for languge exists
        if (bitNamesSets[(Int32)langID] == null)
          bitNamesSets[(Int32)langID] = new SortedList<long, string>(); //Create new sorted list for specified language
        //Add value to dictionary
        bitNamesSets[(Int32)langID].Add(mask, title);
      }
    }

    /// <summary>
    /// Method prepares language - dependent properties for selected language.
    /// </summary>
    /// <param name="currentLang">selected language</param>
    /// <param name="fallbackLang">language to use if selected language is not available</param>
    /// <returns>True if all properties are defined for specified language</returns>
    public override bool SetCurrentLanguage(Languages currentLang, Languages fallbackLang)
    {
      //check if specified language exists
      if (bitNamesSets[(int)currentLang] != null)
      {
        currentBitNameSet = bitNamesSets[(int)currentLang];
        return true;
      }
      else if (bitNamesSets[(int)fallbackLang] != null)
        currentBitNameSet = bitNamesSets[(int)fallbackLang];
      else if (bitNamesSets[(int)Languages.Default] != null)
        currentBitNameSet = bitNamesSets[(int)Languages.Default];
      else
      {
        currentBitNameSet = null;
        foreach (SortedList<Int64, String> bitNameSet in bitNamesSets)
          if (bitNameSet != null)
          {
            currentBitNameSet = bitNameSet;
            break;
          }
      }
      return false;
    }

    /// <summary>
    /// Method returns array of all masks for used bits in bitset
    /// </summary>
    /// <returns>Array of masks of used bits</returns>
    public Int64[] GetUsedBitMasks()
    {
      Int64[] masks = new Int64[currentBitNameSet.Count];
      for (int i = 0; i < currentBitNameSet.Count; i++)
        masks[i] = currentBitNameSet.Keys[i];
      return masks;
    }

    /// <summary>
    /// Returns title of specified bit for currently selected language or
    /// empty string if bit does not exist.
    /// </summary>
    /// <param name="mask">Mask specifying the bit</param>
    /// <returns>Bit title</returns>
    public String GetBitTitle(Int64 mask)
    {
      String title;
      if ((currentBitNameSet == null) || (mask == 0) || !currentBitNameSet.TryGetValue(mask, out title))
        return "";
      else
        return title;
    }

    /// <summary>
    /// Returns array of Mask - Bit value pairs for specified value and current language
    /// </summary>
    /// <param name="value">Value of variable. Value has to be convertible to Int64 data type</param>
    /// <returns>Array of Mask - Formatted bit values pairs</returns>
    public override KeyValuePair<Int64, String>[] GetMasksAndValues(Object value)
    {
      Int64 iVal = Convert.ToInt64(value);

      if (currentBitNameSet == null)
        return new KeyValuePair<long, string>[0];

      KeyValuePair<Int64, String>[] pairs = new KeyValuePair<long, string>[currentBitNameSet.Count];
      String symbol;
      for (int i = 0; i < currentBitNameSet.Count; i++)
      {
        if ((iVal & currentBitNameSet.Keys[i]) != 0)
          symbol = DDDHelper.BoolTrueSymbol;
        else
          symbol = DDDHelper.BoolFalseSymbol;
        pairs[i] = new KeyValuePair<long, string>(currentBitNameSet.Keys[i], symbol);
      }
      return pairs;
    }

    /// <summary>
    /// Returns value of specified bit as string defined for True or False values.
    /// </summary>
    /// <param name="value">Value of bitset variable</param>
    /// <param name="mask">Mask of bit to format</param>
    /// <returns>Value of specified bit as string symbol</returns>
    public String FormatBitValue(Object value, Int64 mask)
    {
      Int64 iVal = Convert.ToInt64(value);
      if ((iVal & mask) != 0)
        return DDDHelper.BoolTrueSymbol;
      else
        return DDDHelper.BoolFalseSymbol;
    }

    /// <summary>
    /// Returns value of specified bit as boolean variable
    /// </summary>
    /// <param name="value">Value of bitset variable</param>
    /// <param name="mask">Mask of requiered bit</param>
    /// <returns>Value of specified bit</returns>
    public bool GetBitValue(Object value, Int64 mask)
    {
      Int64 iVal = Convert.ToInt64(value);
      if ((iVal & mask) != 0)
        return true;
      else
        return false;
    }

    /// <summary>
    /// Method returns bitstring value fromated as series of 1s and 0s.
    /// </summary>
    /// <param name="value">Value to format. Value has to be convertible to Int64 type.</param>
    /// <returns>Value formated in binary representation</returns>
    public override String FormatValue(Object value)
    {
      Int64 iVal = Convert.ToInt64(value);
      return iVal.ToString("x");
    }

    /// <summary>
    /// Formats given value in the form of hexadecimal and binary string:
    /// FFFF H |0000|0000|0000|0000|
    /// </summary>
    /// <param name="value">Integer value</param>
    /// <returns>Formatted string</returns>
    public String FormatSIBASString(Object value)
    {
      return DataTypeObj.FormatHexValue(value) + " H " + DataTypeObj.FormatBinValue(value, "|");
    }


    /// <summary>
    /// Returns string identifying member bit
    /// </summary>
    /// <param name="mask">Mask identifying the bit</param>
    /// <param name="varIDString">String identifying the variable</param>
    /// <returns>Formated ID string</returns>
    public static String GetBitIDString(Int64 mask, String varIDString)
    {
      return varIDString + "." + mask.ToString("0000000000");
    }
  }
}
