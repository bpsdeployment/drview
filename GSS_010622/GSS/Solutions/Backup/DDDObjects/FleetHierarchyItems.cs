using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace FleetHierarchies
{
  /// <summary>
  /// Virtual class, parent for all fleet hierarchy item classes
  /// Defines virtual methods and common members
  /// </summary>
  public class FleetHierarchyItem
  {
    #region Private members
    List<FleetHierarchyItem> childItems = new List<FleetHierarchyItem>();
    #endregion //Private members

    #region Public properties
    /// <summary>
    /// Name of the hierarchy item
    /// </summary>
    public String Name = "";

    /// <summary>
    /// Description of the hierarchy item
    /// </summary>
    public String Description = "";
    /// <summary>
    /// ID of this hierarchy item
    /// </summary>
    public int ItemID = -1;

    /// <summary>
    /// Parent hierarchy item
    /// </summary>
    public FleetHierarchyItem Parent = null;

    /// <summary>
    /// List of child hierarchy items
    /// </summary>
    public List<FleetHierarchyItem> ChildItems
    {
      get { return childItems; }
    }
    #endregion //Public properties

    #region Construction initialization
    /// <summary>
    /// Default constructor
    /// </summary>
    public FleetHierarchyItem()
    {
    }

    /// <summary>
    /// Constructs fleet hierarchy item from given XmlElement, together with all the children
    /// </summary>
    /// <param name="hierarchyElem">XML element with definition of this hierarchy item</param>
    /// <param name="parent">Parent hierarchy item or null</param>
    /// <param name="itemsDict">Dictionary, to which name of this hierarchy item is added</param>
    /// <param name="itemIDsDict">Dictionary, to which ID of this hierarchy item is added</param>
    public FleetHierarchyItem(XmlElement hierarchyElem, FleetHierarchyItem parent, 
      ref Dictionary<String, FleetHierarchyItem> itemsDict, ref Dictionary<int, FleetHierarchyItem> itemIDsDict)
    {
      this.Parent = parent;
      this.Name = hierarchyElem.GetAttribute("name");
      this.Description = hierarchyElem.GetAttribute("description");
      if (hierarchyElem.GetAttribute("itemID") != String.Empty)
        this.ItemID = Convert.ToInt32(hierarchyElem.GetAttribute("itemID"));
      //Add name to names dictionary
      if (!itemsDict.ContainsKey(Name))
        itemsDict.Add(Name, this);
      //Add ID to ids dictionary
      if (!itemIDsDict.ContainsKey(ItemID) && ItemID != -1)
        itemIDsDict.Add(ItemID, this);
      //Create child hierarchy items
      foreach (XmlElement childElem in hierarchyElem.ChildNodes)
        this.childItems.Add(CreateFromXmlElem(childElem, this, ref itemsDict, ref itemIDsDict));
    }
    #endregion //Construction initialization

    #region Virtual methods
    /// <summary>
    /// Returns operator of this hierarchy item or null
    /// </summary>
    /// <returns></returns>
    public virtual FHOperator GetOperator()
    {
      return null;
    }

    /// <summary>
    /// Returns fleet this hierarchy item belongs to or null
    /// </summary>
    /// <returns></returns>
    public virtual FHFleet GetFleet()
    {
      return null;
    }

    /// <summary>
    /// Returns train this hierarchy item belongs to or null
    /// </summary>
    /// <returns></returns>
    public virtual FHTrain GetTrain()
    {
      return null;
    }

    /// <summary>
    /// Returns trainset this hierarchy item belongs to or null
    /// </summary>
    /// <returns></returns>
    public virtual FHTrainset GetTrainset()
    {
      return null;
    }

    /// <summary>
    /// Returns vehicle this hierarchy item belongs to or null
    /// </summary>
    /// <returns></returns>
    public virtual FHVehicle GetVehicle()
    {
      return null;
    }

    /// <summary>
    /// Creates Xml elelment from this hierarchy item, together with all child items (elements)
    /// </summary>
    /// <param name="doc">Xml document used to create new elements</param>
    /// <returns>element representing this item</returns>
    public virtual XmlElement CreateXMLElement(XmlDocument doc)
    {
      return null;
    }
    #endregion //Public methods

    #region Public methods
    #endregion //Public methods

    #region Helper methods
    /// <summary>
    /// Populates given XML element with attributes according to inner parameters.
    /// Creates child elements according to child items.
    /// </summary>
    /// <param name="elem">XML element to populate</param>
    /// <param name="doc">XML document used to construt child XML elements</param>
    protected XmlElement PopulateXmlElement(XmlElement elem, XmlDocument doc)
    {
      elem.SetAttribute("name", Name);
      elem.SetAttribute("description", Description);
      if (ItemID >= 0)
        elem.SetAttribute("itemID", ItemID.ToString());
      foreach (FleetHierarchyItem childItem in childItems)
        elem.AppendChild(childItem.CreateXMLElement(doc));
      return elem;
    }
    #endregion //Helper methods

    #region Static methods
    /// <summary>
    /// Creates fleet hierarchy item from given XmlElement, together with all the children.
    /// Type of the item is determined by given XML element.
    /// </summary>
    /// <param name="hierarchyElem">XML element with definition of this hierarchy item</param>
    /// <param name="parent">Parent hierarchy item or null</param>
    /// <param name="itemsDict">Dictionary, to which name of this hierarchy item is added</param>
    /// <param name="itemIDsDict">Dictionary, to which ID of this hierarchy item is added</param>
    public static FleetHierarchyItem CreateFromXmlElem(XmlElement hierarchyElem, FleetHierarchyItem parent,
      ref Dictionary<String, FleetHierarchyItem> itemsDict, ref Dictionary<int, FleetHierarchyItem> itemIDsDict)
    {
      switch (hierarchyElem.Name)
      {
        case "operator":
          return new FHOperator(hierarchyElem, parent, ref itemsDict, ref itemIDsDict);
        case "fleet":
          return new FHFleet(hierarchyElem, parent, ref itemsDict, ref itemIDsDict);
        case "train":
          return new FHTrain(hierarchyElem, parent, ref itemsDict, ref itemIDsDict);
        case "trainset":
          return new FHTrainset(hierarchyElem, parent, ref itemsDict, ref itemIDsDict);
        case "vehicle":
          return new FHVehicle(hierarchyElem, parent, ref itemsDict, ref itemIDsDict);
      }
      return null;
    }
    #endregion //Static methods
  }

  /// <summary>
  /// Class represents operator of the fleet (top level of hierarchy)
  /// </summary>
  public class FHOperator : FleetHierarchyItem
  {

    #region Construction initialization
    /// <summary>
    /// Default constructor
    /// </summary>
    public FHOperator()
    { }

    /// <summary>
    /// Constructs fleet hierarchy item from given XmlElement, together with all the children
    /// </summary>
    /// <param name="hierarchyElem">XML element with definition of this hierarchy item</param>
    /// <param name="parent">Parent hierarchy item or null</param>
    /// <param name="itemsDict">Dictionary, to which name of this hierarchy item is added</param>
    /// <param name="itemIDsDict">Dictionary, to which ID of this hierarchy item is added</param>
    public FHOperator(XmlElement hierarchyElem, FleetHierarchyItem parent, 
      ref Dictionary<String, FleetHierarchyItem> itemsDict, ref Dictionary<int, FleetHierarchyItem> itemIDsDict)
      : base(hierarchyElem, parent, ref itemsDict, ref itemIDsDict)
    { }
    #endregion //Construction initialization

    #region Virtual methods overrides
    /// <summary>
    /// Returns operator of this hierarchy item or null
    /// </summary>
    /// <returns></returns>
    public override FHOperator GetOperator()
    {
      return this;
    }

    /// <summary>
    /// Creates Xml elelment from this hierarchy item, together with all child items (elements)
    /// </summary>
    /// <param name="doc">Xml document used to create new elements</param>
    /// <returns>element representing this item</returns>
    public override XmlElement CreateXMLElement(XmlDocument doc)
    {
      return PopulateXmlElement(doc.CreateElement("operator"), doc);
    }
    #endregion Virtual methods overrides
  }

  /// <summary>
  /// Class represents one type of fleet in hierarchy
  /// </summary>
  public class FHFleet : FleetHierarchyItem
  {
    #region Construction initialization
    /// <summary>
    /// Default constructor
    /// </summary>
    public FHFleet()
    { }

    /// <summary>
    /// Constructs fleet hierarchy item from given XmlElement, together with all the children
    /// </summary>
    /// <param name="hierarchyElem">XML element with definition of this hierarchy item</param>
    /// <param name="parent">Parent hierarchy item or null</param>
    /// <param name="itemsDict">Dictionary, to which name of this hierarchy item is added</param>
    /// <param name="itemIDsDict">Dictionary, to which ID of this hierarchy item is added</param>
    public FHFleet(XmlElement hierarchyElem, FleetHierarchyItem parent, 
      ref Dictionary<String, FleetHierarchyItem> itemsDict, ref Dictionary<int, FleetHierarchyItem> itemIDsDict)
      : base(hierarchyElem, parent, ref itemsDict, ref itemIDsDict)
    { }
    #endregion //Construction initialization

    #region Virtual methods overrides
    /// <summary>
    /// Returns operator of this hierarchy item or null
    /// </summary>
    /// <returns></returns>
    public override FHOperator GetOperator()
    {
      return Parent.GetOperator();
    }

    /// <summary>
    /// Returns fleet this hierarchy item belongs to or null
    /// </summary>
    /// <returns></returns>
    public override FHFleet GetFleet()
    {
      return this;
    }

    /// <summary>
    /// Creates Xml elelment from this hierarchy item, together with all child items (elements)
    /// </summary>
    /// <param name="doc">Xml document used to create new elements</param>
    /// <returns>element representing this item</returns>
    public override XmlElement CreateXMLElement(XmlDocument doc)
    {
      return PopulateXmlElement(doc.CreateElement("fleet"), doc);
    }
    #endregion Virtual methods overrides
  }

  /// <summary>
  /// Class represents one train in fleet hierarchy
  /// </summary>
  public class FHTrain : FleetHierarchyItem
  {
    
    #region Construction initialization
    /// <summary>
    /// Default constructor
    /// </summary>
    public FHTrain()
    { }

    /// <summary>
    /// Constructs fleet hierarchy item from given XmlElement, together with all the children
    /// </summary>
    /// <param name="hierarchyElem">XML element with definition of this hierarchy item</param>
    /// <param name="parent">Parent hierarchy item or null</param>
    /// <param name="itemsDict">Dictionary, to which name of this hierarchy item is added</param>
    /// <param name="itemIDsDict">Dictionary, to which ID of this hierarchy item is added</param>
    public FHTrain(XmlElement hierarchyElem, FleetHierarchyItem parent, 
      ref Dictionary<String, FleetHierarchyItem> itemsDict, ref Dictionary<int, FleetHierarchyItem> itemIDsDict)
      : base(hierarchyElem, parent, ref itemsDict, ref itemIDsDict)
    { }
    #endregion //Construction initialization

    #region Virtual methods overrides
    /// <summary>
    /// Returns operator of this hierarchy item or null
    /// </summary>
    /// <returns></returns>
    public override FHOperator GetOperator()
    {
      return Parent.GetOperator();
    }

    /// <summary>
    /// Returns fleet this hierarchy item belongs to or null
    /// </summary>
    /// <returns></returns>
    public override FHFleet GetFleet()
    {
      return Parent.GetFleet();
    }

    /// <summary>
    /// Returns train this hierarchy item belongs to or null
    /// </summary>
    /// <returns></returns>
    public override FHTrain GetTrain()
    {
      return this;
    }

    /// <summary>
    /// Creates Xml elelment from this hierarchy item, together with all child items (elements)
    /// </summary>
    /// <param name="doc">Xml document used to create new elements</param>
    /// <returns>element representing this item</returns>
    public override XmlElement CreateXMLElement(XmlDocument doc)
    {
      return PopulateXmlElement(doc.CreateElement("train"), doc);
    }
    #endregion Virtual methods overrides
  }

  /// <summary>
  /// Class represents one trainset in fleet hierarchy
  /// </summary>
  public class FHTrainset : FleetHierarchyItem
  {
    #region Construction initialization
    /// <summary>
    /// Default constructor
    /// </summary>
    public FHTrainset()
    { }

    /// <summary>
    /// Constructs fleet hierarchy item from given XmlElement, together with all the children
    /// </summary>
    /// <param name="hierarchyElem">XML element with definition of this hierarchy item</param>
    /// <param name="parent">Parent hierarchy item or null</param>
    /// <param name="itemsDict">Dictionary, to which name of this hierarchy item is added</param>
    /// <param name="itemIDsDict">Dictionary, to which ID of this hierarchy item is added</param>
    public FHTrainset(XmlElement hierarchyElem, FleetHierarchyItem parent, 
      ref Dictionary<String, FleetHierarchyItem> itemsDict, ref Dictionary<int, FleetHierarchyItem> itemIDsDict)
      : base(hierarchyElem, parent, ref itemsDict, ref itemIDsDict)
    { }
    #endregion //Construction initialization

    #region Virtual methods overrides
    /// <summary>
    /// Returns operator of this hierarchy item or null
    /// </summary>
    /// <returns></returns>
    public override FHOperator GetOperator()
    {
      return Parent.GetOperator();
    }

    /// <summary>
    /// Returns fleet this hierarchy item belongs to or null
    /// </summary>
    /// <returns></returns>
    public override FHFleet GetFleet()
    {
      return Parent.GetFleet();
    }

    /// <summary>
    /// Returns train this hierarchy item belongs to or null
    /// </summary>
    /// <returns></returns>
    public override FHTrain GetTrain()
    {
      return Parent.GetTrain();
    }

    /// <summary>
    /// Returns trainset this hierarchy item belongs to or null
    /// </summary>
    /// <returns></returns>
    public override FHTrainset GetTrainset()
    {
      return this;
    }

    /// <summary>
    /// Creates Xml elelment from this hierarchy item, together with all child items (elements)
    /// </summary>
    /// <param name="doc">Xml document used to create new elements</param>
    /// <returns>element representing this item</returns>
    public override XmlElement CreateXMLElement(XmlDocument doc)
    {
      return PopulateXmlElement(doc.CreateElement("trainset"), doc);
    }
    #endregion Virtual methods overrides
  }

  /// <summary>
  /// Class represents one vehicle in fleet hierarchy
  /// </summary>
  public class FHVehicle : FleetHierarchyItem
  {
    #region Construction initialization
    /// <summary>
    /// Default constructor
    /// </summary>
    public FHVehicle()
    { }

    /// <summary>
    /// Constructs fleet hierarchy item from given XmlElement, together with all the children
    /// </summary>
    /// <param name="hierarchyElem">XML element with definition of this hierarchy item</param>
    /// <param name="parent">Parent hierarchy item or null</param>
    /// <param name="itemsDict">Dictionary, to which name of this hierarchy item is added</param>
    /// <param name="itemIDsDict">Dictionary, to which ID of this hierarchy item is added</param>
    public FHVehicle(XmlElement hierarchyElem, FleetHierarchyItem parent, 
      ref Dictionary<String, FleetHierarchyItem> itemsDict, ref Dictionary<int, FleetHierarchyItem> itemIDsDict)
      : base(hierarchyElem, parent, ref itemsDict, ref itemIDsDict)

    { }
    #endregion //Construction initialization

    #region Virtual methods overrides
    /// <summary>
    /// Returns operator of this hierarchy item or null
    /// </summary>
    /// <returns></returns>
    public override FHOperator GetOperator()
    {
      return Parent.GetOperator();
    }

    /// <summary>
    /// Returns fleet this hierarchy item belongs to or null
    /// </summary>
    /// <returns></returns>
    public override FHFleet GetFleet()
    {
      return Parent.GetFleet();
    }

    /// <summary>
    /// Returns train this hierarchy item belongs to or null
    /// </summary>
    /// <returns></returns>
    public override FHTrain GetTrain()
    {
      return Parent.GetTrain();
    }

    /// <summary>
    /// Returns trainset this hierarchy item belongs to or null
    /// </summary>
    /// <returns></returns>
    public override FHTrainset GetTrainset()
    {
      return Parent.GetTrainset();
    }

    /// <summary>
    /// Returns vehicle this hierarchy item belongs to or null
    /// </summary>
    /// <returns></returns>
    public override FHVehicle GetVehicle()
    {
      return this;
    }

    /// <summary>
    /// Creates Xml elelment from this hierarchy item, together with all child items (elements)
    /// </summary>
    /// <param name="doc">Xml document used to create new elements</param>
    /// <returns>element representing this item</returns>
    public override XmlElement CreateXMLElement(XmlDocument doc)
    {
      return PopulateXmlElement(doc.CreateElement("vehicle"), doc);
    }
    #endregion Virtual methods overrides
  }
}
