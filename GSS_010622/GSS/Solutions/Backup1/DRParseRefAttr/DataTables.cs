using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DDDObjects;

namespace DRParseRefAttr
{
  /// <summary>
  /// Class represents data table with parsed variables values,
  /// </summary>
  class ParsedVarsTable
  {
    private DataTable parsedVars; //DataTable object holding parsed values

    /// <summary>
    /// Constructor, initializes data table with columns
    /// </summary>
    public ParsedVarsTable()
    {
      //initialize the data table
      //Create columns for parsedValues table
      DataColumn colTUInstanceID = new DataColumn("TUInstanceID", Type.GetType("System.Guid"));
      DataColumn colRecordInstID = new DataColumn("RecordInstID", Type.GetType("System.UInt64"));
      DataColumn colRecordDefID = new DataColumn("RecordDefID", Type.GetType("System.Int32"));
      DataColumn colRefAttrID = new DataColumn("RefAttrID", Type.GetType("System.Int32"));
      DataColumn colValue = new DataColumn("Value", Type.GetType("System.Object"));
      //Create parsedValues table and add columns;
      parsedVars = new DataTable();
      parsedVars.Columns.Add(colTUInstanceID);
      parsedVars.Columns.Add(colRecordInstID);
      parsedVars.Columns.Add(colRecordDefID);
      parsedVars.Columns.Add(colRefAttrID);
      parsedVars.Columns.Add(colValue);
    }

    //Property providing read only acces to internal data table
    public DataTable ParsedVars
    {
      get { return parsedVars; }
    }

    /// <summary>
    /// Inserts collection of rows into table according to passed structures.
    /// </summary>
    /// <param name="recbinData">Structure containing record instance information</param>
    /// <param name="parsedVals">List of structures with parsed values</param>
    public void InsertData(sRecBinData recbinData, List<sParsedRefAttrValue> parsedVals)
    {
      foreach (sParsedRefAttrValue parsedVal in parsedVals)
        parsedVars.Rows.Add(recbinData.TUInstanceID, recbinData.RecordInstID, recbinData.RecordDefID, parsedVal.RefAttrID, parsedVal.Value);
    }

    //Deletes all rows from data table
    public void ClearTable()
    {
      parsedVars.Rows.Clear();
    }
  }

  /// <summary>
  /// Class represents data table containing reference to parsed diagnostic records
  /// together with the result of parsing (succes or failure reason)
  /// </summary>
  class RecParseStatTable
  {
    private DataTable parsedRecs; //DataTable object holding reference to parsed records

    /// <summary>
    /// Constructor, initializes data table with columns
    /// </summary>
    public RecParseStatTable()
    {
      //Create columns for parsedRecs table
      DataColumn colTUInstanceID = new DataColumn("TUInstanceID", Type.GetType("System.Guid"));
      DataColumn colRecordInstID = new DataColumn("RecordInstID", Type.GetType("System.UInt64"));
      DataColumn colRecordDefID = new DataColumn("RecordDefID", Type.GetType("System.Int32"));
      DataColumn colFailReasonID = new DataColumn("FailReasonID", Type.GetType("System.Int32"));
      //Create parsedRecs table and add columns
      parsedRecs = new DataTable();
      parsedRecs.Columns.Add(colTUInstanceID);
      parsedRecs.Columns.Add(colRecordInstID);
      parsedRecs.Columns.Add(colRecordDefID);
      parsedRecs.Columns.Add(colFailReasonID);
    }

    //Property providing read only acces to internal data table
    public DataTable ParsedRecs
    {
      get { return parsedRecs; }
    }

    //Inserts row referencing successfully parsed record
    public void InsertRecParseResult(Guid TUInstanceID, Int64 RecordInstID, Int32 RecordDefID, eParseStatus ParseStatus)
    {
      parsedRecs.Rows.Add(TUInstanceID, RecordInstID, RecordDefID, (int)ParseStatus);
    }

    //Deletes all rows from data table
    public void ClearTable()
    {
      parsedRecs.Rows.Clear();
    }
  }
}
