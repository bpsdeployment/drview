using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.ServiceProcess;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
//using System.Runtime.InteropServices;
using SQLXMLBULKLOADLib;
using System.IO;
using System.Resources;
using System.Data.SqlClient;
using System.Threading;
using DRParseRefAttr;


namespace DRImport
{
  /// <summary>
  /// !!! IMPORTANT !!!
  /// Record import has to run in separate STA thread (single threaded apartment state)
  /// This is because the SQLXMLBULKLOAD ActiveX can only run in single threaded enviroments
  /// </summary>
  public partial class DRImport : ServiceBase
  {
    private System.Threading.Timer importTimer;
    private Thread workerThread; //thread that manages the import of diagnostic records
    private bool fastRestart;  //flag if the timer should tick with fast period 
    private DRParserRefAttr drParser;   //parser for diagnostic records
    private SqlCommand cmdDeleteObsolete; //command for parser - deletes obsolete records
    private SqlCommand cmdGetBinRec; //command for parser - retrieves binary records
    private SqlCommand cmdGetUnprocCount; //command for parser - returns number of unprocessed records
    private SqlCommand cmdProcessParsedRec; //command for parser - processes parsed records
    private ParseCmdParams parseCmdParams; //class holding parser parameterisation

    public DRImport()
    {
      InitializeComponent();
      logger.FileName = ImportSettings.Default.LogFileName;
      workerThread = new Thread(this.ImportDiagnosticRecords);
    }

    protected override void OnStart(string[] args)
    {
      fastRestart = false;
      //Trace service startup result  
      logger.LogText("");
      logger.LogText(0, "DRImport", Properties.Resources.msgServiceStart);
      //Create parser for records
      InitiateDRParser();
      //construct the timer
      //First create delegate for callback method
      TimerCallback timerDelegate = new TimerCallback(importTimer_Elapsed);
      //Create and start timer
      importTimer = new System.Threading.Timer(timerDelegate, null, ImportSettings.Default.AfterStartDelaySec * 1000, ImportSettings.Default.ImportPeriodSec * 1000);
    }

    protected override void OnStop()
    {
      importTimer.Dispose();
      logger.LogText(0, "DRImport", Properties.Resources.msgServiceStop);
    }

    /// <summary>
    /// Event raised by a timer after the period elapsed.
    /// Worker thread for record import is started.
    /// If the thread is still running - overloading warning is written to Trace output
    /// and timer is restarted
    /// </summary>
    void importTimer_Elapsed(object state)
    {
      //start the STA thread that imports the records
      try
      {
        if (workerThread.IsAlive)
        {//thread is still running - overloading occured
          //log warning
          logger.LogText(0, "DRImport", Properties.Resources.msgImportThreadOverloading);
          //set fast timer period for next tick
          fastRestart = true;
        }
        else
        {//last thread start completed, start again
          workerThread = new Thread(this.ImportDiagnosticRecords);
          workerThread.SetApartmentState(ApartmentState.STA);
          workerThread.IsBackground = true;
          workerThread.Start();
        }
      }
      catch (Exception ex)
      {
        logger.LogText(0, "DRImport", Properties.Resources.msgErrImportCatastrophic, ex.Message);
        fastRestart = false;
      }
      //determine new timer period
      if (fastRestart)
        importTimer.Change(ImportSettings.Default.BatchDelaySec * 1000, ImportSettings.Default.ImportPeriodSec * 1000);
    }

    private void ImportDiagnosticRecords()
    {
      fastRestart = false;
      using (SqlConnection dbConnection = new SqlConnection())
      {
        try
        {
          //Get fresh configuration settings
          ImportSettings.Default.Reload();
          //Open connection to DB
          dbConnection.ConnectionString = ImportSettings.Default.CDDBConnectionString;
          dbConnection.Open();
          //Insert XML files into DB
          fastRestart = InsertXMLFiles(dbConnection);
          //Process all imported DR in database
          ProcessImportedDR(dbConnection);
          //Run parsing operation - parses values of referential attributes
          ParseRecords();
        }
        catch (Exception ex)
        {
          logger.LogText(1, "DRImportWork", Properties.Resources.msgDiagRecImportFailed, ex.Message);
        }
      }
    }

    /// <summary>
    /// Iterates over all files in DRImport directory and attempts to insert them into the DB.
    /// If the number of records in ImportedDR table exceeds specified limit, import stops.
    /// Files that fail to import are moved to FailedDR folder.
    /// </summary>
    /// <param name="dbConnection">Connection to DB, that is used for queriing the number of records in ImportedDR table</param>
    /// <returns>Retruns true, if there are any files left to process in DRImport directory</returns>
    private bool InsertXMLFiles(SqlConnection dbConnection)
    {
      bool filesLeft = false; //flag if there are still files to process in directory
      //Get maximum allowed number of records to import
      int maxImportedRecords = ImportSettings.Default.MaxRecInBatch;
      int impRecCount = 0;
      //Create FileInfo for XSD import template
      String xsdDir = ImportSettings.Default.XSDFilesDir;
      String xsdFileName = ImportSettings.Default.XMLBulkLoadXSDName;
      FileInfo xsdFile = new FileInfo(Path.Combine(xsdDir, xsdFileName));
      //Create bulk load object and set the properties
      SQLXMLBulkLoadClass  xmlBulkLoad = new SQLXMLBulkLoadClass();
      xmlBulkLoad.ConnectionString = ImportSettings.Default.BulkLoadConnString;
      xmlBulkLoad.ErrorLogFile = ImportSettings.Default.XMLBulkLoadErrFile;
      xmlBulkLoad.ForceTableLock = true;
      xmlBulkLoad.Transaction = true;
      xmlBulkLoad.XMLFragment = true;
      //Get the collection of files to import from DRImport directory
      DirectoryInfo drImportDir = new DirectoryInfo(ImportSettings.Default.ImportDir);
      FileInfo[] drFiles = drImportDir.GetFiles(Properties.Resources.drFilesMask);

      //Go trough the collection and import the files until record count limit is reached
      foreach (FileInfo drFile in drFiles)
      {
        //Test the number of records in ImportedDR table
        impRecCount = GetImportedDRRecCount(dbConnection);
        if (impRecCount > maxImportedRecords)
        {
          filesLeft = true; //There are still files left to process
          break;
        }
        //Import the file
        try
        {
          ImportFile(drFile, xsdFile, xmlBulkLoad);
        }
        catch (Exception ex)
        {
          logger.LogText(1, "DRImportWork", Properties.Resources.msgErrFileImportFailed, drFile.Name, ex.Message);
        }
      }
      //Refresh the number of imported records
      impRecCount = GetImportedDRRecCount(dbConnection);
      if (impRecCount > 0)
        logger.LogText(3, "DRImportWork", Properties.Resources.msgImportedDiagRecords, impRecCount);
      return filesLeft;
    }

    /// <summary>
    /// Executes the XML Bulk load operation to load uploaded DR into
    /// ImportedDR table. If the operation fails, XML file is moved into the 
    /// FaileDR folder.
    /// </summary>
    /// <param name="fileName">File to import</param>
    /// <param name="xsdName">XSD File describing the import</param>
    /// <param name="xmlBulkLoad">Bulk load object used for import</param>
    private void ImportFile(FileInfo fileName, FileInfo xsdName, SQLXMLBulkLoadClass xmlBulkLoad)
    {
      try
      {
        //Test if the file can be opened
        if (!CanOpenFile(fileName))
          return; //file can not be opened - probably it is still open by DDUpload
        if (fileName.Length < ImportSettings.Default.MinFileLength)
        { //File is too short - dont import
          logger.LogText(3, "DRImportWork", "File {0} is too short ({1} bytes). It will not be imported", fileName.Name, fileName.Length);
        }
        else
        { //File is long enough - import it
          //Execute the bulk load operation
          xmlBulkLoad.Execute(xsdName.FullName, fileName.FullName);
          logger.LogText(3, "DRImportWork", Properties.Resources.msgXMLFileImported, fileName.Name);
        }
        fileName.Delete();
      }
      catch (Exception ex)
      {
        //Bulk load failed - move the failed file into failed directory
        logger.LogText(1, "DRImportWork", Properties.Resources.msgErrXMLBulkLoadFailed, fileName.Name, ex.Message);
        FileInfo errFile = new FileInfo(ImportSettings.Default.XMLBulkLoadErrFile);
        DirectoryInfo failedDir = new DirectoryInfo(ImportSettings.Default.FailedDir);
        MoveFailedXMLFile(fileName, errFile, failedDir);
      }
    }

    /// <summary>
    /// Moves failed XML file (bulk load failed) into the failed directory.
    /// Together with the file, XMLBulkLoad error log is moved
    /// </summary>
    /// <param name="failedFile">File that failed to bulk load</param>
    /// <param name="errFile">File with XMLBulkLoad error log</param>
    /// <param name="failedDir">Target directory for failed files</param>
    private void MoveFailedXMLFile(FileInfo failedFile, FileInfo errFile, DirectoryInfo failedDir)
    {
      try
      {
        String failedName = Path.Combine(failedDir.FullName, failedFile.Name);
        String errName = Path.Combine(failedDir.FullName, failedFile.Name) + Properties.Resources.errFileExtension;
        if (File.Exists(failedName))
        { //File already exists in failed directory - remove it
          logger.LogText(2, "DRImportWork", "Deleting old failed file {0}", failedName);
          File.Delete(failedName);
        }
        //Move the failed XML File
        failedFile.MoveTo(failedName);
        //Move the associated error file
        if (File.Exists(errName))
        { //File already exists in failed directory - remove it
          logger.LogText(2, "DRImportWork", "Deleting old failed file {0}", errName);
          File.Delete(errName);
        }
        errFile.MoveTo(errName);
        logger.LogText(3, "DRImportWork", Properties.Resources.msgFileMoved, failedFile.Name);
      }
      catch (Exception ex)
      {
        logger.LogText(1, "DRImportWork", Properties.Resources.msgErrFileMoveFailed, failedFile.Name, ex.Message);
      }
    }

    /// <summary>
    /// Retrieves number of records in ImportedDR table
    /// </summary>
    /// <param name="cddbConnection">Open DB connection to database with ImportedDR table</param>
    /// <returns>Returns number of records in ImportedDR table</returns>
    private int GetImportedDRRecCount(SqlConnection cddbConnection)
    {
      int recCount = 0;
      SqlCommand cmdRecCount = new SqlCommand("GetImportedDRCount", cddbConnection);
      cmdRecCount.CommandType = CommandType.StoredProcedure;
      recCount = (int)cmdRecCount.ExecuteScalar();
      return recCount;
    }

    /// <summary>
    /// Tests if specified file can be opened for reading.
    /// </summary>
    /// <param name="srcFile">File to test</param>
    /// <returns>True if file can be opened for reading</returns>
    private bool CanOpenFile(FileInfo srcFile)
    {
      FileStream srcStream;
      try
      {
        srcStream = srcFile.OpenRead();
        srcStream.Close();
        srcStream.Dispose();
      }
      catch (IOException)
      {
        return false;
      }
      return true;
    }

    /// <summary>
    /// Procedure processes all records imported into ImportedDR table
    /// using stored database procedures.
    /// Records are pruned and copied into DiagnosticRecords table
    /// </summary>
    /// <param name="dbConnection">Open connection to CDDB database</param>
    private void ProcessImportedDR(SqlConnection dbConnection)
    {
      SqlParameter failedRecCount; //number of records that were copied to FailedDR table
      SqlParameter copiedRecCount; //number of records that were copied to DiagnosticRecords table
      SqlParameter updatedRecCount;//number of records that were updated in DiagnosticRecords table

      SqlCommand procCommand = new SqlCommand();
      procCommand.CommandTimeout = ImportSettings.Default.SqlCmdTimeoutSec;
      procCommand.CommandType = CommandType.StoredProcedure;
      procCommand.Connection = dbConnection;

      try
      {
        procCommand.CommandText = "ProcessImportedDR";
        copiedRecCount = procCommand.Parameters.Add("@CopiedRecCount", SqlDbType.Int);
        copiedRecCount.Direction = ParameterDirection.Output;
        failedRecCount = procCommand.Parameters.Add("@FailedRecCount", SqlDbType.Int);
        failedRecCount.Direction = ParameterDirection.Output;
        updatedRecCount = procCommand.Parameters.Add("@UpdatedRecCount", SqlDbType.Int);
        updatedRecCount.Direction = ParameterDirection.Output;

        procCommand.ExecuteNonQuery();
        if (((int)copiedRecCount.Value > 0) || ((int)updatedRecCount.Value > 0) || ((int)failedRecCount.Value > 0))
          logger.LogText(2, "DRImportWork", Properties.Resources.msgProcessedDR, (int)copiedRecCount.Value, (int)updatedRecCount.Value, (int)failedRecCount.Value);
      }
      catch (Exception ex)
      {
        logger.LogText(1, "DRImportWork", Properties.Resources.msgErrDRProcessingFailed, ex.Message);
        procCommand.CommandText = "FailAllImportedDR";
        procCommand.Parameters.Clear();
        failedRecCount = procCommand.Parameters.Add("@FailedRecCount", SqlDbType.Int);
        failedRecCount.Direction = ParameterDirection.Output;
        procCommand.ExecuteNonQuery();
        logger.LogText(2, "DRImportWork", Properties.Resources.msgWarMarkedDRFailed, (int)failedRecCount.Value);
      }
    }

    /// <summary>
    /// Creates new instance of parser for diagnostic records and prepares parameterisation classes
    /// accordnig to configuration file. 
    /// </summary>
    private void InitiateDRParser()
    {
      //Create DB connection
      SqlConnection dbConnection = new SqlConnection(ImportSettings.Default.CDDBConnectionString);
      //Create parameters for parser constructor
      DRParserParams parameters = new DRParserParams();
      parameters.BCPBatchSize = ParseSettings.Default.BCPBatchSize;
      parameters.BCPTimeout = ParseSettings.Default.BCPTimeout;
      parameters.ContinueOnError = ParseSettings.Default.ContinueOnError;
      parameters.DBConnection = dbConnection;
      parameters.MaxErrorCount = ParseSettings.Default.MaxErrorCount;
      parameters.TraceError = ParseSettings.Default.TraceError;
      parameters.TraceInfo = ParseSettings.Default.TraceInfo;
      parameters.TraceWarning = ParseSettings.Default.TraceWarning;
      //Create parser
      drParser = new DRParserRefAttr(parameters);
      //Create SqlCommands
      if (ParseSettings.Default.ProcDeleteObsolete == "")
        cmdDeleteObsolete = null;
      else
      {
        cmdDeleteObsolete = new SqlCommand();
        cmdDeleteObsolete.CommandType = CommandType.StoredProcedure;
        cmdDeleteObsolete.CommandTimeout = ParseSettings.Default.SQLCommandTimeout;
        cmdDeleteObsolete.CommandText = ParseSettings.Default.ProcDeleteObsolete;
      }
      cmdGetBinRec = new SqlCommand();
      cmdGetBinRec.CommandType = CommandType.StoredProcedure;
      cmdGetBinRec.CommandTimeout = ParseSettings.Default.SQLCommandTimeout;
      cmdGetBinRec.CommandText = ParseSettings.Default.ProcGetBinRec;
      cmdGetUnprocCount = new SqlCommand();
      cmdGetUnprocCount.CommandType = CommandType.StoredProcedure;
      cmdGetUnprocCount.CommandTimeout = ParseSettings.Default.SQLCommandTimeout;
      cmdGetUnprocCount.CommandText = ParseSettings.Default.ProcGetUnprocCount;
      cmdProcessParsedRec = new SqlCommand();
      cmdProcessParsedRec.CommandType = CommandType.StoredProcedure;
      cmdProcessParsedRec.CommandTimeout = ParseSettings.Default.SQLCommandTimeout;
      cmdProcessParsedRec.CommandText = ParseSettings.Default.ProcProcessParsed;
      //Create parser command parameters
      parseCmdParams = new ParseCmdParams();
      parseCmdParams.ParsedVarsTableName = ParseSettings.Default.ParsedVarsTable;
      parseCmdParams.ProcDeleteObsolete = cmdDeleteObsolete;
      parseCmdParams.ProcGetBinRec = cmdGetBinRec;
      parseCmdParams.ProcGetUnprocCount = cmdGetUnprocCount;
      parseCmdParams.ProcProcessParsed = cmdProcessParsedRec;
      parseCmdParams.RecParseStatTableName = ParseSettings.Default.RecParseStatTable;
      parseCmdParams.UnparsedCountTreshold = ParseSettings.Default.UnparsedCountTreshold;
      parseCmdParams.MaxIterationCount = ParseSettings.Default.MaxIterationCount;
      parseCmdParams.MaxParseTime = ParseSettings.Default.MaxParseTime;
      //Log
      logger.LogText(0, "DRParser", Properties.Resources.msgParserCreated);
    }

    /// <summary>
    /// Runs one batch of diagnostic record parsing.
    /// All parameters are stored in parseCmdParams member.
    /// If anything is parsed, all results are logged.
    /// </summary>
    private void ParseRecords()
    {
      ParseResults result = null;
      //Run parser
      try
      {
        result = drParser.ParseRecords(parseCmdParams);
      }
      catch (Exception ex)
      { //Exception - parsing failed
        logger.LogText(0, "DRParser", "Parsing of diagnostic records failed: {0}", ex.Message);
      }
      //Check if anything happened
      if (result != null && (result.IterationCount > 0 || result.TotalParsedRecs > 0 || result.TotalParsedVals > 0 || result.Errors.Count > 0))
      { //Log results
        logger.LogText(1, "DRParser", "Diagnostic records parsing results:");
        logger.LogText(2, "DRParser", "Success: {0}", result.Success);
        logger.LogText(2, "DRParser", "Elapsed time: {0}", result.ElapsedTime);
        logger.LogText(2, "DRParser", "Iteration count: {0}", result.IterationCount);
        logger.LogText(2, "DRParser", "Total parsed recs: {0}", result.TotalParsedRecs);
        logger.LogText(2, "DRParser", "Total parsed values: {0}", result.TotalParsedVals);
        logger.LogText(2, "DRParser", "Unparsed records left: {0}", result.UnparsedRecs);
        logger.LogText(2, "DRParser", "Deleted obsolete records: {0}", result.DeletedObsoleteRecs);
        logger.LogText(2, "DRParser", "Load data time: {0}", result.LoadBinDataTime);
        logger.LogText(2, "DRParser", "Parse time: {0}", result.ParseRecordsTime);
        logger.LogText(2, "DRParser", "BulkCopy time: {0}", result.BulkCopyTime);
        logger.LogText(2, "DRParser", "Process parsed time: {0}", result.ProcessParsedRecsTime);
        //Check for any errors
        if (result.Errors.Count > 0)
          logger.LogText(1, "DRParser", "Parsing errors:");
        foreach (Exception ex in result.Errors)
        {
          logger.LogText(2, "DRParser", ex.Message);
          if (ex.InnerException != null)
            logger.LogText(3, "DRParser", ex.InnerException.Message);
          if (ex.InnerException.InnerException != null)
            logger.LogText(4, "DRParser", ex.InnerException.InnerException.Message);
        }
      }
    }
  }
}
