using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace DDDObjects
{
    /// <summary>
    /// Holds all available information about one instance of Telediagnostic Unit
    /// </summary>
    [Serializable]
    public class TUInstance
    {
        /// <summary>
        /// Identification of Telediagnostic Unit
        /// </summary>
        public Guid TUInstanceID;

        /// <summary>
        /// Identification of Telediagnostic Unit type
        /// </summary>
        public Guid TUTypeID;

        /// <summary>
        /// Identification of DDD version running currently on TU
        /// </summary>
        public Guid DDDVersionID;

        /// <summary>
        /// Name of Telediagnostic unit
        /// </summary>
        public String TUName;

        /// <summary>
        /// UIC identification of the vehicle
        /// </summary>
        public UInt64 UICID;

        /// <summary>
        /// User comment
        /// </summary>
        public String Comment;

        /// <summary>
        /// Object representing corresponding TU Type
        /// </summary>
        public TUType TUTypeObject;

        /// <summary>
        /// Default protected constructor
        /// </summary>
        protected TUInstance()
        { }

        /// <summary>
        /// Constructs new TUInstance object from given parameters
        /// </summary>
        /// <param name="tuInstanceID">Identifier of TU Instance</param>
        /// <param name="dddVersionID">Identifier of running DDD Version in TU instance</param>
        /// <param name="tuName">Name of TU Instance</param>
        /// <param name="UICID">UIC number of vehicle TU is installed in</param>
        /// <param name="comment">Comment for the instance</param>
        /// <param name="typeObj">TU Type object instance is associated with</param>
        public TUInstance(Guid tuInstanceID, Guid dddVersionID, String tuName, UInt64 UICID, String comment, TUType typeObj)
        {
            TUInstanceID = tuInstanceID;
            TUTypeID = typeObj.TUTypeID;
            DDDVersionID = dddVersionID;
            TUName = tuName;
            this.UICID = UICID;
            Comment = comment;
            TUTypeObject = typeObj;
        }

        /// <summary>
        /// Creates new object from XML element
        /// </summary>
        /// <param name="instElement">TUInstance element from TUObjects XML file</param>
        /// <param name="helper">DDDHelper object where TUTypes are registered</param>
        /// <returns>new TUInstance object</returns>
        public static TUInstance CreateFromTUObjXML(XmlElement instElement, DDDHelper helper)
        {
            try
            {
                TUInstance instance;
                instance = new TUInstance();
                //Initialize instance
                instance.InitializeFromTUObjXML(instElement, helper);
                return instance;
            }
            catch (Exception ex)
            {
                throw new DDDObjectException(ex, "Failed to create TUInstance object");
            }
        }

        /// <summary>
        /// Initializes instance data from XML element 
        /// </summary>
        /// <param name="instElement">TUInstance element from TUObjects XML file</param>
        /// <param name="helper">DDDHelper object where TUTypes are registered</param>
        protected void InitializeFromTUObjXML(XmlElement instElement, DDDHelper helper)
        {
            //Initialize values of all attributes
            String attribute;
            TUInstanceID = new Guid(instElement.GetAttribute("TUInstanceID"));
            TUTypeID = new Guid(instElement.GetAttribute("TUTypeID"));
            if ((attribute = instElement.GetAttribute("DDDVersionID")) != "")
                DDDVersionID = new Guid(attribute);
            TUName = instElement.GetAttribute("TUName");
            if ((attribute = instElement.GetAttribute("UICID")) != "")
                UICID = Convert.ToUInt64(attribute);
            Comment = instElement.GetAttribute("Comment");

            //Initialize connected type object
            TUTypeObject = helper.GetTUType(TUTypeID);
        }
    }
}

