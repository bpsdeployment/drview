using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace DDDObjects
{
  /// <summary>
  /// Holds all available information about one type of Telediagnostic Unit
  /// </summary>
  [Serializable]
  public class TUType
  {
    /// <summary>
    /// Identification of Telediagnostic Unit type
    /// </summary>
    public Guid TUTypeID;

    /// <summary>
    /// Name of Telediagnostic unit type
    /// </summary>
    public String TypeName;

    /// <summary>
    /// User comment
    /// </summary>
    public String Comment;

    /// <summary>
    /// Default protected constructor
    /// </summary>
    protected TUType()
    {
    }

    /// <summary>
    /// Construts new TUType object from given parameters
    /// </summary>
    /// <param name="typeID">Identifier of new type</param>
    /// <param name="typeName">TU type name</param>
    /// <param name="comment">Comment for the new type</param>
    public TUType(Guid typeID, String typeName, String comment)
    {
      this.TUTypeID = typeID;
      this.TypeName = typeName;
      this.Comment = comment;
    }

    /// <summary>
    /// Creates new object from XML element
    /// </summary>
    /// <param name="typeElement">TUType element from TUObjects XML file</param>
    /// <returns>new TUType object</returns>
    public static TUType CreateFromTUObjXML(XmlElement typeElement)
    {
      try
      {
        TUType type;
        type = new TUType();
        //Initialize type
        type.InitializeFromTUObjXML(typeElement);
        return type;
      }
      catch (Exception ex)
      {
        throw new DDDObjectException(ex, "Failed to create TUType object");
      }
    }

    /// <summary>
    /// Initializes instance data from XML element 
    /// </summary>
    /// <param name="typeElement">TUType element from TUObjects XML file</param>
    protected void InitializeFromTUObjXML(XmlElement typeElement)
    {
      //Initialize values of all attributes
      TUTypeID = new Guid(typeElement.GetAttribute("TUTypeID"));
      TypeName = typeElement.GetAttribute("TypeName");
      Comment = typeElement.GetAttribute("Comment");
    }
  }
}
