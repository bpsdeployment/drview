using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace DDDObjects
{
  /// <summary>
  /// Class represents one record input
  /// </summary>
  [Serializable]
  public class DDDInput
  {
    /// <summary>
    /// Identifiacation of record the input belogs to
    /// </summary>
    public int RecordDefID;

    /// <summary>
    /// Index of input in record
    /// </summary>
    public int InputIndex;

    /// <summary>
    /// Identification of variable associated with the input
    /// </summary>
    public int VariableID;

    /// <summary>
    /// Variable object associated with this input
    /// </summary>
    public DDDVariable Variable;

    /// <summary>
    /// List of all elementary variables associated with this input.
    /// </summary>
    public List<DDDElemVar> ElemVariables;

    /// <summary>
    /// Default protected constructor
    /// </summary>
    protected DDDInput()
    {
    }

    /// <summary>
    /// Creates new object from XML element
    /// </summary>
    /// <param name="inpElement">Input element from DDDObjects XML file</param>
    /// <param name="dddVer">DDDVersion object to which variable belongs</param>
    /// <returns>new DDDInput object</returns>
    public static DDDInput CreateFromDDDObjXML(XmlElement inpElement, DDDVersion dddVer)
    {
      try
      {
        DDDInput inp;
        if ( (inpElement.GetAttribute("SamplingPeriodms") == "") || (inpElement.GetAttribute("SamplingPeriodms") == "0") )
          inp = new DDDSimpleInput();
        else
          inp = new DDDHistorizedInput();

        //Initialize input
        inp.InitializeFromDDDObjXML(inpElement, dddVer);

        return inp;
      }
      catch (Exception ex)
      {
        throw new DDDObjectException(ex, "Failed to create DDDInput object");
      }
    }

    /// <summary>
    /// Initializes instance data from XML element
    /// </summary>
    /// <param name="inpElement">xml element from DDDObjects document</param>
    /// <param name="dddVer">DDDVersion object to which record belongs</param>
    protected virtual void InitializeFromDDDObjXML(XmlElement inpElement, DDDVersion dddVer)
    {
      //Initialize values of all attributes
      RecordDefID = Convert.ToInt32(inpElement.GetAttribute("RecordDefID"));
      InputIndex = Convert.ToInt32(inpElement.GetAttribute("InputIndex"));
      VariableID = Convert.ToInt32(inpElement.GetAttribute("VariableID"));

      //Obtain associated variable
      Variable = dddVer.GetVariable(VariableID);
      //Obtain list of elementary variables
      ElemVariables = Variable.GetElementaryVars();

      //Add input to proper record
      dddVer.GetRecord(RecordDefID).AddInput(InputIndex, this);
    }

    /// <summary>
    /// Parses part of binary data block starting at given dataOffset and advances the offset by total input length.
    /// Parsed values are added to parsedValues list
    /// </summary>
    /// <param name="recBinData">Structure with binary data and record instance information</param>
    /// <param name="parsedValues">List into which parsed values are added</param>
    /// <param name="dataOffset">Offset of input in binary block. Offset is advanced by total input length.</param>
    /// <returns>Result of parsing operation</returns>
    public virtual eParseStatus ParseBinData(sRecBinData recBinData, ref List<sParsedVarValue> parsedValues, ref int dataOffset)
    {
      //virtual implementation - return success
      return eParseStatus.parseSuccessfull;
    }

    /// <summary>
    /// Parses part of binary data block starting at given dataOffset and advances the offset by total input length.
    /// Only values usable for referential attributes are parsed:
    /// -from simple inputs 
    /// -elements 0 of past start array in historized inputs
    /// -values convertible to float
    /// All values are converted to float
    /// Parsed values are added to dictionary with VariableID as a key
    /// </summary>
    /// <param name="recBinData">Structure with binary data and record instance information</param>
    /// <param name="parsedValues">Dictionary with parsed float values (key is VariableID)</param>
    /// <param name="dataOffset">Offset of input in binary block. Offset is advanced by total input length.</param>
    /// <returns>Result of parsing operation</returns>
    public virtual eParseStatus ParseRefAttrData(sRecBinData recBinData, ref Dictionary<int, float> parsedValues, ref int dataOffset)
    {
      //virtual implementation - return success
      return eParseStatus.parseSuccessfull;
    }
  }

  /// <summary>
  /// Simple input storing only one value of associated variable
  /// </summary>
  [Serializable]
  public class DDDSimpleInput : DDDInput
  {
    /// <summary>
    /// Default public constructor
    /// </summary>
    public DDDSimpleInput()
    {
    }

    /// <summary>
    /// Parses part of binary data block starting at given dataOffset and advances the offset by total input length.
    /// Parsed values are added to parsedValues list
    /// </summary>
    /// <param name="recBinData">Structure with binary data and record instance information</param>
    /// <param name="parsedValues">List into which parsed values are added</param>
    /// <param name="dataOffset">Offset of input in binary block. Offset is advanced by total input length.</param>
    /// <returns>Result of parsing operation</returns>
    public override eParseStatus ParseBinData(sRecBinData recBinData, ref List<sParsedVarValue> parsedValues, ref int dataOffset)
    {
      eParseStatus parseRes = eParseStatus.parseSuccessfull;
      //correct dataOffset alignement
      dataOffset = ((dataOffset + Variable.Alignement - 1) / Variable.Alignement) * Variable.Alignement;
      //simple input - only one sample of each elementary variable - call variables parse methods
      foreach (DDDElemVar elemVar in ElemVariables)
        if (
          (parseRes = elemVar.ParseBinData(
            recBinData, ref parsedValues, dataOffset, InputIndex, recBinData.StartTime, 0, 1
           )) != eParseStatus.parseSuccessfull)
          return parseRes;
      //move offset by Variable length
      dataOffset += Variable.Size;
      return parseRes;
    }

    /// <summary>
    /// Parses part of binary data block starting at given dataOffset and advances the offset by total input length.
    /// Only values usable for referential attributes are parsed:
    /// -from simple inputs 
    /// -elements 0 of past start array in historized inputs
    /// -values convertible to float
    /// All values are converted to float
    /// Parsed values are added to dictionary with VariableID as a key
    /// </summary>
    /// <param name="recBinData">Structure with binary data and record instance information</param>
    /// <param name="parsedValues">Dictionary with parsed float values (key is VariableID)</param>
    /// <param name="dataOffset">Offset of input in binary block. Offset is advanced by total input length.</param>
    /// <returns>Result of parsing operation</returns>
    public override eParseStatus ParseRefAttrData(sRecBinData recBinData, ref Dictionary<int, float> parsedValues, ref int dataOffset)
    {
      eParseStatus parseRes;
      float parsedVal;
      //correct dataOffset alignement
      dataOffset = ((dataOffset + Variable.Alignement - 1) / Variable.Alignement) * Variable.Alignement;
      //simple input - only one sample of each elementary variable - call variables parse methods
      foreach (DDDElemVar elemVar in ElemVariables)
      { 
        if (!parsedValues.ContainsKey(elemVar.VariableID))
        {
          parseRes = elemVar.ParseRefAttrData(recBinData, dataOffset, out parsedVal);
          if (parseRes == eParseStatus.parseSuccessfull)
            parsedValues.Add(elemVar.VariableID, parsedVal);
          else if (parseRes != eParseStatus.notParsed)
            return parseRes;
        }
      }
      //move offset by Variable length
      dataOffset += Variable.Size;
      return eParseStatus.parseSuccessfull;
    }
  }

  /// <summary>
  /// Input for event record storing array of samples for associated variable
  /// </summary>
  [Serializable]
  public class DDDHistorizedInput : DDDInput
  {
    /// <summary>
    /// Sampling period
    /// </summary>
    public int SamplingPeriodms;

    /// <summary>
    /// Number of sapmles stored before event start
    /// </summary>
    public int BeforeStart;

    /// <summary>
    /// Number of sapmles stored after event start
    /// </summary>
    public int PastStart;

    /// <summary>
    /// Number of sapmles stored before event end
    /// </summary>
    public int BeforeEnd;

    /// <summary>
    /// Number of sapmles stored after event end
    /// </summary>
    public int PastEnd;

    /// <summary>
    /// Default public constructor
    /// </summary>
    public DDDHistorizedInput()
    {
    }

    /// <summary>
    /// Initializes instance data from XML element
    /// </summary>
    /// <param name="inpElement">xml element from DDDObjects document</param>
    /// <param name="dddVer">DDDVersion object to which record belongs</param>
    protected override void InitializeFromDDDObjXML(XmlElement inpElement, DDDVersion dddVer)
    {
      //Call base class initialization
      base.InitializeFromDDDObjXML(inpElement, dddVer);

      //Initialize values of attributes applicable only to Historized input
      String attribute;
      SamplingPeriodms = Convert.ToInt32(inpElement.GetAttribute("SamplingPeriodms"));
      if ((attribute = inpElement.GetAttribute("BeforeStart")) != "")
        BeforeStart = Convert.ToInt32(attribute);
      if ((attribute = inpElement.GetAttribute("PastStart")) != "")
        PastStart = Convert.ToInt32(attribute);
      if ((attribute = inpElement.GetAttribute("BeforeEnd")) != "")
        BeforeEnd = Convert.ToInt32(attribute);
      if ((attribute = inpElement.GetAttribute("PastEnd")) != "")
        PastEnd = Convert.ToInt32(attribute);
    }

    /// <summary>
    /// Parses part of binary data block starting at given dataOffset and advances the offset by total input length.
    /// Parsed values are added to parsedValues list
    /// </summary>
    /// <param name="recBinData">Structure with binary data and record instance information</param>
    /// <param name="parsedValues">List into which parsed values are added</param>
    /// <param name="dataOffset">Offset of input in binary block. Offset is advanced by total input length.</param>
    /// <returns>Result of parsing operation</returns>
    public override eParseStatus ParseBinData(sRecBinData recBinData, ref List<sParsedVarValue> parsedValues, ref int dataOffset)
    {
      //historized input - several samples of variable in four arrays
      DateTime timeStamp;
      int valueIndex = 0;
      eParseStatus parseRes = eParseStatus.parseSuccessfull;
      //correct dataOffset alignement
      dataOffset = ((dataOffset + Variable.Alignement - 1) / Variable.Alignement) * Variable.Alignement;
      
      for (int i = 0; i < BeforeStart; i++)
      { //Iterate through before start array
        //compute timeStamp
        timeStamp = recBinData.StartTime.AddMilliseconds((-1 * BeforeStart + i) * SamplingPeriodms);
        //Call parse method for eac elementary variable
        foreach (DDDElemVar elemVar in ElemVariables)
          if ((parseRes =
            elemVar.ParseBinData(recBinData, ref parsedValues, dataOffset,
              InputIndex, timeStamp, valueIndex, 0)) != eParseStatus.parseSuccessfull)
            return parseRes;
        valueIndex++;
        //move offset by Variable length
        dataOffset += Variable.Size;
      }
      for (int i = 0; i < PastStart; i++)
      { //Iterate through past start array
        //compute timeStamp
        timeStamp = recBinData.StartTime.AddMilliseconds(i * SamplingPeriodms);
        //Call parse method for eac elementary variable
        foreach (DDDElemVar elemVar in ElemVariables)
          if ((parseRes =
            elemVar.ParseBinData(recBinData, ref parsedValues, dataOffset,
              InputIndex, timeStamp, valueIndex, 1)) != eParseStatus.parseSuccessfull)
            return parseRes;
        valueIndex++;
        //move offset by Variable length
        dataOffset += Variable.Size;
      }
      if (recBinData.EndTime != DateTime.MinValue)
      { //Do not parse data for invalid end time
        for (int i = 0; i < BeforeEnd; i++)
        { //Iterate through before end array
          //compute timeStamp
          timeStamp = recBinData.EndTime.AddMilliseconds((-1 * BeforeEnd + i) * SamplingPeriodms);
          //Call parse method for eac elementary variable
          foreach (DDDElemVar elemVar in ElemVariables)
            if ((parseRes =
              elemVar.ParseBinData(recBinData, ref parsedValues, dataOffset,
                InputIndex, timeStamp, valueIndex, 2)) != eParseStatus.parseSuccessfull)
              return parseRes;
          valueIndex++;
          //move offset by Variable length
          dataOffset += Variable.Size;
        }
        for (int i = 0; i < PastEnd; i++)
        { //Iterate through past end array
          //compute timeStamp
          timeStamp = recBinData.EndTime.AddMilliseconds(i * SamplingPeriodms);
          //Call parse method for eac elementary variable
          foreach (DDDElemVar elemVar in ElemVariables)
            if ((parseRes =
              elemVar.ParseBinData(recBinData, ref parsedValues, dataOffset,
                InputIndex, timeStamp, valueIndex, 3)) != eParseStatus.parseSuccessfull)
              return parseRes;
          valueIndex++;
          //move offset by Variable length
          dataOffset += Variable.Size;
        }
      }
      return parseRes;
    }

    /// <summary>
    /// Parses part of binary data block starting at given dataOffset and advances the offset by total input length.
    /// Only values usable for referential attributes are parsed:
    /// -from simple inputs 
    /// -elements 0 of past start array in historized inputs
    /// -values convertible to float
    /// All values are converted to float
    /// Parsed values are added to dictionary with VariableID as a key
    /// </summary>
    /// <param name="recBinData">Structure with binary data and record instance information</param>
    /// <param name="parsedValues">Dictionary with parsed float values (key is VariableID)</param>
    /// <param name="dataOffset">Offset of input in binary block. Offset is advanced by total input length.</param>
    /// <returns>Result of parsing operation</returns>
    public override eParseStatus ParseRefAttrData(sRecBinData recBinData, ref Dictionary<int, float> parsedValues, ref int dataOffset)
    {
      eParseStatus parseRes;
      float parsedVal;
      //correct dataOffset alignement
      dataOffset = ((dataOffset + Variable.Alignement - 1) / Variable.Alignement) * Variable.Alignement;
      //historized input, for referential attributes we try to parse only first elements of PastStart arrays
      int parseOffset = dataOffset + BeforeStart * Variable.Size;
      //move offset by Variable length for each element
      dataOffset += Variable.Size * (BeforeStart + PastStart + BeforeEnd + PastEnd);
      if (PastStart < 1)
        return eParseStatus.parseSuccessfull; //There are no PastStart elements - no parsing performed
      foreach (DDDElemVar elemVar in ElemVariables)
      {
        if (!parsedValues.ContainsKey(elemVar.VariableID))
        {
          parseRes = elemVar.ParseRefAttrData(recBinData, parseOffset, out parsedVal);
          if (parseRes == eParseStatus.parseSuccessfull)
            parsedValues.Add(elemVar.VariableID, parsedVal);
          else if (parseRes != eParseStatus.notParsed)
            return parseRes;
        }
      }
      return eParseStatus.parseSuccessfull;
    }
  }
}
