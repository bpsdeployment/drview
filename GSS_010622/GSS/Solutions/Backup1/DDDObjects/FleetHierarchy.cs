using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Runtime.Serialization;

namespace FleetHierarchies
{
  /// <summary>
  /// Class represents one version of fleet hierarchy
  /// </summary>
  [Serializable]
  public class FleetHierarchy
  {
    #region Private members
    //Dictionary of item names and hierarchy items
    [NonSerialized]
    Dictionary<String, FleetHierarchyItem> namesDict;
    //Dictionary of item IDs and hierarchy items
    [NonSerialized]
    Dictionary<int, FleetHierarchyItem> idsDict;
    //Top item of fleet hierarchy
    [NonSerialized]
    FleetHierarchyItem topItem;
    //String containing hierarchy in the form of XML - used for serialization
    String hierarchyXML = null;
    #endregion //Private members

    #region Public properties
    /// <summary>
    /// Date from which hierarchy version isi valid
    /// </summary>
    [NonSerialized]
    public DateTime ValidFrom;
    /// <summary>
    /// Date to which hierarchy version is valid
    /// </summary>
    [NonSerialized]
    public DateTime ValidTo;
    /// <summary>
    /// ID of hierarchy version
    /// </summary>
    [NonSerialized]
    public int VersionID;

    /// <summary>
    /// Top item of fleet hierarchy (usually operator item)
    /// </summary>
    public FleetHierarchyItem TopItem
    {
      get { return topItem; }
    }
    #endregion //Public properties

    #region Construction initialization
    /// <summary>
    /// Creates internal fleet hierarchy structure according to given XML document
    /// Creates dictionaries of item names and item IDs pointing to hierarchy items.
    /// </summary>
    /// <param name="hierarchyDoc">XML document with hierarchy</param>
    public FleetHierarchy(XmlDocument hierarchyDoc)
    {
      //Initialize object from XML document
      InitializeFromXMLDocument(hierarchyDoc);
    }
    #endregion //Construction initialization

    #region Public methods
    /// <summary>
    /// Obtains fleet hierarchy item from hierarchy tree
    /// </summary>
    /// <param name="itemID">ID of requested item</param>
    /// <returns>Hierarchy item with given ID</returns>
    public FleetHierarchyItem GetHierarchyItem(int itemID)
    {
      FleetHierarchyItem item;
      if (idsDict.TryGetValue(itemID, out item))
        return item;
      throw new Exception(String.Format("Hierarchy item with ID {0} not found in hierarchy version {1}", itemID, VersionID));
    }

    /// <summary>
    /// Obtains fleet hierarchy item from hierarchy tree
    /// </summary>
    /// <param name="itemName">Name of requested item</param>
    /// <returns>Hierarchy item with given name</returns>
    public FleetHierarchyItem GetHierarchyItem(String itemName)
    {
      FleetHierarchyItem item;
      if (namesDict.TryGetValue(itemName, out item))
        return item;
      throw new Exception(String.Format("Hierarchy item {0} not found in hierarchy version {1}", itemName, VersionID));
    }

    /// <summary>
    /// Obtain XML document fully describing this hierarchy version
    /// </summary>
    /// <returns>XML document with description</returns>
    public XmlDocument GetXMLDocument()
    {
      if (topItem == null)
        throw new Exception("No version loaded in FleetHierarchy object");
      //Create empty XML document
      XmlDocument doc = new XmlDocument();
      //Create root element with hierarchy tree
      XmlElement docElem = topItem.CreateXMLElement(doc);
      //Set attributes to root element
      docElem.SetAttribute("validFrom", ValidFrom.ToString("s"));
      docElem.SetAttribute("validTo", ValidTo.ToString("s"));
      docElem.SetAttribute("versionID", VersionID.ToString());
      //Set document element
      doc.AppendChild(docElem);
      return doc;
    }

    /// <summary>
    /// Returns dictionary of hierarchy item names and item objects
    /// </summary>
    /// <returns>Dictionary with item names</returns>
    public Dictionary<String, FleetHierarchyItem> GetHierarchyItemNamesDictionary()
    {
      return namesDict;
    }

    /// <summary>
    /// Returns dictionary of hierarchy item IDs and item objects
    /// </summary>
    /// <returns>Dictionary with item IDs</returns>
    public Dictionary<int, FleetHierarchyItem> GetHierarchyItemIDsDictionary()
    {
      return idsDict;
    }
    #endregion //Public methods

    #region Helper methods
    /// <summary>
    /// Initialize version hierarchy from given XML document
    /// </summary>
    /// <param name="hierarchyDoc">XML document with hierarchy</param>
    protected void InitializeFromXMLDocument(XmlDocument hierarchyDoc)
    {
      //Store version params from root element
      String tmpAttr = hierarchyDoc.DocumentElement.GetAttribute("validFrom");
      if (tmpAttr != String.Empty)
        this.ValidFrom = Convert.ToDateTime(tmpAttr);
      else
        this.ValidFrom = DateTime.MinValue;
      tmpAttr = hierarchyDoc.DocumentElement.GetAttribute("validTo");
      if (tmpAttr != String.Empty)
        this.ValidTo = Convert.ToDateTime(tmpAttr);
      else
        this.ValidTo = DateTime.MaxValue;
      tmpAttr = hierarchyDoc.DocumentElement.GetAttribute("versionID");
      if (tmpAttr != String.Empty)
        this.VersionID = Convert.ToInt32(tmpAttr);
      else
        this.VersionID = -1;
      //Create dictionaries
      namesDict = new Dictionary<string, FleetHierarchyItem>();
      idsDict = new Dictionary<int, FleetHierarchyItem>();
      //Build hierarchy tree
      topItem = FleetHierarchyItem.CreateFromXmlElem(hierarchyDoc.DocumentElement, null, ref namesDict, ref idsDict);
    }
    #endregion //Helper methods

    #region Serialization
    /// <summary>
    /// Method called before the serialization
    /// </summary>
    [OnSerializing()]
    internal void OnSerializingMethod(StreamingContext context)
    {
      //Store hierarchy in the form of XML document
      this.hierarchyXML = GetXMLDocument().InnerXml;
    }

    /// <summary>
    /// Method called after the serialization
    /// </summary>
    [OnSerialized()]
    internal void OnSerializedMethod(StreamingContext context)
    {
      //Free XML document - not needed any more
      this.hierarchyXML = null;
    }

    /// <summary>
    /// Method called after deserialization
    /// </summary>
    [OnDeserialized()]
    internal void OnDeserializedMethod(StreamingContext context)
    {
      //Check for XML document
      if (this.hierarchyXML == null)
        throw new Exception("Cannot deserialize fleet hierarchy from emty document");
      //Build hierarchy from XML document
      XmlDocument doc = new XmlDocument();
      doc.LoadXml(this.hierarchyXML);
      InitializeFromXMLDocument(doc);
      //Free XML document - not needed any more
      this.hierarchyXML = null;
    }
    #endregion //Serialization
  }
}
