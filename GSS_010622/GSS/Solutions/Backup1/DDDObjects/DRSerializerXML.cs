using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Xml;

namespace DDDObjects
{
  /// <summary>
  /// Class provides the posibility to transfer diagnostic records through byte stream together with necessary DDD helper objects
  /// </summary>
  public class DRSerializerXML : DRSerializerBase
  {
    #region Private members
    XmlReader xmlReader; //Reader for XML in the stream
    bool bigEndianData; //Flag of data byte order
    #endregion //Private members

    #region Public members
    /// <summary>
    /// Identifier of TU from which stream originated
    /// </summary>
    public Guid TUInstanceID;
    /// <summary>
    /// Identifier of DDD version for records in stream
    /// </summary>
    public Guid DDDVersionID;
    #endregion

    #region Construction, initialization
    /// <summary>
    /// Creates new diagnostic record serializer object
    /// </summary>
    /// <param name="dddHelper">Helper class for DDD objects</param>
    /// <param name="stream">Stream to read from or write to</param>
    /// <param name="output">Indicates that records will be written into the stream</param>
    public DRSerializerXML(DDDHelper dddHelper, Stream stream, bool output)
      : base(dddHelper, stream, output)
    {
      //Only reading from XML is supported
      if (output)
        throw new NotImplementedException("Writing to XML not supported");
      //Open XML reader over compressed stream
      xmlReader = XmlReader.Create(zipStream);
      //Move to DD element
      if (!xmlReader.ReadToFollowing("DD"))
        throw new Exception("Element DD not found");
      //Store attributes from DD element
      TUInstanceID = new Guid(xmlReader.GetAttribute("TUInstanceID"));
      DDDVersionID = new Guid(xmlReader.GetAttribute("DDDVersionID"));
      bigEndianData = Convert.ToBoolean(xmlReader.GetAttribute("BigEndianData"));
      //Position reader on first record
      xmlReader.ReadToDescendant("Record");
    }
    #endregion //Construction, initialization

    #region Public methods
    /// <summary>
    /// Deserializes next record from given stream, remembers used DDD objects
    /// </summary>
    /// <returns>Deserialized diagnostic record</returns>
    public override DiagnosticRecord ReadRecordFromStream()
    {
      DiagnosticRecord record = new DiagnosticRecord();
      String attribute;
      //Move reader to next record in the stream
      if (xmlReader.Name != "Record")
        return null;

      try
      {
        //Fill record structure from attributes
        record.BigEndian = bigEndianData;
        record.DDDVersionID = DDDVersionID;
        if ((attribute = xmlReader.GetAttribute("EndTime")) != null)
          record.EndTime = Convert.ToDateTime(attribute).ToUniversalTime();
        else
          record.EndTime = DateTime.MinValue;
        record.LastModified = Convert.ToDateTime(xmlReader.GetAttribute("LastModified")).ToUniversalTime();
        record.ParseStatusID = 0;
        record.RecordDefID = Convert.ToInt32(xmlReader.GetAttribute("RecordDefID"));
        record.RecordInstID = Convert.ToInt64(xmlReader.GetAttribute("RecordInstID"));
        record.Source = xmlReader.GetAttribute("Source");
        record.StartTime = Convert.ToDateTime(xmlReader.GetAttribute("StartTime")).ToUniversalTime();
        record.TUInstanceID = TUInstanceID;
        record.HierarchyItemID = -1;
        if ((attribute = xmlReader.GetAttribute("ImportTime")) != null)
          record.ImportTime = Convert.ToDateTime(attribute).ToUniversalTime();
        else
          record.ImportTime = DateTime.MinValue;
        if ((attribute = xmlReader.GetAttribute("AcknowledgeTime")) != null)
          record.AcknowledgeTime = Convert.ToDateTime(attribute).ToUniversalTime();
        else
          record.AcknowledgeTime = DateTime.MinValue;
        if ((attribute = xmlReader.GetAttribute("VehicleNumber")) != null)
          record.VehicleNumber = Convert.ToInt16(attribute);
        else
          record.VehicleNumber = -1;
        if ((attribute = xmlReader.GetAttribute("DeviceCode")) != null)
          record.DeviceCode = Convert.ToInt16(attribute);
        else
          record.DeviceCode = -1;
        String base64Data = xmlReader.ReadElementContentAsString();
        record.BinData = Convert.FromBase64String(base64Data);
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to read record from XML", ex);
      }
      //Remember used version and TUInst 
      RememberUsedDDDObj(record, true);
      //Position reader on next record
      xmlReader.ReadToNextSibling("Record");
      //Return record
      return record;
    }

    /// <summary>
    /// Reads stored DDD objects from stream. In case not all necessary objects are available in the stream, exception is thrown.S
    /// </summary>
    public override void ReadUsedDDDObjects()
    {
      //Try to read and store DDD version
      try
      {
        //Move to DDD element
        if (!xmlReader.ReadToFollowing("DDD"))
          throw new Exception("Missing element DDD");
        //Create DDD XML document from DDD node
        XmlDocument dddDoc = new XmlDocument();
        String strDDD = xmlReader.ReadOuterXml(); 
        dddDoc.LoadXml(strDDD);
        //Parse XML document and create DDD version
        DDDVersion dddVer = DDDRepository.CreateObjectsFromDDDXML(dddDoc, DDDVersionID);
        //Add version to repository
        DDDHelper.AddDDDVersion(dddVer);
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to read DDD version from file", ex);
      }

      //Try to read TUType and TUInstance
      try
      {
        if (!xmlReader.ReadToFollowing("TUInstanceInfo"))
          throw new Exception("Missing element TUInstanceInfo");
        //Read TUInstance parameters
        String tuName = xmlReader.GetAttribute("Name");
        String tuComment = xmlReader.GetAttribute("Comment");
        //Move to TUType
        if (!xmlReader.ReadToDescendant("TUType"))
          throw new Exception("Missing element TUType");
        //Construct TUType object
        TUType tuType = new TUType(new Guid(xmlReader.GetAttribute("TypeID")), xmlReader.GetAttribute("Name"), xmlReader.GetAttribute("Comment"));
        //Construct TUInstance object
        TUInstance tuInst = new TUInstance(TUInstanceID, DDDVersionID, tuName, 0, tuComment, tuType);
        //Add TUType if it already does not exists
        try { DDDHelper.GetTUType(tuType.TUTypeID); }
        catch (Exception) { DDDHelper.AddTUType(tuType); }
        //Add TUInstance if it already does not exists
        try { DDDHelper.GetTUInstance(TUInstanceID); }
        catch (Exception) { DDDHelper.AddTUInstance(tuInst); }
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to read TU information from file", ex);
      }
      //Check if used DDD objects are available
      CheckUsedDDDObjAvailable();
    }
    #endregion //Public methods
  }
}