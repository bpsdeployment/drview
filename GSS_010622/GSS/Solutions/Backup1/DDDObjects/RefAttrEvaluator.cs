using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Collections.Generic;
using System.Runtime.Serialization;

//Namespase contains classes for evaluation of referential attributes expressions
namespace RefAttrExpressions
{
  /// <summary>
  /// Class represents evaluator of one set of expressions
  /// One class shall correspond to one DDD Version
  /// </summary>
  [Serializable]
  public class RefAttrEvaluator
  {

    #region Private members
    private String evaluatorName;
    [NonSerialized]
    private Object _Compiled = null;
    [NonSerialized]
    private bool compilationTried = false;
    private Dictionary<int, RefAttrExpression> refAttrExpressions;
    #endregion //Private members

    #region Construction
    /// <summary>
    /// Public constructor of expression evaluator
    /// </summary>
    /// <param name="name">Unique name of the evaluator</param>
    public RefAttrEvaluator(String name)
    {
      evaluatorName = name;
      refAttrExpressions = new Dictionary<int, RefAttrExpression>();
    }
    #endregion //Construction

    #region Initialization members
    /// <summary>
    /// Add new expression for one referential attribute to the evaluation
    /// </summary>
    /// <param name="mappingIndex">Index of referential attribute mapping from DB</param>
    /// <param name="expression">Expression which evaluates to the value of referential attribute</param>
    public void AddRefAttrExpression(int mappingIndex, String expression)
    {
      String methodName = "RAM" + mappingIndex.ToString();
      RefAttrExpression exp = new RefAttrExpression(expression, methodName);
      //Remove old expression with same ID if exists
      if (refAttrExpressions.ContainsKey(mappingIndex))
        refAttrExpressions.Remove(mappingIndex);
      refAttrExpressions.Add(mappingIndex, exp);
    }

    /// <summary>
    /// Performs test in memory compilation of given expression to check the syntax
    /// </summary>
    /// <param name="expression">C# expression to test</param>
    public void TestCompileExpression(String expression)
    {
      //Create test expression
      RefAttrExpression exp = new RefAttrExpression(expression, "TestMethod");
      //Create test class source
      StringBuilder code = new StringBuilder();
      code.Append("using System; \n");
      code.Append("using System.Data; \n");
      code.Append("using System.Xml; \n\n");
      code.Append("namespace DynExprClasses\n{\n");
      code.AppendFormat("  public class TestClass\n");
      code.Append("  {\n");
      code.Append(exp.GetExprMethodSource());
      code.Append("  }\n}\n");
      //Create compile parameters
      CompilerParameters cp = new CompilerParameters();
      cp.ReferencedAssemblies.Add("system.dll");
      cp.ReferencedAssemblies.Add("system.data.dll");
      cp.ReferencedAssemblies.Add("system.xml.dll");
      cp.GenerateExecutable = false;
      cp.GenerateInMemory = true;
      //Try to compile the test class
      CompilerResults cr = CSharpCodeProvider.CreateProvider("CSharp").CompileAssemblyFromSource(cp, code.ToString());
      if (cr.Errors.HasErrors)
      {
        StringBuilder error = new StringBuilder();
        error.AppendFormat("Test compilation of expression {0} failed:\n", expression);
        foreach (System.CodeDom.Compiler.CompilerError err in cr.Errors)
          error.AppendFormat("{0}\n", err.ErrorText);
        throw new Exception(error.ToString());
      }
    }

    /// <summary>
    /// Returns number of defined referential attribute expressions
    /// </summary>
    /// <returns>number of defined referential attribute expressions</returns>
    public int GetRefAttrExprCount()
    {
      return refAttrExpressions.Count;
    }

    /// <summary>
    /// Compiles internal evaluator class after all the expressions are added.
    /// Class has to be compiled, before evaluation is performed
    /// </summary>
    public void Compile()
    {
      try
      {
        compilationTried = true;
        CompilerParameters cp = new CompilerParameters();
        cp.ReferencedAssemblies.Add("system.dll");
        cp.ReferencedAssemblies.Add("system.data.dll");
        cp.ReferencedAssemblies.Add("system.xml.dll");
        cp.GenerateExecutable = false;
        cp.GenerateInMemory = true;

        StringBuilder code = new StringBuilder();
        code.Append("using System; \n");
        code.Append("using System.Data; \n");
        code.Append("using System.Xml; \n\n");
        code.Append("namespace DynExprClasses\n{\n");
        code.AppendFormat("  public class {0}\n", evaluatorName);
        code.Append("  {\n");
        foreach (RefAttrExpression item in refAttrExpressions.Values)
          code.Append(item.GetExprMethodSource());
        code.Append("  }\n}\n");

        CompilerResults cr = CSharpCodeProvider.CreateProvider("CSharp").CompileAssemblyFromSource(cp, code.ToString());
        if (cr.Errors.HasErrors)
        {
          StringBuilder error = new StringBuilder();
          foreach (System.CodeDom.Compiler.CompilerError err in cr.Errors)
            error.AppendFormat("{0}\n", err.ErrorText);
          throw new Exception(error.ToString());
        }
        _Compiled = cr.CompiledAssembly.CreateInstance("DynExprClasses." + evaluatorName);
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to compile the evaluator class", ex);
      }
    }
    #endregion //Initialization Members

    #region Evaluation Members
    /// <summary>
    /// Evaluates expression of specified referential attribute mapping
    /// Passes the parameters to the evaluation
    /// </summary>
    /// <param name="mappingIndex">Index of referential attribute mapping</param>
    /// <param name="parameters">Parameters for the evaluation</param>
    /// <returns>Evaluated value</returns>
    public float EvaluateRefAttr(int mappingIndex, object[] parameters)
    {
      RefAttrExpression expr;
      if (!refAttrExpressions.TryGetValue(mappingIndex, out expr))
        throw new Exception("Unknown referential attribute mapping index " + mappingIndex.ToString());
      return (float)ExecuteMethod(expr.Name, parameters);
    }

    /// <summary>
    /// Returns list of parameters for expression associated with specified referential attribute mapping
    /// </summary>
    /// <param name="mappingIndex">Index of referential attribute mapping</param>
    /// <returns>List of expression parameters</returns>
    public List<sExprParam> GetRefAttrExprParameters(int mappingIndex)
    {
      RefAttrExpression expr;
      if (!refAttrExpressions.TryGetValue(mappingIndex, out expr))
        throw new Exception("Unknown referential attribute mapping " + mappingIndex.ToString());
      return expr.Parameters;
    }

    /// <summary>
    /// Evaluates value of specified referential attribute mapping from values of environmental variables.
    /// Values of env vars are passed in the form of dictionary, where key is VariableID.
    /// Returns true in case of successfull evaluation.
    /// In case of failure, refAttrVal contains NaN value.
    /// </summary>
    /// <param name="mappingIndex">Index of referential attribute mapping to evaluate</param>
    /// <param name="varValues">Dictionary of environmental variables values</param>
    /// <param name="refAttrVal">Evaluated value of referential attribute or NaN in case of failure</param>
    /// <returns>true in case of successfull evaluation</returns>
    public bool EvaluateRefAttr(int mappingIndex, Dictionary<int, float> varValues, out float refAttrVal)
    {
      //Preset output value
      refAttrVal = Single.NaN;
      //Obtain list of necessary environmental variable values for evaluation
      List<sExprParam> expParams = GetRefAttrExprParameters(mappingIndex);
      //Create array of parameters for evaluation
      object[] parameters = new object[expParams.Count];
      //Fill array with actual values
      float val;
      for (int i = 0; i < expParams.Count; i++)
      {
        if (!varValues.TryGetValue(expParams[i].VariableID, out val) ||
             expParams[i].ParamType != typeof(float))
          return false; //Specified parameter was not found in array of values, or it is not a float
        parameters[i] = val; //assign value to parameters array
      }
      //Evaluate the expression
      refAttrVal = EvaluateRefAttr(mappingIndex, parameters);
      return true;
    }
    #endregion //Evaluation Members

    #region Helper methods
    /// <summary>
    /// Executes method with specified name in internal compiled class.
    /// Parameters are passed to the method
    /// </summary>
    /// <param name="name">Name of the method</param>
    /// <param name="parameters">Method parameters</param>
    /// <returns></returns>
    private object ExecuteMethod(string name, object[] parameters)
    {
      if (!compilationTried)
        Compile();
      if (_Compiled == null)
        return null;
      MethodInfo mi = _Compiled.GetType().GetMethod(name);
      return mi.Invoke(_Compiled, parameters);
    }

    /// <summary>
    /// Method called automatically after the deserialization of the object
    /// Compiles all contained expressions
    /// </summary>
    /// <param name="sc"></param>
    [OnDeserialized]
    private void CompileAfterDeserialization(StreamingContext sc)
    {
      _Compiled = null;
      compilationTried = false;
    }
    #endregion //Helper methods
  }

  /// <summary>
  /// Represents one expression to evaluate.
  /// Parameters has to be in a form parXXXf or parXXXi or parXXXb 
  /// where XXX represents integer number - VariableID
  /// f stands for float, i for integer (Int32), b for bool
  /// Expression is transformed to the form of class method, which is called upon evaluation
  /// </summary>
  [Serializable]
  internal class RefAttrExpression
  {
    #region Public and private members
    public string Name;
    public string Expression;
    public List<sExprParam> Parameters;
    #endregion

    #region Construction
    /// <summary>
    /// Construction
    /// Initialize the expression, parse the parameters
    /// </summary>
    /// <param name="expression"></param>
    /// <param name="name"></param>
    public RefAttrExpression(String expression, String name)
    {
      Expression = expression;
      Name = name;
      Parameters = new List<sExprParam>();
      ExtractParameters(expression);
    }
    #endregion

    #region Helper methods
    /// <summary>
    /// Extracts and creates list of all parameters from specified expression
    /// Parameters has to be in a form parXXXf or parXXXi or parXXXb 
    /// where XXX represents integer number - VariableID
    /// f stands for float, i for integer (Int32), b for bool
    /// </summary>
    /// <param name="expression">Expression from which parameters are extracted</param>
    private void ExtractParameters(String expression)
    {
      try
      {
        //Locate all parameters in expression
        Regex rx = new Regex("[pv]ar([0-9])+[fib]", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        foreach (Match parMatch in rx.Matches(expression))
        { //Create new parameter structure for each match
          sExprParam param = new sExprParam();
          param.ParamName = parMatch.Value;
          switch (parMatch.Value[parMatch.Length - 1])
          {
            case 'f':
              param.ParamType = typeof(float);
              break;
            case 'i':
              param.ParamType = typeof(Int32);
              break;
            case 'b':
              param.ParamType = typeof(bool);
              break;
            default:
              param.ParamType = typeof(float);
              break;
          }
          param.VariableID = Int32.Parse(parMatch.Value.Substring(3, parMatch.Length - 4));
          //Add parameter to the collection
          Parameters.Add(param);
        }
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to parse expression parameters", ex);
      }
    }

    /// <summary>
    /// Creates and returns source code representing method which will evaluate the expression.
    /// Method will contain parmameters according to list of extracted parameters.
    /// </summary>
    /// <returns></returns>
    public String GetExprMethodSource()
    {
      StringBuilder src = new StringBuilder();
      src.AppendFormat("    public float {0}(", Name);
      foreach (sExprParam par in Parameters)
        src.AppendFormat("{0} {1}, ", par.ParamType.Name, par.ParamName);
      src.Append(")\n");
      src.Append("    {\n");
      src.AppendFormat("      return ({0});\n", Expression);
      src.Append("    }\n");
      String code = src.ToString();
      code = code.Replace(", )", ")");
      return code;
    }
    #endregion
  }

  /// <summary>
  /// Defines one expression parameter.
  /// This parameter is than expected during the call to expression evaluation method
  /// </summary>
  [Serializable]
  public struct sExprParam
  {
    /// <summary>
    /// Name of the expression parameter
    /// </summary>
    public String ParamName;
    /// <summary>
    /// ID of variable, which is supposed to be this parameter
    /// </summary>
    public int VariableID;
    /// <summary>
    /// Data type of the parameter
    /// </summary>
    public Type ParamType;
  }

}
