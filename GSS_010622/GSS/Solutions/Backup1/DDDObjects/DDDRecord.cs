using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using RefAttrExpressions;

namespace DDDObjects
{
  /// <summary>
  /// List of all posible record triger sentivity types
  /// </summary>
  public enum TriggerSensitivity
  {
    /// <summary>
    /// Trigger reacts on leading edge of triggered signal
    /// </summary>
    LeadingEdge = 1,
    /// <summary>
    /// Trigger reacts on trailing edge of triggered signal
    /// </summary>
    TrailingEdge = 2,
    /// <summary>
    /// Trigger reacts on change of triggered signal
    /// </summary>
    Both = 3
  }

  /// <summary>
  /// Definition of fault priorities as specified in UIC 557
  /// </summary>
  public enum SeverityType
  {
    /// <summary>
    /// FFault requiring action during train running or before return to home railway
    /// </summary>
    A = 1,
    /// <summary>
    /// Fault requiring immediate action
    /// </summary>
    A1 = 2,
    /// <summary>
    /// Fault requiring action which may not be deffered until the next scheduled maintenance operation
    /// </summary>
    B = 3,
    /// <summary>
    /// Fault requiring action which may not be deffered until the next scheduled maintenance operation and may be acted upon by non-owning railways
    /// </summary>
    B1 = 4,
    /// <summary>
    /// All other faults
    /// </summary>
    C = 5
  }

  /// <summary>
  /// Definition of possible record categories
  /// </summary>
  [Flags]
  public enum eRecordCategory
  {
    /// <summary>
    /// Event record category
    /// </summary>
    Event = 0x02,
    /// <summary>
    /// Trace record category
    /// </summary>
    Trace = 0x04,
    /// <summary>
    /// Snap record category
    /// </summary>
    Snap = 0x08
  }

  /// <summary>
  /// Base class representing diagnostic record togehter with all the attributes
  /// </summary>
  [Serializable]
  public class DDDRecord
  {
    /// <summary>
    /// Record ID inside of DDD
    /// </summary>
    public int RecordDefID;

    /// <summary>
    /// Identification of record type
    /// </summary>
    public int RecordTypeID;

    /// <summary>
    /// Record name
    /// </summary>
    public String Name;

    /// <summary>
    /// ID of Trigger variable
    /// </summary>
    public int TriggerVarID;

    /// <summary>
    /// Type of trigger sensitivity
    /// </summary>
    public TriggerSensitivity TriggerType;

    /// <summary>
    /// User comment for record
    /// </summary>
    public String Comment;

    /// <summary>
    /// Trigger variable
    /// </summary>
    public DDDTriggerVar Trigger;

    /// <summary>
    /// Array of titles, one for each language
    /// </summary>
    private String[] titles;
    /// <summary>
    /// Title for currently selected language
    /// </summary>
    private String currentTitle;
    /// <summary>
    /// Record inputs
    /// </summary>
    public SortedList<int, DDDInput> Inputs;

    /// <summary>
    /// List of mappings between referential attributes and environmental variables
    /// defined for this record
    /// </summary>
    private List<DDDRefAttrMap> refAttrMappings; 

    /// <summary>
    /// Record title in selected language
    /// </summary>
    public String Title
    {
      get { return currentTitle; }
    }

    /// <summary>
    /// Protected constructor
    /// </summary>
    protected DDDRecord()
    {
      titles = new String[DDDRepository.LanguageCount];
      Inputs = new SortedList<int, DDDInput>();
      refAttrMappings = new List<DDDRefAttrMap>();
    }

    /// <summary>
    /// Creates new object from XML element
    /// </summary>
    /// <param name="recElement">Record element from DDDObjects XML file</param>
    /// <param name="dddVer">DDDVersion object to which variable belongs</param>
    /// <returns>new DDDRecord object</returns>
    public static DDDRecord CreateFromDDDObjXML(XmlElement recElement, DDDVersion dddVer)
    {
      try
      {
        DDDRecord rec;
        int RecordTypeID = Convert.ToInt32(recElement.GetAttribute("RecordTypeID"));
        switch (RecordTypeID)
        {
          case 1:
            rec = new DDDEventRecord();
            break;
          case 2:
            rec = new DDDTraceRecord();
            break;
          case 3:
            rec = new DDDSnapRecord();
            break;
          default:
            throw new DDDObjectException("Unkown RecordTypeID {0}", RecordTypeID);
        }

        //Initialize record
        rec.InitializeFromDDDObjXML(recElement, dddVer);

        return rec;
      }
      catch (Exception ex)
      {
        throw new DDDObjectException(ex, "Failed to create DDDRecord object");
      }
    }
    
    /// <summary>
    /// Initializes instance data from XML element
    /// </summary>
    /// <param name="recElement">xml element from DDDObjects document</param>
    /// <param name="dddVer">DDDVersion object to which record belongs</param>
    protected virtual void InitializeFromDDDObjXML(XmlElement recElement, DDDVersion dddVer)
    {
      //Initialize values of all attributes
      RecordDefID = Convert.ToInt32(recElement.GetAttribute("RecordDefID"));
      RecordTypeID = Convert.ToInt32(recElement.GetAttribute("RecordTypeID"));
      Name = recElement.GetAttribute("Name");
      TriggerVarID = Convert.ToInt32(recElement.GetAttribute("TriggerVarID"));
      TriggerType = TriggerSensitivity.LeadingEdge;
      Comment = recElement.GetAttribute("Comment");

      //Create array of titles
      //Select all title elements
      XmlNodeList titleElems = recElement.SelectNodes("./Title");
      Languages langID;
      String text;
      //Walk through and add to dicitonary each element
      foreach (XmlElement title in titleElems)
      {
        //Select attributes
        langID = (Languages)Convert.ToInt32(title.GetAttribute("LanguageID"));
        text = title.GetAttribute("Text");
        //Check language boundary
        if ((Int32)langID >= DDDRepository.LanguageCount)
          throw new DDDObjectException("Unknown language ID {0}", langID);
        //Add title to array
        titles[(Int32)langID] = text;
      }

      //Obtain trigger variable
      Trigger = dddVer.GetVariable(TriggerVarID) as DDDTriggerVar;
    }
    
    /// <summary>
    /// Adds new input to diagnostic record
    /// </summary>
    /// <param name="index">Ordinal index of the input in record</param>
    /// <param name="input">Input object</param>
    public void AddInput(int index, DDDInput input)
    {
      Inputs.Add(index, input);
    }

    /// <summary>
    /// Method prepares language - dependent properties for selected language.
    /// </summary>
    /// <param name="currentLang">selected language</param>
    /// <param name="fallbackLang">language to use if selected language is not available</param>
    /// <returns>True if all properties are defined for specified language</returns>
    public virtual bool SetCurrentLanguage(Languages currentLang, Languages fallbackLang)
    {
      //check if specified language exists
      if (titles[(int)currentLang] != null)
      {
        currentTitle = titles[(int)currentLang];
        return true;
      }
      else if (titles[(int)fallbackLang] != null)
        currentTitle = titles[(int)fallbackLang];
      else if (titles[(int)Languages.Default] != null)
        currentTitle = titles[(int)Languages.Default];
      else
      {
        foreach(String tmpTitle in titles)
          if (tmpTitle != null)
          {
            currentTitle = tmpTitle;
            break;
          }
      }
      return false;
    }

    /// <summary>
    /// Parses block of binary data of one record instance
    /// </summary>
    /// <param name="recBinData">Structure with binary data and instance information</param>
    /// <param name="parsedValues">List of parsed values. New list is created and returned.</param>
    /// <returns>Result of parsing operation</returns>
    public eParseStatus ParseBinData(sRecBinData recBinData, out List<sParsedVarValue> parsedValues)
    {
      int dataOffset = 0; //offset inside of binary block
      eParseStatus parseStat = eParseStatus.parseSuccessfull;
      //Create new list for parsed values
      parsedValues = new List<sParsedVarValue>();
      try
      {
          //go through each record input and parse input variables
        foreach (KeyValuePair<int, DDDInput> inpPair in Inputs)
        {
          //Parse input variable
          parseStat = inpPair.Value.ParseBinData(recBinData, ref parsedValues, ref dataOffset);
          //Check result
          if (parseStat != eParseStatus.parseSuccessfull)
            break; //parsing was not successfull
        }
      }
      catch (Exception)
      { return eParseStatus.unknownReason; }
      return parseStat;
    }

    /// <summary>
    /// Parses values of referential attributes of one record instance
    /// </summary>
    /// <param name="recBinData">Structure with binary data and instance information</param>
    /// <param name="expEvaluator">Preconfigured evaluator of referential attributes expressions</param>
    /// <param name="parsedValues">List of parsed values. New list is created and returned.</param>
    /// <returns>Result of parsing operation</returns>
    public eParseStatus ParseRefAttributes(sRecBinData recBinData, RefAttrEvaluator expEvaluator, out List<sParsedRefAttrValue> parsedValues)
    {
      //Create list for values of referential attributes
      parsedValues = new List<sParsedRefAttrValue>();

      //First parse values of referential attributes for all inputs
      int dataOffset = 0; //offset inside of binary block
      eParseStatus parseStat = eParseStatus.parseSuccessfull;
      //Create dictionary for parsed values of variables
      Dictionary<int, float> varValues = new Dictionary<int, float>();
      try
      {
        //go through each record input and parse input variables
        foreach (KeyValuePair<int, DDDInput> inpPair in Inputs)
        {
          //Parse input variable
          parseStat = inpPair.Value.ParseRefAttrData(recBinData, ref varValues, ref dataOffset);
          //Check result
          if (parseStat != eParseStatus.parseSuccessfull)
            return parseStat; //parsing was not successfull
        }
      }
      catch (Exception)
      { return eParseStatus.unknownReason; }

      //Go through mapping of referential attributes and try to find variable for each attribute
      foreach (DDDRefAttrMap attrMap in refAttrMappings)
      {
        float val;
        if ( ((attrMap.Expression == "") && varValues.TryGetValue(attrMap.VariableID, out val)) || //Attribute is mapped to single variable
             ((attrMap.Expression != "") && expEvaluator.EvaluateRefAttr(attrMap.MappingIndex, varValues, out val))) //Attribute is mapped to the expression 
        { //Sucessfully obtained value of referential attribute - add to output list
            sParsedRefAttrValue refAttrVal = new sParsedRefAttrValue();
            refAttrVal.RefAttrID = attrMap.RefAttrID;
            refAttrVal.Value = val;
            parsedValues.Add(refAttrVal);
        }
      }
      //Return status of parsing operation
      return parseStat;
    }

    /// <summary>
    /// Finds all mappings of referential attributes for this record.
    /// Referential attributes can be mapped only to elementary variables inside of simple inputs
    /// or in first element of Past Start array of historized input
    /// </summary>
    /// <param name="refAttrMappingsDDD">List of mappings defined for DDD Version</param>
    /// <param name="expEvaluator">Preconfigured evaluator of referential attributes expressions</param>
    public void FindRefAttrMappings(List<DDDRefAttrMap> refAttrMappingsDDD, RefAttrEvaluator expEvaluator)
    {
      //Clear any old mappings
      refAttrMappings.Clear();
      //Temporarily build list of all elementary variables associated with this record
      Dictionary<int, int> elemVars = new Dictionary<int, int>(); //Use dictionary jus because of searching capability
      //go through each record input and parse input variables
      foreach (KeyValuePair<int, DDDInput> inpPair in Inputs)
        foreach (DDDElemVar elemVar in inpPair.Value.Variable.GetElementaryVars())
          if (!elemVars.ContainsKey(elemVar.VariableID))
            elemVars.Add(elemVar.VariableID, elemVar.VariableID);
      //Now go through DDD attribute mappings, add only those which have coresponding variable(s)
      foreach (DDDRefAttrMap refAttrMap in refAttrMappingsDDD)
      {
        if (refAttrMap.Expression == "")
        {//Referential attribute is mapped to single variable
          if (elemVars.ContainsKey(refAttrMap.VariableID))
            refAttrMappings.Add(refAttrMap);
        }
        else
        {//Referential attribute uses an expression - all expression parameters has to be tested
          //Obtain list of expression parameters
          List<sExprParam> expParams = expEvaluator.GetRefAttrExprParameters(refAttrMap.MappingIndex);
          //Test if all the paramaters have corresponding variables
          bool allParsFound = true;
          foreach (sExprParam expParam in expParams)
            if (!elemVars.ContainsKey(expParam.VariableID))
              allParsFound = false;
          //Add ref attrbute mapping if all parameters were found
          if (allParsFound)
            refAttrMappings.Add(refAttrMap);
        }
      }
    }

    /// <summary>
    /// Returns unique string identifying record type
    /// </summary>
    /// <param name="TUInstanceID">Identification of TU Instance</param>
    /// <param name="DDDVersionID">identification of DDD version</param>
    /// <param name="RecordDefID">identification of record</param>
    /// <returns>String uniquely identifying variable</returns>
    public static String GetRecIDString(Guid TUInstanceID, Guid DDDVersionID, int RecordDefID)
    {
      return TUInstanceID.ToString() + "." + DDDVersionID.ToString() + "." + RecordDefID.ToString("0000"); ;
    }

    /// <summary>
    /// Return record Title as default implementation of ToString method
    /// </summary>
    /// <returns>Record title</returns>
    public override string ToString()
    {
      return Title;
    }
  }

  /// <summary>
  /// Class represents event diagnostic record
  /// </summary>
  [Serializable]
  public class DDDEventRecord : DDDRecord
  {
    /// <summary>
    /// Event record severity
    /// </summary>
    public SeverityType Severity;

    /// <summary>
    /// Additional event severity level
    /// </summary>
    public int SubSeverity = 0;

    /// <summary>
    /// Event record fault code
    /// </summary>
    public int FaultCode = 0;

    /// <summary>
    /// Default public constructor
    /// </summary>
    public DDDEventRecord()
    {
    }

    /// <summary>
    /// Initializes instance data from XML element
    /// </summary>
    /// <param name="recElement">xml element from DDDObjects document</param>
    /// <param name="dddVer">DDDVersion object to which record belongs</param>
    protected override void InitializeFromDDDObjXML(XmlElement recElement, DDDVersion dddVer)
    {
      //Call base class initialization
      base.InitializeFromDDDObjXML(recElement, dddVer);

      //Read attributes applicable to Event records only
      String attribute;
      TriggerType = (TriggerSensitivity)Convert.ToInt32(recElement.GetAttribute("TriggerTypeID"));
      Severity = (SeverityType)Convert.ToInt32(recElement.GetAttribute("SeverityID"));
      if ((attribute = recElement.GetAttribute("SubSeverity")) != "")
        SubSeverity = Convert.ToInt32(attribute);
      if ((attribute = recElement.GetAttribute("FaultCode")) != "")
        FaultCode = Convert.ToInt32(attribute);
    }
  }

  /// <summary>
  /// Class represents trace diagnostic record
  /// </summary>
  [Serializable]
  public class DDDTraceRecord : DDDRecord
  {
    /// <summary>
    /// Number of last records stored for this record type
    /// </summary>
    public int LastRecs = 0;

    /// <summary>
    /// Code of the trace record type
    /// </summary>
    public int Code = 0;

    /// <summary>
    /// Default public constructor
    /// </summary>
    public DDDTraceRecord()
    {
    }

    /// <summary>
    /// Initializes instance data from XML element
    /// </summary>
    /// <param name="recElement">xml element from DDDObjects document</param>
    /// <param name="dddVer">DDDVersion object to which record belongs</param>
    protected override void InitializeFromDDDObjXML(XmlElement recElement, DDDVersion dddVer)
    {
      //Call base class initialization
      base.InitializeFromDDDObjXML(recElement, dddVer);

      //Read attributes applicable to Trace records only
      String attribute;
      if ((attribute = recElement.GetAttribute("LastRecs")) != "")
        LastRecs = Convert.ToInt32(attribute);
      if ((attribute = recElement.GetAttribute("Code")) != "")
        Code = Convert.ToInt32(attribute);
    }
  }

  /// <summary>
  /// Class represents snap diagnostic record
  /// </summary>
  [Serializable]
  public class DDDSnapRecord : DDDRecord
  {
    /// <summary>
    /// Code of the trace record type
    /// </summary>
    public int Code = 0;

    /// <summary>
    /// Default public constructor
    /// </summary>
    public DDDSnapRecord()
    {
    }

    /// <summary>
    /// Initializes instance data from XML element
    /// </summary>
    /// <param name="recElement">xml element from DDDObjects document</param>
    /// <param name="dddVer">DDDVersion object to which record belongs</param>
    protected override void InitializeFromDDDObjXML(XmlElement recElement, DDDVersion dddVer)
    {
      //Call base class initialization
      base.InitializeFromDDDObjXML(recElement, dddVer);

      //Read attributes applicable to Snap records only
      //Read attributes applicable to Trace records only
      String attribute;
      if ((attribute = recElement.GetAttribute("Code")) != "")
        Code = Convert.ToInt32(attribute);
    }
  }

  /// <summary>
  /// Class represents mapping of one referential attribute to one elementary variable which is associated with DDDRecord.
  /// Dictionary of DDDRefAttrMap objects is used for quick parsing of referential attribute values.
  /// </summary>
  [Serializable]
  public class DDDRefAttrMap
  {
    /// <summary>
    /// Index of ref attribute mapping from DB
    /// </summary>
    public int MappingIndex = 0;

    /// <summary>
    /// Identification of referential attribute
    /// </summary>
    public int RefAttrID = 0;

    /// <summary>
    /// ID of variable associated with referentila attribute
    /// or 0 if expression is defined
    /// </summary>
    public int VariableID = 0;

    /// <summary>
    /// Expression used for evaluation of referential attribute
    /// </summary>
    public String Expression = "";
  }
}
