using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TDManagementForms
{
  public partial class UserInput : Form
  {
    private UserInput(String caption)
    {
      InitializeComponent();
      this.Text = caption;
    }

    private DialogResult GetInput(out String userInput)
    {
      DialogResult result = this.ShowDialog();
      userInput = this.tbInput.Text;
      return result;
    }

    public static DialogResult GetUserInput(String caption, out String userInput)
    {
      UserInput inpForm = new UserInput(caption);
      DialogResult res = inpForm.GetInput(out userInput);
      inpForm.Dispose();
      return res;
    }
  }
}