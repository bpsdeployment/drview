namespace TDManagementForms
{
    partial class FileManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.components = new System.ComponentModel.Container();
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileManager));
          this.rmtDir = new System.Windows.Forms.ListView();
          this.filename = new System.Windows.Forms.ColumnHeader();
          this.size = new System.Windows.Forms.ColumnHeader();
          this.time = new System.Windows.Forms.ColumnHeader();
          this.cntxMenuDir = new System.Windows.Forms.ContextMenuStrip(this.components);
          this.copyFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.deleteFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.toolStripFileSeparator = new System.Windows.Forms.ToolStripSeparator();
          this.createDirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.toolStripCompareSeparator = new System.Windows.Forms.ToolStripSeparator();
          this.compareFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.showFileHashToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.imageList1 = new System.Windows.Forms.ImageList(this.components);
          this.splitContainer = new System.Windows.Forms.SplitContainer();
          this.rmtPath = new System.Windows.Forms.TextBox();
          this.locDir = new System.Windows.Forms.ListView();
          this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
          this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
          this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
          this.locPath = new System.Windows.Forms.TextBox();
          this.statusStrip = new System.Windows.Forms.StatusStrip();
          this.toolStripStatus = new System.Windows.Forms.ToolStripStatusLabel();
          this.fileProgressBar = new System.Windows.Forms.ToolStripProgressBar();
          this.splitter1 = new System.Windows.Forms.Splitter();
          this.panel1 = new System.Windows.Forms.Panel();
          this.btRefresh = new System.Windows.Forms.Button();
          this.label1 = new System.Windows.Forms.Label();
          this.tbURL = new System.Windows.Forms.TextBox();
          this.cntxMenuDir.SuspendLayout();
          this.splitContainer.Panel1.SuspendLayout();
          this.splitContainer.Panel2.SuspendLayout();
          this.splitContainer.SuspendLayout();
          this.statusStrip.SuspendLayout();
          this.panel1.SuspendLayout();
          this.SuspendLayout();
          // 
          // rmtDir
          // 
          this.rmtDir.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.rmtDir.BackColor = System.Drawing.SystemColors.Window;
          this.rmtDir.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.filename,
            this.size,
            this.time});
          this.rmtDir.ContextMenuStrip = this.cntxMenuDir;
          this.rmtDir.FullRowSelect = true;
          this.rmtDir.GridLines = true;
          this.rmtDir.HideSelection = false;
          this.rmtDir.Location = new System.Drawing.Point(3, 29);
          this.rmtDir.Name = "rmtDir";
          this.rmtDir.Size = new System.Drawing.Size(335, 434);
          this.rmtDir.SmallImageList = this.imageList1;
          this.rmtDir.TabIndex = 2;
          this.rmtDir.UseCompatibleStateImageBehavior = false;
          this.rmtDir.View = System.Windows.Forms.View.Details;
          this.rmtDir.ItemActivate += new System.EventHandler(this.RemoteDir_ActivateItem);
          // 
          // filename
          // 
          this.filename.Text = "filename";
          this.filename.Width = 140;
          // 
          // size
          // 
          this.size.Text = "size";
          this.size.Width = 68;
          // 
          // time
          // 
          this.time.Text = "time";
          this.time.Width = 120;
          // 
          // cntxMenuDir
          // 
          this.cntxMenuDir.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyFileToolStripMenuItem,
            this.deleteFileToolStripMenuItem,
            this.toolStripFileSeparator,
            this.createDirectoryToolStripMenuItem,
            this.toolStripCompareSeparator,
            this.compareFilesToolStripMenuItem,
            this.showFileHashToolStripMenuItem});
          this.cntxMenuDir.Name = "cntxMenuDir";
          this.cntxMenuDir.Size = new System.Drawing.Size(165, 126);
          this.cntxMenuDir.Opening += new System.ComponentModel.CancelEventHandler(this.cntxMenuDir_Opening);
          // 
          // copyFileToolStripMenuItem
          // 
          this.copyFileToolStripMenuItem.Name = "copyFileToolStripMenuItem";
          this.copyFileToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
          this.copyFileToolStripMenuItem.Text = "Copy";
          this.copyFileToolStripMenuItem.Click += new System.EventHandler(this.copyFileToolStripMenuItem_Click);
          // 
          // deleteFileToolStripMenuItem
          // 
          this.deleteFileToolStripMenuItem.Name = "deleteFileToolStripMenuItem";
          this.deleteFileToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
          this.deleteFileToolStripMenuItem.Text = "Delete";
          this.deleteFileToolStripMenuItem.Click += new System.EventHandler(this.deleteFileToolStripMenuItem_Click);
          // 
          // toolStripFileSeparator
          // 
          this.toolStripFileSeparator.Name = "toolStripFileSeparator";
          this.toolStripFileSeparator.Size = new System.Drawing.Size(161, 6);
          // 
          // createDirectoryToolStripMenuItem
          // 
          this.createDirectoryToolStripMenuItem.Name = "createDirectoryToolStripMenuItem";
          this.createDirectoryToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
          this.createDirectoryToolStripMenuItem.Text = "Create directory";
          this.createDirectoryToolStripMenuItem.Click += new System.EventHandler(this.createDirectoryToolStripMenuItem_Click);
          // 
          // toolStripCompareSeparator
          // 
          this.toolStripCompareSeparator.Name = "toolStripCompareSeparator";
          this.toolStripCompareSeparator.Size = new System.Drawing.Size(161, 6);
          // 
          // compareFilesToolStripMenuItem
          // 
          this.compareFilesToolStripMenuItem.Name = "compareFilesToolStripMenuItem";
          this.compareFilesToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
          this.compareFilesToolStripMenuItem.Text = "Compare files";
          this.compareFilesToolStripMenuItem.Click += new System.EventHandler(this.compareFilesToolStripMenuItem_Click);
          // 
          // showFileHashToolStripMenuItem
          // 
          this.showFileHashToolStripMenuItem.Name = "showFileHashToolStripMenuItem";
          this.showFileHashToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
          this.showFileHashToolStripMenuItem.Text = "Show file hash";
          this.showFileHashToolStripMenuItem.Click += new System.EventHandler(this.showFileHashToolStripMenuItem_Click);
          // 
          // imageList1
          // 
          this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
          this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
          this.imageList1.Images.SetKeyName(0, "folder.bmp");
          this.imageList1.Images.SetKeyName(1, "file.bmp");
          this.imageList1.Images.SetKeyName(2, "up.bmp");
          // 
          // splitContainer
          // 
          this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.splitContainer.Location = new System.Drawing.Point(0, 28);
          this.splitContainer.Name = "splitContainer";
          // 
          // splitContainer.Panel1
          // 
          this.splitContainer.Panel1.Controls.Add(this.rmtDir);
          this.splitContainer.Panel1.Controls.Add(this.rmtPath);
          // 
          // splitContainer.Panel2
          // 
          this.splitContainer.Panel2.Controls.Add(this.locDir);
          this.splitContainer.Panel2.Controls.Add(this.locPath);
          this.splitContainer.Size = new System.Drawing.Size(690, 466);
          this.splitContainer.SplitterDistance = 341;
          this.splitContainer.SplitterWidth = 2;
          this.splitContainer.TabIndex = 3;
          this.splitContainer.TabStop = false;
          // 
          // rmtPath
          // 
          this.rmtPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.rmtPath.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
          this.rmtPath.Location = new System.Drawing.Point(3, 3);
          this.rmtPath.Name = "rmtPath";
          this.rmtPath.Size = new System.Drawing.Size(335, 20);
          this.rmtPath.TabIndex = 0;
          this.rmtPath.TabStop = false;
          // 
          // locDir
          // 
          this.locDir.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.locDir.BackColor = System.Drawing.SystemColors.Window;
          this.locDir.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
          this.locDir.ContextMenuStrip = this.cntxMenuDir;
          this.locDir.FullRowSelect = true;
          this.locDir.GridLines = true;
          this.locDir.HideSelection = false;
          this.locDir.Location = new System.Drawing.Point(3, 29);
          this.locDir.Name = "locDir";
          this.locDir.Size = new System.Drawing.Size(341, 434);
          this.locDir.SmallImageList = this.imageList1;
          this.locDir.TabIndex = 2;
          this.locDir.UseCompatibleStateImageBehavior = false;
          this.locDir.View = System.Windows.Forms.View.Details;
          this.locDir.ItemActivate += new System.EventHandler(this.LocalDir_ActivateItem);
          // 
          // columnHeader1
          // 
          this.columnHeader1.Text = "filename";
          this.columnHeader1.Width = 140;
          // 
          // columnHeader2
          // 
          this.columnHeader2.Text = "size";
          this.columnHeader2.Width = 68;
          // 
          // columnHeader3
          // 
          this.columnHeader3.Text = "time";
          this.columnHeader3.Width = 120;
          // 
          // locPath
          // 
          this.locPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.locPath.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
          this.locPath.Location = new System.Drawing.Point(3, 3);
          this.locPath.Name = "locPath";
          this.locPath.Size = new System.Drawing.Size(341, 20);
          this.locPath.TabIndex = 0;
          this.locPath.TabStop = false;
          // 
          // statusStrip
          // 
          this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatus,
            this.fileProgressBar});
          this.statusStrip.Location = new System.Drawing.Point(0, 497);
          this.statusStrip.Name = "statusStrip";
          this.statusStrip.Size = new System.Drawing.Size(690, 22);
          this.statusStrip.TabIndex = 5;
          this.statusStrip.Text = "statusStrip1";
          // 
          // toolStripStatus
          // 
          this.toolStripStatus.Name = "toolStripStatus";
          this.toolStripStatus.Size = new System.Drawing.Size(35, 17);
          this.toolStripStatus.Text = "ready";
          // 
          // fileProgressBar
          // 
          this.fileProgressBar.Name = "fileProgressBar";
          this.fileProgressBar.Size = new System.Drawing.Size(200, 16);
          this.fileProgressBar.Visible = false;
          // 
          // splitter1
          // 
          this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
          this.splitter1.Location = new System.Drawing.Point(0, 494);
          this.splitter1.Name = "splitter1";
          this.splitter1.Size = new System.Drawing.Size(690, 3);
          this.splitter1.TabIndex = 6;
          this.splitter1.TabStop = false;
          // 
          // panel1
          // 
          this.panel1.Controls.Add(this.btRefresh);
          this.panel1.Controls.Add(this.label1);
          this.panel1.Controls.Add(this.tbURL);
          this.panel1.Controls.Add(this.splitContainer);
          this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
          this.panel1.Location = new System.Drawing.Point(0, 0);
          this.panel1.Name = "panel1";
          this.panel1.Size = new System.Drawing.Size(690, 494);
          this.panel1.TabIndex = 7;
          // 
          // btRefresh
          // 
          this.btRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
          this.btRefresh.Location = new System.Drawing.Point(603, 2);
          this.btRefresh.Name = "btRefresh";
          this.btRefresh.Size = new System.Drawing.Size(75, 23);
          this.btRefresh.TabIndex = 7;
          this.btRefresh.Text = "Refresh";
          this.btRefresh.UseVisualStyleBackColor = true;
          this.btRefresh.Click += new System.EventHandler(this.btRefresh_Click);
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(5, 7);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(32, 13);
          this.label1.TabIndex = 6;
          this.label1.Text = "URL:";
          // 
          // tbURL
          // 
          this.tbURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.tbURL.Location = new System.Drawing.Point(43, 4);
          this.tbURL.Name = "tbURL";
          this.tbURL.Size = new System.Drawing.Size(545, 20);
          this.tbURL.TabIndex = 5;
          this.tbURL.TabStop = false;
          this.tbURL.Text = "http://localhost/axis/FileService";
          // 
          // FileManager
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(690, 519);
          this.Controls.Add(this.panel1);
          this.Controls.Add(this.splitter1);
          this.Controls.Add(this.statusStrip);
          this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
          this.Name = "FileManager";
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
          this.Text = "Remote File Manager";
          this.Load += new System.EventHandler(this.FileManager_Load);
          this.cntxMenuDir.ResumeLayout(false);
          this.splitContainer.Panel1.ResumeLayout(false);
          this.splitContainer.Panel1.PerformLayout();
          this.splitContainer.Panel2.ResumeLayout(false);
          this.splitContainer.Panel2.PerformLayout();
          this.splitContainer.ResumeLayout(false);
          this.statusStrip.ResumeLayout(false);
          this.statusStrip.PerformLayout();
          this.panel1.ResumeLayout(false);
          this.panel1.PerformLayout();
          this.ResumeLayout(false);
          this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView rmtDir;
        private System.Windows.Forms.ColumnHeader filename;
        private System.Windows.Forms.ColumnHeader size;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.ListView locDir;
        private System.Windows.Forms.ColumnHeader columnHeader1;
      private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.TextBox rmtPath;
        private System.Windows.Forms.TextBox locPath;
        private System.Windows.Forms.ColumnHeader time;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatus;
      private System.Windows.Forms.ToolStripProgressBar fileProgressBar;
      private System.Windows.Forms.ContextMenuStrip cntxMenuDir;
      private System.Windows.Forms.ToolStripMenuItem copyFileToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem deleteFileToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem compareFilesToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem showFileHashToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem createDirectoryToolStripMenuItem;
      private System.Windows.Forms.ToolStripSeparator toolStripFileSeparator;
      private System.Windows.Forms.ToolStripSeparator toolStripCompareSeparator;
      private System.Windows.Forms.Splitter splitter1;
      private System.Windows.Forms.Panel panel1;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.TextBox tbURL;
      private System.Windows.Forms.Button btRefresh;
    }
}

