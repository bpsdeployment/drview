namespace TDManagementForms
{
  partial class TUTypesManagement
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TUTypesManagement));
      this.dgvTUTypes = new System.Windows.Forms.DataGridView();
      this.tUTypeIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.typeNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.commentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.tUTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.tUTypesDataSet = new TDManagementForms.DSTUTypes.TUTypesDataSet();
      this.toolStrip = new System.Windows.Forms.ToolStrip();
      this.tsbRefresh = new System.Windows.Forms.ToolStripButton();
      this.tsbCommit = new System.Windows.Forms.ToolStripButton();
      this.panel = new System.Windows.Forms.Panel();
      this.tUTypesTableAdapter = new TDManagementForms.DSTUTypes.TUTypesDataSetTableAdapters.TUTypesTableAdapter();
      ((System.ComponentModel.ISupportInitialize)(this.dgvTUTypes)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.tUTypesBindingSource)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.tUTypesDataSet)).BeginInit();
      this.toolStrip.SuspendLayout();
      this.panel.SuspendLayout();
      this.SuspendLayout();
      // 
      // dgvTUTypes
      // 
      this.dgvTUTypes.AutoGenerateColumns = false;
      this.dgvTUTypes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
      this.dgvTUTypes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvTUTypes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tUTypeIDDataGridViewTextBoxColumn,
            this.typeNameDataGridViewTextBoxColumn,
            this.commentDataGridViewTextBoxColumn});
      this.dgvTUTypes.DataSource = this.tUTypesBindingSource;
      this.dgvTUTypes.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgvTUTypes.Location = new System.Drawing.Point(0, 0);
      this.dgvTUTypes.Name = "dgvTUTypes";
      this.dgvTUTypes.RowHeadersWidth = 30;
      this.dgvTUTypes.Size = new System.Drawing.Size(682, 152);
      this.dgvTUTypes.TabIndex = 0;
      this.dgvTUTypes.DefaultValuesNeeded += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvTUTypes_DefaultValuesNeeded);
      this.dgvTUTypes.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvTUTypes_DataError);
      // 
      // tUTypeIDDataGridViewTextBoxColumn
      // 
      this.tUTypeIDDataGridViewTextBoxColumn.DataPropertyName = "TUTypeID";
      this.tUTypeIDDataGridViewTextBoxColumn.FillWeight = 60F;
      this.tUTypeIDDataGridViewTextBoxColumn.HeaderText = "TUTypeID";
      this.tUTypeIDDataGridViewTextBoxColumn.Name = "tUTypeIDDataGridViewTextBoxColumn";
      this.tUTypeIDDataGridViewTextBoxColumn.Visible = false;
      // 
      // typeNameDataGridViewTextBoxColumn
      // 
      this.typeNameDataGridViewTextBoxColumn.DataPropertyName = "TypeName";
      this.typeNameDataGridViewTextBoxColumn.FillWeight = 40F;
      this.typeNameDataGridViewTextBoxColumn.HeaderText = "Name";
      this.typeNameDataGridViewTextBoxColumn.Name = "typeNameDataGridViewTextBoxColumn";
      // 
      // commentDataGridViewTextBoxColumn
      // 
      this.commentDataGridViewTextBoxColumn.DataPropertyName = "Comment";
      this.commentDataGridViewTextBoxColumn.HeaderText = "Comment";
      this.commentDataGridViewTextBoxColumn.Name = "commentDataGridViewTextBoxColumn";
      // 
      // tUTypesBindingSource
      // 
      this.tUTypesBindingSource.DataMember = "TUTypes";
      this.tUTypesBindingSource.DataSource = this.tUTypesDataSet;
      // 
      // tUTypesDataSet
      // 
      this.tUTypesDataSet.DataSetName = "TUTypesDataSet";
      this.tUTypesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
      // 
      // toolStrip
      // 
      this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbRefresh,
            this.tsbCommit});
      this.toolStrip.Location = new System.Drawing.Point(0, 0);
      this.toolStrip.Name = "toolStrip";
      this.toolStrip.Size = new System.Drawing.Size(682, 25);
      this.toolStrip.TabIndex = 1;
      this.toolStrip.Text = "toolStrip1";
      // 
      // tsbRefresh
      // 
      this.tsbRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbRefresh.Image = ((System.Drawing.Image)(resources.GetObject("tsbRefresh.Image")));
      this.tsbRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbRefresh.Name = "tsbRefresh";
      this.tsbRefresh.Size = new System.Drawing.Size(23, 22);
      this.tsbRefresh.Click += new System.EventHandler(this.tsbRefresh_Click);
      // 
      // tsbCommit
      // 
      this.tsbCommit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tsbCommit.Image = ((System.Drawing.Image)(resources.GetObject("tsbCommit.Image")));
      this.tsbCommit.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbCommit.Name = "tsbCommit";
      this.tsbCommit.Size = new System.Drawing.Size(23, 22);
      this.tsbCommit.ToolTipText = "Commit changes";
      this.tsbCommit.Click += new System.EventHandler(this.tsbCommit_Click);
      // 
      // panel
      // 
      this.panel.Controls.Add(this.dgvTUTypes);
      this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel.Location = new System.Drawing.Point(0, 25);
      this.panel.Name = "panel";
      this.panel.Size = new System.Drawing.Size(682, 152);
      this.panel.TabIndex = 2;
      // 
      // tUTypesTableAdapter
      // 
      this.tUTypesTableAdapter.ClearBeforeFill = true;
      // 
      // TUTypesManagement
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(682, 177);
      this.Controls.Add(this.panel);
      this.Controls.Add(this.toolStrip);
      this.Name = "TUTypesManagement";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Telediagnostic Unit Types";
      this.Load += new System.EventHandler(this.TUTypesManagement_Load);
      ((System.ComponentModel.ISupportInitialize)(this.dgvTUTypes)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.tUTypesBindingSource)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.tUTypesDataSet)).EndInit();
      this.toolStrip.ResumeLayout(false);
      this.toolStrip.PerformLayout();
      this.panel.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.DataGridView dgvTUTypes;
    private TDManagementForms.DSTUTypes.TUTypesDataSet tUTypesDataSet;
    private System.Windows.Forms.BindingSource tUTypesBindingSource;
    private TDManagementForms.DSTUTypes.TUTypesDataSetTableAdapters.TUTypesTableAdapter tUTypesTableAdapter;
    private System.Windows.Forms.DataGridViewTextBoxColumn tUTypeIDDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn typeNameDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn commentDataGridViewTextBoxColumn;
    private System.Windows.Forms.ToolStrip toolStrip;
    private System.Windows.Forms.ToolStripButton tsbRefresh;
    private System.Windows.Forms.ToolStripButton tsbCommit;
    private System.Windows.Forms.Panel panel;
  }
}