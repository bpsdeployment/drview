using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Reflection;
using System.IO;

namespace TDManagementForms
{
  /// <summary>
  /// Class provides methods to access resources embedded in executing assembly.
  /// </summary>
  public class ResourceAccess
  {
    /// <summary>
    /// Obtain icon from embbedded resource in entry assembly.
    /// In case empty name is specified or resource is not found, null is returned.
    /// </summary>
    /// <param name="iconName">Name of the icon</param>
    /// <returns>Icon from resource or null</returns>
    public static Icon GetIcon(String iconName)
    {
      //Check for correct resource name
      if (iconName == null || iconName == String.Empty)
        return null;
      try
      {
        String[] resources = Assembly.GetEntryAssembly().GetManifestResourceNames();
        using (Stream stream = Assembly.GetEntryAssembly().GetManifestResourceStream(iconName))
        { //Obtain stream with resource
          return new Icon(stream); //Create and return icon from stream
        }
      }
      catch (Exception)
      { //Unable to load resource
        return null;
      }
    }

    /// <summary>
    /// Obtain bitmap from embbedded resource in entry assembly.
    /// In case empty name is specified or resource is not found, null is returned.
    /// </summary>
    /// <param name="imageName">Name of the image</param>
    /// <returns>Image from resource or null</returns>
    public static Bitmap GetBitmap(String imageName)
    {
      //Check for correct resource name
      if (imageName == null || imageName == String.Empty)
        return null;
      try
      {
        String[] resources = Assembly.GetEntryAssembly().GetManifestResourceNames();
        using (Stream stream = Assembly.GetEntryAssembly().GetManifestResourceStream(imageName))
        { //Obtain stream with resource
          return new Bitmap(stream); //Create and return icon from stream
        }
      }
      catch (Exception)
      { //Unable to load resource
        return null;
      }
    }
  }
}
