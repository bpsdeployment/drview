#define TRACE

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Xsl;
using System.IO;
using System.Configuration;
using System.Diagnostics;
using TDManagementForms;

[WebService(Namespace = "http://unicontrols.cz/Telediagnostica/DDUpload")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class DDUpload : System.Web.Services.WebService
{
  public DDUpload()
  {
    //Uncomment the following line if using designed components 
    //InitializeComponent(); 
  }

  [return: XmlAttribute(AttributeName = "DDUResult")]
  [WebMethod]
  public bool UploadData([XmlAnyElement]XmlElement DDMessage) 
  {
    XslCompiledTransform DRMessTransform = Application["DRMessTransform"] as XslCompiledTransform;
    XmlWriter DRXmlWriter;
    Logger logger = Application["Logger"] as Logger;
    try
    {
      Application.Lock();
      //Validate the message against schema
      ValidateMessage(DDMessage);
      DRXmlWriter = GetOpenWriter();
      if (DRXmlWriter == null)
        DRXmlWriter = GetNewWriter();
      DRMessTransform.Transform(DDMessage, DRXmlWriter);
      DRXmlWriter.Flush();
      logger.LogText(4, "DDUpload", "Data upload from host: {0}, Record count {1} TUInstanceID {2}, DDDVersionID {3}", 
                     HttpContext.Current.Request.UserHostAddress, DDMessage.ChildNodes.Count, 
                     DDMessage.Attributes["TUInstanceID"].Value, DDMessage.Attributes["DDDVersionID"].Value);
    }
    catch (Exception ex)
    {
      logger.LogText(1, "DDUpload", "DDUpload from host {0} failed: {1}", HttpContext.Current.Request.UserHostAddress, ex.Message);
      throw ex;
    }
    finally
    {
      Application.UnLock();
    }
    return true;
  }

  /// <summary>
  /// Method returns open XmlWriter for a file in DRImport directory.
  /// XmlWriter from application state is used, 
  /// however if the underlaying file is too big or too old, new file is
  /// created and XmlWriter is directed to this file
  /// </summary>
  private XmlWriter GetOpenWriter()
  {
    try
    {
      uint maxDRStreamLenkB = Convert.ToUInt32(ConfigurationManager.AppSettings["StreamMaxSizekB"]);
      uint maxDRStreamAgeSec = Convert.ToUInt32(ConfigurationManager.AppSettings["StreamMaxAgeSec"]);
      FileInfo DRFileInfo = Application["DRFileInfo"] as FileInfo;
      XmlWriter DRXmlWriter = Application["DRXmlWriter"] as XmlWriter;
      DRFileInfo.Refresh();

      if ((DRFileInfo.Length >= maxDRStreamLenkB * 1024) ||
           (DRFileInfo.CreationTime.AddSeconds(maxDRStreamAgeSec) <= DateTime.Now))
      { //it is necessary to create new output DRFile
        //Get existing objects from ApplicationState
        uint DRFileID = Convert.ToUInt32(Application["DRFileID"]);
        //Create new file name
        DRFileID++;
        Application["DRFileID"] = DRFileID;
        String importDir = ConfigurationManager.AppSettings["ImportDir"];
        String DRFileName = importDir + DRFileID.ToString("000000") + ".xml";
        //Open new XmlWriter
        DRXmlWriter = OpenNewXmlWriter(DRFileName);
      }
      return DRXmlWriter;
    }
    catch(Exception ex)
    {
      Logger logger = Application["Logger"] as Logger;
      logger.LogText(1, "DDUpload", Resources.DDUpload.msgErrXmlWriterOpen, ex.Message);
      return null;
    }
  }

  /// <summary>
  /// Called in case the standard method for retrieving XmlWriter fails.
  /// Name of DR file is determined by DR Directory content.
  /// </summary>
  /// <returns>Returns new XmlWriter</returns>
  private XmlWriter GetNewWriter()
  {
    Logger logger = Application["Logger"] as Logger;
    logger.LogText(1, "DDUpload", Resources.DDUpload.msgWarCreatingDRFile);
    //Load fresh application configuration
    ConfigurationManager.RefreshSection("appSettings");
    String importDir = ConfigurationManager.AppSettings["ImportDir"];
    String failedDir = ConfigurationManager.AppSettings["FailedDir"];
    //Get ID for new DR file
    //Check import directory
    uint DRFileIDImport = GetNextFreeDRFileID(importDir);
    //Check failed directory
    uint DRFileIDFailed = GetNextFreeDRFileID(failedDir);
    uint DRFileID = Math.Max(DRFileIDImport, DRFileIDFailed);
    Application["DRFileID"] = DRFileID;
    //Create name for output file
    String DRFileName = importDir + DRFileID.ToString("000000") + ".xml";
    return OpenNewXmlWriter(DRFileName);
  }

  /// <summary>
  /// Creates new file in DRImport directory and opens XML Writer for this file.
  /// </summary>
  /// <returns>Returns open XmlWriter or throws an exception</returns>
  private XmlWriter OpenNewXmlWriter(String DRFileName)
  {
    //Get existing objects from Application state
    XslCompiledTransform DRMessTransform = Application["DRMessTransform"] as XslCompiledTransform;
    FileStream DRFileStream = Application["DRFileStream"] as FileStream;
    XmlWriter DRXmlWriter = Application["DRXmlWriter"] as XmlWriter;
    //Close the writer and stream
    DRXmlWriter.Close();
    DRFileStream.Close();
    //Create stream for output file
    DRFileStream = new FileStream(DRFileName, FileMode.CreateNew, FileAccess.Write, FileShare.None);
    //Create XML writer for the stream
    XmlWriterSettings DRWriterSettings = new XmlWriterSettings();
    DRWriterSettings.Indent = true;
    DRWriterSettings.Encoding = System.Text.Encoding.UTF8;
    DRWriterSettings.OmitXmlDeclaration = true;
    DRWriterSettings.ConformanceLevel = ConformanceLevel.Fragment;
    DRXmlWriter = XmlWriter.Create(DRFileStream, DRWriterSettings);
    //Create FileInfo for DR File
    FileInfo DRFileInfo = new FileInfo(DRFileName);
    //Store objects in ApplicationState
    Application["DRFileStream"] = DRFileStream;
    Application["DRXmlWriter"] = DRXmlWriter;
    Application["DRFileInfo"] = DRFileInfo;
    
    Logger logger = Application["Logger"] as Logger;
    logger.LogText(2, "DDUpload", Resources.DDUpload.msgNewDRFileCreated, DRFileName);
    return DRXmlWriter;
  }

  /// <summary>
  /// Performs validation of DDMessage element using validating document stored in application state.
  /// In case of validation failure, throws an exception.
  /// </summary>
  /// <param name="DDMessage">Message to validate</param>
  private void ValidateMessage(XmlElement DDMessage)
  {
    XmlDocument doc = Application["ValDocument"] as XmlDocument;
    //Removes all elements from document
    doc.RemoveAll();
    //Appends copy of passed element
    doc.AppendChild(doc.ImportNode(DDMessage, true));
    //Validates the element
    doc.Validate(null);
  }

  /// <summary>
  /// Iterates over all XML files in specified directory
  /// and finds first free number that can be used as file name
  /// </summary>
  /// <param name="dirPath">Path for the directory to check</param>
  /// <returns>First free file ID</returns>
  public static uint GetNextFreeDRFileID(String dirPath)
  {
    uint DRFileID = 0;
    string[] drFiles;
    uint currFileID = 0;
    //Get array of file names in the directory for xml files
    drFiles = System.IO.Directory.GetFiles(dirPath, "*.xml", System.IO.SearchOption.TopDirectoryOnly);
    foreach (String file in drFiles)
    { //check each file name
      try
      {
        //try to convert the file name to uint
        currFileID = Convert.ToUInt32(System.IO.Path.GetFileNameWithoutExtension(file));
        //check if the number is bigger than maximum found
        if (currFileID > DRFileID)
          DRFileID = currFileID;
      }
      catch (Exception) { }
    }
    //return new number, that can be used as file name
    return (DRFileID + 1);
  }

  /// <summary>
  /// Creates and returns logger component typed as Object
  /// </summary>
  /// <returns></returns>
  public static Logger CreateLogger()
  {
    //Create logger component
    Logger logger = new Logger();
    logger.FileName = ConfigurationManager.AppSettings["LogFileName"];
    logger.FileVerbosity = 6;
    logger.LogToFile = true;
    logger.MaxFileLength = Convert.ToInt32(ConfigurationManager.AppSettings["MaxLogSize"]);
    logger.MaxHistoryFiles = Convert.ToInt32(ConfigurationManager.AppSettings["MaxLogHistoryFiles"]);
    return logger;
  }

  public static XmlDocument CreateValidatingDocument(String schemaFileName)
  {
    XmlDocument doc = new XmlDocument();
    doc.Schemas.Add(null, schemaFileName);
    return doc;
  }
}


