using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using TDManagementForms;

/// <summary>
/// Provides methods for updating or querying TU connection information
/// stored in local database.
/// </summary>
[WebService(Namespace = "http://unicontrols.cz/Telediagnostica/TUConnInfo")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class TUConnInfo : System.Web.Services.WebService
{
  public TUConnInfo () 
  {
    //Uncomment the following line if using designed components 
    //InitializeComponent(); 
  }

  /// <summary>
  /// Updates TU IP addresses in local DB.
  /// </summary>
  /// <param name="TUInstanceID">TU Identification</param>
  /// <param name="IPAddrGPRS">Current IP address of GPRS interface</param>
  /// <param name="IPAddrWiFi">Current IP address of WiFi interface</param>
  [WebMethod]
  public void UpdateIPAddr(Guid TUInstanceID, String IPAddrGPRS, String IPAddrWiFi) 
  {
    //Create connection and command objects
    SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["CDDBConnectionString"].ConnectionString);
    SqlCommand dbCommand = new SqlCommand();
    Logger logger = Application["Logger"] as Logger;
    try
    {
      //Set command parameters
      dbCommand.Connection = dbConnection;
      dbCommand.CommandType = CommandType.StoredProcedure;
      dbCommand.CommandText = "tuUpdateIPAddr";
      dbCommand.Parameters.Add("@TUInstanceID", SqlDbType.UniqueIdentifier).Value = TUInstanceID;
      dbCommand.Parameters.Add("@IPAddrGPRS", SqlDbType.NVarChar, 50).Value = IPAddrGPRS;
      dbCommand.Parameters.Add("@IPAddrWiFI", SqlDbType.NVarChar, 50).Value = IPAddrWiFi;
      //Open connection and execute command
      dbConnection.Open();
      dbCommand.ExecuteNonQuery();
      logger.LogText(4, "TUConnInfo", "UpdateIPAddr from host: {0}, IP GPRS {1}, IP WiFi {2}, TUInstanceID {3}",
                     HttpContext.Current.Request.UserHostAddress, IPAddrGPRS, IPAddrWiFi, TUInstanceID);
    }
    catch (Exception ex)
    {
      logger.LogText(1, "TUConnInfo", "UpdateIPAddr from host {0} failed: {1}", HttpContext.Current.Request.UserHostAddress, ex.Message);
      throw ex;
    }
    finally
    {
      //Close db connection
      dbCommand.Dispose();
      dbConnection.Close();
    }
  }

  /// <summary>
  /// Updates TU IP addresses in local DB.
  /// </summary>
  /// <param name="TUInstanceID">TU Identification</param>
  /// <param name="IPAddrGPRS">Current IP address of GPRS interface</param>
  /// <param name="IPAddrWiFi">Current IP address of WiFi interface</param>
  /// <param name="DDDVersionID">Identifier of currently running DDD version</param>
  [WebMethod]
  public void UpdateIPAddr2(Guid TUInstanceID, String IPAddrGPRS, String IPAddrWiFi, String DDDVersionID)
  {
    //Create connection and command objects
    SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["CDDBConnectionString"].ConnectionString);
    SqlCommand dbCommand = new SqlCommand();
    Logger logger = Application["Logger"] as Logger;
    try
    {
      //Set command parameters
      dbCommand.Connection = dbConnection;
      dbCommand.CommandType = CommandType.StoredProcedure;
      dbCommand.CommandText = "tuUpdateIPAddr2";
      dbCommand.Parameters.Add("@TUInstanceID", SqlDbType.UniqueIdentifier).Value = TUInstanceID;
      dbCommand.Parameters.Add("@IPAddrGPRS", SqlDbType.NVarChar, 50).Value = IPAddrGPRS;
      dbCommand.Parameters.Add("@IPAddrWiFI", SqlDbType.NVarChar, 50).Value = IPAddrWiFi;
      if (DDDVersionID != null && DDDVersionID != String.Empty)
        dbCommand.Parameters.Add("@DDDVersionID", SqlDbType.UniqueIdentifier).Value = new Guid(DDDVersionID);
      else
        dbCommand.Parameters.Add("@DDDVersionID", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
      //Open connection and execute command
      dbConnection.Open();
      dbCommand.ExecuteNonQuery();
      logger.LogText(4, "TUConnInfo", "UpdateIPAddr from host: {0}, IP GPRS {1}, IP WiFi {2}, TUInstanceID {3}, DDDVersionID {4}",
                     HttpContext.Current.Request.UserHostAddress, IPAddrGPRS, IPAddrWiFi, TUInstanceID, DDDVersionID);
    }
    catch (Exception ex)
    {
      logger.LogText(1, "TUConnInfo", "UpdateIPAddr from host {0} failed: {1}", HttpContext.Current.Request.UserHostAddress, ex.Message);
      throw ex;
    }
    finally
    {
      //Close db connection
      dbCommand.Dispose();
      dbConnection.Close();
    }
  }

  /// <summary>
  /// Queries and returns actual IP addresses of specified TU in local DB.
  /// </summary>
  /// <param name="TUInstanceID">TU Identification</param>
  /// <param name="IPAddrGPRS">Current IP address of GPRS interface</param>
  /// <param name="IPAddrWiFi">Current IP address of WiFi interface</param>
  /// <returns>Time of last update of IP addresses from TU</returns>
  [WebMethod]
  public DateTime QueryIPAddr(Guid TUInstanceID, out String IPAddrGPRS, out String IPAddrWiFi)
  {
    DateTime LastUpdTime;
    //Create connection and command objects
    SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["CDDBConnectionString"].ConnectionString);
    SqlCommand dbCommand = new SqlCommand();
    Logger logger = Application["Logger"] as Logger;
    try
    {
      //Set command parameters
      dbCommand.Connection = dbConnection;
      dbCommand.CommandType = CommandType.StoredProcedure;
      dbCommand.CommandText = "tuGetIPAddr";
      dbCommand.Parameters.Add("@TUInstanceID", SqlDbType.UniqueIdentifier).Value = TUInstanceID;
      dbCommand.Parameters.Add("@IPAddrGPRS", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output;
      dbCommand.Parameters.Add("@IPAddrWiFI", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output;
      dbCommand.Parameters.Add("@IPAddrUpdTime", SqlDbType.DateTime).Direction = ParameterDirection.Output;
      //Open connection and execute command
      dbConnection.Open();
      dbCommand.ExecuteNonQuery();
      //Read return parameters
      if (dbCommand.Parameters["@IPAddrGPRS"].Value.GetType() != typeof(DBNull))
        IPAddrGPRS = (String)dbCommand.Parameters["@IPAddrGPRS"].Value;
      else
        IPAddrGPRS = "";
      if (dbCommand.Parameters["@IPAddrWiFI"].Value.GetType() != typeof(DBNull))
        IPAddrWiFi = (String)dbCommand.Parameters["@IPAddrWiFI"].Value;
      else
        IPAddrWiFi = "";
      if (dbCommand.Parameters["@IPAddrUpdTime"].Value.GetType() != typeof(DBNull))
        LastUpdTime = (DateTime)dbCommand.Parameters["@IPAddrUpdTime"].Value;
      else
        LastUpdTime = DateTime.MinValue;
      logger.LogText(4, "TUConnInfo", "QueryIPAddr from host: {0}, TUInstanceID {1}",
                     HttpContext.Current.Request.UserHostAddress, TUInstanceID);
    }
    catch (Exception ex)
    {
      logger.LogText(1, "TUConnInfo", "QueryIPAddr from host {0} failed: {1}", HttpContext.Current.Request.UserHostAddress, ex.Message);
      throw ex;
    }
    finally
    {
      //Close db connection
      dbCommand.Dispose();
      dbConnection.Close();
    }
    return LastUpdTime;
  }
}
