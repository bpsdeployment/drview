using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Resources;
using System.Security.Cryptography;

namespace TDManagementForms
{
  public partial class FileManager : Form
  {
    #region Private members
    private FileService.FileService fileService;
    private FileService.Credentials credentials;
    private bool isRefreshed = false;
    private int fileChunkSize;
    #endregion //Private members

    #region Construction initialization
    /// <summary>
    /// Creates the form, stores passed parameters
    /// </summary>
    /// <param name="remoteHostAddr">Address of remote host to manage</param>
    /// <param name="fileChunkSize">Size of file chunk to download/upload in ine call</param>
    public FileManager(String remoteHostAddr, int fileChunkSize)
    {
      InitializeComponent();
      fileService = new FileService.FileService();
      credentials = new FileService.Credentials();
      credentials.credentials = "thats me!";
      fileService.Url = ResFileManager.fileServiceURL.Replace("RemoteHost", remoteHostAddr);
      this.fileChunkSize = fileChunkSize;
      this.Text += " " + remoteHostAddr;
      tbURL.Text = fileService.Url;
    }
    #endregion //Construction initialization

    #region Helper methods
    private void SetStatusText(String text)
    {
      toolStripStatus.Text = text;
      Update();
    }

    private String GetLocPath()
    {
      if (locPath.Text.EndsWith("\\"))
        return locPath.Text;
      else
        return locPath.Text + "\\";
    }

    private String GetRmtPath()
    {
      // append backslash if not there
      if (!rmtPath.Text.EndsWith("/") && !rmtPath.Text.EndsWith("\\"))
      {
        if (rmtPath.Text.Contains("\\"))
          return rmtPath.Text + "\\";
        else
          return rmtPath.Text + "/";
      }
      else
        return rmtPath.Text;
    }

    private String GetLocFileFullName()
    {
      if ((locDir.SelectedItems.Count > 0) && (locDir.SelectedItems[0].SubItems[1].Text != "<DIR>"))
        return GetLocPath() + locDir.SelectedItems[0].Text;
      else
        return "";
    }

    private String GetLocFileName()
    {
      if ((locDir.SelectedItems.Count > 0) && (locDir.SelectedItems[0].SubItems[1].Text != "<DIR>"))
        return locDir.SelectedItems[0].Text;
      else
        return "";
    }

    private String GetRmtFileFullName()
    {
      if ((rmtDir.SelectedItems.Count > 0) && (rmtDir.SelectedItems[0].SubItems[1].Text != "<DIR>"))
        return GetRmtPath() + rmtDir.SelectedItems[0].Text;
      else
        return "";
    }

    private String GetRmtFileName()
    {
      if ((rmtDir.SelectedItems.Count > 0) && (rmtDir.SelectedItems[0].SubItems[1].Text != "<DIR>"))
        return rmtDir.SelectedItems[0].Text;
      else
        return "";
    }

    private bool IsDir(ListViewItem item)
    {
      return (item.SubItems[1].Text == "<DIR>");
    }
    #endregion //Helper methods

    #region Private methods
    private void RefreshRemoteDir()
    {
      SetStatusText("refreshing remote directory ...");

      // remove all items from listview
      rmtDir.Items.Clear();

      if (rmtPath.Text.Trim().Length == 0)
        rmtPath.Text = ".";

      try
      {
        // invoke web service
        FileService.Dir dir = fileService.GetDir(credentials, rmtPath.Text);

        // fill in directories
        foreach (FileService.DirItem item in dir.item)
        {
          if (!item.isdir || item.name == ".")
            continue;

          ListViewItem lvi = new ListViewItem(item.name);
          lvi.ImageIndex = item.name == ".." ? 2 : 0;
          lvi.SubItems.Add("<DIR>");
          lvi.SubItems.Add(item.time.ToString("dd.MM.yyyy HH:mm:ss"));
          rmtDir.Items.Add(lvi);
        }

        // fill in files
        foreach (FileService.DirItem item in dir.item)
        {
          if (item.isdir)
            continue;

          ListViewItem lvi = new ListViewItem(item.name, 1);
          lvi.SubItems.Add(item.size.ToString());
          lvi.SubItems.Add(item.time.ToString("dd.MM.yyyy HH:mm:ss"));
          rmtDir.Items.Add(lvi);
        }

        rmtPath.Text = dir.path;
      }
      catch (Exception err)
      {
        MessageBox.Show(err.Message, "Failed to refresh remote directory", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }

      SetStatusText(ResFileManager.strReady);
    }

    private void RemoteDir_ActivateItem(object sender, EventArgs e)
    {
      if (rmtDir.SelectedItems.Count == 0)
        return;

      ListViewItem item = rmtDir.SelectedItems[0];
      // check if item is a directory
      if (!IsDir(item))
        return; // not a directory

      // check for . and ..
      if (item.Text == ".")
        return; // nothing to do        

      if (item.Text == "..")
      {
        if (rmtPath.Text.Contains("\\"))
          // one level up
          rmtPath.Text = rmtPath.Text.Substring(0, rmtPath.Text.LastIndexOf('\\'));
        else if (rmtPath.Text.Contains("/"))
          // one level up
          rmtPath.Text = rmtPath.Text.Substring(0, rmtPath.Text.LastIndexOf('/'));
        else
          return;
      }
      else
        rmtPath.Text = GetRmtPath() + item.Text; // append directory name

      // and refresh remote directory
      RefreshRemoteDir();
    }

    private void RefreshLocalDir()
    {
      SetStatusText("refreshing local directory ...");

      try
      {
        // remove all items from listview
        locDir.Items.Clear();

        if (locPath.Text.Trim().Length == 0)
          locPath.Text = ".";

        // reference to local directory
        DirectoryInfo dir = new DirectoryInfo(locPath.Text);

        // prepare full name of directory
        locPath.Text = dir.FullName;

        if (!locPath.Text.EndsWith(":\\"))
        {
          ListViewItem lvi2 = new ListViewItem("..", 2);
          lvi2.SubItems.Add("<DIR>");
          lvi2.SubItems.Add(dir.Parent.LastWriteTime.ToString("dd.MM.yyyy HH:mm:ss"));
          locDir.Items.Add(lvi2);
        }

        // array of subdirectories
        DirectoryInfo[] subdirs = dir.GetDirectories();
        foreach (DirectoryInfo subdir in subdirs)
        {
          ListViewItem lvi = new ListViewItem(subdir.Name, 0);
          lvi.SubItems.Add("<DIR>");
          lvi.SubItems.Add(subdir.LastWriteTime.ToString());
          locDir.Items.Add(lvi);
        }

        // array representing the files in the current directory
        FileInfo[] files = dir.GetFiles();

        // fill in files in current directory into the listview
        foreach (FileInfo file in files)
        {
          ListViewItem lvi = new ListViewItem(file.Name, 1);
          lvi.SubItems.Add(file.Length.ToString());
          lvi.SubItems.Add(file.LastWriteTime.ToString("dd.MM.yyyy HH:mm:ss"));
          locDir.Items.Add(lvi);
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, "Failed to refresh local directory", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      SetStatusText(ResFileManager.strReady);
    }

    private void LocalDir_ActivateItem(object sender, EventArgs e)
    {
      if (locDir.SelectedItems.Count == 0)
        return;

      ListViewItem item = locDir.SelectedItems[0];
      // check if item is a directory
      if (!IsDir(item))
        return; // not a directory

      // check for . and ..
      if (item.Text == ".")
        return; // nothing to do

      if (item.Text == "..")
        locPath.Text = Path.GetDirectoryName(locPath.Text);
      else
        locPath.Text = GetLocPath() + item.Text; // append directory name

      // and refresh local directory
      RefreshLocalDir();
    }

    private void RefreshDirectories()
    {
      // refresh remote directory
      RefreshRemoteDir();

      // refresh local directory
      RefreshLocalDir();
      isRefreshed = true;
    }

    private void cntxMenuDir_Opening(object sender, CancelEventArgs e)
    {
      ListView dirSender = cntxMenuDir.SourceControl as ListView;
      if (!isRefreshed)
      { //Form has never been refreshed - don't show menu
        e.Cancel = true;
        return;
      }
      //Check if anything is selected
      if (dirSender.SelectedItems.Count > 0)
      {
        copyFileToolStripMenuItem.Visible = true;
        deleteFileToolStripMenuItem.Visible = true;
        toolStripFileSeparator.Visible = true;
      }
      else
      {
        copyFileToolStripMenuItem.Visible = false;
        deleteFileToolStripMenuItem.Visible = false;
        toolStripFileSeparator.Visible = false;
      }
      //Check if file is selected
      if ((dirSender.SelectedItems.Count == 1) && (!IsDir(dirSender.SelectedItems[0])))
      { //Enable / disable menu items
        compareFilesToolStripMenuItem.Visible = true;
        showFileHashToolStripMenuItem.Visible = true;
        toolStripCompareSeparator.Visible = true;
      }
      else
      {
        compareFilesToolStripMenuItem.Visible = false;
        showFileHashToolStripMenuItem.Visible = false;
        toolStripCompareSeparator.Visible = false;
      }
    }

    private void copyFileToolStripMenuItem_Click(object sender, EventArgs e)
    {
      ListView dirSender = cntxMenuDir.SourceControl as ListView;
      DialogResult result;
      foreach (ListViewItem sourceItem in dirSender.SelectedItems)
      {
        if (IsDir(sourceItem))
        {
          MessageBox.Show("Directory copy not supported", sourceItem.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
          continue;
        }
        FileTransfer transfDialog = new FileTransfer();
        if (dirSender == rmtDir)
        { //Copying file from remote directory to local
          transfDialog.InitiateDialog(sourceItem.Text, locPath.Text, sourceItem.Text, rmtPath.Text, fileService, credentials, true, fileChunkSize);
        }
        else if (dirSender == locDir)
        { //Copying file from local to remote directory
          transfDialog.InitiateDialog(sourceItem.Text, locPath.Text, sourceItem.Text, rmtPath.Text, fileService, credentials, false, fileChunkSize);
        }
        //Show file transfer dialog
        result = transfDialog.ShowDialog();
        transfDialog.Dispose();
        if (result != DialogResult.OK)
          break;
      }
      //Refresh directories
      RefreshDirectories();
    }

    /// <summary>
    /// Delete selected file
    /// </summary>
    private void deleteFileToolStripMenuItem_Click(object sender, EventArgs e)
    {
      ListView dirSender = cntxMenuDir.SourceControl as ListView;
      String message;
      SetStatusText("deleting...");
      foreach (ListViewItem sourceItem in dirSender.SelectedItems)
      {
        //Check for special names .. and .
        if (sourceItem.Text == ".." || sourceItem.Text == ".")
          continue;
        try
        {
          if (dirSender == rmtDir)
          { //Remote path
            if (IsDir(sourceItem))
            {//Delete directory
              if (!fileService.DeleteDirectory(credentials, GetRmtPath(), sourceItem.Text, out message))
                throw new Exception(message);
            }
            else
            {//Delete file
              if (!fileService.DeleteFile(credentials, GetRmtPath(), sourceItem.Text, out message)) //Delete remote file - use fileService
                throw new Exception(message);
            }
          }
          else if (dirSender == locDir)
          { //Local path
            if (IsDir(sourceItem))
            {//Delete directory
              Directory.Delete(GetLocPath() + sourceItem.Text);
            }
            else
            {//Delete file
              File.Delete(GetLocPath() + sourceItem.Text);
            }
          }
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.Message, "File delete error", MessageBoxButtons.OK, MessageBoxIcon.Error);
          break;
        }
      }
      RefreshDirectories();
      SetStatusText(ResFileManager.strReady);
    }

    /// <summary>
    /// Compare local and remote file by comparing digests
    /// </summary>
    private void compareFilesToolStripMenuItem_Click(object sender, EventArgs e)
    {
      ListView dirSender = cntxMenuDir.SourceControl as ListView;
      ListViewItem sourceItem = dirSender.SelectedItems[0];
      SetStatusText("comparing files...");
      byte[] locFileDigest;
      byte[] rmtFileDigest;
      try
      {
        //Obtain digest for remote file
        String message;
        if (!fileService.VerifyFile(credentials, GetRmtPath(), sourceItem.Text, out message, out rmtFileDigest))
          throw new Exception(message);
        //Obtain digest for local file
        FileInfo locFileInfo = new FileInfo(GetLocPath() + sourceItem.Text);
        FileStream locFile = locFileInfo.OpenRead();
        MD5 md5 = MD5CryptoServiceProvider.Create();
        locFileDigest = md5.ComputeHash(locFile);
        locFile.Dispose();
        //Compare both digests
        for (int i = 0; i < locFileDigest.Length; i++)
          if (locFileDigest[i] != rmtFileDigest[i])
          {
            MessageBox.Show("Files are different", "File comparison", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            SetStatusText(ResFileManager.strReady);
            return;
          }
        MessageBox.Show("Files are identical", "File comparison", MessageBoxButtons.OK, MessageBoxIcon.Information);
        SetStatusText(ResFileManager.strReady);
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, "File comparison error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    /// <summary>
    /// Obtain file hash and show it in message box
    /// </summary>
    private void showFileHashToolStripMenuItem_Click(object sender, EventArgs e)
    {
      ListView dirSender = cntxMenuDir.SourceControl as ListView;
      ListViewItem sourceItem = dirSender.SelectedItems[0];
      byte[] fileDigest;
      String message;
      SetStatusText("obtaining hash...");
      try
      {
        //Obtain digest
        if (dirSender == rmtDir)
        { //Remote file - use fileService
          if (!fileService.VerifyFile(credentials, GetRmtPath(), sourceItem.Text, out message, out fileDigest))
            throw new Exception(message);
        }
        else if (dirSender == locDir)
        { //Local file
          FileInfo locFileInfo = new FileInfo(GetLocPath() + sourceItem.Text);
          FileStream locFile = locFileInfo.OpenRead();
          MD5 md5 = MD5CryptoServiceProvider.Create();
          fileDigest = md5.ComputeHash(locFile);
          locFile.Dispose();
        }
        else
          return;
        //Display digest
        String strHash = "";
        foreach (byte hashByte in fileDigest)
          strHash += hashByte.ToString("X2") + " ";
        MessageBox.Show(strHash, sourceItem.Text + " hash", MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, "Failed to obtain file hash", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      SetStatusText(ResFileManager.strReady);
    }

    /// <summary>
    /// Creates new directory
    /// </summary>
    private void createDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
    {
      String dirName;
      String message;
      ListView dirSender = cntxMenuDir.SourceControl as ListView;
      //Obtain directory name
      if (UserInput.GetUserInput("Create directory", out dirName) != DialogResult.OK)
        return; //Cancel was pressed
      //Create directory
      SetStatusText("creating directory...");
      try
      {
        if (dirSender == rmtDir)
        { //Create remote directory
          //throw new Exception("Operation not implemented");
          if (!fileService.CreateDirectory(credentials, GetRmtPath(), dirName, out message))
            throw new Exception(message);
          RefreshRemoteDir();
        }
        else if (dirSender == locDir)
        { //Local directory
          Directory.CreateDirectory(GetLocPath() + dirName);
          RefreshLocalDir();
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, "Failed to create directory", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      SetStatusText(ResFileManager.strReady);
    }

    private void FileManager_Load(object sender, EventArgs e)
    {
      RefreshDirectories();
    }
    #endregion //Private methods

    private void btRefresh_Click(object sender, EventArgs e)
    {
      fileService.Url = tbURL.Text;
      RefreshDirectories();
    }
  }
}