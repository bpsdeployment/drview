﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("TDManagementForms")]
[assembly: AssemblyDescription("Helper management classes for telediagnostica system")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("UniControls")]
[assembly: AssemblyProduct("TDManagementForms")]
[assembly: AssemblyCopyright("Copyright © UniControls 2006")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("DE547E87-DF6E-461f-9163-8D47E0A3AD55")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.4.*")]
[assembly: AssemblyFileVersion("1.4.0.0")]
