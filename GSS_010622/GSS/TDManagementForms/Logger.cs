using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace TDManagementForms
{
  /// <summary>
  /// Provides logging capability for list box and files with history
  /// </summary>
  public partial class Logger : Component
  {
    #region Members
    protected ListBox logBox = null;
    protected String logFileName = Application.ProductName + ".log";
    protected int maxLBLines = 1000;
    protected int maxFileLength = 100000;
    protected int maxHistoryFiles = 3;
    protected int maxLBVerbosity = 3;
    protected int maxFileVerbosity = 5;
    protected StreamWriter logFileWriter = null;
    protected FileInfo logFileInfo = null;
    protected bool logFile = false;
    protected bool logLB = false;
    private Object logLock = new Object();  
    #endregion //Members

    #region Public properties
    public ListBox ListBox
    {
      get { return logBox; }
      set { logBox = value; }
    }

    public String FileName
    {
      get { return logFileName; }
      set { logFileName = value; }
    }

    public int MaxListBoxLines
    {
      get { return maxLBLines; }
      set { maxLBLines = value; }
    }

    public int MaxFileLength
    {
      get { return maxFileLength; }
      set { maxFileLength = value; }
    }

    public int MaxHistoryFiles
    {
      get { return maxHistoryFiles; }
      set { maxHistoryFiles = value; }
    }

    public int ListBoxVerbosity
    {
      get { return maxLBVerbosity; }
      set { maxLBVerbosity = value; }
    }

    public int FileVerbosity
    {
      get { return maxFileVerbosity; }
      set { maxFileVerbosity = value; }
    }

    public bool LogToFile
    {
      get { return logFile; }
      set { logFile = value; }
    }

    public bool LogToListBox
    {
      get { return logLB; }
      set { logLB = value; }
    }
    #endregion //Public properties

    #region Construction
    public Logger()
    {
      InitializeComponent();
    }

    public Logger(IContainer container)
    {
      container.Add(this);

      InitializeComponent();
    }
    #endregion //Construction

    #region Public methods
    public void LogText(int verbosity, String source, String text, params Object[] parameters)
    {
      lock (logLock)
      { //Lock the code - allow access to one thread only
        try
        {
          String head = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
          if (source.Length > 0)
            head += "  [" + source + "]";
          head = head.PadRight(40 + 4 * verbosity);
          text = String.Format(text, parameters);
          text = head + text;

          WriteToFile(text, verbosity);
          WriteToListBox(text, verbosity);
        }
        catch (Exception)
        { }
      }
    }

    public void LogText(int verbosity, String text, params Object[] parameters)
    {
      LogText(verbosity, "", text, parameters);
    }

    public void LogText(String text, params Object[] parameters)
    {
      LogText(0, "", text, parameters);
    }

    public void LogException(int startVerbosity, String source, Exception ex)
    {
      Exception exception = ex;
      int verbosity = startVerbosity;
      while (exception != null)
      {
        LogText(verbosity, source, exception.Message);
        verbosity++;
        exception = exception.InnerException;
      }
    }
    #endregion

    #region Helper methods
    private void WriteToFile(String message, int verbosity)
    {
      if (verbosity > maxFileVerbosity)
        return;
      if (!logFile || ((logFileWriter == null) && !CreateNewLogFile()))
        return;
      logFileWriter.WriteLine(message);
      logFileWriter.Flush();
      logFileInfo.Refresh();
      if (logFileInfo.Length >= maxFileLength)
        CreateNewLogFile();
    }

    private void WriteToListBox(String message, int verbosity)
    {
      if (verbosity > maxLBVerbosity)
        return;
      if (!logLB || (logBox == null))
        return;
      logBox.Items.Add(message);
      if (logBox.Items.Count > maxLBLines)
        logBox.Items.RemoveAt(0);
    }

    private bool CreateNewLogFile()
    {
      String oldName;
      String newName = "";
      if (!logFile)
        return false;
      try
      {
        if (logFileWriter != null)
        {
          logFileWriter.Close();
          logFileWriter.Dispose();
          logFileWriter = null;
        }
        try
        {
          if ((logFileInfo) != null && (logFileInfo.Length >= maxFileLength))
          { //Move old log files
            for (int i = maxHistoryFiles; i >= 0; i--)
            {
              if (i == 0)
                oldName = logFileName;
              else
                oldName = logFileName + "." + i.ToString();
              if (File.Exists(oldName))
              {
                if (newName == "")
                  File.Delete(oldName);
                else
                  File.Move(oldName, newName);
              }
              newName = oldName;
            }
          }
        }
        catch(Exception)
        { }
        FileStream stream = new FileStream(logFileName, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
        logFileWriter = new StreamWriter(stream, System.Text.Encoding.Unicode);
        logFileInfo = new FileInfo(logFileName);
      }
      catch(Exception)
      {
        logFileWriter = null;
        return false;
      }
      return true;
    }
    #endregion //Helper methods
  }
}
