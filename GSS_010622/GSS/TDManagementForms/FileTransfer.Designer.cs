namespace TDManagementForms
{
  partial class FileTransfer
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.statusStrip = new System.Windows.Forms.StatusStrip();
      this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
      this.btCancel = new System.Windows.Forms.Button();
      this.progressBar = new System.Windows.Forms.ProgressBar();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.lbSize = new System.Windows.Forms.Label();
      this.lbTransferred = new System.Windows.Forms.Label();
      this.statusStrip.SuspendLayout();
      this.SuspendLayout();
      // 
      // statusStrip
      // 
      this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
      this.statusStrip.Location = new System.Drawing.Point(0, 103);
      this.statusStrip.Name = "statusStrip";
      this.statusStrip.Size = new System.Drawing.Size(257, 22);
      this.statusStrip.TabIndex = 0;
      this.statusStrip.Text = "statusStrip1";
      // 
      // statusLabel
      // 
      this.statusLabel.Name = "statusLabel";
      this.statusLabel.Size = new System.Drawing.Size(35, 17);
      this.statusLabel.Text = "ready";
      // 
      // btCancel
      // 
      this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btCancel.Location = new System.Drawing.Point(91, 77);
      this.btCancel.Name = "btCancel";
      this.btCancel.Size = new System.Drawing.Size(75, 23);
      this.btCancel.TabIndex = 1;
      this.btCancel.Text = "Cancel";
      this.btCancel.UseVisualStyleBackColor = true;
      // 
      // progressBar
      // 
      this.progressBar.Location = new System.Drawing.Point(3, 46);
      this.progressBar.Name = "progressBar";
      this.progressBar.Size = new System.Drawing.Size(250, 23);
      this.progressBar.TabIndex = 2;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(0, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(47, 13);
      this.label1.TabIndex = 3;
      this.label1.Text = "File size:";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(0, 28);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(64, 13);
      this.label2.TabIndex = 4;
      this.label2.Text = "Transferred:";
      // 
      // lbSize
      // 
      this.lbSize.AutoSize = true;
      this.lbSize.Location = new System.Drawing.Point(80, 9);
      this.lbSize.Name = "lbSize";
      this.lbSize.Size = new System.Drawing.Size(0, 13);
      this.lbSize.TabIndex = 5;
      // 
      // lbTransferred
      // 
      this.lbTransferred.AutoSize = true;
      this.lbTransferred.Location = new System.Drawing.Point(80, 28);
      this.lbTransferred.Name = "lbTransferred";
      this.lbTransferred.Size = new System.Drawing.Size(0, 13);
      this.lbTransferred.TabIndex = 6;
      // 
      // FileTransfer
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(257, 125);
      this.Controls.Add(this.lbTransferred);
      this.Controls.Add(this.lbSize);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.progressBar);
      this.Controls.Add(this.btCancel);
      this.Controls.Add(this.statusStrip);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "FileTransfer";
      this.ShowIcon = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FileTransfer_FormClosed);
      this.Shown += new System.EventHandler(this.FileTransfer_Shown);
      this.statusStrip.ResumeLayout(false);
      this.statusStrip.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.StatusStrip statusStrip;
    private System.Windows.Forms.ToolStripStatusLabel statusLabel;
    private System.Windows.Forms.Button btCancel;
    private System.Windows.Forms.ProgressBar progressBar;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label lbSize;
    private System.Windows.Forms.Label lbTransferred;
  }
}