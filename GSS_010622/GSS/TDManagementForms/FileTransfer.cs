using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;

namespace TDManagementForms
{
  /// <summary>
  /// Form handles download or upload of one file
  /// </summary>
  public partial class FileTransfer : Form
  {
    protected FileService.FileService fileService;
    protected FileService.Credentials credentials;
    protected String locFileName;
    protected String locPath;
    protected String rmtFileName;
    protected String rmtPath;
    protected bool downloading;
    protected int fileChunkSize;
    protected uint fileSize = 0;
    protected uint transferredBytes = 0;
    protected String message;
    protected FileInfo locFileInfo = null;
    protected FileStream locFile = null;
    protected bool initialized = false;
    protected uint uploadChunkSize = 0;

    public FileTransfer()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Stores parameters and prepares fileService
    /// </summary>
    public void InitiateDialog(String locFileName, String locPath, String rmtFileName, String rmtPath, 
                                  FileService.FileService fileService, FileService.Credentials credentials, bool fileDownload, int fileChunkSize)
    {
      //Append backslash to paths
      if (!locPath.EndsWith("\\"))
        locPath += "\\";
      if (!rmtPath.EndsWith("\\"))
        rmtPath += "\\";
      //Store parameters
      this.fileService = fileService;
      this.credentials = credentials;
      this.locFileName = locFileName;
      this.locPath = locPath;
      this.rmtFileName = rmtFileName;
      this.rmtPath = rmtPath;
      this.fileChunkSize = fileChunkSize;
      //Append event handlers for fileService
      fileService.DownloadFileSegmentCompleted += new FileService.DownloadFileSegmentCompletedEventHandler(fileService_DownloadFileSegmentCompleted);
      fileService.UploadFileSegmentCompleted += new FileService.UploadFileSegmentCompletedEventHandler(fileService_UploadFileSegmentCompleted);
      downloading = fileDownload;
      initialized = true;
    }

    /// <summary>
    /// Starts download of one file from remote device to local disk.
    /// </summary>
    private void DownloadFile()
    {
      //Set form caption and status
      this.Text = rmtFileName + " download";
      statusLabel.Text = "initiating file transfer...";
      Update();

      try
      {
        //Create file info and file stream for local file
        locFile = File.Create(locPath + locFileName);
        locFileInfo = new FileInfo(locPath + locFileName);

        //Call download init method
        if (!fileService.DownloadFileInit(credentials, rmtPath, rmtFileName, out message, out fileSize))
          throw new Exception("File download init failed: " + message);
      }
      catch (Exception ex)
      {
        ErrorOccured(ex);
        return;
      }

      //Update form
      transferredBytes = 0;
      lbSize.Text = fileSize.ToString();
      lbTransferred.Text = transferredBytes.ToString();
      statusLabel.Text = "downloading file...";
      Update();
      //Start download
      DownloadFileChunk();
    }

    /// <summary>
    /// Calls asynchronously the download method of the file service
    /// </summary>
    protected void DownloadFileChunk()
    {
      try
      {
        uint requestedSize = Math.Min((uint)fileChunkSize, fileSize);
        fileService.DownloadFileSegmentAsync(credentials, rmtPath, rmtFileName, transferredBytes, requestedSize);
      }
      catch (Exception ex)
      {
        ErrorOccured(ex);
      }
    }

    /// <summary>
    /// Handles the completed event for download file chunk
    /// </summary>
    protected void fileService_DownloadFileSegmentCompleted(object sender, FileService.DownloadFileSegmentCompletedEventArgs e)
    {
      //Check for error
      if (e.Error != null)
      {
        ErrorOccured(e.Error);
        return;
      }
      if (!e.Result)
      {
        ErrorOccured(new Exception("Download file error: " + e.message));
        return;
      }
      //Save recieved data
      try
      {
        locFile.Write(e.data, 0, e.data.Length);
        transferredBytes += (uint)e.data.Length;
        //Update form
        if (fileSize > 0)
          progressBar.Value = (int)(((float)transferredBytes / (float)fileSize) * 100);
        lbTransferred.Text = transferredBytes.ToString();
        Update();
        //Check if download is finished
        if (transferredBytes >= fileSize)
          FinishDownload();
        else
          DownloadFileChunk(); //start downloading next chunk
      }
      catch (Exception ex)
      {
        ErrorOccured(ex);
      }
    }

    /// <summary>
    /// Finishes download operation, closes the form
    /// </summary>
    protected void FinishDownload()
    {
      //Update form
      statusLabel.Text = "finishing file transfer...";
      Update();

      try
      {
        //Call finish method
        byte[] digest = new byte[16];
        if (!fileService.DownloadFileDone(credentials, rmtPath, rmtFileName, out message, out digest))
          throw new Exception("Failed to finish file download: " + message);
        //Close file stream
        locFile.Dispose();
        //Close the form
        this.DialogResult = DialogResult.OK;
      }
      catch (Exception ex)
      {
        ErrorOccured(ex);
      }
    }

    /// <summary>
    /// Starts upload of one file from local to remote directory
    /// </summary>
    private void UploadFile()
    {
      //Set form caption and status
      this.Text = rmtFileName + " upload";
      statusLabel.Text = "initiating file transfer...";
      Update();

      try
      {
        //Create file info and file stream for local file
        locFileInfo = new FileInfo(locPath + locFileName);
        locFile = locFileInfo.OpenRead();
        fileSize = (uint)locFile.Length;

        //Call upload init method
        if (!fileService.UploadFileInit(credentials, rmtPath, rmtFileName, fileSize, out message))
          throw new Exception("File download init failed: " + message);
      }
      catch (Exception ex)
      {
        ErrorOccured(ex);
        return;
      }

      //Update form
      transferredBytes = 0;
      lbSize.Text = fileSize.ToString();
      lbTransferred.Text = transferredBytes.ToString();
      statusLabel.Text = "uploading file...";
      Update();
      //Start upload
      UploadFileChunk();
    }

    /// <summary>
    /// Calls asynchronously the upload method of the file service
    /// </summary>
    protected void UploadFileChunk()
    {
      try
      {
        //Read file chunk to buffer
        byte[] buffer = new byte[fileChunkSize];
        uploadChunkSize = (uint)locFile.Read(buffer, 0, buffer.Length);
        if (uploadChunkSize < buffer.Length)
          Array.Resize<Byte>(ref buffer, (int)uploadChunkSize);
        
        //Check if there is something to send
        if (uploadChunkSize > 0)
        {//Send file chunk asynchronously to server
          fileService.UploadFileSegmentAsync(credentials, rmtPath, rmtFileName, transferredBytes, uploadChunkSize, buffer);
        }
        else
        {//Finish upload
          FinishUpload();
        }
      }
      catch (Exception ex)
      {
        ErrorOccured(ex);
      }
    }
     
    /// <summary>
    /// Handles the completed event for upload file chunk
    /// </summary>
    protected void fileService_UploadFileSegmentCompleted(object sender, FileService.UploadFileSegmentCompletedEventArgs e)
    {
      //Check for error
      if (e.Error != null)
      {
        ErrorOccured(e.Error);
        return;
      }
      if (!e.Result)
      {
        ErrorOccured(new Exception("Upload file error: " + e.message));
      }
      transferredBytes += uploadChunkSize;
      //Update form
      if (fileSize > 0)
        progressBar.Value = (int)(((float)transferredBytes / (float)fileSize) * 100);
      lbTransferred.Text = transferredBytes.ToString();
      Update();
      //Upload next chunk or finish
      UploadFileChunk();
    }

    /// <summary>
    /// Finishes upload operation, closes the form
    /// </summary>
    protected void FinishUpload()
    {
      //Update form
      statusLabel.Text = "finishing file transfer...";
      Update();

      try
      {
        //Compute MD5 digest for file
        byte[] digest;
        locFile.Seek(0, SeekOrigin.Begin);
        MD5 hashGenerator = MD5CryptoServiceProvider.Create();
        digest = hashGenerator.ComputeHash(locFile);

        locFile.Seek(0, SeekOrigin.Begin);
        byte[] tmpBuff = new byte[locFile.Length];
        locFile.Read(tmpBuff, 0, (int)locFile.Length);
        byte[] digest2;
        digest2 = hashGenerator.ComputeHash(tmpBuff);

        //Call finish method
        if (!fileService.UploadFileDone(credentials, rmtPath, rmtFileName, digest, out message))
          throw new Exception("Failed to finish file upload: " + message);
        //Close file stream
        locFile.Dispose();
        //Close the form
        this.DialogResult = DialogResult.OK;
      }
      catch (Exception ex)
      {
        ErrorOccured(ex);
      }
    }

    /// <summary>
    /// Displays error message and closes file transfer dialog
    /// </summary>
    /// <param name="ex"></param>
    protected void ErrorOccured(Exception ex)
    {
      //Show error mesage
      MessageBox.Show(ex.Message, "File transfer error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      //Remove local file if exists
      try
      {
        if (locFile != null)
          locFile.Dispose();
        if ((locFileInfo != null) && downloading)
          locFileInfo.Delete();
      }
      catch (Exception)
      { }
      //Close the form
      this.DialogResult = DialogResult.Abort;
    }

    private void FileTransfer_FormClosed(object sender, FormClosedEventArgs e)
    {
      //Remove event handlers for fileService
      if (fileService != null)
      {
        fileService.DownloadFileSegmentCompleted -= this.fileService_DownloadFileSegmentCompleted;
        fileService.UploadFileSegmentCompleted -= this.fileService_UploadFileSegmentCompleted;
      }

    }

    private void FileTransfer_Shown(object sender, EventArgs e)
    {
      if (initialized && downloading)
        DownloadFile();
      else if (initialized)
        UploadFile();
      else
        DialogResult = DialogResult.OK;
    }
  }
}