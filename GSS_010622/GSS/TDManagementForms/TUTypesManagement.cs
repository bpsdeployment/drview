using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace TDManagementForms
{
  /// <summary>
  /// Form for management of registered TU Types
  /// </summary>
  public partial class TUTypesManagement : Form
  {
    /// <summary>
    /// Creates the form and initializes connection to database
    /// </summary>
    /// <param name="dbConnection"></param>
    public TUTypesManagement(SqlConnection dbConnection)
    {
      InitializeComponent();
      //Store database connection
      tUTypesTableAdapter.Connection = dbConnection;
    }

    /// <summary>
    /// Fill table in DataSet with values from database
    /// </summary>
    private void TUTypesManagement_Load(object sender, EventArgs e)
    {
      this.tUTypesTableAdapter.Fill(this.tUTypesDataSet.TUTypes);
    }

    /// <summary>
    /// Create new TUTypeID for added row
    /// </summary>
    private void dgvTUTypes_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e)
    {
      e.Row.Cells["tUTypeIDDataGridViewTextBoxColumn"].Value = Guid.NewGuid();
    }

    /// <summary>
    /// Refresh values in table
    /// </summary>
    private void tsbRefresh_Click(object sender, EventArgs e)
    {
      this.tUTypesTableAdapter.Fill(this.tUTypesDataSet.TUTypes);
    }

    /// <summary>
    /// Commint changes to DB
    /// </summary>
    private void tsbCommit_Click(object sender, EventArgs e)
    {
      this.tUTypesTableAdapter.Update(this.tUTypesDataSet.TUTypes);
      this.tUTypesTableAdapter.Fill(this.tUTypesDataSet.TUTypes);
    }

    /// <summary>
    /// Data validation error - display message
    /// </summary>
    private void dgvTUTypes_DataError(object sender, DataGridViewDataErrorEventArgs e)
    {
      e.ThrowException = false;
      e.Cancel = false;
      MessageBox.Show(String.Format("Invalid {0} at row {1}", dgvTUTypes.Columns[e.ColumnIndex].HeaderText, e.RowIndex), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
    }
  }
}