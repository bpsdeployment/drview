namespace BombardierE464DDSImport
{
  partial class DDSImport
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.logger = new TDManagementForms.Logger(this.components);
      // 
      // logger
      // 
      this.logger.FileName = "DDSImport-E464.log";
      this.logger.FileVerbosity = 5;
      this.logger.ListBox = null;
      this.logger.ListBoxVerbosity = 3;
      this.logger.LogToFile = true;
      this.logger.LogToListBox = false;
      this.logger.MaxFileLength = 4000000;
      this.logger.MaxHistoryFiles = 3;
      this.logger.MaxListBoxLines = 1000;
      // 
      // DDSImport
      // 
      /*SERVICE*///this.ServiceName = "DDSImport";

    }

    #endregion

    private TDManagementForms.Logger logger;
  }
}
