using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;
using System.Xml;

namespace BombardierE464DDSImport
{
    /// <summary>
    /// BT_Record class
    /// </summary>
    public partial class BT_Record
    {
        public enum RecordResult { EMPTY = 0, OK, HEADER_TOO_SHORT, BAD_STARTTIME, BAD_ENDTIME, ENVDATA_TOO_SHORT };
        public static string[] RecordResultText = { "EMPTY", "OK", "HEADER_TOO_SHORT", "BAD_STARTTIME", "BAD_ENDTIME", "ENVDATA_TOO_SHORT" };

        private RecordResult _Status;
        private string _DateTimeFormat;
        private bool _AdditionalInfo;

        public int RefNum;
        public byte ProcNum;
        public int DDSID;
        public byte ProgAnom;
        public byte GRW;
        public byte FlagActive;

        public DateTime StartTime;
        public DateTime EndTime;

        public byte BlocksNum;
        public byte BlockLen;
        public byte TrigPtr;
        public byte OldPtr;

        public byte[] EnvData;

        public BT_Record()
        {
            _Status = RecordResult.EMPTY;
            _DateTimeFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'";
            _AdditionalInfo = true;
        }

        public RecordResult Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
            }
        }
        public string DateTimeFormat
        {
            get
            {
                return _DateTimeFormat;
            }
            set
            {
                _DateTimeFormat = value;
            }
        }


        public void LoadEnvData(byte[] buffer)
        {
            EnvData = new byte[buffer.Length];
            int i = 0;
            foreach (byte c in buffer)
                EnvData[i++] = c;
        }

        public string ToBase64String()
        {
            return Convert.ToBase64String(EnvData);
        }

        public XmlNode ToXmlNode(XmlDocument xmldoc)
        {
            XmlNode n = xmldoc.CreateElement("DDS");
            XmlAttribute n1 = xmldoc.CreateAttribute("PROCESS_NR");
            XmlAttribute n2 = xmldoc.CreateAttribute("DIST_NR");
            XmlAttribute n3 = xmldoc.CreateAttribute("StartTime");
            XmlAttribute n4 = xmldoc.CreateAttribute("EndTime");
            XmlAttribute n5 = xmldoc.CreateAttribute("LastModified");
            n1.Value = ProcNum.ToString();
            n2.Value = DDSID.ToString();
            n3.Value = StartTime.ToString(_DateTimeFormat);
            if (EndTime != DateTime.MinValue)
                n4.Value = EndTime.ToString(_DateTimeFormat);
            else
                n4.Value = "";
            if (EndTime != DateTime.MinValue)
                n5.Value = EndTime.ToString(_DateTimeFormat);
            else
                n5.Value = StartTime.ToString(_DateTimeFormat);
            //n4.Value = "";
            n.Attributes.Append(n1);
            n.Attributes.Append(n2);
            n.Attributes.Append(n3);
            if (n4.Value != "") n.Attributes.Append(n4);
            n.Attributes.Append(n5);
            if (_AdditionalInfo)
            {
                XmlAttribute an1 = xmldoc.CreateAttribute("BlocksNum");
                XmlAttribute an2 = xmldoc.CreateAttribute("BlockLen");
                XmlAttribute an3 = xmldoc.CreateAttribute("TrigPtr");
                an1.Value = BlocksNum.ToString();
                an2.Value = BlockLen.ToString();
                an3.Value = TrigPtr.ToString();
                n.Attributes.Append(an1);
                n.Attributes.Append(an2);
                n.Attributes.Append(an3);
            }
            n.InnerText = ToBase64String();
            return n;
        }
    }

    /// <summary>
    /// BT_FileConvert class
    /// </summary>
    public partial class BT_FileConvert
    {
        public enum ConvertResult
        {
            EMPTY = 0,
            OK,
            HEADER_TOO_SHORT, 
            BAD_HEADER1, 
            BAD_HEADER2,
            BAD_VEHICLE_NR,
            BAD_DATETIME,
            BAD_NDDS, 
            BAD_FILE_LEN, 
            BAD_RECORD, 
            UNPARSED_CHARS,
            EMPTY_FILE
        };
        public static string[] RecordResultText = 
        { 
            "EMPTY",
            "OK",
            "HEADER_TOO_SHORT", 
            "BAD_HEADER1", 
            "BAD_HEADER2",
            "BAD_VEHICLE_NR",
            "BAD_DATETIME",
            "BAD_NDDS", 
            "BAD_FILE_LEN", 
            "BAD_RECORD", 
            "UNPARSED_CHARS",
            "EMPTY_FILE"
        };

        private ConvertResult _Status;
        private string _DateTimeFormat;
        private string _VehicleNameFormat;
        private string _VehicleName2Format;
        private string _VersionFormat;
        private string _FileName;
        private string _BadFileDir;

        public string ProjectName;
        public string Version;
        public string Vehicle;
        public int nDDS;
        public int FileLen;
        public BT_Record.RecordResult LastRecordStatus;

        //"DD_XPS2.00PV"
        private static char[] FileHead_1 = { 'D', 'D', '_', 'X', 'P', 'S', '2', '.', '0', '0', 'P', 'V' };
        //"IN"
        private static char[] FileHead_2 = { 'I', 'N' };
        private byte[] buffer;

        private ArrayList Records;

        public BT_FileConvert()
        {
            _BadFileDir = "";
            _Status = ConvertResult.EMPTY;
            _DateTimeFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.fff'Z'";
            _VehicleNameFormat = "{0}-{1:D3}";
            _VehicleName2Format = "{0}-{1}";
            _VersionFormat = "{0}-{1}";
        }

        public string BadFileDir
        {
            get
            {
                return _BadFileDir;
            }
            set
            {
                _BadFileDir = value;
            }
        }
        public ConvertResult Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
            }
        }
        public string DateTimeFormat
        {
            get
            {
                return _DateTimeFormat;
            }
            set
            {
                _DateTimeFormat = value;
            }
        }
        public string VehicleNameFormat
        {
            get
            {
                return _VehicleNameFormat;
            }
            set
            {
                _VehicleNameFormat = value;
            }
        }
        public string VehicleName2Format
        {
            get
            {
                return _VehicleName2Format;
            }
            set
            {
                _VehicleName2Format = value;
            }
        }
        public string VersionFormat
        {
            get
            {
                return _VersionFormat;
            }
            set
            {
                _VersionFormat = value;
            }
        }


        public bool ConvertDirectory(string dirpath, string VehicleNameFormat)
        {
            _VehicleNameFormat = VehicleNameFormat;
            return ConvertDirectory(dirpath);
        }
        public bool ConvertDirectory(string dirpath)
        {
            DirectoryInfo DirInfo = new DirectoryInfo(dirpath);
            DirInfo.Refresh();
            foreach (FileInfo file in DirInfo.GetFiles("*", SearchOption.TopDirectoryOnly))
            {
                string filename = file.FullName;
                ConvertFile(filename);
            }
            return true;
        }


        public bool ConvertFile(string filename, string VehicleNameFormat)
        {
            _VehicleNameFormat = VehicleNameFormat;
            return ConvertFile(filename);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool ConvertFile(string filename)
        {
            bool rtn = false;

            ConvertResult res = LoadFile(filename);
            if (res == ConvertResult.OK || res == ConvertResult.BAD_RECORD || res==ConvertResult.UNPARSED_CHARS)
            {
                Save();
                rtn = true;
            }
            if (res != ConvertResult.OK && _BadFileDir != "")
            {
                FileInfo file = new FileInfo(filename);
                file.CopyTo(Path.Combine(_BadFileDir, file.Name), true);
            }
            return rtn;
        }

        /// <summary>
        /// Save decoded data into a xml file with filename given at load time
        /// </summary>
        /// <returns>true if no error</returns>
        public bool Save()
        {
            string xmlfilename = _FileName + ".xml";
            return SaveAs(xmlfilename);
        }
        /// <summary>
        /// Save decoded data into a xml file as given filename
        /// </summary>
        /// <param name="filename">Full file name</param>
        /// <returns>true if no error</returns>
        public bool SaveAs(string filename)
        {
            bool rtn = true;
            try
            {
                XmlDocument doc = GetXML();
                doc.Save(filename);
            }
            catch (Exception e)
            {
                rtn = false;
            }
            finally
            {
            }
            return rtn;
        }

        /// <summary>
        /// Format all decoded records into xml document
        /// </summary>
        /// <returns></returns>
        public XmlDocument GetXML()
        {
            XmlDocument xmldoc = new XmlDocument();
            //XmlDeclaration dec = xmldoc.CreateXmlDeclaration("1.0", null, "no");
            XmlDeclaration dec = xmldoc.CreateXmlDeclaration("1.0", System.Text.Encoding.UTF8.HeaderName, "no");
            xmldoc.AppendChild(dec);
            XmlNode root = xmldoc.CreateElement("DDSExport");
            XmlAttribute a1 = xmldoc.CreateAttribute("BigEndianData");
            XmlAttribute a2 = xmldoc.CreateAttribute("ConfigID");
            XmlAttribute a3 = xmldoc.CreateAttribute("Version");
            XmlAttribute a4 = xmldoc.CreateAttribute("VehicleID");
            a1.Value = "false";
            a2.Value = Version;
            a3.Value = String.Format(VersionFormat, ProjectName, Version);
            a4.Value = Vehicle;
            root.Attributes.Append(a1);
            root.Attributes.Append(a2);
            root.Attributes.Append(a3);
            root.Attributes.Append(a4);
            
            foreach (BT_Record rec in Records)
            {
                if (rec.Status == BT_Record.RecordResult.OK)
                {
                    XmlNode n = rec.ToXmlNode(xmldoc);
                    root.AppendChild(n);
                }
            }
            xmldoc.AppendChild(root);
            //string t = xmldoc.OuterXml;
            return xmldoc;
        }

        /// <summary>
        /// LoadFile a file
        /// </summary>
        /// <param name="filename">Full file name to load</param>
        /// <returns></returns>
        public ConvertResult LoadFile(string filename)
        {
            return LoadFile(filename, _VehicleNameFormat);
        }
        /// <summary>
        /// Load a file with a given vehicle name format
        /// </summary>
        /// <param name="filename">Full file name to load</param>
        /// <param name="VehicleNameFormat">Two parameter format string such as "{0}-{1:D3}"</param>
        /// <returns></returns>
        public ConvertResult LoadFile(string filename, string VehicleNameFormat)
        {
            _FileName = filename;
            Records = new ArrayList();
            _Status = ConvertResult.EMPTY;
            LastRecordStatus = BT_Record.RecordResult.EMPTY;
            FileStream fs = new FileStream(filename, FileMode.Open);

            buffer = new byte[80];
            int n = fs.Read(buffer, 0, 80);
            if (n < 80)
            {
                fs.Close();
                _Status = ConvertResult.HEADER_TOO_SHORT;
                return _Status;
            }

            _Status = GetHeader(VehicleNameFormat, Path.GetFileNameWithoutExtension(filename), Path.GetExtension(filename));
            if (_Status != ConvertResult.OK)
            {
                fs.Close();
                return _Status;
            }

            if (nDDS == 0)
            {
                fs.Close();
                return ConvertResult.EMPTY_FILE;
            }

            for (int recNr = 0; recNr < nDDS; recNr++)
            {
                // new record
                BT_Record rec = new BT_Record();
                rec.DateTimeFormat = _DateTimeFormat;
                // read record header
                buffer = new byte[48];
                n = fs.Read(buffer, 0, 48);
                if (n < 48)
                {
                    fs.Close();
                    LastRecordStatus = BT_Record.RecordResult.HEADER_TOO_SHORT;
                    rec.Status = LastRecordStatus;
                    _Status = ConvertResult.BAD_RECORD;
                    return _Status;
                }
                LastRecordStatus = GetRecordHeader(rec);
                rec.Status = LastRecordStatus;
                if (rec.Status != BT_Record.RecordResult.OK)
                {
                    fs.Close();
                    _Status = ConvertResult.BAD_RECORD;
                    return _Status;
                }
                // read Env Data
                int datalen = rec.BlockLen * rec.BlocksNum * 2;
                buffer = new byte[datalen];
                n = fs.Read(buffer, 0, datalen);
                if (n < datalen)
                {
                    fs.Close();
                    LastRecordStatus = BT_Record.RecordResult.ENVDATA_TOO_SHORT;
                    rec.Status = LastRecordStatus;
                    _Status = ConvertResult.BAD_RECORD;
                    return _Status;
                }
                rec.LoadEnvData(buffer);
                Records.Add(rec);
            }

            if (fs.Position != fs.Length)
            {
                fs.Close();
                _Status = ConvertResult.UNPARSED_CHARS;
                return _Status;
            }

            fs.Close();
            _Status = ConvertResult.OK;
            return _Status;
        }

        private ConvertResult GetHeader(string VehicleNameFormat, string filename, string fileextension)
        {
            if (!Compare(FileHead_1, 0))
            {
                return ConvertResult.BAD_HEADER1;
            }
            else if (!Compare(FileHead_2, 14))
            {
                return ConvertResult.BAD_HEADER2;
            }
            //string _PV;
            string _VehicleName;
            string _NVe;
            string _DateTime;
            string _nDDS;
            string _FileLen;

            //_PV = GetString(12, 2);
            Version = GetString(12, 2);
            _VehicleName = GetString(16, 12).Trim();
            _NVe = GetString(29, 3);
            _DateTime = GetString(32, 13);
            _nDDS = GetString(48, 4);
            _FileLen = GetString(52, 8);
            ProjectName = GetString(60, 8).Trim();

            if (_VehicleName == "VEHICLE")
            {
                int pos = filename.IndexOf("__");
                if (pos > 0)
                {
                    //Vehicle = ProjectName.Trim() + "-" + filename.Substring(0, pos);
                    Vehicle = String.Format(VehicleName2Format, ProjectName, filename.Substring(0, pos));
                }
            }
            else
            {
                int nVe = 0;
                if (!int.TryParse(_NVe, out nVe))
                    return ConvertResult.BAD_VEHICLE_NR;
                Vehicle = string.Format(VehicleNameFormat, _VehicleName, nVe);
            }
            if (!int.TryParse(_nDDS, System.Globalization.NumberStyles.HexNumber, null, out nDDS))
                return ConvertResult.BAD_NDDS;

            if (!int.TryParse(_FileLen, System.Globalization.NumberStyles.HexNumber, null, out FileLen))
                return ConvertResult.BAD_FILE_LEN;

            return ConvertResult.OK;
        }

        private BT_Record.RecordResult GetRecordHeader(BT_Record rec)
        {
            //RefNum2 = (byte) buffer[0];
            //RefNum1 = (byte)buffer[1];
            rec.RefNum = 256 * (byte)buffer[1] + (byte)buffer[0];
            rec.ProcNum = (byte)buffer[5];
            rec.DDSID = 256 * buffer[9] + buffer[8];
            rec.ProgAnom = (byte)buffer[10];
            rec.GRW = (byte)buffer[13];
            rec.FlagActive = (byte)buffer[16];

            string sStartTime;
            //date
            sStartTime = string.Format("20{0:X2}-{1:X2}-{2:X2}", (byte)buffer[18], (byte)buffer[19], (byte)buffer[20]);
            //time
            sStartTime += string.Format("T{0:X2}:{1:X2}:{2:X2}", (byte)buffer[21], (byte)buffer[22], (byte)buffer[23]);
            //milli
            sStartTime += string.Format(".{0:D3}", 256 * (byte)buffer[25] + (byte)buffer[24]);
            //micro
            sStartTime += string.Format("{0:D3}", 256 * (byte)buffer[27] + (byte)buffer[26]);

            if (sStartTime == string.Format("20{0:X2}-{1:X2}-{2:X2}T{3:X2}:{4:X2}:{5:X2}.{6:D3}{7:D3}", 0, 0, 0, 0, 0, 0, 0, 0))
                rec.StartTime = DateTime.MinValue;
            else
                if (!DateTime.TryParse(sStartTime, out rec.StartTime))
                    return BT_Record.RecordResult.BAD_STARTTIME;
                else
                    rec.StartTime = rec.StartTime.ToUniversalTime();

            string sEndTime;
            //date
            sEndTime = string.Format("20{0:X2}-{1:X2}-{2:X2}", (byte)buffer[28], (byte)buffer[29], (byte)buffer[30]);
            //time
            sEndTime += string.Format("T{0:X2}:{1:X2}:{2:X2}", (byte)buffer[31], (byte)buffer[32], (byte)buffer[33]);
            //milli
            sEndTime += string.Format(".{0:D3}", 256 * (byte)buffer[35] + (byte)buffer[34]);
            //micro
            sEndTime += string.Format("{0:D3}", 256 * (byte)buffer[37] + (byte)buffer[36]);

            if (sEndTime == string.Format("20{0:X2}-{1:X2}-{2:X2}T{3:X2}:{4:X2}:{5:X2}.{6:D3}{7:D3}", 0, 0, 0, 0, 0, 0, 0, 0))
                rec.EndTime = DateTime.MinValue;
            else
                if (!DateTime.TryParse(sEndTime, out rec.EndTime))
                    return BT_Record.RecordResult.BAD_ENDTIME;
                else
                    rec.EndTime = rec.EndTime.ToUniversalTime();

            rec.BlocksNum = (byte)buffer[40];
            rec.BlockLen = (byte)buffer[41];
            rec.TrigPtr = (byte)buffer[42];
            rec.OldPtr = (byte)buffer[43];

            return BT_Record.RecordResult.OK;
        }

        private bool Compare(char[] par, int pos)
        {
            foreach (byte c in par)
                if (c != buffer[pos++])
                    return false;
            return true;
        }
        private string GetString(int pos, int len)
        {
            string rtn = "";
            for (int i = 0; i < len; i++)
                rtn += (char)buffer[pos + i];
            return rtn;
        }
    }
}
