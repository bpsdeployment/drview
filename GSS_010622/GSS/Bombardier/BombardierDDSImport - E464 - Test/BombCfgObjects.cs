using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using System.Data;
using TDManagementForms;
using System.Collections;

namespace BombardierE464DDSImport
{
    /// <summary>
    /// Configuration of all TU instance and config versions
    /// </summary>
    public class BombConfig
    {
        #region Private members
        private List<BombTUType> tuTypes;
        private Dictionary<String, Guid> tuInstIDs; //Dictionary of TU Instances IDS
        private Dictionary<String, BombTUType> tuInstTypes; //Types of TU Instances
        private Logger logger;
        private bool isConfigured;
        #endregion

        #region Public Properties
        /// <summary>
        /// Correct configuration 
        /// </summary>
        public bool IsConfigured
        {
            get { return isConfigured; }
        }
        #endregion

        #region Construction
        /// <summary>
        /// Standard constructor
        /// </summary>
        public BombConfig(Logger logger)
        {
            tuTypes = new List<BombTUType>();
            tuInstIDs = new Dictionary<string, Guid>();
            tuInstTypes = new Dictionary<string, BombTUType>();
            this.logger = logger;
            isConfigured = false;
        }
        #endregion

        #region Configuration
        /// <summary>
        /// Creates configuration objects based on the XML configuration file and data from DB specified by connection string
        /// </summary>
        public void CreateConfiguration(String connString, String cfgFileName)
        {
            logger.LogText(0, "BombCfg", "Creating configuration from file {0}", cfgFileName);
            isConfigured = false;
            SqlConnection dbConn = null;
            try
            {
                //Clear old configuration
                tuInstIDs.Clear();
                tuInstTypes.Clear();

                //Create connection to database
                dbConn = new SqlConnection(connString);
                dbConn.Open();

                //Create XML document from configuration file
                XmlDocument cfgDoc = new XmlDocument();
                cfgDoc.Load(cfgFileName);

                //Select all TUType elements
                XmlNodeList typeElems = cfgDoc.SelectNodes("//TUType");
                foreach (XmlElement typeElem in typeElems)
                {
                    //Create new type
                    String typeName = typeElem.GetAttribute("Name");
                    String instNamePrefix = typeElem.GetAttribute("InstNamePrefix");
                    BombTUType tuType = new BombTUType(this, typeName, instNamePrefix, logger);
                    logger.LogText(1, "BombCfg", "Created TU Type {0}. Instance name prefix {1}", typeName, instNamePrefix);
                    //Create type configuration
                    tuType.ReadProcessorsConfig(typeElem);
                    tuType.ReadTypeConfigurations(dbConn);
                    //Add type to type list
                    tuTypes.Add(tuType);
                }
                logger.LogText(0, "BombCfg", "Configuration created. {0} TU Types, {1} TU Instances", tuInstTypes.Count, tuInstIDs.Count);
                isConfigured = true;
            }
            catch (Exception ex)
            {
                logger.LogText(0, "BombCfg", "Failed to create configuration: {0}", ex.Message);
                throw;
            }
            finally
            {
                //Dispose database connection
                if (dbConn != null)
                {
                    dbConn.Close();
                    dbConn.Dispose();
                }
            }
        }

        /// <summary>
        /// Add new TU Instance to repository
        /// </summary>
        public void AddTUInstance(String name, Guid instanceID, BombTUType tuType)
        {
            if (!tuInstIDs.ContainsKey(name))
                tuInstIDs.Add(name, instanceID);
            if (!tuInstTypes.ContainsKey(name))
                tuInstTypes.Add(name, tuType);
        }

        /// <summary>
        /// Creates new TU Instance in database and returns ID of created instance
        /// </summary>
        public Guid CreateNewTUInstance(String instanceName, String userVersion, String connString)
        {
            Guid tuInstID;
            //First determine TU Type of the instance
            BombTUType tuType = null;
            foreach (BombTUType tmpType in tuTypes)
                if (tmpType.IsTypeOfInstance(instanceName))
                {
                    tuType = tmpType;
                    break;
                }
            if (tuType == null)
                throw new Exception("Unable to determine TU Type of TU instance " + instanceName);

            //Find DDDVersion
            BombDDDVersion dddVersion = tuType.GetDDDVersion(userVersion);

            //Execute DB procedure to create new TU Type
            SqlConnection dbConn = null;
            SqlCommand dbCmd = null;
            try
            {
                //Create connection to database
                dbConn = new SqlConnection(connString);
                dbConn.Open();

                //Create db command
                dbCmd = new SqlCommand("InsertTUInstance", dbConn);
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add("@TUInstanceID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                dbCmd.Parameters.Add("@TUTypeName", SqlDbType.NVarChar, 50).Value = tuType.Name;
                dbCmd.Parameters.Add("@DDDVersionID", SqlDbType.UniqueIdentifier).Value = dddVersion.DDDVersionID;
                dbCmd.Parameters.Add("@TUName", SqlDbType.NVarChar, 50).Value = instanceName;
                dbCmd.Parameters.Add("@Comment", SqlDbType.NVarChar, 255).Value = "Automatically added by DDS Import service";

                //Execute db command
                dbCmd.ExecuteNonQuery();

                //Retrieve ID of created TU Instance
                tuInstID = (Guid)dbCmd.Parameters["@TUInstanceID"].Value;

                //Add created TU Instance to repository
                AddTUInstance(instanceName, tuInstID, tuType);

                //Return Instance ID
                return tuInstID;
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to create new TU Instance " + instanceName, ex);
            }
            finally
            {
                //Dispose command
                if (dbCmd != null)
                    dbCmd.Dispose();
                //Dispose database connection
                if (dbConn != null)
                {
                    dbConn.Close();
                    dbConn.Dispose();
                }
            }
        }

        public int UpdateRecordSampling(int recordDefID, int beforeStart, int pastStart, Guid userVersion, String connString)
        {

            //Execute DB procedure to create new TU Type
            SqlConnection dbConn = null;
            SqlCommand dbCmd = null;
            try
            {
                //Create connection to database
                dbConn = new SqlConnection(connString);
                dbConn.Open();

                //Create db command
                dbCmd = new SqlCommand("UpdateRecordSampleValues", dbConn);
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add("@DDDVersionID", SqlDbType.UniqueIdentifier).Value = userVersion;
                dbCmd.Parameters.Add("@RecordDefID", SqlDbType.Int).Value = recordDefID;
                dbCmd.Parameters.Add("@BeforeStart", SqlDbType.Int).Value = beforeStart;
                dbCmd.Parameters.Add("@PastStart", SqlDbType.Int).Value = pastStart;
                dbCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;

                //Execute db command
                dbCmd.ExecuteNonQuery();

                //Retrieve ID of created TU Instance
                int error = (int)dbCmd.Parameters["@Error"].Value;

                //Return Error
                return error;
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to update record sampling values for recordID: " + recordDefID.ToString(), ex);
            }
            finally
            {
                //Dispose command
                if (dbCmd != null)
                    dbCmd.Dispose();
                //Dispose database connection
                if (dbConn != null)
                {
                    dbConn.Close();
                    dbConn.Dispose();
                }
            }
        }
        #endregion

        #region Config acces
        /// <summary>
        /// Get ID of TU Instance
        /// </summary>
        public Guid GetTUInstanceID(String name)
        {
            Guid id;
            if (tuInstIDs.TryGetValue(name, out id))
                return id;
            else
                throw new Exception(String.Format("TU Instance {0} not found in configuration", name));
        }

        /// <summary>
        /// Get type of TU Instance
        /// </summary>
        public BombTUType GetTUType(String instName)
        {
            BombTUType type;
            if (tuInstTypes.TryGetValue(instName, out type))
                return type;
            else
                throw new Exception(String.Format("TU Instance {0} not found in configuration", instName));
        }

        /// <summary>
        /// Return true in case instance is registered in repository
        /// </summary>
        public bool InstanceExists(String instanceName)
        {
            return tuInstIDs.ContainsKey(instanceName);
        }
        #endregion
    }

    /// <summary>
    /// Represents one type of bombardier diagnostic application (i.e. NCDP)
    /// </summary>
    public class BombTUType
    {
        #region Private members
        public String Name; //Name of diagnostic application
        private String InstNamePrefix; //Prefix of instance name used to determine TUType of the instance
        private Logger logger; //logger object
        private BombConfig configObject; //parent configuration object

        private Dictionary<int, BombProcessor> processors; //Dictionary of processors for diagnostic application
        private Dictionary<String, BombDDDVersion> dddVersions; //Dictionary of DDD versions
        #endregion

        #region Construction
        /// <summary>
        /// Standard constructor
        /// </summary>
        public BombTUType(BombConfig parent, String name, String instNamePrefix, Logger logger)
        {
            configObject = parent;
            Name = name;
            InstNamePrefix = instNamePrefix;
            processors = new Dictionary<int, BombProcessor>();
            dddVersions = new Dictionary<string, BombDDDVersion>();
            this.logger = logger;
        }
        #endregion

        #region Configuration
        /// <summary>
        /// Add ddd version to repository
        /// </summary>
        public void AddDDDVersion(String userVersion, Guid versionID)
        {
            if (!dddVersions.ContainsKey(userVersion))
                dddVersions.Add(userVersion, new BombDDDVersion(userVersion, versionID));
        }

        /// <summary>
        /// Add record definition to repository of specified DDD version
        /// </summary>
        public void AddRecordDef(String userVersion, String recName, int recDefID)
        {
            BombDDDVersion dddVer = GetDDDVersion(userVersion);
            dddVer.AddRecordDef(recName, recDefID);
        }

        public void AddRecordSample(String userVersion, int recDefID, int beforeStart, int pastStart)
        {
            BombDDDVersion dddVer = GetDDDVersion(userVersion);
            dddVer.AddRecordSamples(recDefID, beforeStart, pastStart);
        }

        /// <summary>
        /// Adds list of processors from configuration XmlElement
        /// </summary>
        public void ReadProcessorsConfig(XmlElement tuTypeElem)
        {
            logger.LogText(1, "BombCfg", "Reading processors configuration of type {0}", Name);
            processors.Clear();
            XmlNodeList procs = tuTypeElem.SelectNodes("./Processor");
            foreach (XmlElement procElem in procs)
            {
                int procNum = Convert.ToInt32(procElem.GetAttribute("Number"));
                BombProcessor proc = new BombProcessor(
                  procNum,
                  procElem.GetAttribute("EvtName"),
                  procElem.GetAttribute("SrcName")
                  );
                processors.Add(procNum, proc);
            }
            logger.LogText(2, "BombCfg", "Found {0} processors", procs.Count);
            logger.LogText(1, "BombCfg", "Reading of processor configuration finished");
        }

        /// <summary>
        /// Read configurations of this TU Type from db
        /// </summary>
        /// <param name="dbConn"></param>
        public void ReadTypeConfigurations(SqlConnection dbConn)
        {
            SqlCommand cmd = null;
            SqlDataReader reader = null;
            int count;
            try
            {
                logger.LogText(1, "BombCfg", "Reading configuration for type {0} from database", Name);
                //Execute stored procedure in DB to obtain configuration
                cmd = new SqlCommand("Bombardier_GetConfigs", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@TUTypeName", SqlDbType.NVarChar, 50).Value = Name;
                reader = cmd.ExecuteReader();

                //Read list of TU instance
                count = 0;
                while (reader.Read())
                {
                    //Add TUInstance to parent object
                    configObject.AddTUInstance(reader.GetString(0), reader.GetGuid(1), this);
                    count++;
                }
                logger.LogText(2, "BombCfg", "{0} TU Instances", count);

                reader.NextResult();

                //Read list of DDD versions
                count = 0;
                while (reader.Read())
                {
                    //Add DDD versions to repository
                    AddDDDVersion(reader.GetString(0), reader.GetGuid(1));
                    count++;
                }
                logger.LogText(2, "BombCfg", "{0} DDD Versions", count);

                reader.NextResult();

                //Read list of record definitions
                count = 0;
                while (reader.Read())
                {
                    AddRecordDef(reader.GetString(0), reader.GetString(1), reader.GetInt32(2));
                    count++;
                }
                logger.LogText(2, "BombCfg", "{0} Record definitions", count);

                reader.NextResult();

                //Read list of record sampling
                count = 0;
                while (reader.Read())
                {
                    AddRecordSample(reader.GetString(0), reader.GetInt32(1), reader.GetInt32(2), reader.GetInt32(3));
                    count++;
                }
                logger.LogText(2, "BombCfg", "{0} Record sample definitions", count);

                //end
                logger.LogText(1, "BombCfg", "Reading of configuration finished");
            }
            catch (Exception ex)
            {
                logger.LogText(1, "BombCfg", "Failed to read configuration of type {0}: {1}", Name, ex.Message);
                throw;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Config access
        /// <summary>
        /// Returns DDD version object according to version name
        /// </summary>
        public BombDDDVersion GetDDDVersion(String userVersion)
        {
            BombDDDVersion version;
            if (dddVersions.TryGetValue(userVersion, out version))
                return version;
            else
                throw new Exception(String.Format("DDD Version {0} not found in TU Type {1}", userVersion, Name));
        }

        /// <summary>
        /// Returns processor parameters according to processor number
        /// </summary>
        public void GetProcessorParams(int procNum, out String evtName, out String srcName)
        {
            BombProcessor proc;
            if (processors.TryGetValue(procNum, out proc))
            {
                evtName = proc.EvtName;
                srcName = proc.SrcName;
            }
            else
                throw new Exception(String.Format("Processor with number {0} not found in TU Type {1}", procNum, Name));
        }

        /// <summary>
        /// Determines if given instance name represents instance of this type
        /// </summary>
        public bool IsTypeOfInstance(String instanceName)
        {
            return instanceName.StartsWith(InstNamePrefix);
        }
        #endregion
    }

    /// <summary>
    /// Represents one DDD version associated with Bombardier diagnostic type
    /// </summary>
    public class BombDDDVersion
    {
        #region Private ad public members
        private String UserVersion; //Name of the configuration version
        public Guid DDDVersionID; //ID of associated DDD Version

        private Dictionary<String, int> diagRecords; //Dictionary of diagnostic records configured for the version
        private Dictionary<int, ArrayList> diagSampling;
        #endregion

        #region Construction
        /// <summary>
        /// Standard constructor
        /// </summary>
        public BombDDDVersion(String userVersion, Guid dddVersionID)
        {
            UserVersion = userVersion;
            DDDVersionID = dddVersionID;
            diagRecords = new Dictionary<string, int>();
            diagSampling = new Dictionary<int, ArrayList>();
        }
        #endregion

        #region Configuration
        /// <summary>
        /// Add record definition to repository
        /// </summary>
        public void AddRecordDef(String name, int defID)
        {
            if (!diagRecords.ContainsKey(name))
                diagRecords.Add(name, defID);
        }
        public void AddRecordSamples(int recDefID, int beforeStart, int pastStart)
        {
            if (!diagSampling.ContainsKey(recDefID))
            {
                ArrayList couple = new ArrayList(2);
                couple.Add(beforeStart);
                couple.Add(pastStart);
                diagSampling.Add(recDefID, couple);
            }
        }
        #endregion

        #region Config access
        /// <summary>
        /// Get record definition ID for given record name
        /// </summary>
        public int GetRecordDefID(String recName)
        {
            int defId;
            if (diagRecords.TryGetValue(recName, out defId))
                return defId;
            else
                throw new Exception(String.Format("Definition of record {0} not found in configuration version {1}", recName, UserVersion));
        }

        /// <summary>
        /// Get record definition ID for given processor name and disturbance number
        /// </summary>
        public int GetRecordDefID(String procEvtName, int distNr)
        {
            return GetRecordDefID(procEvtName + '_' + distNr.ToString());
        }

        public ArrayList GetRecordSamples(int recDefID)
        {
            ArrayList sampleDef;
            if (diagSampling.TryGetValue(recDefID, out sampleDef))
                return sampleDef;
            else
                return null;
        }
        #endregion
    }

    /// <summary>
    /// Structure represents description of one diagnostic processor in diagnostic application
    /// </summary>
    public struct BombProcessor
    {
        public int Number;
        public String EvtName;
        public String SrcName;

        /// <summary>
        /// Standard constructor
        /// </summary>
        public BombProcessor(int number, String evtName, String srcName)
        {
            Number = number;
            EvtName = evtName;
            SrcName = srcName;
        }
    }
}
