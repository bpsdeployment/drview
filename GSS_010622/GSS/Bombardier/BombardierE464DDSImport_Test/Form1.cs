using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BombardierE464DDSImport;
using System.IO;
using System.Xml;

namespace BombardierE464DDSImport_Test
{
    public partial class Form1 : Form
    {
        private BombardierE464DDSImport.DDSImport ddsImport;

        public Form1()
        {
            InitializeComponent();
            ddsImport = new BombardierE464DDSImport.DDSImport();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ddsImport.Start();
        }

        private void btnLoadFile_Click(object sender, EventArgs e)
        {
            string current = txtFileName1.Text;
            if (!Path.IsPathRooted(current))
            {
                current = "D:\\Telediagnostica\\App_Dirs\\BombardierE464\\DDSSource\\"; //Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), current));
            }
            openFileDialog1.CheckFileExists = true;
            //openFileDialog1.Filter = "File XML|*.xml";
            openFileDialog1.InitialDirectory = Path.GetDirectoryName(current);
            openFileDialog1.FileName = Path.GetFileName(current);

            //DirectoryInfo srcDirInfo = new DirectoryInfo(BombardierE464DDSImport.Properties.Settings.Default.DDSSourceDir);
            //openFileDialog1.InitialDirectory = "D:\\Telediagnostica\\App_Dirs\\BombardierE464\\DDSSource\\";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtFileName1.Text = openFileDialog1.FileName;

                BT_FileConvert fc = new BT_FileConvert();
                fc.LoadFile(txtFileName1.Text);
                fc.GetXML();
            }
        }

        private void btnConvertDir_Click(object sender, EventArgs e)
        {
            string current = txtDirectory.Text;
            if (!Path.IsPathRooted(current))
            {
                current = "D:\\Telediagnostica\\App_Dirs\\BombardierE464\\Convert\\"; //Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), current));
            }
            folderBrowserDialog1.SelectedPath = current;

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtDirectory.Text = folderBrowserDialog1.SelectedPath;

                BT_FileConvert fc = new BT_FileConvert();
                fc.ConvertDirectory(txtDirectory.Text);
            }
        }

        private void btnMerge_Click(object sender, EventArgs e)
        {
            string current = txtDirectory.Text;
            if (!Path.IsPathRooted(current))
            {
                current = "D:\\Telediagnostica\\App_Dirs\\BombardierE464\\Convert\\"; //Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), current));
            }
            folderBrowserDialog1.SelectedPath = current;

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtDirectory.Text = folderBrowserDialog1.SelectedPath;

                DirectoryInfo DirInfo = new DirectoryInfo(txtDirectory.Text);
                DirInfo.Refresh();
                XmlDocument destDoc = new XmlDocument();
                XmlNode root = destDoc.CreateElement("ROOT");
                foreach (FileInfo srcFile in DirInfo.GetFiles("*.xml", SearchOption.TopDirectoryOnly))
                {
                    try
                    {
                        //Create XML document with source file
                        XmlDocument srcDoc = new XmlDocument();
                        srcDoc.Load(srcFile.FullName);
                        //Validate document
                        //srcDoc.Schemas.Add(null, Properties.Settings.Default.DDSSchemaFile);
                        //srcDoc.Validate(null);
                        //Check if TU Instance is registered and create new one if necessary and possible
                        String instName = srcDoc.DocumentElement.GetAttribute("VehicleID");
                        String userVersion = srcDoc.DocumentElement.GetAttribute("ConfigID");

                        XmlNode ver;

                        XmlNodeList eVersion = root.SelectNodes("/Version[@ID='" + userVersion + "']");
                        if (eVersion.Count == 0)
                        {
                            ver = destDoc.CreateElement("Version");
                            XmlAttribute avNew = destDoc.CreateAttribute("ID");
                            avNew.Value = userVersion;
                            ver.Attributes.Append(avNew);
                            root.AppendChild(ver);
                        }
                        else //if (eVersion.Count == 1)
                            ver = eVersion[0];

                        XmlNodeList ddsElemes = srcDoc.SelectNodes("//DDS");
                        foreach (XmlElement ddsElem in ddsElemes)
                        {
                            string pid = ddsElem.GetAttribute("PROCESS_NR");
                            string DIST_NR = ddsElem.GetAttribute("DIST_NR");
                            string BlocksNum = ddsElem.GetAttribute("BlocksNum");
                            string BlockLen = ddsElem.GetAttribute("BlockLen");
                            string TrigPtr = ddsElem.GetAttribute("TrigPtr");

                            XmlNode processor;
                            XmlNodeList eProc = ver.SelectNodes("//Processor[@ID='" + pid + "']");
                            if (eProc.Count == 0)
                            {
                                processor = destDoc.CreateElement("Processor");
                                XmlAttribute apNew = destDoc.CreateAttribute("ID");
                                apNew.Value = pid;
                                processor.Attributes.Append(apNew);
                                ver.AppendChild(processor);
                            }
                            else
                                processor = eProc[0];

                            XmlNodeList find = processor.SelectNodes("//Processor[@ID='" + pid + "']/DDS[@PROCESS_NR='" + pid + "' and @DIST_NR='" + DIST_NR + "' and @BlocksNum='" + BlocksNum + "' and @BlockLen='" + BlockLen + "' and @TrigPtr='" + TrigPtr + "']");
                            if (find.Count == 0)
                            {
                                XmlNode nNew = destDoc.CreateElement(ddsElem.Name);
                                XmlAttribute avNew = destDoc.CreateAttribute("Vehicle");
                                avNew.Value = instName;
                                nNew.Attributes.Append(avNew);

                                XmlAttribute avNew1 = destDoc.CreateAttribute("PROCESS_NR");
                                XmlAttribute avNew2 = destDoc.CreateAttribute("DIST_NR");
                                XmlAttribute avNew3 = destDoc.CreateAttribute("BlocksNum");
                                XmlAttribute avNew4 = destDoc.CreateAttribute("BlockLen");
                                XmlAttribute avNew5 = destDoc.CreateAttribute("TrigPtr");
                                avNew1.Value = pid;
                                avNew2.Value = DIST_NR;
                                avNew3.Value = BlocksNum;
                                avNew4.Value = BlockLen;
                                avNew5.Value = TrigPtr;
                                nNew.Attributes.Append(avNew1);
                                nNew.Attributes.Append(avNew2);
                                nNew.Attributes.Append(avNew3);
                                nNew.Attributes.Append(avNew4);
                                nNew.Attributes.Append(avNew5);
                                /*
                                                            foreach (XmlAttribute a in ddsElem.Attributes)
                                                            {
                                                                XmlAttribute aNew = destDoc.CreateAttribute(a.Name);
                                                                aNew.Value = a.Value;
                                                                nNew.Attributes.Append(aNew);
                                                            }
                                                            */
                                //nNew.InnerText = ddsElem.InnerText;

                                XmlNodeList list = processor.SelectNodes("//Processor[@ID='" + pid + "']/DDS[@PROCESS_NR='" + pid + "']");
                                if (list.Count == 0)
                                {
                                    processor.AppendChild(nNew);
                                }
                                else
                                {
                                    bool found = false;
                                    XmlElement elfound = (XmlElement)nNew;
                                    int DDS = int.Parse(DIST_NR);
                                    int ifound = int.MaxValue;
                                    foreach (XmlElement el in list)
                                    {
                                        int eDDS = int.Parse(el.GetAttribute("DIST_NR"));
                                        //if (eDDS == DDS) found = true;
                                        if (eDDS > DDS && eDDS < ifound)
                                        {
                                            ifound = eDDS;
                                            elfound = el;
                                            found = true;
                                            //continue;
                                        }
                                    }
                                    if(!found)
                                        processor.AppendChild(nNew);
                                    else
                                        processor.InsertBefore(nNew, elfound);
                                }
                            }
                        }


                        /*if (!bombConfig.InstanceExists(instName) && Properties.Settings.Default.AutRegisterUnknownTUInst)
                        {
                            logger.LogText(1, "DDSImp", "TU Instance {0} is not registered. Performing automatic registration.", instName);
                            bombConfig.CreateNewTUInstance(instName, userVersion, Properties.Settings.Default.CDDBConnString);
                            logger.LogText(1, "DDSImp", "Automatically registered TU instance {0}", instName);
                        }
                        //Obtain TUInstanceID, TUType object and DDDVersion object
                        Guid tuInstanceID = bombConfig.GetTUInstanceID(instName);
                        BombTUType tuType = bombConfig.GetTUType(instName);
                        BombDDDVersion dddVer = tuType.GetDDDVersion(userVersion);
                        //Iterate over all DDS elements in document and create corresponding ImportedDR elements
                        String procEvtName;
                        String procSrcName;
                        int recDefID;
                        int procNum;
                        DateTime startTime;
                        DateTime endTime;
                        DateTime lastModifTime;
                        XmlNodeList ddsElemes = srcDoc.SelectNodes("//DDS");*/
                        /*TEST*//*
                        Random rndGen = new Random();
                        foreach (XmlElement ddsElem in ddsElemes)
                        {
                            procNum = Convert.ToInt32(ddsElem.GetAttribute("PROCESS_NR"));
                            tuType.GetProcessorParams(procNum, out procEvtName, out procSrcName);
                            dstWriter.WriteStartElement("ImportedDR");
                            dstWriter.WriteElementString("TUInstanceID", tuInstanceID.ToString());
                            dstWriter.WriteElementString("DDDVersionID", dddVer.DDDVersionID.ToString());
                            recDefID = dddVer.GetRecordDefID(procEvtName, Convert.ToInt32(ddsElem.GetAttribute("DIST_NR")));
                            dstWriter.WriteElementString("RecordDefID", recDefID.ToString());
                            startTime = Convert.ToDateTime(ddsElem.GetAttribute("StartTime")).ToUniversalTime();
                            dstWriter.WriteElementString("RecordInstID", startTime.Ticks.ToString());
                            dstWriter.WriteElementString("StartTime", startTime.ToString("yyyy-MM-ddTHH:mm:ss.fff"));
                            if (ddsElem.GetAttribute("EndTime") != "")
                            {
                                endTime = Convert.ToDateTime(ddsElem.GetAttribute("EndTime")).ToUniversalTime();
                                dstWriter.WriteElementString("EndTime", endTime.ToString("yyyy-MM-ddTHH:mm:ss.fff"));
                            }
                            lastModifTime = Convert.ToDateTime(ddsElem.GetAttribute("LastModified")).ToUniversalTime();
                            dstWriter.WriteElementString("LastModified", lastModifTime.ToString("yyyy-MM-ddTHH:mm:ss.fff"));
                            dstWriter.WriteElementString("CompleteData", "true");
                            dstWriter.WriteElementString("BigEndianData", "false");*/
                            /*TEST*/
                            //dstWriter.WriteElementString("Source", procSrcName);
                    //        dstWriter.WriteElementString("BinaryData", ddsElem.InnerText);

                            /*TEST*/
                        //    dstWriter.WriteElementString("VehicleNumber", rndGen.Next(1, 6).ToString());
                            /*TEST*/
                         //   dstWriter.WriteElementString("DeviceCode", procNum.ToString());
                            /*TEST*/
                         //   dstWriter.WriteElementString("AcknowledgeTime", startTime.ToString("yyyy-MM-ddTHH:mm:ss.fff"));

                         //   dstWriter.WriteEndElement();
                 //       }
                        //Delete processed file
                        //srcFile.Delete();
                    }
                    catch (Exception ex)
                    {
                    }
                }
                destDoc.AppendChild(root);
                destDoc.Save(txtDirectory.Text + "\\..\\ALL.xml");
            }
        }
    }
}