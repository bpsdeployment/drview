using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.IO;
using System.Xml;
using System.Security;

namespace BombardierDDSImport
{
  partial class DDSImport : ServiceBase
  {
    #region Private members
    private BombConfig bombConfig; //Represents bombardier diagnostic configurations
    private System.Threading.Timer baseTimer; //Timer for basic periods
    private DirectoryInfo srcDirInfo;
    private DirectoryInfo convDirInfo;
    private DirectoryInfo backupDirInfo;
    private DirectoryInfo drDirInfo;
    private int currDRFileID; //Current ID (number) of DR file
    private FileStream currDRFileStream; //Current file stream for DR files
    private bool bWorkerRunning; //Worker thread is already running
    private int movedCount; //Number of files moved from source to convert directory
    #endregion

    #region Construction
    public DDSImport()
    {
      InitializeComponent();
      logger.FileName = Properties.Settings.Default.LogFileName;
      logger.FileVerbosity = Properties.Settings.Default.LogVerbosity;
      bombConfig = new BombConfig(logger);
      srcDirInfo = new DirectoryInfo(Properties.Settings.Default.DDSSourceDir);
      convDirInfo = new DirectoryInfo(Properties.Settings.Default.DDSConvertDir);
      backupDirInfo = new DirectoryInfo(Properties.Settings.Default.DDSBackDir);
      drDirInfo = new DirectoryInfo(Properties.Settings.Default.DRImportDir);
      currDRFileID = -1;
      bWorkerRunning = false;
    }
    #endregion

    #region Event handlers
    /// <summary>
    /// Handles start of service.
    /// Import timer is started
    /// </summary>
    protected override void OnStart(string[] args)
    {
      logger.LogText(0, "", "");
      logger.LogText(0, "", "----------------------------------------------");
      logger.LogText(0, "DDSImp", "Bombardier DDS Import service started");
      //Create period timer
      //First create delegate for callback method
      TimerCallback timerDelegate = new TimerCallback(baseTimer_Elapsed);
      //Create and start timer
      baseTimer = new System.Threading.Timer(
        timerDelegate, 
        null, 
        Properties.Settings.Default.StartupDelaySec * 1000, 
        Properties.Settings.Default.BasePerSec * 1000);
      logger.LogText(0, "DDSImp", "Created timer with period {0} sec, startup delay {1} sec", 
        Properties.Settings.Default.BasePerSec, Properties.Settings.Default.StartupDelaySec);
    }

    protected override void OnStop()
    {
      baseTimer.Dispose();
      logger.LogText(0, "DDSImp", "Bombardier DDS Import service stopped");
    }

    /// <summary>
    /// Raised after timer is elapsed
    /// </summary>
    /// <param name="state"></param>
    void baseTimer_Elapsed(object state)
    {
      //Check if worker thread is not already running
      if (bWorkerRunning)
      {
        logger.LogText(0, "DDSImp", "DDS Import is overloading");
        return;
      }
      //Set worker thread is running
      bWorkerRunning = true; 
      //logger.LogText(5, "DDSImp", "Timer elapsed");
      //Check if configuration is already loaded
      if (!bombConfig.IsConfigured)
      { //Load configuration
        try
        {
          logger.LogText(0, "DDSImp", "Loading configuration...");
          bombConfig.CreateConfiguration(Properties.Settings.Default.CDDBConnString, Properties.Settings.Default.ConfigFileName);
          logger.LogText(0, "DDSImp", "Configuration loaded");
        }
        catch (Exception ex)
        {
          logger.LogText(0, "DDSImp", "Failed to load configuration: {0}", ex.Message);
          //Worker thread is not running
          bWorkerRunning = false;
          return; //End period
        }
      }
      //Perform actual data transformation
      try
      {
        //Move DDS files from source directory
        MoveDDSFilesToConvertDir();
        //Transform DDS to XML using MWDConverter
        ConvertFiles();
        //Transform XML files to DR files
        TransformXMLFiles();
        //Backup transformed DDS files
        BackupDDSFiles();
        //Remove old backup directories
        RemoveOldBackupDirs();
      }
      catch (Exception ex)
      {
        //Transformation failed - log the error together with inner errors
        logger.LogText(0, "DDSImp", "DDS Transformation failed: {0}", ex.Message);
        if (ex.InnerException != null)
        {
          logger.LogText(1, "DDSImp", ex.InnerException.Message);
          if (ex.InnerException.InnerException != null)
            logger.LogText(1, "DDSImp", ex.InnerException.InnerException.Message);
        }
      }
      //Worker thread is not running
      bWorkerRunning = false;
    }
    #endregion

    #region Helper methods
    /// <summary>
    /// Moves all available files with DDS from source directory to convert directory
    /// </summary>
    private void MoveDDSFilesToConvertDir()
    {
      movedCount = 0;
      //Refresh info about source directory
      srcDirInfo.Refresh();
      //Move available files to conversion directory
      foreach (FileInfo file in srcDirInfo.GetFiles("*.*", SearchOption.AllDirectories))
      {
        try
        {
          file.MoveTo(Path.Combine(convDirInfo.FullName, file.Name));
          movedCount++;
        }
        catch (Exception) 
        {
          logger.LogText(6, "DDSImp", "Unable to move source file {0}", file.Name);
        }
      }
      if (movedCount > 0)
        logger.LogText(5, "DDSImp", "Moved {0} files from source to convert directory", movedCount);
    }

    /// <summary>
    /// Convert all files in Convert directory using MWDConverter
    /// </summary>
    private void ConvertFiles()
    {
      try
      {
        //Create ProcessStartInfo
        ProcessStartInfo mwdStartInfo = new ProcessStartInfo(Properties.Settings.Default.MWDConverter, Properties.Settings.Default.DDSConvertDir);
        mwdStartInfo.CreateNoWindow = true;
        mwdStartInfo.UseShellExecute = false;
        mwdStartInfo.RedirectStandardOutput = true;
        //Create MWDConverter process
        Process mwdProc = new Process();
        mwdProc.StartInfo = mwdStartInfo;
        //Add handler for output data of the process
        if (Properties.Settings.Default.LogMWDConverterOutput)
          mwdProc.OutputDataReceived += new DataReceivedEventHandler(MWDProcessOutputDataReceived);
        //Start MWDConverter process
        mwdProc.Start();
        //Start asynchronous reading of output data
        if (Properties.Settings.Default.LogMWDConverterOutput)
          mwdProc.BeginOutputReadLine();
        //Wait for process to exit with specified timeout
        if (!mwdProc.WaitForExit(Properties.Settings.Default.MWDConvTimeoutSec * 1000))
        { //Timeout expired, kill the process
          logger.LogText(0, "DDSImp", "MWDConverter did not finish in {0} sec. Killing the process.", Properties.Settings.Default.MWDConvTimeoutSec);
          mwdProc.Kill();
        }
        mwdProc.Dispose();
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to convert files using MWDConverter", ex);
      }
    }

    /// <summary>
    /// Handler called automatically when MWDConverter process writes data to its standard output
    /// </summary>
    /// <param name="sender">Sender of the event</param>
    /// <param name="e">Data written to the output</param>
    void MWDProcessOutputDataReceived(object sender, DataReceivedEventArgs e)
    {
      //Write data to log file (only if some files were moved to convert dir and line is not empty)
      if ((movedCount > 0) && (e.Data != null) && (e.Data.Trim() != String.Empty))
        logger.LogText(6, "MWDConverter", e.Data);
    }

    /// <summary>
    /// Moves all DDS files from convert directory to current backup directory
    /// </summary>
    private void BackupDDSFiles()
    {
      int movedCount = 0;
      String trgPath;
      //Create backup directory info
      DirectoryInfo currBackDir = new DirectoryInfo(
        Path.Combine(Properties.Settings.Default.DDSBackDir, DateTime.Now.ToString("yyyyMMdd")));
      //Create current backup directory, if it does not exist
      if (!currBackDir.Exists)
        currBackDir.Create();
      //now move all files from convert directory to backup directory
      convDirInfo.Refresh();
      foreach (FileInfo ddsFileInfo in convDirInfo.GetFiles())
      {
        try
        {
          trgPath = Path.Combine(currBackDir.FullName, ddsFileInfo.Name);
          if (File.Exists(trgPath))
          {
            logger.LogText(2, "DDSImp", "File {0} already existis in backup direcory {1}. It will be deleted.", ddsFileInfo.Name, currBackDir.Name);
            File.Delete(trgPath);
          }
          ddsFileInfo.MoveTo(trgPath);
          movedCount++;
        }
        catch (Exception ex)
        {
          logger.LogText(6, "DDSImp", "Unable to move file {0} to backup directory {1}: {2}", ddsFileInfo.Name, currBackDir.Name, ex.Message);
        }
      }
      if (movedCount > 0)
        logger.LogText(5, "DDSImp", "Moved {0} files to backup direcory {1}", movedCount, currBackDir.Name);
    }

    /// <summary>
    /// Deletes backup DDS directories older than given treshold
    /// </summary>
    private void RemoveOldBackupDirs()
    {
      int delCount = 0;
      //Get name of last kept backup directory
      String minBckDirName = DateTime.Now.AddDays(-1*Properties.Settings.Default.DDSHistoryDays).ToString("yyyyMMdd");
      //Iterate over backup directories, remove too old directories
      backupDirInfo.Refresh();
      foreach (DirectoryInfo bckDayDir in backupDirInfo.GetDirectories())
      {
        try
        {
          if (String.Compare(bckDayDir.Name, minBckDirName) < 0)
          { //directory is too old - remove
            bckDayDir.Delete(true);
            logger.LogText(6, "DDSImp", "Deleted backup directory {0}", bckDayDir.Name);
            delCount++;
          }
        }
        catch (Exception ex)
        {
          logger.LogText(6, "DDSImp", "Unable to delete backup directory {0}: {1}", bckDayDir.Name, ex.Message);
        }
      }
      if (delCount > 0)
        logger.LogText(5, "DDSImp", "Deleted {0} backup directories", delCount);
    }

    /// <summary>
    /// Creates new DR file in DR directory with new name (ID).
    /// Returns XML writer opened over this file
    /// </summary>
    private XmlWriter CreateNewDRFile()
    {
      try
      {
        //Find first free DR File ID
        int newFileID = 0;
        int tmpID;
        if (currDRFileID > 0)
          newFileID = currDRFileID + 1;
        else
        { //No file was opened yet - open new one
          drDirInfo.Refresh();
          foreach (FileInfo drFileIfo in drDirInfo.GetFiles("b*.xml", SearchOption.TopDirectoryOnly))
          { //Search all files, find highest ID
            String strID = Path.GetFileNameWithoutExtension(drFileIfo.Name).Remove(0, 1);
            if (Int32.TryParse(strID, out tmpID) && (tmpID > newFileID))
              newFileID = tmpID;
          }
          newFileID++;
        }
        //Create file name
        String drFileName = "b" + newFileID.ToString("000000") + ".xml";
        drFileName = Path.Combine(drDirInfo.FullName, drFileName);
        //Open new file stream with name
        currDRFileStream = new FileStream(drFileName, FileMode.CreateNew, FileAccess.Write, FileShare.None);
        //Open new XML writer
        XmlWriterSettings settings = new XmlWriterSettings();
        settings.Indent = true;
        settings.Encoding = Encoding.UTF8;
        settings.OmitXmlDeclaration = true;
        settings.ConformanceLevel = ConformanceLevel.Fragment;
        settings.CloseOutput = true;
        XmlWriter drXMLWriter = XmlWriter.Create(currDRFileStream, settings);
        return drXMLWriter;
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to create new DR file", ex);
      }
    }

    /// <summary>
    /// Transforms one source XML file into destination XML DR file.
    /// Successfully transformed file is removed.
    /// Unsuccessfully transformed file is moved to failed directory
    /// </summary>
    private void TransformXMLFile(FileInfo srcFile, XmlWriter dstWriter)
    {
      try
      {
        //Create XML document with source file
        XmlDocument srcDoc = new XmlDocument();
        srcDoc.Load(srcFile.FullName);
        //Validate document
        srcDoc.Schemas.Add(null, Properties.Settings.Default.DDSSchemaFile);
        srcDoc.Validate(null);
        //Check if TU Instance is registered and create new one if necessary and possible
        String instName = srcDoc.DocumentElement.GetAttribute("VehicleID");
        String userVersion = srcDoc.DocumentElement.GetAttribute("ConfigID");
        if (!bombConfig.InstanceExists(instName) && Properties.Settings.Default.AutRegisterUnknownTUInst)
        {
          logger.LogText(1, "DDSImp", "TU Instance {0} is not registered. Performing automatic registration.", instName);
          bombConfig.CreateNewTUInstance(instName, userVersion, Properties.Settings.Default.CDDBConnString);
          logger.LogText(1, "DDSImp", "Automatically registered TU instance {0}", instName);
        }
        //Obtain TUInstanceID, TUType object and DDDVersion object
        Guid tuInstanceID = bombConfig.GetTUInstanceID(instName);
        BombTUType tuType = bombConfig.GetTUType(instName);
        BombDDDVersion dddVer = tuType.GetDDDVersion(userVersion);
        //Iterate over all DDS elements in document and create corresponding ImportedDR elements
        String procEvtName;
        String procSrcName;
        int recDefID;
        int procNum;
        DateTime startTime;
        DateTime endTime;
        DateTime lastModifTime;
        XmlNodeList ddsElemes = srcDoc.SelectNodes("//DDS");
        /*TEST*/Random rndGen = new Random();
        foreach (XmlElement ddsElem in ddsElemes)
        {
          procNum = Convert.ToInt32(ddsElem.GetAttribute("PROCESS_NR"));
          tuType.GetProcessorParams(procNum, out procEvtName, out procSrcName);
          dstWriter.WriteStartElement("ImportedDR");
          dstWriter.WriteElementString("TUInstanceID", tuInstanceID.ToString());
          dstWriter.WriteElementString("DDDVersionID", dddVer.DDDVersionID.ToString());
          recDefID = dddVer.GetRecordDefID(procEvtName, Convert.ToInt32(ddsElem.GetAttribute("DIST_NR")));
          dstWriter.WriteElementString("RecordDefID", recDefID.ToString());
          startTime = Convert.ToDateTime(ddsElem.GetAttribute("StartTime")).ToUniversalTime();
          dstWriter.WriteElementString("RecordInstID", startTime.Ticks.ToString());
          dstWriter.WriteElementString("StartTime", startTime.ToString("yyyy'-'MM'-'ddTHH':'mm':'ss'.'fff"));
          if (ddsElem.GetAttribute("EndTime") != "")
          {
            endTime = Convert.ToDateTime(ddsElem.GetAttribute("EndTime")).ToUniversalTime();
            dstWriter.WriteElementString("EndTime", endTime.ToString("yyyy'-'MM'-'ddTHH':'mm':'ss'.'fff"));
          }
          lastModifTime = Convert.ToDateTime(ddsElem.GetAttribute("LastModified")).ToUniversalTime();
          dstWriter.WriteElementString("LastModified", lastModifTime.ToString("yyyy'-'MM'-'ddTHH':'mm':'ss'.'fff"));
          dstWriter.WriteElementString("CompleteData", "true");
          dstWriter.WriteElementString("BigEndianData", "false");
          /*TEST*///dstWriter.WriteElementString("Source", procSrcName);
          dstWriter.WriteElementString("BinaryData", ddsElem.InnerText);

          /*TEST*/dstWriter.WriteElementString("VehicleNumber", rndGen.Next(1,6).ToString());
          /*TEST*/dstWriter.WriteElementString("DeviceCode", procNum.ToString());
          /*TEST*/
          dstWriter.WriteElementString("AcknowledgeTime", startTime.ToString("yyyy'-'MM'-'ddTHH':'mm':'ss'.'fff"));

          dstWriter.WriteEndElement();
        }
        //Delete processed file
        srcFile.Delete();
      }
      catch (Exception ex)
      {
        //Try to move the file into failed direcory
        try
        {
          logger.LogText(1, "DDSImp", "Failed to transform file {0}: {1}", srcFile.Name, ex.Message);
          if (ex.InnerException != null)
            logger.LogText(2, "DDSImp", ex.InnerException.Message);
          logger.LogText(1, "DDSImp", "Moving the file to failed direcory");
          
          srcFile.MoveTo(Path.Combine(Properties.Settings.Default.DDSFailedDir, srcFile.Name));
        }
        catch (Exception ex2)
        {
          logger.LogText(1, "DDSImp", "Failed to move file {0} to failed direcory: {1}", srcFile.Name, ex2.Message);
        }
      }
    }

    /// <summary>
    /// Transform all XML files in Convert directory into DR files in DR files directory.
    /// Succesfully transformed files are deleted.
    /// Unsuccessfully transformed files are moved to failed directory.
    /// </summary>
    private void TransformXMLFiles()
    {
      try
      {
        int transfCount = 0;
        //Get output XmlWriter
        XmlWriter dstWriter = null;
        //Iterate over all XML files in Convert direcory
        convDirInfo.Refresh();
        foreach (FileInfo srcXmlFile in convDirInfo.GetFiles("*.xml", SearchOption.TopDirectoryOnly))
        {
          if (dstWriter == null)
            dstWriter = CreateNewDRFile(); //Create new writer if necessary
          TransformXMLFile(srcXmlFile, dstWriter); //Transform the file
          logger.LogText(6, "DDSImp", "Transformed src XML file {0}", srcXmlFile.Name);
          transfCount++;
          dstWriter.Flush();
          if (currDRFileStream.Length > (Properties.Settings.Default.MaxDRFileLenkB * 1024))
          {
            //Close writer
            dstWriter.Close();
            //Close and dispose file stream
            currDRFileStream.Close();
            currDRFileStream.Dispose();
            currDRFileStream = null;
            dstWriter = null;
          }
        }
        if (transfCount > 0)
          logger.LogText(5, "DDSImp", "Transformed {0} src XML files to DR file", transfCount);
        //Close writer if opened
        if (dstWriter != null)
        {
          dstWriter.Close();
          dstWriter = null;
        }
        //Close and dispose output file stream if opened
        if (currDRFileStream != null)
        {
          currDRFileStream.Close();
          currDRFileStream.Dispose();
          currDRFileStream = null;
        }
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to transform source XML files", ex);
      }
    }
    #endregion
  }
}
