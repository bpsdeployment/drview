namespace BombardierDDSImport
{
  partial class ProjectInstaller
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.DDSImportProcess = new System.ServiceProcess.ServiceProcessInstaller();
      this.DDSImport = new System.ServiceProcess.ServiceInstaller();
      // 
      // DDSImportProcess
      // 
      this.DDSImportProcess.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
      this.DDSImportProcess.Password = null;
      this.DDSImportProcess.Username = null;
      // 
      // DDSImport
      // 
      this.DDSImport.Description = "Converts Bombardier DDS files into DR Import files";
      this.DDSImport.DisplayName = "DDS Import";
      this.DDSImport.ServiceName = "TDDDSImport";
      // 
      // ProjectInstaller
      // 
      this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.DDSImportProcess,
            this.DDSImport});

    }

    #endregion

    private System.ServiceProcess.ServiceProcessInstaller DDSImportProcess;
    private System.ServiceProcess.ServiceInstaller DDSImport;
  }
}