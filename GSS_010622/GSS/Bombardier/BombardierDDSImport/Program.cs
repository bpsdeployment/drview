using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;

namespace BombardierDDSImport
{
  static class Program
  {
    static void Main()
    {
      ServiceBase[] ServicesToRun;

      // More than one user Service may run within the same process. To add
      // another service to this process, change the following line to
      // create a second service object. For example,
      //
      //   ServicesToRun = new ServiceBase[] {new Service1(), new MySecondUserService()};
      //
      ServicesToRun = new ServiceBase[] { new DDSImport() };

      ServiceBase.Run(ServicesToRun);
    }
  }
}
