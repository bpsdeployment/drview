using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using DDDObjects;
using TDManagementForms;
using TasksDataset;
using TasksDataset.TasksTableAdapters;

namespace DRMailer
{
  /// <summary>
  /// Class maintains dataset with defined tasks and last task results.
  /// On request, all scheduled tasks are executed and results are mailed.
  /// </summary>
  class Scheduler
  {
    #region Private members
    DDDHelper dddHelper; //DDD Helper object
    QueryExecuter queryExec; //Executes task queries
    Tasks tasks; //Dataset with task definitions
    Mailer mailer; //Mailer of task results
    Logger logger; //Logger object
    #endregion //Private members

    #region Construction initialization
    /// <summary>
    /// Standard constructor
    /// </summary>
    /// <param name="logger">Logger object - used to write log messages</param>
    public Scheduler(Logger logger)
    {
      //Store logger objects
      this.logger = logger;
      //Create datasets
      tasks = new Tasks();
      //Create DDD helper
      dddHelper = new DDDHelper();
      dddHelper.ConnectionString = Properties.Settings.Default.CDDBConnectionString;
      //Load saved DDD objects
      String dddDir;
      if (Properties.Settings.Default.runtimeDDDObjDir == "")
        dddDir = Environment.CurrentDirectory;
      else
        dddDir = Properties.Settings.Default.runtimeDDDObjDir;
      dddHelper.LoadDDDVersionsFromDir(dddDir);
      //Load TU objects from DB
      dddHelper.LoadTUObjectsFromDB();
      //Create query executer
      queryExec = new QueryExecuter(dddHelper);
      //Create result mailer
      mailer = new Mailer();
    }
    #endregion //Construction initialization

    #region Public methods
    /// <summary>
    /// Loads task definitions and last task results from XML files.
    /// Executes queries for scheduled tasks.
    /// Mails query results to recipients.
    /// </summary>
    public void ExecuteTasks()
    {
      DateTime from;
      DateTime to;
      DateTime runTime = DateTime.MinValue;
      DateTime actTime;
      String fileName = "";
      int recCount;
      bool recOverload;

      try
      {
        //Load datasets
        logger.LogText("Loading task definitions and last results.");
        LoadDatasets();
        //Determine actual time
        actTime = DateTime.UtcNow;
        logger.LogText("Starting task scheduling for actual time {0:s}", actTime);
        //Iterate over all tasks
        foreach (Tasks.TaskDefRow task in tasks.TaskDef)
        {
          //Check if task is scheduled
          if (ScheduleTask(actTime, task, out runTime, out from, out to))
          { //Task is scheduled
            //Create new row in task log
            Tasks.TaskLogRow logRow = tasks.TaskLog.NewTaskLogRow();
            logRow.TaskName = task.TaskName;
            logRow.ExecTime = actTime;
            logRow.StartTime = from;
            logRow.EndTime = to;
            logRow.Recipients = task.Recipients;
            fileName = "";
            try
            {
              //Create query file name
              fileName = CreateFileName(task, to);
              //Execute query for task
              logger.LogText("Executing query for task {0} from {1:s} to {2:s}. Result file {3}", task.TaskName, from, to, fileName);
              recCount = queryExec.ExecuteQuery(task, from, to, Properties.Settings.Default.tmpResDir + fileName, out recOverload);
              logger.LogText("Query returned {0} records", recCount);
              logRow.RecCount = recCount;
              logRow.RecOverload = recOverload;
              //Send task results
              if ((recCount == 0) && !task.SendEmptyFile)
              { //Do not send empty results
                logger.LogText("Empty task result not sent.");
              }
              else
              { //Send results
                mailer.SendTaskResult(task, fileName, dddHelper.UTCToUserTime(from), dddHelper.UTCToUserTime(to), recOverload);
                logger.LogText("Query result sent to: {0}", task.Recipients);
              }
              //Update last run time
              task.LastSuccRun = runTime;
              task.LastFailed = false;
              logRow.Succeeded = true;
            }
            catch (Exception ex)
            {
              logger.LogText("Processing of task {0} failed: {1}", task.TaskName, ex.Message);
              //Store task error result
              task.LastFailed = true;
              logRow.Succeeded = false;
              logRow.ErrorMessage = ex.Message;
              if (ex.InnerException != null)
                logRow.ErrorMessage += "; " + ex.InnerException.Message;
            }
            finally
            {
              //Delete file with query result if exists
              if ((fileName != "") && File.Exists(Properties.Settings.Default.tmpResDir + fileName))
              {
                try { File.Delete(Properties.Settings.Default.tmpResDir + fileName); }
                catch (Exception) { }
              }
            }
            //Add log row to log table
            tasks.TaskLog.AddTaskLogRow(logRow);
          }
        }
        //Save task results
        SaveTaskResults();
        //Prune task log
        PruneTaskLog(actTime);
        //All task finished
        logger.LogText("All scheduled tasks finished");
      }
      catch (Exception ex)
      {
        logger.LogText("Fatal error occured: {0}", ex.Message);
      }
    }

    /// <summary>
    /// Saves loaded runtime DDD objects from DDD helper to binary files.
    /// </summary>
    public void SaveRuntimeDDDObjects()
    {
      //Check directory name
      String dddDir;
      if (Properties.Settings.Default.runtimeDDDObjDir == "")
        dddDir = Environment.CurrentDirectory;
      else
        dddDir = Properties.Settings.Default.runtimeDDDObjDir;
      //Store objects to files
      dddHelper.SaveAllDDDVersions(dddDir, DDDVersionOrigin.DB);
      dddHelper.SaveTUObjectsToFile(dddDir);
    }
    #endregion //Public methods

    #region Helper methods
    /// <summary>
    /// Loads task definition from database
    /// </summary>
    private void LoadDatasets()
    {
      //Clear datasets
      tasks.Clear();
      //Load task definitions from database
      TaskDefTableAdapter taskDefAdapter = new TaskDefTableAdapter();
      taskDefAdapter.Fill(tasks.TaskDef);
      taskDefAdapter.Dispose();
    }

    /// <summary>
    /// Saves task results into database
    /// </summary>
    private void SaveTaskResults()
    {
      //Update task definitions
      TaskDefTableAdapter taskDefAdapter = new TaskDefTableAdapter();
      taskDefAdapter.Update(tasks.TaskDef);
      taskDefAdapter.Dispose();
      //Store task execution log
      TaskLogTableAdapter taskLogAdapter = new TaskLogTableAdapter();
      String errMessage;
      int? recCount;
      bool? recOverload;
      foreach (Tasks.TaskLogRow logRow in tasks.TaskLog)
      {
        if (logRow.IsErrorMessageNull())
          errMessage = null;
        else
          errMessage = logRow.ErrorMessage;
        if (logRow.IsRecCountNull())
          recCount = null;
        else
          recCount = logRow.RecCount;
        if (logRow.IsRecOverloadNull())
          recOverload = null;
        else
          recOverload = logRow.RecOverload;
        taskLogAdapter.Insert(logRow.TaskName, logRow.ExecTime, logRow.StartTime, logRow.EndTime, logRow.Recipients, 
                              recCount, recOverload, logRow.Succeeded, errMessage);
      }
      taskLogAdapter.Dispose();
    }

    /// <summary>
    /// Decides if given task is scheduled for execution and 
    /// generates time interval for task execution.
    /// </summary>
    /// <param name="actTime">Actual time</param>
    /// <param name="taskDef">Task definition structure</param>
    /// <param name="lastTaskRes">Result of last task execution</param>
    /// <param name="runTime">Generated time of execution</param>
    /// <param name="from">Generated start of interval</param>
    /// <param name="to">Generated end of interval</param>
    /// <returns>True if task is scheduled for execution</returns>
    private bool ScheduleTask(DateTime actTime, Tasks.TaskDefRow taskDef, 
                              out DateTime runTime, out DateTime from, out DateTime to)
    {
      //For first task run set time of last successfull run
      if (taskDef.IsLastSuccRunNull())
        taskDef.LastSuccRun = taskDef.StartTime.AddHours(-1 * taskDef.Period);
      //Decide if task is scheduled
      if ((taskDef.StartTime < actTime) &&
          (taskDef.LastSuccRun.AddHours(taskDef.Period) <= actTime) &&
          taskDef.Enabled)
      { //Task is scheduled, compute query interval
        DateTime minAllowedTime;
        //Compute minimum allowed time for tasl exectution
        minAllowedTime = new DateTime(Math.Max(taskDef.LastSuccRun.AddHours(taskDef.Period).Ticks, actTime.AddHours(-1 * taskDef.MaxDelay).Ticks));
        //Round up to multiple of periods since task StartTime to get runTime
        TimeSpan sinceStart = minAllowedTime-taskDef.StartTime;
        runTime = minAllowedTime.AddHours(Convert.ToInt64(sinceStart.TotalHours) % taskDef.Period);
        //Compute interval boundaries
        from = runTime.AddHours(-1 * (taskDef.Delay + taskDef.Interval));
        to = runTime.AddHours(-1 * taskDef.Delay);
        //Check if recipients exist for given task
        if (taskDef.Recipients == "")
        { //Task shall be scheduled, bud no recipients exist - no point to execute task
          logger.LogText("Task {0} is scheduled, but no recipients exist. Task is not executed.", taskDef.TaskName);
          return false;
        }
        //Return true, because task is scheduled
        return true;
      }
      //Task is not scheduled
      runTime = DateTime.MinValue;
      from = DateTime.MinValue;
      to = DateTime.MinValue;
      return false;
    }

    /// <summary>
    /// Create name of file for query result based on format string and query time
    /// </summary>
    /// <param name="taskDef"></param>
    /// <param name="time"></param>
    /// <returns></returns>
    private String CreateFileName(Tasks.TaskDefRow taskDef, DateTime time)
    {
      //Load format string
      String fileName = taskDef.FileNameFormat;
      //Convert time to local time
      time = dddHelper.UTCToUserTime(time);
      //Replace format strings
      fileName = fileName.Replace("%yy", time.ToString("yy"));
      fileName = fileName.Replace("%mm", time.ToString("MM"));
      fileName = fileName.Replace("%dd", time.ToString("dd"));
      fileName = fileName.Replace("%hh", time.ToString("HH"));
      //return file name
      return fileName;
    }

    /// <summary>
    /// Deletes from task log table all records older than specified treshold
    /// </summary>
    /// <param name="actTime">Actual time from which treshold is calculated</param>
    private void PruneTaskLog(DateTime actTime)
    {
      //Compute treshold
      DateTime treshold = actTime.AddDays(-1*Properties.Settings.Default.taskLogHistoryDays);
      //create table adapter
      TaskLogTableAdapter logAdapter = new TaskLogTableAdapter();
      try
      {
        logAdapter.PruneTaskLog(treshold);
        logger.LogText("Deleted task history records older than {0:s} from task history table", treshold);
      }
      catch (Exception ex)
      {
        logger.LogText("Failed to delete task history records: {0}", ex.Message);
      }
      finally
      {
        logAdapter.Dispose();
      }

    }
    #endregion //Helper methods
  }
}
