using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using DDDObjects;
using TasksDataset;

namespace DRMailer
{
  /// <summary>
  /// Class executes query specified by one task and creates file with query results.
  /// </summary>
  class QueryExecuter
  {
    #region Private members
    SqlConnection dbConnection; //Connection to database
    SqlCommand queryCommand; //Command used to run database query
    DDDHelper dddHelper;
    #endregion //Private members

    #region Construction, initialization
    /// <summary>
    /// Creates new instance of QueryExecuter. 
    /// Initializes connection to database from application properties. 
    /// Creates and prepares database query command.
    /// </summary>
    /// <param name="dddHelper">DDD helper object</param>
    public QueryExecuter(DDDHelper dddHelper)
    {
      //Create connection to database from stored connection string
      dbConnection = new SqlConnection(Properties.Settings.Default.CDDBConnectionString);
      //Create query command
      queryCommand = new SqlCommand(Properties.Settings.Default.queryProcName, dbConnection);
      queryCommand.CommandTimeout = Properties.Settings.Default.queryTimeout;
      queryCommand.CommandType = CommandType.StoredProcedure;
      //Add query parameters
      queryCommand.Parameters.Add("@TUInstances", SqlDbType.NVarChar, 4000);
      queryCommand.Parameters.Add("@From", SqlDbType.DateTime);
      queryCommand.Parameters.Add("@To", SqlDbType.DateTime);
      queryCommand.Parameters.Add("@RecTypeMask", SqlDbType.Int);
      queryCommand.Parameters.Add("@Sources", SqlDbType.NVarChar, 255);
      queryCommand.Parameters.Add("@FaultCodes", SqlDbType.NVarChar, 255);
      queryCommand.Parameters.Add("@TitleKeywords", SqlDbType.NVarChar, 255);
      queryCommand.Parameters.Add("@MatchAllKeywords", SqlDbType.Bit);
      queryCommand.Parameters.Add("@MaxRows", SqlDbType.Int);
      //Store DDD helper object
      this.dddHelper = dddHelper;
    }
    #endregion //Construction, initialization

    #region Public methods
    /// <summary>
    /// Executes database query with parameters specified by Task row from dataset of tasks.
    /// Result of the query is saved by DRSerializer to specified file.
    /// </summary>
    /// <param name="task">Row from tasks dataset with query parameters</param>
    /// <param name="from">Start of the query time range</param>
    /// <param name="to">End of the query time range</param>
    /// <param name="outFileName">Name of output file</param>
    /// <param name="recOverload">Maximum number of records or max file length exceeded</param>
    /// <returns>Returns number of records serialized to file</returns>
    public int ExecuteQuery(Tasks.TaskDefRow task, DateTime from, DateTime to, String outFileName, out bool recOverload)
    {
      SqlDataReader dataReader = null;
      FileStream outStream = null;
      DRSerializer recSerializer = null;
      int recCount = 0;
      recOverload = false;
      try
      {
        //Ensure DB connection is open
        if (dbConnection.State != ConnectionState.Open)
        {
          dbConnection.Close();
          dbConnection.Open();
        }
        //Set query parameters
        SetQueryParameters(task, from, to);
        //Execute query and obtain reader object
        dataReader = queryCommand.ExecuteReader();
        //Open output file
        outStream = new FileStream(outFileName, FileMode.Create, FileAccess.Write);
        //Create record serializer
        recSerializer = new DRSerializer(dddHelper, outStream, true);
        //Serialize records to stream
        while (recSerializer.WriteDBRecordToStream(dataReader))
        {
          recCount++;
          //Check maximum record count
          if (!task.IsMaxRecordsNull() && (recCount >= task.MaxRecords))
          {
            recOverload = true;
            break;
          }
          //Flush stream for each few records
          if ((recCount % Properties.Settings.Default.recFlushBatch) == 0)
          {
            outStream.Flush();
            //Check for maximum size of output file
            if (!task.IsMaxFileLengthBytesNull()
                && (outStream.Length > (task.MaxFileLengthBytes * Properties.Settings.Default.fileSizeTrh)))
            {
              recOverload = true;
              break;
            }
          }
        }
        //Store used DDD objects to file
        recSerializer.WriteUsedDDDObjects();
      }
      catch (Exception ex)
      {
        throw ex;
      }
      finally
      {
        //Close and dispose IO objects
        if (recSerializer != null)
          recSerializer.CloseZipStream();
        if (outStream != null)
        {
          outStream.Close();
          outStream.Dispose();
        }
        if (dataReader != null)
        {
          dataReader.Close();
          dataReader.Dispose();
        }
      }
      //Return number of serialized records
      return recCount;
    }
    #endregion //Public methods

    #region Private members
    /// <summary>
    /// Sets parameters of internal query object
    /// </summary>
    /// <param name="task">Row from tasks dataset with query parameters</param>
    /// <param name="from">Start of the query time range</param>
    /// <param name="to">End of the query time range</param>
    private void SetQueryParameters(Tasks.TaskDefRow task, DateTime from, DateTime to)
    {
      //Set up query parameters
      queryCommand.Parameters["@TUInstances"].Value = task.TUInstances;
      queryCommand.Parameters["@From"].Value = from;
      queryCommand.Parameters["@To"].Value = to;
      if (task.IsRecTypeMaskNull())
        queryCommand.Parameters["@RecTypeMask"].Value = DBNull.Value;
      else
        queryCommand.Parameters["@RecTypeMask"].Value = task.RecTypeMask;
      if (task.IsSourcesNull())
        queryCommand.Parameters["@Sources"].Value = DBNull.Value;
      else
        queryCommand.Parameters["@Sources"].Value = task.Sources;
      if (task.IsFaultCodesNull())
        queryCommand.Parameters["@FaultCodes"].Value = DBNull.Value;
      else
        queryCommand.Parameters["@FaultCodes"].Value = task.FaultCodes;
      if (task.IsTitleKeywordsNull())
        queryCommand.Parameters["@TitleKeywords"].Value = DBNull.Value;
      else
        queryCommand.Parameters["@TitleKeywords"].Value = task.TitleKeywords;
      if (task.IsMatchAllKeywordsNull())
        queryCommand.Parameters["@MatchAllKeywords"].Value = DBNull.Value;
      else
        queryCommand.Parameters["@MatchAllKeywords"].Value = task.MatchAllKeywords;
      if (task.IsMaxRecordsNull())
        queryCommand.Parameters["@MaxRows"].Value = DBNull.Value;
      else
        queryCommand.Parameters["@MaxRows"].Value = task.MaxRecords;
    }

    #endregion //Private members
  }
}
