using System;
using System.Collections.Generic;
using System.Text;
using TDManagementForms;
using TasksDataset;

namespace DRMailer
{
  /// <summary>
  /// Class encapsulates DRMailer program.
  /// Executes task scheduler when requested.
  /// </summary>
  class Program
  {
    #region Main method
    static void Main(string[] args)
    {
      try
      {
        //Create program object
        Program prog = new Program();
        //Run scheduler
        prog.RunScheduler();
      }
      catch (Exception ex)
      {
        Console.WriteLine("Fatal error occured: " + ex.Message);
        return;
      }
    }
    #endregion //Main method

    #region Private members
    Logger logger; //Logger object
    Scheduler scheduler; //Task scheduler
    #endregion //Private members

    #region Construction, initialization
    /// <summary>
    /// Standard constructor
    /// </summary>
    public Program()
    {
      //Create logger object
      logger = new Logger();
      //Set logger parameters
      logger.FileName = Properties.Settings.Default.logFileName;
      logger.FileVerbosity = 5;
      logger.LogToFile = true;
      logger.MaxFileLength = Properties.Settings.Default.logFileMaxLength;
      logger.MaxHistoryFiles = Properties.Settings.Default.logFileHistory;
      //log startup
      logger.LogText("");
      logger.LogText("*******************************************");
      logger.LogText("DRMailer application started");
      //Create scheduler
      try
      {
        scheduler = new Scheduler(logger);
      }
      catch (Exception ex)
      {
        logger.LogText("Failed to create task scheduler: " + ex.Message);
        throw ex;
      }
      logger.LogText("Created task scheduler");
      logger.LogText("");
    }
    #endregion //Construction, initialization

    #region Public methods
    /// <summary>
    /// Run task scheduler to schedule all tasks
    /// </summary>
    public void RunScheduler()
    {
      //Schedule tasks
      logger.LogText("Starting task scheduler");
      try
      {
        scheduler.ExecuteTasks();
        //Save runtime objects
        scheduler.SaveRuntimeDDDObjects();
      }
      catch (Exception ex)
      {
        logger.LogText("Fatal error in task scheduler: " + ex.Message);
      }
      logger.LogText("Task scheduler finished");
      logger.LogText("");
    }
    #endregion //Public methods
  }
}
