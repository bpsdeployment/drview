﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("DRMailer")]
[assembly: AssemblyDescription("Mailer application for diagnostic records")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("UniControls")]
[assembly: AssemblyProduct("DRMailer")]
[assembly: AssemblyCopyright("Copyright © UniControls 2006")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("704a898c-8402-4fb1-9113-9c164e5d0c8f")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.4.*")]
[assembly: AssemblyFileVersion("1.4.0.0")]
