using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace AlstomDDSImport
{
  /// <summary>
  /// Class stores unprocessed DDS data for each train.
  /// Data are stored in files in specified directory 
  /// in files - one file per train (TUInstance).
  /// </summary>
  public class AlstomDDSStorage
  {
    #region Private members
    private String directoryName; //Name of the directory where files are stored
    private String filesExtension; //Extension of the files
    private BinaryFormatter formatter; //Formatter used to serialize and deserialize objects
    private Dictionary<String, List<AlstomDDSRecord>> ddsLists; //Dictionary of stored lists keyed by TUInstance name
    #endregion

    #region Construction
    /// <summary>
    /// Standard constructor
    /// </summary>
    public AlstomDDSStorage(String dirName, String filesExt)
    {
      this.directoryName = dirName;
      this.filesExtension = filesExt;
      ddsLists = new Dictionary<string, List<AlstomDDSRecord>>();
      formatter = new BinaryFormatter();
    }
    #endregion

    #region Public methods
    /// <summary>
    /// Loads all stored files with unprocessed DDS records
    /// </summary>
    public void LoadStoredDDS()
    {
      //Clear dictionary
      ddsLists.Clear();
      //Create directory info
      DirectoryInfo dirInfo = new DirectoryInfo(directoryName);
      //Load all files
      foreach (FileInfo fileInfo in dirInfo.GetFiles("*." + filesExtension, SearchOption.AllDirectories))
      { //Iterate over the files in the directory
        using (FileStream file = fileInfo.OpenRead())
        { //Open the file
          //Read stored objects
          String tuInstName = (String)formatter.Deserialize(file);
          List<AlstomDDSRecord> records = (List<AlstomDDSRecord>)formatter.Deserialize(file);
          //Add object to the dictionary
          ddsLists.Add(tuInstName, records);
        }
      }
    }
    /// <summary>
    /// Retrieves records stored for given TUInstance.
    /// Or null if no records are stored.
    /// </summary>
    /// <returns></returns>
    public List<AlstomDDSRecord> GetStoredDDS(AlstomTUInstnace tuInstance)
    {
      if (ddsLists.ContainsKey(tuInstance.TUInstanceName))
        return ddsLists[tuInstance.TUInstanceName];
      else
        return null;
    }
    /// <summary>
    /// Store given list of DDS records for given TUInstance.
    /// Records are stored both to internal dictionary and to the file.
    /// </summary>
    public void StoreDDS(List<AlstomDDSRecord> ddsRecords, AlstomTUInstnace tuInstance)
    {
      //First store records in the dictionary
      if (ddsLists.ContainsKey(tuInstance.TUInstanceName))
        ddsLists[tuInstance.TUInstanceName] = ddsRecords;
      else
        ddsLists.Add(tuInstance.TUInstanceName, ddsRecords);
      //Now store the list into the file
      String fileName = Path.Combine(directoryName, tuInstance.TUInstanceName + "." + filesExtension);
      using (FileStream file = new FileStream(fileName, FileMode.Create, FileAccess.Write))
      {
        formatter.Serialize(file, tuInstance.TUInstanceName);
        formatter.Serialize(file, ddsRecords);
      }
    }
    /// <summary>
    /// Return number of TUInstances for which DDS are stored
    /// </summary>
    public int GetTUInstCount()
    {
      return ddsLists.Count;
    }
    #endregion
  }
}
