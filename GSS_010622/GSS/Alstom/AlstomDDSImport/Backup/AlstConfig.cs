using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Collections;
using System.Globalization;
using System.Diagnostics;
using System.ServiceProcess;
using System.Xml;
using System.Security;
using TDManagementForms;
using AlstomDDSImport;



namespace AlstomDDSImport
{
  public class AlstConfig
  {
    SqlConnection sqlConnection = new SqlConnection();
    private Logger logger;
    private bool isConfigured;
    //AlstomCfg alstomCfg;

    /// <summary>
    /// Correct configuration 
    /// </summary>
    public bool IsConfigured
    {
      get { return isConfigured; }
    }

    public AlstConfig(Logger logger)
    {
      this.logger = logger;
      isConfigured = false;
      string connectionString = Properties.Settings.Default.CDDBConnString;
      sqlConnection.ConnectionString = connectionString;
    }

    public int SqlIni()
    {
      StringBuilder errorMessage = new StringBuilder();
      errorMessage.Append("Sql connection error");
      //  database connection
      try
      {
        if (sqlConnection.State != ConnectionState.Open)
        {
          sqlConnection.Close();
          sqlConnection.Open();
        }
      }
      catch (InvalidOperationException)
      {
        logger.LogText(1, "AlstCfg", errorMessage.ToString());
        return -1;

      }
      catch (SqlException Sqlex)
      {
        for (int i = 0; i < Sqlex.Errors.Count; i++)
        {
          errorMessage.Append("\n" + Sqlex.Errors[i].Message);
        }
        logger.LogText(1, "AlstCfg", errorMessage.ToString());
        return -1;
      }
      return 0;
    }

    /// <summary>
    /// Loads Alstom configuration from CDDB
    /// </summary>
    /// <returns></returns>
    public AlstomCfg LoadAlstConfig()
    {
      int result = SqlIni();
      logger.LogText(1, "AlstCfg", "Load configuration");
      AlstomCfg alstomCfg = new AlstomCfg();
      #region InputFile_creation
      //create input XML Document;
      XmlDocument inpXmlDoc = new XmlDocument();
      string configFileName = Properties.Settings.Default.ConfigFileName;
      try
      {
        inpXmlDoc.Load(configFileName);
      }
      catch (Exception e)
      {
        logger.LogText(1, "AlstCfg", "Error in configuration file {0}" + e.Message, configFileName);
      }
      #endregion //InputFile_creation

      #region AlstomCFG objects
      XmlNodeList elemList;
      XmlNodeList childElemList;
      alstomCfg.TUTypes = new Dictionary<string, AlstomTUType>();
      //Alstom TUTypes
      string alstTUTypeName; //  Alstom name of TUType
      string TUTypeName;    //   name of TUType from config. file
      string DDSRecordLength;//  environmet data lengt

      elemList = inpXmlDoc.SelectNodes("//AlstomTUTypes/TUType");
      foreach (XmlElement elem in elemList)
      {//throught TUTypes

        alstTUTypeName = elem.GetAttribute("AlstNumber");
        alstTUTypeName = alstTUTypeName.Trim();
        alstTUTypeName = alstTUTypeName.PadLeft(2, '0');
        TUTypeName = elem.GetAttribute("Name");
        //##DDSRecordLength = elem.GetAttribute("DDSRecordLength");
        //AlstomTUType
        AlstomTUType alstomTUType = new AlstomTUType(); // create AlstomTUType object for current TUType 
        alstomTUType.DDDVersions = new Dictionary<string, AlstomDDDVersion>();//dictionary of DDD versions
        alstomTUType.TUInstances = new Dictionary<int, AlstomTUInstnace>();  //dictionary of known instances for given TU type

        alstomTUType.TUTypeName = TUTypeName;
        alstomTUType.AlstomTypeName = alstTUTypeName;
      //##  alstomTUType.DDSRecordLength = Convert.ToInt32(DDSRecordLength);

        #region TUType DDDVersions
        childElemList = elem.ChildNodes;
        foreach (/*XmlNode */XmlElement childElem in childElemList)
        {//throught DDDVersions 
          if (childElem.LocalName == "DDDVer")
          {//DDDVer
            // configuration  from Alstom TUTypes file
            string alstDDDVerName = childElem.GetAttribute("AlstNumber");
            alstDDDVerName = alstDDDVerName.Trim();
            alstDDDVerName = alstDDDVerName.PadLeft(2, '0');

            string userVersion = childElem.GetAttribute("Name");

            /* version ID from DB*/
            SqlCommand versionIDSqlCommand = new SqlCommand();
            versionIDSqlCommand.Connection = sqlConnection;
            versionIDSqlCommand.CommandTimeout = Properties.Settings.Default.SQLTimeout;
            versionIDSqlCommand.CommandType = CommandType.Text;

            SqlParameter inpVersionIDSQLParameter = new SqlParameter();
            inpVersionIDSQLParameter.ParameterName = "@UserVersion";
            inpVersionIDSQLParameter.IsNullable = false;
            inpVersionIDSQLParameter.SqlDbType = SqlDbType.VarChar;
            inpVersionIDSQLParameter.Direction = ParameterDirection.Input;
            inpVersionIDSQLParameter.Size = 20;
            inpVersionIDSQLParameter.Value = userVersion;
            versionIDSqlCommand.Parameters.Add(inpVersionIDSQLParameter);

            versionIDSqlCommand.CommandText = " select VersionID from DDDVersions where UserVersion = @UserVersion";
            bool dDDVerfound = true;
            Guid DDDVersionID = new Guid();
            try
            {
              DDDVersionID = (Guid)versionIDSqlCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
              logger.LogText(1, "AlstCfg", "Error-DDDVersionID wasn't still found in the DB, " + ex.Message);
              dDDVerfound = false;
            }
            if (dDDVerfound)
            {// DDDVersion is registred in the DB
              AlstomDDDVersion alstomDDDVersion = new AlstomDDDVersion();
              alstomDDDVersion.DeviceNames = new Dictionary<int, string>();
              alstomDDDVersion.RecordDefIDs = new Dictionary<int, int>();

              alstomDDDVersion.UserVersion = userVersion;
              alstomDDDVersion.AlstomVersionName = alstDDDVerName;
              alstomDDDVersion.SamplesAfterStart = Convert.ToInt32(childElem.GetAttribute("SamplesAfterStart"));
              alstomDDDVersion.SamplesBeforeStart = Convert.ToInt32(childElem.GetAttribute("SamplesBeforeStart"));
              alstomDDDVersion.SamplingPeriod = Convert.ToInt32(childElem.GetAttribute("SamplingPeriod"));
              DDSRecordLength = childElem.GetAttribute("DDSRecordLength");
              alstomDDDVersion.DDSRecordLength = Convert.ToInt32(DDSRecordLength);
              alstomDDDVersion.EnvDataSampleLen = Convert.ToInt32(childElem.GetAttribute("EnvDataSampleLen"));
              alstomDDDVersion.DDDVersionID = DDDVersionID;

              /* dictionary of devices*/
              SqlCommand devicesSqlCommand = new SqlCommand();
              devicesSqlCommand.Connection = sqlConnection;
              devicesSqlCommand.CommandTimeout = Properties.Settings.Default.SQLTimeout;
              devicesSqlCommand.CommandType = CommandType.Text;

              SqlParameter inpDDDVersionIDSQLParameter = new SqlParameter();
              inpDDDVersionIDSQLParameter.ParameterName = "@DDDVersionID";
              inpDDDVersionIDSQLParameter.IsNullable = false;
              inpDDDVersionIDSQLParameter.SqlDbType = SqlDbType.UniqueIdentifier;
              inpDDDVersionIDSQLParameter.Direction = ParameterDirection.Input;
              inpDDDVersionIDSQLParameter.Value = DDDVersionID;
              devicesSqlCommand.Parameters.Add(inpDDDVersionIDSQLParameter);

              devicesSqlCommand.CommandText = "select Code, [Name] from dbo.DeviceCodes where VersionID= @DDDVersionID";
              SqlDataReader reader = null;
              reader = devicesSqlCommand.ExecuteReader();
              int count = 0;
              int deviceCode;
              string deviceName;
              while (reader.Read())
              {
                //Add devices to dictionary
                deviceCode = reader.GetInt16(0);
                deviceName = reader.GetString(1);
                alstomDDDVersion.DeviceNames.Add(deviceCode, deviceName);
                count++;
              }
              logger.LogText(2, "AlstCfg", "TUType name: {0}, DDDVersion: {1} - {2} Devices", TUTypeName, userVersion, count);
              reader.Close();

              /* dictionary of RecordDefIDs*/
              SqlCommand recordDefIDsSqlCommand = new SqlCommand();
              recordDefIDsSqlCommand.Connection = sqlConnection;
              recordDefIDsSqlCommand.CommandTimeout = Properties.Settings.Default.SQLTimeout;
              recordDefIDsSqlCommand.CommandType = CommandType.Text;

              SqlParameter inpDDDVersionIDRDSQLParameter = new SqlParameter();
              inpDDDVersionIDRDSQLParameter.ParameterName = "@DDDVersionID";
              inpDDDVersionIDRDSQLParameter.IsNullable = false;
              inpDDDVersionIDRDSQLParameter.SqlDbType = SqlDbType.UniqueIdentifier;
              inpDDDVersionIDRDSQLParameter.Direction = ParameterDirection.Input;
              inpDDDVersionIDRDSQLParameter.Value = DDDVersionID;
              recordDefIDsSqlCommand.Parameters.Add(inpDDDVersionIDRDSQLParameter);

              /*        string commandText = " select FaultCode, RecordDefID from RecordDefinitions " +
                                         " inner join RecordTypes  On RecordTypes.RecordTypeID=RecordDefinitions.RecordTypeID " +
                                         " where VersionID= @DDDVersionID and RecordTypes.RecordType='Event'";*/
              string commandText = " select FaultCode, RecordDefID from RecordDefinitions " +
                                 " inner join RecordTypes  On RecordTypes.RecordTypeID=RecordDefinitions.RecordTypeID " +
                                 " where VersionID= @DDDVersionID And FaultCode Is Not Null";

              recordDefIDsSqlCommand.CommandText = commandText; //"select FaultCode, RecordDefID from RecordDefinitions where VersionID= @DDDVersionID";
              reader = null;
              StringBuilder errorMessage = new StringBuilder();
              errorMessage.Append("SQL command error");
              try
              {
                reader = recordDefIDsSqlCommand.ExecuteReader();
              }
              catch (SqlException Sqlex)
              {
                for (int i = 0; i < Sqlex.Errors.Count; i++)
                {
                  errorMessage.Append("\n" + Sqlex.Errors[i].Message);
                }
                logger.LogText(1, "AlstCfg", errorMessage.ToString());

                // return -1;
              }
              count = 0;
              while (reader.Read())
              {
                //Add devices to dictionary
                alstomDDDVersion.RecordDefIDs.Add(reader.GetInt32(0), reader.GetInt32(1));
                count++;
              }
              logger.LogText(2, "AlstCfg", "TUType name: {0}, DDDVersion: {1} - {2} Fault codes", TUTypeName, userVersion, count);
              reader.Close();

              // DDDversion to dictionary
              alstomTUType.DDDVersions.Add(alstDDDVerName, alstomDDDVersion);
            }
          }//DDDVer
        }//throught DDDVersions
        #endregion ///TUType DDDVersions

        #region AlstomTUInstances
        //logger.LogText(1, "AlstCfg", "AlstomTUInstances region");
        //AlstomTUInstances
        SqlCommand TUInstancesSqlCommand = new SqlCommand();
        TUInstancesSqlCommand.Connection = sqlConnection;
        TUInstancesSqlCommand.CommandTimeout = Properties.Settings.Default.SQLTimeout;
        TUInstancesSqlCommand.CommandType = CommandType.Text;

        SqlParameter inpTUTypeSQLParameter = new SqlParameter();
        inpTUTypeSQLParameter.ParameterName = "@dBTUTypeName ";
        inpTUTypeSQLParameter.IsNullable = false;
        inpTUTypeSQLParameter.SqlDbType = SqlDbType.VarChar;
        inpTUTypeSQLParameter.Direction = ParameterDirection.Input;
        inpTUTypeSQLParameter.Size = 50;
        inpTUTypeSQLParameter.Value = TUTypeName;
        TUInstancesSqlCommand.Parameters.Add(inpTUTypeSQLParameter);

        // Modify by Romeo 2011-02-22
        //string commantText = "Select distinct TUInstances.UICID as AlstTrainNumber , TUInstances.TUName, TUInstances.TUInstanceID, DDDVersions.UserVersion, DDDVersions.VersionID " +
        string commantText = "Select distinct TUInstances.TrainUICNo as AlstTrainNumber , TUInstances.TUName, TUInstances.TUInstanceID, DDDVersions.UserVersion, DDDVersions.VersionID " +
                             " From TUInstances " +
                             " inner join  TUTypes on TUTypes.TUTypeID=TUInstances.TUTypeID " +
                             " inner join DDDVersions on DDDVersions.VersionID=TUInstances.DDDVersionID " +
                             " Where TUTypes.TypeName=@dBTUTypeName";

        TUInstancesSqlCommand.CommandText = commantText;
        SqlDataReader readerTUI = null;
        readerTUI = TUInstancesSqlCommand.ExecuteReader();
        //logger.LogText(1, "AlstCfg", "ExecuteReader");
        int countTUI = 0;
        while (readerTUI.Read())
        {
          //AlstomTUInstances
          AlstomTUInstnace alstomTUInstnace = new AlstomTUInstnace(); // create AlstomTUInstnace object for current TUInstances 
          //Train number
          int alstTrainNumber = (int)readerTUI.GetInt32(0);  //(int)(readerTUI.GetInt64(0));
          alstomTUInstnace.TrainNumber = alstTrainNumber;
          //logger.LogText(2, "AlstCfg", "TrainNumber: {0}", alstomTUInstnace.TrainNumber);
          //TU instance name
          alstomTUInstnace.TUInstanceName = readerTUI.GetString(1);
          //logger.LogText(2, "AlstCfg", "TUInstanceName: {0}", alstomTUInstnace.TUInstanceName);
          //TU instance ID
          alstomTUInstnace.TUInstanceID = readerTUI.GetGuid(2);
          //logger.LogText(2, "AlstCfg", "TUInstanceID: {0}", alstomTUInstnace.TUInstanceID.ToString());
          //DDDVersionName
          alstomTUInstnace.DDDVersionName = readerTUI.GetString(3);
          //logger.LogText(2, "AlstCfg", "DDDVersionName: {0}", alstomTUInstnace.DDDVersionName);
          // dictionary of TUInstances

          if (!alstomTUType.TUInstances.ContainsKey(alstTrainNumber))
          {
            alstomTUType.TUInstances.Add(alstTrainNumber, alstomTUInstnace);
            countTUI++;
          }
          else
          {
            //  logger.LogText(2, "AlstCfg", "Error -TUInstances {0} already exists", alstTrainNumber);
            logger.LogText(2, "AlstCfg", "Error - TUInstance {0}in the TUType name: {1} already exists", alstTrainNumber, TUTypeName);
          }
        }
        logger.LogText(2, "AlstCfg", "TUType name: {0} - {1} TUInstances", TUTypeName, countTUI);

        readerTUI.Close();

        #endregion //AlstomTUInstances

        alstomCfg.TUTypes.Add(alstTUTypeName, alstomTUType);// add TUType to dictionary

      #endregion //AlstomCFG objects
      }//throught TUTypes
      sqlConnection.Close();

      return alstomCfg;
    }

    
    /// <summary>
    /// Saves new TU instances
    /// </summary>
    /// <param name="trainNb">Alstom tarin number</param>
    /// <param name="trainTypeNb">Alstom train type number</param>
    /// <param name="DDDVersion">alstom DDD version </param>
    /// <param name="alstCfg"> alstom config. object</param>
    /// <returns>AlstomTUInstnace congig. object</returns>
    public AlstomTUInstnace SaveAlstConfig(string trainNb, string trainTypeNb, string DDDVersion, AlstomCfg alstomCfg)
    {
      int result = SqlIni();
      logger.LogText(1, "AlstCfg", "Save configuration");

      AlstomTUInstnace alstomTUInstnace = new AlstomTUInstnace();
      string TUInstanceName;

      #region TUType
      //Alstom TUTypes
      int intTrainNumber;      // alstrom train number
      string sTrainNumber;     // alstom train number
      string sTrainTypeNumber; // alstom train type number
      string sDDDVersion;      // alstom DDDVersion
      string TUTypeName;       // name of TUType from config. file
      string userVersion;
      //train type number
      sTrainTypeNumber = trainTypeNb.Trim();
      sTrainTypeNumber = sTrainTypeNumber.PadLeft(2, '0');

      //train instance number
      intTrainNumber = Convert.ToInt32(trainNb);
      sTrainNumber = trainNb.Trim();
      sTrainNumber = sTrainNumber.PadLeft(3, '0');

      //DDD version
      sDDDVersion = DDDVersion.Trim();
      sDDDVersion = sDDDVersion.PadLeft(10, '0');

      TUTypeName = null;    //   name of TUType from config. file
      userVersion = null;

      #endregion //TUType

      #region InsertTUInstance
      if (alstomCfg.TUTypes.ContainsKey(sTrainTypeNumber))
      {// Train type exists
        AlstomTUType alstomTUType;
        alstomCfg.TUTypes.TryGetValue(sTrainTypeNumber, out alstomTUType);
        TUTypeName = alstomTUType.TUTypeName;
        TUInstanceName = TUTypeName + "_" + sTrainNumber;
        if (alstomTUType.DDDVersions.ContainsKey(sDDDVersion))
        { //DDDVersion version exists
          AlstomDDDVersion alstomDDDVersion;
          alstomTUType.DDDVersions.TryGetValue(sDDDVersion, out alstomDDDVersion);
          userVersion = alstomDDDVersion.UserVersion;

          //insert TTInstance to DB
          #region ToDB

          if (!alstomTUType.TUInstances.ContainsKey(intTrainNumber))
          {// if TUinstances dictionary doesn't contain inserted instance 
            //AlstomTUInstances
            SqlCommand TUInstancesSqlCommand = new SqlCommand();
            TUInstancesSqlCommand.Connection = sqlConnection;
            TUInstancesSqlCommand.CommandTimeout = Properties.Settings.Default.SQLTimeout;
            TUInstancesSqlCommand.CommandType = CommandType.Text;

            SqlParameter trainNbSQLPar = new SqlParameter();
            trainNbSQLPar.ParameterName = "@TrainNumber";
            trainNbSQLPar.IsNullable = false;
            trainNbSQLPar.SqlDbType = SqlDbType.Int;
            trainNbSQLPar.Direction = ParameterDirection.Input;
            trainNbSQLPar.Value = intTrainNumber;
            TUInstancesSqlCommand.Parameters.Add(trainNbSQLPar);

            SqlParameter TUTypeNameSQLPar = new SqlParameter();
            TUTypeNameSQLPar.ParameterName = "@TUTypeName";
            TUTypeNameSQLPar.IsNullable = false;
            TUTypeNameSQLPar.SqlDbType = SqlDbType.VarChar;
            TUTypeNameSQLPar.Direction = ParameterDirection.Input;
            TUTypeNameSQLPar.Size = 50;
            TUTypeNameSQLPar.Value = TUTypeName;
            TUInstancesSqlCommand.Parameters.Add(TUTypeNameSQLPar);

            SqlParameter userVerSQLPar = new SqlParameter();
            userVerSQLPar.ParameterName = "@UserVersion";
            userVerSQLPar.IsNullable = false;
            userVerSQLPar.SqlDbType = SqlDbType.VarChar;
            userVerSQLPar.Direction = ParameterDirection.Input;
            userVerSQLPar.Size = 50;
            userVerSQLPar.Value = userVersion;
            TUInstancesSqlCommand.Parameters.Add(userVerSQLPar);

            SqlParameter TUNameSQLPar = new SqlParameter();
            TUNameSQLPar.ParameterName = "@TUName";
            TUNameSQLPar.IsNullable = false;
            TUNameSQLPar.SqlDbType = SqlDbType.VarChar;
            TUNameSQLPar.Direction = ParameterDirection.Input;
            TUNameSQLPar.Size = 50;
            TUNameSQLPar.Value = TUTypeName + "_" + sTrainNumber;
            TUInstancesSqlCommand.Parameters.Add(TUNameSQLPar);

            SqlParameter commentSQLPar = new SqlParameter();
            commentSQLPar.ParameterName = "@Comment";
            commentSQLPar.IsNullable = false;
            commentSQLPar.SqlDbType = SqlDbType.VarChar;
            commentSQLPar.Direction = ParameterDirection.Input;
            commentSQLPar.Size = 250;
            commentSQLPar.Value = "TUInstance_" + TUTypeName + "_" + sTrainNumber;
            TUInstancesSqlCommand.Parameters.Add(commentSQLPar);

            // Modify by Romeo 2011-02-22
            //string commantText = " INSERT INTO[TUInstances]([TUInstanceID],[TUTypeID],[DDDVersionID],[TUName],[UICID],[Comment]) " +
            string commantText = " INSERT INTO[TUInstances]([TUInstanceID],[TUTypeID],[DDDVersionID],[TUName],[TrainUICNo],[Comment]) " +
                                 " select     newid(),TUTypes.TUTypeID,DDDVersions.VersionID,@TUName,@TrainNumber,@Comment " +
                                 " from TUTypes " +
                                 " inner join DDDVersions On DDDVersions.TUTypeID=TUTypes.TUTypeID " +
                                 " where TUTypes.TypeName=@TUTypeName And DDDVersions.UserVersion=@UserVersion ";

            TUInstancesSqlCommand.CommandText = commantText;

            int aff = TUInstancesSqlCommand.ExecuteNonQuery();
          #endregion//ToDB
            //insert new instance to alstomCfg object
            #region ToAlstomCFG
            //AlstomTUInstances
            SqlCommand TUInstanceSqlCommand = new SqlCommand();
            TUInstanceSqlCommand.Connection = sqlConnection;
            TUInstanceSqlCommand.CommandTimeout = Properties.Settings.Default.SQLTimeout;
            TUInstanceSqlCommand.CommandType = CommandType.Text;

            SqlParameter inpTUTypeSQLPar = new SqlParameter();
            inpTUTypeSQLPar.ParameterName = "@TUTypeName";
            inpTUTypeSQLPar.IsNullable = false;
            inpTUTypeSQLPar.SqlDbType = SqlDbType.VarChar;
            inpTUTypeSQLPar.Direction = ParameterDirection.Input;
            inpTUTypeSQLPar.Size = 50;
            inpTUTypeSQLPar.Value = TUTypeName;
            TUInstanceSqlCommand.Parameters.Add(inpTUTypeSQLPar);

            SqlParameter inpUserVerSQLPar = new SqlParameter();
            inpUserVerSQLPar.ParameterName = "@UserVersion";
            inpUserVerSQLPar.IsNullable = false;
            inpUserVerSQLPar.SqlDbType = SqlDbType.VarChar;
            inpUserVerSQLPar.Direction = ParameterDirection.Input;
            inpUserVerSQLPar.Size = 50;
            inpUserVerSQLPar.Value = userVersion;
            TUInstanceSqlCommand.Parameters.Add(inpUserVerSQLPar);

            SqlParameter inpTrainNbSQLPar = new SqlParameter();
            inpTrainNbSQLPar.ParameterName = "@TrainNumber";
            inpTrainNbSQLPar.IsNullable = false;
            inpTrainNbSQLPar.SqlDbType = SqlDbType.Int;
            inpTrainNbSQLPar.Direction = ParameterDirection.Input;
            inpTrainNbSQLPar.Value = intTrainNumber;
            TUInstanceSqlCommand.Parameters.Add(inpTrainNbSQLPar);

            commantText = " Select TUInstances.[TUName], TUInstances.TUInstanceID, DDDVersions.VersionID " +
                                 " From TUInstances " +
                                 " inner join  TUTypes on TUTypes.TUTypeID=TUInstances.TUTypeID " +
                                 " inner join DDDVersions on DDDVersions.VersionID=TUInstances.DDDVersionID " +
                                 " Where TUTypes.TypeName=@TUTypeName And DDDVersions.UserVersion=@UserVersion and TUInstances.TrainUICNo=@TrainNumber";
            // Modify by Romeo 2011-02-22
            //" Where TUTypes.TypeName=@TUTypeName And DDDVersions.UserVersion=@UserVersion and TUInstances.UICID=@TrainNumber";

            TUInstanceSqlCommand.CommandText = commantText;
            SqlDataReader readerTUI = null;

            readerTUI = TUInstanceSqlCommand.ExecuteReader();
            int countTUI = 0;
            while (readerTUI.Read())
            {
              //Train number
              alstomTUInstnace.TrainNumber = intTrainNumber;
              //TU instance name
              alstomTUInstnace.TUInstanceName = readerTUI.GetString(0); //TUInstanceName;
              //TU instance ID
              alstomTUInstnace.TUInstanceID = readerTUI.GetGuid(1);
              //DDDVersionName
              alstomTUInstnace.DDDVersionName = userVersion;
              // dictionary of TUInstances

              if (!alstomTUType.TUInstances.ContainsKey(intTrainNumber))
              {
                alstomTUType.TUInstances.Add(intTrainNumber, alstomTUInstnace);
                countTUI++;
              }
              else
              {
                logger.LogText(2, "AlstCfg", "Error - TUInstance {0} for the TUType name: {1} already exists", intTrainNumber, TUTypeName);
              }
            } //while (readerTUI.Read())
            logger.LogText(2, "AlstCfg", "TUType name: {0} - {1} TUInstances", TUTypeName, countTUI);

            readerTUI.Close();

          }//if TUinstances dictionary doesn't contain inserted instance 
          else
          {
            logger.LogText(2, "AlstCfg", "Error - TUInstance {0} for the TUType name: {1} already exists", intTrainNumber, TUTypeName);
          }
            #endregion //ToAlstomCFG

          //insert TUInstance to hierarchy
          #region ToHierarchy
          //set SQL command
          SqlCommand hierSqlCommand = new SqlCommand();
          hierSqlCommand.Connection = sqlConnection;
          hierSqlCommand.CommandTimeout = Properties.Settings.Default.SQLTimeout;
          hierSqlCommand.CommandType = CommandType.StoredProcedure;

          //set SQL command parameters
          SqlParameter pathSQLPar = new SqlParameter();
          pathSQLPar.ParameterName = "@path";
          pathSQLPar.IsNullable = false;
          pathSQLPar.SqlDbType = SqlDbType.NVarChar;
          pathSQLPar.Direction = ParameterDirection.Input;
          pathSQLPar.Size = 255;
          pathSQLPar.Value = Properties.Settings.Default.NewTUInstHierPath;
          hierSqlCommand.Parameters.Add(pathSQLPar);

          SqlParameter typeSQLPar = new SqlParameter();
          typeSQLPar.ParameterName = "@type";
          typeSQLPar.IsNullable = false;
          typeSQLPar.SqlDbType = SqlDbType.NVarChar;
          typeSQLPar.Direction = ParameterDirection.Input;
          typeSQLPar.Size = 255;
          typeSQLPar.Value = Properties.Settings.Default.NewTUInstHierType;
          hierSqlCommand.Parameters.Add(typeSQLPar);

          SqlParameter nameSQLPar = new SqlParameter();
          nameSQLPar.ParameterName = "@name";
          nameSQLPar.IsNullable = false;
          nameSQLPar.SqlDbType = SqlDbType.NVarChar;
          nameSQLPar.Direction = ParameterDirection.Input;
          nameSQLPar.Size = 255;
          nameSQLPar.Value = TUInstanceName;
          hierSqlCommand.Parameters.Add(nameSQLPar);

          SqlParameter descriptionSQLPar = new SqlParameter();
          descriptionSQLPar.ParameterName = "@description";
          descriptionSQLPar.IsNullable = false;
          descriptionSQLPar.SqlDbType = SqlDbType.NVarChar;
          descriptionSQLPar.Direction = ParameterDirection.Input;
          descriptionSQLPar.Size = 255;
          descriptionSQLPar.Value = "";
          hierSqlCommand.Parameters.Add(descriptionSQLPar);

          try
          {//execute stored procedure
            hierSqlCommand.CommandText = "Hierarchy.InsertNewItem";
            int AffRows = hierSqlCommand.ExecuteNonQuery();
          }
          catch (SqlException Sqlex)
          {
            StringBuilder errorMessage = new StringBuilder();
            errorMessage.Append("Error in execution of Hierarchy.InsertNewItem stored procedure");
            for (int i = 0; i < Sqlex.Errors.Count; i++)
            {
              errorMessage.Append("\n" + Sqlex.Errors[0].Message);
            }
            logger.LogText(1, "AlstCfg", errorMessage.ToString());
            hierSqlCommand.Connection.Close();
            //return -1;
          }
          #endregion //ToHierarchy

        }
        else
        {
          logger.LogText(1, "AlstCfg", "Error - no valid fault description version {0}", sDDDVersion);
        }
      }
      else
      {
        logger.LogText(1, "AlstCfg", "Error - no valid train type number {0} ", sTrainTypeNumber);
      }

      #endregion //ins TUInstances to config file
      sqlConnection.Close();

      return alstomTUInstnace;
    }

  }
}
