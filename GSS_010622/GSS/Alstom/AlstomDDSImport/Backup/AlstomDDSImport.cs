using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TDManagementForms;
using System.Threading;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.IO;
using System.Xml;
using System.Security;
using System.IO.Compression;
using System.Globalization;

namespace AlstomDDSImport
{
  partial class AlstomDDSImport : ServiceBase
  {
    #region Private members
    private AlstomCfg alstomConfig; //Represents Alstom diagnostic configurations
    private AlstConfig configManager; //Manager of configuration objects
    private AlstomDDDGenerator dddGenerator; //Generator of DDD Versions from Alstom configuration files
    private bool bConfigurationLoaded; //Configuration has already been loaded
    private System.Threading.Timer baseTimer = null; //Timer for basic periods
    private DirectoryInfo srcDirInfo;
    private DirectoryInfo convDirInfo;
    private DirectoryInfo backupDirInfo;
    private DirectoryInfo drDirInfo;
    private int currDRFileID; //Current ID (number) of DR file
    private FileStream currDRFileStream; //Current file stream for DR files
    private bool bWorkerRunning; //Worker thread is already running
    private int movedCount; //Number of files moved from source to convert directory
    private TDManagementForms.Logger logger; //Logger object
    private FileInfoComparer fileInfoComparer; //Comparer for FileInfo type
    private AlstomDDSStorage ddsStorage; //Storage of unprocessed DDS records
    private static DateTimeFormatInfo timeFormatInfo; //Time format info object
    #endregion

    #region Construction
    /// <summary>
    /// Standard constructor
    /// </summary>
    public AlstomDDSImport()
    {
      InitializeComponent();
      //Create time format info
      timeFormatInfo = new DateTimeFormatInfo();
      timeFormatInfo.TimeSeparator = ":";
      //Create FileInfo comparer
      fileInfoComparer = new FileInfoComparer();
      //Create logger
      logger = new Logger();
      logger.LogToFile = true;
      logger.MaxFileLength = Properties.Settings.Default.MaxLogSize;
      logger.MaxHistoryFiles = Properties.Settings.Default.MaxLogHistoryFiles;
      logger.FileName = Properties.Settings.Default.LogFileName;
      logger.FileVerbosity = Properties.Settings.Default.LogVerbosity;
      //Create directory infos for working directories
      srcDirInfo = new DirectoryInfo(Properties.Settings.Default.DDSSourceDir);
      convDirInfo = new DirectoryInfo(Properties.Settings.Default.DDSConvertDir);
      backupDirInfo = new DirectoryInfo(Properties.Settings.Default.DDSBackDir);
      drDirInfo = new DirectoryInfo(Properties.Settings.Default.DRImportDir);
      //Create storage object
      ddsStorage = new AlstomDDSStorage(
        Properties.Settings.Default.DDSStorageDir, 
        Properties.Settings.Default.DDSStorageFileExt);
      //Create configuration manager
      configManager = new AlstConfig(logger);
      //Create ddd generator
      dddGenerator = new AlstomDDDGenerator(logger);
      //Initialize status
      currDRFileID = -1;
      bWorkerRunning = false;
      bConfigurationLoaded = false;
    }
    #endregion

    #region Event handlers
    /// <summary>
    /// Handles start of service.
    /// Import timer is started
    /// </summary>
    protected override void OnStart(string[] args)
    {
      logger.LogText(0, "", "");
      logger.LogText(0, "", "----------------------------------------------");
      logger.LogText(0, "DDSImp", "Alstom DDS Import service started");
      //Create period timer
      //First create delegate for callback method
      TimerCallback timerDelegate = new TimerCallback(baseTimer_Elapsed);
      //Create and start timer
      baseTimer = new System.Threading.Timer(
        timerDelegate,
        null,
        Properties.Settings.Default.StartupDelaySec * 1000,
        Properties.Settings.Default.BasePerSec * 1000);
      logger.LogText(0, "DDSImp", "Created timer with period {0} sec, startup delay {1} sec",
        Properties.Settings.Default.BasePerSec, Properties.Settings.Default.StartupDelaySec);
    }

    protected override void OnStop()
    {
      if (baseTimer != null)
        baseTimer.Dispose();
      logger.LogText(0, "DDSImp", "Alstom DDS Import service stopped");
    }


    /// <summary>
    /// Raised after timer is elapsed
    /// </summary>
    /// <param name="state"></param>
    protected void baseTimer_Elapsed(object state)
    {
      //Check if worker thread is not already running
      if (bWorkerRunning)
      {
        logger.LogText(0, "DDSImpErr", "DDS Import is overloading");
        return;
      }
      //Set worker thread is running
      bWorkerRunning = true;
      //logger.LogText(5, "DDSImp", "Timer elapsed");

      //Load configuration
      //Check if configuration is already loaded
      if (!bConfigurationLoaded)
      { //Load configuration
        try
        {
          //Load CFG objects
          logger.LogText(0, "DDSImp", "Loading configuration...");
          //Load configuration
          alstomConfig = configManager.LoadAlstConfig();
          logger.LogText(0, "DDSImp", "Configuration loaded");
          //Set configuration loaded flag
          bConfigurationLoaded = true;
          //Load stored unprocessed DDS records
          logger.LogText(0, "DDSImp", "Loading stored unprocessed DDS records...");
          ddsStorage.LoadStoredDDS();
          logger.LogText(0, "DDSImp", "Loaded stored DDS for {0} TU Instances", ddsStorage.GetTUInstCount());
          //Retry files failed because of unknown DDD version
          logger.LogText(0, "DDSImp", "Checking all subdirectories in failed dir for known DDD Version...");
          RetryUnknownVersionFiles();
          logger.LogText(0, "DDSImp", "All subdirectories of failed dir rechecked");
        }
        catch (Exception ex)
        {
          logger.LogText(0, "DDSImpErr", "Failed to load configuration");
          logger.LogException(1, "DDSImpErr", ex);
          //Worker thread is not running
          bWorkerRunning = false;
          return; //End period
        }
      }

      //Perform actual data transformation
      try
      {
        //Try to import new configuration version
        ImportNewConfigVersions();
        //Move DDS files from source directory
        MoveDDSFilesToConvertDir();
        //Transform XML files to DR files
        TransformDDSFiles();
        //Backup transformed DDS files
        BackupDDSFiles();
        //Remove old backup directories
        RemoveOldBackupDirs();
      }
      catch (Exception ex)
      {
        //Transformation failed - log the error together with inner errors
        logger.LogText(0, "DDSImpErr", "DDS Transformation failed: {0}", ex.Message);
        if (ex.InnerException != null)
          logger.LogException(1, "DDSImpErr", ex.InnerException);
      }
      //Worker thread is not running
      bWorkerRunning = false;
    }
    #endregion

    #region Helper methods
    /// <summary>
    /// Moves all available files with DDS from source directory to convert directory
    /// </summary>
    private void MoveDDSFilesToConvertDir()
    {
      movedCount = 0; //number of moved files
      //Refresh info about source directory
      srcDirInfo.Refresh();
      //Move available files to conversion directory
      foreach (FileInfo file in srcDirInfo.GetFiles("*F.dat.gz", SearchOption.AllDirectories))
      {
        try
        {
          file.MoveTo(Path.Combine(convDirInfo.FullName, file.Name));// directory + file name 
          movedCount++;
        }
        catch (Exception)
        {
          logger.LogText(6, "DDSImpErr", "Unable to move source file {0}", file.Name);
        }
      }
      if (movedCount > 0)
        logger.LogText(5, "DDSImp", "Moved {0} files from source to convert directory", movedCount);
    }

      /// <summary>
      /// Moves all DDS files from convert directory to current backup directory
      /// </summary>
    private void BackupDDSFiles()
    {
      int movedCount = 0;
      String trgPath;
      //Create backup directory info
      DirectoryInfo currBackDir = new DirectoryInfo(
        Path.Combine(Properties.Settings.Default.DDSBackDir, DateTime.Now.ToString("yyyyMMdd")));
      //Create current backup directory, if it does not exist
      if (!currBackDir.Exists)
        currBackDir.Create();
      //now move all files from convert directory to backup directory
      convDirInfo.Refresh();
      foreach (FileInfo ddsFileInfo in convDirInfo.GetFiles())
      {
        try
        {
          trgPath = Path.Combine(currBackDir.FullName, ddsFileInfo.Name);
          if (File.Exists(trgPath))
          {
            logger.LogText(2, "DDSImp", "File {0} already existis in backup direcory {1}. It will be deleted.", ddsFileInfo.Name, currBackDir.Name);
            File.Delete(trgPath);
          }
          ddsFileInfo.MoveTo(trgPath);
          movedCount++;
        }
        catch (Exception ex)
        {
          logger.LogText(6, "DDSImpErr", "Unable to move file {0} to backup directory {1}: {2}", ddsFileInfo.Name, currBackDir.Name, ex.Message);
        }
      }
      if (movedCount > 0)
        logger.LogText(5, "DDSImp", "Moved {0} files to backup direcory {1}", movedCount, currBackDir.Name);
    }

      /// <summary>
      /// Deletes backup DDS directories older than given treshold
      /// </summary>
    private void RemoveOldBackupDirs()
    {
      int delCount = 0;
      //Get name of last kept backup directory
      String minBckDirName = DateTime.Now.AddDays(-1 * Properties.Settings.Default.DDSHistoryDays).ToString("yyyyMMdd");
      //Iterate over backup directories, remove too old directories
      backupDirInfo.Refresh();
      foreach (DirectoryInfo bckDayDir in backupDirInfo.GetDirectories())
      {
        try
        {
          if (String.Compare(bckDayDir.Name, minBckDirName) < 0)
          { //directory is too old - remove
            bckDayDir.Delete(true);
            logger.LogText(6, "DDSImp", "Deleted backup directory {0}", bckDayDir.Name);
            delCount++;
          }
        }
        catch (Exception ex)
        {
          logger.LogText(6, "DDSImpErr", "Unable to delete backup directory {0}: {1}", bckDayDir.Name, ex.Message);
        }
      }
      if (delCount > 0)
        logger.LogText(5, "DDSImp", "Deleted {0} backup directories", delCount);
    }

      /// <summary>
      /// Creates new DR file in DR directory with new name (ID).
      /// Returns XML writer opened over this file
      /// </summary>
    private XmlWriter CreateNewDRFile()
    {
      try
      {
        //Find first free DR File ID
        int newFileID = 0;
        int tmpID;
        if (currDRFileID > 0)
          newFileID = currDRFileID + 1;
        else
        { //No file was opened yet - open new one
          drDirInfo.Refresh();
          foreach (FileInfo drFileIfo in drDirInfo.GetFiles("a*.xml", SearchOption.TopDirectoryOnly))
          { //Search all files, find highest ID
            String strID = Path.GetFileNameWithoutExtension(drFileIfo.Name).Remove(0, 1);
            if (Int32.TryParse(strID, out tmpID) && (tmpID > newFileID))
              newFileID = tmpID;
          }
          newFileID++;
        }
        //Create file name
        String drFileName = "a" + newFileID.ToString("000000") + ".xml";
        drFileName = Path.Combine(drDirInfo.FullName, drFileName);
        //Open new file stream with name
        currDRFileStream = new FileStream(drFileName, FileMode.CreateNew, FileAccess.Write, FileShare.None);
        //Open new XML writer
        XmlWriterSettings settings = new XmlWriterSettings();
        settings.Indent = true;
        settings.Encoding = Encoding.UTF8;
        settings.OmitXmlDeclaration = true;
        settings.ConformanceLevel = ConformanceLevel.Fragment;
        settings.CloseOutput = true;
        XmlWriter drXMLWriter = XmlWriter.Create(currDRFileStream, settings);
        return drXMLWriter;
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to create new DR file", ex);
      }
    }

    /// <summary>
    /// Transforms one source DDS file into destination XML DR file.
    /// Unsuccessfully transformed file is copied to failed directory
    /// </summary>
    private void TransformDDSFile(
      FileInfo srcFileInfo, XmlWriter dstWriter, 
      out int succRecCount, out int failedRecCount)
    {
      failedRecCount = 0;
      succRecCount = 0;
      try
      {
        AlstomDDDVersion dddVersion;
        AlstomTUType tuType;
        AlstomTUInstnace tuInstance;
        int trainID;
        //Obtain configuration objects based on file name
        AlstomDDSFileHelper.GetCfgObjects(srcFileInfo.Name, alstomConfig, out tuType, out dddVersion, out trainID);
        //Obtain TUInstance or register new one
        if (!tuType.TUInstances.TryGetValue(trainID, out tuInstance))
        { //Instance not registered
          if (Properties.Settings.Default.AutRegisterUnknownTUInst)
            tuInstance = configManager.SaveAlstConfig(trainID.ToString(), tuType.AlstomTypeName, dddVersion.AlstomVersionName, alstomConfig);
          else
            throw new Exception(String.Format("Unknown train ID {0} (TU Instance does not exist)", trainID));
        }
        //Try to obtain previously stored DDS for TUInstance
        List<AlstomDDSRecord> storedDDS = ddsStorage.GetStoredDDS(tuInstance);
        //Load DDS from the file
        List<AlstomDDSRecord> loadedDDS;
        using (FileStream fileStream = srcFileInfo.OpenRead())
        { //Open the file
          using (GZipStream zipStream = new GZipStream(fileStream, CompressionMode.Decompress))
          {
            //Load DDS records from the file
            if (!Properties.Settings.Default.StoreDecodedDDSFile)
              loadedDDS = AlstomDDSFileHelper.LoadDDSRecords(zipStream, dddVersion.DDSRecordLength, dddVersion.EnvDataSampleLen, null);
            else
            { //Create also decoded output file
              String outFileName = Path.Combine(srcFileInfo.DirectoryName, Path.GetFileNameWithoutExtension(srcFileInfo.Name) + ".txt");
              using (StreamWriter outFile = new StreamWriter(outFileName, false))
              { //Load DDS records and create output file
                loadedDDS = AlstomDDSFileHelper.LoadDDSRecords(zipStream, dddVersion.DDSRecordLength, dddVersion.EnvDataSampleLen, outFile);
              }
            }
          }
        }
        //Combine the lists
        List<AlstomDDSRecord> ddsRecords;
        if (storedDDS != null && storedDDS.Count > 0 && loadedDDS.Count > 0)
        { //Some records were stored for the train
          if (loadedDDS[0].TimeStamp < storedDDS[storedDDS.Count - 1].TimeStamp)
          { //Time sequence is disrupted
            /*
            logger.LogText(1, "DDSImpWar", "Last rec stored for train {0} is newer than first loaded rec ({1:yyyy-MM-ddTHH:mm:ss}, {2:yyyy-MM-ddTHH:mm:ss}",
              tuInstance.TUInstanceName, storedDDS[storedDDS.Count - 1].TimeStamp, loadedDDS[0].TimeStamp);
            */
            ddsRecords = loadedDDS; //Work only with loaded records
          }
          else
          { //Combine stored and loaded records
            ddsRecords = storedDDS;
            ddsRecords.AddRange(loadedDDS);
          }
        }
        else
          ddsRecords = loadedDDS; //Work only with loaded records
        //Iterate over DDS, extract faults
        DateTime lastTimeStamp = DateTime.MinValue;
        for (int i = 0; i < ddsRecords.Count; i++)
        {
          try
          {
            AlstomDDSRecord ddsRecord = ddsRecords[i];
            /*
            //Check for continues time sequence
            if (ddsRecord.TimeStamp < lastTimeStamp)
              logger.LogText(3, "DDSImpWar", "Time sequence is disrupted. Rec index: {0}, Current: {1:yyyy-MM-ddTHH:mm:ss}, Previous: {2:yyyy-MM-ddTHH:mm:ss}",
                i, ddsRecord.TimeStamp, lastTimeStamp);
            */
            lastTimeStamp = ddsRecord.TimeStamp;
            if (ddsRecord.FaultCode != 0)
            { //Record represents fault code
              //Obtain environmental data
              int foundSamples;
              Byte[] envData = AlstomDDSFileHelper.GetEnvDataArray2(ddsRecords, i, dddVersion, out foundSamples);
              if (envData == null)
                continue; //Env data are not complete yet - we have to wait for next file
              //Obtain record def id and device code
              int recordDefID;
              String source;
              if (!dddVersion.RecordDefIDs.TryGetValue(ddsRecord.FaultCode, out recordDefID))
                throw new Exception(String.Format("Unknown fault code {0}", ddsRecord.FaultCode));
              if (!dddVersion.DeviceNames.TryGetValue(ddsRecord.DeviceCode, out source))
                throw new Exception(String.Format("Unknown device code {0}", ddsRecord.DeviceCode));
              //Write record to output XML file
              dstWriter.WriteStartElement("ImportedDR");
              dstWriter.WriteElementString("TUInstanceID", tuInstance.TUInstanceID.ToString());
              dstWriter.WriteElementString("DDDVersionID", dddVersion.DDDVersionID.ToString());
              dstWriter.WriteElementString("RecordDefID", recordDefID.ToString());
              //Convert time to UTC
              DateTime startTime = ddsRecord.TimeStamp.ToUniversalTime(); ;
              dstWriter.WriteElementString("RecordInstID", startTime.Ticks.ToString());
              dstWriter.WriteElementString("StartTime", startTime.ToString("yyyy-MM-ddTHH:mm:ss.fff", timeFormatInfo));
              dstWriter.WriteElementString("LastModified", startTime.ToString("yyyy-MM-ddTHH:mm:ss.fff", timeFormatInfo));
              dstWriter.WriteElementString("CompleteData", "true");
              dstWriter.WriteElementString("BigEndianData", "false");
              dstWriter.WriteElementString("Source", source);
              dstWriter.WriteElementString("BinaryData", Convert.ToBase64String(envData));
              dstWriter.WriteElementString("VehicleNumber", ddsRecord.VehicleNumber.ToString());
              dstWriter.WriteElementString("DeviceCode", ddsRecord.DeviceCode.ToString());
              dstWriter.WriteEndElement();
              //Clear fault code from processed record
              ddsRecord.FaultCode = 0;
              //Increment number of successfully processed records
              succRecCount++;
            }
          }
          catch (Exception ex)
          { //Failed to process the DDS record
            logger.LogText(3, "DDSImpErr", "Failed to process DDS Record with index {0}", i);
            logger.LogException(4, "DDSImpErr", ex);
            failedRecCount++;
          }
        }

        //Store unprocessed DDS records
        List<AlstomDDSRecord> unprocessedDDS = AlstomDDSFileHelper.GetUnprocessedDDSRecords(ddsRecords, dddVersion);
        ddsStorage.StoreDDS(unprocessedDDS, tuInstance);
        //Check if file was processed successfully
        if (failedRecCount > 0)
          throw new Exception(String.Format("{0} records were not processed sucessfully", failedRecCount));
      }
      catch (UnknownDDDException ex)
      { //File refers to unknown DDD version - copy to failed subdirectory
        logger.LogText(1, "DDSImpErr", "Failed to transform file {0}: {1}", srcFileInfo.Name, ex.Message);
        if (ex.InnerException != null)
          logger.LogException(2, "DDSImpErr", ex.InnerException);
        logger.LogText(1, "DDSImp", "Copying the file into failed subdirecory " + ex.AlstomVerName);
        CopyFileToFailedDirectory(srcFileInfo, ex.AlstomVerName);
      }
      catch (Exception ex)
      { //Other exception - copy file to failed directory
        logger.LogText(1, "DDSImpErr", "Failed to transform file {0}: {1}", srcFileInfo.Name, ex.Message);
        if (ex.InnerException != null)
          logger.LogException(2, "DDSImpErr", ex.InnerException);
        logger.LogText(1, "DDSImp", "Copying the file into failed direcory");
        CopyFileToFailedDirectory(srcFileInfo, null);
      }
    }

    /// <summary>
    /// Transform all DDS files in Convert directory into DR files in DR files directory.
    /// Unsuccessfully transformed files are copied to failed directory.
    /// </summary>
    private void TransformDDSFiles()
    {
      try
      {
        int transfCount = 0;
        //Get output XmlWriter
        XmlWriter dstWriter = null;
        //Refesh info about convert directory
        convDirInfo.Refresh();
        //Obtain array of FileInfos from Convert directory
        FileInfo[] srcFiles = convDirInfo.GetFiles("*.*", SearchOption.TopDirectoryOnly);
        //Sort file infos accroding to file name
        Array.Sort<FileInfo>(srcFiles, fileInfoComparer);
        //Iterate over all sorted DDS files in Convert direcory
        int failedRecCount;
        int succRecCount;
        foreach (FileInfo srcDDSFile in srcFiles)
        {
          if (dstWriter == null)
            dstWriter = CreateNewDRFile(); //Create new writer if necessary
          TransformDDSFile(srcDDSFile, dstWriter, out succRecCount, out failedRecCount); //Transform the file
          logger.LogText(6, "DDSImp", "Transformed DDS file {0}: Succ. rec. {1}, Failed rec. {2}", 
            srcDDSFile.Name, succRecCount, failedRecCount);
          transfCount++;
          dstWriter.Flush();
          if (currDRFileStream.Length > (Properties.Settings.Default.MaxDRFileLenkB * 1024))
          {
            //Close writer
            dstWriter.Close();
            //Close and dispose file stream
            currDRFileStream.Close();
            currDRFileStream.Dispose();
            currDRFileStream = null;
            dstWriter = null;
          }
        }
        if (transfCount > 0)
          logger.LogText(5, "DDSImp", "Transformed {0} DDS files into DR file", transfCount);
        //Close writer if opened
        if (dstWriter != null)
        {
          dstWriter.Close();
          dstWriter = null;
        }
        //Close and dispose output file stream if opened
        if (currDRFileStream != null)
        {
          currDRFileStream.Close();
          currDRFileStream.Dispose();
          currDRFileStream = null;
        }
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to transform source DDS files", ex);
      }
    }

    /// <summary>
    /// Copies file to failed directory.
    /// If unknownVerName is not null, copies the file to subdirectory with the same name.
    /// </summary>
    /// <param name="srcFileInfo">File to copy to failed directory</param>
    /// <param name="failSubdirName">Name of the unknown version</param>
    private void CopyFileToFailedDirectory(FileInfo srcFileInfo, String failSubdirName)
    {
      try
      {
        String targetPath = Properties.Settings.Default.DDSFailedDir;
        if (failSubdirName != null && failSubdirName != String.Empty)
        { //Name of the subdirectory is specified
          targetPath = Path.Combine(targetPath, failSubdirName);
          //Subfolder for this version does not exist yet - create new one
          if (!Directory.Exists(targetPath))
            Directory.CreateDirectory(targetPath);
        }
        //Copy the file to failed directory, overwrite existing
        srcFileInfo.CopyTo(Path.Combine(targetPath, srcFileInfo.Name), true);
      }
      catch (Exception ex)
      {
        logger.LogText(1, "DDSImpErr", "Failed to copy file {0} to failed direcory: {1}", srcFileInfo.Name, ex.Message);
        if (ex.InnerException != null)
          logger.LogException(2, "DDSImpErr", ex.InnerException);
      }
    }

    /// <summary>
    /// Method rechecks all the subdirectories of failed directory.
    /// If name of one of them is the same as name of known DDD version 
    /// all the files are moved to DDSSource directory and subdirectory is deleted.
    /// Used to retry the import in case new DDD version is available.
    /// </summary>
    private void RetryUnknownVersionFiles()
    {
      DirectoryInfo failedDirInfo = new DirectoryInfo(Properties.Settings.Default.DDSFailedDir);
      //Obtain all subdirectories of failed directory
      foreach (DirectoryInfo dirInfo in failedDirInfo.GetDirectories())
      { //Iterate over all subdirectories
        try
        {
          bool bVerExists = false;
          //Try if version with Dir name already exists in some TU Type
          foreach (AlstomTUType tuType in alstomConfig.TUTypes.Values)
            if (tuType.DDDVersions.ContainsKey(dirInfo.Name))
            {
              bVerExists = true;
              break;
            }
          if (bVerExists)
          { //Version already exists - move files to DDSSource and delete version subdirecotry
            logger.LogText(3, "DDSImp", "Retrying to import files for DDD Version {0}", dirInfo.Name);
            foreach (FileInfo fileInfo in dirInfo.GetFiles())
            {
              logger.LogText(4, "DDSImp", "Moving file {0} to {1}", fileInfo.Name, Properties.Settings.Default.DDSSourceDir);
              fileInfo.MoveTo(Path.Combine(Properties.Settings.Default.DDSSourceDir, fileInfo.Name));
            }
            logger.LogText(4, "DDSImp", "Files moved to DDSSource directory");
            //Delete the subdirectory
            dirInfo.Delete();
            logger.LogText(4, "DDSImp", "Failed subdirectory {0} deleted", dirInfo.Name);
          }
        }
        catch (Exception ex)
        { //Failed to retry the files
          logger.LogText(1, "DDSImpErr", "Failed to retry files with unknown DDD version {0}:", dirInfo.Name);
          logger.LogException(2, "DDSImpErr", ex);
        }
      }
    }

    /// <summary>
    /// Checks source directory for new uploade configuration versions 
    /// and imports them into the database.
    /// </summary>
    private void ImportNewConfigVersions()
    {
      bool bSomeVersionImported = false;
      //Refresh source directory info
      srcDirInfo.Refresh();
      //Get list of file versions
      foreach (FileInfo configFile in srcDirInfo.GetFiles("*ConfigIT.xml.gz", SearchOption.TopDirectoryOnly))
      { //Iterate over version files
        try
        { //Try to move the file to source directory first
          configFile.MoveTo(Path.Combine(convDirInfo.FullName, configFile.Name));
        }
        catch (Exception)
        { //Couldn't move the file - maybe not uploaded completely
          continue; //wait for next period
        }

        try
        { //File in source directory - continue with the import
          logger.LogText(3, "DDSImp", "Importing configuration file {0}", configFile.Name);
          //Load file into XML document
          XmlDocument configDoc = new XmlDocument();
          logger.LogText(4, "DDSImp", "Loading file into XML document");
          using (FileStream fileStream = configFile.OpenRead())
          { //Open zipped file
            using (GZipStream zipStream = new GZipStream(fileStream, CompressionMode.Decompress))
            { //Create decompress stream
              configDoc.Load(zipStream); //Load XML into the document
            }
          }
          //Call the import procedure
          logger.LogText(4, "DDSImp", "Importing the configuration");
          String versionName; //Name of the imported version
          ArrayList affectedTUTypes = new ArrayList(); //List of TU Types for which version was imported
          ImportDDDVerResult result;
          result = dddGenerator.GenerDDDFile(configDoc, out versionName, alstomConfig, affectedTUTypes);
          //Set flag in case of success
          if (affectedTUTypes.Count > 0)
          { //Version imported for at least one TU Type
            bSomeVersionImported = true;
            logger.LogText(4, "DDSImp", "Configuration imported for {0} TU Types",
              affectedTUTypes.Count);
          }
          //Check for import error
          if (result != ImportDDDVerResult.OK && result != ImportDDDVerResult.TUTypeNotExist)
            throw new Exception("Failed to import the configuration: " + result.ToString());
        }
        catch (Exception ex)
        { //Failed to import the configuration file
          logger.LogText(1, "DDSImpErr", "Failed to import config version {0}:", configFile.Name);
          logger.LogException(2, "DDSImpErr", ex);
          //Copy file to failed directory
          logger.LogText(3, "DDSImp", "Copying file to failed directory");
          CopyFileToFailedDirectory(configFile, "Configuration");
        }

        try
        { //Move file to backup directory - regardless of import success
          String bckDirName = Path.Combine(backupDirInfo.FullName, "Configuration");
          logger.LogText(4, "DDSImp", "Moving file into backup directory {0}", bckDirName);
          if (!Directory.Exists(bckDirName))
            Directory.CreateDirectory(bckDirName);
          String trgPath = Path.Combine(bckDirName, configFile.Name);
          if (File.Exists(trgPath))
          { //File already exists in backup directory - it was converted before
            logger.LogText(2, "DDSImp", "File {0} already existis in backup direcory {1}. It will be deleted.", configFile.Name, bckDirName);
            File.Delete(trgPath);
          }
          configFile.MoveTo(trgPath);
        }
        catch (Exception ex)
        { //Failed to move the file into backup directory
          logger.LogText(1, "DDSImpErr", "Failed to move file {0} to backup directory:", configFile.Name);
          logger.LogException(2, "DDSImpErr", ex);
        }
      }

      //Check if some version was imported successfuly
      if (bSomeVersionImported)
        RetryUnknownVersionFiles(); //Retry import of failed dat files for new known versions
    }

    #endregion
  }//class DDSImport

  #region FileInfoComparer class
  /// <summary>
  /// Class implementing IComparer interface which is able to compare two FileInfo objects
  /// </summary>
  class FileInfoComparer : IComparer<FileInfo>
  {
    public int Compare(FileInfo x, FileInfo y)
    {
      return x.Name.CompareTo(y.Name);
    }
  }
  #endregion
}
