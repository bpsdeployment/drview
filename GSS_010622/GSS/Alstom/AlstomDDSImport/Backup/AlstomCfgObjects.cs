using System;
using System.Collections.Generic;
using System.Text;

namespace AlstomDDSImport
{
  /// <summary>
  /// Class holding configuration of Alstom diagnostic application
  /// </summary>
  public class AlstomCfg
  {
    #region Public members
    /// <summary>
    /// Dictionary of configured Alstom TU Types.
    /// Stored under Alstom train type name (number)
    /// </summary>
    public Dictionary<String, AlstomTUType> TUTypes;
    #endregion //Public members
  }

  /// <summary>
  /// Information about one configured TU Type representing Alstom train
  /// </summary>
  public class AlstomTUType
  {
    #region Public members
    /// <summary>
    /// Name of the TU Type in CDDB database
    /// </summary>
    public String TUTypeName;
    /// <summary>
    /// Alstom train type name (number)
    /// </summary>
    public String AlstomTypeName;
    /// <summary>
    /// Dictionary of DDD Versions associated with this TU Type.
    /// Versions are stored under Alstom version name
    /// </summary>
    public Dictionary<String, AlstomDDDVersion> DDDVersions;

    /// <summary>
    /// Dictionary of known TU Instances.
    /// Stored under Alstom train number.
    /// </summary>
    public Dictionary<int, AlstomTUInstnace> TUInstances;


    #endregion //Public members
  }

  /// <summary>
  /// Information about one configured DDDVersion representing 
  /// configuration of Alstom diagnostic application
  /// </summary>
  public class AlstomDDDVersion
  {
    #region Public members
    /// <summary>
    /// Name of the version in CDDB database
    /// </summary>
    public String UserVersion; 
    /// <summary>
    /// Alstom version identification
    /// </summary>
    public String AlstomVersionName;
    /// <summary>
    /// Unique version identifier from CDDB database
    /// </summary>
    public Guid DDDVersionID;
    /// <summary>
    /// Number of environmental data samples stored before 
    /// record start for each record.
    /// </summary>
    public int SamplesBeforeStart;
    /// <summary>
    /// Number of environmental data samples stored after 
    /// record start for each record (including sample at the time of record start).
    /// </summary>
    public int SamplesAfterStart;
    /// <summary>
    /// Period of samples
    /// </summary>
    public int SamplingPeriod;
    /// <summary>
    /// Length of one record in DDS file
    /// </summary>
    public int DDSRecordLength;
    /// <summary>
    /// Length of one environmental data sample in bytes.
    /// </summary>
    public int EnvDataSampleLen;
    /// <summary>
    /// Dictionary with device codes defined in DDD and corresponding device names
    /// </summary>
    public Dictionary<int, String> DeviceNames;
    /// <summary>
    /// Dictionary with fault codes defined in DDD and corresponding record definition IDs
    /// </summary>
    public Dictionary<int, int> RecordDefIDs;
    #endregion //Public members
  }

  /// <summary>
  /// Represents one instance of telediagnostic unit which corresponds with alstom train
  /// </summary>
  public class AlstomTUInstnace
  {
    #region Public members
    /// <summary>
    /// Number of Alstom train number associated with this TU Instance
    /// </summary>
    public int TrainNumber;
    /// <summary>
    /// Name of the instance in CDDB database
    /// </summary>
    public String TUInstanceName;
    /// <summary>
    /// ID of the instance in CDDB database
    /// </summary>
    public Guid TUInstanceID;
    /// <summary>
    /// UserVersion of DDD configured for this instance
    /// </summary>
    public String DDDVersionName;
    #endregion //Public members
  }

}
