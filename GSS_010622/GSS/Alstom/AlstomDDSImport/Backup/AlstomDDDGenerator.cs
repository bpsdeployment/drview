using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Collections;
using System.Globalization;
using System.Xml;
using TDManagementForms;
using System.Security.Cryptography;



namespace AlstomDDSImport
{
  public enum ImportDDDVerResult : int
  {// errors enumeration
    Error = -1,  // error 
    OK = 0,  // DDDVersions successfully imported 
    TUTypeNotExist = 1,  // TUType not exists
    DDDVerExists = 2,  // DDDVersion already exists
    CfgFileConvErr = 3,  // Input cfg. file error 
    DDDVerNotRegDB = 4,  // DDDVersion registration in database error 
    AlstCfgObjErr = 5   // Alstom Cfg Object error
  };

  public class ConfiConvDDD
  {
    private Dictionary<String, string> faultCodeDict;
    private Dictionary<String, int> deviceSubsDict;
    private Dictionary<String, int> deviceFuncDict;
    private Dictionary<String, string> deviceCodeDict;
    private Dictionary<String, int> represNameDict;
    private Dictionary<String, string> varOffsetDict;
    public int envDataSampleLen = 0;
    public string samplesBeforeStart;
    public string samplesAfterStart;
    public string samplingPeriod;
    public int isamplingPeriod;
    public string DDSRecordLength;
    Guid DDDVersionID;// = new Guid();
    SqlConnection sqlConnection = new SqlConnection();
    private NumberFormatInfo numberFormatInfo; //Number format info object
    private NumberFormatInfo sourceNumberFormatInfo;

    public ConfiConvDDD()
    {
      envDataSampleLen = 0;
      samplesBeforeStart = Properties.Settings.Default.SamplesBeforeStart;// "5"
      samplesAfterStart = Properties.Settings.Default.SettingSamplesAfterStart;//"6"
      samplingPeriod = Properties.Settings.Default.SamplingPeriod; //"10" SamplingPeriod is 1/10s 
      isamplingPeriod = Convert.ToInt32(samplingPeriod) * 100; //attribute SamplingPeriod is 1/10s isamplingPeriod=SamplingPeriod/10 then convert to ms isamplingPeriod=isamplingPeriod*1000
      samplingPeriod = isamplingPeriod.ToString();
      DDSRecordLength = Properties.Settings.Default.DDSRecordLength;
      string connectionString = Properties.Settings.Default.CDDBConnString;
      sqlConnection.ConnectionString = connectionString;
      DDDVersionID = new Guid();
      // Modify by Romeo 2011-02-22
      //Create number format info
      numberFormatInfo = new NumberFormatInfo();
      numberFormatInfo.NumberDecimalSeparator = Properties.Settings.Default.NumberDecimalSeparator; // ".";
      sourceNumberFormatInfo = new NumberFormatInfo();
      sourceNumberFormatInfo.NumberDecimalSeparator = Properties.Settings.Default.SourceNumberDecimalSeparator; // ".";
      sourceNumberFormatInfo.NumberGroupSeparator = Properties.Settings.Default.SourceNumberGroupSeparator; // "";
  }

    public int SqlIni(Logger logger)
    {
      StringBuilder errorMessage = new StringBuilder();
      errorMessage.Append("Sql connection error");
      //  database connection
      try
      {
        if (sqlConnection.State != ConnectionState.Open)
        {
          sqlConnection.Close();
          sqlConnection.Open();
        }
      }
      catch (InvalidOperationException)
      {
        logger.LogText(3, "AlstDDDGen", errorMessage.ToString());
        return -1;

      }
      catch (SqlException Sqlex)
      {
        for (int i = 0; i < Sqlex.Errors.Count; i++)
        {
          errorMessage.Append("\n" + Sqlex.Errors[i].Message);
        }
        logger.LogText(3, "AlstDDDGen", errorMessage.ToString());
        return -1;
      }
      return 0;
    }

    /// <summary>
    /// Converts ConfigIT data type to DDD data type 
    /// </summary>
    /// <param name="configDataType">ConfigIT data type</param>
    /// <param name="configLen">variable length</param>
    /// <returns>DDD data type</returns>
    public string getDDDDataType(string configDataType, string configLen)
    {
      string DDDDataType = "U8";
      if (configDataType == "ANA")
        switch (configLen)
        {
          case "1": DDDDataType = "U8"; break;
          case "2": DDDDataType = "U16"; break;
          case "3":
          case "4": DDDDataType = "U32"; break;
          case "5":
          case "6":
          case "7":
          case "8": DDDDataType = "U64"; break;
          default: DDDDataType = "U64"; break;
        }
      if (configDataType == "DIG")
        DDDDataType = "U8";
    if (configDataType == "MDIG")
        DDDDataType = "U8";
    if (configDataType == "ASCII" || configDataType == "STRING")
        DDDDataType = "ASCII";

      return DDDDataType;
    }
    /// <summary>
    /// Converts bit variable position in the config file to mask
    /// </summary>
    /// <param name="configPos">Config position</param>
    /// <returns>DDD mask</returns>
    public string getDDDMask(string configPos)
    {
      string DDDMask = "1";
      ushort u16DDDMask = 1;
      ushort u16ConfigPos = Convert.ToUInt16(configPos);
      u16ConfigPos--;
      u16DDDMask = (ushort)(u16DDDMask << u16ConfigPos);
      DDDMask = u16DDDMask.ToString();
      return DDDMask;
    }

    /// <summary>
    /// Gets name o Representation depend on it's type
    /// </summary>
    /// <param name="element">XML element</param>
    /// <param name="represType">type of repres 0-simple 1- bitset</param>
    /// <param name="confVarType">type of configIT file variable 0-Spare Data  1- Environment Data</param>
    /// <returns>representation name</returns>
    public string getRepresName(XmlElement element, byte represType, byte confVarType)
    {
      string represName = "";
      if (represType == 0)
      {//Simple Representation
        if (confVarType == 0) //from Spare Date 
          represName = "DT:" + element.GetAttribute("Type") + "_L:1_" + "U:" + element.GetAttribute("Unit") + '_' + "B:" + element.GetAttribute("Bias") + '_' + "M:" + element.GetAttribute("Mult") + '_' + "F:" + element.GetAttribute("Format");

        if (confVarType == 1)//from Environment Date
          represName = "DT:" + element.GetAttribute("Type") + "_L:" + element.GetAttribute("Length") + "_U:" + element.GetAttribute("Unit") + "_B:" + element.GetAttribute("Bias") + "_M:" + element.GetAttribute("Mult") + "_F:" + element.GetAttribute("Format");
      }
      if (represType == 1)
      { //Bitset Representation 
          if (confVarType == 0) //from Spare Date 
              represName = "DT:" + element.GetAttribute("Type") + "_L:1_" + "Sh:" + element.GetAttribute("Short");

          if (confVarType == 1)//from Environment Date
              represName = "DT:" + element.GetAttribute("Type") + "_L:" + element.GetAttribute("Length") + '_' + "Sh:" + element.GetAttribute("Short");
      }
      if (represType == 2)
      { //Binary Representation 
          if (confVarType == 0) //from Spare Date 
              represName = "DT:" + element.GetAttribute("Type") + "_L:1_" + "Sh:" + element.GetAttribute("Short");

          if (confVarType == 1)//from Environment Date
              represName = "DT:" + element.GetAttribute("Type") + "_L:" + element.GetAttribute("Length") + '_' + "Sh:" + element.GetAttribute("Short");
      }
      return represName;
    }

    protected String GetFileAsXmlString(XmlDocument xmlDoc, bool withDecl)
    {
      String fileContent;
      if (!withDecl)
      {
        if (xmlDoc.FirstChild.GetType() == typeof(XmlDeclaration)) xmlDoc.RemoveChild(xmlDoc.FirstChild);
      }
      //Return xml as string
      fileContent = xmlDoc.OuterXml;
      return fileContent;
    }

    
    /// <summary>
    /// inserts  new DDDVersion to database
    /// </summary>
    /// <param name="outXmlDoc">DDD Version Xml document</param>
    /// <param name="logger">logger</param>
    /// <returns>0-OK, 4 - registration to DB error  </returns>
    public ImportDDDVerResult addDDDVerToDB(XmlDocument outXmlDoc, Logger logger)
    {
      logger.LogText(2, "AlstDDDGen", "Insertion of new DDD Version to database");
      //using (StreamWriter sw = new StreamWriter(Path.Combine(Properties.Settings.Default.DDSBackDir, "DDD.xml")))
      //{
      //    sw.Write(outXmlDoc.OuterXml);
      //}
      ImportDDDVerResult retVal = ImportDDDVerResult.OK;//0;
      int result = SqlIni(logger);
      if (result == -1) return ImportDDDVerResult.Error;

      //SQL command for ImportTUConfig stored procedure
      SqlCommand sqlCommand = new SqlCommand();
      sqlCommand.Connection = sqlConnection;
      sqlCommand.CommandTimeout = Properties.Settings.Default.SQLTimeout;
      sqlCommand.CommandType = CommandType.StoredProcedure;

      //stored procedure ImportTUConfig parameters
      //parameter @DDDFileName
      string DDDFileName = "DDD.xml";

      //logger.LogText(3, "AlstDDDGen", "Param DDDFileName: {0}", DDDFileName);
      SqlParameter DDDFileNameSQLParameter = new SqlParameter("@DDDFileName",SqlDbType.NVarChar,50);
      DDDFileNameSQLParameter.IsNullable = false;
      DDDFileNameSQLParameter.Direction = ParameterDirection.Input;
      DDDFileNameSQLParameter.Value = DDDFileName;
      sqlCommand.Parameters.Add(DDDFileNameSQLParameter);

      //parameter @DDDFile
      string sXml = GetFileAsXmlString(outXmlDoc, true);// string Xml with declaration 
      byte[] DDDFile =  Encoding.UTF8.GetBytes(sXml);
      
      SqlParameter DDDFileSQLParameter = new SqlParameter("@DDDFile", SqlDbType.Image);
      DDDFileSQLParameter.IsNullable = false;
      DDDFileSQLParameter.Direction = ParameterDirection.Input;
      DDDFileSQLParameter.Value = DDDFile;
      sqlCommand.Parameters.Add(DDDFileSQLParameter);
      
      //parameter @DDDFileXml
      string DDDFileXml = GetFileAsXmlString(outXmlDoc, false); // string Xml without declaration 
      
      SqlParameter DDDFileXmlSQLParameter = new SqlParameter("@DDDFileXml", SqlDbType.Xml);
      DDDFileXmlSQLParameter.IsNullable = false;
      DDDFileXmlSQLParameter.Direction = ParameterDirection.Input;
      DDDFileXmlSQLParameter.Value = DDDFileXml;
      sqlCommand.Parameters.Add(DDDFileXmlSQLParameter);
            
      //parameter @DDDSignature
      MD5 md5 = new MD5CryptoServiceProvider();
      byte[] MD5Signature = md5.ComputeHash(DDDFile);

      //logger.LogText(3, "AlstDDDGen", "Length of MD5Signature: {0}", MD5Signature.GetLength(0).ToString());
      SqlParameter DDDSignatureSQLParameter = new SqlParameter("@DDDSignature", SqlDbType.Binary, 16);
      DDDSignatureSQLParameter.IsNullable = false;
      DDDSignatureSQLParameter.Direction = ParameterDirection.Input;
      DDDSignatureSQLParameter.Value = MD5Signature;
      sqlCommand.Parameters.Add(DDDSignatureSQLParameter);

      //parameter @TaskFileName
      SqlParameter taskFileNameSQLParameter = new SqlParameter("@TaskFileName", SqlDbType.NVarChar, 50);
      taskFileNameSQLParameter.IsNullable = true;
      taskFileNameSQLParameter.Direction = ParameterDirection.Input;
      taskFileNameSQLParameter.Value = null;
      sqlCommand.Parameters.Add(taskFileNameSQLParameter);
      //parameter @TaskFile
      SqlParameter taskFileSQLParameter = new SqlParameter("@TaskFile", SqlDbType.Image);
      taskFileSQLParameter.IsNullable = true;
      taskFileSQLParameter.Direction = ParameterDirection.Input;
      taskFileSQLParameter.Value = null;
      sqlCommand.Parameters.Add(taskFileSQLParameter);
      //parameter @TaskSignature
      SqlParameter taskSignatureSQLParameter = new SqlParameter("@TaskSignature", SqlDbType.Binary, 16);
      taskSignatureSQLParameter.IsNullable = true;
      taskSignatureSQLParameter.Direction = ParameterDirection.Input;
      taskSignatureSQLParameter.Value = null;
      sqlCommand.Parameters.Add(taskSignatureSQLParameter);

      //parameter @LibraryFileName
      SqlParameter libraryFileNameSQLParameter = new SqlParameter("@LibraryFileName", SqlDbType.NVarChar, 50);
      libraryFileNameSQLParameter.IsNullable = true;
      libraryFileNameSQLParameter.Direction = ParameterDirection.Input;
      libraryFileNameSQLParameter.Value = null;
      sqlCommand.Parameters.Add(libraryFileNameSQLParameter);
      //parameter @LibraryFile
      SqlParameter libraryFileSQLParameter = new SqlParameter("@LibraryFile", SqlDbType.Image);
      libraryFileSQLParameter.IsNullable = true;
      libraryFileSQLParameter.Direction = ParameterDirection.Input;
      libraryFileSQLParameter.Value = null;
      sqlCommand.Parameters.Add(libraryFileSQLParameter);
      //parameter @LibrarySignature
      SqlParameter librarySignatureSQLParameter = new SqlParameter("@LibrarySignature", SqlDbType.Binary, 16);
      librarySignatureSQLParameter.IsNullable = true;
      librarySignatureSQLParameter.Direction = ParameterDirection.Input;
      librarySignatureSQLParameter.Value = null;
      sqlCommand.Parameters.Add(librarySignatureSQLParameter);

      //parameter @SourceFileName
      SqlParameter sourceFileNameSQLParameter = new SqlParameter("@SourceFileName", SqlDbType.NVarChar, 50);
      sourceFileNameSQLParameter.IsNullable = true;
      sourceFileNameSQLParameter.Direction = ParameterDirection.Input;
      sourceFileNameSQLParameter.Value = null;
      sqlCommand.Parameters.Add(sourceFileNameSQLParameter);
      //parameter @SourceFile
      SqlParameter sourceFileSQLParameter = new SqlParameter("@SourceFile", SqlDbType.Image);
      sourceFileSQLParameter.IsNullable = true;
      sourceFileSQLParameter.Direction = ParameterDirection.Input;
      sourceFileSQLParameter.Value = null;
      sqlCommand.Parameters.Add(sourceFileSQLParameter);
      //parameter @SourceSignature
      SqlParameter sourceSignatureSQLParameter = new SqlParameter("@SourceSignature", SqlDbType.Binary, 16);
      sourceSignatureSQLParameter.IsNullable = true;
      sourceSignatureSQLParameter.Direction = ParameterDirection.Input;
      sourceSignatureSQLParameter.Value = null;
      sqlCommand.Parameters.Add(sourceSignatureSQLParameter);

      //parameter @DDDVersionID
      SqlParameter DDDVersionIDSQLParameter = new SqlParameter("@DDDVersionID", SqlDbType.UniqueIdentifier);
      DDDVersionIDSQLParameter.Direction = ParameterDirection.Output;
      sqlCommand.Parameters.Add(DDDVersionIDSQLParameter);

      //logger.LogText(3, "AlstDDDGen", "GO");
      sqlCommand.CommandText = "ImportTUConfig";
      try
      {
         sqlCommand.ExecuteXmlReader();
         DDDVersionID = (Guid)DDDVersionIDSQLParameter.Value;
         logger.LogText(3, "AlstDDDGen", "Sql command execution: ImportTUConfig (DDDVersionID=" + DDDVersionID.ToString() + ")");
     }
      catch (Exception ex)
      {
        logger.LogText(3, "AlstDDDGen", "Sql command error, " + ex.Message);
        return ImportDDDVerResult.DDDVerNotRegDB;//4;
      }

      sqlConnection.Close();
      return retVal;
    }

    /// <summary>
    /// adds new DDD version of given TUType to AlstomTUTypes configuration file and alstom cfg object
    /// </summary>
    /// <param name="dbTUTypeName"> DB name of TUtype </param>
    /// <param name="dbDDDName">DB name of DDDversion </param>
    /// <param name="AlstConfNumberr"> Alstom configuration number</param>
    /// <param name="logger">logger</param>
    /// <returns> 0 if  DDD version was inserted, 1 if DDD version couldn't be inserted, -1 if file error occured</returns>
    public ImportDDDVerResult addDDDVerToCfg(string dbTUTypeName, string dbDDDName, string AlstConfNumber, AlstomCfg alstomCfg, Logger logger)
    {
      logger.LogText(2, "AlstDDDGen", "Insertion of new DDD Version to TUTypes configuration file");
      ImportDDDVerResult retVal = ImportDDDVerResult.OK;
      string AlstomTUTypeName; 
      //create input XML Document;
      XmlDocument XmlDoc = new XmlDocument();
      string configTUTypesFileName = Properties.Settings.Default.ConfigFileName;
      try
      {
        XmlDoc.Load(configTUTypesFileName);
      }
      catch (Exception e)
      {
        logger.LogText(3, "AlstDDDGen", "Error of TUTypes configuration file {0} " + e.Message, configTUTypesFileName);
        return ImportDDDVerResult.Error;//-1;
      }
      XmlNodeList elemList;
      bool tUtypeFound = false;
      elemList = XmlDoc.SelectNodes("//AlstomTUTypes/TUType");
      foreach (XmlElement elem in elemList)
      {//throught TUTypes\
        if (dbTUTypeName == elem.GetAttribute("Name"))
        {// TUType name is found in TUTypes config. file
          AlstomTUTypeName = elem.GetAttribute("AlstNumber");
          tUtypeFound = true;
          // DDDVersion in the given TUType doesn't exists -> insert it
          XmlElement dDDElem = XmlDoc.CreateElement("DDDVer");
          dDDElem.SetAttribute("Name", dbDDDName);
          dDDElem.SetAttribute("AlstNumber", AlstConfNumber);
          dDDElem.SetAttribute("SamplesBeforeStart", samplesBeforeStart);
          dDDElem.SetAttribute("SamplesAfterStart", samplesAfterStart);
          dDDElem.SetAttribute("SamplingPeriod", samplingPeriod);
          dDDElem.SetAttribute("DDSRecordLength", DDSRecordLength);
          dDDElem.SetAttribute("EnvDataSampleLen", envDataSampleLen.ToString());
          elem.AppendChild(dDDElem);//add DDDVersion element to TUType element
          // add DDDVersion to AlstomCfg Object
          ImportDDDVerResult res = addDDDVerToCfgObj(AlstomTUTypeName, dbDDDName, AlstConfNumber, alstomCfg, logger);
          // if everything is OK saves config file
          if (retVal == ImportDDDVerResult.OK || retVal == ImportDDDVerResult.DDDVerExists)
          {
            XmlDoc.Save(configTUTypesFileName);
          }
          else retVal = res;
          return retVal;
        }// TUType name is found in TUTypes config. file
      }
      if (!tUtypeFound)
      {
        logger.LogText(3, "AlstDDDGen", "Error-TUType:{0} wasn't found in the TUTypes configuration file", dbTUTypeName);
        return ImportDDDVerResult.TUTypeNotExist;
      }
      return ImportDDDVerResult.OK;
    }


    /// <summary>
    /// adds new DDD version of given TUType to AlstomCfg Object
    /// </summary>
    /// <param name="AlstomTUTypeName"> Alstom type number</param>
    /// <param name="userVersion"> DB DDDVersion name </param>
    /// <param name="alstDDDVerName">Alstom DDDVersion number</param>
    /// <param name="alstomCfg"> AlstomCfg Object</param>
    /// <param name="logger">logger</param>
    /// <returns>
    /// </returns>
    public ImportDDDVerResult addDDDVerToCfgObj(string AlstomTUTypeName, string userVersion, string alstDDDVerName, AlstomCfg alstomCfg, Logger logger)
    {
      logger.LogText(2, "AlstDDDGen", "Insertion of new DDD Version to AlstomCfg object");
      ImportDDDVerResult retVal = ImportDDDVerResult.OK;//0;
      int result = SqlIni(logger);
      if (result == -1) return ImportDDDVerResult.Error;
      #region AlstomCFG objects
      if (alstomCfg.TUTypes.ContainsKey(AlstomTUTypeName))
      {// Train type exists
        AlstomTUType alstomTUType;
        alstomCfg.TUTypes.TryGetValue(AlstomTUTypeName, out alstomTUType);
        string TUTypeName = alstomTUType.TUTypeName;
        //##!!!!! testovani    if (alstomTUType.DDDVersions.ContainsKey(alstDDDVerName)) alstomTUType.DDDVersions.Remove(alstDDDVerName);
        if (!alstomTUType.DDDVersions.ContainsKey(alstDDDVerName))
        { //DDDVersion version not exists in the object
          /* version ID from DB, global variable DDDVersionID set by addDDDVerToDB procedure */
          // add new DDD Version to Cfg object
          AlstomDDDVersion alstomDDDVersion = new AlstomDDDVersion();
          alstomDDDVersion.DeviceNames = new Dictionary<int, string>();
          alstomDDDVersion.RecordDefIDs = new Dictionary<int, int>();
          //DDD version parameters
          alstomDDDVersion.UserVersion = userVersion;
          alstomDDDVersion.AlstomVersionName = alstDDDVerName;
          alstomDDDVersion.SamplesAfterStart = Convert.ToInt32(samplesAfterStart);
          alstomDDDVersion.SamplesBeforeStart = Convert.ToInt32(samplesBeforeStart);
          alstomDDDVersion.SamplingPeriod = Convert.ToInt32(samplingPeriod);
          alstomDDDVersion.DDSRecordLength = Convert.ToInt32(DDSRecordLength);
          alstomDDDVersion.EnvDataSampleLen = envDataSampleLen;
          alstomDDDVersion.DDDVersionID = DDDVersionID;
          
          /* dictionary of devices*/
          SqlCommand devicesSqlCommand = new SqlCommand();
          devicesSqlCommand.Connection = sqlConnection;
          devicesSqlCommand.CommandTimeout = Properties.Settings.Default.SQLTimeout;
          devicesSqlCommand.CommandType = CommandType.Text;

          SqlParameter inpDDDVersionIDSQLParameter = new SqlParameter();
          inpDDDVersionIDSQLParameter.ParameterName = "@DDDVersionID";
          inpDDDVersionIDSQLParameter.IsNullable = false;
          inpDDDVersionIDSQLParameter.SqlDbType = SqlDbType.UniqueIdentifier;
          inpDDDVersionIDSQLParameter.Direction = ParameterDirection.Input;
          inpDDDVersionIDSQLParameter.Value = DDDVersionID;
          devicesSqlCommand.Parameters.Add(inpDDDVersionIDSQLParameter);
          devicesSqlCommand.CommandText = "select Code, [Name] from dbo.DeviceCodes where VersionID= @DDDVersionID";
          SqlDataReader reader = null;
          StringBuilder errorMessage = new StringBuilder();
          errorMessage.Append("SQL command error");
          try
          {
            reader = devicesSqlCommand.ExecuteReader();
          }
          catch (SqlException Sqlex)
          {
            for (int i = 0; i < Sqlex.Errors.Count; i++)
            {
              errorMessage.Append("\n" + Sqlex.Errors[i].Message);
            }
            logger.LogText(3, "AlstDDDGen", errorMessage.ToString() + ", list of devices");
            retVal = ImportDDDVerResult.AlstCfgObjErr;
          }
          int count = 0;
          int deviceCode;
          string deviceName;
          while (reader.Read())
          {
            //Add devices to dictionary
            deviceCode = reader.GetInt16(0);
            deviceName = reader.GetString(1);
            alstomDDDVersion.DeviceNames.Add(deviceCode, deviceName);
            count++;
          }
          logger.LogText(2, "AlstDDDGen", "TUType name: {0}, DDDVersion: {1} - {2} Devices", TUTypeName, userVersion, count);
          reader.Close();

          /* dictionary of RecordDefIDs*/
          SqlCommand recordDefIDsSqlCommand = new SqlCommand();
          recordDefIDsSqlCommand.Connection = sqlConnection;
          recordDefIDsSqlCommand.CommandTimeout = Properties.Settings.Default.SQLTimeout;
          recordDefIDsSqlCommand.CommandType = CommandType.Text;

          SqlParameter inpDDDVersionIDRDSQLParameter = new SqlParameter();
          inpDDDVersionIDRDSQLParameter.ParameterName = "@DDDVersionID";
          inpDDDVersionIDRDSQLParameter.IsNullable = false;
          inpDDDVersionIDRDSQLParameter.SqlDbType = SqlDbType.UniqueIdentifier;
          inpDDDVersionIDRDSQLParameter.Direction = ParameterDirection.Input;
          inpDDDVersionIDRDSQLParameter.Value = DDDVersionID;
          recordDefIDsSqlCommand.Parameters.Add(inpDDDVersionIDRDSQLParameter);
          string commandText = " select FaultCode, RecordDefID from RecordDefinitions " +
                             " inner join RecordTypes  On RecordTypes.RecordTypeID=RecordDefinitions.RecordTypeID " +
                             " where VersionID= @DDDVersionID And FaultCode Is Not Null";

          recordDefIDsSqlCommand.CommandText = commandText;
          reader = null;
          try
          {
            reader = recordDefIDsSqlCommand.ExecuteReader();
          }
          catch (SqlException Sqlex)
          {
            for (int i = 0; i < Sqlex.Errors.Count; i++)
            {
              errorMessage.Append("\n" + Sqlex.Errors[i].Message);
            }
            logger.LogText(3, "AlstDDDGen", errorMessage.ToString());
            retVal = ImportDDDVerResult.AlstCfgObjErr;//5;
          }
          count = 0;
          while (reader.Read())
          {
            //Add devices to dictionary
            alstomDDDVersion.RecordDefIDs.Add(reader.GetInt32(0), reader.GetInt32(1));
            count++;
          }
          logger.LogText(2, "AlstDDDGen", "TUType name: {0}, DDDVersion: {1} - {2} Fault codes", TUTypeName, userVersion, count);
          reader.Close();

          // DDDversion to TUType dictionary
          alstomTUType.DDDVersions.Add(alstDDDVerName, alstomDDDVersion);
        }//DDDVersion version not exists in the object
        else
        {
          logger.LogText(3, "AlstDDDGen", "Error-DDD Version {0} already exists in AlstomCfg object ", userVersion);
          return ImportDDDVerResult.DDDVerExists;
        }
      }// Train type exists
      else
      {// Train type not exists
        logger.LogText(3, "AlstDDDGen", "Error-TUType:{0} wasn't found in AlstomCfg object", AlstomTUTypeName);
        return ImportDDDVerResult.TUTypeNotExist;
      }
      #endregion //AlstomCFG objects
      sqlConnection.Close();
      return retVal;
    }

    /// <summary>
    ///  gets  TUType name, checks whether TUType or  DDD version exists in AlstomTUTypes configuration file
    /// </summary>
    /// <param name="AlstTUTypeNb">Alstom TUType number</param>
    /// <param name="AlstConfNb">Alstom config. file version</param>
    /// <param name="sTUType"> output of TUType name </param>
    /// <param name="logger">logger</param>
    /// <returns> 0 TUType was found and DDDVersin wasn't found in the config. file;  
    ///           1 TUType wasn'found in the TUTypes configuration file; 
    ///           2 DDD Version already exists in the TUTypes configuration file; 
    ///          -1 file error occured</returns>
    public ImportDDDVerResult getTUType(string AlstTUTypeNb, string AlstConfNb, ref string sTUType, Logger logger)
    {
      logger.LogText(2, "AlstDDDGen", "Check of TUType:{0} and DDD Version:{1} during automatic DDD Version import", AlstTUTypeNb, AlstConfNb);
      
      //create input XML Document;
      XmlDocument XmlDoc = new XmlDocument();
      string configTUTypesFileName = Properties.Settings.Default.ConfigFileName;
      try
      {
        XmlDoc.Load(configTUTypesFileName);
      }
      catch (Exception e)
      {
        logger.LogText(2, "AlstDDDGen", "Error of TUTypes configuration file {0} " + e.Message, configTUTypesFileName);
        return ImportDDDVerResult.Error; ;
      }
      XmlNodeList elemList;
      XmlNodeList childElemList;
      bool tUtypeFound = false;
      elemList = XmlDoc.SelectNodes("//AlstomTUTypes/TUType");
      // check TUType
      foreach (XmlElement elem in elemList)
      {//throught TUTypes\
        string sAlstNumber = elem.GetAttribute("AlstNumber");
        sAlstNumber = sAlstNumber.Trim();
        sAlstNumber = sAlstNumber.PadLeft(2, '0');
        if (AlstTUTypeNb == sAlstNumber)
        {// TUType Alstom number is found in TUTypes config. file
          // get TUType string from config. file
          string sTUTypeName = elem.GetAttribute("Name");
          sTUType = sTUTypeName.Substring(0, sTUTypeName.IndexOf('_'));
          tUtypeFound = true;
          //DDDVersion
          childElemList = elem.ChildNodes;
          // check DDDversion in given TUType
          foreach (XmlElement childElem in childElemList)
          {//throught DDDVersions 
            if (childElem.LocalName == "DDDVer")
            {//DDDVer
              string alstDDDVerName = childElem.GetAttribute("AlstNumber");
              alstDDDVerName = alstDDDVerName.Trim();
              alstDDDVerName = alstDDDVerName.PadLeft(10, '0');
              if (AlstConfNb == alstDDDVerName)
              {
                logger.LogText(2, "AlstDDDGen", "Error-DDD Version {0} already exists in the TUTypes configuration file ", AlstConfNb);
                return ImportDDDVerResult.DDDVerExists; ;
              }
            }//DDDVer
          }
        }// TUType name is found in TUTypes config. file
      }
      if (!tUtypeFound)
      {
        logger.LogText(3, "AlstDDDGen", "Error-TUType:{0} wasn't found in the TUTypes configuration file", AlstTUTypeNb);
        return ImportDDDVerResult.TUTypeNotExist ;
      }
      return ImportDDDVerResult.OK;
    }

   
    /// <summary>
    /// Converts Config XML file to DDD XML file 
    /// </summary>
    /// <param name="inpXmlDc">input XML file </param>
    /// <param name="outXmlDc">output XML file</param>
    /// <returns> error </returns>
    public ImportDDDVerResult convConfigToDDD(XmlDocument inpXmlDoc, XmlDocument outXmlDoc, string TUType, Logger logger)
    {
      //lists
      represNameDict = new Dictionary<string, int>();//list of representations 
      varOffsetDict = new Dictionary<string, string>();//list of variables 
      faultCodeDict = new Dictionary<string, string>();//list of fault codes 
      deviceSubsDict = new Dictionary<string, int>();//list of devices in the Subsystem
      deviceFuncDict = new Dictionary<string, int>();//list of devices in the Function
      deviceCodeDict = new Dictionary<string, string>();//list of devices in the DeviceCodes

      StringBuilder errorMessage = new StringBuilder();
      logger.LogText(2, "AlstDDDGen", "Converting of Alstom configuration file to DDD Version file, TUType: {0}", TUType);

      XmlElement outElem;
      XmlNodeList elemList;
      XmlNodeList childElemList;
      XmlElement binaryRepresElem;
      XmlNodeList binaryRepresElemList;
      XmlElement bitsetRepresElem;
      XmlNodeList bitsetRepresElemList;
      string configDataType;

      #region Representations
      XmlElement rootElem;
      rootElem = (XmlElement)outXmlDoc.SelectSingleNode("//DDD");
      // Representation
      XmlElement repElem = outXmlDoc.CreateElement("Representations");
      rootElem.AppendChild(repElem);

      //Representation for Spare Data
      elemList = inpXmlDoc.SelectNodes("//Impianti/SpareData/Spare");
      foreach (XmlElement elem in elemList)
      {//throught Spare Data
        //throught attributes
        configDataType = elem.GetAttribute("Type");
        string attribValue;
        double doubAttribValue = 0;
        if (configDataType == "ASCII" || configDataType == "STRING")
        {//BinaryRepres
            attribValue = getRepresName(elem, 2, 1);
            if (!represNameDict.ContainsKey(attribValue))
            {
                binaryRepresElem = outXmlDoc.CreateElement("BinaryRepres");//output element
                represNameDict.Add(attribValue, 1); // name to dictionary
                // attribute Name
                binaryRepresElem.SetAttribute("Name", attribValue);
                // attribute Length
                binaryRepresElem.SetAttribute("Length", elem.GetAttribute("Length"));
                // attribute Format
                attribValue = getDDDDataType(configDataType, elem.GetAttribute("Length"));
                binaryRepresElem.SetAttribute("Format", attribValue);
                // attribute Comment
                binaryRepresElem.SetAttribute("Comment", "Ascii representation for " + elem.GetAttribute("Name") + " variable");
                repElem.AppendChild(binaryRepresElem);
            }
        }//BinaryRepres
        else if (configDataType == "MDIG")
        {//BitsetRepres
          attribValue = getRepresName(elem, 1, 0);
          // attribValue = "DT:" + elem.GetAttribute("Type") + "_L:1_" + "Sh:" + elem.GetAttribute("Short");
          if (!represNameDict.ContainsKey(attribValue))
          {
            bitsetRepresElem = outXmlDoc.CreateElement("BitsetRepres");//output element
            represNameDict.Add(attribValue, 1); // name to dictionary
            // attribute Name
            bitsetRepresElem.SetAttribute("Name", attribValue);
            // attribute DataTypeName
            attribValue = getDDDDataType(configDataType, "1");
            bitsetRepresElem.SetAttribute("DataTypeName", attribValue);
            // attribute Comment
            bitsetRepresElem.SetAttribute("Comment", "Representation for " + elem.GetAttribute("Name") + " variable");
            repElem.AppendChild(bitsetRepresElem);
            //Bits
            bitsetRepresElemList = inpXmlDoc.SelectNodes("//Impianti/SpareData/Spare/SpareBit");
            foreach (XmlElement bSRepElem in bitsetRepresElemList)
            {//throught Bit Spare Data
              outElem = outXmlDoc.CreateElement("BitRepres");//output element
              outElem.SetAttribute("Language", "english");
              outElem.SetAttribute("Mask", getDDDMask(bSRepElem.GetAttribute("Pos")));
              outElem.SetAttribute("Text", bSRepElem.GetAttribute("Name"));
              bitsetRepresElem.AppendChild(outElem);
            }
          }
        }//BitsetRepres
        else
        {
          //SimpleRepres
          attribValue = getRepresName(elem, 0, 0);
          if (!represNameDict.ContainsKey(attribValue))
          {
            outElem = outXmlDoc.CreateElement("SimpleRepres");//output element
            // attribute Name
            represNameDict.Add(attribValue, 1);//add name to dictionary
            outElem.SetAttribute("Name", attribValue);
            // attribute DataTypeName
            attribValue = getDDDDataType(configDataType, "1");
            outElem.SetAttribute("DataTypeName", attribValue);
            // attribute UnitSymbol
            outElem.SetAttribute("UnitSymbol", elem.GetAttribute("Unit"));
            // attribute Divisor
            attribValue = elem.GetAttribute("Mult");
            attribValue = attribValue.Trim();
            if (!String.IsNullOrEmpty(attribValue))
            {//count Divisor = 1/Mult
                // Modify by Romeo 2011-02-22
                doubAttribValue = Convert.ToDouble(attribValue, sourceNumberFormatInfo);
                doubAttribValue = 1 / doubAttribValue;
                // Modify by Romeo 2011-02-22
                //logger.LogText(2, "AlstDDDGen", "Converting float, Mult: {0}  Div: {1}", attribValue, doubAttribValue.ToString(numberFormatInfo));
                attribValue = doubAttribValue.ToString(numberFormatInfo);
                outElem.SetAttribute("Divisor", attribValue);
            }

            // attribute Format
            attribValue = elem.GetAttribute("Format");
            outElem.SetAttribute("Format", attribValue);
            // attribute Bias
            attribValue = elem.GetAttribute("Bias");
            attribValue = attribValue.Trim();
            if (!String.IsNullOrEmpty(attribValue))
            {//if Bias exists
              outElem.SetAttribute("Bias", attribValue);
            }
            // add SimpleRepres element
            repElem.AppendChild(outElem);
          }
        }//SimpleRepres

      }//Spare Data

      //Representation for Environment Data
      elemList = inpXmlDoc.SelectNodes("//Impianti/EnvironmentData/Environment");
      foreach (XmlElement elem in elemList)
      {//throught Environment Data
          //throught attributes
          configDataType = elem.GetAttribute("Type");
          string attribValue;
          double doubAttribValue = 0;
          if (configDataType == "ASCII" || configDataType == "STRING")
          {//BinaryRepres
              attribValue = getRepresName(elem, 2, 1);
              if (!represNameDict.ContainsKey(attribValue))
              {
                  binaryRepresElem = outXmlDoc.CreateElement("BinaryRepres");//output element
                  represNameDict.Add(attribValue, 1); // name to dictionary
                  // attribute Name
                  binaryRepresElem.SetAttribute("Name", attribValue);
                  // attribute Length
                  binaryRepresElem.SetAttribute("Length", elem.GetAttribute("Length"));
                  // attribute Format
                  attribValue = getDDDDataType(configDataType, elem.GetAttribute("Length"));
                  binaryRepresElem.SetAttribute("Format", attribValue);
                  // attribute Comment
                  binaryRepresElem.SetAttribute("Comment", "Ascii representation for " + elem.GetAttribute("Name") + " variable");
                  repElem.AppendChild(binaryRepresElem);
              }
          }//BinaryRepres
          else if (configDataType == "MDIG")
          {//BitsetRepres
              attribValue = getRepresName(elem, 1, 1);
              if (!represNameDict.ContainsKey(attribValue))
              {
                  bitsetRepresElem = outXmlDoc.CreateElement("BitsetRepres");//output element
                  represNameDict.Add(attribValue, 1); // name to dictionary
                  // attribute Name
                  bitsetRepresElem.SetAttribute("Name", attribValue);
                  // attribute DataTypeName
                  attribValue = getDDDDataType(configDataType, elem.GetAttribute("Length"));
                  bitsetRepresElem.SetAttribute("DataTypeName", attribValue);
                  // attribute Comment
                  bitsetRepresElem.SetAttribute("Comment", "Representation for " + elem.GetAttribute("Name") + " variable");
                  repElem.AppendChild(bitsetRepresElem);
                  //Bits
                  //chyba:  bitsetRepresElemList = inpXmlDoc.SelectNodes("//Impianti/EnvironmentData/Environment/EnvironmentBit");
                  bitsetRepresElemList = elem.ChildNodes;
                  foreach (XmlElement bSRepElem in bitsetRepresElemList)
                  {//throught Bit Spare Data
                      outElem = outXmlDoc.CreateElement("BitRepres");//output element
                      outElem.SetAttribute("Language", "english");
                      outElem.SetAttribute("Mask", getDDDMask(bSRepElem.GetAttribute("Pos")));
                      outElem.SetAttribute("Text", bSRepElem.GetAttribute("Name"));
                      bitsetRepresElem.AppendChild(outElem);
                  }
              }
          }//BitsetRepres
          else
          {
              //SimpleRepres
              attribValue = getRepresName(elem, 0, 1);
              if (!represNameDict.ContainsKey(attribValue))
              {
                  outElem = outXmlDoc.CreateElement("SimpleRepres");//output element
                  // attribute Name
                  represNameDict.Add(attribValue, 1);//add name to dictionary
                  outElem.SetAttribute("Name", attribValue);
                  // attribute DataTypeName
                  attribValue = getDDDDataType(configDataType, elem.GetAttribute("Length"));
                  outElem.SetAttribute("DataTypeName", attribValue);
                  // attribute UnitSymbol
                  outElem.SetAttribute("UnitSymbol", elem.GetAttribute("Unit"));
                  // attribute Divisor
                  attribValue = elem.GetAttribute("Mult");
                  attribValue = attribValue.Trim();
                  if (!String.IsNullOrEmpty(attribValue))
                  {//count Divisor = 1/Mult
                      // Modify by Romeo 2011-02-22
                      doubAttribValue = Convert.ToDouble(attribValue, sourceNumberFormatInfo);
                      doubAttribValue = 1 / doubAttribValue;
                      // Modify by Romeo 2011-02-22
                      attribValue = doubAttribValue.ToString(numberFormatInfo);

                      outElem.SetAttribute("Divisor", attribValue);
                  }
                  // attribute Format
                  attribValue = elem.GetAttribute("Format");
                  outElem.SetAttribute("Format", attribValue);
                  // attribute Bias
                  attribValue = elem.GetAttribute("Bias");
                  attribValue = attribValue.Trim();
                  if (!String.IsNullOrEmpty(attribValue))
                  {//if Bias exists

                      outElem.SetAttribute("Bias", attribValue);
                  }
                  // add SimpleRepres element
                  repElem.AppendChild(outElem);
              }
          }//SimpleRepres
      }//Environment Data
      //SimpleRepres for dummy variables
      string dummyRepres = "DummySimpleRepres";
      if (!represNameDict.ContainsKey(dummyRepres))
      {
        outElem = outXmlDoc.CreateElement("SimpleRepres");//output element
        // attribute Name
        represNameDict.Add(dummyRepres, 1);//add name to dictionary
        outElem.SetAttribute("Name", dummyRepres);
        // attribute DataTypeName
        outElem.SetAttribute("DataTypeName", "U8");
        // attribute UnitSymbol
        outElem.SetAttribute("UnitSymbol", "");
        // attribute Divisor
        outElem.SetAttribute("Divisor", "1");
        // attribute Format
        outElem.SetAttribute("Format", "");
        // add SimpleRepres element
        repElem.AppendChild(outElem);
      }
      #endregion //Representations

      #region Variables
      string represName;
      string varNB = "0";
      int intVarNB = 0;
      int intMaxOffset = 0;
      int intMaxOffsetLength = 0;

      string histVariableName = "Env_Data";
      XmlElement variablesElem = outXmlDoc.CreateElement("Variables");
      rootElem.AppendChild(variablesElem);
      //structure
      XmlElement structVarElem = outXmlDoc.CreateElement("Structure");
      structVarElem.SetAttribute("Name", histVariableName);
      structVarElem.SetAttribute("Comment", "Structure of all saved variables");
      variablesElem.AppendChild(structVarElem);
      //structure title
      outElem = outXmlDoc.CreateElement("Title");
      outElem.SetAttribute("Language", "english");
      outElem.SetAttribute("Text", "Env_Data");
      structVarElem.AppendChild(outElem);
      // structure variables
      //Spare Data
      elemList = inpXmlDoc.SelectNodes("//Impianti/SpareData/Spare");
      foreach (XmlElement elem in elemList)
      {//throught Spare Data
        // variable
        XmlElement variableElem = outXmlDoc.CreateElement("Variable");
        //attribute Name 
        variableElem.SetAttribute("Name", elem.GetAttribute("Short"));
        //attribute RepresName 
        configDataType = elem.GetAttribute("Type");// data type from config file
        if (configDataType == "ASCII" || configDataType == "STRING")
            represName = getRepresName(elem, 2, 0);
        else if (configDataType == "MDIG")
            represName = getRepresName(elem, 1, 0);
        else
          represName = getRepresName(elem, 0, 0);
        variableElem.SetAttribute("RepresName", represName);
        //attribute Comment 
        string ID = elem.GetAttribute("Id");
        variableElem.SetAttribute("Comment", "ID:" + elem.GetAttribute("Id") + "_" + elem.GetAttribute("Name"));
        structVarElem.AppendChild(variableElem);
        // variable element Title
        outElem = outXmlDoc.CreateElement("Title");
        outElem.SetAttribute("Language", "english");
        outElem.SetAttribute("Text", elem.GetAttribute("Name"));
        variableElem.AppendChild(outElem);
        intVarNB++;//number of Spare Data
      }//Spare Data
      //Environment Data
      elemList = inpXmlDoc.SelectNodes("//Impianti/EnvironmentData/Environment");
      string offset;
      int intOffset;
      // int intMaxOffset = 0;
      // int intMaxOffsetLength = 0;  
      foreach (XmlElement elem in elemList)
      {//maximal offset of Environment Data 
        offset = elem.GetAttribute("Offset");
        if (!String.IsNullOrEmpty(offset))
        {//Offset attribute exists in the element 
          intOffset = Convert.ToInt32(offset);
          if (intOffset > intMaxOffset)
          {
            intMaxOffset = intOffset;
            intMaxOffsetLength = Convert.ToInt32(elem.GetAttribute("Length"));
          }
        }
        else
        {//Offset attribute doesn't exist in the element-error 
          errorMessage.Remove(0, errorMessage.Length);
          errorMessage.Append("Error-Variable without Offset attribute, error variable:" + elem.GetAttribute("Short"));
          logger.LogText(3, "AlstDDDGen", errorMessage.ToString());
          return ImportDDDVerResult.CfgFileConvErr;
        }
      }
      envDataSampleLen = intVarNB + (intMaxOffset + intMaxOffsetLength);  // length of variable data

      //add Environment variables in the order of the offset
      bool varOffFound = false;
      string length = "";
      for (int reqOffset = 0; reqOffset <= intMaxOffset; reqOffset++)
      {//Environmet variable for required offset
        varOffFound = false;
        foreach (XmlElement elem in elemList)
        {//throught Environment Data
          // variable
          offset = elem.GetAttribute("Offset");
          intOffset = Convert.ToInt32(offset);
          if (intOffset == reqOffset)
          {//required offset
            varOffFound = true;
            string id = elem.GetAttribute("Id");
            length = elem.GetAttribute("Length");
            int intLength;
            if (!String.IsNullOrEmpty(length))
            {//Lengtht attribute exists in the element 
              intLength = Convert.ToInt32(length);
            }
            else
            {//Offset attribute doesn't exist in the element - error 
              errorMessage.Remove(0, errorMessage.Length);
              errorMessage.Append("Error-Variable without Length attribute, error variable:" + elem.GetAttribute("Short"));
              logger.LogText(3, "AlstDDDGen", errorMessage.ToString());
              return ImportDDDVerResult.CfgFileConvErr;
            }
            if (varOffsetDict.ContainsKey(offset))
            {//overlapping
              errorMessage.Remove(0, errorMessage.Length);
              errorMessage.Append("Error-Variable with the same offset already exists, error variable:" + elem.GetAttribute("Short"));
              logger.LogText(3, "AlstDDDGen", errorMessage.ToString());
              return ImportDDDVerResult.CfgFileConvErr;
            }
            else
            {   //o overlapping
              //add offsets to dictionary
              for (int i = intOffset /*0*/; i < intOffset + intLength; i++)
              {
                // intOffset = intOffset + i;
                //                                varOffsetDict.Add(intOffset.ToString(), id);
                varOffsetDict.Add(i.ToString(), id);
                varNB = i.ToString();
              }
              //Element Variable 
              XmlElement variableElem = outXmlDoc.CreateElement("Variable");
              //attribute Name 
              variableElem.SetAttribute("Name", elem.GetAttribute("Short"));
              //attribute RepresName 
              configDataType = elem.GetAttribute("Type");// data type from config file
              if (configDataType == "ASCII" || configDataType == "STRING")
                  represName = getRepresName(elem, 2, 1);
              else if (configDataType == "MDIG")
                represName = getRepresName(elem, 1, 1);
              else
                represName = getRepresName(elem, 0, 1);
              variableElem.SetAttribute("RepresName", represName);
              //attribute Comment 
              variableElem.SetAttribute("Comment", "Offset:" + elem.GetAttribute("Offset") + "_ID:" + elem.GetAttribute("Id") + "_" + elem.GetAttribute("Name"));
              structVarElem.AppendChild(variableElem);
              // variable element Title
              outElem = outXmlDoc.CreateElement("Title");
              outElem.SetAttribute("Language", "english");
              outElem.SetAttribute("Text", elem.GetAttribute("Name"));
              variableElem.AppendChild(outElem);
            }
            break;
          }//required offset
        }//throught Environment Data
        if (!varOffFound)
        {//variable with required offset didn't find 
          string strReqOffset = reqOffset.ToString();
          if (!varOffsetDict.ContainsKey(strReqOffset))
          {//add dummy variable
            //Element Variable 
            XmlElement variableElem = outXmlDoc.CreateElement("Variable");
            //attribute Name 
            variableElem.SetAttribute("Name", "Dummy_" + strReqOffset);
            //attribute RepresName 
            variableElem.SetAttribute("RepresName", dummyRepres);
            //attribute Comment 
            variableElem.SetAttribute("Comment", "Offset:" + strReqOffset + "_Dummy variable");
            structVarElem.AppendChild(variableElem);
            // variable element Title
            outElem = outXmlDoc.CreateElement("Title");
            outElem.SetAttribute("Language", "english");
            outElem.SetAttribute("Text", "Dummy variable for offset " + strReqOffset);
            variableElem.AppendChild(outElem);
          }//add dummy variable
        }//variable with required offset didn't find 
      }//Envinroment Data-//Environmet variable for required offset


      //string varNB= varOffsetDict.Keys[varOffsetDict.Keys.Count - 1];
      StringBuilder report = new StringBuilder("Length of environment variables= " + envDataSampleLen.ToString());
      logger.LogText(2, "AlstDDDGen", report.ToString());

      //Dummy Variable
      XmlElement dummyVarElem = outXmlDoc.CreateElement("Variable");
      //attribute Name 
      dummyVarElem.SetAttribute("Name", "DumVar");
      //attribute Name 
      dummyVarElem.SetAttribute("RepresName", dummyRepres);
      //attribute Comment 
      dummyVarElem.SetAttribute("Comment", "Dummy Variable");
      variablesElem.AppendChild(dummyVarElem);


      //Trigger Variable
      XmlElement triggerElem = outXmlDoc.CreateElement("Trigger");
      //attribute Name 
      triggerElem.SetAttribute("Name", "Trigger");
      //attribute Comment 
      triggerElem.SetAttribute("Comment", "Dummy Trigger");
      variablesElem.AppendChild(triggerElem);

      #endregion //Variables

      #region Subsystems
      XmlElement subsystemsElem = outXmlDoc.CreateElement("Subsystems");
      rootElem.AppendChild(subsystemsElem);
      //subsystem
      XmlElement subsystemElem = outXmlDoc.CreateElement("Subsystem");
      subsystemElem.SetAttribute("Name", TUType+"Train");
      subsystemsElem.AppendChild(subsystemElem);
      //Subsystem devices
      elemList = inpXmlDoc.SelectNodes("//Impianti/Impianto/Nome");
      foreach (XmlElement elem in elemList)
      {//throught devices-Impianto
        string name = elem.InnerText;
        if (!deviceSubsDict.ContainsKey(name))
        {
          deviceSubsDict.Add(name, 1);
          outElem = outXmlDoc.CreateElement("Device");
          outElem.SetAttribute("Name", elem.InnerText);
          subsystemElem.AppendChild(outElem);
        }
        else
        {//device already exists
          errorMessage.Remove(0, errorMessage.Length);
          errorMessage.Append("Warning-Device in the Subsystem already exists, Device name: " + name);
          logger.LogText(3, "AlstDDDGen", errorMessage.ToString());
        }
      }
      #endregion //Subsystems

      #region Functions
      XmlElement functionsElem = outXmlDoc.CreateElement("Functions");
      rootElem.AppendChild(functionsElem);
      //Function
      XmlElement functionElem = outXmlDoc.CreateElement("Function");
      functionElem.SetAttribute("Name", TUType+"Train function");
      functionsElem.AppendChild(functionElem);
      //Function devices
      elemList = inpXmlDoc.SelectNodes("//Impianti/Impianto/Nome");
      foreach (XmlElement elem in elemList)
      {//throught devices-Impianto
        string name = elem.InnerText;
        if (!deviceFuncDict.ContainsKey(name))
        {
          deviceFuncDict.Add(name, 1);
          outElem = outXmlDoc.CreateElement("NeedsDevice");
          outElem.SetAttribute("DeviceName", elem.InnerText);
          functionElem.AppendChild(outElem);

        }
        else
        {//device already exists
          errorMessage.Remove(0, errorMessage.Length);
          errorMessage.Append("Warning-Device in the Function already exists, Device name: " + name);
          logger.LogText(3, "AlstDDDGen", errorMessage.ToString());
        }
      }
      #endregion //Functions

      #region DeviceCodes
      XmlElement deviceCodesElem = outXmlDoc.CreateElement("DeviceCodes");
      rootElem.AppendChild(deviceCodesElem);

      elemList = inpXmlDoc.SelectNodes("//Impianti/Impianto");
      foreach (XmlElement elem in elemList)
      {//throught devices-Impianto
        //DeviceCode
        XmlElement deviceCodeElem = outXmlDoc.CreateElement("DeviceCode");

        string deviceName = "";
        string deviceCode = "";
        childElemList = elem.ChildNodes;
        foreach (XmlNode childElem in childElemList)
        {//Device name 
          if (childElem.LocalName == "Nome")
          {
            deviceName = childElem.InnerText;
          }
          if (childElem.LocalName == "Impianto_Id")
          {
            deviceCode = childElem.InnerText;
          }
          if (deviceName != "" && deviceCode != "")
            break;
        }
        if (deviceCodeDict.ContainsKey(deviceCode))
        {
          string dictDevName = "";
          deviceCodeDict.TryGetValue(deviceCode, out dictDevName);
          errorMessage.Remove(0, errorMessage.Length);
          errorMessage.Append("Error-Device with the same Device Code already exists, Device Code: " + deviceCode + ", actual Device name: " + deviceName + ", previous Device name: " + dictDevName);
          logger.LogText(3, "AlstDDDGen", errorMessage.ToString());
          return ImportDDDVerResult.CfgFileConvErr;//## outXmlDoc;
        }
        else
        {
          deviceCodeDict.Add(deviceCode, deviceName);
          deviceCodeElem.SetAttribute("Code", deviceCode);
          deviceCodeElem.SetAttribute("Name", deviceName);
          deviceCodesElem.AppendChild(deviceCodeElem);// append particular DeviceCode element
        }
      }//throught devices-Impianto
      #endregion //DeviceCodes

      #region EventRecords
      // set sampling attributes from input config. file, if appropriate element exists  
      XmlElement samplingAtribs = (XmlElement)inpXmlDoc.SelectSingleNode("//Impianti/Sampling");
      if (samplingAtribs != null)
      {
        samplesBeforeStart = samplingAtribs.GetAttribute("BeforeFault");
        int isamplesAfterStart = Convert.ToInt32(samplingAtribs.GetAttribute("AfterFault"))+1;
        samplesAfterStart = isamplesAfterStart.ToString();// samplingAtribs.GetAttribute("AfterFault");
        samplingPeriod = samplingAtribs.GetAttribute("Period");
        isamplingPeriod = Convert.ToInt32(samplingPeriod) * 100;//attribute Period is 1/10s isamplingPeriod=Period/10 then convert to ms isamplingPeriod=isamplingPeriod*1000
        samplingPeriod = isamplingPeriod.ToString();
        if (samplingAtribs.HasAttribute("DDSRecordLength")) DDSRecordLength = samplingAtribs.GetAttribute("DDSRecordLength");
      }
      bool duplFaultCode = false;//true if Fault Code already exists 
      XmlElement eventRecordsElem = outXmlDoc.CreateElement("EventRecords");
      rootElem.AppendChild(eventRecordsElem);
      // from impianti list
      elemList = inpXmlDoc.SelectNodes("//Impianti/Impianto");
      foreach (XmlElement elem in elemList)
      {//throught devices-Impianto
        //EventRecord
        string deviceName = "";
        string deviceCode = "";
        childElemList = elem.ChildNodes;
        foreach (XmlNode childElem in childElemList)
        {//Device name 
          if (childElem.LocalName == "Nome")
          {
            deviceName = childElem.InnerText;
            break;
          }
        }
        foreach (XmlNode childElem in childElemList)
        {//Impianto_Id 
          if (childElem.LocalName == "Impianto_Id")
          {
            deviceCode = childElem.InnerText;
            break;
          }
        }
        foreach (XmlNode childElem in childElemList)
        {//Avaria
          if (childElem.LocalName == "Avaria")
          {//throught avaria elements 
              // Modify by Romeo 2011-02-22
              string strSeverity = Properties.Settings.Default.DefaultSeverity; // "B";
              foreach (XmlNode el in childElem)
              {//find Severity
                  if (el.NodeType == XmlNodeType.Element && el.LocalName == "Severity")
                  {
                      strSeverity = el.InnerText;
                      break;
                  }
              }
            duplFaultCode = false;
            XmlElement eventRecordElem = outXmlDoc.CreateElement("EventRecord");
            XmlNodeType nodeType;
            XmlNode avariaElem = childElem.FirstChild;
            string strFaultCode = "";
            while (avariaElem != null)
            // foreach (XmlElement avariaElem in avariaElemList)
            {//throught inner avaria elements 
              nodeType = avariaElem.NodeType;//omit comments 
              if (nodeType == XmlNodeType.Element)
              {
                  if (avariaElem.LocalName == "Identificativo")
                  {
                      strFaultCode = avariaElem.InnerText;
                      if (faultCodeDict.ContainsKey(strFaultCode))
                      {//fault code already exists
                          string dictDevCode = "";
                          faultCodeDict.TryGetValue(strFaultCode, out dictDevCode);
                          string dictDevName = "";
                          deviceCodeDict.TryGetValue(dictDevCode, out dictDevName);
                          errorMessage.Remove(0, errorMessage.Length);
                          errorMessage.Append("Error-Fault Code already exists, Fault Code: " + strFaultCode + ", actual Device Name: " + deviceName + ", actual Device Code: " + deviceCode + ", previous Device Name: " + dictDevName + ", previous Device Code: " + dictDevCode);
                          logger.LogText(3, "AlstDDDGen", errorMessage.ToString());
                          duplFaultCode = true;
                          //return outXmlDoc;
                      }
                      if (!duplFaultCode)
                      {// if Fault Code doesn't exist add new EventRecord
                          faultCodeDict.Add(strFaultCode, deviceCode);// add fault code to the dictionary
                          eventRecordElem.SetAttribute("Name", deviceName + "_" + avariaElem.InnerText);
                          eventRecordElem.SetAttribute("Severity", strSeverity);  //"B");
                          eventRecordElem.SetAttribute("Trigger", "Trigger");
                          eventRecordElem.SetAttribute("TriggerReaction", "LeadingEdge");
                          eventRecordElem.SetAttribute("FaultCode", avariaElem.InnerText);
                          eventRecordsElem.AppendChild(eventRecordElem);
                      }
                  }
                  if (avariaElem.LocalName == "Descrizione")
                  {
                      if (!duplFaultCode)
                      {// if Fault Code doesn't exist add title to  EventRecord
                          //title
                          XmlElement eventRecTitleElem = outXmlDoc.CreateElement("Title");
                          eventRecTitleElem.SetAttribute("Language", "Italian");
                          eventRecTitleElem.SetAttribute("Text", avariaElem.InnerText);
                          eventRecordElem.AppendChild(eventRecTitleElem);
                      }
                  }
              }
              avariaElem = avariaElem.NextSibling;
            }//throught inner avaria elements 
            if (!duplFaultCode)
            {// if Fault Code doesn't exist add AssocFunction , AssocFunction, HistorizedInput to  EventRecord
                //AssocFunction
                XmlElement eventRecElems = outXmlDoc.CreateElement("AssocFunction");
                eventRecElems.SetAttribute("Name", TUType + "Train function");
                eventRecordElem.AppendChild(eventRecElems);
                //AssocFunction
                eventRecElems = outXmlDoc.CreateElement("AssocDevice");
                eventRecElems.SetAttribute("Name", deviceName);
                eventRecordElem.AppendChild(eventRecElems);
                // Modify by Romeo 2011-02-22
                if (samplesBeforeStart == "0" && samplesAfterStart == "1")
                { //Input
                    eventRecElems = outXmlDoc.CreateElement("Input");
                    eventRecElems.SetAttribute("VarName", histVariableName);
                    eventRecordElem.AppendChild(eventRecElems);
                }
                else
                { //HistorizedInput
                    eventRecElems = outXmlDoc.CreateElement("HistorizedInput");
                    eventRecElems.SetAttribute("VarName", histVariableName);
                    eventRecElems.SetAttribute("BeforeStart", samplesBeforeStart);// "5"
                    eventRecElems.SetAttribute("PastStart", samplesAfterStart);//"6"
                    eventRecElems.SetAttribute("SamplingPeriod", samplingPeriod);
                    eventRecordElem.AppendChild(eventRecElems);
                }
            }
          }//throught Avaria elements 
        }//Avaria
      }//throught devices-Impianto
      #endregion //EventRecords

      #region TraceRecords
      XmlElement traceRecordsElem = outXmlDoc.CreateElement("TraceRecords");
      rootElem.AppendChild(traceRecordsElem);
      /*
        XmlElement traceRecordElem = outXmlDoc.CreateElement("TraceRecord");
        traceRecordElem.SetAttribute("Name", "TR");
        traceRecordElem.SetAttribute("LastRecs", "1");
        traceRecordElem.SetAttribute("Trigger", "Trigger");
        traceRecordsElem.AppendChild(traceRecordElem);

        outElem = outXmlDoc.CreateElement("Title");
        outElem.SetAttribute("Language", "english");
        outElem.SetAttribute("Text", "Dummy Trace Record");
        traceRecordElem.AppendChild(outElem);

        outElem = outXmlDoc.CreateElement("Input");
        outElem.SetAttribute("VarName", "DumVar");
        traceRecordElem.AppendChild(outElem);
      */
      #endregion //TraceRecords


      #region SnapRecords
      XmlElement snapRecordsElem = outXmlDoc.CreateElement("SnapRecords");
      rootElem.AppendChild(snapRecordsElem);
      /*
        XmlElement snapRecordElem = outXmlDoc.CreateElement("SnapRecord");
        snapRecordElem.SetAttribute("Name", "SR");
        snapRecordElem.SetAttribute("Trigger", "Trigger");
        snapRecordsElem.AppendChild(snapRecordElem);

        outElem = outXmlDoc.CreateElement("Title");
        outElem.SetAttribute("Language", "english");
        outElem.SetAttribute("Text", "Dummy Snap Record");
        snapRecordElem.AppendChild(outElem);

        outElem = outXmlDoc.CreateElement("Input");
        outElem.SetAttribute("VarName", "DumVar");
        snapRecordElem.AppendChild(outElem);*/

      #endregion //SnapRecords

      return ImportDDDVerResult.OK;//## outXmlDoc;

    }
  }

  
  public class AlstomDDDGenerator
  {
    private Logger logger;
    // @@@ comment private string outFilePath;
    private DateTimeFormatInfo timeFormatInfo; //Time format info object


      public AlstomDDDGenerator(Logger logger)
      {
          this.logger = logger;
          // @@@ comment outFilePath = Properties.Settings.Default.DDDfile;

          // Modify by Romeo 2011-02-22
          //Create time format info
          timeFormatInfo = new DateTimeFormatInfo();
          timeFormatInfo.DateSeparator = Properties.Settings.Default.DateSeparator; // "-";
          timeFormatInfo.TimeSeparator = Properties.Settings.Default.TimeSeparator; // ":";
      }
 
    
    /// <summary>
    /// Generates DDD Version  from Alstom configuration file, add new DDD Version to database, Alstom configuration file and AlstomCfg object  
    /// </summary>
    /// <param name="inpXmlDoc">input Alstom configuration</param>
    /// <param name="AlstomVersionName">output parametr-Alstom config. version</param>
    /// <param name="alstomCfg">AlstomCfg object</param>
    /// <param name="TUTypesArrList">list of TUType with newly registered DDD Version</param>
    /// <returns>according to  mportDDDVerResult enumeration </returns>
    public ImportDDDVerResult GenerDDDFile(XmlDocument inpXmlDoc, out string AlstomVersionName, AlstomCfg alstomCfg, ArrayList TUTypesArrList)
    {
      if (TUTypesArrList.Count>0)TUTypesArrList.RemoveAt(0);
      StringBuilder errorMessage = new StringBuilder();
      string sTUType = Properties.Settings.Default.TUTypeName;// "ETR600"
      string alstomTypeNb = Properties.Settings.Default.AlstomTrainType;// "01"
      ConfiConvDDD ConfigConvDDD = new ConfiConvDDD();
      ImportDDDVerResult resOfTUTypeCheck = ImportDDDVerResult.OK;
      ImportDDDVerResult retVal = ImportDDDVerResult.OK;
      if (TUTypesArrList.Count > 0) TUTypesArrList.RemoveAt(0);

      logger.LogText(0, "", "");
      logger.LogText(0, "", "----------------------------------------------");
      logger.LogText(1, "AlstDDDGen", "DDD version file generation");

      // list of Alstom TUTypes
      #region TUTypes
      XmlElement impiantiElem = (XmlElement)inpXmlDoc.SelectSingleNode("//Impianti");

      //Alstom config number ~ DDD version
      //inp atribute VersioneAvarieTraction
      string sVersioneAvarieTraction = impiantiElem.GetAttribute("VersioneAvarieTraction");
      sVersioneAvarieTraction = sVersioneAvarieTraction.Trim();
      sVersioneAvarieTraction = sVersioneAvarieTraction.PadLeft(5, '0');
      //inp atribute VersioneAvarieComfort
      string sVersioneAvarieComfort = impiantiElem.GetAttribute("VersioneAvarieComfort");
      sVersioneAvarieComfort = sVersioneAvarieComfort.Trim();
      sVersioneAvarieComfort = sVersioneAvarieComfort.PadLeft(5, '0');
      string alstomConfigNb = sVersioneAvarieTraction + sVersioneAvarieComfort;
      AlstomVersionName = alstomConfigNb;// output parameter

      logger.LogText(2, "AlstDDDGen", "Alstom version name = {0}", AlstomVersionName);

      //List of TUTypes
      string sAlstTUTypeList = impiantiElem.GetAttribute("Flotta");
      string[] AlstTUTypeList = sAlstTUTypeList.Split(new Char[] { ',', ';' });

      foreach (string AlstTUType in AlstTUTypeList)
      {//throught list of Alstom TUType 
        alstomTypeNb = AlstTUType.Trim();
        alstomTypeNb = alstomTypeNb.PadLeft(2, '0');
        resOfTUTypeCheck = ConfigConvDDD.getTUType(alstomTypeNb, alstomConfigNb, ref sTUType, logger);
        if (resOfTUTypeCheck == ImportDDDVerResult.Error) return resOfTUTypeCheck; // error
        if (retVal == ImportDDDVerResult.OK) retVal = resOfTUTypeCheck; // TUType not exists or DDDVersion exists (in Alstom cfg. xml file
        if (resOfTUTypeCheck == ImportDDDVerResult.OK)
        {// TUType exists and DDDVersion not exists(in Alstom cfg. xml file ->import new DDD Version
          #region Root

          XmlDocument outXmlDoc = new XmlDocument();
          outXmlDoc.LoadXml("<DDD xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='DDD.xsd'>" +
                      "</DDD>");
          //Create an XML declaration. 
          XmlDeclaration xmldecl;
          xmldecl = outXmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);

          //Add the new node to the document.
          XmlElement root = outXmlDoc.DocumentElement;
          outXmlDoc.InsertBefore(xmldecl, root);

          //DDD - VersionInfo element
          XmlElement verInfoElem = outXmlDoc.CreateElement("VersionInfo");
          //DDD - TUTypeName attribute
          string dBTUType = sTUType + "_" + alstomTypeNb;
          verInfoElem.SetAttribute("TUTypeName", dBTUType);
          //DDD - DDDVersion attribute
          string sDDDVersion = sTUType + '_' + alstomConfigNb;
          verInfoElem.SetAttribute("DDDVersion", sDDDVersion);
          //DDD - CreationDate attribute
          DateTime ActTime = new DateTime();
          ActTime = DateTime.Now;
          string TimeFormat = Properties.Settings.Default.DateTimeFormat; //"yyyy-MM-ddTHH:mm:ss";
          verInfoElem.SetAttribute("CreationDate", ActTime.ToString(TimeFormat, timeFormatInfo));
          //DDD - Comment attribute
          verInfoElem.SetAttribute("Comment", sTUType + " Diagnostic Unit configuration");
          root.AppendChild(verInfoElem);//DDD - add VersionInfo element
          #endregion //Root

          //conversion ERT 600 ConfigIT file to DDD
          ImportDDDVerResult result = ConfigConvDDD.convConfigToDDD(inpXmlDoc, outXmlDoc, sTUType, logger);
          if (result == ImportDDDVerResult.OK)
          {// output file was created without error
           /* //@@@comment creation of output file
            try
            {//save output XML Document;
              outXmlDoc.Save(outFilePath);
            }
            catch (Exception e)
            {
              errorMessage.Remove(0, errorMessage.Length);
              errorMessage.Append("Error-Output cofiguration file could not be written: " + e.Message);
              logger.LogText(3, "AlstDDDGen", errorMessage.ToString());
              return ImportDDDVerResult.Error; ;
            } //@@@@end of comment */

            logger.LogText(2, "AlstDDDGen", "DDD version {0} was generated", sDDDVersion);

            //insert new config. version to database
            ImportDDDVerResult res = ConfigConvDDD.addDDDVerToDB(outXmlDoc, logger);
            if (res == ImportDDDVerResult.OK)
            {// DDDVersion is saved in the DB
              //insert new config. version to database, AlstomTUTypes.xml file and AlstomCfg object
              res = ConfigConvDDD.addDDDVerToCfg(dBTUType, sDDDVersion, alstomConfigNb, alstomCfg, logger);
              if (res == ImportDDDVerResult.OK || res == ImportDDDVerResult.DDDVerExists)
              {
                TUTypesArrList.Add(alstomTypeNb);
              }
            }
            else retVal = res;
          }
          else
          {// error in input cfg file ->output file was not created 
            retVal = result;
          } 
        }// import new DDD Version
      }//throught list of Alstom TUType 
      #endregion //TUTypes

      return retVal;
    }//GenerDDDFile


  }
}
