namespace AlstomDDSImport
{
  partial class ProjectInstaller
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.AlstProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
      this.AlstServiceInstaller = new System.ServiceProcess.ServiceInstaller();
      // 
      // AlstProcessInstaller
      // 
      this.AlstProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
      this.AlstProcessInstaller.Password = null;
      this.AlstProcessInstaller.Username = null;
      // 
      // AlstServiceInstaller
      // 
      this.AlstServiceInstaller.Description = "Converts Alstom DDS files into DRImport files";
      this.AlstServiceInstaller.DisplayName = "Alstom DDS Service";
      this.AlstServiceInstaller.ServiceName = "AlstomDDSImport";
      // 
      // ProjectInstaller
      // 
      this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.AlstProcessInstaller,
            this.AlstServiceInstaller});

    }

    #endregion

    private System.ServiceProcess.ServiceProcessInstaller AlstProcessInstaller;
    private System.ServiceProcess.ServiceInstaller AlstServiceInstaller;
  }
}