using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace AlstomDDSImport
{
  /// <summary>
  /// Class represents one record (line) in Alstom DDS file
  /// </summary>
  [Serializable]
  public class AlstomDDSRecord
  {
    /// <summary>
    /// Code of the device which generated the record (SystemId)
    /// </summary>
    public UInt16 DeviceCode;
    /// <summary>
    /// Fault code represented by this record 
    /// (0 in case record is just env. data sample)
    /// </summary>
    public UInt16 FaultCode;
    /// <summary>
    /// Number of the vehicle from which the record originated
    /// </summary>
    public Byte VehicleNumber;
    /// <summary>
    /// Time stamp of the records
    /// </summary>
    public DateTime TimeStamp;
    /// <summary>
    /// Array of environmental data of correct length 
    /// (according to DDD Version which is associated with this record)
    /// </summary>
    public Byte[] EnvironmentalData;
    /// <summary>
    /// Length of record header in bytes
    /// </summary>
    private static int RecHeaderLen = 11;
    /// <summary>
    /// Temporary buffer for parsing of big endian data
    /// </summary>
    private static byte[] temporaryBuffer = new byte[2];
    /// <summary>
    /// Create the record from DDS file binary "line"
    /// </summary>
    /// <param name="buffer">Data read from DDS file</param>
    /// <param name="envDataSampleLen">length of env. data sample</param>
    /// <returns>Record created from the binary data</returns>
    public static AlstomDDSRecord CreateFromDDSFile(Byte[] buffer, int envDataSampleLen)
    {
      //Check buffer for correct length
      if (buffer == null || buffer.Length < envDataSampleLen + RecHeaderLen)
        throw new Exception("Cannot create DDS record - buffer is too small");
      //Create new instance of DDS record
      AlstomDDSRecord ddsRecord = new AlstomDDSRecord();
      //Decode members
      //DeviceCode
      temporaryBuffer[0] = buffer[1];
      temporaryBuffer[1] = buffer[0];
      ddsRecord.DeviceCode = BitConverter.ToUInt16(temporaryBuffer, 0);
      temporaryBuffer[0] = buffer[3];
      temporaryBuffer[1] = buffer[2];
      ddsRecord.FaultCode = BitConverter.ToUInt16(temporaryBuffer, 0);
      ddsRecord.VehicleNumber = buffer[4];
      //Decode time
      ddsRecord.TimeStamp =
        new DateTime((DateTime.Now.Year / 100) * 100 + buffer[7], //Year
        buffer[6], buffer[5], buffer[8], buffer[9], buffer[10]);
      //Create buffer with binary data
      ddsRecord.EnvironmentalData = new Byte[envDataSampleLen];
      Array.Copy(buffer, RecHeaderLen, ddsRecord.EnvironmentalData, 0, envDataSampleLen);
      //Return created record
      return ddsRecord;
    }
  }

  /// <summary>
  /// Helper class with methods used to handle DDS file processing
  /// </summary>
  public class AlstomDDSFileHelper
  {
    /// <summary>
    /// Decode name of the file and return found configuration objects
    /// plus ID of the train
    /// </summary>
    public static void GetCfgObjects(
      String fileName, AlstomCfg alstomCfg, 
      out AlstomTUType tuType, out AlstomDDDVersion dddVersion, out int trainID)
    {
      //Decode parts of the file name
      if (fileName.Length < 32)
        throw new Exception("File name is too short");
      String strTypeID = fileName.Substring(15, 2);
      String strTrainID = fileName.Substring(18, 3);
      String strDDDVerID = fileName.Substring(22, 10);
      //TU Type
      if (!alstomCfg.TUTypes.TryGetValue(strTypeID, out tuType))
        throw new Exception("Unknown train type " + strTypeID);
      //DDDVersion
      if (!tuType.DDDVersions.TryGetValue(strDDDVerID, out dddVersion))
        throw new UnknownDDDException("Unknown faults descrition version " + strDDDVerID, strDDDVerID);
      //Train ID
      if (!Int32.TryParse(strTrainID, out trainID))
        throw new Exception("Invalid train ID " + strTrainID);
    }
    /// <summary>
    /// Load binary DDS records from file and convert them to DDS record objects
    /// </summary>
    public static List<AlstomDDSRecord> LoadDDSRecords(
      Stream ddsFile, int ddsRecordLen, int envDataSampleLen, StreamWriter outFile)
    {
      //Create buffer for one DDS record
      Byte[] buffer = new Byte[ddsRecordLen];
      //Create list for DDS records
      List<AlstomDDSRecord> ddsRecords = new List<AlstomDDSRecord>();
      //Read records from file and add them to the list
      try
      {
        int recIndex = 0;
        while (ddsFile.Read(buffer, 0, ddsRecordLen) == ddsRecordLen)
        {
          AlstomDDSRecord ddsRecord = AlstomDDSRecord.CreateFromDDSFile(buffer, envDataSampleLen);
          ddsRecords.Add(ddsRecord);
          if (outFile != null)
          { //Store transformed record to output file
            outFile.WriteLine("T:{0:yyyy-MM-ddTHH:mm:ss}\tF:{1}\tD:{2}\tV:{3}\t{4}",
              ddsRecord.TimeStamp, ddsRecord.FaultCode, ddsRecord.DeviceCode, ddsRecord.VehicleNumber,
              BitConverter.ToString(buffer));
          }
          recIndex++;
        }
      }
      catch (Exception ex)
      {
        throw new Exception("Failed to load DDS record with index {0}", ex);
      }
      //Return list of records
      return ddsRecords;
    }
    /// <summary>
    /// Method returns environmental data for one fault.
    /// Fault is defined as an index in the list of DDS records.
    /// When all necessary DDS records are available in the list - env data array is returned.
    /// When there are DDS records missing after the fault null is returne - 
    ///   fault may be processed again with new DDS file.
    /// When there are DDS records missing before the fault in the lis exception is thrown.
    /// </summary>
    public static Byte[] GetEnvDataArray(
      List<AlstomDDSRecord> ddsRecords, int faultIndex, AlstomDDDVersion dddVersion)
    {
      //Check fault index
      if (faultIndex < 0 || faultIndex >= ddsRecords.Count)
        throw new Exception("Invalid index of fault DDS record in the list: " + faultIndex.ToString());
      //Check if there is enough DDS records before the fault
      if (faultIndex - dddVersion.SamplesBeforeStart < 0)
        throw new Exception("Environmental data before fault start are missing");
      //Check if there is enough DDS records after the fault
      if (faultIndex + dddVersion.SamplesAfterStart > ddsRecords.Count)
        return null; //Samples have to be filled in from next file
      //Create array for env data
      int envDataSampleLen = ddsRecords[faultIndex].EnvironmentalData.Length;
      Byte[] envData = 
        new Byte[envDataSampleLen * (dddVersion.SamplesBeforeStart + dddVersion.SamplesAfterStart)];
      //Iterate over DDS records, copy data to the output array
      int ddsRecIndex;
      TimeSpan lastTimeStampDiff = new TimeSpan(0); //Difference in timestamps between last two consecutive records
      TimeSpan currTimeStampDiff; //Difference in timestamps between current two consecutive records
      double samplingDiff; //Difference in sampling of environmental data
      for (int i = 0; i < dddVersion.SamplesBeforeStart + dddVersion.SamplesAfterStart; i++)
      { //Copy environmental data from each record into target array
        ddsRecIndex = faultIndex - dddVersion.SamplesBeforeStart + i;
        Array.Copy(ddsRecords[ddsRecIndex].EnvironmentalData, 0, envData, i * envDataSampleLen, envDataSampleLen);
        //Check for periodic time sampling
        if (i > 0)
        { //Second and next records
          currTimeStampDiff = ddsRecords[ddsRecIndex].TimeStamp - ddsRecords[ddsRecIndex - 1].TimeStamp;
          samplingDiff = currTimeStampDiff.TotalSeconds - lastTimeStampDiff.TotalSeconds;
          if ((lastTimeStampDiff.Ticks != 0) && (samplingDiff < -2 || samplingDiff > 2))
            throw new Exception(String.Format("Env data sampling not periodic (prev {0}, curr {1})",
              lastTimeStampDiff.TotalSeconds, currTimeStampDiff.TotalSeconds));
          lastTimeStampDiff = currTimeStampDiff;
        }
      }
      //Return environmental data array
      return envData;
    }
    /// <summary>
    /// Method returns environmental data for one fault.
    /// Fault is defined as an index in the list of DDS records.
    /// Environmental data samples are searched by their timestamp using sampling period.
    /// Missing samples are filled by 0
    /// When fault is at the end of record list null is returned - 
    ///   fault may be processed again with new DDS file.
    /// </summary>
    public static Byte[] GetEnvDataArray2(
      List<AlstomDDSRecord> ddsRecords, int faultIndex, AlstomDDDVersion dddVersion, out int foundSamples)
    {
      foundSamples = 0;
      //Check fault index
      if (faultIndex < 0 || faultIndex >= ddsRecords.Count)
        throw new Exception("Invalid index of fault DDS record in the list: " + faultIndex.ToString());
      //Check if there is enough DDS records after the fault
      if (faultIndex + dddVersion.SamplesAfterStart > ddsRecords.Count)
        return null; //Samples have to be filled in from next file
      //Create array for env data
      int envDataSampleLen = ddsRecords[faultIndex].EnvironmentalData.Length;
      Byte[] envData =
        new Byte[envDataSampleLen * (dddVersion.SamplesBeforeStart + dddVersion.SamplesAfterStart)];
      //Zero out the entire array
      Array.Clear(envData, 0, envData.Length);
      //Obtain timestamp of fault start
      DateTime faultStart = ddsRecords[faultIndex].TimeStamp;
      //Copy environmental data sample belonging to fault start
      Array.Copy(ddsRecords[faultIndex].EnvironmentalData, 0, envData, dddVersion.SamplesBeforeStart * envDataSampleLen, envDataSampleLen);
      foundSamples = 1;
      //Walk backwards in records list, try to find samples belonging to before start array
      int currIndex = faultIndex;
      DateTime currTimeStamp = faultStart;
      for (int sampleIndex = dddVersion.SamplesBeforeStart; sampleIndex > 0; sampleIndex--)
      { //Iterate over each sample before the fault start
        currTimeStamp = currTimeStamp.AddMilliseconds(-1 * dddVersion.SamplingPeriod);
        while (currIndex >= 0)
        { //Walk backwards in the list of records
          if (ddsRecords[currIndex].TimeStamp == currTimeStamp)
          { //Sample has been found - copy data to the array
            Array.Copy(ddsRecords[currIndex].EnvironmentalData, 0, envData, (sampleIndex - 1) * envDataSampleLen, envDataSampleLen);
            foundSamples++;
            break; //Move to next sample
          }
          else if (ddsRecords[currIndex].TimeStamp < currTimeStamp)
          { //Sample with correct timestamp does not exist - try to locate sample with rounded timestamp
            TimeSpan timeDiff = currTimeStamp - ddsRecords[currIndex].TimeStamp;
            if (timeDiff.TotalMilliseconds <= dddVersion.SamplingPeriod)
            { //Sample is correct, but with rounded timestamp
              //Copy the data
              Array.Copy(ddsRecords[currIndex].EnvironmentalData, 0, envData, (sampleIndex - 1) * envDataSampleLen, envDataSampleLen);
              foundSamples++;
              //Correct current timestamp
              currTimeStamp = ddsRecords[currIndex].TimeStamp;
            }
            break; //Move to next sample
          }
          currIndex--; //Shift to previous record
        }
      }
      //Walk forward in the list, try to find elements belonging to after start array
      currIndex = faultIndex;
      currTimeStamp = faultStart;
      for (int sampleIndex = dddVersion.SamplesBeforeStart + 1; sampleIndex < (dddVersion.SamplesBeforeStart + dddVersion.SamplesAfterStart); sampleIndex++)
      { //Iterate over each sample after the fault start (first after start is represented by fault itself - already copied)
        currTimeStamp = currTimeStamp.AddMilliseconds(+1 * dddVersion.SamplingPeriod);
        while (currIndex < ddsRecords.Count)
        { //Walk forward in the list of records
          if (ddsRecords[currIndex].TimeStamp == currTimeStamp)
          { //Sample has been found - copy data to the array
            Array.Copy(ddsRecords[currIndex].EnvironmentalData, 0, envData, sampleIndex * envDataSampleLen, envDataSampleLen);
            foundSamples++;
            break; //Move to next sample
          }
          else if (ddsRecords[currIndex].TimeStamp > currTimeStamp)
          { //Sample with correct timestamp does not exist - try to locate sample with rounded timestamp
            TimeSpan timeDiff = ddsRecords[currIndex].TimeStamp - currTimeStamp;
            if (timeDiff.TotalMilliseconds <= dddVersion.SamplingPeriod)
            { //Sample is correct, but with rounded timestamp
              //Copy the data
              Array.Copy(ddsRecords[currIndex].EnvironmentalData, 0, envData, (sampleIndex - 1) * envDataSampleLen, envDataSampleLen);
              foundSamples++;
              //Correct current timestamp
              currTimeStamp = ddsRecords[currIndex].TimeStamp;
            }
            break; //Move to next sample
          }
          currIndex++; //Shift to next record
        }
      }
      //Return environmental data array
      return envData;
    }
    /// <summary>
    /// Returns list of DDS records from current set which has to be stored 
    /// and processed again with next DDS file (some samples may have to be used)
    /// </summary>
    public static List<AlstomDDSRecord> GetUnprocessedDDSRecords(
      List<AlstomDDSRecord> ddsRecords, AlstomDDDVersion dddVersion)
    {
      //Determine index of first record to be stored
      int firstIndex = ddsRecords.Count - (dddVersion.SamplesBeforeStart + dddVersion.SamplesAfterStart);
      if (firstIndex < 0)
        firstIndex = 0;
      //Create and return list with unprocessed records
      return ddsRecords.GetRange(firstIndex, ddsRecords.Count - firstIndex);
    }
  }

  /// <summary>
  /// Exception thrown in case unknown Alstom Cfg version name is encountered
  /// </summary>
  public class UnknownDDDException : Exception
  {
    /// <summary>
    /// Name of the unknown version
    /// </summary>
    public String AlstomVerName;
    /// <summary>
    /// Standard constructor
    /// </summary>
    /// <param name="message">Exception message</param>
    /// <param name="verName">Name of the unknown version</param>
    public UnknownDDDException(String message, String verName)
      : base(message)
    {
      this.AlstomVerName = verName;
    }
  }
}
