using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using DDDObjects;
using DRSelectorComponents;
using DSDiagnosticRecords;
using DSVisualRecords;

namespace DRViewComponents
{
  /// <summary>
  /// Visual component displaying Diagnostic Records in Master-Slave grid.
  /// Master grid diaplays record instances, slave grid displays enviromental 
  /// variables values for selected record in horizontal form.
  /// </summary>
  public partial class DRViewMaster_Slave : DRViewComponents.DRViewBase
  {
    #region Protected members
    protected int startTimeRow = -1; //Index of row with Starting time for variables grid
    protected Color startTimeColor = Color.Red; //Color of start of relative time
    protected List<int> recordsSortColumns; //list of column indexes, that shall be sorted against
    protected int[] recColsSortOrders; //array of sort orders for all columns in record grid
    protected bool bRaiseSelectRecordEvent = true; //Blocks recursive SelectRecord event generation
    protected bool bChangeVisibleColumns = true; //Blocks changing of visible grid columns during menu opening
    #endregion //Protected members

    #region Public properties
    /// <summary>
    /// Color of StartTime cell serving as start time for relative time in variables grid
    /// </summary>
    public Color StartTimeColor
    {
      get { return startTimeColor; }
      set { startTimeColor = value; }
    }
    #endregion //Public properties

    #region Construction and initialization
    public DRViewMaster_Slave()
      : base()
    {
      InitializeComponent();
      hvgVariables.ResizeColumns();
      recordsSortColumns = new List<int>();
      recColsSortOrders = new int[dgvRecords.Columns.Count];
    }
    #endregion //Construction and initialization

    #region Virtual methods overrides
    /// <summary>
    /// Handles new data ready event from selector component
    /// </summary>
    /// <param name="sender">Selector component</param>
    protected override void OnNewDataReady(DRSelectorBase sender, NDREventArgs args)
    {
      try
      {
        bRaiseSelectRecordEvent = false;
        base.OnNewDataReady(sender, args);
        //Check if control shall display data
        if (!displayData)
          return;
        OnInfoMessage("Loading records into master-slave grid view");
        //Fill grid
        FillRecordsGrid();
        //Sort the grid according to selected criteria
        dgvRecords.Sort(new RecordsRowComparer(recordsSortColumns, recColsSortOrders));
        //Send info message
        OnInfoMessage("Records loaded into master-slave grid view");
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRViewException(ex, "Failed to load records into master-slave grid view"));
      }
      finally
      {
        bRaiseSelectRecordEvent = true;
      }
    }

    /// <summary>
    /// Stores reference to assigned helper object.
    /// </summary>
    /// <param name="helper">Helper object</param>
    protected override void SetDDDHelper(DDDHelper helper)
    {
      //Call base implementation
      base.SetDDDHelper(helper);
      //Set helper for horizontal grid
      hvgVariables.DDDHelper = helper;
    }

    protected override void OnSelectRecord(DRSelectorBase sender, SelectRecordEventArgs args)
    {
      bRaiseSelectRecordEvent = false;
      try
      {
        //Call base implementation
        base.OnSelectRecord(sender, args);
        //Check if control displays data
        if (!displayData)
          return;
        //Deselect all selected records
        dgvRecords.ClearSelection();
        //We have to iterate over all rows and try to find the right one
        foreach (DataGridViewRow row in dgvRecords.Rows)
          if (args.TUInstanceID == (Guid)row.Cells["colTUInstanceID"].Value &&
              args.RecordInstID == (Int64)row.Cells["colRecordInstID"].Value &&
              args.RecordDefID == (int)row.Cells["colRecordDefID"].Value)
          {
            row.Selected = true;
            if (!row.Displayed)
              dgvRecords.FirstDisplayedScrollingRowIndex = row.Index;
            break;
          }
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRViewException(ex, "Failed to select record in grid view"));
      }
      finally
      {
        bRaiseSelectRecordEvent = true;
      }
    }

    protected override void SetViewParameters(ViewConfiguration viewConfiguration)
    { //Call base implementation
 	    base.SetViewParameters(viewConfiguration);
      //Set view parameters
      //First set all columns to invisible mode
      foreach (DataGridViewColumn col in dgvRecords.Columns)
        col.Visible = false;
      //Set selected columns to visible and in correct order
      for (int i = 0; i < viewConfiguration.Attributes.Count; i++)
      { //Iterate over all view attributes, set corresponding columns to visible mode
        String colName = "col" + viewConfiguration.Attributes[i].DataField;
        if (dgvRecords.Columns.Contains(colName))
        {
          dgvRecords.Columns[colName].Visible = true;
          dgvRecords.Columns[colName].DisplayIndex = i;
        }
        else
          throw new DRViewException("Unknown view column {0}", viewConfiguration.Attributes[i].DataField);
      }
    }
    #endregion //Virtual methods overrides

    #region Helper methods
    /// <summary>
    /// Fills dgvRecords component with data from currently assigned Selector component
    /// using assigned DDDHelper.
    /// </summary>
    protected void FillRecordsGrid()
    {
      //Number of processed records
      int processed = 0;
      int totalRecs = selector.RecordsView.Count;
      
      //Delete all old rows
      dgvRecords.Rows.Clear();
      //Set formatting for time columns
      colStartTime.DefaultCellStyle.Format = dddHelper.TimeFormat;
      colEndTime.DefaultCellStyle.Format = dddHelper.TimeFormat;
      colLastModified.DefaultCellStyle.Format = dddHelper.TimeFormat;

      //Iterate over all rows in selector Rows table
      foreach (DataRowView rowView in selector.RecordsView)
      {
        DiagnosticRecords.RecordsRow srcRow = (DiagnosticRecords.RecordsRow)rowView.Row;
        int RecTypeCount = selector.GetRecTypeCount(srcRow.TUInstanceID, srcRow.DDDVersionID, srcRow.RecordDefID);
        //Insert row into data grid
        dgvRecords.Rows.Add(new Object[] {
          srcRow.TUInstanceID, srcRow.RecordInstID, srcRow.DDDVersionID, srcRow.RecordDefID,
          (!srcRow.IsStartTimeNull()) ? (DateTime?)srcRow.StartTime : null,
          (!srcRow.IsEndTimeNull()) ? (DateTime?)srcRow.EndTime : null,
          (!srcRow.IsLastModifiedNull()) ? (DateTime?)srcRow.LastModified : null,
          srcRow.Source,
          (!srcRow.IsHierarchyItemIDNull()) ? (int?)srcRow.HierarchyItemID : null,
          (!srcRow.IsImportTimeNull()) ? (DateTime?)srcRow.ImportTime : null,
          (!srcRow.IsAcknowledgeTimeNull()) ? (DateTime?)srcRow.AcknowledgeTime : null,
          (!srcRow.IsVehicleNumberNull()) ? (int?)srcRow.VehicleNumber : null,
          (!srcRow.IsDeviceCodeNull()) ? (int?)srcRow.DeviceCode : null,
          srcRow.TUInstanceName, srcRow.TUTypeName, srcRow.DDDVersionName, srcRow.RecTitle,
          (!srcRow.IsFaultCodeNull()) ? (int?)srcRow.FaultCode : null,
          srcRow.RecordName, srcRow.TrainName, srcRow.VehicleName, srcRow.RecordType, 
          srcRow.FaultSeverity,
          (!srcRow.IsFaultSubSeverityNull()) ? (int?)srcRow.FaultSubSeverity : null,
          (!srcRow.IsTrcSnpCodeNull()) ? (int?)srcRow.TrcSnpCode : null,
          (!srcRow.IsGPSLatitudeNull()) ? (Single?)srcRow.GPSLatitude : null,
          (!srcRow.IsGPSLongitudeNull()) ? (Single?)srcRow.GPSLongitude : null,
          (RecTypeCount != -1) ? (int?)RecTypeCount : null
        });
        //Report progress
        processed++;
        if ((processed % 50) == 0)
          OnReportProgress((int)(((float)processed / (float)totalRecs) * 100), true);
      }
      //Resize grid view columns
      colRecTitle.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      dgvRecords.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
      colRecTitle.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
      //Hide progress bar
      OnReportProgress(0, false);
    }

    private void dgvRecords_SelectionChanged(object sender, EventArgs e)
    {
      Guid TUInstanceID = new Guid();
      Guid DDDVersionID = new Guid();
      Int64 RecordInstID = -1;
      int RecordDefID = -1 ;
      List<VarValue>  varValues;
      //Set wait cursor
      this.ParentForm.Cursor = Cursors.WaitCursor;
      try
      {
        //First clear values grid
        hvgVariables.ClearGrid();
        //Check if anything is selected
        if (dgvRecords.SelectedRows.Count == 0)
        {
          //Set cursor back to normal
          this.ParentForm.Cursor = Cursors.Default;
          return;
        }
        //Reset highilt of last start time
        if ((startTimeRow != -1) && (dgvRecords.Rows.Count > startTimeRow))
          dgvRecords.Rows[startTimeRow].Cells["colStartTime"].Style.SelectionBackColor = dgvRecords.DefaultCellStyle.SelectionBackColor;
        //Highlight start time
        startTimeRow = dgvRecords.SelectedRows[dgvRecords.SelectedRows.Count - 1].Index;
        dgvRecords.Rows[startTimeRow].Cells["colStartTime"].Style.SelectionBackColor = Color.Red;
        //Get startTime value for horizontal grid
        hvgVariables.StartTime = (DateTime)dgvRecords.Rows[startTimeRow].Cells["colStartTime"].Value;
        //Now iterate through selected rows
        foreach (DataGridViewRow row in dgvRecords.SelectedRows)
        {
          //Obtain parametrs from selected row
          TUInstanceID = (Guid)row.Cells["colTUInstanceID"].Value;
          RecordInstID = (Int64)row.Cells["colRecordInstID"].Value;
          DDDVersionID = (Guid)row.Cells["colDDDVersionID"].Value;
          RecordDefID = (int)row.Cells["colRecordDefID"].Value;
          //Get list of variable values
          varValues = selector.GetVarValuesForRecord(TUInstanceID, RecordInstID, RecordDefID);
          //Add values to horizontal grid
          hvgVariables.AddRecords(varValues, DDDVersionID, RecordDefID);
        }
        //Raise SelectRecord event using the selector
        if (dgvRecords.SelectedRows.Count > 0 && bRaiseSelectRecordEvent)
          selector.SelectRecord(TUInstanceID, RecordInstID, RecordDefID);
        //Adjust width of all displayed columns
        hvgVariables.ResizeColumns();
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRViewException(ex, "Failed to display enviromental variable values"));
      }
      //Set cursor back to normal
      this.ParentForm.Cursor = Cursors.Default;
    }
    #endregion //Helper methods

    #region Internal event handlers
    private void dgvRecords_ColumnDividerDoubleClick(object sender, DataGridViewColumnDividerDoubleClickEventArgs e)
    {
      //Resize grid view columns
      colRecTitle.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      dgvRecords.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
      colRecTitle.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
    }

    /// <summary>
    /// Handles click to records grid header - controls sorting
    /// Grid can be sorted by more than one column - depends on the state of Control key
    /// </summary>
    private void dgvRecords_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      if (Control.ModifierKeys == Keys.Control)
      { //control key is pressed - add clicked column to the list of sorted columns or just change sort direction
        if (recColsSortOrders[e.ColumnIndex] == 0)
        { //Column not sorted yet - add it as ascending sort at the end
          recordsSortColumns.Add(e.ColumnIndex);
          recColsSortOrders[e.ColumnIndex] = 1;
        }
        else
          recColsSortOrders[e.ColumnIndex] = recColsSortOrders[e.ColumnIndex] * (-1); //Change sort order for already sorted column
        //Set sort glyph
        if (recColsSortOrders[e.ColumnIndex] == 1)
          dgvRecords.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
        else
          dgvRecords.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Descending;
      }
      else
      { //control key is not pressed - only sort by one currently clicked column
        //First clear sort glyphs for all previously sorted columns
        foreach (int colIndex in recordsSortColumns)
          if (colIndex != e.ColumnIndex)
          {
            dgvRecords.Columns[colIndex].HeaderCell.SortGlyphDirection = SortOrder.None;
            recColsSortOrders[colIndex] = 0;
          }
        recordsSortColumns.Clear();
        //Now set current sort direction for clicked column
        if (recColsSortOrders[e.ColumnIndex] == 0)
        {
          recColsSortOrders[e.ColumnIndex] = 1;
          dgvRecords.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
        }
        else if (recColsSortOrders[e.ColumnIndex] == 1)
        {
          recColsSortOrders[e.ColumnIndex] = -1;
          dgvRecords.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Descending;
        }
        else
        {
          recColsSortOrders[e.ColumnIndex] = 0;
          dgvRecords.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.None;
        }
        //Add column index to list of sorted columns
        recordsSortColumns.Add(e.ColumnIndex);
      }
      //Sort the grid according to selected criteria
      dgvRecords.Sort(new RecordsRowComparer(recordsSortColumns, recColsSortOrders));
    }
    #endregion //Internal event handlers

    #region RecordsRowComparer class
    /// <summary>
    /// Class which is capable of comparing rows in records grid according to selected sorting criteria
    /// </summary>
    private class RecordsRowComparer : System.Collections.IComparer
    {
      private static List<int> recordsSortColumns; //list of column indexes, that shall be sorted against
      private static int[] recColsSortOrders; //array of sort orders for all columns in record grid

      public RecordsRowComparer(List<int> sortColumns, int[] sortOrders)
      {
        recordsSortColumns = sortColumns;
        recColsSortOrders = sortOrders;
      }

      public int Compare(object x, object y)
      {
        DataGridViewRow row1 = (DataGridViewRow)x;
        DataGridViewRow row2 = (DataGridViewRow)y;

        int CompareResult = 0;
        foreach (int colIndex in recordsSortColumns)
        {
          if (row1.Cells[colIndex].Value != null)
          {
            if (row2.Cells[colIndex].Value != null)
              CompareResult = ((IComparable)(row1.Cells[colIndex].Value)).CompareTo(row2.Cells[colIndex].Value) * recColsSortOrders[colIndex];
            else
              CompareResult = 1 * recColsSortOrders[colIndex];
          }
          else
          {
            if (row2.Cells[colIndex].Value != null)
              CompareResult = -1 * recColsSortOrders[colIndex];
            else
              CompareResult = 0;
          }
          if (CompareResult != 0)
            return CompareResult;
        }
        return CompareResult;
      }
    }
    #endregion //RecordsRowComparer class
  }
}

