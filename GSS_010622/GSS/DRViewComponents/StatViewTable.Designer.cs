namespace StatViewComponents
{
  partial class StatViewTable
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.gStat = new SourceGrid.Grid();
      this.SuspendLayout();
      // 
      // gStat
      // 
      this.gStat.Dock = System.Windows.Forms.DockStyle.Fill;
      this.gStat.Location = new System.Drawing.Point(0, 0);
      this.gStat.Name = "gStat";
      this.gStat.OptimizeMode = SourceGrid.CellOptimizeMode.ForRows;
      this.gStat.SelectionMode = SourceGrid.GridSelectionMode.Cell;
      this.gStat.Size = new System.Drawing.Size(708, 491);
      this.gStat.TabIndex = 0;
      this.gStat.TabStop = true;
      this.gStat.ToolTipText = "";
      // 
      // StatViewTable
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.Controls.Add(this.gStat);
      this.Name = "StatViewTable";
      this.Size = new System.Drawing.Size(708, 491);
      this.ResumeLayout(false);

    }

    #endregion

    private SourceGrid.Grid gStat;
  }
}
