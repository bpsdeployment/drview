namespace DRViewComponents
{
  partial class GraphPropDlg
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btShowSelected = new System.Windows.Forms.Button();
      this.btCancel = new System.Windows.Forms.Button();
      this.varTreeView = new DRViewComponents.VarTreeView();
      this.bgAnalogVars = new System.Windows.Forms.GroupBox();
      this.rbAnalogBoth = new System.Windows.Forms.RadioButton();
      this.rbAnalogLine = new System.Windows.Forms.RadioButton();
      this.rbAnalogPoints = new System.Windows.Forms.RadioButton();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.rbBoolLine = new System.Windows.Forms.RadioButton();
      this.rbBoolPoints = new System.Windows.Forms.RadioButton();
      this.chbCompleteVarTitle = new System.Windows.Forms.CheckBox();
      this.gbLegend = new System.Windows.Forms.GroupBox();
      this.tbcGraphProp = new System.Windows.Forms.TabControl();
      this.tpVariables = new System.Windows.Forms.TabPage();
      this.tpProperties = new System.Windows.Forms.TabPage();
      this.bgAnalogVars.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.gbLegend.SuspendLayout();
      this.tbcGraphProp.SuspendLayout();
      this.tpVariables.SuspendLayout();
      this.tpProperties.SuspendLayout();
      this.SuspendLayout();
      // 
      // btShowSelected
      // 
      this.btShowSelected.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btShowSelected.Location = new System.Drawing.Point(48, 358);
      this.btShowSelected.Name = "btShowSelected";
      this.btShowSelected.Size = new System.Drawing.Size(87, 23);
      this.btShowSelected.TabIndex = 0;
      this.btShowSelected.Text = "Show selected";
      this.btShowSelected.UseVisualStyleBackColor = true;
      // 
      // btCancel
      // 
      this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btCancel.Location = new System.Drawing.Point(192, 358);
      this.btCancel.Name = "btCancel";
      this.btCancel.Size = new System.Drawing.Size(75, 23);
      this.btCancel.TabIndex = 1;
      this.btCancel.Text = "Cancel";
      this.btCancel.UseVisualStyleBackColor = true;
      // 
      // varTreeView
      // 
      this.varTreeView.DDDHelper = null;
      this.varTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
      this.varTreeView.Location = new System.Drawing.Point(3, 3);
      this.varTreeView.Name = "varTreeView";
      this.varTreeView.ShowBits = true;
      this.varTreeView.Size = new System.Drawing.Size(300, 312);
      this.varTreeView.TabIndex = 2;
      this.varTreeView.UseFilter = true;
      // 
      // bgAnalogVars
      // 
      this.bgAnalogVars.Controls.Add(this.rbAnalogBoth);
      this.bgAnalogVars.Controls.Add(this.rbAnalogLine);
      this.bgAnalogVars.Controls.Add(this.rbAnalogPoints);
      this.bgAnalogVars.Location = new System.Drawing.Point(6, 6);
      this.bgAnalogVars.Name = "bgAnalogVars";
      this.bgAnalogVars.Size = new System.Drawing.Size(169, 81);
      this.bgAnalogVars.TabIndex = 4;
      this.bgAnalogVars.TabStop = false;
      this.bgAnalogVars.Text = "Analog variables";
      // 
      // rbAnalogBoth
      // 
      this.rbAnalogBoth.AutoSize = true;
      this.rbAnalogBoth.Location = new System.Drawing.Point(6, 58);
      this.rbAnalogBoth.Name = "rbAnalogBoth";
      this.rbAnalogBoth.Size = new System.Drawing.Size(86, 17);
      this.rbAnalogBoth.TabIndex = 7;
      this.rbAnalogBoth.Text = "Points + Line";
      this.rbAnalogBoth.UseVisualStyleBackColor = true;
      // 
      // rbAnalogLine
      // 
      this.rbAnalogLine.AutoSize = true;
      this.rbAnalogLine.Checked = true;
      this.rbAnalogLine.Location = new System.Drawing.Point(6, 39);
      this.rbAnalogLine.Name = "rbAnalogLine";
      this.rbAnalogLine.Size = new System.Drawing.Size(45, 17);
      this.rbAnalogLine.TabIndex = 6;
      this.rbAnalogLine.TabStop = true;
      this.rbAnalogLine.Text = "Line";
      this.rbAnalogLine.UseVisualStyleBackColor = true;
      // 
      // rbAnalogPoints
      // 
      this.rbAnalogPoints.AutoSize = true;
      this.rbAnalogPoints.Location = new System.Drawing.Point(6, 19);
      this.rbAnalogPoints.Name = "rbAnalogPoints";
      this.rbAnalogPoints.Size = new System.Drawing.Size(54, 17);
      this.rbAnalogPoints.TabIndex = 5;
      this.rbAnalogPoints.Text = "Points";
      this.rbAnalogPoints.UseVisualStyleBackColor = true;
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.rbBoolLine);
      this.groupBox1.Controls.Add(this.rbBoolPoints);
      this.groupBox1.Location = new System.Drawing.Point(6, 93);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(169, 67);
      this.groupBox1.TabIndex = 8;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Logical variables";
      // 
      // rbBoolLine
      // 
      this.rbBoolLine.AutoSize = true;
      this.rbBoolLine.Checked = true;
      this.rbBoolLine.Location = new System.Drawing.Point(6, 39);
      this.rbBoolLine.Name = "rbBoolLine";
      this.rbBoolLine.Size = new System.Drawing.Size(66, 17);
      this.rbBoolLine.TabIndex = 6;
      this.rbBoolLine.TabStop = true;
      this.rbBoolLine.Text = "Step line";
      this.rbBoolLine.UseVisualStyleBackColor = true;
      // 
      // rbBoolPoints
      // 
      this.rbBoolPoints.AutoSize = true;
      this.rbBoolPoints.Location = new System.Drawing.Point(6, 19);
      this.rbBoolPoints.Name = "rbBoolPoints";
      this.rbBoolPoints.Size = new System.Drawing.Size(54, 17);
      this.rbBoolPoints.TabIndex = 5;
      this.rbBoolPoints.Text = "Points";
      this.rbBoolPoints.UseVisualStyleBackColor = true;
      // 
      // chbCompleteVarTitle
      // 
      this.chbCompleteVarTitle.AutoSize = true;
      this.chbCompleteVarTitle.Location = new System.Drawing.Point(6, 19);
      this.chbCompleteVarTitle.Name = "chbCompleteVarTitle";
      this.chbCompleteVarTitle.Size = new System.Drawing.Size(163, 17);
      this.chbCompleteVarTitle.TabIndex = 9;
      this.chbCompleteVarTitle.Text = "Show complete variable titles";
      this.chbCompleteVarTitle.UseVisualStyleBackColor = true;
      // 
      // gbLegend
      // 
      this.gbLegend.Controls.Add(this.chbCompleteVarTitle);
      this.gbLegend.Location = new System.Drawing.Point(6, 166);
      this.gbLegend.Name = "gbLegend";
      this.gbLegend.Size = new System.Drawing.Size(169, 43);
      this.gbLegend.TabIndex = 9;
      this.gbLegend.TabStop = false;
      this.gbLegend.Text = "Legend";
      // 
      // tbcGraphProp
      // 
      this.tbcGraphProp.Controls.Add(this.tpVariables);
      this.tbcGraphProp.Controls.Add(this.tpProperties);
      this.tbcGraphProp.Location = new System.Drawing.Point(1, 2);
      this.tbcGraphProp.Name = "tbcGraphProp";
      this.tbcGraphProp.SelectedIndex = 0;
      this.tbcGraphProp.Size = new System.Drawing.Size(314, 344);
      this.tbcGraphProp.TabIndex = 10;
      // 
      // tpVariables
      // 
      this.tpVariables.Controls.Add(this.varTreeView);
      this.tpVariables.Location = new System.Drawing.Point(4, 22);
      this.tpVariables.Name = "tpVariables";
      this.tpVariables.Padding = new System.Windows.Forms.Padding(3);
      this.tpVariables.Size = new System.Drawing.Size(306, 318);
      this.tpVariables.TabIndex = 0;
      this.tpVariables.Text = "Variables";
      this.tpVariables.UseVisualStyleBackColor = true;
      // 
      // tpProperties
      // 
      this.tpProperties.Controls.Add(this.groupBox1);
      this.tpProperties.Controls.Add(this.gbLegend);
      this.tpProperties.Controls.Add(this.bgAnalogVars);
      this.tpProperties.Location = new System.Drawing.Point(4, 22);
      this.tpProperties.Name = "tpProperties";
      this.tpProperties.Padding = new System.Windows.Forms.Padding(3);
      this.tpProperties.Size = new System.Drawing.Size(306, 318);
      this.tpProperties.TabIndex = 1;
      this.tpProperties.Text = "Properties";
      this.tpProperties.UseVisualStyleBackColor = true;
      // 
      // GraphPropDlg
      // 
      this.AcceptButton = this.btShowSelected;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btCancel;
      this.ClientSize = new System.Drawing.Size(315, 392);
      this.Controls.Add(this.tbcGraphProp);
      this.Controls.Add(this.btCancel);
      this.Controls.Add(this.btShowSelected);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "GraphPropDlg";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Graph properties";
      this.bgAnalogVars.ResumeLayout(false);
      this.bgAnalogVars.PerformLayout();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.gbLegend.ResumeLayout(false);
      this.gbLegend.PerformLayout();
      this.tbcGraphProp.ResumeLayout(false);
      this.tpVariables.ResumeLayout(false);
      this.tpProperties.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btShowSelected;
    private System.Windows.Forms.Button btCancel;
    private VarTreeView varTreeView;
    private System.Windows.Forms.GroupBox bgAnalogVars;
    private System.Windows.Forms.RadioButton rbAnalogBoth;
    private System.Windows.Forms.RadioButton rbAnalogLine;
    private System.Windows.Forms.RadioButton rbAnalogPoints;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.RadioButton rbBoolLine;
    private System.Windows.Forms.RadioButton rbBoolPoints;
    private System.Windows.Forms.CheckBox chbCompleteVarTitle;
    private System.Windows.Forms.GroupBox gbLegend;
    private System.Windows.Forms.TabControl tbcGraphProp;
    private System.Windows.Forms.TabPage tpVariables;
    private System.Windows.Forms.TabPage tpProperties;
  }
}