using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DDDObjects;
using DRSelectorComponents;
using DSDiagnosticRecords;
using System.Diagnostics;

namespace DRViewComponents
{
  /// <summary>
  /// Displays diagnostic records and enviromental data in the form
  /// of hierarchical tree
  /// </summary>
  public partial class DRViewTreeList : DRViewBase
  {
    #region Private members
    //Dictionary with variable values
    private SortedDictionary<UInt64, VarValue> valuesDictionary = new SortedDictionary<ulong,VarValue>(); 
    //Dictionary with hierarchy item nodes (keyed by names)
    private Dictionary<String, HierarchyTreeListNode> hierarchyNodes = new Dictionary<string,HierarchyTreeListNode>();
    //List of top level nodes in view (always hierarchy nodes)
    private List<TreeListNode> topLevelNodes = new List<TreeListNode>();
    //List of column indexes in selector data set for defined record attributes
    private List<int> attrColIndexes = new List<int>();
    #endregion //Private members

    #region Construction initialization
    public DRViewTreeList()
    {
      InitializeComponent();
    }
    #endregion //Construction initialization

    #region Virtual method overrides
    protected override void OnNewDataReady(DRSelectorBase sender, NDREventArgs args)
    {
      try
      {
        //Call base implementation
        base.OnNewDataReady(sender, args);
        //Check if records shall be diasplayed
        if (!displayData)
          return;
        OnInfoMessage("Loading records into tree view");
        //Clear TreeListView
        treeListView.ClearItems();
        //Create nodes for TreeListView
        List<TreeListNode> nodes;
        nodes = CreateRecordsNodes();
        //Sort nodes
        nodes.Sort();
        //Display nodes
        treeListView.AddItems(nodes);
        OnInfoMessage("Records loaded into tree view");
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRViewException(ex, "Failed to load records into tree view"));
      }
    }
    #endregion //Virtual method overrides

    #region Helper methods
    private List<TreeListNode> CreateRecordsNodes()
    {
      DDDRecord dddRecord;
      RecordTreeListNode recordNode;
      TreeListNode attributesNode;
      String tmpStr;
      //Clear dictionary of hierarchy nodes and list of top level nodes
      hierarchyNodes.Clear();
      topLevelNodes.Clear();
      //Find column indexes for all defined view attributes
      attrColIndexes.Clear();
      DiagnosticRecords.RecordsDataTable table = (DiagnosticRecords.RecordsDataTable)selector.RecordsView.Table;
      foreach (ViewConfiguration.ViewAttribute attribute in viewConfiguration.Attributes)
        attrColIndexes.Add(table.Columns.IndexOf(attribute.DataField));
      //Obtain view of records from selector sorted over time, and hierarchy item names
      using (DataView sortedView = new DataView(selector.RecordsView.Table, selector.RecordsView.RowFilter,
                                               "TrainName, VehicleName, StartTime", selector.RecordsView.RowStateFilter))
      {
        //Iterate over all records rows in selector
        foreach (DataRowView rowView in sortedView)
        {
          DiagnosticRecords.RecordsRow recRow = (DiagnosticRecords.RecordsRow)rowView.Row;
          try
          {
            //Obtain record helper object
            dddRecord = dddHelper.GetRecord(recRow.DDDVersionID, recRow.RecordDefID); ;
            //Create record node
            tmpStr = (!recRow.IsEndTimeNull() && (recRow.EndTime > DateTime.MinValue)) ? recRow.EndTime.ToString(dddHelper.TimeFormat) : "";
            recordNode = new RecordTreeListNode(
              recRow.RecTitle, recRow.StartTime.ToString(dddHelper.TimeFormat),
              tmpStr, eTreeListImage.EventRec, recRow.StartTime.ToString("s"));
            recordNode.Nodes.Sortable = false;

            //Set node icon according to record type
            if (dddRecord.GetType() == typeof(DDDEventRecord))
              recordNode.ImageIndex = (int)eTreeListImage.EventRec;
            else if (dddRecord.GetType() == typeof(DDDTraceRecord))
              recordNode.ImageIndex = (int)eTreeListImage.TraceRec;
            else if (dddRecord.GetType() == typeof(DDDSnapRecord))
              recordNode.ImageIndex = (int)eTreeListImage.SnapRec;

            //Create attributes node
            tmpStr = (dddRecord.GetType() == typeof(DDDEventRecord)) ? ResTreeList.strFaultCode + ((DDDEventRecord)dddRecord).FaultCode.ToString() : "";
            attributesNode = new TreeListNode(
              ResTreeList.nodeAttributes, recRow.TUInstanceName, tmpStr, eTreeListImage.Attributes);
            attributesNode.Nodes.Sortable = false;
            //Add all configured attributes under attributes node
            for (int i = 0; i < attrColIndexes.Count; i++)
            {
              TreeListNode attrNode = 
                CreateAttributeNode(recRow, ViewConfiguration.Attributes[i], attrColIndexes[i]);
              attributesNode.Nodes.Add(attrNode);
            }
            //Add attributes node under record node
            recordNode.Nodes.Add(attributesNode);

            //Add node for enviromental data (empty)
            RecInstInfo instanceInfo = new RecInstInfo(recRow.TUInstanceID, recRow.RecordInstID, dddRecord);
            TreeListNode envDataNode = new TreeListNode(ResTreeList.nodeEnvData, "", "", eTreeListImage.EnvData, instanceInfo);
            envDataNode.Nodes.Sortable = false;
            recordNode.Nodes.Add(envDataNode);
            //Add record to hierarchy node
            AddRecordToHierarchy(recordNode, recRow);
          }
          catch (Exception ex)
          { //Error adding record to tree
            OnErrorMessage(new DRViewException(ex, "Failed to add record instance {0} from TU {1} to tree", recRow.RecordInstID, recRow.TUInstanceID));
          }
        }
      }
      return topLevelNodes;
    }

    /// <summary>
    /// Method creates tree list node with record attribute specified in attribute parameter.
    /// </summary>
    /// <param name="recRow">Row with diagnostic record from selector dataset</param>
    /// <param name="attribute">Record attribute</param>
    /// <param name="columnIndex">Index of column in selector dataset for specified attribute</param>
    /// <returns>Node with record attribute</returns>
    private TreeListNode CreateAttributeNode(
      DiagnosticRecords.RecordsRow recRow, ViewConfiguration.ViewAttribute attribute, int columnIndex)
    {
      String strValue = String.Empty;
      eTreeListImage nodeImage = eTreeListImage.GenAttribute;
      //Format the value of the attribute
      if (columnIndex >= 0 && columnIndex < recRow.ItemArray.Length && !recRow.IsNull(columnIndex))
      { //Specified column exists in table and has non null value
        Object value = recRow.ItemArray[columnIndex];
        if (value.GetType() == typeof(DateTime))
        {
          strValue = ((DateTime)value).ToString(dddHelper.TimeFormat);
          nodeImage = eTreeListImage.Time;
        }
        else
          strValue = value.ToString();
      }
      //Create and return node with attribute name and value (or empty string)
      return new TreeListNode(attribute.HeaderText, strValue, String.Empty, nodeImage);
    }

    /// <summary>
    /// Fills enviromental data node
    /// with child nodes for all inputs and var values.
    /// Called by TreeListView before node expansion.
    /// </summary>
    /// <param name="envDataNode">Enviromental data node to fill</param>
    private void FillEnvDataNode(TreeListNode envDataNode)
    {
      DDDInput dddInput;
      //First obtain RecinstInfo structure
      if ((envDataNode.Tag == null) || (envDataNode.Tag.GetType() != typeof(RecInstInfo)))
        return; //Node does not contain record instance information
      RecInstInfo instInfo = envDataNode.Tag as RecInstInfo;

      //Create dictionary for var values
      CreateValsDictionary(instInfo.TUInstanceID, instInfo.RecordInstID, instInfo.dddRecord.RecordDefID);
      //Create node for each input
      foreach (KeyValuePair<int, DDDInput> inpPair in instInfo.dddRecord.Inputs)
      { //iterate over all inputs of record
        dddInput = inpPair.Value;
        //Determine input type and add input node into enviromental data node
        if (dddInput.GetType() == typeof(DDDSimpleInput))
        {
          TreeListNode inpNode = CreateVarNode(dddInput.Variable, 0);
          inpNode.ImageIndex = (int)eTreeListImage.SimpleInput;
          envDataNode.Nodes.Add(inpNode);
        }
        else if (dddInput.GetType() == typeof(DDDHistorizedInput))
          envDataNode.Nodes.Add(CreateHistInputNode((DDDHistorizedInput)dddInput));
      }
      return;
    }

    /// <summary>
    /// Creates node for one variable. For simple variable adds value and timestamp as details,
    /// for structured variable creates child nodes.
    /// </summary>
    /// <param name="dddInput">Variable helper object</param>
    /// <param name="valueIndex">Index of value sample (0 for simple inputs)</param>
    /// <returns>TreeList node with variable</returns>
    private TreeListNode CreateVarNode(DDDVariable dddVariable, int valueIndex)
    {
      TreeListNode varNode;
      //Determine variable type (simple / structure)
      if (dddVariable.GetType() == typeof(DDDElemVar))
      {//Elementary variable
        varNode = CreateElemVarNode((DDDElemVar)dddVariable, valueIndex);
      }
      else if (dddVariable.GetType() == typeof(DDDStructure))
      { //Structured variable
        DDDStructure structVar = (DDDStructure)dddVariable;
        varNode = new TreeListNode(structVar.Title, "", "", eTreeListImage.Structure);
        varNode.Nodes.Sortable = false;
        //Recursively add child var nodes
        foreach (DDDVariable childVar in structVar.ChildVars)
          varNode.Nodes.Add(CreateVarNode(childVar, valueIndex));
      }
      else
      { //Unknown var type
        varNode = new TreeListNode("", "", "", eTreeListImage.Default);
        varNode.Nodes.Sortable = false;
      }
      return varNode;
    }

    /// <summary>
    /// Creates node for one historized input together with child nodes for
    /// all elements of history array and child variables.
    /// </summary>
    /// <param name="dddInput">Input helper object</param>
    /// <returns>TreeList node with input</returns>
    private TreeListNode CreateHistInputNode(DDDHistorizedInput dddInput)
    {
      TreeListNode inpNode;
      TreeListNode arrayNode;
      TreeListNode elemNode;
      int valueIndex = 0;
      String tmpStr;
      //Create input node
      tmpStr = String.Format("[{0}], [{1}], [{2}], [{3}]", dddInput.BeforeStart, dddInput.PastStart, dddInput.BeforeEnd, dddInput.PastEnd);
      inpNode = new TreeListNode(dddInput.Variable.Title, tmpStr, "", eTreeListImage.HistInput);
      inpNode.Nodes.Sortable = false;
      //Create Before start array
      if (dddInput.BeforeStart > 0)
      {
        //Create array node
        tmpStr = String.Format("[{0}]", dddInput.BeforeStart);
        arrayNode = new TreeListNode(ResTreeList.nodeBeforeStart, tmpStr, "", eTreeListImage.Array);
        arrayNode.Nodes.Sortable = false;
        //Create element nodes for each elemnt in array
        for (int i = 0; i < dddInput.BeforeStart; i++)
        { 
          elemNode = CreateVarNode(dddInput.Variable, valueIndex);
          //Change node text
          elemNode.Text = i.ToString();
          elemNode.ImageIndex = (int)eTreeListImage.SimpleInput;
          //Add node to array node
          arrayNode.Nodes.Add(elemNode);
          valueIndex++;
        }
        //add array node to input node
        inpNode.Nodes.Add(arrayNode);
      }
      //Create Past start array
      if (dddInput.PastStart > 0)
      {
        //Create array node
        tmpStr = String.Format("[{0}]", dddInput.PastStart);
        arrayNode = new TreeListNode(ResTreeList.nodePastStart, tmpStr, "", eTreeListImage.Array);
        arrayNode.Nodes.Sortable = false;
        //Create element nodes for each elemnt in array
        for (int i = 0; i < dddInput.PastStart; i++)
        {
          elemNode = CreateVarNode(dddInput.Variable, valueIndex);
          //Change node text
          elemNode.Text = i.ToString();
          elemNode.ImageIndex = (int)eTreeListImage.SimpleInput;
          //Add node to array node
          arrayNode.Nodes.Add(elemNode);
          valueIndex++;
        }
        //add array node to input node
        inpNode.Nodes.Add(arrayNode);
      }
      //Create Before end array
      if (dddInput.BeforeEnd > 0)
      {
        //Create array node
        tmpStr = String.Format("[{0}]", dddInput.BeforeEnd);
        arrayNode = new TreeListNode(ResTreeList.nodeBeforeEnd, tmpStr, "", eTreeListImage.Array);
        arrayNode.Nodes.Sortable = false;
        //Create element nodes for each elemnt in array
        for (int i = 0; i < dddInput.BeforeEnd; i++)
        {
          elemNode = CreateVarNode(dddInput.Variable, valueIndex);
          //Change node text
          elemNode.Text = i.ToString();
          elemNode.ImageIndex = (int)eTreeListImage.SimpleInput;
          //Add node to array node
          arrayNode.Nodes.Add(elemNode);
          valueIndex++;
        }
        //add array node to input node
        inpNode.Nodes.Add(arrayNode);
      }
      //Create Past end array
      if (dddInput.PastEnd > 0)
      {
        //Create array node
        tmpStr = String.Format("[{0}]", dddInput.PastEnd);
        arrayNode = new TreeListNode(ResTreeList.nodePastEnd, tmpStr, "", eTreeListImage.Array);
        arrayNode.Nodes.Sortable = false;
        //Create element nodes for each elemnt in array
        for (int i = 0; i < dddInput.PastEnd; i++)
        {
          elemNode = CreateVarNode(dddInput.Variable, valueIndex);
          //Change node text
          elemNode.Text = i.ToString();
          elemNode.ImageIndex = (int)eTreeListImage.SimpleInput;
          //Add node to array node
          arrayNode.Nodes.Add(elemNode);
          valueIndex++;
        }
        //add array node to input node
        inpNode.Nodes.Add(arrayNode);
      }
      //Return input node
      return inpNode;
    }

    /// <summary>
    /// Creates dictionary with var values for given record.
    /// </summary>
    /// <param name="TUInstanceID">Identification of TUInstance</param>
    /// <param name="RecordInstID">Identification of Record instance</param>
    /// <param name="RecordDefID">Identification of Record instance</param>
    private void CreateValsDictionary(Guid TUInstanceID, Int64 RecordInstID, Int32 RecordDefID)
    {
      UInt64 valKey;
      //Clear the dictionary
      valuesDictionary.Clear();
      //Obtain value rows from selector
      List<VarValue> varValues;
      varValues = selector.GetVarValuesForRecord(TUInstanceID, RecordInstID, RecordDefID);
      //Add rows to dictionary
      foreach (VarValue varValue in varValues)
      {
        valKey = GetValKey((ulong)varValue.VariableID, (ulong)varValue.ValueIndex);
        valuesDictionary.Add(valKey, varValue);
      }
    }

    /// <summary>
    /// Constructs 64bit key to values dictionary.
    /// </summary>
    /// <param name="VariableID">Variable identification</param>
    /// <param name="ValueIndex">Index of value sample</param>
    /// <returns>Key to dictionary</returns>
    private UInt64 GetValKey(UInt64 VariableID, UInt64 ValueIndex)
    {
      return ((VariableID << 32) | ValueIndex);
    }

    /// <summary>
    /// Creates node for one elementary variable. 
    /// Adds value and timestamp as details,
    /// for bitset variables, adds child nodes representing bits.
    /// </summary>
    /// <param name="dddInput">Variable helper object</param>
    /// <param name="valueIndex">Index of value sample (0 for simple inputs)</param>
    /// <returns>TreeList node with variable</returns>
    private TreeListNode CreateElemVarNode(DDDElemVar dddVariable, int valueIndex)
    {
      TreeListNode varNode;
      VarValue varValue;
      String value = "";
      String time = "";
      //Obtain variable value from dictionary
      if (!valuesDictionary.TryGetValue(GetValKey((ulong)dddVariable.VariableID, (ulong)valueIndex), out varValue))
      { //no value found
        return new TreeListNode(dddVariable.Title, "", "", eTreeListImage.Default);
      }
      //Format time
      time = varValue.TimeStamp.ToString(dddHelper.TimeFormat);
      if (!dddVariable.IsBitSet())
      { //Simple variable
        value = dddVariable.FormatValue(varValue.Value);
        varNode = new TreeListNode(dddVariable.Title, value, time, eTreeListImage.ElemVar);
        varNode.Nodes.Sortable = false;
        //Add physical unit, if possible
        if ((dddVariable.Representation.GetType() == typeof(DDDSimpleRepres)) &&
             (((DDDSimpleRepres)dddVariable.Representation).UnitSymbol != ""))
          value += " [" + ((DDDSimpleRepres)dddVariable.Representation).UnitSymbol + "]";
      }
      else
      { //Variable is bitset - add bit nodes
        //Create node for bitset
        varNode = new TreeListNode(dddVariable.Title, "", "", eTreeListImage.Structure);
        varNode.Nodes.Sortable = false;
        //Obtain representation
        DDDBitsetRepres bitRepres = dddVariable.Representation as DDDBitsetRepres;
        String bitName;
        foreach (KeyValuePair<ulong, String> maskVal in bitRepres.GetMasksAndValues(varValue.Value))
        { //Iterate over all bit values
          //Get bit name
          bitName = bitRepres.GetBitTitle(maskVal.Key);
          //Add bit node
          varNode.Nodes.Add(new TreeListNode(bitName, maskVal.Value, time, eTreeListImage.ElemVar));
        }
      }
      //Return node
      return varNode;
    }

    /// <summary>
    /// Adds record node under one of hierarchy nodes in the view.
    /// If necessary, new hierarchy node is created.
    /// Based on record parameters it is added under one of following hierarchy nodes
    /// (in order of precedence):
    ///   -Vehicle node
    ///   -Train node
    ///   -TUInstance node
    /// </summary>
    /// <param name="recNode">Record node to add under some hierarchy node</param>
    /// <param name="recRow">Row with record parameters</param>
    private void AddRecordToHierarchy(RecordTreeListNode recNode, DiagnosticRecords.RecordsRow recRow)
    {
      //Obtain hierarchy node 
      String hierItemName = String.Empty;
      HierarchyTreeListNode hierNode;
      //Obtain name of hierarchy item
      if (recRow.VehicleName != String.Empty)
        hierItemName = recRow.VehicleName;
      else if (recRow.TrainName != String.Empty)
        hierItemName = recRow.TrainName;
      else
        hierItemName = recRow.TUInstanceName;
      //Try to find the item in dictionary
      if (!hierarchyNodes.TryGetValue(hierItemName, out hierNode))
        hierNode = CreateHierarchyNode(recRow); //New hierarchy node has to be created
      //Add record to hierarchy node
      hierNode.AddRecordNode(recNode, recRow.StartTime);
    }

    /// <summary>
    /// Creates new hierarchy node in the view, which can contain given record
    /// </summary>
    /// <param name="recRow">Row with record containing information about hierarchy node to create</param>
    private HierarchyTreeListNode CreateHierarchyNode(DiagnosticRecords.RecordsRow recRow)
    {
      //Obtain or create parent node
      HierarchyTreeListNode parentNode = null;
      if (recRow.TrainName != String.Empty && recRow.VehicleName != String.Empty)
      {//Create or obtain parent hierarchy node from train name
        if (!hierarchyNodes.TryGetValue(recRow.TrainName, out parentNode))
        { //has to create train node
          parentNode = new HierarchyTreeListNode(recRow.TrainName, "", "", eTreeListImage.Train, recRow.TrainName);
          parentNode.Nodes.Sortable = false;
          //Add to list of top level nodes and dictionary of hierarchy nodes
          topLevelNodes.Add(parentNode);
          hierarchyNodes.Add(parentNode.Text, parentNode);
        }
      }
      //Create hierarchy node for the record
      HierarchyTreeListNode hierNode;
      if (recRow.VehicleName != String.Empty)
        hierNode = new HierarchyTreeListNode(recRow.VehicleName, "", "", eTreeListImage.Vehicle, recRow.VehicleName);
      else if (recRow.TrainName != String.Empty && parentNode == null)
        hierNode = new HierarchyTreeListNode(recRow.TrainName, "", "", eTreeListImage.Train, recRow.TrainName);
      else
        hierNode = new HierarchyTreeListNode(recRow.TUInstanceName, "", "", eTreeListImage.TUName, recRow.TUInstanceName);
      hierNode.Nodes.Sortable = false;
      //Add hierarchy node to dictionary
      hierarchyNodes.Add(hierNode.Text, hierNode);
      //Add to parent node or list of top level nodes
      if (parentNode != null)
        parentNode.Nodes.Add(hierNode);
      else
        topLevelNodes.Add(hierNode);
      //return hierarchy node
      return hierNode;
    }
    #endregion //Helper methods
  }
  
  #region RecInstInfo class
  internal class RecInstInfo
  {
    public Guid TUInstanceID;
    public Int64 RecordInstID;
    public DDDRecord dddRecord;

    public RecInstInfo(Guid tuInstanceID, Int64 recordInstID, DDDRecord dddRecord)
    {
      this.TUInstanceID = tuInstanceID;
      this.RecordInstID = recordInstID;
      this.dddRecord = dddRecord;
    }
  }
  #endregion //RecInstInfo class
}
