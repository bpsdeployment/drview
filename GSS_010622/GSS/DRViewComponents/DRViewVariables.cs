using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DDDObjects;
using DRSelectorComponents;
using DSDiagnosticRecords;

namespace DRViewComponents
{
  #region Delegates definitions
  /// <summary>
  /// Delegate defines event handler for GraphVariables event
  /// </summary>
  /// <param name="sender">Sender of the event</param>
  /// <param name="selectedVariables">Selected variables which shall be displayed in graph</param>
  public delegate void GraphVariablesEventHandler(object sender, VarIDDictionary selectedVariables);
  #endregion //Delegates definitions
  
  public partial class DRViewVariables : DRViewBase
  {
    #region Public Events
    /// <summary>
    /// Event occurs when selected variables shall be displayed in graph
    /// </summary>
    public event GraphVariablesEventHandler OnGraphVariables;
    #endregion //Public Events

    #region Construction initialization
    public DRViewVariables()
    {
      InitializeComponent();
    }
    #endregion //Construction initialization

    #region Virtual methods overrides
    /// <summary>
    /// Displays list of available variables in tree view.
    /// </summary>
    /// <param name="sender"></param>
    protected override void OnNewDataReady(DRSelectorBase sender, NDREventArgs args)
    {
      try
      {
        //call base implementation
        base.OnNewDataReady(sender, args);
        //Check if control shall display data
        if (!displayData)
          return;
        OnInfoMessage("Loading variables list into variables view");

        //Clear variable grid
        vertVariableGrid.Clear();
        //Remember selected variables
        VarIDDictionary selVariables = varTreeView.GetSelectedVarIDs();
        //Fill tree view with available variables
        varTreeView.CreateVarTree(selector.RecordsView, selector.VariableFilter);
        //Check again previously selected variables
        varTreeView.CheckSelectedVars(selVariables);
        //Set time formatting
        vertVariableGrid.TimeFormat = dddHelper.TimeFormat;
        //Show again selected variables
        btShowVars_Click(this, null);
        OnInfoMessage("Variables list loaded");
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRViewException(ex, "Failed to display data in variables view"));
      }
    }

    protected override void SetDDDHelper(DDDHelper helper)
    {
      base.SetDDDHelper(helper);
      vertVariableGrid.DDDHelper = helper;
      varTreeView.DDDHelper = helper;
    }
    #endregion //Virtual methods overrides

    #region Event handlers
    private void btShowVars_Click(object sender, EventArgs e)
    {
      OnInfoMessage("Displaying variables...");
      try
      {
        //Stop grid redrawing
        vertVariableGrid.BeginUpdate();
        //Clear grid
        vertVariableGrid.Clear();
        //Display variables
        vertVariableGrid.AddVariablesToGrid(varTreeView.GetSelectedVarIDs(), selector);
        //Redraw grid
        vertVariableGrid.EndUpdate();
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRViewException(ex, "Failed to display values of selected variables"));
      }
      OnInfoMessage("Variables displayed");
    }

    private void btClear_Click(object sender, EventArgs e)
    {
      //Stop grid redrawing
      vertVariableGrid.BeginUpdate();
      //Clear variable grid
      vertVariableGrid.Clear();
      //Uncheck all check boxes in tree
      varTreeView.UncheckAll();
      //Redraw grid
      vertVariableGrid.EndUpdate();
    }
    
    private void btGraph_Click(object sender, EventArgs e)
    {
      if (OnGraphVariables != null)
        OnGraphVariables(this, varTreeView.GetSelectedVarIDs());
    }
    #endregion //Event handlers
  }
}
