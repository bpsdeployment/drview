using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DDDObjects;
using Gigasoft.ProEssentials.Enums;
using DRSelectorComponents;

namespace DRViewComponents
{
  public partial class GraphPropDlg : Form
  {
    #region Public properties
    public SGraphPlottingMethods AnalogPlottingMethod
    {
      get
      {
        if (rbAnalogPoints.Checked)
          return SGraphPlottingMethods.Point;
        else if (rbAnalogLine.Checked)
          return SGraphPlottingMethods.Line;
        else
          return SGraphPlottingMethods.PointsPlusLine;
      }
    }

    public SGraphPlottingMethods BoolPlottingMethod
    {
      get
      {
        if (rbBoolPoints.Checked)
          return SGraphPlottingMethods.Point | SGraphPlottingMethods.OnRightAxis;
        else 
          return SGraphPlottingMethods.Step | SGraphPlottingMethods.OnRightAxis;
      }
    }

    public VarIDDictionary SelectedVars
    {
      get { return varTreeView.GetSelectedVarIDs(); }
    }

    public DDDHelper DDDHelper
    {
      set { varTreeView.DDDHelper = value; }
    }

    public bool ShowCompVarTitle
    {
      get { return chbCompleteVarTitle.Checked; }
    }
    #endregion //Public properties

    #region Construction initialization
    public GraphPropDlg()
    {
      InitializeComponent();
    }
    #endregion //Construction initialization

    #region Public methods
    /// <summary>
    /// Creates tree of variables from selected records
    /// </summary>
    public void CreateVarTree(DataView recTableView, VarIDDictionary varFilter, VarIDDictionary selectedVariables)
    {
      varTreeView.CreateVarTree(recTableView, varFilter);
      if (selectedVariables != null)
        varTreeView.CheckSelectedVars(selectedVariables);
    }
    #endregion //Public methods
  }
}