using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DDDObjects;
using StatSelectorComponents;

namespace StatViewComponents
{
  /// <summary>
  /// Base class for all Diagnostic records visualisation components
  /// </summary>
  public partial class StatViewBase : UserControl
  {
    #region Members
    protected DDDHelper dddHelper = null;  //class providing DDD objects functionality
    protected StatSelectorBase selector = null; //component providing selected statistical data to display
    protected bool displayData = true; //New data shall be displayed after NewDataReady event
    #endregion //Members

    #region Public properties
    /// <summary>
    /// DDD objects functionality
    /// </summary>
    public DDDHelper DDDHelper
    {
      set { SetDDDHelper(value); }
      get { return dddHelper; }
    }

    /// <summary>
    /// New data shall be displayed from selector after NDR event
    /// </summary>
    public bool DisplayData
    {
      set 
      {
        if (!displayData && value && (selector != null) && (dddHelper != null))
        { //Control transfers from invisible to visible - display selected records
          displayData = value;
          //Set wait cursor
          this.ParentForm.Cursor = Cursors.WaitCursor;
          //Raise event
          OnNewDataReady(selector, new NDREventArgs());
          //Set cursor back to normal
          this.ParentForm.Cursor = Cursors.Default;
        }
        displayData = value; 
      }
      get { return displayData; }
    }

    /// <summary>
    /// Attach this handler to the source of NewLanguageSelected event
    /// </summary>
    public NewLanguageSelectedEventHandler NewLanguageHandler;
    /// <summary>
    /// Attach this handler to the source of NewDataReady event (Selector component)
    /// </summary>
    public NDREventHandler NewDataReadyHandler;
    #endregion //Public properties

    #region Public Events
    /// <summary>
    /// Event occurs when there is an information message to pass to component owner
    /// </summary>
    public event InfoMessageEventhandler InfoMessage;
    /// <summary>
    /// Event occurs when there is an error encountered during selection process
    /// </summary>
    public event ErrorMessageEventhandler ErrorMessage;
    #endregion //Public Events

    #region Construction and initialization
    /// <summary>
    /// Default constructor
    /// </summary>
    public StatViewBase()
    {
      InitializeComponent();
      //Initialize public delegates
      NewLanguageHandler = OnNewLanguageSelected;
      NewDataReadyHandler = OnNewDataReady;
    }
    #endregion //Construction and initialization

    #region Event raisers
    /// <summary>
    /// Method raises InfoMessage event
    /// </summary>
    protected void OnInfoMessage(String message)
    {
      if (InfoMessage != null)
        InfoMessage(this, message);
    }

    /// <summary>
    /// Raises ErrorMessage event
    /// </summary>
    /// <param name="error">Exception describing the error</param>
    protected void OnErrorMessage(Exception error)
    {
      if (ErrorMessage != null)
        ErrorMessage(this, error);
    }

    /// <summary>
    /// Method raises InfoMessage event
    /// </summary>
    protected void OnInfoMessage(String message, params Object[] args)
    {
      if (InfoMessage != null)
        InfoMessage(this, String.Format(message, args));
    }
    #endregion //Event raisers

    #region Virtual prototypes
    /// <summary>
    /// Method handles the event of language selection.
    /// Method should be overriden in derived classes to perform all necessary actions.
    /// Base class iplementation has to called in derived classes. 
    /// </summary>
    /// <param name="language">New selected language</param>
    protected virtual void OnNewLanguageSelected(Languages language)
    {
    }

    /// <summary>
    /// Method handles NewDataReady event raised by SelectorComponent.
    /// Method has to be overwriten in derived classes.
    /// Base class iplementation has to called in derived classes. 
    /// </summary>
    /// <param name="sender"></param>
    protected virtual void OnNewDataReady(StatSelectorBase sender, NDREventArgs args)
    {
      selector = sender;
    }

    /// <summary>
    /// Stores reference to assigned DDDHelper object.
    /// Derived classes may perform any necessary actions.
    /// </summary>
    /// <param name="helper">Assigned DDDHelper object</param>
    protected virtual void SetDDDHelper(DDDHelper helper)
    {
      this.dddHelper = helper;
    }
    #endregion //Virtual prototypes
  }

  #region StatView exception
  /// <summary>
  /// Exception class thrown by all Stat View objects
  /// </summary>
  class StatViewException : Exception
  {
    public StatViewException(String message, params Object[] args)
      : base(String.Format(message, args))
    { }
    public StatViewException(Exception inner, String message, params Object[] args)
      : base(String.Format(message, args), inner)
    { }
  }
  #endregion //StatView exception
}