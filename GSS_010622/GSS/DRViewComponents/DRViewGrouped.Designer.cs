namespace DRViewComponents
{
  partial class DRViewGrouped
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.Windows.Forms.TreeListViewItemCollection.TreeListViewItemCollectionComparer treeListViewItemCollectionComparer1 = new System.Windows.Forms.TreeListViewItemCollection.TreeListViewItemCollectionComparer();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DRViewGrouped));
      this.treeListView = new System.Windows.Forms.TreeListView();
      this.colRecTitle = new System.Windows.Forms.ColumnHeader();
      this.colRecCount = new System.Windows.Forms.ColumnHeader();
      this.colStartTime = new System.Windows.Forms.ColumnHeader();
      this.colEndTime = new System.Windows.Forms.ColumnHeader();
      this.colFaultCode = new System.Windows.Forms.ColumnHeader();
      this.colSource = new System.Windows.Forms.ColumnHeader();
      this.imgListIcons = new System.Windows.Forms.ImageList(this.components);
      this.SuspendLayout();
      // 
      // treeListView
      // 
      this.treeListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colRecTitle,
            this.colRecCount,
            this.colStartTime,
            this.colEndTime,
            this.colFaultCode,
            this.colSource});
      treeListViewItemCollectionComparer1.Column = 0;
      treeListViewItemCollectionComparer1.SortOrder = System.Windows.Forms.SortOrder.None;
      this.treeListView.Comparer = treeListViewItemCollectionComparer1;
      this.treeListView.Dock = System.Windows.Forms.DockStyle.Fill;
      this.treeListView.GridLines = true;
      this.treeListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
      this.treeListView.HideSelection = false;
      this.treeListView.Location = new System.Drawing.Point(0, 0);
      this.treeListView.MultiSelect = false;
      this.treeListView.Name = "treeListView";
      this.treeListView.ShowGroups = false;
      this.treeListView.Size = new System.Drawing.Size(591, 308);
      this.treeListView.SmallImageList = this.imgListIcons;
      this.treeListView.Sorting = System.Windows.Forms.SortOrder.None;
      this.treeListView.TabIndex = 0;
      this.treeListView.UseCompatibleStateImageBehavior = false;
      this.treeListView.BeforeExpand += new System.Windows.Forms.TreeListViewCancelEventHandler(this.treeListView_BeforeExpand);
      this.treeListView.SizeChanged += new System.EventHandler(this.treeListView_SizeChanged);
      this.treeListView.BeforeCollapse += new System.Windows.Forms.TreeListViewCancelEventHandler(this.treeListView_BeforeCollapse);
      // 
      // colRecTitle
      // 
      this.colRecTitle.Text = "Title";
      this.colRecTitle.Width = 118;
      // 
      // colRecCount
      // 
      this.colRecCount.Text = "Count";
      this.colRecCount.Width = 123;
      // 
      // colStartTime
      // 
      this.colStartTime.Text = "Start Time";
      this.colStartTime.Width = 90;
      // 
      // colEndTime
      // 
      this.colEndTime.Text = "End Time";
      this.colEndTime.Width = 85;
      // 
      // colFaultCode
      // 
      this.colFaultCode.Text = "Fault Code";
      this.colFaultCode.Width = 85;
      // 
      // colSource
      // 
      this.colSource.Text = "Source";
      this.colSource.Width = 86;
      // 
      // imgListIcons
      // 
      this.imgListIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgListIcons.ImageStream")));
      this.imgListIcons.TransparentColor = System.Drawing.Color.Transparent;
      this.imgListIcons.Images.SetKeyName(0, "Event.ICO");
      this.imgListIcons.Images.SetKeyName(1, "EnvData.ICO");
      // 
      // DRViewGrouped
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.Controls.Add(this.treeListView);
      this.Name = "DRViewGrouped";
      this.Size = new System.Drawing.Size(591, 308);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TreeListView treeListView;
    private System.Windows.Forms.ColumnHeader colRecCount;
    private System.Windows.Forms.ColumnHeader colRecTitle;
    private System.Windows.Forms.ColumnHeader colStartTime;
    private System.Windows.Forms.ColumnHeader colEndTime;
    private System.Windows.Forms.ColumnHeader colFaultCode;
    private System.Windows.Forms.ColumnHeader colSource;
    private System.Windows.Forms.ImageList imgListIcons;
  }
}
