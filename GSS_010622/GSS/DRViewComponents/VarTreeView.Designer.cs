namespace DRViewComponents
{
  partial class VarTreeView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VarTreeView));
      this.treeView = new System.Windows.Forms.TreeView();
      this.imageList = new System.Windows.Forms.ImageList(this.components);
      this.SuspendLayout();
      // 
      // treeView
      // 
      this.treeView.CheckBoxes = true;
      this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
      this.treeView.ImageIndex = 0;
      this.treeView.ImageList = this.imageList;
      this.treeView.ItemHeight = 17;
      this.treeView.Location = new System.Drawing.Point(0, 0);
      this.treeView.Name = "treeView";
      this.treeView.SelectedImageIndex = 1;
      this.treeView.Size = new System.Drawing.Size(249, 270);
      this.treeView.TabIndex = 0;
      this.treeView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterCheck);
      // 
      // imageList
      // 
      this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
      this.imageList.TransparentColor = System.Drawing.Color.Transparent;
      this.imageList.Images.SetKeyName(0, "Emty.ico");
      this.imageList.Images.SetKeyName(1, "Selected.ico");
      this.imageList.Images.SetKeyName(2, "StructIcon.ico");
      this.imageList.Images.SetKeyName(3, "SimpleVarIcon.ico");
      this.imageList.Images.SetKeyName(4, "BitIcon.ico");
      // 
      // VarTreeView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.treeView);
      this.Name = "VarTreeView";
      this.Size = new System.Drawing.Size(249, 270);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TreeView treeView;
    private System.Windows.Forms.ImageList imageList;
  }
}
