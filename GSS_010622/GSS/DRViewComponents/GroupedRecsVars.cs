using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Data;
using DDDObjects;
using DRSelectorComponents;

namespace DRViewComponents
{
  /// <summary>
  /// Component represents table where records and values of
  /// environmental variables are groupped together in one table.
  /// Each row in the table represents one timestamp from one TUInstance.
  /// Each column represents one of the following:
  ///   - attribute of diagnostic record (Train, FaultCode...). 
  ///     In this case only rows which represent record have these column filled.
  ///   - value of environmental variable. 
  ///     In this case only row where variable value exists have this column filled.
  /// </summary>
  public partial class GroupedRecsVars : Component
  {
    #region Private members
    /// <summary>
    /// List of column indexes in selector dataset 
    /// One index for each record attribute column present in grouped table.
    /// Rec attribute columns are always the first columns in grouped table.
    /// First column is always TimeStamp column
    /// </summary>
    private List<int> recAttrColIndexes;
    /// <summary>
    /// Two level dictionary of rows in DataTable. 
    /// First level is divided by TUInstanceID 
    ///   (two records with same timestamp from different TUInstances have different rows)
    /// Second level is divided by timestamp
    /// </summary>
    private Dictionary<Guid, Dictionary<DateTime, DataRow>> rowDictionary;
    /// <summary>
    /// Two level dictionary mapping selected variables to columns in data table.
    /// First level is divided by DDDVersionID
    /// Second level is divided by VariableID.
    /// </summary>
    private Dictionary<Guid, Dictionary<int, SelectedVariable>> varDictionary;
    #endregion

    #region Public properties
    /// <summary>
    /// Data table with grouped records and variable values
    /// </summary>
    public DataTable GroupedTable;
    /// <summary>
    /// Header for each data table column
    /// </summary>
    public List<String> ColumnHeaders;
    /// <summary>
    /// Format string for each data table column
    /// </summary>
    public List<String> ColumnFormatStrings;
    /// <summary>
    /// DDD Helper object
    /// </summary>
    public DDDHelper DDDHelper;
    /// <summary>
    /// DR Selector object which provides the data
    /// </summary>
    public DRSelectorBase Selector;
    #endregion

    #region Construction, initialization
    /// <summary>
    /// Standard constructor
    /// </summary>
    public GroupedRecsVars()
    {
      InitializeComponent();
      //Initialize members
      Initialize();
    }
    /// <summary>
    /// Standard constructor
    /// </summary>
    public GroupedRecsVars(IContainer container)
    {
      container.Add(this);
      InitializeComponent();
      //Initialize members
      Initialize();
    }
    /// <summary>
    /// Initialize members
    /// </summary>
    private void Initialize()
    {
      //Create members
      recAttrColIndexes = new List<int>();
      rowDictionary = new Dictionary<Guid, Dictionary<DateTime, DataRow>>();
      varDictionary = new Dictionary<Guid, Dictionary<int, SelectedVariable>>();
      GroupedTable = new DataTable();
      ColumnHeaders = new List<string>();
      ColumnFormatStrings = new List<string>();
    }
    #endregion 


    #region Internal classes
    /// <summary>
    /// Represents one elementary environmental variable
    /// included in grouped table.
    /// </summary>
    private class SelectedVariable
    {
      /// <summary>
      /// Elementary variable DDD object
      /// </summary>
      public DDDElemVar Variable;
      /// <summary>
      /// Index of corresponding column in grouped table
      /// </summary>
      public int ColumnIndex;
      /// <summary>
      /// Standard constructor
      /// </summary>
      public SelectedVariable(DDDElemVar var, int colIndx)
      {
        this.ColumnIndex = colIndx;
        this.Variable = var;
      }
    }
    /// <summary>
    /// Represents selected bits of bitset variable
    /// included in grouped table.
    /// </summary>
    private class SelectedBits : SelectedVariable
    {
      /// <summary>
      /// List of selected bit masks
      /// </summary>
      List<Int64> BitMasks;
      /// <summary>
      /// Standard constructor
      /// </summary>
      public SelectedBits(DDDElemVar var, int colIndx)
        : base(var, colIndx) //Call parent constructor
      {
      }
    }
    #endregion
  }
}
