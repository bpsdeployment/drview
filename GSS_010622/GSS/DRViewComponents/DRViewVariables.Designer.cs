namespace DRViewComponents
{
  partial class DRViewVariables
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new System.Windows.Forms.Label();
      this.splitContainer = new System.Windows.Forms.SplitContainer();
      this.btGraph = new System.Windows.Forms.Button();
      this.btClear = new System.Windows.Forms.Button();
      this.btShowVars = new System.Windows.Forms.Button();
      this.varTreeView = new DRViewComponents.VarTreeView();
      this.label2 = new System.Windows.Forms.Label();
      this.vertVariableGrid = new DRViewComponents.VertVariableGrid();
      this.splitContainer.Panel1.SuspendLayout();
      this.splitContainer.Panel2.SuspendLayout();
      this.splitContainer.SuspendLayout();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(3, 3);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(98, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Available variables:";
      // 
      // splitContainer
      // 
      this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer.Location = new System.Drawing.Point(0, 0);
      this.splitContainer.Name = "splitContainer";
      // 
      // splitContainer.Panel1
      // 
      this.splitContainer.Panel1.Controls.Add(this.btGraph);
      this.splitContainer.Panel1.Controls.Add(this.btClear);
      this.splitContainer.Panel1.Controls.Add(this.btShowVars);
      this.splitContainer.Panel1.Controls.Add(this.varTreeView);
      this.splitContainer.Panel1.Controls.Add(this.label1);
      // 
      // splitContainer.Panel2
      // 
      this.splitContainer.Panel2.Controls.Add(this.label2);
      this.splitContainer.Panel2.Controls.Add(this.vertVariableGrid);
      this.splitContainer.Size = new System.Drawing.Size(821, 474);
      this.splitContainer.SplitterDistance = 271;
      this.splitContainer.TabIndex = 3;
      // 
      // btGraph
      // 
      this.btGraph.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btGraph.Location = new System.Drawing.Point(191, 445);
      this.btGraph.Name = "btGraph";
      this.btGraph.Size = new System.Drawing.Size(75, 23);
      this.btGraph.TabIndex = 5;
      this.btGraph.Text = "Show graph";
      this.btGraph.UseVisualStyleBackColor = true;
      this.btGraph.Click += new System.EventHandler(this.btGraph_Click);
      // 
      // btClear
      // 
      this.btClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btClear.Location = new System.Drawing.Point(105, 445);
      this.btClear.Name = "btClear";
      this.btClear.Size = new System.Drawing.Size(75, 23);
      this.btClear.TabIndex = 4;
      this.btClear.Text = "Clear";
      this.btClear.UseVisualStyleBackColor = true;
      this.btClear.Click += new System.EventHandler(this.btClear_Click);
      // 
      // btShowVars
      // 
      this.btShowVars.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btShowVars.Location = new System.Drawing.Point(5, 445);
      this.btShowVars.Name = "btShowVars";
      this.btShowVars.Size = new System.Drawing.Size(89, 23);
      this.btShowVars.TabIndex = 3;
      this.btShowVars.Text = "Show selected";
      this.btShowVars.UseVisualStyleBackColor = true;
      this.btShowVars.Click += new System.EventHandler(this.btShowVars_Click);
      // 
      // varTreeView
      // 
      this.varTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.varTreeView.DDDHelper = null;
      this.varTreeView.Location = new System.Drawing.Point(0, 19);
      this.varTreeView.Name = "varTreeView";
      this.varTreeView.ShowBits = true;
      this.varTreeView.Size = new System.Drawing.Size(269, 420);
      this.varTreeView.TabIndex = 2;
      this.varTreeView.UseFilter = true;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(2, 3);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(142, 13);
      this.label2.TabIndex = 4;
      this.label2.Text = "Values of selected variables:";
      // 
      // vertVariableGrid
      // 
      this.vertVariableGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.vertVariableGrid.DDDHelper = null;
      this.vertVariableGrid.Location = new System.Drawing.Point(0, 19);
      this.vertVariableGrid.Name = "vertVariableGrid";
      this.vertVariableGrid.Size = new System.Drawing.Size(546, 455);
      this.vertVariableGrid.TabIndex = 0;
      // 
      // DRViewVariables
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.splitContainer);
      this.Name = "DRViewVariables";
      this.Size = new System.Drawing.Size(821, 474);
      this.splitContainer.Panel1.ResumeLayout(false);
      this.splitContainer.Panel1.PerformLayout();
      this.splitContainer.Panel2.ResumeLayout(false);
      this.splitContainer.Panel2.PerformLayout();
      this.splitContainer.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.SplitContainer splitContainer;
    private VertVariableGrid vertVariableGrid;
    private System.Windows.Forms.Label label2;
    private VarTreeView varTreeView;
    private System.Windows.Forms.Button btShowVars;
    private System.Windows.Forms.Button btClear;
    private System.Windows.Forms.Button btGraph;
  }
}
