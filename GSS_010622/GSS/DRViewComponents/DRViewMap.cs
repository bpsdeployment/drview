using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Globalization;
using System.Security.Permissions;
using DDDObjects;
using DSDiagnosticRecords;
using DRSelectorComponents;

namespace DRViewComponents
{
  #region Delegate definitions
  /// <summary>
  /// Defines handler for MapViewChange event
  /// </summary>
  /// <param name="sender">Sender of the event</param>
  public delegate void MapViewChangeEventhandler(DRViewMap sender);
  #endregion //Delegate definitions

  public partial class DRViewMap : DRViewBase
  {
    #region Public enums
    /// <summary>
    /// Specifies which set of records shall be shown on the map
    /// </summary>
    public enum eChangeShownRecs
    {
      /// <summary>
      /// Show first record
      /// </summary>
      ShowFirst,
      /// <summary>
      /// Show previous set
      /// </summary>
      ShowPrevious,
      /// <summary>
      /// Show next set
      /// </summary>
      ShowNext,
      /// <summary>
      /// Show last record
      /// </summary>
      ShowLast
    }

    /// <summary>
    /// Direction into which map shall be moved
    /// </summary>
    public enum ePanMapDirection
    {
      Up,
      Down,
      Left,
      Right
    }

    /// <summary>
    /// Direction for map zooming
    /// </summary>
    public enum eMapZoomDir
    {
      ZoomIn,
      ZoomOut
    }
    #endregion Public enums

    #region Private members
    private DataView recordsView; //View of records from selector, sorted by StartTime
    private bool bHtmlLoaded; //Html page is loaded into the browser
    private int maxRecsPerTab = 10; //Maximum number of records shown on one info tab
    private int maxTabCount = 5; //Maximum number of tabs displayed in one info window
    private String tabCaptionBase = "Records"; //Caption for each tab of marker info window (tab index is added)
    private String invGPSIconURL = "http://maps.google.com/mapfiles/kml/pal3/icon37.png"; //URL of icon used for markers with invalid GPS data
    private String invGPSText = "Invalid GPS data"; //Text displayed instead of GPS position for invalid GPS data
    private String moreRecsTitle = "More records, click for details..."; //Title of marker, which contains more than one record
    private int currFirstRecIndx = 0; //Index of currently first record displayed on the map
    private int maxRecordsInMap = 100; //Maximum number of records displayed in the map at one time
    private double minMarkerDist = 100; //Minimum distance between two neighbrhood markers
    private bool showTUNames = false; //Show name of TU together with record title in info window
    private int currLastRecIndx = 0; //Index of currently shown last record
    private DateTime firstStartTime; //Start time of current first shown record
    private DateTime lastStartTime; //Start time of current last shown record
    private SortedList<GPSPoint, List<int>> markerList = null; //Sorted list of markers and lists of record indexes displayed currently in the map
    private MapHelperComVisible mapHelper = null;
    #endregion //Private members

    #region Public properties
    /// <summary>
    /// Maximum number of records shown on one info tab
    /// </summary>
    public int MaxRecsPerTab
    {
      get { return maxRecsPerTab; }
      set { maxRecsPerTab = value; }
    }

    /// <summary>
    /// Maximum number of tabs displayed in one info window
    /// </summary>
    public int MaxTabCount
    {
      get { return maxTabCount; }
      set { maxTabCount = value; }
    }

    /// <summary>
    /// Caption for each tab of marker info window 
    /// (tab index is added)
    /// </summary>
    public String TabCaptionBase
    {
      get { return tabCaptionBase; }
      set { tabCaptionBase = value; }
    }

    /// <summary>
    /// URL of icon used for markers with invalid GPS data
    /// </summary>
    public String InvGPSIconURL
    {
      get { return invGPSIconURL; }
      set { invGPSIconURL = value; }
    }

    /// <summary>
    /// Text displayed instead of GPS position for invalid GPS data
    /// </summary>
    public String InvGPSText
    {
      get { return invGPSText; }
      set { invGPSText = value; }
    }

    /// <summary>
    /// Title of marker, which contains more than one record
    /// </summary>
    public String MoreRecsTitle
    {
      get { return moreRecsTitle; }
      set { moreRecsTitle = value; }
    }

    /// <summary>
    /// Show name of TU together with record title in info window
    /// </summary>
    public bool ShowTUNames
    {
      get { return showTUNames; }
      set { showTUNames = value; }
    }

    /// <summary>
    /// Index of currently shown first record
    /// </summary>
    public int CurrFirstRecIndx
    {
      get { return currFirstRecIndx; }
      set { currFirstRecIndx = value; }
    }

    /// <summary>
    /// Index of currently shown last record
    /// </summary>
    public int CurrLastRecIndx
    {
      get { return currLastRecIndx; }
      set { currLastRecIndx = value; }
    }

    /// <summary>
    /// Start time of current first shown record
    /// </summary>
    public DateTime FirstStartTime
    {
      get { return firstStartTime; }
      set { firstStartTime = value; }
    }

    /// <summary>
    ///Start time of current last shown record
    /// </summary>
    public DateTime LastStartTime
    {
      get { return lastStartTime; }
      set { lastStartTime = value; }
    }
    #endregion //Public properties

    #region Public Events
    /// <summary>
    /// Event occurs when some change occurs in map (displayed record changes...)
    /// </summary>
    public event MapViewChangeEventhandler MapViewChange;
    #endregion

    #region Construction initialization
    public DRViewMap()
    {
      InitializeComponent();
      bHtmlLoaded = false;
      //Set this component as object callable from java script inside of the browser
      mapHelper = new MapHelperComVisible(this);
      browser.ObjectForScripting = mapHelper;
    }
    #endregion //Construction initialization

    #region Virtual methods overrides
    /// <summary>
    /// Called each time new set of records is available from the selector
    /// </summary>
    protected override void OnNewDataReady(DRSelectorBase sender, NDREventArgs args)
    {
      try
      {
        //Call base implementation
        base.OnNewDataReady(sender, args);
        //Check if data shall be displayed
        if (!displayData)
          return;
        //Create view of records from the selector
        recordsView = new DataView(selector.RecordsView.Table, selector.RecordsView.RowFilter, "StartTime", DataViewRowState.CurrentRows);
        //Show records from the view on the map
        if (!bHtmlLoaded)
          LoadMapHtml();
        //Set properties of range controls
        tbRecords.Maximum = ((recordsView.Count + maxRecordsInMap - 1) / maxRecordsInMap - 1) * maxRecordsInMap;
        if (tbRecords.Maximum < 0)
          tbRecords.Maximum = 0;
        tbRecords.TickFrequency = maxRecordsInMap;
        tbRecords.LargeChange = maxRecordsInMap;
        tbRecords.Value = 0;
        currFirstRecIndx = 0;
        //Display new records
        DisplayRecMarkers();
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRViewException(ex, "Failed to load records into map view"));
      }
    }

    /// <summary>
    /// Handles the on select record event from selector
    /// Selected record is highlighted on the map
    /// </summary>
    /// <param name="sender">Sender of the event</param>
    /// <param name="args">Event parameters</param>
    protected override void OnSelectRecord(DRSelectorBase sender, SelectRecordEventArgs args)
    {
      try
      { //Call base implementation
        base.OnSelectRecord(sender, args);
        if (!bHtmlLoaded || markerList == null)
          return;
        //Find the record in the view
        DiagnosticRecords.RecordsRow recTableRow = (DiagnosticRecords.RecordsRow)recordsView.Table.Rows[args.DataTableIndex];
        int recViewIndex = -1;
        for(int i = recordsView.Find(recTableRow.StartTime); i < recordsView.Count; i++)
          if (recordsView.Table.Rows.IndexOf(recordsView[i].Row) == args.DataTableIndex)
          {
            recViewIndex = i;
            break;
          }
        if (recViewIndex == -1)
          return; //Record not found in the view
        //Check if selected record is currently displayed on the map
        if (!(recViewIndex >= currFirstRecIndx && recViewIndex <= currLastRecIndx))
        { //Record is not currently displayed, display it first
          currFirstRecIndx = Math.Max(0, ((recViewIndex + maxRecordsInMap - 1) / maxRecordsInMap -1) * maxRecordsInMap);
          tbRecords.Value = currFirstRecIndx;
          DisplayRecMarkers();
        }
        //Now find index of marker in currently displayed batch
        int markerIndex = markerList.IndexOfKey(GetRecordPoint(recTableRow));
        if (markerIndex == -1)
          return; //Marker not found
        //Call javascript method which selects given marker and record
        browser.Document.InvokeScript("selectRecord", new object[] { markerIndex, recViewIndex });
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRViewException(ex, "Failed to select record in map view"));
      }
    }
    #endregion

    #region Public methods
    /// <summary>
    /// Change displayed set of records on the map 
    /// </summary>
    /// <param name="changeDir">Direction of the change</param>
    public void ChangeDisplayedRecords(eChangeShownRecs changeDir)
    {
      switch (changeDir)
      {
        case eChangeShownRecs.ShowFirst:
          FirstRecIndxChanged(btGoFirstRec, null);
          break;
        case eChangeShownRecs.ShowNext:
          FirstRecIndxChanged(btGoNextRec, null);
          break;
        case eChangeShownRecs.ShowPrevious:
          FirstRecIndxChanged(btGoPrevRec, null);
          break;
        case eChangeShownRecs.ShowLast:
          FirstRecIndxChanged(btGoLastRec, null);
          break;
      }
    }

    /// <summary>
    /// Moves the map in specified direction
    /// </summary>
    /// <param name="direction">Direction to move</param>
    public void PanMap(ePanMapDirection direction)
    {
      if (!bHtmlLoaded)
        return; //Map page is not loaded yet
      switch (direction)
      {
        case ePanMapDirection.Up:
          browser.Document.InvokeScript("panUp");
          break;
        case ePanMapDirection.Down:
          browser.Document.InvokeScript("panDown");
          break;
        case ePanMapDirection.Left:
          browser.Document.InvokeScript("panLeft");
          break;
        case ePanMapDirection.Right:
          browser.Document.InvokeScript("panRight");
          break;
      }
    }

    /// <summary>
    /// Zooms the map in or out
    /// </summary>
    /// <param name="zoomDir">Zoom direction</param>
    public void ZoomMap(eMapZoomDir zoomDir)
    {
      if (!bHtmlLoaded)
        return; //Map page is not loaded yet
      switch (zoomDir)
      {
        case eMapZoomDir.ZoomIn:
          browser.Document.InvokeScript("zoomIn");
          break;
        case eMapZoomDir.ZoomOut:
          browser.Document.InvokeScript("zoomOut");
          break;
      }
    }

    /// <summary>
    /// Method called from jscript code when user clicks on a hyperlink in info window
    /// Raises OnSelectRecord event using the selector - record is selected in other views
    /// </summary>
    /// <param name="recordIndex">Index of record in view</param>
    public void SelectRecord(int recordIndex)
    {
      try
      {
        //Call selectors select record method
        selector.SelectRecord(recordsView.Table.Rows.IndexOf(recordsView[recordIndex].Row));
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRViewException(ex, "Failed to select record from map"));
      }
    }
    #endregion //Public methods

    #region Helper methods
    /// <summary>
    /// Method displays markers for diagnostic records
    /// </summary>
    protected void DisplayRecMarkers()
    {
      if (!bHtmlLoaded)
        return; //Map page is not loaded yet
      OnInfoMessage("Displaying records in map view...");
      //Create XML document with markers
      XmlDocument doc = CreateMarkersXML();
      //Show the document on the map
      browser.Document.InvokeScript("addMarkers", new object[] { doc.OuterXml, ShowTUNames });
      //Set text of label
      DislayRecRangeText();
      OnInfoMessage("Records displayed in map view");
    }

    /// <summary>
    /// Method loads map html document into the browser
    /// </summary>
    protected void LoadMapHtml()
    {
      try
      {
        OnInfoMessage("Loading map into map view...");
        bHtmlLoaded = false;
        //Create and set map url
        Uri url = new Uri(Path.Combine(Application.StartupPath, "Map\\map.html"));
        browser.Url = url;
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRViewException(ex, "Failed to load map page into the browser"));
      }
    }

    /// <summary>
    /// Creates dictionary of markers and record indexes using view of records,
    /// index of first record and maximum number of records shown on the map
    /// </summary>
    /// <returns>Dictionary with markers and record indexes</returns>
    protected SortedList<GPSPoint, List<int>> CreateMarkerList()
    {
      //Create empty dictionary
      SortedList<GPSPoint, List<int>> markerList = new SortedList<GPSPoint, List<int>>();
      //Iterate over records in view and use GPS coordinates to create markers...
      for (int i = currFirstRecIndx;
          (i < (currFirstRecIndx + maxRecordsInMap)) && (i < recordsView.Count);
          i++)
      {
        DiagnosticRecords.RecordsRow recRow = (DiagnosticRecords.RecordsRow)recordsView[i].Row;
        //Create GPS point for record
        GPSPoint point = GetRecordPoint(recRow);
        //Add point to dictionary if necessary
        if (!markerList.ContainsKey(point))
          markerList.Add(point, new List<int>());
        //Add index of record to marker representing the point
        markerList[point].Add(i);
      }
      //Retrun the dictionary
      return markerList;
    }

    /// <summary>
    /// Creates XML document with set of markers and records from current marker dictionary
    /// </summary>
    /// <returns>XML document with markers</returns>
    protected XmlDocument CreateMarkersXML()
    {
      //First create dictionary with markers
      markerList = CreateMarkerList();
      //Create number format info object for GPS coordinates
      NumberFormatInfo numFormat = new NumberFormatInfo();
      numFormat.NumberDecimalSeparator = ".";      
      //Create empty XML document
      XmlDocument doc = new XmlDocument();
      //Create document element
      doc.AppendChild(doc.CreateElement("markers"));
      //Iterate over all markers in dictionary
      foreach (KeyValuePair<GPSPoint, List<int>> marker in markerList)
      {
        //Create marker element
        XmlElement markerElem = doc.CreateElement("marker");
        markerElem.SetAttribute("lat", marker.Key.Latitude.ToString(numFormat));
        markerElem.SetAttribute("lng", marker.Key.Longitude.ToString(numFormat));
        //Iterate over all rows associated with marker, append elements
        String title = "";
        foreach (int i in marker.Value)
        { //Create recInfo element and add to marker element
          DiagnosticRecords.RecordsRow recRow = (DiagnosticRecords.RecordsRow)recordsView[i].Row;
          //Create element and set attributes
          XmlElement recElem = doc.CreateElement("recinfo");
          recElem.SetAttribute("starttime", recRow.StartTime.ToString(DDDHelper.TimeFormat));
          recElem.SetAttribute("tuname", recRow.TUInstanceName);
          recElem.SetAttribute("recindx", i.ToString());
          title = recRow.RecTitle;
          recElem.InnerText = " ";
          recElem.SetAttribute("title", title);
          markerElem.AppendChild(recElem);
        }
        //Set title to marker element
        if (marker.Key.Latitude == 0 && marker.Key.Longitude == 0)
          markerElem.SetAttribute("title", InvGPSText);
        else if (marker.Value.Count > 1)
          markerElem.SetAttribute("title", MoreRecsTitle);
        else
          markerElem.SetAttribute("title", title);
        //Append marker elemnt to document
        doc.DocumentElement.AppendChild(markerElem);
      }
      //Return document
      return doc;
    }

    /// <summary>
    /// Updates text of label with current range of records
    /// </summary>
    private void DislayRecRangeText()
    {
      lbRecords.Text = "";
      if (recordsView.Count <= 0)
        return;
      currLastRecIndx = Math.Min(recordsView.Count - 1, currFirstRecIndx + maxRecordsInMap - 1);
      firstStartTime = ((DiagnosticRecords.RecordsRow)recordsView[currFirstRecIndx].Row).StartTime;
      lastStartTime = ((DiagnosticRecords.RecordsRow)recordsView[currLastRecIndx].Row).StartTime;
      lbRecords.Text = "Records " +
        currFirstRecIndx.ToString() + " - " + currLastRecIndx.ToString() + "\n" +
        " (" + firstStartTime.ToString(DDDHelper.TimeFormat) + " - " + lastStartTime.ToString(DDDHelper.TimeFormat) + ")";
      lbRecords.Left = lbRecords.Parent.Width / 2 - lbRecords.Width / 2;
      //Call event notifying of map change
      if (MapViewChange != null)
        MapViewChange(this);
    }

    /// <summary>
    /// Creates and returns GPS point object for given diagnostic record row
    /// </summary>
    /// <param name="recRow">Row with the record</param>
    /// <returns>GPS point object</returns>
    private GPSPoint GetRecordPoint(DiagnosticRecords.RecordsRow recRow)
    {
      if (recRow.IsGPSLatitudeNull() || recRow.IsGPSLongitudeNull() ||
          recRow.GPSLatitude == 0 || recRow.GPSLongitude == 0)
        return new GPSPoint(0, 0, minMarkerDist); //Invalid GPS coordinates for given record
      else
        return new GPSPoint(recRow.GPSLatitude, recRow.GPSLongitude, minMarkerDist); //Valid GPS data
    }
    #endregion //Helper methods

    #region Event handlers
    /// <summary>
    /// Event raised when browser completes loading of the document
    /// </summary>
    private void browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
    {
      try
      {
        //Set flag, that document is loaded in the browesr
        bHtmlLoaded = true;
        //Run script which creates the map
        browser.Document.InvokeScript("loadMap",
          new object[] { MaxRecsPerTab, TabCaptionBase, InvGPSIconURL, InvGPSText });
        OnInfoMessage("Map loaded into map view");
        //Display current set of records
        DisplayRecMarkers();
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRViewException(ex, "Failed to create map object in browser"));
      }
    }

    /// <summary>
    /// Event raised each time any control for first record index is clicked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void FirstRecIndxChanged(object sender, EventArgs e)
    {
      //Store index of first record
      int prevFirstRecIndx = currFirstRecIndx;
      //Determine if event was raised by one of the buttons and correct index
      if (sender == btGoFirstRec)
        currFirstRecIndx = 0;
      else if (sender == btGoPrevRec)
        currFirstRecIndx = Math.Max(0, currFirstRecIndx - maxRecordsInMap);
      else if (sender == btGoNextRec)
      {
        if ((currFirstRecIndx + maxRecordsInMap) < recordsView.Count)
          currFirstRecIndx = currFirstRecIndx + maxRecordsInMap;
      }
      else if (sender == btGoLastRec)
        currFirstRecIndx = Math.Max(0, ((recordsView.Count + maxRecordsInMap - 1) / maxRecordsInMap - 1) * maxRecordsInMap);
      //Set the value of track bar
      tbRecords.Value = currFirstRecIndx;
      //Display records
      if (currFirstRecIndx != prevFirstRecIndx)
        DisplayRecMarkers();
    }

    /// <summary>
    /// Show properties panel
    /// </summary>
    private void btShowProp_Click(object sender, EventArgs e)
    {
      if (bHtmlLoaded)
        panProp.Visible = true;
    }

    /// <summary>
    /// Hide properties panel
    /// </summary>
    private void btHideProp_Click(object sender, EventArgs e)
    {
      panProp.Visible = false;
    }

    /// <summary>
    /// Raised when maximum number of records 
    /// which can be displayed on the map at one time has changed
    /// </summary>
    private void nudMaxRecCount_ValueChanged(object sender, EventArgs e)
    {
      maxRecordsInMap = (int)nudMaxRecCount.Value;
      //Correct markers on track bar
      tbRecords.TickFrequency = maxRecordsInMap;
      tbRecords.LargeChange = maxRecordsInMap;
      //Correct index of first displayed record
      if (currFirstRecIndx + maxRecordsInMap >= recordsView.Count)
        currFirstRecIndx = Math.Max(0, ((recordsView.Count + maxRecordsInMap - 1) / maxRecordsInMap - 1) * maxRecordsInMap);
      //Display records 
      DisplayRecMarkers();
    }

    /// <summary>
    /// Called when user moves the track bar which selects first diaplayed record
    /// </summary>
    private void tbRecords_ValueChanged(object sender, EventArgs e)
    {
      if (currFirstRecIndx != tbRecords.Value && tbRecords.Value >= 0)
      {
        currFirstRecIndx = tbRecords.Value;
        DisplayRecMarkers();
      }
    }

    /// <summary>
    /// Called when minimum distance between markers has changed
    /// </summary>
    private void nudMinMarkerDist_ValueChanged(object sender, EventArgs e)
    {
      minMarkerDist = (double)nudMinMarkerDist.Value;
      DisplayRecMarkers();
    }

    /// <summary>
    /// Called when request to display TU names in info tabs has changed
    /// </summary>
    private void chbShowTUNames_CheckedChanged(object sender, EventArgs e)
    {
      showTUNames = chbShowTUNames.Checked;
      DisplayRecMarkers();
    }
    #endregion //Event handlers
  }

  #region COMVisible helper
  /// <summary>
  /// COM visible class used as an mediator between calls from jscript code and map view
  /// </summary>
  [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
  [System.Runtime.InteropServices.ComVisibleAttribute(true)]
  public class MapHelperComVisible
  {
    /// <summary>
    /// Reference to parent map view
    /// </summary>
    private DRViewMap parent;

    /// <summary>
    /// Constructor, accepts reference to parent map view
    /// </summary>
    public MapHelperComVisible(DRViewMap parent)
    {
      this.parent = parent;
    }

    /// <summary>
    /// Method called from jscript code when user clicks on a hyperlink in info window
    /// Calls implementation in parent map view class
    /// </summary>
    /// <param name="recordIndex">Index of record in view</param>
    public void SelectRecord(int recordIndex)
    {
      parent.SelectRecord(recordIndex);
    }
  }
  #endregion //COMVisible helper
}
