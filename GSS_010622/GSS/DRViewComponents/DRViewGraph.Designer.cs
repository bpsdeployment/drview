namespace DRViewComponents
{
  partial class DRViewGraph
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.graph = new Gigasoft.ProEssentials.Pesgo();
      this.btSelectVars = new System.Windows.Forms.Button();
      this.btClose = new System.Windows.Forms.Button();
      this.btReset = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // graph
      // 
      this.graph.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.graph.Location = new System.Drawing.Point(14, 14);
      this.graph.Name = "graph";
      this.graph.Size = new System.Drawing.Size(655, 372);
      this.graph.TabIndex = 0;
      this.graph.Text = "pesgo1";
      this.graph.PeCustomMenu += new Gigasoft.ProEssentials.Pesgo.CustomMenuEventHandler(this.graph_PeCustomMenu);
      this.graph.PeCustomGridNumber += new Gigasoft.ProEssentials.Pesgo.CustomGridNumberEventHandler(this.graph_PeCustomGridNumber);
      // 
      // btSelectVars
      // 
      this.btSelectVars.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btSelectVars.Location = new System.Drawing.Point(14, 392);
      this.btSelectVars.Name = "btSelectVars";
      this.btSelectVars.Size = new System.Drawing.Size(95, 23);
      this.btSelectVars.TabIndex = 1;
      this.btSelectVars.Text = "Select variables";
      this.btSelectVars.UseVisualStyleBackColor = true;
      this.btSelectVars.Click += new System.EventHandler(this.btSelectVars_Click);
      // 
      // btClose
      // 
      this.btClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btClose.Location = new System.Drawing.Point(224, 392);
      this.btClose.Name = "btClose";
      this.btClose.Size = new System.Drawing.Size(95, 23);
      this.btClose.TabIndex = 2;
      this.btClose.Text = "Close graph";
      this.btClose.UseVisualStyleBackColor = true;
      this.btClose.Click += new System.EventHandler(this.btClose_Click);
      // 
      // btReset
      // 
      this.btReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btReset.Location = new System.Drawing.Point(128, 392);
      this.btReset.Name = "btReset";
      this.btReset.Size = new System.Drawing.Size(75, 23);
      this.btReset.TabIndex = 3;
      this.btReset.Text = "Reset";
      this.btReset.UseVisualStyleBackColor = true;
      this.btReset.Click += new System.EventHandler(this.btReset_Click);
      // 
      // DRViewGraph
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.Controls.Add(this.btReset);
      this.Controls.Add(this.btClose);
      this.Controls.Add(this.btSelectVars);
      this.Controls.Add(this.graph);
      this.Name = "DRViewGraph";
      this.Size = new System.Drawing.Size(683, 420);
      this.ResumeLayout(false);

    }

    #endregion

    private Gigasoft.ProEssentials.Pesgo graph;
    private System.Windows.Forms.Button btSelectVars;
    private System.Windows.Forms.Button btClose;
    private System.Windows.Forms.Button btReset;
  }
}
