using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace DRViewComponents
{
  /// <summary>
  /// Visual control providing the functionality of DataGridView with multiple
  /// header rows and mergeable cells in header
  /// </summary>
  public partial class MergeGridView : UserControl
  {
    #region Members
    protected ArrayList headerGrids; //Grids representing header lines
    protected int headerCount = 1; //Number of header rows
    #endregion //Members

    #region Public properties
    /// <summary>
    /// Main DataGridView representing all displayed data
    /// </summary>
    public DataGridView GridView
    {
      get { return mainGrid; }
    }

    public DataGridViewColumnCollection Columns
    {
      get { return mainGrid.Columns; }
    }

    /// <summary>
    /// number of header rows
    /// </summary>
    public int HeaderCount
    {
      get { return headerCount; }
      set { headerCount = value; }
    }
    #endregion //Public properties

    #region Initialization
    /// <summary>
    /// Public default constructor
    /// </summary>
    public MergeGridView()
    {
      InitializeComponent();
      headerGrids = new ArrayList();
      CreateHeaderGrids();
      SetGridsSizes();
    }

    /// <summary>
    /// Creates empty grid components
    /// </summary>
    private void CreateHeaderGrids()
    {
      this.SuspendLayout();

      //Create header grid controls
      //First remove old grid components
      foreach (DataGridView grid in headerGrids)
      {
        Controls.Remove(grid);
        grid.Dispose();
      }
      headerGrids.Clear();
      //Now create new header grids
      DataGridView headerGrid;
      for (int i = 0; i < headerCount; i++)
      {
        headerGrid = new DataGridView();
        ((System.ComponentModel.ISupportInitialize)(headerGrid)).BeginInit();

        headerGrid.AllowUserToAddRows = false;
        headerGrid.AllowUserToDeleteRows = false;
        headerGrid.AllowUserToResizeRows = false;
        headerGrid.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
        headerGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
        headerGrid.BorderStyle = BorderStyle.None;
        headerGrid.CausesValidation = false;
        headerGrid.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
        headerGrid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        headerGrid.ColumnHeadersVisible = true;
        headerGrid.EnableHeadersVisualStyles = false;
        headerGrid.ReadOnly = true;
        headerGrid.RowHeadersVisible = false;
        headerGrid.ScrollBars = System.Windows.Forms.ScrollBars.None;
        ((System.ComponentModel.ISupportInitialize)(headerGrid)).EndInit();
        Controls.Add(headerGrid);
        headerGrids.Add(headerGrid);
      }

      this.ResumeLayout(false);
    }

    /// <summary>
    /// Sets size and location for each grid
    /// </summary>
    private void SetGridsSizes()
    {
      int headerHeight = mainGrid.ColumnHeadersHeight;
      Size gridRect = new Size(this.Width - vScrollBar.Width, this.Height - hScrollBar.Height);
      //Set position and size of main grid
      mainGrid.Location = new Point(0, headerHeight * headerCount);
      mainGrid.Width = gridRect.Width;
      if ((gridRect.Height - (headerHeight * headerCount)) > 0)
        mainGrid.Height = gridRect.Height - (headerHeight * headerCount);
      else
        mainGrid.Height = 0;
      //Set position and size for each header grid
      DataGridView headerGrid;
      for (int i = 0; i < headerGrids.Count; i++)
      {
        headerGrid = (DataGridView)headerGrids[i];
        headerGrid.Location = new Point (0, i * headerHeight);
        headerGrid.Width = gridRect.Width;
        headerGrid.Height = headerHeight;
      }
    }

    protected override void OnSizeChanged(EventArgs e)
    {
      base.OnSizeChanged(e);
      SetGridsSizes();
    }

    /// <summary>
    /// Sets internal DataGridViews event handlers
    /// </summary>
    private void SetGridsEventHandlers()
    {
      /*
      this.dgvMerge.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvMerge_ColumnHeaderMouseClick);
      this.dgvMerge.SizeChanged += new System.EventHandler(this.dgvMerge_SizeChanged);
      this.dgvMerge.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgvMerge_Scroll);
      this.dgvMerge.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dgvMerge_ColumnWidthChanged_1);
      this.dgvMerge.Resize += new System.EventHandler(this.dgvMerge_Resize);
      */
    }
    #endregion //Initialization

  }
}
