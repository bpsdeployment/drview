namespace DRViewComponents
{
  partial class HorizVariableGrid
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.dgvValues = new System.Windows.Forms.DataGridView();
      this.cntxMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.visibleColumnsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.tUNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.dDDVersionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.variableTitleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.physicalUintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.visibleRowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.headerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.relativeTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.relativeTimeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
      this.millisecondsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.secondsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.minutesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.secondsMillisecondsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.minSecmsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      ((System.ComponentModel.ISupportInitialize)(this.dgvValues)).BeginInit();
      this.cntxMenu.SuspendLayout();
      this.SuspendLayout();
      // 
      // dgvValues
      // 
      this.dgvValues.AllowUserToAddRows = false;
      this.dgvValues.AllowUserToDeleteRows = false;
      this.dgvValues.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvValues.ColumnHeadersVisible = false;
      this.dgvValues.ContextMenuStrip = this.cntxMenu;
      this.dgvValues.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgvValues.EnableHeadersVisualStyles = false;
      this.dgvValues.Location = new System.Drawing.Point(0, 0);
      this.dgvValues.Name = "dgvValues";
      this.dgvValues.ReadOnly = true;
      this.dgvValues.RowHeadersVisible = false;
      this.dgvValues.RowTemplate.Height = 18;
      this.dgvValues.Size = new System.Drawing.Size(594, 245);
      this.dgvValues.TabIndex = 0;
      // 
      // cntxMenu
      // 
      this.cntxMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.visibleColumnsToolStripMenuItem,
            this.visibleRowsToolStripMenuItem,
            this.relativeTimeToolStripMenuItem1});
      this.cntxMenu.Name = "cntxMenu";
      this.cntxMenu.Size = new System.Drawing.Size(169, 92);
      // 
      // visibleColumnsToolStripMenuItem
      // 
      this.visibleColumnsToolStripMenuItem.CheckOnClick = true;
      this.visibleColumnsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tUNameToolStripMenuItem,
            this.dDDVersionToolStripMenuItem,
            this.variableTitleToolStripMenuItem,
            this.physicalUintToolStripMenuItem});
      this.visibleColumnsToolStripMenuItem.Name = "visibleColumnsToolStripMenuItem";
      this.visibleColumnsToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
      this.visibleColumnsToolStripMenuItem.Text = "Visible columns";
      // 
      // tUNameToolStripMenuItem
      // 
      this.tUNameToolStripMenuItem.CheckOnClick = true;
      this.tUNameToolStripMenuItem.Name = "tUNameToolStripMenuItem";
      this.tUNameToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
      this.tUNameToolStripMenuItem.Text = "TU Name";
      this.tUNameToolStripMenuItem.CheckedChanged += new System.EventHandler(this.VisibleColMenuItem_CheckedChanged);
      // 
      // dDDVersionToolStripMenuItem
      // 
      this.dDDVersionToolStripMenuItem.CheckOnClick = true;
      this.dDDVersionToolStripMenuItem.Name = "dDDVersionToolStripMenuItem";
      this.dDDVersionToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
      this.dDDVersionToolStripMenuItem.Text = "DDD Version";
      this.dDDVersionToolStripMenuItem.CheckedChanged += new System.EventHandler(this.VisibleColMenuItem_CheckedChanged);
      // 
      // variableTitleToolStripMenuItem
      // 
      this.variableTitleToolStripMenuItem.Checked = true;
      this.variableTitleToolStripMenuItem.CheckOnClick = true;
      this.variableTitleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
      this.variableTitleToolStripMenuItem.Name = "variableTitleToolStripMenuItem";
      this.variableTitleToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
      this.variableTitleToolStripMenuItem.Text = "Variable title";
      this.variableTitleToolStripMenuItem.CheckedChanged += new System.EventHandler(this.VisibleColMenuItem_CheckedChanged);
      // 
      // physicalUintToolStripMenuItem
      // 
      this.physicalUintToolStripMenuItem.Checked = true;
      this.physicalUintToolStripMenuItem.CheckOnClick = true;
      this.physicalUintToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
      this.physicalUintToolStripMenuItem.Name = "physicalUintToolStripMenuItem";
      this.physicalUintToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
      this.physicalUintToolStripMenuItem.Text = "Physical uint";
      this.physicalUintToolStripMenuItem.CheckedChanged += new System.EventHandler(this.VisibleColMenuItem_CheckedChanged);
      // 
      // visibleRowsToolStripMenuItem
      // 
      this.visibleRowsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.headerToolStripMenuItem,
            this.relativeTimeToolStripMenuItem});
      this.visibleRowsToolStripMenuItem.Name = "visibleRowsToolStripMenuItem";
      this.visibleRowsToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
      this.visibleRowsToolStripMenuItem.Text = "Visible rows";
      // 
      // headerToolStripMenuItem
      // 
      this.headerToolStripMenuItem.CheckOnClick = true;
      this.headerToolStripMenuItem.Name = "headerToolStripMenuItem";
      this.headerToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
      this.headerToolStripMenuItem.Text = "Header";
      this.headerToolStripMenuItem.CheckedChanged += new System.EventHandler(this.VisibleRowMenuItem_Click);
      // 
      // relativeTimeToolStripMenuItem
      // 
      this.relativeTimeToolStripMenuItem.Checked = true;
      this.relativeTimeToolStripMenuItem.CheckOnClick = true;
      this.relativeTimeToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
      this.relativeTimeToolStripMenuItem.Name = "relativeTimeToolStripMenuItem";
      this.relativeTimeToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
      this.relativeTimeToolStripMenuItem.Text = "Relative time";
      this.relativeTimeToolStripMenuItem.CheckedChanged += new System.EventHandler(this.VisibleRowMenuItem_Click);
      // 
      // relativeTimeToolStripMenuItem1
      // 
      this.relativeTimeToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.millisecondsToolStripMenuItem,
            this.secondsToolStripMenuItem,
            this.minutesToolStripMenuItem,
            this.secondsMillisecondsToolStripMenuItem,
            this.minSecmsToolStripMenuItem});
      this.relativeTimeToolStripMenuItem1.Name = "relativeTimeToolStripMenuItem1";
      this.relativeTimeToolStripMenuItem1.Size = new System.Drawing.Size(168, 22);
      this.relativeTimeToolStripMenuItem1.Text = "Relative time unit";
      // 
      // millisecondsToolStripMenuItem
      // 
      this.millisecondsToolStripMenuItem.Name = "millisecondsToolStripMenuItem";
      this.millisecondsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
      this.millisecondsToolStripMenuItem.Text = "ms";
      this.millisecondsToolStripMenuItem.Click += new System.EventHandler(this.setRelTimeUnitToolStripMenuItem_Click);
      // 
      // secondsToolStripMenuItem
      // 
      this.secondsToolStripMenuItem.Name = "secondsToolStripMenuItem";
      this.secondsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
      this.secondsToolStripMenuItem.Text = "sec";
      this.secondsToolStripMenuItem.Click += new System.EventHandler(this.setRelTimeUnitToolStripMenuItem_Click);
      // 
      // minutesToolStripMenuItem
      // 
      this.minutesToolStripMenuItem.Name = "minutesToolStripMenuItem";
      this.minutesToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
      this.minutesToolStripMenuItem.Text = "min";
      this.minutesToolStripMenuItem.Click += new System.EventHandler(this.setRelTimeUnitToolStripMenuItem_Click);
      // 
      // secondsMillisecondsToolStripMenuItem
      // 
      this.secondsMillisecondsToolStripMenuItem.Name = "secondsMillisecondsToolStripMenuItem";
      this.secondsMillisecondsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
      this.secondsMillisecondsToolStripMenuItem.Text = "sec.ms";
      this.secondsMillisecondsToolStripMenuItem.Click += new System.EventHandler(this.setRelTimeUnitToolStripMenuItem_Click);
      // 
      // minSecmsToolStripMenuItem
      // 
      this.minSecmsToolStripMenuItem.Name = "minSecmsToolStripMenuItem";
      this.minSecmsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
      this.minSecmsToolStripMenuItem.Text = "min:sec.ms";
      this.minSecmsToolStripMenuItem.Click += new System.EventHandler(this.setRelTimeUnitToolStripMenuItem_Click);
      // 
      // HorizVariableGrid
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.dgvValues);
      this.Name = "HorizVariableGrid";
      this.Size = new System.Drawing.Size(594, 245);
      ((System.ComponentModel.ISupportInitialize)(this.dgvValues)).EndInit();
      this.cntxMenu.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.DataGridView dgvValues;
    private System.Windows.Forms.ContextMenuStrip cntxMenu;
    private System.Windows.Forms.ToolStripMenuItem visibleColumnsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem tUNameToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem dDDVersionToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem variableTitleToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem physicalUintToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem visibleRowsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem headerToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem relativeTimeToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem relativeTimeToolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem millisecondsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem secondsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem minutesToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem secondsMillisecondsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem minSecmsToolStripMenuItem;
  }
}
