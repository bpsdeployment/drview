using System;
using System.Collections.Generic;
using System.Text;

namespace DRViewComponents
{
  /// <summary>
  /// Class holds the configuration of diagnostic record views.
  /// Configuration is loaded from component configuration file at application startup.
  /// </summary>
  public class ViewConfiguration
  {
    public List<ViewAttribute> Attributes;

    /// <summary>
    /// Constructor
    /// </summary>
    public ViewConfiguration()
    {
      Attributes = new List<ViewAttribute>();
    }

    /// <summary>
    /// Class represents one attribute visualized in record views (StartTime, FaultCode...)
    /// </summary>
    public class ViewAttribute
    {
      //Name of the data field in selector dat set
      public String DataField;
      //Text of the attribute header
      public String HeaderText;

      /// <summary>
      /// Constructor
      /// </summary>
      public ViewAttribute(String DataField, String HeaderText)
      {
        this.DataField = DataField;
        this.HeaderText = HeaderText;
      }
    }
  }
}
