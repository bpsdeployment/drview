using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.IO;
using System.Data;
using DDDObjects;
using DRSelectorComponents;
using DSDiagnosticRecords;
using DRViewComponents.VarTreeNodes;

namespace DRViewComponents
{
  /// <summary>
  /// Component provides methods for exporting diagnostic records to file in various formats
  /// </summary>
  public partial class DRExport : Component
  {
    #region Public enumerations
    /// <summary>
    /// Columns that shall be written to CSV file
    /// </summary>
    [Flags]
    public enum RecordsCSVColumns
    {
      None =            0x00000000,
      StartTime =       0x00000001,
      EndTime =         0x00000002,
      LastModified =    0x00000004,
      Source =          0x00000008,
      ImportTime =      0x00000010,
      AcknowledgeTime = 0x00000020,
      VehicleNumber =   0x00000040,
      DeviceCode =      0x00000080,
      TUInstanceName =  0x00000100,
      TUTypeName =      0x00000200,
      DDDVersionName =  0x00000400,
      RecTitle =        0x00000800,
      FaultCode =       0x00001000,
      RecordName =      0x00002000,      
      TrainName =       0x00004000,
      VehicleName =     0x00008000,
      RecordType =      0x00010000,
      FaultSeverity =   0x00020000,
      FaultSubSeverity= 0x00040000,
      TrcSnpCode =      0x00080000,
      GPSLatitude =     0x00100000,
      GPSLongitude =    0x00200000,
      TypeCount =       0x00400000
    }
    #endregion //Public enumerations

    #region Members
    protected DDDHelper dddHelper = null;  //class providing DDD objects functionality
    protected DRSelectorBase selector = null; //component providing selected DR to display
    #endregion //Members

    #region Public properties
    /// <summary>
    /// DDD objects functionality
    /// </summary>
    public DDDHelper DDDHelper
    {
      set { dddHelper = value; }
      get { return dddHelper; }
    }

    /// <summary>
    /// Records selector object
    /// </summary>
    public DRSelectorBase Selector
    {
      set {selector = value;}
      get { return selector; }
    }
    #endregion //Public properties

    #region Public Events
    /// <summary>
    /// Event occurs when there is an information message to pass to component owner
    /// </summary>
    public event InfoMessageEventhandler InfoMessage;
    /// <summary>
    /// Event occurs when there is an error encountered during selection process
    /// </summary>
    public event ErrorMessageEventhandler ErrorMessage;
    /// <summary>
    /// Event occurs when component needs to report progress of running operation
    /// </summary>
    public event ReportProgressEventHandler ProgressReport;
    #endregion //Public Events

    #region Construction and initialization
    /// <summary>
    /// Default constructor
    /// </summary>
    public DRExport()
    {
      InitializeComponent();
    }

    public DRExport(IContainer container)
    {
      container.Add(this);

      InitializeComponent();
    }

    #endregion //Construction and initialization

    #region Event raisers
    /// <summary>
    /// Method raises InfoMessage event
    /// </summary>
    protected void OnInfoMessage(String message)
    {
      if (InfoMessage != null)
        InfoMessage(this, message);
    }

    /// <summary>
    /// Raises ErrorMessage event
    /// </summary>
    /// <param name="error">Exception describing the error</param>
    protected void OnErrorMessage(Exception error)
    {
      if (ErrorMessage != null)
        ErrorMessage(this, error);
    }

    /// <summary>
    /// Method raises InfoMessage event
    /// </summary>
    protected void OnInfoMessage(String message, params Object[] args)
    {
      if (InfoMessage != null)
        InfoMessage(this, String.Format(message, args));
    }

    /// <summary>
    /// Method raises Report progress event
    /// </summary>
    /// <param name="progress">reported progress</param>
    /// <param name="barVisible">visibility of progress bar</param>
    protected void OnReportProgress(int progress, bool barVisible)
    {
      if (ProgressReport != null)
        ProgressReport(this, progress, barVisible);
    }
    #endregion //Event raisers

    #region Export methods
    /// <summary>
    /// Saves records in the form of csv file.
    /// </summary>
    /// <param name="separator">Column separator</param>
    /// <param name="timeFormat">Format string for times</param>
    /// <param name="fileName">Name of target file</param>
    /// <param name="columns">List of columns to write</param>
    public void SaveRecordsToCSV(String separator, String timeFormat, String fileName, RecordsCSVColumns columns)
    {
      try
      {
        FileStream file;
        StreamWriter writer;
        String line;
        int totalRecs;
        int recIndex;
        //Generate info message
        OnInfoMessage("Exporting records to CSV file...");
        //Open fileStream and writer
        file = new FileStream(fileName, FileMode.Create, FileAccess.Write);
        writer = new StreamWriter(file);
        //Create header line
        line = "";
        if ((columns & RecordsCSVColumns.TrainName) > 0)
          line += ResDRExport.strTrain + separator;
        if ((columns & RecordsCSVColumns.VehicleName) > 0)
          line += ResDRExport.strVehicle + separator;
        if ((columns & RecordsCSVColumns.TUInstanceName) > 0)
          line += ResDRExport.strTUName + separator;
        if ((columns & RecordsCSVColumns.TUTypeName) > 0)
          line += ResDRExport.strTUType + separator;
        if ((columns & RecordsCSVColumns.VehicleNumber) > 0)
          line += ResDRExport.strVehicleNumber + separator;
        if ((columns & RecordsCSVColumns.Source) > 0)
          line += ResDRExport.strSource + separator;
        if ((columns & RecordsCSVColumns.DeviceCode) > 0)
          line += ResDRExport.strDeviceCode + separator;
        if ((columns & RecordsCSVColumns.StartTime) > 0)
          line += ResDRExport.strStartTime + separator;
        if ((columns & RecordsCSVColumns.EndTime) > 0)
          line += ResDRExport.strEndTime + separator;
        if ((columns & RecordsCSVColumns.LastModified) > 0)
          line += ResDRExport.strLastModified + separator;
        if ((columns & RecordsCSVColumns.AcknowledgeTime) > 0)
          line += ResDRExport.strAcknowledgeTime + separator;
        if ((columns & RecordsCSVColumns.ImportTime) > 0)
          line += ResDRExport.strImportTime + separator;
        if ((columns & RecordsCSVColumns.DDDVersionName) > 0)
          line += ResDRExport.strDDDVersion + separator;
        if ((columns & RecordsCSVColumns.RecTitle) > 0)
          line += ResDRExport.strRecTitle + separator;
        if ((columns & RecordsCSVColumns.RecordName) > 0)
          line += ResDRExport.strRecordName + separator;
        if ((columns & RecordsCSVColumns.FaultCode) > 0)
          line += ResDRExport.strFaultCode + separator;
        if ((columns & RecordsCSVColumns.TrcSnpCode) > 0)
          line += ResDRExport.strTrcSnpCode + separator;
        if ((columns & RecordsCSVColumns.RecordType) > 0)
          line += ResDRExport.strRecordType + separator;
        if ((columns & RecordsCSVColumns.FaultSeverity) > 0)
          line += ResDRExport.strFaultSeverity + separator;
        if ((columns & RecordsCSVColumns.FaultSubSeverity) > 0)
          line += ResDRExport.strFaultSubSeverity + separator;
        if ((columns & RecordsCSVColumns.GPSLatitude) > 0)
          line += ResDRExport.strGPSLat + separator;
        if ((columns & RecordsCSVColumns.GPSLongitude) > 0)
          line += ResDRExport.strGPSLong + separator;
        if ((columns & RecordsCSVColumns.TypeCount) > 0)
          line += ResDRExport.strTypeCount + separator;
        //Write header line
        writer.WriteLine(line);
        //Store record count for progress report
        totalRecs = selector.RecordsView.Count;
        recIndex = 0;
        //Create view over records from selector sorted by start time
        DataView sortView = new DataView(selector.RecordsView.Table, selector.RecordsView.RowFilter, "StartTime ASC", selector.RecordsView.RowStateFilter);
        //Iterate over all records in view
        foreach (DataRowView rowView in sortView)
        {
          DiagnosticRecords.RecordsRow recRow = (DiagnosticRecords.RecordsRow)rowView.Row;
          try
          {
            //Create string with line
            line = "";
            if ((columns & RecordsCSVColumns.TrainName) > 0)
            {
              if (!recRow.IsTrainNameNull())
                line += recRow.TrainName;
              line += separator;
            }
            if ((columns & RecordsCSVColumns.VehicleName) > 0)
            {
              if (!recRow.IsVehicleNameNull())
                line += recRow.VehicleName;
              line += separator;
            }
            if ((columns & RecordsCSVColumns.TUInstanceName) > 0)
            {
              if (!recRow.IsTUInstanceNameNull())
                line += recRow.TUInstanceName;
              line += separator;
            }
            if ((columns & RecordsCSVColumns.TUTypeName) > 0)
            {
              if (!recRow.IsTUTypeNameNull())
                line += recRow.TUTypeName;
              line += separator;
            }
            if ((columns & RecordsCSVColumns.VehicleNumber) > 0)
            {
              if (!recRow.IsVehicleNumberNull())
                line += recRow.VehicleNumber;
              line += separator;
            }
            if ((columns & RecordsCSVColumns.Source) > 0)
            {
              if (!recRow.IsSourceNull())
                line += recRow.Source;
              line += separator;
            }
            if ((columns & RecordsCSVColumns.DeviceCode) > 0)
            {
              if (!recRow.IsDeviceCodeNull())
                line += recRow.DeviceCode;
              line += separator;
            }
            if ((columns & RecordsCSVColumns.StartTime) > 0)
            {
              line += recRow.StartTime.ToString(timeFormat);
              line += separator;
            }
            if ((columns & RecordsCSVColumns.EndTime) > 0)
            {
              if (!recRow.IsEndTimeNull() && recRow.EndTime > DateTime.MinValue)
                line += recRow.EndTime.ToString(timeFormat);
              line += separator;
            }
            if ((columns & RecordsCSVColumns.LastModified) > 0)
            {
              line += recRow.LastModified.ToString(timeFormat);
              line += separator;
            }
            if ((columns & RecordsCSVColumns.AcknowledgeTime) > 0)
            {
              if (!recRow.IsAcknowledgeTimeNull() && recRow.AcknowledgeTime > DateTime.MinValue)
                line += recRow.AcknowledgeTime.ToString(timeFormat);
              line += separator;
            }
            if ((columns & RecordsCSVColumns.ImportTime) > 0)
            {
              if (!recRow.IsImportTimeNull() && recRow.ImportTime > DateTime.MinValue)
                line += recRow.ImportTime.ToString(timeFormat);
              line += separator;
            }
            if ((columns & RecordsCSVColumns.DDDVersionName) > 0)
            {
              if (!recRow.IsDDDVersionNameNull())
                line += recRow.DDDVersionName;
              line += separator;
            }
            if ((columns & RecordsCSVColumns.RecTitle) > 0)
            {
              if (!recRow.IsRecTitleNull())
                line += recRow.RecTitle;
              line += separator;
            }
            if ((columns & RecordsCSVColumns.RecordName) > 0)
            {
              if (!recRow.IsRecordNameNull())
                line += recRow.RecordName;
              line += separator;
            }
            if ((columns & RecordsCSVColumns.FaultCode) > 0)
            {
              if (!recRow.IsFaultCodeNull())
                line += recRow.FaultCode.ToString();
              line += separator;
            }
            if ((columns & RecordsCSVColumns.TrcSnpCode) > 0)
            {
              if (!recRow.IsTrcSnpCodeNull())
                line += recRow.TrcSnpCode.ToString();
              line += separator;
            }
            if ((columns & RecordsCSVColumns.RecordType) > 0)
            {
              if (!recRow.IsRecordTypeNull())
                line += recRow.RecordType;
              line += separator;
            }
            if ((columns & RecordsCSVColumns.FaultSeverity) > 0)
            {
              if (!recRow.IsFaultSeverityNull())
                line += recRow.FaultSeverity.ToString();
              line += separator;
            }
            if ((columns & RecordsCSVColumns.FaultSubSeverity) > 0)
            {
              if (!recRow.IsFaultSubSeverityNull())
                line += recRow.FaultSubSeverity.ToString();
              line += separator;
            }
            if ((columns & RecordsCSVColumns.GPSLatitude) > 0)
            {
              if (!recRow.IsGPSLatitudeNull())
                line += recRow.GPSLatitude.ToString();
              line += separator;
            }
            if ((columns & RecordsCSVColumns.GPSLongitude) > 0)
            {
              if (!recRow.IsGPSLongitudeNull())
                line += recRow.GPSLongitude.ToString();
              line += separator;
            }
            if ((columns & RecordsCSVColumns.TypeCount) > 0)
            {
              line +=
                Selector.GetRecTypeCount(recRow.TUInstanceID, recRow.DDDVersionID, recRow.RecordDefID).ToString();
              line += separator;
            }
            //Write line to file
            writer.WriteLine(line);
            //Generate progress event
            if ((recIndex % 50) == 0)
              OnReportProgress((int)(((float)recIndex / (float)totalRecs) * 100), true);
            recIndex++;
          }
          catch (Exception ex)
          { //Error writing record to file
            OnErrorMessage(new DRViewException(ex, "Failed to write record instance {0} from TU {1} to file", recRow.RecordInstID, recRow.TUInstanceID));
          }
        }
        //Close writer and file
        writer.Close();
        file.Dispose();
        //Generate info message
        OnInfoMessage("Records exported");
        //Hide progress bar
        OnReportProgress(100, false);
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRViewException(ex, "Failed to save records to csv file {0}", fileName));
      }
    }

    /// <summary>
    /// Saves variable values in the form of CSV file
    /// </summary>
    /// <param name="varIDs">Dictionary with IDs of selected variables</param>
    /// <param name="separator">Column separator</param>
    /// <param name="timeFormat">Format string for times</param>
    /// <param name="fileName">Name of target file</param>
    /// <param name="completeTitle">Use complete title for variables</param>
    /// <param name="includeVerInfo">Include version info in var title</param>
    public void SaveVarsToCSV(VarIDDictionary varIDs, String separator, String timeFormat, String fileName, bool completeTitle, bool includeVerInfo)
    {
      String[] line;
      SortedList<DateTime, String[]> lines;
      FileStream file;
      StreamWriter writer;
      List<VarValue> varValues;
      int valCount = 0;
      try
      {
        OnInfoMessage("Exporting variables to CSV file...");
        //Create lines of the file from selector variables table
        lines = new SortedList<DateTime, string[]>();
        //Obtain values for all selected variables
        varValues = selector.GetValuesOfVariables(varIDs);
        //Walk through all selected values and create sorted list of lines
        foreach (VarValue val in varValues)
        {
          try
          {
            //Obtain line for timestamp
            if (!lines.TryGetValue(val.TimeStamp, out line))
            {//line does not exist - create new one
              line = new String[varIDs.Variables.Count + 1];
              line[0] = val.TimeStamp.ToString(timeFormat);
              lines.Add(val.TimeStamp, line);
            }
            //Obtain formatted value and store in line
            VarDefIDValue varDef = varIDs.GetVariable(val.TUInstanceID, val.DDDVersionID, val.VariableID);
            if (varDef.GetType() == typeof(BitsetDefIDValue))
            { //Variable is bitset, format each used bit separatly
              BitsetDefIDValue bitset = varDef as BitsetDefIDValue;
              DDDBitsetRepres bitsetRepres = ((DDDElemVar)bitset.DDDVariable).Representation as DDDBitsetRepres;
              foreach (BitDefIDValue bit in bitset.GetUsedBits())
                line[bit.Index + 1] = bitsetRepres.FormatBitValue(val.Value, (ulong)bit.Mask);
            }
            else
            { //Elementary variable
              line[varDef.Index + 1] = ((DDDElemVar)varDef.DDDVariable).FormatValue(val.Value);
            }
          }
          catch (Exception ex)
          {
            OnErrorMessage(new DRViewException(ex, "Failed to format value of variable {0} from DDD version {1}, TU {2}",
                                               val.VariableID, val.DDDVersionID, val.TUInstanceID));
          }
          valCount++;
          //Report progress
          if ((valCount % 500) == 0)
            OnReportProgress((int)(((float)valCount / (float)varValues.Count) * (float)50), true);
        }

        //Save created lines to file
        //Open fileStream and writer
        file = new FileStream(fileName, FileMode.Create, FileAccess.Write);
        writer = new StreamWriter(file);
        //Create and save header
        writer.Write(ResDRExport.strTimeStamp + separator);
        foreach (VarDefIDValue variable in varIDs.Variables)
        {
          Int64 mask = 0;
          if (variable.GetType() == typeof(BitDefIDValue))
            mask = ((BitDefIDValue)variable).Mask;
          if (includeVerInfo)
            writer.Write(dddHelper.FormatSourceName(variable.TUInstance, variable.DDDVersion) + DDDHelper.TitleSeparator);
          writer.Write(variable.DDDVariable.FormatVarTitle(completeTitle, (ulong)mask, true));
          writer.Write(separator);
        }
        writer.WriteLine();
        //Save prepared lines
        int lineIndex = 0;
        foreach (KeyValuePair<DateTime, String[]> linePair in lines)
        { //Iterate over all lines
          line = linePair.Value;
          foreach (String item in line)
            writer.Write(item + separator); //Iterate over all items in line and write to file
          //New line
          writer.WriteLine();
          //Report progress
          if ((lineIndex % 100) == 0)
            OnReportProgress((int)(((float)lineIndex / (float)lines.Count) * (float)50) + 50, true);
          lineIndex++;
        }
        //Close writer and file
        writer.Close();
        file.Dispose();
        //Generate info message
        OnInfoMessage("Variables exported");
        OnReportProgress(0, false);
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRViewException(ex, "Failed to export variables to file {0}", fileName));
      }
    }
    #endregion //Export methods

    #region Helper methods
    /// <summary>
    /// Saves values of all elementary variables from simple inputs of given record instance to CSV file
    /// </summary>
    /// <param name="fileWriter">Writer to target file</param>
    /// <param name="recInstance">Specifies record instance</param>
    /// <param name="separator">Column separator</param>
    private void SaveElementaryVarsToCSV(StreamWriter fileWriter, RecInstInfo recInstance, String separator)
    {
      DDDInput dddInput;
      Dictionary<int, VarValue> varValues;
      VarValue value;
      String line;

      //Write header line for enviromental data
      line = ResDRExport.strEnvData + separator;
      fileWriter.WriteLine(line);

      //Create dictionary for var values
      varValues = CreateValDictionary(recInstance);
      //Write line for each elementary variable of each simple input
      foreach (KeyValuePair<int, DDDInput> inpPair in recInstance.dddRecord.Inputs)
      { //iterate over all inputs of record
        dddInput = inpPair.Value;
        //Determine input type, work only with simple inputs
        if (dddInput.GetType() != typeof(DDDSimpleInput))
          continue; //Not simple input - skip
        else
        { //Simple input
          foreach (DDDElemVar elemVar in dddInput.Variable.GetElementaryVars())
          { //Iterate over all elementary variables
            //Obtain value for variable
            if (!varValues.TryGetValue(elemVar.VariableID, out value))
              continue; //No value for variable found
            //Create line with var value
            line = elemVar.Title + separator;
            if (!elemVar.IsBitSet())
              line += elemVar.FormatValue(value.Value) + separator;
            else
              line += ((DDDBitsetRepres)(elemVar.Representation)).FormatSIBASString(value.Value) + separator;
            String unit = "";
            if (elemVar.Representation.GetType() == typeof(DDDSimpleRepres))
              unit = ((DDDSimpleRepres)(elemVar.Representation)).UnitSymbol;
            if (unit != "")
              unit = "[" + unit + "]";
            line += unit + separator;
            //Save line to file
            fileWriter.WriteLine(line);
          }
        }
      }
    }

    /// <summary>
    /// Creates dictionary of var values for simple inputs of specified record instance
    /// </summary>
    /// <param name="recInfo">Specification of record instance</param>
    /// <returns>Dictionary of var values, keyed by VariableID</returns>
    private Dictionary<int, VarValue> CreateValDictionary(RecInstInfo recInfo)
    {
      //First obtain list of values for specified record instance
      List<VarValue> values = selector.GetVarValuesForRecord(recInfo.TUInstanceID, recInfo.RecordInstID, recInfo.dddRecord.RecordDefID);
      //Create output dictionary
      Dictionary<int, VarValue> dictionary = new Dictionary<int, VarValue>();
      //Iterate over all obtained values
      foreach (VarValue val in values)
        if (val.ValueIndex == 0 && !dictionary.ContainsKey(val.VariableID))
          dictionary.Add(val.VariableID, val); //Add to dictionary only values of simple inputs, that are not already there
      //Return created dictionary
      return dictionary;
    }
    #endregion //Helper methods
  }
}
