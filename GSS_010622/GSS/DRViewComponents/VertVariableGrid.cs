using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DDDObjects;
using DSDiagnosticRecords;
using DRSelectorComponents;

namespace DRViewComponents
{
  /// <summary>
  /// Displays values of selected variables in vertical grid.
  /// Obne fixed column displays timestamp, other columns display each one elementary variable.
  /// Only last portion of variable name is displayd in header grid.
  /// </summary>
  public partial class VertVariableGrid : UserControl
  {
    #region Private members
    protected DataTable valsTable; //Contains values of selected variables
    protected Dictionary<String, DataColumn> varMap; //Maps variables to columns in data table
    private Dictionary<String, sGridColAttributes> colAttributesMap; //Map of data table column names to attributes for grid view columns
    protected DDDHelper dddHelper; //Helper object
    protected DataColumn timeCol; //column holding timestamp in internal data table
    private sGridColAttributes timeColAttributes; //Attributes for data time column
    #endregion //Private members

    #region Public properties
    /// <summary>
    /// DDD helper object
    /// </summary>
    public DDDHelper DDDHelper
    {
      get { return dddHelper; }
      set { dddHelper = value; }
    }

    /// <summary>
    /// Format string used to format timestamp
    /// </summary>
    public String TimeFormat
    {
      set { 
            if (gridView.Columns.Count > 0) 
              gridView.Columns[0].DefaultCellStyle.Format = value;
            timeColAttributes.FormatString = value;
          }
    }
    #endregion //Public properties

    #region Construction initialization
    public VertVariableGrid()
    {
      InitializeComponent();
      //Create map and data table
      varMap = new Dictionary<string, DataColumn>();
      colAttributesMap = new Dictionary<string, sGridColAttributes>();
      valsTable = new DataTable();
      //Add time column to data table
      timeCol = new DataColumn("colTimestamp", typeof(DateTime));
      //Create attributes struct for time col
      timeColAttributes = new sGridColAttributes();
      timeColAttributes.HeaderText = "Timestamp";
      timeColAttributes.TooltipText = "Value timestamp";
      timeColAttributes.SortMode = DataGridViewColumnSortMode.Automatic;
      colAttributesMap.Add(timeCol.ColumnName, timeColAttributes);
      valsTable.Columns.Add(timeCol);
      //Set column properties
      timeCol.DataType = typeof(DateTime);
      //Set data table as data source for grid
      gridView.DataSource = valsTable;
      //Set time column as PK
      valsTable.PrimaryKey = new DataColumn[] {timeCol};
    }
    #endregion //Construction initialization

    #region Public methods
    /// <summary>
    /// Adds columns to display to grid for specified variable.
    /// For bitset adds either only column fo specific bit or columns for all bits (mask == 0).
    /// Does not update the grid, call UpdateGrid afterwards.
    /// </summary>
    /// <param name="TUInstance"></param>
    /// <param name="DDDVersion"></param>
    /// <param name="variable"></param>
    public void AddVariableToGrid(TUInstance TUInstance, DDDVersion DDDVersion, DDDElemVar variable, Int64 mask, DRSelectorBase selector)
    {
      //Check if variable is bitset
      if ((!variable.IsBitSet()) || (mask != 0))
      { //simple var or one bit
        AddTableColumn(TUInstance.TUInstanceID, DDDVersion.VersionID, variable.VariableID, mask);
      }
      else
      { //bitset, add all bits
        DDDBitsetRepres bitsetRep = variable.Representation as DDDBitsetRepres;
        ulong[] masks = bitsetRep.GetUsedBitMasks();
        foreach(Int64 bitMask in masks)
          AddTableColumn(TUInstance.TUInstanceID, DDDVersion.VersionID, variable.VariableID, bitMask);
      }
      //obtain values for variable from selector
      List<VarValue> varValues = selector.GetValuesOfVariable(TUInstance, DDDVersion, variable);
      //Fill internal data table with values
      valsTable.BeginLoadData();
      AddRows(varValues);
      valsTable.EndLoadData();
    }

    /// <summary>
    /// Displays values of specified variables in grid
    /// </summary>
    /// <param name="variables"></param>
    /// <param name="selector"></param>
    public void AddVariablesToGrid(VarIDDictionary variables, DRSelectorBase selector)
    {
      foreach (VarDefIDValue varDef in variables.Variables)
      {
        if (varDef.GetType() == typeof(BitDefIDValue))
          AddTableColumn(varDef.TUInstance.TUInstanceID, varDef.DDDVersion.VersionID, varDef.DDDVariable.VariableID, 
                         ((BitDefIDValue)varDef).Mask);
        else
          AddTableColumn(varDef.TUInstance.TUInstanceID, varDef.DDDVersion.VersionID, varDef.DDDVariable.VariableID, 0);
      }
      //obtain values for variables from selector
      List<VarValue> varValues = selector.GetValuesOfVariables(variables);
      //Fill internal data table with values
      valsTable.BeginLoadData();
      AddRows(varValues);
      valsTable.EndLoadData();
    }

    /// <summary>
    /// Removes column(s) displayin specified variable from grid.
    /// For bitset removes either only column for specific bit or columns for all bits (mask == 0).
    /// Does not update the grid, call UpdateGrid afterwards.
    /// </summary>
    public void RemoveVariableFromGrid(TUInstance TUInstance, DDDVersion DDDVersion, DDDElemVar variable, Int64 mask)
    {
      String strVarID;
      DataColumn col;
      //Check if variable is bitset
      if ((!variable.IsBitSet()) || (mask != 0))
      { //simple var or one bit
        strVarID = CreateVarID(TUInstance.TUInstanceID, DDDVersion.VersionID, variable.VariableID, mask);
        if (varMap.ContainsKey(strVarID))
        {
          //Remove column from table
          col = GetTableColumn(TUInstance.TUInstanceID, DDDVersion.VersionID, variable.VariableID, mask);
          if (col != null)
            valsTable.Columns.Remove(col);
          //remove column from map
          varMap.Remove(strVarID);
          colAttributesMap.Remove(strVarID);
        }
      }
      else
      { //bitset, remove all bits
        DDDBitsetRepres bitsetRep = variable.Representation as DDDBitsetRepres;
        ulong[] masks = bitsetRep.GetUsedBitMasks();
        foreach (Int64 bitMask in masks)
        {
          strVarID = CreateVarID(TUInstance.TUInstanceID, DDDVersion.VersionID, variable.VariableID, bitMask);
          if (varMap.ContainsKey(strVarID))
          {
            //Remove column from table
            col = GetTableColumn(TUInstance.TUInstanceID, DDDVersion.VersionID, variable.VariableID, bitMask);
            if (col != null)
              valsTable.Columns.Remove(col);
            //remove column from map
            varMap.Remove(strVarID);
            colAttributesMap.Remove(strVarID);
          }
        }
      }
      //Find all empty rows
      List<DataRow> emptyRows = new List<DataRow>();
      bool rowEmpty;
      foreach (DataRow row in valsTable.Rows)
      { //Iterate over all rows
        rowEmpty = true; //Assume row is empty
        foreach(DataColumn column in valsTable.Columns)
          if ((column != timeCol) && (row[column] != DBNull.Value))
          { //cell is not empty
            rowEmpty = false;
            break;
          }
        if (rowEmpty)
          emptyRows.Add(row);
      }
      //Remove all empty rows
      foreach (DataRow row in emptyRows)
        valsTable.Rows.Remove(row);
    }

    /// <summary>
    /// Displays all visual changes from last BeginUpdate call
    /// </summary>
    public void EndUpdate()
    {
      gridView.DataSource = valsTable;
      gridView.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
      //gridView.Sort(gridView.Columns[0], ListSortDirection.Ascending);
    }

    /// <summary>
    /// Turns off all visual changes before data changes
    /// </summary>
    public void BeginUpdate()
    {
      gridView.DataSource = null;
    }

    /// <summary>
    /// Clears all values from grid
    /// </summary>
    public void Clear()
    {
      //Clear internal table
      valsTable.PrimaryKey = null;
      valsTable.Rows.Clear();
      valsTable.Columns.Clear();
      //Clear map of column attributes
      colAttributesMap.Clear();
      colAttributesMap.Add(timeCol.ColumnName, timeColAttributes);
      //Insert back time column and set as PK
      valsTable.Columns.Add(timeCol);
      valsTable.PrimaryKey = new DataColumn[] { timeCol };
      //Clear internal variable map
      varMap.Clear();
    }
    #endregion //Public methods

    #region Helper methods
    /// <summary>
    /// Adds values of variables to internal data table.
    /// Creates new columns for new variables and new rows for timestamps as necessary.
    /// </summary>
    /// <param name="varValues">List of variables to display</param>
    protected void AddRows(List<VarValue> varValues)
    {
      DataRow valRow;
      DataColumn valCol;
      DDDElemVar varObject;
      String value;
      foreach (VarValue varValue in varValues)
      { //Iterate over all values in list
        valRow = GetTableRow(varValue.TimeStamp);
        //Obtain variable object
        varObject = dddHelper.GetVariable(varValue.DDDVersionID, varValue.VariableID) as DDDElemVar;
        //Check if variable is bitstring
        if (!varObject.IsBitSet())
        { //Not bitset
          valCol = GetTableColumn(varValue.TUInstanceID, varValue.DDDVersionID, varValue.VariableID, 0);
          if (valCol == null)
            continue;
          //Format value
          value = varObject.FormatValue(varValue.Value);
          //Set value to row
          valRow[valCol] = value;
        }
        else
        { //Variable is bitset, process each bit separately
          KeyValuePair<ulong, String>[] bitVals;
          //Obtain array of bit values
          bitVals = varObject.Representation.GetMasksAndValues(varValue.Value);
          //Process bits
          foreach (KeyValuePair<ulong, String> bitVal in bitVals)
          { 
            //Obtain column
            valCol = GetTableColumn(varValue.TUInstanceID, varValue.DDDVersionID, varValue.VariableID, (long)bitVal.Key);
            if (valCol == null)
              continue;
            //Set value to row
            valRow[valCol] = bitVal.Value;
          }
        }
      }
    }

    /// <summary>
    /// Returns row from data table representing specified timestamp.
    /// Creates new row if necessary.
    /// </summary>
    /// <param name="timeStamp">Timestamp</param>
    /// <returns>Row from internal data table</returns>
    protected DataRow GetTableRow(DateTime timeStamp)
    {
      DataRow row;
      //Try to find existing row
      row = valsTable.Rows.Find(timeStamp);
      if (row != null)
        return row; // Row already exists
      //Create new row
      row = valsTable.NewRow();
      //Set value for TimeStamp column
      row[0] = timeStamp;
      //Add row to table
      valsTable.Rows.Add(row);
      //return the row
      return row;
    }

    /// <summary>
    /// Returns column from data table representing specified variable.
    /// New column is created if necessary.
    /// </summary>
    /// <returns>Column from internal data table</returns>
    protected DataColumn AddTableColumn(Guid TUInstanceID, Guid DDDVersionID, int VariableID, Int64 Mask)
    {
      String strVarID = CreateVarID(TUInstanceID, DDDVersionID, VariableID, Mask);
      DataColumn col;
      //Check if column already exists
      if (varMap.TryGetValue(strVarID, out col))
        return col; //column already exists
      //Create new column
      col = new DataColumn();
      col.AllowDBNull = true;
      col.DataType = typeof(String);
      //Get helper objects
      DDDVersion dddVersion = dddHelper.GetVersion(DDDVersionID);
      DDDVariable var = dddVersion.GetVariable(VariableID);
      TUInstance tuInstance = dddHelper.GetTUInstance(TUInstanceID);
      //Get variable name and tooltip for column
      String varTitle;
      String completeName;
      if (var.GetType() != typeof(DDDElemVar))
        throw new Exception("Cannot display structured variables in one column");
      DDDElemVar elemVar = var as DDDElemVar;
      completeName = dddHelper.FormatSourceName(tuInstance, dddVersion) + DDDHelper.TitleSeparator + var.FormatVarTitle(true, (ulong)Mask, true);
      varTitle = var.FormatVarTitle(false, (ulong)Mask, false);
      //column name
      col.ColumnName = strVarID;
      //create column attributes
      sGridColAttributes colAttributes = new sGridColAttributes();
      colAttributes.HeaderText = varTitle;
      colAttributes.TooltipText = completeName;
      colAttributesMap.Add(col.ColumnName, colAttributes);
      //Add column to data table and map
      valsTable.Columns.Add(col);
      varMap.Add(strVarID, col);
      //Return column
      return col;
    }

    /// <summary>
    /// Returns column from data table representing specified variable.
    /// Does not create new columns.
    /// </summary>
    /// <returns>Column from internal data table or null</returns>
    protected DataColumn GetTableColumn(Guid TUInstanceID, Guid DDDVersionID, int VariableID, Int64 Mask)
    {
      String strVarID = CreateVarID(TUInstanceID, DDDVersionID, VariableID, Mask);
      DataColumn col;
      if (varMap.TryGetValue(strVarID, out col))
        return col; //column exists
      else
        return null; //column does not exist
    }

    /// <summary>
    /// Craetes string uniquely representing variable
    /// </summary>
    /// <param name="TUInstanceID"></param>
    /// <param name="DDDVersionID"></param>
    /// <param name="VariableID"></param>
    /// <param name="Mask"></param>
    /// <returns>Var representation</returns>
    private String CreateVarID(Guid TUInstanceID, Guid DDDVersionID, int VariableID, Int64 Mask)
    {
      return TUInstanceID.ToString() + "_" + DDDVersionID.ToString() + "_" + VariableID.ToString() + "_" + Mask.ToString();
    }
    #endregion //Helper methods

    #region Event handlers
    private void gridView_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
    {
      //Try to find column attributes
      sGridColAttributes colAttributes;
      if (colAttributesMap.TryGetValue(e.Column.DataPropertyName, out colAttributes))
      { //set attributes for column
        e.Column.HeaderText = colAttributes.HeaderText;
        e.Column.ToolTipText = colAttributes.TooltipText;
        if (colAttributes.FormatString != "")
          e.Column.DefaultCellStyle.Format = colAttributes.FormatString;
        e.Column.SortMode = colAttributes.SortMode;
      }
    }
    #endregion //Event handlers
  }


  #region Helper classes
  internal class sGridColAttributes
  {
    public String HeaderText = "";
    public String TooltipText = "";
    public String FormatString = "";
    public DataGridViewColumnSortMode SortMode = DataGridViewColumnSortMode.NotSortable;
  }
  #endregion //Helper classes
}
