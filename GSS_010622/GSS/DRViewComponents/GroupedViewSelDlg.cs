using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DDDObjects;
using DRSelectorComponents;

namespace DRViewComponents
{
  /// <summary>
  /// Dialog provides the user the possibility to select attributes and
  /// variables which will be displayed in grouped records and variables view
  /// </summary>
  public partial class GroupedViewSelDlg : Form
  {
    #region Public members
    #endregion

    #region Private members
    #endregion

    #region Construction and initialization
    /// <summary>
    /// Dialog constructor
    /// </summary>
    /// <param name="dddHelper">DDD Helper object</param>
    /// <param name="selector">Selector component</param>
    /// <param name="viewConfig">View attributes configuration</param>
    public GroupedViewSelDlg(DDDHelper dddHelper, DRSelectorBase selector, ViewConfiguration viewConfig)
    {
      InitializeComponent();
      //Initiate components
      varTreeView.DDDHelper = dddHelper;

    }
    #endregion

    #region Public methods
    #endregion


  }
}