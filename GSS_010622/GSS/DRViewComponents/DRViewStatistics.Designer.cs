namespace DRViewComponents
{
  partial class DRViewStatistics
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.dgvStatistics = new System.Windows.Forms.DataGridView();
      this.TrainName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.VehicleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.VehicleNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Source = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.FaultCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.RecTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.RecCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.FirstTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.LastTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.FaultSeverity = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.FaultSubSeverity = new System.Windows.Forms.DataGridViewTextBoxColumn();
      ((System.ComponentModel.ISupportInitialize)(this.dgvStatistics)).BeginInit();
      this.SuspendLayout();
      // 
      // dgvStatistics
      // 
      this.dgvStatistics.AllowUserToAddRows = false;
      this.dgvStatistics.AllowUserToDeleteRows = false;
      this.dgvStatistics.AllowUserToOrderColumns = true;
      this.dgvStatistics.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvStatistics.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TrainName,
            this.VehicleName,
            this.VehicleNumber,
            this.Source,
            this.FaultCode,
            this.RecTitle,
            this.RecCount,
            this.FirstTime,
            this.LastTime,
            this.FaultSeverity,
            this.FaultSubSeverity});
      this.dgvStatistics.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgvStatistics.Location = new System.Drawing.Point(0, 0);
      this.dgvStatistics.Name = "dgvStatistics";
      this.dgvStatistics.ReadOnly = true;
      this.dgvStatistics.RowHeadersVisible = false;
      this.dgvStatistics.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
      this.dgvStatistics.Size = new System.Drawing.Size(805, 262);
      this.dgvStatistics.TabIndex = 0;
      this.dgvStatistics.VisibleChanged += new System.EventHandler(this.dgvStatistics_VisibleChanged);
      // 
      // TrainName
      // 
      this.TrainName.HeaderText = "Train Name";
      this.TrainName.Name = "TrainName";
      this.TrainName.ReadOnly = true;
      // 
      // VehicleName
      // 
      this.VehicleName.HeaderText = "Vehicle Name";
      this.VehicleName.Name = "VehicleName";
      this.VehicleName.ReadOnly = true;
      // 
      // VehicleNumber
      // 
      this.VehicleNumber.HeaderText = "Vehicle Number";
      this.VehicleNumber.Name = "VehicleNumber";
      this.VehicleNumber.ReadOnly = true;
      // 
      // Source
      // 
      this.Source.HeaderText = "Source";
      this.Source.Name = "Source";
      this.Source.ReadOnly = true;
      // 
      // FaultCode
      // 
      this.FaultCode.HeaderText = "Fault Code";
      this.FaultCode.Name = "FaultCode";
      this.FaultCode.ReadOnly = true;
      // 
      // RecTitle
      // 
      this.RecTitle.HeaderText = "Record Title";
      this.RecTitle.MinimumWidth = 100;
      this.RecTitle.Name = "RecTitle";
      this.RecTitle.ReadOnly = true;
      // 
      // RecCount
      // 
      this.RecCount.HeaderText = "Count";
      this.RecCount.Name = "RecCount";
      this.RecCount.ReadOnly = true;
      // 
      // FirstTime
      // 
      this.FirstTime.HeaderText = "Time of first";
      this.FirstTime.Name = "FirstTime";
      this.FirstTime.ReadOnly = true;
      // 
      // LastTime
      // 
      this.LastTime.HeaderText = "Time of last";
      this.LastTime.Name = "LastTime";
      this.LastTime.ReadOnly = true;
      // 
      // FaultSeverity
      // 
      this.FaultSeverity.HeaderText = "Severity";
      this.FaultSeverity.Name = "FaultSeverity";
      this.FaultSeverity.ReadOnly = true;
      // 
      // FaultSubSeverity
      // 
      this.FaultSubSeverity.HeaderText = "Sub Severity";
      this.FaultSubSeverity.Name = "FaultSubSeverity";
      this.FaultSubSeverity.ReadOnly = true;
      // 
      // DRViewStatistics
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.Controls.Add(this.dgvStatistics);
      this.Name = "DRViewStatistics";
      this.Size = new System.Drawing.Size(805, 262);
      ((System.ComponentModel.ISupportInitialize)(this.dgvStatistics)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.DataGridView dgvStatistics;
    private System.Windows.Forms.DataGridViewTextBoxColumn TrainName;
    private System.Windows.Forms.DataGridViewTextBoxColumn VehicleName;
    private System.Windows.Forms.DataGridViewTextBoxColumn VehicleNumber;
    private System.Windows.Forms.DataGridViewTextBoxColumn Source;
    private System.Windows.Forms.DataGridViewTextBoxColumn FaultCode;
    private System.Windows.Forms.DataGridViewTextBoxColumn RecTitle;
    private System.Windows.Forms.DataGridViewTextBoxColumn RecCount;
    private System.Windows.Forms.DataGridViewTextBoxColumn FirstTime;
    private System.Windows.Forms.DataGridViewTextBoxColumn LastTime;
    private System.Windows.Forms.DataGridViewTextBoxColumn FaultSeverity;
    private System.Windows.Forms.DataGridViewTextBoxColumn FaultSubSeverity;
  }
}
