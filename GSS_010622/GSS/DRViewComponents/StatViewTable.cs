using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DDDObjects;
using DRStatistics;
using StatSelectorComponents;
using SourceGrid;

namespace StatViewComponents
{
  public partial class StatViewTable : StatViewBase
  {
    #region Private members
    private bool showSums = true;
    #endregion //Private members

    #region Public properties
    /// <summary>
    /// Show sum columns and rows
    /// </summary>
    public bool ShowSums
    {
      set 
      {
        if (value && !showSums)
          AddSumCellsToTable();
        if (!value && showSums)
          RemoveSumCellsFromTable();
        showSums = value; 
      }
      get { return showSums; }
    }
    #endregion //Public properties

    #region Construction initialization
    public StatViewTable()
    {
      InitializeComponent();
    }
    #endregion //Construction initialization

    #region Virtual method overrides
    /// <summary>
    /// Handles new data ready event from selector component
    /// </summary>
    /// <param name="sender">Selector component</param>
    protected override void OnNewDataReady(StatSelectorBase sender, NDREventArgs args)
    {
      try
      {
        base.OnNewDataReady(sender, args);
        //Check if control shall display data
        if (!displayData)
          return;
        OnInfoMessage("Loading statistical data into table view");
        //Clear old data, initiate parameters
        InitiateTable();
        //Create header rows and columns
        SetTableHeaders();
        //Add data cells
        AddDataToTable();
        //Show sum cells, if requested
        if (showSums)
          AddSumCellsToTable();
        //Autosize table cells
        gStat.AutoSizeCells();
        //Send info message
        OnInfoMessage("Statistical data loaded into table view");
      }
      catch (Exception ex)
      {
        OnErrorMessage(new StatViewException(ex, "Failed to load statistical data into table view"));
      }
      finally
      {
      }
    }

    /// <summary>
    /// Stores reference to assigned helper object.
    /// </summary>
    /// <param name="helper">Helper object</param>
    protected override void SetDDDHelper(DDDHelper helper)
    {
      base.SetDDDHelper(helper);
    }
    #endregion //Virtual method overrides

    #region Public methods
    /// <summary>
    /// Exports statistics data from the grid into CSV file
    /// </summary>
    /// <param name="streamWriter">Writer for the CSV file</param>
    public void ExportToCSVFile(StreamWriter streamWriter)
    {
      SourceGrid.Exporter.CSV csvExport = new SourceGrid.Exporter.CSV();
      csvExport.FieldSeparator = ";";
      csvExport.LineSeparator = "\n";
      csvExport.Export(gStat, streamWriter);
    }
    #endregion //Public methods

    #region Helper methods
    /// <summary>
    /// Removes all rows and columns from the table, sets initial parameters
    /// </summary>
    private void InitiateTable()
    {
      //Remove all rows
      gStat.Rows.Clear();
      //Remove all columns
      gStat.Columns.Clear();
      //Set parameters
      //gStat.BorderStyle = BorderStyle.FixedSingle;
      gStat.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
      gStat.ClipboardMode = ClipboardMode.Copy;
      gStat.SelectionMode = GridSelectionMode.Cell;
      gStat.Selection.EnableMultiSelection = true;
    }

    /// <summary>
    /// Adds all necessary columns and rows to the table, sets titels of rows and columns
    /// </summary>
    private void SetTableHeaders()
    {
      GroupsDescriptors groupDescs = selector.StatisticsHelper.GroupsDescriptors;
      //Compute number of columns and rows
      int fixedRows = Math.Max(0, groupDescs.GetGroupLevelsCount() - 1);
      int fixedCols = 1;
      int dataRows = 1;
      int dataCols = 1;
      if (groupDescs.GetGroupLevelsCount() > 0)
        dataRows = groupDescs.GetGroupCount(groupDescs.GetGroupLevelsCount() - 1);
      if (groupDescs.GetGroupLevelsCount() > 1)
        for (int level = 0; level < (groupDescs.GetGroupLevelsCount() - 1); level++)
          dataCols = dataCols * groupDescs.GetGroupCount(level);
      //Now set grid column count and add rows
      gStat.ColumnsCount = fixedCols + dataCols;
      gStat.FixedColumns = fixedCols;
      gStat.FixedRows = fixedRows;
      for (int rowIndx = 0; rowIndx < (fixedRows + dataRows); rowIndx++)
        gStat.Rows.Insert(rowIndx);
      //Create column headers, start from the last fixed row and go up
      int colSpan = 1;
      for (int rowIndx = fixedRows - 1; rowIndx >= 0; rowIndx--)
      {
        int groupIndex = 0;
        for (int colIndx = fixedCols; colIndx < gStat.ColumnsCount;)
        {
          gStat[rowIndx, colIndx] = new StatColHeader(groupDescs.GetGroupTitle(rowIndx, groupIndex));
          gStat[rowIndx, colIndx].ColumnSpan = colSpan;
          colIndx += colSpan;
          groupIndex++;
          if (groupIndex == groupDescs.GetGroupCount(rowIndx))
            groupIndex = 0;
        }
        colSpan *= groupDescs.GetGroupCount(rowIndx);
        if (colSpan > Grid.MaxSpan)
          Grid.MaxSpan = colSpan;
      }
      //Create row headers
      if (groupDescs.GetGroupLevelsCount() < 1)
        gStat[0, 0] = new StatRowHeader("Count");
      else
        for (int rowIndx = fixedRows; rowIndx < fixedRows + groupDescs.GetGroupCount(fixedRows); rowIndx++)
          gStat[rowIndx, 0] = new StatRowHeader(groupDescs.GetGroupTitle(fixedRows, rowIndx - fixedRows));
      //Create header in top left corner
      if (fixedRows > 0)
      {
        gStat[0, 0] = new StatColHeader("");
        gStat[0, 0].RowSpan = fixedRows;
      }
    }

    /// <summary>
    /// Adds values to table data cells
    /// </summary>
    private void AddDataToTable()
    {
      GroupValue groupValue = selector.StatisticsHelper.GroupValue;
      int colIndx = gStat.FixedColumns;
      AddValueToTable(0, 0, groupValue, ref colIndx); 
    }

    /// <summary>
    /// Recursively called method which iterates over all groups on all levels and fills 
    /// appropriate values to data table
    /// </summary>
    private void AddValueToTable(int groupLevel, int currIndex, GroupValue currValue, ref int colIndx)
    {
      GroupsDescriptors groupDescs = selector.StatisticsHelper.GroupsDescriptors;
      if (groupLevel == groupDescs.GetGroupLevelsCount())
        gStat[gStat.FixedRows + currIndex, colIndx] = new StatDataCell(currValue.Value);
      else
      {
        for (int indx = 0; indx < groupDescs.GetGroupCount(groupLevel); indx++)
        {
          int dataIndx = groupDescs.GetGroupIndex(groupLevel, indx);
          AddValueToTable(groupLevel + 1, indx, currValue.GetChildGroup(dataIndx), ref colIndx);
        }
        if (groupLevel == groupDescs.GetGroupLevelsCount() - 1)
          colIndx++;
      }
    }


    /// <summary>
    /// Adds rows / columns to table which contains sums for each column / row
    /// </summary>
    private void AddSumCellsToTable()
    {
      //Add sum column at the end, if necessary
      if (gStat.ColumnsCount > 2)
      {
        //Insert sum column at the end
        int sumColIndx = gStat.ColumnsCount;
        gStat.Columns.Insert(sumColIndx);
        //Create header cell for sum column
        gStat[0, sumColIndx] = new StatSumColHeader("");
        gStat[0, sumColIndx].RowSpan = gStat.FixedRows;
        //Create summ cell for each non fixed row
        for (int rowIndx = gStat.FixedRows; rowIndx < gStat.RowsCount; rowIndx++)
          gStat[rowIndx, sumColIndx] = new StatSumRowCell((StatRowHeader)gStat[rowIndx, 0]);
      }
      //Add sum rows if necessary
      if ((gStat.RowsCount - gStat.FixedRows) > 1)
      {//Add one sum row for each fixed row
        for (int i = gStat.FixedRows - 1; i >=0 ; i--)
        {
          //Insert sum row
          int sumRowIndx = gStat.RowsCount;
          gStat.Rows.Insert(sumRowIndx);
          //Create header cell for sum row
          gStat[sumRowIndx, 0] = new StatSumRowHeader("");
          //Create sum cell for each non fixed column
          for (int colIndx = gStat.FixedColumns; colIndx < gStat.ColumnsCount; )
          {
            StatColHeader colHeader = (StatColHeader)gStat[i, colIndx];
            gStat[sumRowIndx, colIndx] = new StatSumColCell(colHeader);
            colIndx += colHeader.ColumnSpan;
          }
        }
        if (gStat.FixedRows == 0)
        {//Only one group - add one sum cell at the bottom
          //Insert sum row
          int sumRowIndx = gStat.RowsCount;
          gStat.Rows.Insert(sumRowIndx);
          //Create header cell for sum row
          gStat[sumRowIndx, 0] = new StatSumRowHeader("");
          //Create sum cell
          gStat[sumRowIndx, 1] = new StatSumColCell(1, gStat);
        }
      }
    }

    /// <summary>
    /// Remove column / rows with sums from the table
    /// </summary>
    private void RemoveSumCellsFromTable()
    {
      if (gStat.ColumnsCount > 0)
        while (gStat[0, gStat.ColumnsCount - 1].GetType() == typeof(StatSumColHeader))
          gStat.Columns.Remove(gStat.ColumnsCount - 1);
      if (gStat.RowsCount > 0)
        while (gStat[gStat.RowsCount - 1, 0].GetType() == typeof(StatSumRowHeader))
          gStat.Rows.Remove(gStat.RowsCount - 1);
    }
    #endregion //Helper methods

    #region Event handlers
    #endregion //Event handlers
  }

  #region Grid helper classes
  #region Cell views
  public class StatColHeaderView : SourceGrid.Cells.Views.ColumnHeader
  {
    public static StatColHeaderView StatView = new StatColHeaderView();
    public StatColHeaderView()
    {
//      this.Font = new Font(FontFamily.GenericMonospace, 8, FontStyle.Bold);
			this.TextAlignment = DevAge.Drawing.ContentAlignment.MiddleCenter;
    }
  }

  public class StatRowHeaderView : SourceGrid.Cells.Views.RowHeader
  {
    public static StatRowHeaderView StatView = new StatRowHeaderView();
    public StatRowHeaderView()
    {
//      this.Font = new Font(FontFamily.GenericMonospace, 8, FontStyle.Bold);
      this.TextAlignment = DevAge.Drawing.ContentAlignment.MiddleLeft;
    }
  }

  public class StatDataCellView : SourceGrid.Cells.Views.Cell
  {
    public static StatDataCellView StatView = new StatDataCellView();
    public StatDataCellView()
    {
      this.TextAlignment = DevAge.Drawing.ContentAlignment.MiddleRight;
    }
  }

  public class StatSumCellView : SourceGrid.Cells.Views.Cell
  {
    public static StatSumCellView StatView = new StatSumCellView();
    public StatSumCellView()
    {
      this.TextAlignment = DevAge.Drawing.ContentAlignment.MiddleRight;
      this.BackColor = Color.Yellow;
      this.ForeColor = Color.Black;
    }
  }
  #endregion //Cell views

  #region Cell controllers
  public class StatRowHeaderClickController : SourceGrid.Cells.Controllers.ControllerBase
  {
    public static StatRowHeaderClickController Controller = new StatRowHeaderClickController();

    public override void OnClick(CellContext sender, EventArgs e)
    {
      base.OnClick(sender, e);

      if (sender.Cell.GetType() == typeof(StatRowHeader) || sender.Cell.GetType() == typeof(StatSumRowHeader))
      {
        StatRowHeader headerCell = (StatRowHeader)sender.Cell;
        headerCell.Grid.Selection.ResetSelection(false);
        headerCell.Grid.Selection.SelectRange(headerCell.Row.Range, true);
        headerCell.Grid.Selection.FocusFirstCell(false);
      }
      else if (sender.Cell.GetType() == typeof(StatColHeader) || sender.Cell.GetType() == typeof(StatSumColHeader))
      {
        StatColHeader headerCell = (StatColHeader)sender.Cell;
        headerCell.Grid.Selection.ResetSelection(false);
        Range range;
        if (headerCell.Column.Index == 0)
          range = new Range(0,0,headerCell.Grid.RowsCount-1, headerCell.Grid.ColumnsCount-1);
        else
        {
          range = headerCell.Column.Range;
          range.ColumnsCount = headerCell.ColumnSpan;
        }
        headerCell.Grid.Selection.SelectRange(range, true);
        headerCell.Grid.Selection.FocusFirstCell(false);
      }
    }
  }
  #endregion //Cell controllers

  #region Cell types
  public class StatColHeader : SourceGrid.Cells.ColumnHeader
  {
    public StatColHeader(String value)
      : base(value)
    {
      this.View = StatColHeaderView.StatView;
      this.AutomaticSortEnabled = false;
      this.Controller.AddController(StatRowHeaderClickController.Controller);
    }
  }

  public class StatRowHeader : SourceGrid.Cells.RowHeader
  {
    public StatRowHeader(String value)
      : base(value)
    {
      this.View = StatRowHeaderView.StatView;
      this.Controller.AddController(StatRowHeaderClickController.Controller);
    }
  }

  public class StatDataCell : SourceGrid.Cells.Cell
  {
    public StatDataCell(int value)
      : base(value)
    {
      this.View = StatDataCellView.StatView;
    }
  }

  public class StatSumRowCell : SourceGrid.Cells.Cell
  {
    public StatSumRowCell(StatRowHeader rowHeader)
      : base()
    {
      this.View = new StatSumCellView();
      int sum = 0;
      for (int colIndx = 0; colIndx < rowHeader.Grid.ColumnsCount; colIndx++)
      {
        SourceGrid.Cells.ICell cell = rowHeader.Grid[rowHeader.Row.Index, colIndx];
        if (cell != null && cell.Value != null &&
          (cell.GetType() == typeof(StatDataCell) || cell.GetType() == typeof(StatSumColCell)))
          sum += (int)cell.Value;
      }
      this.Value = sum;
    }
  }

  public class StatSumColCell : SourceGrid.Cells.Cell
  {
    public StatSumColCell(StatColHeader colHeader)
      : base()
    {
      this.View = new StatSumCellView();
      this.ColumnSpan = colHeader.ColumnSpan;
      if (this.ColumnSpan > 1)
        this.View.TextAlignment = DevAge.Drawing.ContentAlignment.MiddleCenter;
      int sum = 0;
      for(int c = 0; c < colHeader.ColumnSpan; c++)
        for (int rowIndx = 0; rowIndx < colHeader.Grid.RowsCount; rowIndx++)
        {
          SourceGrid.Cells.ICell cell = colHeader.Grid[rowIndx, colHeader.Column.Index + c];
          if (cell != null && cell.Value != null &&
            (cell.GetType() == typeof(StatDataCell) || cell.GetType() == typeof(StatSumRowCell)))
            sum += (int)cell.Value;
        }
      this.Value = sum;
    }

    public StatSumColCell(int colIndex, Grid grid)
      : base()
    {
      this.View = new StatSumCellView();
      int sum = 0;
      for (int rowIndx = 0; rowIndx < grid.RowsCount; rowIndx++)
      {
        SourceGrid.Cells.ICell cell = grid[rowIndx, colIndex];
        if (cell != null && cell.Value != null &&
          (cell.GetType() == typeof(StatDataCell) || cell.GetType() == typeof(StatSumRowCell)))
          sum += (int)cell.Value;
      }
      this.Value = sum;
    }
  }

  public class StatSumColHeader : StatColHeader
  {
    public StatSumColHeader(String value)
      : base(value)
    { }
  }

  public class StatSumRowHeader : StatRowHeader
  {
    public StatSumRowHeader(String value)
      : base(value)
    { }
  }
  #endregion //Cell types
  #endregion //Grid helper classes
}

