namespace DRViewComponents
{
  partial class MergeGridView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.mainGrid = new System.Windows.Forms.DataGridView();
      this.hScrollBar = new System.Windows.Forms.HScrollBar();
      this.vScrollBar = new System.Windows.Forms.VScrollBar();
      ((System.ComponentModel.ISupportInitialize)(this.mainGrid)).BeginInit();
      this.SuspendLayout();
      // 
      // mainGrid
      // 
      this.mainGrid.AllowUserToAddRows = false;
      this.mainGrid.AllowUserToDeleteRows = false;
      this.mainGrid.AllowUserToResizeRows = false;
      this.mainGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.mainGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
      this.mainGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.mainGrid.CausesValidation = false;
      this.mainGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.mainGrid.ColumnHeadersVisible = false;
      this.mainGrid.EnableHeadersVisualStyles = false;
      this.mainGrid.Location = new System.Drawing.Point(12, 103);
      this.mainGrid.Name = "mainGrid";
      this.mainGrid.ReadOnly = true;
      this.mainGrid.RowHeadersVisible = false;
      this.mainGrid.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.mainGrid.Size = new System.Drawing.Size(331, 66);
      this.mainGrid.TabIndex = 0;
      // 
      // hScrollBar
      // 
      this.hScrollBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.hScrollBar.Location = new System.Drawing.Point(0, 183);
      this.hScrollBar.Name = "hScrollBar";
      this.hScrollBar.Size = new System.Drawing.Size(383, 17);
      this.hScrollBar.TabIndex = 1;
      // 
      // vScrollBar
      // 
      this.vScrollBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.vScrollBar.Location = new System.Drawing.Point(383, -1);
      this.vScrollBar.Name = "vScrollBar";
      this.vScrollBar.Size = new System.Drawing.Size(17, 185);
      this.vScrollBar.TabIndex = 2;
      // 
      // MergeGridView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.Controls.Add(this.vScrollBar);
      this.Controls.Add(this.hScrollBar);
      this.Controls.Add(this.mainGrid);
      this.Name = "MergeGridView";
      this.Size = new System.Drawing.Size(400, 200);
      ((System.ComponentModel.ISupportInitialize)(this.mainGrid)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    public System.Windows.Forms.DataGridView mainGrid;
    private System.Windows.Forms.HScrollBar hScrollBar;
    private System.Windows.Forms.VScrollBar vScrollBar;


  }
}
