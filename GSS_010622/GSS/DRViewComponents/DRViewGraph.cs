using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Gigasoft.ProEssentials.Enums;
using Gigasoft.ProEssentials;
using DRSelectorComponents;
using DDDObjects;

namespace DRViewComponents
{
  #region Delegates definitions
  /// <summary>
  /// Delegate defines event handler for Close graph event
  /// </summary>
  /// <param name="sender">DRViewGraph component requesting to be closed</param>
  public delegate void CloseGraphEventHandler(DRViewGraph sender, EventArgs args);
  #endregion //Delegates definitions

  public partial class DRViewGraph : DRViewComponents.DRViewBase
  {
    #region Private members
    GraphPropDlg propDlg; //properties dialog
    VarIDDictionary selVariables; //variable selected for visualisation
    int boolVarCount; //number of displayed bool variables
    #endregion //Private members

    #region Public Events
    /// <summary>
    /// Event occurs when user clicks the close button on control
    /// </summary>
    public event CloseGraphEventHandler OnCloseGraph;
    #endregion //Public Events

    #region Construction, initialization
    public DRViewGraph()
    {
      InitializeComponent();
      //Create properties dialog 
      propDlg = new GraphPropDlg();
      selVariables = new VarIDDictionary();
    }
    #endregion //Construction, initialization

    #region Helper methods
    private void SetGraphParameters()
    {
      //First reset to original state
      //graph.PeFunction.Reset();
      //Set properties
      graph.PeData.SubsetByPoint = false;
      graph.PeData.UsingXDataii = true;
      graph.PeGrid.Option.CustomGridNumbersX = true;
      graph.PeGrid.Option.CustomGridNumbersRY = true;

      graph.PePlot.Allow.Step = true;
      graph.PePlot.Allow.Area = false;
      graph.PePlot.Allow.Bar = false;
      graph.PePlot.DataShadows = DataShadows.None;

      graph.PeLegend.SimpleLine = true;
      graph.PeColor.GraphBackground = Color.AliceBlue;
      graph.PeColor.Desk = Color.White;
      graph.PeConfigure.BorderTypes = TABorder.SingleLine;
      graph.PeString.MainTitle = "";
      graph.PeString.SubTitle = "";
      graph.PeString.XAxisLabel = "";
      graph.PeString.RYAxisLabel = "";
      graph.PeString.YAxisLabel = "";

      graph.PeGrid.Configure.ManualMaxRY = 10;
      graph.PeGrid.Configure.ManualMinRY = 0;
      graph.PeGrid.Configure.ManualScaleControlRY = ManualScaleControl.MinMax;
      graph.PeGrid.Option.ShowTickMarkRY = ShowTickMarks.TicksHide;
      graph.PeGrid.Option.ShowRYAxis = ShowAxis.GridNumbers;

      graph.PeUserInterface.Allow.Zooming = AllowZooming.HorzAndVert;
      graph.PeUserInterface.Menu.PlotMethod = MenuControl.Hide;
      graph.PeUserInterface.Menu.IncludeDataLabels = MenuControl.Hide;
      graph.PeUserInterface.Menu.DataShadow = MenuControl.Hide;
      graph.PeUserInterface.Menu.DataPrecision = MenuControl.Hide;
      graph.PeUserInterface.Menu.Maximize = MenuControl.Hide;
      graph.PeUserInterface.Menu.Help = MenuControl.Hide;
      graph.PeUserInterface.Dialog.Subsets = false;

      //Create custom menu
      graph.PeUserInterface.Menu.CustomMenuText[0] = "|";
      graph.PeUserInterface.Menu.CustomMenuText[1] = "Select Variables";
      graph.PeUserInterface.Menu.CustomMenuText[2] = "Reset";
      graph.PeUserInterface.Menu.CustomMenuText[3] = "Close Graph";

      // Set all menu items to bottom location //
      graph.PeUserInterface.Menu.CustomMenuLocation[0] = CustomMenuLocation.Bottom;
      graph.PeUserInterface.Menu.CustomMenuLocation[1] = CustomMenuLocation.Bottom;
      graph.PeUserInterface.Menu.CustomMenuLocation[2] = CustomMenuLocation.Bottom;
      graph.PeUserInterface.Menu.CustomMenuLocation[3] = CustomMenuLocation.Bottom;
      
      graph.PeFunction.Reinitialize();
      graph.PeFunction.ReinitializeResetImage();
      graph.Refresh();
    }

    /// <summary>
    /// Display dialog with properties and set graph according to result
    /// </summary>
    private void ShowPropDialog()
    {
      //Display dialog
      if (propDlg.ShowDialog() != DialogResult.OK)
        return; //Cancel pressed
      //Show variables in graph
      selVariables = propDlg.SelectedVars;
      DisplayVariables();
    }

    /// <summary>
    /// Displays one analog variable in graph
    /// </summary>
    /// <param name="varDef">Variable definition</param>
    /// <param name="subsetIndx">Index of graph subset</param>
    private void DisplayAnalogVar(VarDefIDValue varDef, int subsetIndx)
    {
      try
      {
        //Obtain values for given variables
        List<VarValue> values;
        values = selector.GetValuesOfVariable(varDef.TUInstance, varDef.DDDVersion, varDef.DDDVariable);
        //Sort values
        values.Sort();
        //Set (correct number of points in graph)
        if (graph.PeData.Points < values.Count)
          graph.PeData.Points = values.Count;
        //Fill in values into graph
        for (int i = 0; i < values.Count; i++)
        {
          graph.PeData.Xii[subsetIndx, i] = values[i].TimeStamp.ToOADate();
          //Add value scaled based on the representation
          Object value = DDDHelper.GetElementaryVariable(values[i].DDDVersionID, values[i].VariableID).ScaleValue(values[i].Value);
          graph.PeData.Y[subsetIndx, i] = Convert.ToSingle(value);
        }
        //Set subset name
        if (propDlg.ShowCompVarTitle)
          graph.PeString.SubsetLabels[subsetIndx] = varDef.DDDVariable.GetCompleteVarTitle();
        else
          graph.PeString.SubsetLabels[subsetIndx] = varDef.DDDVariable.Title;
        //Set subset plotting method
        graph.PePlot.Methods[subsetIndx] = propDlg.AnalogPlottingMethod;
        //Set line and mark type
        graph.PePlot.SubsetLineTypes[subsetIndx] = LineType.ThinSolid;
        graph.PePlot.SubsetPointTypes[subsetIndx] = PointType.Cross;
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRViewException(ex, "Failed to display variable {0}", varDef.DDDVariable.Title));
      }
    }

    /// <summary>
    /// Displays one bool variable in graph
    /// </summary>
    /// <param name="varDef">Variable definition</param>
    /// <param name="subsetIndx">Index of graph subset</param>
    private void DisplayBoolVar(BitDefIDValue varDef, int subsetIndx)
    {
      DDDBitsetRepres repres = null;
      try
      {
        int val;
        repres = (DDDBitsetRepres)(((DDDElemVar)varDef.DDDVariable).Representation);
        //Obtain values for given variables
        List<VarValue> values;
        values = selector.GetValuesOfVariable(varDef.TUInstance, varDef.DDDVersion, varDef.DDDVariable);
        //Sort values
        values.Sort();
        //Set (correct number of points in graph)
        if (graph.PeData.Points < values.Count)
          graph.PeData.Points = values.Count;
        //Fill in values into graph
        for (int i = 0; i < values.Count; i++)
        {
          graph.PeData.Xii[subsetIndx, i] = values[i].TimeStamp.ToOADate();
          val = ((Convert.ToInt64(values[i].Value) & varDef.Mask) > 0) ? 1 : 0;
          graph.PeData.Y[subsetIndx, i] = (Single)(val + boolVarCount*2 + 1);
        }
        //Set subset name
        if (propDlg.ShowCompVarTitle)
          graph.PeString.SubsetLabels[subsetIndx] = varDef.DDDVariable.GetCompleteVarTitle() + DDDHelper.TitleSeparator + repres.GetBitTitle((ulong)varDef.Mask);
        else
          graph.PeString.SubsetLabels[subsetIndx] = repres.GetBitTitle((ulong)varDef.Mask);
        //Set subset plotting method
        graph.PePlot.Methods[subsetIndx] = propDlg.BoolPlottingMethod;
        //Set line and mark type
        graph.PePlot.SubsetLineTypes[subsetIndx] = LineType.ThinSolid;
        graph.PePlot.SubsetPointTypes[subsetIndx] = PointType.Cross;
      }
      catch (Exception ex)
      {
        if (varDef.DDDVariable.IsBitSet())
          OnErrorMessage(new DRViewException(ex, "Failed to display bit {0}", repres.GetBitTitle((ulong)varDef.Mask)));
        else
          OnErrorMessage(new DRViewException(ex, "Failed to display bit from variable {0}", varDef.DDDVariable.Title));
      }
    }
    #endregion //Helper methods

    #region Overriden methods
    protected override void SetDDDHelper(DDDObjects.DDDHelper helper)
    {
      base.SetDDDHelper(helper);
      //Set DDDHelper for properties dialog
      propDlg.DDDHelper = helper;
    }

    protected override void OnNewDataReady(DRSelectorComponents.DRSelectorBase sender, DRSelectorComponents.NDREventArgs args)
    {
      try
      {
        //Base implementation
        base.OnNewDataReady(sender, args);
        if (!displayData)
          return;
        //Create variable tree
        propDlg.CreateVarTree(sender.RecordsView, sender.VariableFilter, selVariables);
        selVariables = propDlg.SelectedVars;
        //Display selected variables
        DisplayVariables();
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRViewException(ex, "Failed to display variables in graph"));
      }
    }
    #endregion //Overriden methods

    #region Event handlers
    private void btSelectVars_Click(object sender, EventArgs e)
    {
      //Show dialog with variables and properties
      ShowPropDialog();
    }

    /// <summary>
    /// Handles formatting of values for X and RY axes
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void graph_PeCustomGridNumber(object sender, Gigasoft.ProEssentials.EventArg.CustomGridNumberEventArgs e)
    {
      if (e.AxisType == 2)
      { //X-axis, format string with milliseconds
        e.NumberString = DateTime.FromOADate(e.NumberValue).ToString(dddHelper.TimeFormatGraph);
      }
      if (e.AxisType == 1)
      { //Rigth Y-axis (bool signals), format numbers as 0s and 1s
        int iVal = (int)e.NumberValue;
        if (((double)iVal != e.NumberValue) || (iVal >= (boolVarCount*2+1)) )
          e.NumberString = "";
        else if ((iVal % 2) == 1)
          e.NumberString = DDDObjects.DDDHelper.BoolFalseSymbol;
        else if (iVal != 0)
          e.NumberString = DDDObjects.DDDHelper.BoolTrueSymbol;
        else
          e.NumberString = "";
      }
    }

    /// <summary>
    /// Reset graph properties back to original values
    /// </summary>
    private void btReset_Click(object sender, EventArgs e)
    {
      SetGraphParameters();
    }

    /// <summary>
    /// Raise the OnCloseGraph event
    /// </summary>
    private void btClose_Click(object sender, EventArgs e)
    {
      if (OnCloseGraph != null)
        OnCloseGraph(this, new EventArgs());
    }

    /// <summary>
    /// Handle clicks on custom menu items
    /// </summary>
    private void graph_PeCustomMenu(object sender, Gigasoft.ProEssentials.EventArg.CustomMenuEventArgs e)
    {
      //Index is 0 base, separator is first
      if (e.MenuIndex == 1)
        btSelectVars_Click(this, null);
      else if (e.MenuIndex == 2)
        btReset_Click(this, null);
      else if (e.MenuIndex == 3)
        btClose_Click(this, null);
    }
    #endregion //Event handlers

    #region Public methods
    /// <summary>
    /// Initiates graph parameters and tree of variables
    /// </summary>
    /// <param name="selector">selector component</param>
    /// <param name="selVariables">variables selected with different component which shall be displayed in graph or null</param>
    public void InitiateGraph(DRSelectorBase selector, VarIDDictionary selVariables)
    {
      //Store selected variables
      if (selVariables != null)
        this.selVariables = selVariables;
      //Preset graph parameters
      SetGraphParameters();
      //No subsets
      graph.PeData.Subsets = 0;
      graph.PeData.Points = 0;
      //Disable labels
      graph.PeGrid.Option.ShowXAxis = ShowAxis.Empty;
      graph.PeGrid.Option.ShowYAxis = ShowAxis.Empty;
      //0 bool vars
      boolVarCount = 0;
      //Store selector
      this.selector = selector;
      //Create variable tree
      propDlg.CreateVarTree(selector.RecordsView, selector.VariableFilter, selVariables);
    }

    /// <summary>
    /// Display selected variables in graph
    /// </summary>
    /// <param name="selVariables"></param>
    public void DisplayVariables()
    {
      try
      {
        if (selVariables.Variables.Count > 0)
          OnInfoMessage("Displaying selected variables in graph");
        //Clear graph of all data
        graph.PeData.Y.Clear();
        graph.PeData.Xii.Clear();
        graph.PeData.Subsets = 0;
        graph.PeData.Points = 0;
        boolVarCount = 0;
        //Set number of subsets
        graph.PeData.Subsets = selVariables.Variables.Count;
        //Show axis, if subsets > 0
        if (graph.PeData.Subsets > 0)
        {
          graph.PeGrid.Option.ShowXAxis = ShowAxis.All;
          graph.PeGrid.Option.ShowYAxis = ShowAxis.All;
        }
        else
        {
          graph.PeGrid.Option.ShowXAxis = ShowAxis.Empty;
          graph.PeGrid.Option.ShowYAxis = ShowAxis.Empty;
        }
        //Iterate over selected variables, display them
        int subsetIndx = 0;
        foreach (VarDefIDValue selVariable in selVariables.Variables)
        {//First iterate over analog vars
          if (selVariable.GetType() == typeof(BitDefIDValue))
            continue; //no bool in first round
          DisplayAnalogVar(selVariable, subsetIndx);
          subsetIndx++;
        }
        foreach (VarDefIDValue selVariable in selVariables.Variables)
        {//Now display bool variables
          if (selVariable.GetType() != typeof(BitDefIDValue))
            continue; //only bools now
          DisplayBoolVar((BitDefIDValue)selVariable, subsetIndx);
          boolVarCount++;
          subsetIndx++;
        }
        //Set maximum for bool axis
        graph.PeGrid.Configure.ManualMaxRY = boolVarCount * 2 + 2;
        //Reinitialize graph
        graph.PeFunction.Reinitialize();
        graph.PeFunction.ReinitializeResetImage();
        graph.Refresh();
        //Set X-axis label frequency
        //AltFrequencies
        if (selVariables.Variables.Count > 0)
          OnInfoMessage("Variables displayed in graph");
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRViewException(ex, "Failed to display variables in graph"));
      }
    }
    #endregion //Public methods
  }
}

