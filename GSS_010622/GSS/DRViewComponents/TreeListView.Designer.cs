namespace DRViewComponents
{
  partial class TreeListView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TreeListView));
      System.Windows.Forms.TreeListViewItemCollection.TreeListViewItemCollectionComparer treeListViewItemCollectionComparer1 = new System.Windows.Forms.TreeListViewItemCollection.TreeListViewItemCollectionComparer();
      this.imgListIcons = new System.Windows.Forms.ImageList(this.components);
      this.view = new System.Windows.Forms.TreeListView();
      this.colNodes = new System.Windows.Forms.ColumnHeader();
      this.colDetail1 = new System.Windows.Forms.ColumnHeader();
      this.colDetail2 = new System.Windows.Forms.ColumnHeader();
      this.SuspendLayout();
      // 
      // imgListIcons
      // 
      this.imgListIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgListIcons.ImageStream")));
      this.imgListIcons.TransparentColor = System.Drawing.Color.Fuchsia;
      this.imgListIcons.Images.SetKeyName(0, "default.bmp");
      this.imgListIcons.Images.SetKeyName(1, "selected.bmp");
      this.imgListIcons.Images.SetKeyName(2, "Event.ICO");
      this.imgListIcons.Images.SetKeyName(3, "trace.ico");
      this.imgListIcons.Images.SetKeyName(4, "snap.ico");
      this.imgListIcons.Images.SetKeyName(5, "attributes.bmp");
      this.imgListIcons.Images.SetKeyName(6, "FaultCode.ICO");
      this.imgListIcons.Images.SetKeyName(7, "Time.ICO");
      this.imgListIcons.Images.SetKeyName(8, "TUName.BMP");
      this.imgListIcons.Images.SetKeyName(9, "TUType.BMP");
      this.imgListIcons.Images.SetKeyName(10, "DDDVersion.bmp");
      this.imgListIcons.Images.SetKeyName(11, "Recinstance.ico");
      this.imgListIcons.Images.SetKeyName(12, "EnvData.ICO");
      this.imgListIcons.Images.SetKeyName(13, "SimpleInput.ICO");
      this.imgListIcons.Images.SetKeyName(14, "Array.ICO");
      this.imgListIcons.Images.SetKeyName(15, "HistInput.ICO");
      this.imgListIcons.Images.SetKeyName(16, "structure.bmp");
      this.imgListIcons.Images.SetKeyName(17, "ElemVar.bmp");
      this.imgListIcons.Images.SetKeyName(18, "train.ico");
      this.imgListIcons.Images.SetKeyName(19, "vehicle.ico");
      // 
      // view
      // 
      this.view.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colNodes,
            this.colDetail1,
            this.colDetail2});
      treeListViewItemCollectionComparer1.Column = 0;
      treeListViewItemCollectionComparer1.SortOrder = System.Windows.Forms.SortOrder.None;
      this.view.Comparer = treeListViewItemCollectionComparer1;
      this.view.Dock = System.Windows.Forms.DockStyle.Fill;
      this.view.GridLines = true;
      this.view.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
      this.view.HideSelection = false;
      this.view.Location = new System.Drawing.Point(0, 0);
      this.view.MultiSelect = false;
      this.view.Name = "view";
      this.view.Size = new System.Drawing.Size(306, 212);
      this.view.SmallImageList = this.imgListIcons;
      this.view.Sorting = System.Windows.Forms.SortOrder.None;
      this.view.TabIndex = 0;
      this.view.UseCompatibleStateImageBehavior = false;
      this.view.AfterCollapse += new System.Windows.Forms.TreeListViewEventHandler(this.view_AfterCollapse);
      this.view.BeforeExpand += new System.Windows.Forms.TreeListViewCancelEventHandler(this.view_BeforeExpand);
      this.view.SizeChanged += new System.EventHandler(this.view_SizeChanged);
      // 
      // colNodes
      // 
      this.colNodes.Text = "Node";
      this.colNodes.Width = 100;
      // 
      // colDetail1
      // 
      this.colDetail1.Text = "Detail1";
      this.colDetail1.Width = 100;
      // 
      // colDetail2
      // 
      this.colDetail2.Text = "Detail2";
      this.colDetail2.Width = 100;
      // 
      // TreeListView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.view);
      this.Name = "TreeListView";
      this.Size = new System.Drawing.Size(306, 212);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ImageList imgListIcons;
    private System.Windows.Forms.ColumnHeader colNodes;
    private System.Windows.Forms.ColumnHeader colDetail1;
    private System.Windows.Forms.ColumnHeader colDetail2;
    private System.Windows.Forms.TreeListView view;
  }
}
