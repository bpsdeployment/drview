  //Define global variables
  var map = null; //map object
  var markerManager = null; //manager of marker objects
  var markers = null; //array of marker objects
  var selRecIndx = null; //index of currently selected record
  var maxRecsPerTab; //maximum number of records displayed in one tab in info window
  var tabCaptionBase; //text displayed in caption of info tab, together with index of tab
  var invGPSMarkerIcon; //Icon used to display marker for records with invalid GPS data
  var invGPSText; //Text displayed instead of GPS coordinates for records with invalid GPS data
  var invGPSMarkerIndex; //Index of marker with invalid GPS data
  var showTUNames; //Names of diagnostic units shall be displayed in info windows with record titles
    
  //Create map object and place it into the box
  function loadMap(MaxRecsPerTab, TabCaptionBase, InvGPSIconURL, InvGPSText)
  {
    //check for browser compatibility
    if (!GBrowserIsCompatible())
      return;

    //Create map object, add necessary controls
    map = new GMap2(document.getElementById('mapHolder'));
    map.addControl(new GZoomControl(), new GControlPosition(G_ANCHOR_TOP_LEFT, new GSize(10,30)));
    map.addControl(new GLargeMapControl(), new GControlPosition(G_ANCHOR_TOP_LEFT, new GSize(10,55)));
    map.addControl(new GMapTypeControl());
    map.setCenter(new GLatLng(0, 0), 8);
    //Set map center and zoom
    map.setCenter(new GLatLng(0, 0), 8);
    
    //Create icon for invalid GPS marker
    invGPSMarkerIcon = new GIcon();
    invGPSMarkerIcon.image = InvGPSIconURL;
    invGPSMarkerIcon.iconSize=new GSize(32,32);
    invGPSMarkerIcon.shadowSize=new GSize(56,32);
    invGPSMarkerIcon.iconAnchor=new GPoint(16,32);
    invGPSMarkerIcon.infoWindowAnchor=new GPoint(16,0);     
    //Store parameters to global variables
    maxRecsPerTab = MaxRecsPerTab; tabCaptionBase = TabCaptionBase; invGPSText = InvGPSText;
    
    //Add map event handlers
    //Add event listener to map moveend event
    GEvent.addListener(map, "moveend", function() {placeInvGPSMarker();});
    //Add event listener to map zoomend event
    GEvent.addListener(map, "zoomend", function(oldLevel, newLevel) {placeInvGPSMarker();});
    //Add event listner to map on click
    GEvent.addListener(map, "click", function(marker, point) {showMarkerInfoWindow(marker);});
  }
    
  //function places markers on the map
  function addMarkers(markersXmlStr, ShowTUNames)
  {
    //check for browser compatibility
    if (!GBrowserIsCompatible())
      return;
    
    showTUNames = ShowTUNames;
    //Remove markers defined in marker manager from map
    if (markerManager)
     markerManager.clearMarkers();
    //Create marker manager
    markerManager = new MarkerManager(map, {trackMarkers: true});
    markers = null;
    invGPSMarkerIndex = -1;
    //Parse xml with marker data
    var xmlDoc = GXml.parse(markersXmlStr);
    //Extract array of marker elements from XML
    var markerElements = xmlDoc.documentElement.getElementsByTagName("marker");
    //Declare array of markers representing diagnostic records
    markers = new Array(markerElements.length);
    //Declare bounds object
    var mapBounds;
     
    //Create marker for each item in arrays
    for (var i = 0; i < markerElements.length; i++) 
    {
      //Extract attributes from marker element
      var lat = parseFloat(markerElements[i].getAttribute("lat"));
      var lng = parseFloat(markerElements[i].getAttribute("lng"));
      var title = markerElements[i].getAttribute("title");
      //Create position for marker
      var posn = new GLatLng(lat, lng);
      //Create marker
      var marker;
      //Check for invalid GPS data on marker
      if (lat == 0 && lng == 0) 
      { //invalid GPS data
        invGPSMarkerIndex = i;
        marker = new GMarker(map.getCenter(), { icon: invGPSMarkerIcon, title: title });      
      }
      else
      { //Valid gps data
        marker = new GMarker(posn, { title: title });
        //Add point to map bounds
        if (mapBounds)
          mapBounds.extend(posn);
        else 
          mapBounds = new GLatLngBounds(posn, posn);
      }
      //Store marker element in marker object
      marker.markerElement = markerElements[i];
      //Add marker into the array
      markers[i] = marker;
      //Add marker into the marker manager
      markerManager.addMarker(marker, 0);    
    }
    //Display markers on map
    markerManager.refresh();
    //Center map between the markers
    if (mapBounds)
    {
      var zoomLevel = map.getBoundsZoomLevel(mapBounds)-1;
      if (zoomLevel > 10)
        zoomLevel = 10;
      map.setCenter(mapBounds.getCenter(), zoomLevel);
    }
  }

  //Correct coordinates of marker for records with invalid GPS data
  //Marker is displayed in bottom-centre part of the map
  function placeInvGPSMarker() 
  {
    if (invGPSMarkerIndex >= 0 && markers != null && markers.length > invGPSMarkerIndex)
    { //Marker is defined - correct coordinates
      var mapSW   = map.getBounds().getSouthWest();
      var mapSpan = map.getBounds().toSpan();
      //Compute position 
      var markerPoint = new GLatLng(mapSW.lat()+0.01*mapSpan.lat(), mapSW.lng()+0.5*mapSpan.lng());
      //Place marker
      markers[invGPSMarkerIndex].setPoint(markerPoint);
    }
  }
  
  //Handles the resizing of map
  //Called by html jscript event
  function mapResize()
  {
    //Let google map adpat to resized object
    if (map != null)
      map.checkResize();
    //place marker for invisible GPS data
    placeInvGPSMarker();
  }
  
  //Displays info window over diagnostic record marker
  //Window contains description of events...
  function showMarkerInfoWindow(marker)
  {
    //Check if marker is defined and contains xml element with record infos
    if (marker == null || (typeof marker.markerElement == "undefined") || (marker.markerElement == null)) 
      return;
     
    //Check if marke is invalid gps marker
    var invGPS = false;
    if (invGPSMarkerIndex >= 0 && markers[invGPSMarkerIndex] == marker)
      invGPS = true;
      
    //Create text representing GPS coordinates
    var gpsText = "";
    gpsText += "<div style='font-size:small'><b>";
    if (invGPS)
      gpsText += invGPSText; //marker with invalid GPS data
    else
      gpsText += marker.getPoint().toString(); //valid GPS data
    gpsText += "</b></div>"
        
    //Obtain list of all recInfo elements defined for marker
    var recInfos = marker.markerElement.getElementsByTagName("recinfo");
    var currText = "";
    if (recInfos.length > maxRecsPerTab)
      currText += "<div style='height: 210px; overflow: scroll'>"
    currText += "<table style='font-size:x-small' cellpadding='2' cellspacing='0'>"; //Current text for diagnostic records
    //Iterate over all defined recInfos
    for (var j = 0; j < recInfos.length; j++)
    {
      //obtain index of current record
      var currRecIndx = parseInt(recInfos[j].getAttribute("recindx"));
      //Add text for current record
      if (currRecIndx == selRecIndx)
        currText += "<tr style='background-color:#0055FF'>"; //Record is currently selected
      else
        currText += "<tr>";
      currText += "<td><b>" + recInfos[j].getAttribute("starttime") + "</b></td>";
      if (showTUNames)
        currText += "<td>" + recInfos[j].getAttribute("tuname") + "</td>";
      currText += "<td>";
      if (currRecIndx != selRecIndx)
        currText += "<a href=\"javascript:recordSelected(" + currRecIndx.toString() + ")\">";
      currText += recInfos[j].getAttribute("title");
      if (currRecIndx != selRecIndx)
        currText += "</a>";
      currText += "</td>";
      currText += "</tr>";
    }
    currText += "</table>";
    if (recInfos.length > maxRecsPerTab)
      currText += "</div>"
    //Open info window over the marker
    marker.openInfoWindowHtml(gpsText + currText);
  }
  
  //Opens info window for given marker and highlights given record
  function selectRecord(markerIndex, recordIndex)
  {
    if ((markers == null) || (markerIndex >= markers.length) )
      return; //Invalid marker index or record already selected
    //Store index of selected record
    selRecIndx = recordIndex;
    //Show info window for given marker
    showMarkerInfoWindow(markers[markerIndex]);
  }
  
  //Called when user clicks on a record in info window
  function recordSelected(recordIndex)
  {
    //Call method in hosting component
    window.external.SelectRecord(recordIndex);
  }
  
  function panUp()
  {
    if (map != null)
      map.panDirection(0, -1);
  }
  
  function panDown()
  {
    if (map != null)
      map.panDirection(0, 1);
  }  
  
  function panLeft()
  {
    if (map != null)
      map.panDirection(-1, 0);
  }    
  
  function panRight()
  {
    if (map != null)
      map.panDirection(1, 0);
  }      
  
  function zoomIn()
  {
    if (map != null)
      map.zoomIn();
  }      
  
  function zoomOut()
  {
    if (map != null)
      map.zoomOut();
  }      
  