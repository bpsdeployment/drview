using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DDDObjects;
using DSDiagnosticRecords;
using DRSelectorComponents;

namespace DRViewComponents
{
  public partial class DRViewGrouped : DRViewComponents.DRViewBase
  {
    #region Construction initialization
    public DRViewGrouped()
    {
      InitializeComponent();
    }
    #endregion //Construction initialization

    #region Virtual methods overrides
    protected override void OnNewDataReady(DRSelectorComponents.DRSelectorBase sender, DRSelectorComponents.NDREventArgs args)
    {
      try
      {
        //Call base implementation
        base.OnNewDataReady(sender, args);
        //Check if records shall be diasplayed
        if (!displayData)
          return;
        OnInfoMessage("Loading records into grouped view");
        treeListView.BeginUpdate();
        //Clear TreeListView
        treeListView.Items.Clear();
        treeListView.Items.Sortable = false;
        //Create nodes
        CreateRecNodes();
        //Add nodes into TreeListView
        treeListView.EndUpdate();
        OnInfoMessage("Records loaded into grouped view");
      }
      catch (Exception ex)
      {
        OnErrorMessage(new Exception("Failed to display records in grouped view", ex));
      }
    }
    #endregion //Virtual methods overrides

    #region Helper methods
    private void CreateRecNodes()
    {
      GroupTreeNode node;
      int recCount;
      int faultCode;
      int totalRecs;
      int processed = 0;
      DDDRecord dddRecord;
      RecInstInfo recInfo;
      //Create sorted view of records
      DataView sortedView = new DataView(selector.RecordsView.Table, selector.RecordsView.RowFilter, "StartTime", selector.RecordsView.RowStateFilter);
      totalRecs = sortedView.Count;
      //Iterate over all records in view
      foreach (DataRowView rowView in sortedView)
      {
        DiagnosticRecords.RecordsRow recRow = (DiagnosticRecords.RecordsRow)rowView.Row;
        //Obtain count of records for record type
        recCount = selector.GetRecTypeCount(recRow.TUInstanceID, recRow.DDDVersionID, recRow.RecordDefID);
        //Obtain fault code
        if (!recRow.IsFaultCodeNull())
          faultCode = recRow.FaultCode;
        else
          faultCode = -1;
        //Obtain record object
        dddRecord = dddHelper.GetRecord(recRow.DDDVersionID, recRow.RecordDefID);
        //Construct record instance infor
        recInfo = new RecInstInfo(recRow.TUInstanceID, recRow.RecordInstID, dddRecord);
        //Construct node object
        node = new GroupTreeNode(
          recCount, dddRecord.Title, recRow.StartTime, 
          (!recRow.IsEndTimeNull()) ? recRow.EndTime : DateTime.MinValue,
          faultCode, recRow.Source, dddHelper.TimeFormat, recInfo);
        //Add node to view
        treeListView.Items.Add(node);
        //Report progress
        processed++;
        if ((processed % 50) == 0)
          OnReportProgress((int)(((float)processed / (float)totalRecs) * 100), true);
      }
      //Hide progress bar
      OnReportProgress(0, false);
    }

    /// <summary>
    /// Resizes widths of all columns according to current control width
    /// </summary>
    private void ResizeColumns()
    {
      colStartTime.Width = Math.Min(treeListView.Width / 6, 140);
      colEndTime.Width = Math.Min(treeListView.Width / 6, 140);
      colFaultCode.Width = Math.Min(treeListView.Width / 6, 70);
      colRecCount.Width = Math.Min(treeListView.Width / 6, 70);
      colSource.Width = Math.Min(treeListView.Width / 6, 100); 
      colRecTitle.Width = treeListView.Width - 25 - colSource.Width - colRecCount.Width - colFaultCode.Width - colEndTime.Width - colStartTime.Width;
    }

    /// <summary>
    /// Fills enviromental data node
    /// with child nodes for all inputs and their elementary variables.
    /// Called before node expansion.
    /// </summary>
    /// <param name="envDataNode">Enviromental data node to fill</param>
    private void FillEnvDataNode(GroupTreeNode envDataNode)
    {
      DDDInput dddInput;
      Dictionary<int, VarValue> varValues;
      VarValue value;
      //First obtain RecinstInfo structure
      if ((envDataNode.Tag == null) || (envDataNode.Tag.GetType() != typeof(RecInstInfo)))
        return; //Node does not contain record instance information
      RecInstInfo instInfo = envDataNode.Tag as RecInstInfo;

      //Create dictionary for var values
      varValues = CreateValDictionary(instInfo);
      //Create node for each elementary variable of each simple input
      foreach (KeyValuePair<int, DDDInput> inpPair in instInfo.dddRecord.Inputs)
      { //iterate over all inputs of record
        dddInput = inpPair.Value;
        //Determine input type, work only with simple inputs
        if (dddInput.GetType() != typeof(DDDSimpleInput))
          continue; //Not simple input - skip
        else
        { //Simple input
          foreach (DDDElemVar elemVar in dddInput.Variable.GetElementaryVars())
          { //Iterate over all elementary variables
            //Obtain value for variable
            if (!varValues.TryGetValue(elemVar.VariableID, out value))
              continue; //No value for variable found
            //Add node with value of elementary variable
            envDataNode.Items.Add(CreateElemVarNode(elemVar, value)); 
          }
        }
      }
      return;
    }

    /// <summary>
    /// Creates dictionary of var values for simple inputs of specified record instance
    /// </summary>
    /// <param name="recInfo">Specification of record instance</param>
    /// <returns>Dictionary of var values, keyed by VariableID</returns>
    private Dictionary<int, VarValue> CreateValDictionary(RecInstInfo recInfo)
    {
      //First obtain list of values for specified record instance
      List<VarValue> values = selector.GetVarValuesForRecord(recInfo.TUInstanceID, recInfo.RecordInstID, recInfo.dddRecord.RecordDefID);
      //Create output dictionary
      Dictionary<int, VarValue> dictionary = new Dictionary<int, VarValue>();
      //Iterate over all obtained values
      foreach (VarValue val in values)
        if (val.ValueIndex == 0 && !dictionary.ContainsKey(val.VariableID))
          dictionary.Add(val.VariableID, val); //Add to dictionary only values of simple inputs, that are not already there
      //Return created dictionary
      return dictionary;
    }

    /// <summary>
    /// Creates node with value of elementary variable
    /// </summary>
    /// <param name="elemVar">Helper object for elementary variable</param>
    /// <param name="varValue">Value of variable</param>
    /// <returns>Node with value</returns>
    private GroupTreeNode CreateElemVarNode(DDDElemVar elemVar, VarValue varValue)
    {
      String strValue;
      String unit = "";
      if (!elemVar.IsBitSet())
        strValue = elemVar.FormatValue(varValue.Value);
      else
        strValue = ((DDDBitsetRepres)(elemVar.Representation)).FormatSIBASString(varValue.Value);
      if (elemVar.Representation.GetType() == typeof(DDDSimpleRepres))
        unit = ((DDDSimpleRepres)(elemVar.Representation)).UnitSymbol;
      if (unit != "")
        unit = "[" + unit + "]";
      return new GroupTreeNode(elemVar.Title, strValue, unit);
    }
    #endregion //Helper methods

    #region Event handlers
    private void treeListView_SizeChanged(object sender, EventArgs e)
    {
      ResizeColumns();
    }

    private void treeListView_BeforeCollapse(object sender, TreeListViewCancelEventArgs e)
    {
      try
      {
        GroupTreeNode node = e.Item as GroupTreeNode;
        if (node.Tag != null)
        { //clear children, add dummy
          node.Items.Clear();
          node.AddDummyNode();
        }
      }
      catch (Exception)
      { }
    }

    private void treeListView_BeforeExpand(object sender, TreeListViewCancelEventArgs e)
    {
      try
      {
        GroupTreeNode node = e.Item as GroupTreeNode;
        if (node.Tag != null)
        { //Remove dummy node, fill nodes with var values
          node.Items.Clear();
          FillEnvDataNode(node);
        }
      }
      catch (Exception ex)
      {
        OnErrorMessage(new Exception("Failed to display enviromental data", ex));
      }
    }
    #endregion //Event handlers
  }

  #region GroupTreeNode
  internal class GroupTreeNode : TreeListViewItem
  {
    /// <summary>
    /// Constructor which creates new node for record
    /// </summary>
    public GroupTreeNode(int recCount, String recTitle, DateTime startTime, DateTime endTime, int faultCode, String source, String timeFormat, RecInstInfo recordInstanceInfo)
    {
      this.ImageIndex = 0;
      this.Text = recTitle; 
      //Create list view item
      if (recCount != -1)
        this.SubItems.Add(recCount.ToString());
      else
        this.SubItems.Add("");
      this.SubItems.Add(startTime.ToString(timeFormat));
      if (endTime != DateTime.MinValue)
        this.SubItems.Add(endTime.ToString(timeFormat));
      else
        this.SubItems.Add("");
      if (faultCode != -1)
        this.SubItems.Add(faultCode.ToString());
      else
        this.SubItems.Add("");
      this.SubItems.Add(source);
      //Stroe record instance info
      this.Tag = recordInstanceInfo;
      this.Items.Sortable = false;
      //Add one dummy node
      AddDummyNode();
    }

    /// <summary>
    /// Creates new node for enviromental data, with one dummy node as a child
    /// </summary>
    public GroupTreeNode(String text, RecInstInfo recordInstanceInfo)
    {
      this.ImageIndex = 1;
      this.Text = text;
      this.Tag = recordInstanceInfo;
      this.Items.Sortable = false;
      //Add one dummy node
      AddDummyNode();
    }

    /// <summary>
    /// Creates new node for structured variable
    /// </summary>
    public GroupTreeNode(String title)
    {
      this.ImageIndex = 1;
      this.Text = title;
      this.Items.Sortable = false;
    }

    /// <summary>
    /// Creates new node for value of elementary variable
    /// </summary>
    public GroupTreeNode(String title, String value, String unit)
    {
      this.ImageIndex = 1;
      this.Text = title;
      this.Items.Sortable = false;
      //Create subitems
      this.SubItems.Add(value);
      if (unit != "")
        this.SubItems.Add(unit);
    }

    /// <summary>
    /// Adds dummy node under current node (just to display + sign).
    /// Removes all other child nodes.
    /// </summary>
    public void AddDummyNode()
    {
      this.Items.Clear();
      this.Items.Add("");
    }
  }
  #endregion //GroupTreeNode
}
