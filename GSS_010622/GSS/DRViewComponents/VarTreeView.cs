using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DSDiagnosticRecords;
using DDDObjects;
using DRSelectorComponents;

namespace DRViewComponents
{
  #region Delegate definitions
  /// <summary>
  /// Defines handler for VarTreeVarChecked event
  /// </summary>
  /// <param name="sender">Sender of the event</param>
  /// <param name="args">Class with info about the variable node</param>
  public delegate void VariableCheckEventhandler(VarTreeView sender, VarTreeVarCheckEventArgs args);

  /// <summary>
  /// Occurs before any VariableChecked event is raised
  /// </summary>
  /// <param name="sender">Sender of the event</param>
  public delegate void BeforeVariablesCheckedEventhandler(VarTreeView sender);
  /// <summary>
  /// Occurs after all VariableChecked events are raised
  /// </summary>
  /// <param name="sender">Sender of the event</param>
  public delegate void AfterVariablesCheckedEventhandler(VarTreeView sender);
  #endregion //Delegate definitions

  /// <summary>
  /// Displays variables in tree with check boxes
  /// </summary>
  public partial class VarTreeView : UserControl
  {
    #region Private members
    Dictionary<Guid, VarTreeNodes.TUInstNode> instanceNodes;
    DDDHelper dddHelper;
    bool showBits = true;
    bool useFilter = true;
    #endregion //Private members

    #region Public members
    /// <summary>
    /// Event occurs after elementary variable (or bit) node is checked or unchecked
    /// </summary>
    public event VariableCheckEventhandler VariableChecked;
    /// <summary>
    /// Occurs before any VariableChecked event is raised
    /// </summary>
    public event BeforeVariablesCheckedEventhandler BeforeVariablesChecked;
    /// <summary>
    /// Occurs after all VariableChecked events are raised
    /// </summary>
    public event AfterVariablesCheckedEventhandler AfterVariablesChecked;

    /// <summary>
    /// Helper object
    /// </summary>
    public DDDHelper DDDHelper
    {
      get { return dddHelper; }
      set { dddHelper = value; }
    }

    public bool ShowBits
    {
      get { return showBits; }
      set { showBits = value; }
    }

    public bool UseFilter
    {
      get { return useFilter; }
      set { useFilter = value; }
    }
    #endregion //Public members

    #region Construction initialization
    public VarTreeView()
    {
      InitializeComponent();
      instanceNodes = new Dictionary<Guid, VarTreeNodes.TUInstNode>();
    }
    #endregion //Construction initialization

    #region Public methods
    /// <summary>
    /// Iterates over all selected records and creates tree of available variables
    /// </summary>
    public void CreateVarTree(DataView recTableView, VarIDDictionary varFilter)
    {
      try
      {
        //Check if variable filter shall be used
        if (!UseFilter)
          varFilter = null;
        //Clear TreeView
        treeView.Nodes.Clear();
        instanceNodes.Clear();
        //Iterate over all selected records
        foreach (DataRowView rowView in recTableView)
        {
          DiagnosticRecords.RecordsRow recRow = (DiagnosticRecords.RecordsRow)rowView.Row;
          VarTreeNodes.TUInstNode instanceNode;
          //Obtain instance node
          if (!instanceNodes.TryGetValue(recRow.TUInstanceID, out instanceNode))
          { //Create new instance node
            instanceNode = new VarTreeNodes.TUInstNode(recRow.TUInstanceID, dddHelper);
            instanceNodes.Add(recRow.TUInstanceID, instanceNode);
          }
          //Call add method to add all variables of the record
          instanceNode.AddRecord(recRow.DDDVersionID, recRow.RecordDefID, dddHelper, showBits, varFilter);
        }
        //Add all TUInstance nodes to tree view
        foreach (VarTreeNodes.TUInstNode instNode in instanceNodes.Values)
          treeView.Nodes.Add(instNode);
      }
      catch (Exception ex)
      {
        throw new DRViewException(ex, "Failed to create tree of variables");
      }
    }

    /// <summary>
    /// Returns list of nodes representing selected elementary variables or bits of bitstring
    /// </summary>
    /// <returns>List of nodes representing selected variables</returns>
    public List<VarTreeNodes.DDDVariableNode> GetSelectedVars()
    {
      List<VarTreeNodes.DDDVariableNode> selNodes = new List<DRViewComponents.VarTreeNodes.DDDVariableNode>();
      foreach (TreeNode node in treeView.Nodes)
        selNodes.AddRange(GetSelectedVars(node));
      return selNodes;
    }

    /// <summary>
    /// Returns dictionary of IDs of all selected variables
    /// </summary>
    /// <returns>Dictionary of selected variables</returns>
    public VarIDDictionary GetSelectedVarIDs()
    {
      List<VarTreeNodes.DDDVariableNode> selNodes = GetSelectedVars();
      VarIDDictionary dictionary = new VarIDDictionary();
      foreach (VarTreeNodes.DDDVariableNode varNode in selNodes)
      {
        if (varNode.GetType() == typeof(VarTreeNodes.DDDElemVarNode))
        {
          VarTreeNodes.DDDElemVarNode elemVarNode = (VarTreeNodes.DDDElemVarNode)varNode;
          if (elemVarNode.DDDElemVarObject.Representation.GetType() == typeof(DDDBitsetRepres))
            dictionary.AddBitsetBitIDs(varNode.TUInstObject, varNode.DDDVerObject, elemVarNode.DDDElemVarObject);
          else
            dictionary.AddVarID(varNode.TUInstObject, varNode.DDDVerObject, varNode.DDDVarObject);
        }
        else if (varNode.GetType() == typeof(VarTreeNodes.DDDBitVarNode))
          dictionary.AddBitID(varNode.TUInstObject, varNode.DDDVerObject, varNode.DDDVarObject,
                              ((VarTreeNodes.DDDBitVarNode)varNode).mask);
      }
      return dictionary;
    }

    /// <summary>
    /// Expands all nodes in tree
    /// </summary>
    public void ExpandAll()
    {
      treeView.ExpandAll();
    }

    /// <summary>
    /// Collaps all nodes in tree view
    /// </summary>
    public void CollapseAll()
    {
      treeView.CollapseAll();
    }

    /// <summary>
    /// Uncheck all nodes in tree
    /// </summary>
    public void UncheckAll()
    {
      //Handle child nodes
      foreach (TreeNode childNode in treeView.Nodes)
        HandleNodeCheck(childNode, false, false, true);
    }

    /// <summary>
    /// Checks all nodes in the tree
    /// </summary>
    public void CheckAll()
    {
      //Handle child nodes
      foreach (TreeNode childNode in treeView.Nodes)
        HandleNodeCheck(childNode, true, false, true);
    }

    /// <summary>
    /// Expand partially this node (down to first variable level)
    /// </summary>
    public void PartExpand()
    {
      foreach (VarTreeNodes.TUInstNode node in treeView.Nodes)
        node.PartExpand();
    }

    /// <summary>
    /// Check nodes with selected variables
    /// </summary>
    /// <param name="selVariables">selected variables</param>
    public void CheckSelectedVars(VarIDDictionary selVariables)
    {
      CheckSelectedVars(selVariables.GetVarIDStrings());      
    }

    /// <summary>
    /// Check nodes with selected variables
    /// </summary>
    /// <param name="selVariables">selected variables</param>
    public void CheckSelectedVars(List<String> selVariables)
    {
      foreach (VarTreeNodes.TUInstNode node in treeView.Nodes)
        node.CheckSelectedVars(selVariables);
    }
    #endregion //Public methods

    #region Helper methods
    /// <summary>
    /// Method raises VariableChecked event
    /// </summary>
    protected void OnVariableChecked(TUInstance TUInstance, DDDVersion DDDVersion, DDDElemVar Variable, Int64 Mask, bool Checked)
    {
      if (VariableChecked != null)
      {
        //Create event arguments
        VarTreeVarCheckEventArgs args = new VarTreeVarCheckEventArgs();
        args.DDDVersion = DDDVersion;
        args.Checked = Checked;
        args.Mask = Mask;
        args.TUInstance = TUInstance;
        args.Variable = Variable;
        //Raise event
        VariableChecked(this, args);
      }
    }

    /// <summary>
    /// Method raises BeforeVariableChecked event
    /// </summary>
    protected void OnBeforeVarChecked()
    {
      if (BeforeVariablesChecked != null)
        BeforeVariablesChecked(this);
    }

    /// <summary>
    /// Method raises AfterVariableCheked event
    /// </summary>
    protected void OnAfterVarChecked()
    {
      if (AfterVariablesChecked != null)
        AfterVariablesChecked(this);
    }

    private void HandleNodeCheck(TreeNode node, bool checkState, bool parentHandled, bool noEvents)
    {
      node.Checked = checkState;

      //Check for elementary variable
      if ((node.GetType() == typeof(VarTreeNodes.DDDElemVarNode)) && !noEvents)
      {//Raise Variable checked event
        VarTreeNodes.DDDElemVarNode elemNode = node as VarTreeNodes.DDDElemVarNode;
        OnVariableChecked(elemNode.TUInstObject, elemNode.DDDVerObject, elemNode.DDDElemVarObject, 0, checkState);
      }
      else if ((node.GetType() == typeof(VarTreeNodes.DDDBitVarNode)) && !parentHandled && !noEvents)
      { //Raise Variable checked event
        VarTreeNodes.DDDBitVarNode bitNode = node as VarTreeNodes.DDDBitVarNode;
        OnVariableChecked(bitNode.TUInstObject, bitNode.DDDVerObject, bitNode.DDDElemVarObject, bitNode.mask, checkState);
      }

      //Handle child nodes
      foreach (TreeNode childNode in node.Nodes)
      {
        if ((childNode.Checked != checkState) || noEvents)
          HandleNodeCheck(childNode, checkState, true, noEvents);
      }
    }

    /// <summary>
    /// Returns list of selected child nodes for given node representing elementary variables or bits of bitstring
    /// </summary>
    private List<VarTreeNodes.DDDVariableNode> GetSelectedVars(TreeNode node)
    {
      List<VarTreeNodes.DDDVariableNode> selNodes = new List<DRViewComponents.VarTreeNodes.DDDVariableNode>();
      foreach (TreeNode childNode in node.Nodes)
      {
        if (childNode.Checked && 
          (childNode.GetNodeCount(false) == 0) && 
          childNode.GetType().IsSubclassOf(typeof(VarTreeNodes.DDDVariableNode)))
          selNodes.Add((VarTreeNodes.DDDVariableNode)childNode);
        else if (childNode.GetNodeCount(false) > 0)
          selNodes.AddRange(GetSelectedVars(childNode));
      }
      return selNodes;
    }
    #endregion //Helper methods

    #region Event handlers
    private void treeView_AfterCheck(object sender, TreeViewEventArgs e)
    {
      if (e.Action != TreeViewAction.Unknown)
      {
        //Set cursor to wait
        this.ParentForm.Cursor = Cursors.WaitCursor;
        //Raise before var checked event
        OnBeforeVarChecked();
        //Handle check
        HandleNodeCheck(e.Node, e.Node.Checked, false, false);
        //Raise after var checked event
        OnAfterVarChecked();
        //Set cursor back to normal
        this.ParentForm.Cursor = Cursors.Default;
      }
    }
    #endregion //Event handlers

  }

  #region Helper classes
  namespace VarTreeNodes
  {
    /// <summary>
    /// Node for tree view representing one TUInstance
    /// </summary>
    public class TUInstNode : TreeNode
    {
      public TUInstance TUInstObject;
      protected Dictionary<Guid, DDDVersionNode> versionNodes; //available DDDVersion nodes
      /// <summary>
      /// Default constructor
      /// </summary>
      public TUInstNode(Guid TUInstanceID, DDDHelper dddHelper)
      {
        //Store TUInstance object
        TUInstObject = dddHelper.GetTUInstance(TUInstanceID);
        //Store text for node
        this.Text = TUInstObject.TUName;
        //Create dictionary for DDDVersions
        versionNodes = new Dictionary<Guid, DDDVersionNode>();
        //Expand the node
        this.Expand();
      }

      /// <summary>
      /// Adds variables from one record to the tree.
      /// </summary>
      /// <param name="DDDVersionID">Identifier of DDDVersion</param>
      /// <param name="recordDefID">Identifier of record</param>
      /// <param name="dddHelper">helper object</param>
      /// <param name="showBits">show bit nodes for bitstring</param>
      /// <param name="varFilter">use filter for variables</param>
      public void AddRecord(Guid DDDVersionID, int recordDefID, DDDHelper dddHelper, bool showBits, VarIDDictionary varFilter)
      {
        DDDVersionNode versionNode;
        //Obtain version node, or create new one
        if (!versionNodes.TryGetValue(DDDVersionID, out versionNode))
        { //Create new DDDVersion node
          versionNode = new DDDVersionNode(DDDVersionID, dddHelper, TUInstObject);
          versionNodes.Add(DDDVersionID, versionNode);
          this.Nodes.Add(versionNode);
        }
        //Add record to DDDVersion
        versionNode.AddRecord(recordDefID, dddHelper, showBits, varFilter);
      }

      /// <summary>
      /// Expand partially this node (down to first variable level)
      /// </summary>
      public void PartExpand()
      {
        this.Expand();
        foreach (TreeNode node in Nodes)
          node.Expand();
      }

      /// <summary>
      /// Check nodes with selected variables
      /// </summary>
      /// <param name="selVariables">selected variables</param>
      public void CheckSelectedVars(List<String> selVariables)
      {
        foreach (DDDVersionNode node in Nodes)
        {
          node.CheckSelectedVars(selVariables);
        }
      }
    }

    /// <summary>
    /// Node for tree view representing one DDDVersion
    /// </summary>
    public class DDDVersionNode : TreeNode
    {
      public TUInstance TUInstObject;
      public DDDVersion DDDVerObject;
      protected Dictionary<int, DDDRecord> records; //Available records
      protected Dictionary<int, DDDVariableNode> varNodes; //Available variables
      /// <summary>
      /// Default constructor
      /// </summary>
      public DDDVersionNode(Guid DDDVersionID, DDDHelper dddHelper, TUInstance tuInstance)
      {
        //Store DDDVersion object
        DDDVerObject = dddHelper.GetVersion(DDDVersionID);
        //Store TUInstance
        this.TUInstObject = tuInstance;
        //Store text for node
        this.Text = DDDVerObject.UserVersion;
        //Create dictionaries
        records = new Dictionary<int, DDDRecord>();
        varNodes = new Dictionary<int, DDDVariableNode>();
        //Expand the node
        this.Expand();
      }

      /// <summary>
      /// Adds variables from one record to the tree.
      /// </summary>
      /// <param name="recordDefID">Identifier of record</param>
      /// <param name="dddHelper">helper object</param>
      /// <param name="showBits">show bit nodes for bitstring</param>
      /// <param name="varFilter">filter for variables</param>
      public void AddRecord(int recordDefID, DDDHelper dddHelper, bool showBits, VarIDDictionary varFilter)
      {
        //Check if record was previously added
        if (records.ContainsKey(recordDefID))
          return; //Record was previously added

        //Obtain and store record object
        DDDRecord recObj = DDDVerObject.GetRecord(recordDefID);
        records.Add(recordDefID, recObj);
        //Add all variables from record inputs
        foreach (DDDInput inpObject in recObj.Inputs.Values)
        {
          //Check if variable exists in the tree
          if (varNodes.ContainsKey(inpObject.VariableID))
            continue; //variable already exists in the tree
          //Add variable
          DDDVariableNode varNode;
          if (inpObject.Variable.GetType() != typeof(DDDElemVar))
            varNode = new DDDVariableNode(inpObject.Variable, DDDVerObject, TUInstObject, showBits, varFilter);
          else
          { //elementary variable node
            if (varFilter != null && !varFilter.ContainsVarID(TUInstObject.TUInstanceID, DDDVerObject.VersionID, inpObject.Variable.VariableID))
              continue; //variable is filtered out
            varNode = new DDDElemVarNode(inpObject.Variable, DDDVerObject, TUInstObject, showBits, varFilter);
          }
          varNodes.Add(inpObject.VariableID, varNode);
          this.Nodes.Add(varNode);
        }
      }

      /// <summary>
      /// Check nodes with selected variables
      /// </summary>
      /// <param name="selVariables">selected variables</param>
      public void CheckSelectedVars(List<String> selVariables)
      {
        foreach (DDDVariableNode node in Nodes)
        {
          node.CheckSelectedVars(selVariables);
        }
      }
    }

    /// <summary>
    /// Node for tree view representing one Variable
    /// </summary>
    public class DDDVariableNode : TreeNode
    {
      public TUInstance TUInstObject;
      public DDDVariable DDDVarObject;
      public DDDVersion DDDVerObject;

      /// <summary>
      /// Default constructor
      /// </summary>
      public DDDVariableNode(DDDVariable varObject, DDDVersion versionObject, TUInstance tuInstance, bool showBits, VarIDDictionary varFilter)
      {
        //Store variable object
        DDDVarObject = varObject;
        DDDVerObject = versionObject;
        TUInstObject = tuInstance;
        //Set text for node
        this.Text = DDDVarObject.Title;
        this.ImageIndex = 2;
        //Create nodes for child variables if this is structure
        if (DDDVarObject.GetType() == typeof(DDDStructure))
        {
          DDDStructure structVar = (DDDStructure)varObject;
          foreach (DDDVariable childVar in structVar.ChildVars)
          {
            if (childVar.GetType() != typeof(DDDElemVar))
              this.Nodes.Add(new DDDVariableNode(childVar, versionObject, tuInstance, showBits, varFilter));
            else if ((varFilter == null) || (varFilter.ContainsVarID(tuInstance.TUInstanceID, versionObject.VersionID, childVar.VariableID)))
              this.Nodes.Add(new DDDElemVarNode(childVar, versionObject, tuInstance, showBits, varFilter));
          }
        }
      }

      /// <summary>
      /// Check nodes with selected variables
      /// </summary>
      /// <param name="selVariables">selected variables</param>
      public virtual void CheckSelectedVars(List<String> selVariables)
      {
        foreach (DDDVariableNode node in Nodes)
        {
          node.CheckSelectedVars(selVariables);
        }
      }
    }

    public class DDDElemVarNode : DDDVariableNode
    {
      public bool IsBitset;
      public DDDElemVar DDDElemVarObject;

      /// <summary>
      /// Default constructor
      /// </summary>
      public DDDElemVarNode(DDDVariable varObject, DDDVersion versionObject, TUInstance tuInstance, bool showBits, VarIDDictionary varFilter)
        : base(varObject, versionObject, tuInstance, showBits, varFilter)
      {
        DDDElemVarObject = DDDVarObject as DDDElemVar;
        IsBitset = DDDElemVarObject.IsBitSet();
        this.ImageIndex = 3;
        if (IsBitset && showBits)
        { //Add all bits
          DDDBitsetRepres repres = DDDElemVarObject.Representation as DDDBitsetRepres;
          ulong[] masks = repres.GetUsedBitMasks();
          foreach (long mask in masks)
          {
            if ((varFilter == null) || (varFilter.ContainsBit(tuInstance.TUInstanceID, versionObject.VersionID, varObject.VariableID, mask)))
              this.Nodes.Add(new DDDBitVarNode(varObject, versionObject, tuInstance, mask, repres.GetBitTitle((ulong)mask)));
          }
        }
      }

      public override void CheckSelectedVars(List<String> selVariables)
      {
        if (selVariables.Contains(DDDVariable.GetVarIDString(TUInstObject.TUInstanceID, DDDVerObject.VersionID, DDDVarObject.VariableID)))
          Checked = true;
        base.CheckSelectedVars(selVariables);
      }
    }

    public class DDDBitVarNode : DDDVariableNode
    {
      public Int64 mask;
      public DDDElemVar DDDElemVarObject;

      /// <summary>
      /// Default constructor
      /// </summary>
      public DDDBitVarNode(DDDVariable varObject, DDDVersion versionObject, TUInstance tuInstance, Int64 mask, String text)
        : base(varObject, versionObject, tuInstance, false, null)
      {
        DDDElemVarObject = DDDVarObject as DDDElemVar;
        this.mask = mask;
        this.Text = text;
        this.ImageIndex = 4;
      }

      public override void CheckSelectedVars(List<String> selVariables)
      {
        if (selVariables.Contains(
              DDDBitsetRepres.GetBitIDString((ulong)mask, 
                                             DDDVariable.GetVarIDString(TUInstObject.TUInstanceID, DDDVerObject.VersionID, DDDVarObject.VariableID)))
           )
          Checked = true;
      }
    }
  }

  /// <summary>
  /// Class represents event arguments for variable check event
  /// </summary>
  public class VarTreeVarCheckEventArgs : EventArgs
  {
    public TUInstance TUInstance;
    public DDDVersion DDDVersion;
    public DDDElemVar Variable;
    public Int64 Mask;
    public bool Checked;
  }
  #endregion //Helper classes
}
