using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DRSelectorComponents;
using DSDiagnosticRecords;
using DDDObjects;

namespace DRViewComponents
{
  public partial class DRViewStatistics : DRViewComponents.DRViewBase
  {
    #region Construction initialization
    public DRViewStatistics()
    {
      InitializeComponent();
    }
    #endregion //Construction initialization

    #region Virtual methods overrides
    /// <summary>
    /// Compute statistics for selected record and display in grid
    /// </summary>
    /// <param name="sender"></param>
    protected override void OnNewDataReady(DRSelectorBase sender, NDREventArgs args)
    {
      int count = 0;
      DateTime? firstTime = null;
      DateTime? lastTime = null;
      int processed = 0;

      try
      {
        //call base implementation
        base.OnNewDataReady(sender, args);
        //Check if control shall display data
        if (!displayData)
          return;
        OnInfoMessage("Creating records statistics");
        //Set formatting for time columns
        FirstTime.DefaultCellStyle.Format = dddHelper.TimeFormat;
        LastTime.DefaultCellStyle.Format = dddHelper.TimeFormat;
        //Remove all rows
        dgvStatistics.Rows.Clear();

        //Obtain view of selected records, sorted correctly
        DataView sortedView = new DataView(selector.RecordsView.Table, selector.RecordsView.RowFilter, "TUInstanceID, HierarchyItemID, VehicleNumber, Source, RecordDefID, StartTime", selector.RecordsView.RowStateFilter);
        //Iterate over records, create groups
        for (int i = 0; i < sortedView.Count; i++)
        {
          DiagnosticRecords.RecordsRow row = (DiagnosticRecords.RecordsRow)sortedView[i].Row;
          //Increment count
          count++;
          //Correct time of first and last
          if (!row.IsStartTimeNull())
          {
            if (!firstTime.HasValue || (firstTime > row.StartTime))
              firstTime = row.StartTime;
            if (!lastTime.HasValue || (lastTime < row.StartTime))
              lastTime = row.StartTime;
          }
          //Check if this is the last row in the batch (record type)
          if (
            (i == sortedView.Count - 1) || //Last row in the view
            !CompareRows(row, (DiagnosticRecords.RecordsRow)sortedView[i + 1].Row)) //Next row is different
          { //Insert row into grid for current record type
            dgvStatistics.Rows.Add(new Object[] { 
              row.TrainName, row.VehicleName,
              (!row.IsVehicleNumberNull()) ? row.VehicleNumber.ToString() : String.Empty, 
              row.Source,
              (!row.IsFaultCodeNull()) ? row.FaultCode.ToString() : String.Empty, 
              row.RecTitle, count,
              (firstTime.HasValue) ? firstTime : null, 
              (lastTime.HasValue) ?  lastTime : null,
              (!row.IsFaultSeverityNull()) ? row.FaultSeverity.ToString() : String.Empty, 
              (!row.IsFaultSubSeverityNull()) ? row.FaultSubSeverity.ToString() : String.Empty
            });
            //Reset count, times and active flag
            count = 0;
            firstTime = null;
            lastTime = null;
          }
          //Report progress
          processed++;
          if ((processed % 50) == 0)
            OnReportProgress((int)(((float)processed / (float)sortedView.Count) * 100), true);
        }

        //Set columns widths
        RecTitle.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        dgvStatistics.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        RecTitle.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
      }
      catch (Exception ex)
      {
        OnErrorMessage(new DRViewException(ex, "Failed to display record statistics"));
      }
      OnReportProgress(0, false);
      OnInfoMessage("Record statistics displayed");
    }
    #endregion //Virtual methods overrides

    #region Helper methods
    /// <summary>
    /// Method compares two diagnostic records row for same record source and type.
    /// </summary>
    /// <param name="row1">First row to compare</param>
    /// <param name="row2">Second row to compare</param>
    /// <returns>True if rows are the same, false otherwise</returns>
    private bool CompareRows(DiagnosticRecords.RecordsRow row1, DiagnosticRecords.RecordsRow row2)
    {
      //Check for null rows
      if (row1 == null || row2 == null)
        return false;

      //Check for same source and type of record
      //TUInstanceID, HierarchyItemID, VehicleNumber, Source, RecordDefID
      if (
        (row1.TUInstanceID == row2.TUInstanceID) &&
        (
          (row1.IsHierarchyItemIDNull() == row2.IsHierarchyItemIDNull()) || 
          (!row1.IsHierarchyItemIDNull() && row1.HierarchyItemID == row2.HierarchyItemID)
        ) &&
        (
          (row1.IsVehicleNumberNull() == row2.IsVehicleNumberNull()) || 
          (!row1.IsVehicleNumberNull() && row1.VehicleNumber == row2.VehicleNumber)
        ) &&
        (row1.Source == row2.Source) &&
        (row1.RecordDefID == row2.RecordDefID)
      )
        return true; //Rows are the same

      return false; //Rows are different
    }
    #endregion //Helper methods

    #region Event handlers
    private void dgvStatistics_VisibleChanged(object sender, EventArgs e)
    {
      if (dgvStatistics.Visible)
      {
        //Set columns widths
        RecTitle.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        dgvStatistics.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        RecTitle.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
      }
    }
    #endregion //Event handlers
  }
}

