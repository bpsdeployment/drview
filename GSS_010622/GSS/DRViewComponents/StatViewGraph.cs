using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DDDObjects;
using DRStatistics;
using StatSelectorComponents;
using Gigasoft.ProEssentials.Enums;
using Gigasoft.ProEssentials;

namespace StatViewComponents
{
  public partial class StatViewGraph : StatViewBase
  {
    #region Private members
    Pego graph = null; //Standard 2D graph
    #endregion //Private members

    #region Public properties
    #endregion //Public properties

    #region Construction initialization
    public StatViewGraph()
    {
      InitializeComponent();
    }
    #endregion //Construction initialization

    #region Virtual method overrides
    /// <summary>
    /// Handles new data ready event from selector component
    /// </summary>
    /// <param name="sender">Selector component</param>
    protected override void OnNewDataReady(StatSelectorBase sender, NDREventArgs args)
    {
      try
      {
        base.OnNewDataReady(sender, args);
        //Check if control shall display data
        if (!displayData)
          return;
        OnInfoMessage("Loading statistical data into graph view");
        //Dispose old graph objects
        DisposeOldGraph();
        //Decide which graph to create - based on number of group levels in selector
        int groupLevels = selector.StatisticsHelper.GroupsDescriptors.GetGroupLevelsCount();
        if (groupLevels == 1 || groupLevels == 2 || groupLevels == 3)
          Create2DGraph();
        else
          throw new Exception(String.Format("Cannot create graph for {0} group levels", groupLevels));
        //Send info message
        OnInfoMessage("Statistical data loaded into graph view");
      }
      catch (Exception ex)
      {
        OnErrorMessage(new StatViewException(ex, "Failed to load statistical data into graph view"));
      }
      finally
      {
      }
    }

    /// <summary>
    /// Stores reference to assigned helper object.
    /// </summary>
    /// <param name="helper">Helper object</param>
    protected override void SetDDDHelper(DDDHelper helper)
    {
      base.SetDDDHelper(helper);
    }
    #endregion //Virtual method overrides

    #region Public methods
    #endregion //Public methods

    #region Helper methods
    /// <summary>
    /// Removes old graph objects - if exist
    /// </summary>
    private void DisposeOldGraph()
    {
      if (graph != null)
      {
        this.Controls.Remove(graph);
        graph.Dispose();
        graph = null;
      }
    }

    /// <summary>
    /// Creates and initiates 2D graph object - for one or two groups in selector
    /// </summary>
    private void Create2DGraph()
    {
      //Create graph object
      this.graph = new Pego();
      this.SuspendLayout();
      this.graph.Dock = System.Windows.Forms.DockStyle.Fill;
      this.graph.Location = new System.Drawing.Point(0, 0);
      this.graph.Name = "graph";
      this.graph.TabIndex = 0;
      this.graph.Text = "graph";
      this.Controls.Add(this.graph);
      this.ResumeLayout(false);
      //Set graph object properties
      graph.PeConfigure.PrepareImages = true;
      graph.PeString.MainTitle = "";
      graph.PeString.SubTitle = "";
      graph.PeString.XAxisLabel = "";
      graph.PeString.YAxisLabel = "";
      //Fill graph with data and labels
      Set2DGraphData();
      //Set additional graph properties
      graph.PePlot.DataShadows = DataShadows.Shadows;
      graph.PeUserInterface.Allow.FocalRect = false;
      graph.PePlot.Method = GraphPlottingMethod.Bar;
      graph.PeGrid.LineControl = GridLineControl.None;
      graph.PePlot.Allow.Ribbon = true;
      graph.PeUserInterface.Allow.Zooming = AllowZooming.HorzAndVert;
      graph.PeUserInterface.Allow.ZoomStyle = ZoomStyle.Ro2Not;
      graph.PeConfigure.ImageAdjustTop = 100;

      graph.PeLegend.SimplePoint = true;
      graph.PeLegend.SimpleLine = true;
      graph.PeLegend.Style = LegendStyle.TwoLine;
      graph.PeFont.Fixed = true;
      graph.PeUserInterface.Menu.TableWhat = MenuControl.Hide;
      graph.PeTable.Allow = false;
      graph.PeUserInterface.Menu.DataPrecision = MenuControl.Hide;
      graph.PePlot.Allow.Area = false;
      graph.PePlot.Allow.BestFitCurve = false;
      graph.PePlot.Allow.BestFitCurveGraphed = false;
      graph.PePlot.Allow.BestFitLine = false;
      graph.PePlot.Allow.BestFitLineGraphed = false;
      graph.PePlot.Allow.Histogram = false;
      graph.PePlot.Allow.HorzBarStacked = false;
      graph.PePlot.Allow.HorziontalBar = false;
      graph.PePlot.Allow.PointsPlusSpline = false;
      graph.PePlot.Allow.Ribbon = false;
      graph.PePlot.Allow.Spline = false;
      graph.PePlot.Allow.StackedData = false;
      graph.PePlot.Allow.Step = false;

      //Recreate graph image from data
      graph.PeFunction.ReinitializeResetImage();
      this.Refresh();
    }

    /// <summary>
    /// Inserts values into 2D graph
    /// </summary>
    private void Set2DGraphData()
    { 
      GroupsDescriptors groups = selector.StatisticsHelper.GroupsDescriptors;
      GroupValue value = selector.StatisticsHelper.GroupValue;
      if (groups.GetGroupLevelsCount() == 3)
      { //3 groups - more subsets, more points
        //Counts
        graph.PeData.Subsets = groups.GetGroupCount(0);
        int point1Count = groups.GetGroupCount(1);
        int point2Count = groups.GetGroupCount(2);
        graph.PeData.Points = point1Count * point2Count;
        //Subset labels
        for (int subs = 0; subs < groups.GetGroupCount(0); subs++)
          graph.PeString.SubsetLabels[subs] = groups.GetGroupTitle(0, subs);
        //Point labels
        graph.PeString.PointLabelRows = 2; //2 line point labels
        for (int point1 = 0; point1 < point1Count; point1++)
          for (int point2 = 0; point2 < point2Count; point2++)
            graph.PeString.PointLabels[point1 * point2Count + point2] = groups.GetGroupTitle(1, point1) + "|" + groups.GetGroupTitle(2, point2);
        //Set data
        for (int subs = 0; subs < groups.GetGroupCount(0); subs++)
        {  //Subsets
          GroupValue subValues = value.GetChildGroup(groups.GetGroupIndex(0, subs));
          for (int point1 = 0; point1 < point1Count; point1++)
          { //Points - 1. level
            GroupValue point1Value = subValues.GetChildGroup(groups.GetGroupIndex(1, point1));
            for (int point2 = 0; point2 < point2Count; point2++)
            { //Points - 2. level
              GroupValue point2Value = point1Value.GetChildGroup(groups.GetGroupIndex(2, point2));
              graph.PeData.Y[subs, point1 * point2Count + point2] = point2Value.Value;
            }
          }
        }
      }
      else if (groups.GetGroupLevelsCount() == 2)
      { //2 groups - more subsets
        //Counts
        graph.PeData.Subsets = groups.GetGroupCount(0);
        graph.PeData.Points = groups.GetGroupCount(1);
        //Subset labels
        for (int subs = 0; subs < groups.GetGroupCount(0); subs++)
          graph.PeString.SubsetLabels[subs] = groups.GetGroupTitle(0, subs);
        //Point labels
        for (int point = 0; point < groups.GetGroupCount(1); point++)
          graph.PeString.PointLabels[point] = groups.GetGroupTitle(1, point);
        //Set data
        for (int subs = 0; subs < groups.GetGroupCount(0); subs++)
        {
          GroupValue subValues = value.GetChildGroup(groups.GetGroupIndex(0, subs));
          for (int point = 0; point < groups.GetGroupCount(1); point++)
          {
            GroupValue poinValue = subValues.GetChildGroup(groups.GetGroupIndex(1, point));
            graph.PeData.Y[subs, point] = poinValue.Value;
          }
        }
      }
      else
      { //1 group - 1 subset
        //Counts
        graph.PeData.Subsets = 1;
        graph.PeData.Points = groups.GetGroupCount(0);
        //Labels and data
        for (int point = 0; point < groups.GetGroupCount(0); point++)
        {
          graph.PeString.PointLabels[point] = groups.GetGroupTitle(0, point);
          GroupValue poinValue = value.GetChildGroup(groups.GetGroupIndex(0, point));
          graph.PeData.Y[0, point] = poinValue.Value;
        }
      }
    }
    #endregion //Helper methods

    #region Event handlers
    #endregion //Event handlers
  }
}

