namespace DRViewComponents
{
  partial class DRViewMaster_Slave
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
      this.dgvRecords = new System.Windows.Forms.DataGridView();
      this.hvgVariables = new DRViewComponents.HorizVariableGrid();
      this.panel1 = new System.Windows.Forms.Panel();
      this.label1 = new System.Windows.Forms.Label();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.panel2 = new System.Windows.Forms.Panel();
      this.label2 = new System.Windows.Forms.Label();
      this.colTUInstanceID = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colRecordInstID = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colDDDVersionID = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colRecordDefID = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colStartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colEndTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colLastModified = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colSource = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colHierarchyItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colImportTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colAcknowledgeTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colVehicleNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colDeviceCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colTUInstanceName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colTUTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colDDDVersionName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colRecTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colFaultCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colRecordName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colTrainName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colVehicleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colRecordType = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colFaultSeverity = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colFaultSubSeverity = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colTrcSnpCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colGPSLatitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colGPSLongitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.colRecTypeCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
      ((System.ComponentModel.ISupportInitialize)(this.dgvRecords)).BeginInit();
      this.panel1.SuspendLayout();
      this.panel2.SuspendLayout();
      this.SuspendLayout();
      // 
      // dgvRecords
      // 
      this.dgvRecords.AllowUserToAddRows = false;
      this.dgvRecords.AllowUserToOrderColumns = true;
      this.dgvRecords.AllowUserToResizeRows = false;
      this.dgvRecords.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.dgvRecords.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
      this.dgvRecords.CausesValidation = false;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
      dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgvRecords.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgvRecords.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvRecords.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colTUInstanceID,
            this.colRecordInstID,
            this.colDDDVersionID,
            this.colRecordDefID,
            this.colStartTime,
            this.colEndTime,
            this.colLastModified,
            this.colSource,
            this.colHierarchyItemID,
            this.colImportTime,
            this.colAcknowledgeTime,
            this.colVehicleNumber,
            this.colDeviceCode,
            this.colTUInstanceName,
            this.colTUTypeName,
            this.colDDDVersionName,
            this.colRecTitle,
            this.colFaultCode,
            this.colRecordName,
            this.colTrainName,
            this.colVehicleName,
            this.colRecordType,
            this.colFaultSeverity,
            this.colFaultSubSeverity,
            this.colTrcSnpCode,
            this.colGPSLatitude,
            this.colGPSLongitude,
            this.colRecTypeCount});
      this.dgvRecords.Location = new System.Drawing.Point(0, 16);
      this.dgvRecords.Name = "dgvRecords";
      this.dgvRecords.ReadOnly = true;
      this.dgvRecords.RowHeadersWidth = 20;
      this.dgvRecords.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
      this.dgvRecords.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      this.dgvRecords.RowTemplate.Height = 18;
      this.dgvRecords.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgvRecords.ShowEditingIcon = false;
      this.dgvRecords.ShowRowErrors = false;
      this.dgvRecords.Size = new System.Drawing.Size(724, 203);
      this.dgvRecords.TabIndex = 0;
      this.dgvRecords.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvRecords_ColumnHeaderMouseClick);
      this.dgvRecords.ColumnDividerDoubleClick += new System.Windows.Forms.DataGridViewColumnDividerDoubleClickEventHandler(this.dgvRecords_ColumnDividerDoubleClick);
      this.dgvRecords.SelectionChanged += new System.EventHandler(this.dgvRecords_SelectionChanged);
      // 
      // hvgVariables
      // 
      this.hvgVariables.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.hvgVariables.DDDHelper = null;
      this.hvgVariables.HeaderColor = System.Drawing.SystemColors.Control;
      this.hvgVariables.Location = new System.Drawing.Point(0, 20);
      this.hvgVariables.Name = "hvgVariables";
      this.hvgVariables.RelTimeUnit = DRViewComponents.HorizVariableGrid.eRelTimeUnit.sec_ms;
      this.hvgVariables.Size = new System.Drawing.Size(724, 233);
      this.hvgVariables.StartTime = new System.DateTime(((long)(0)));
      this.hvgVariables.TabIndex = 1;
      this.hvgVariables.VisibleColumns = ((DRViewComponents.HorizVariableGrid.eVisibleColumns)((DRViewComponents.HorizVariableGrid.eVisibleColumns.ColVariable | DRViewComponents.HorizVariableGrid.eVisibleColumns.ColUnit)));
      this.hvgVariables.VisibleRows = DRViewComponents.HorizVariableGrid.eVisibleRows.RelTime;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.label1);
      this.panel1.Controls.Add(this.hvgVariables);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel1.Location = new System.Drawing.Point(0, 222);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(724, 253);
      this.panel1.TabIndex = 2;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(1, 4);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(113, 13);
      this.label1.TabIndex = 2;
      this.label1.Text = "Enviromental variables";
      // 
      // splitter1
      // 
      this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter1.Location = new System.Drawing.Point(0, 219);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(724, 3);
      this.splitter1.TabIndex = 3;
      this.splitter1.TabStop = false;
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.label2);
      this.panel2.Controls.Add(this.dgvRecords);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel2.Location = new System.Drawing.Point(0, 0);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(724, 219);
      this.panel2.TabIndex = 4;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(3, 0);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(95, 13);
      this.label2.TabIndex = 3;
      this.label2.Text = "Diagnostic records";
      // 
      // colTUInstanceID
      // 
      this.colTUInstanceID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
      this.colTUInstanceID.HeaderText = "TU Instance ID";
      this.colTUInstanceID.Name = "colTUInstanceID";
      this.colTUInstanceID.ReadOnly = true;
      this.colTUInstanceID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colTUInstanceID.Visible = false;
      // 
      // colRecordInstID
      // 
      this.colRecordInstID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
      this.colRecordInstID.HeaderText = "Record Instance ID";
      this.colRecordInstID.Name = "colRecordInstID";
      this.colRecordInstID.ReadOnly = true;
      this.colRecordInstID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colRecordInstID.Visible = false;
      // 
      // colDDDVersionID
      // 
      this.colDDDVersionID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
      this.colDDDVersionID.HeaderText = "DDD Version ID";
      this.colDDDVersionID.Name = "colDDDVersionID";
      this.colDDDVersionID.ReadOnly = true;
      this.colDDDVersionID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colDDDVersionID.Visible = false;
      // 
      // colRecordDefID
      // 
      this.colRecordDefID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
      this.colRecordDefID.HeaderText = "Record Definition ID";
      this.colRecordDefID.Name = "colRecordDefID";
      this.colRecordDefID.ReadOnly = true;
      this.colRecordDefID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colRecordDefID.Visible = false;
      // 
      // colStartTime
      // 
      this.colStartTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
      dataGridViewCellStyle2.Format = "yyyy-MM-dd HH:mm:ss.fff";
      dataGridViewCellStyle2.NullValue = null;
      this.colStartTime.DefaultCellStyle = dataGridViewCellStyle2;
      this.colStartTime.HeaderText = "Start Time";
      this.colStartTime.MinimumWidth = 100;
      this.colStartTime.Name = "colStartTime";
      this.colStartTime.ReadOnly = true;
      this.colStartTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colStartTime.Visible = false;
      // 
      // colEndTime
      // 
      this.colEndTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
      dataGridViewCellStyle3.Format = "yyyy-MM-dd HH:mm:ss.fff";
      dataGridViewCellStyle3.NullValue = null;
      this.colEndTime.DefaultCellStyle = dataGridViewCellStyle3;
      this.colEndTime.HeaderText = "End Time";
      this.colEndTime.MinimumWidth = 100;
      this.colEndTime.Name = "colEndTime";
      this.colEndTime.ReadOnly = true;
      this.colEndTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colEndTime.Visible = false;
      // 
      // colLastModified
      // 
      this.colLastModified.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
      dataGridViewCellStyle4.Format = "yyyy-MM-dd HH:mm:ss.fff";
      dataGridViewCellStyle4.NullValue = null;
      this.colLastModified.DefaultCellStyle = dataGridViewCellStyle4;
      this.colLastModified.HeaderText = "Last Modified";
      this.colLastModified.Name = "colLastModified";
      this.colLastModified.ReadOnly = true;
      this.colLastModified.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colLastModified.Visible = false;
      // 
      // colSource
      // 
      this.colSource.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
      this.colSource.HeaderText = "Source";
      this.colSource.Name = "colSource";
      this.colSource.ReadOnly = true;
      this.colSource.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colSource.Visible = false;
      // 
      // colHierarchyItemID
      // 
      this.colHierarchyItemID.HeaderText = "Hierarchy Item ID";
      this.colHierarchyItemID.Name = "colHierarchyItemID";
      this.colHierarchyItemID.ReadOnly = true;
      this.colHierarchyItemID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colHierarchyItemID.Visible = false;
      this.colHierarchyItemID.Width = 95;
      // 
      // colImportTime
      // 
      this.colImportTime.HeaderText = "Import Time";
      this.colImportTime.Name = "colImportTime";
      this.colImportTime.ReadOnly = true;
      this.colImportTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colImportTime.Visible = false;
      this.colImportTime.Width = 68;
      // 
      // colAcknowledgeTime
      // 
      this.colAcknowledgeTime.HeaderText = "Acknowledge Time";
      this.colAcknowledgeTime.Name = "colAcknowledgeTime";
      this.colAcknowledgeTime.ReadOnly = true;
      this.colAcknowledgeTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colAcknowledgeTime.Visible = false;
      this.colAcknowledgeTime.Width = 104;
      // 
      // colVehicleNumber
      // 
      this.colVehicleNumber.HeaderText = "Vehicle Number";
      this.colVehicleNumber.Name = "colVehicleNumber";
      this.colVehicleNumber.ReadOnly = true;
      this.colVehicleNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colVehicleNumber.Visible = false;
      this.colVehicleNumber.Width = 88;
      // 
      // colDeviceCode
      // 
      this.colDeviceCode.HeaderText = "Device Code";
      this.colDeviceCode.Name = "colDeviceCode";
      this.colDeviceCode.ReadOnly = true;
      this.colDeviceCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colDeviceCode.Visible = false;
      this.colDeviceCode.Width = 75;
      // 
      // colTUInstanceName
      // 
      this.colTUInstanceName.HeaderText = "TU Instance";
      this.colTUInstanceName.Name = "colTUInstanceName";
      this.colTUInstanceName.ReadOnly = true;
      this.colTUInstanceName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colTUInstanceName.Visible = false;
      this.colTUInstanceName.Width = 72;
      // 
      // colTUTypeName
      // 
      this.colTUTypeName.HeaderText = "TU Type";
      this.colTUTypeName.Name = "colTUTypeName";
      this.colTUTypeName.ReadOnly = true;
      this.colTUTypeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colTUTypeName.Visible = false;
      this.colTUTypeName.Width = 55;
      // 
      // colDDDVersionName
      // 
      this.colDDDVersionName.HeaderText = "DDD Version";
      this.colDDDVersionName.Name = "colDDDVersionName";
      this.colDDDVersionName.ReadOnly = true;
      this.colDDDVersionName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colDDDVersionName.Visible = false;
      this.colDDDVersionName.Width = 75;
      // 
      // colRecTitle
      // 
      this.colRecTitle.HeaderText = "Record Title";
      this.colRecTitle.MinimumWidth = 100;
      this.colRecTitle.Name = "colRecTitle";
      this.colRecTitle.ReadOnly = true;
      this.colRecTitle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colRecTitle.Visible = false;
      // 
      // colFaultCode
      // 
      this.colFaultCode.HeaderText = "Fault Code";
      this.colFaultCode.Name = "colFaultCode";
      this.colFaultCode.ReadOnly = true;
      this.colFaultCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colFaultCode.Visible = false;
      this.colFaultCode.Width = 64;
      // 
      // colRecordName
      // 
      this.colRecordName.HeaderText = "Record Name";
      this.colRecordName.Name = "colRecordName";
      this.colRecordName.ReadOnly = true;
      this.colRecordName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colRecordName.Visible = false;
      this.colRecordName.Width = 79;
      // 
      // colTrainName
      // 
      this.colTrainName.HeaderText = "Train";
      this.colTrainName.Name = "colTrainName";
      this.colTrainName.ReadOnly = true;
      this.colTrainName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colTrainName.Visible = false;
      this.colTrainName.Width = 37;
      // 
      // colVehicleName
      // 
      this.colVehicleName.HeaderText = "Vehicle";
      this.colVehicleName.Name = "colVehicleName";
      this.colVehicleName.ReadOnly = true;
      this.colVehicleName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colVehicleName.Visible = false;
      this.colVehicleName.Width = 48;
      // 
      // colRecordType
      // 
      this.colRecordType.HeaderText = "Record Type";
      this.colRecordType.Name = "colRecordType";
      this.colRecordType.ReadOnly = true;
      this.colRecordType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colRecordType.Visible = false;
      this.colRecordType.Width = 75;
      // 
      // colFaultSeverity
      // 
      this.colFaultSeverity.HeaderText = "Fault Severity";
      this.colFaultSeverity.Name = "colFaultSeverity";
      this.colFaultSeverity.ReadOnly = true;
      this.colFaultSeverity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colFaultSeverity.Visible = false;
      this.colFaultSeverity.Width = 77;
      // 
      // colFaultSubSeverity
      // 
      this.colFaultSubSeverity.HeaderText = "Fault SubSeverity";
      this.colFaultSubSeverity.Name = "colFaultSubSeverity";
      this.colFaultSubSeverity.ReadOnly = true;
      this.colFaultSubSeverity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colFaultSubSeverity.Visible = false;
      this.colFaultSubSeverity.Width = 96;
      // 
      // colTrcSnpCode
      // 
      this.colTrcSnpCode.HeaderText = "Trace or Snap Code";
      this.colTrcSnpCode.Name = "colTrcSnpCode";
      this.colTrcSnpCode.ReadOnly = true;
      this.colTrcSnpCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colTrcSnpCode.Visible = false;
      this.colTrcSnpCode.Width = 109;
      // 
      // colGPSLatitude
      // 
      this.colGPSLatitude.HeaderText = "Latitude";
      this.colGPSLatitude.Name = "colGPSLatitude";
      this.colGPSLatitude.ReadOnly = true;
      this.colGPSLatitude.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colGPSLatitude.Visible = false;
      this.colGPSLatitude.Width = 51;
      // 
      // colGPSLongitude
      // 
      this.colGPSLongitude.HeaderText = "Longitude";
      this.colGPSLongitude.Name = "colGPSLongitude";
      this.colGPSLongitude.ReadOnly = true;
      this.colGPSLongitude.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colGPSLongitude.Visible = false;
      this.colGPSLongitude.Width = 60;
      // 
      // colRecTypeCount
      // 
      this.colRecTypeCount.HeaderText = "Type Count";
      this.colRecTypeCount.Name = "colRecTypeCount";
      this.colRecTypeCount.ReadOnly = true;
      this.colRecTypeCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
      this.colRecTypeCount.Visible = false;
      this.colRecTypeCount.Width = 68;
      // 
      // DRViewMaster_Slave
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.Controls.Add(this.panel2);
      this.Controls.Add(this.splitter1);
      this.Controls.Add(this.panel1);
      this.Name = "DRViewMaster_Slave";
      this.Size = new System.Drawing.Size(724, 475);
      ((System.ComponentModel.ISupportInitialize)(this.dgvRecords)).EndInit();
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.panel2.ResumeLayout(false);
      this.panel2.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.DataGridView dgvRecords;
    private HorizVariableGrid hvgVariables;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Splitter splitter1;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.DataGridViewTextBoxColumn colTUInstanceID;
    private System.Windows.Forms.DataGridViewTextBoxColumn colRecordInstID;
    private System.Windows.Forms.DataGridViewTextBoxColumn colDDDVersionID;
    private System.Windows.Forms.DataGridViewTextBoxColumn colRecordDefID;
    private System.Windows.Forms.DataGridViewTextBoxColumn colStartTime;
    private System.Windows.Forms.DataGridViewTextBoxColumn colEndTime;
    private System.Windows.Forms.DataGridViewTextBoxColumn colLastModified;
    private System.Windows.Forms.DataGridViewTextBoxColumn colSource;
    private System.Windows.Forms.DataGridViewTextBoxColumn colHierarchyItemID;
    private System.Windows.Forms.DataGridViewTextBoxColumn colImportTime;
    private System.Windows.Forms.DataGridViewTextBoxColumn colAcknowledgeTime;
    private System.Windows.Forms.DataGridViewTextBoxColumn colVehicleNumber;
    private System.Windows.Forms.DataGridViewTextBoxColumn colDeviceCode;
    private System.Windows.Forms.DataGridViewTextBoxColumn colTUInstanceName;
    private System.Windows.Forms.DataGridViewTextBoxColumn colTUTypeName;
    private System.Windows.Forms.DataGridViewTextBoxColumn colDDDVersionName;
    private System.Windows.Forms.DataGridViewTextBoxColumn colRecTitle;
    private System.Windows.Forms.DataGridViewTextBoxColumn colFaultCode;
    private System.Windows.Forms.DataGridViewTextBoxColumn colRecordName;
    private System.Windows.Forms.DataGridViewTextBoxColumn colTrainName;
    private System.Windows.Forms.DataGridViewTextBoxColumn colVehicleName;
    private System.Windows.Forms.DataGridViewTextBoxColumn colRecordType;
    private System.Windows.Forms.DataGridViewTextBoxColumn colFaultSeverity;
    private System.Windows.Forms.DataGridViewTextBoxColumn colFaultSubSeverity;
    private System.Windows.Forms.DataGridViewTextBoxColumn colTrcSnpCode;
    private System.Windows.Forms.DataGridViewTextBoxColumn colGPSLatitude;
    private System.Windows.Forms.DataGridViewTextBoxColumn colGPSLongitude;
    private System.Windows.Forms.DataGridViewTextBoxColumn colRecTypeCount;
  }
}
