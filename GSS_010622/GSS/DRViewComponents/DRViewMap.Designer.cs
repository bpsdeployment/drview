namespace DRViewComponents
{
  partial class DRViewMap
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DRViewMap));
      this.browser = new System.Windows.Forms.WebBrowser();
      this.panProp = new System.Windows.Forms.Panel();
      this.chbShowTUNames = new System.Windows.Forms.CheckBox();
      this.label2 = new System.Windows.Forms.Label();
      this.nudMinMarkerDist = new System.Windows.Forms.NumericUpDown();
      this.label1 = new System.Windows.Forms.Label();
      this.nudMaxRecCount = new System.Windows.Forms.NumericUpDown();
      this.btHideProp = new System.Windows.Forms.Button();
      this.btGoLastRec = new System.Windows.Forms.Button();
      this.btGoNextRec = new System.Windows.Forms.Button();
      this.btGoPrevRec = new System.Windows.Forms.Button();
      this.btGoFirstRec = new System.Windows.Forms.Button();
      this.lbRecords = new System.Windows.Forms.Label();
      this.tbRecords = new System.Windows.Forms.TrackBar();
      this.btShowProp = new System.Windows.Forms.Button();
      this.panProp.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudMinMarkerDist)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMaxRecCount)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.tbRecords)).BeginInit();
      this.SuspendLayout();
      // 
      // browser
      // 
      this.browser.Dock = System.Windows.Forms.DockStyle.Fill;
      this.browser.IsWebBrowserContextMenuEnabled = false;
      this.browser.Location = new System.Drawing.Point(0, 0);
      this.browser.Margin = new System.Windows.Forms.Padding(0);
      this.browser.MinimumSize = new System.Drawing.Size(20, 20);
      this.browser.Name = "browser";
      this.browser.ScriptErrorsSuppressed = true;
      this.browser.ScrollBarsEnabled = false;
      this.browser.Size = new System.Drawing.Size(350, 330);
      this.browser.TabIndex = 1;
      this.browser.WebBrowserShortcutsEnabled = false;
      this.browser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.browser_DocumentCompleted);
      // 
      // panProp
      // 
      this.panProp.BackColor = System.Drawing.Color.LightGray;
      this.panProp.Controls.Add(this.chbShowTUNames);
      this.panProp.Controls.Add(this.label2);
      this.panProp.Controls.Add(this.nudMinMarkerDist);
      this.panProp.Controls.Add(this.label1);
      this.panProp.Controls.Add(this.nudMaxRecCount);
      this.panProp.Controls.Add(this.btHideProp);
      this.panProp.Controls.Add(this.btGoLastRec);
      this.panProp.Controls.Add(this.btGoNextRec);
      this.panProp.Controls.Add(this.btGoPrevRec);
      this.panProp.Controls.Add(this.btGoFirstRec);
      this.panProp.Controls.Add(this.lbRecords);
      this.panProp.Controls.Add(this.tbRecords);
      this.panProp.Location = new System.Drawing.Point(0, 0);
      this.panProp.Name = "panProp";
      this.panProp.Size = new System.Drawing.Size(350, 113);
      this.panProp.TabIndex = 2;
      this.panProp.Visible = false;
      // 
      // chbShowTUNames
      // 
      this.chbShowTUNames.AutoSize = true;
      this.chbShowTUNames.Location = new System.Drawing.Point(213, 88);
      this.chbShowTUNames.Name = "chbShowTUNames";
      this.chbShowTUNames.Size = new System.Drawing.Size(105, 17);
      this.chbShowTUNames.TabIndex = 10;
      this.chbShowTUNames.Text = "Show TU names";
      this.chbShowTUNames.UseVisualStyleBackColor = true;
      this.chbShowTUNames.CheckedChanged += new System.EventHandler(this.chbShowTUNames_CheckedChanged);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(96, 71);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(108, 13);
      this.label2.TabIndex = 9;
      this.label2.Text = "Minimum distance [m]";
      // 
      // nudMinMarkerDist
      // 
      this.nudMinMarkerDist.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudMinMarkerDist.Location = new System.Drawing.Point(99, 87);
      this.nudMinMarkerDist.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
      this.nudMinMarkerDist.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.nudMinMarkerDist.Name = "nudMinMarkerDist";
      this.nudMinMarkerDist.Size = new System.Drawing.Size(78, 20);
      this.nudMinMarkerDist.TabIndex = 8;
      this.nudMinMarkerDist.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudMinMarkerDist.ValueChanged += new System.EventHandler(this.nudMinMarkerDist_ValueChanged);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(3, 71);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(72, 13);
      this.label1.TabIndex = 7;
      this.label1.Text = "Record count";
      // 
      // nudMaxRecCount
      // 
      this.nudMaxRecCount.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudMaxRecCount.Location = new System.Drawing.Point(3, 87);
      this.nudMaxRecCount.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
      this.nudMaxRecCount.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.nudMaxRecCount.Name = "nudMaxRecCount";
      this.nudMaxRecCount.Size = new System.Drawing.Size(72, 20);
      this.nudMaxRecCount.TabIndex = 6;
      this.nudMaxRecCount.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudMaxRecCount.ValueChanged += new System.EventHandler(this.nudMaxRecCount_ValueChanged);
      // 
      // btHideProp
      // 
      this.btHideProp.Image = ((System.Drawing.Image)(resources.GetObject("btHideProp.Image")));
      this.btHideProp.Location = new System.Drawing.Point(0, -1);
      this.btHideProp.Name = "btHideProp";
      this.btHideProp.Size = new System.Drawing.Size(23, 23);
      this.btHideProp.TabIndex = 4;
      this.btHideProp.UseVisualStyleBackColor = true;
      this.btHideProp.Click += new System.EventHandler(this.btHideProp_Click);
      // 
      // btGoLastRec
      // 
      this.btGoLastRec.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btGoLastRec.Image = ((System.Drawing.Image)(resources.GetObject("btGoLastRec.Image")));
      this.btGoLastRec.Location = new System.Drawing.Point(324, 41);
      this.btGoLastRec.Name = "btGoLastRec";
      this.btGoLastRec.Size = new System.Drawing.Size(23, 23);
      this.btGoLastRec.TabIndex = 5;
      this.btGoLastRec.UseVisualStyleBackColor = true;
      this.btGoLastRec.Click += new System.EventHandler(this.FirstRecIndxChanged);
      // 
      // btGoNextRec
      // 
      this.btGoNextRec.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btGoNextRec.Image = ((System.Drawing.Image)(resources.GetObject("btGoNextRec.Image")));
      this.btGoNextRec.Location = new System.Drawing.Point(295, 41);
      this.btGoNextRec.Name = "btGoNextRec";
      this.btGoNextRec.Size = new System.Drawing.Size(23, 23);
      this.btGoNextRec.TabIndex = 4;
      this.btGoNextRec.UseVisualStyleBackColor = true;
      this.btGoNextRec.Click += new System.EventHandler(this.FirstRecIndxChanged);
      // 
      // btGoPrevRec
      // 
      this.btGoPrevRec.Image = ((System.Drawing.Image)(resources.GetObject("btGoPrevRec.Image")));
      this.btGoPrevRec.Location = new System.Drawing.Point(32, 41);
      this.btGoPrevRec.Name = "btGoPrevRec";
      this.btGoPrevRec.Size = new System.Drawing.Size(23, 23);
      this.btGoPrevRec.TabIndex = 3;
      this.btGoPrevRec.UseVisualStyleBackColor = true;
      this.btGoPrevRec.Click += new System.EventHandler(this.FirstRecIndxChanged);
      // 
      // btGoFirstRec
      // 
      this.btGoFirstRec.Image = ((System.Drawing.Image)(resources.GetObject("btGoFirstRec.Image")));
      this.btGoFirstRec.Location = new System.Drawing.Point(3, 41);
      this.btGoFirstRec.Name = "btGoFirstRec";
      this.btGoFirstRec.Size = new System.Drawing.Size(23, 23);
      this.btGoFirstRec.TabIndex = 2;
      this.btGoFirstRec.UseVisualStyleBackColor = true;
      this.btGoFirstRec.Click += new System.EventHandler(this.FirstRecIndxChanged);
      // 
      // lbRecords
      // 
      this.lbRecords.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.lbRecords.AutoSize = true;
      this.lbRecords.Location = new System.Drawing.Point(155, 4);
      this.lbRecords.Name = "lbRecords";
      this.lbRecords.Size = new System.Drawing.Size(40, 13);
      this.lbRecords.TabIndex = 1;
      this.lbRecords.Text = "0 - 100";
      this.lbRecords.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // tbRecords
      // 
      this.tbRecords.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.tbRecords.LargeChange = 100;
      this.tbRecords.Location = new System.Drawing.Point(61, 33);
      this.tbRecords.Maximum = 5000;
      this.tbRecords.Name = "tbRecords";
      this.tbRecords.Size = new System.Drawing.Size(228, 45);
      this.tbRecords.TabIndex = 0;
      this.tbRecords.TickFrequency = 1000;
      this.tbRecords.ValueChanged += new System.EventHandler(this.tbRecords_ValueChanged);
      // 
      // btShowProp
      // 
      this.btShowProp.Image = ((System.Drawing.Image)(resources.GetObject("btShowProp.Image")));
      this.btShowProp.Location = new System.Drawing.Point(0, 0);
      this.btShowProp.Name = "btShowProp";
      this.btShowProp.Size = new System.Drawing.Size(23, 23);
      this.btShowProp.TabIndex = 3;
      this.btShowProp.UseVisualStyleBackColor = true;
      this.btShowProp.Click += new System.EventHandler(this.btShowProp_Click);
      // 
      // DRViewMap
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.panProp);
      this.Controls.Add(this.btShowProp);
      this.Controls.Add(this.browser);
      this.MinimumSize = new System.Drawing.Size(350, 330);
      this.Name = "DRViewMap";
      this.Size = new System.Drawing.Size(350, 330);
      this.panProp.ResumeLayout(false);
      this.panProp.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudMinMarkerDist)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMaxRecCount)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.tbRecords)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.WebBrowser browser;
    private System.Windows.Forms.Panel panProp;
    private System.Windows.Forms.TrackBar tbRecords;
    private System.Windows.Forms.Button btGoFirstRec;
    private System.Windows.Forms.Label lbRecords;
    private System.Windows.Forms.Button btGoPrevRec;
    private System.Windows.Forms.Button btGoLastRec;
    private System.Windows.Forms.Button btGoNextRec;
    private System.Windows.Forms.Button btShowProp;
    private System.Windows.Forms.Button btHideProp;
    private System.Windows.Forms.NumericUpDown nudMaxRecCount;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.NumericUpDown nudMinMarkerDist;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.CheckBox chbShowTUNames;
  }
}
