namespace DRViewComponents
{
  partial class GroupedViewSelDlg
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btnOK = new System.Windows.Forms.Button();
      this.chlbRecAttributes = new System.Windows.Forms.CheckedListBox();
      this.varTreeView = new DRViewComponents.VarTreeView();
      this.btnCancel = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // btnOK
      // 
      this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnOK.Location = new System.Drawing.Point(91, 300);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(75, 23);
      this.btnOK.TabIndex = 0;
      this.btnOK.Text = "OK";
      this.btnOK.UseVisualStyleBackColor = true;
      // 
      // chlbRecAttributes
      // 
      this.chlbRecAttributes.FormattingEnabled = true;
      this.chlbRecAttributes.Location = new System.Drawing.Point(12, 12);
      this.chlbRecAttributes.Name = "chlbRecAttributes";
      this.chlbRecAttributes.Size = new System.Drawing.Size(127, 274);
      this.chlbRecAttributes.TabIndex = 1;
      // 
      // varTreeView
      // 
      this.varTreeView.DDDHelper = null;
      this.varTreeView.Location = new System.Drawing.Point(160, 12);
      this.varTreeView.Name = "varTreeView";
      this.varTreeView.ShowBits = true;
      this.varTreeView.Size = new System.Drawing.Size(259, 274);
      this.varTreeView.TabIndex = 2;
      this.varTreeView.UseFilter = true;
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(208, 300);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 3;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      // 
      // GroupedViewSelDlg
      // 
      this.AcceptButton = this.btnOK;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(431, 335);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.varTreeView);
      this.Controls.Add(this.chlbRecAttributes);
      this.Controls.Add(this.btnOK);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "GroupedViewSelDlg";
      this.Text = "Select columns";
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnOK;
    private System.Windows.Forms.CheckedListBox chlbRecAttributes;
    private VarTreeView varTreeView;
    private System.Windows.Forms.Button btnCancel;
  }
}