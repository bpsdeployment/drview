using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using DRSelectorComponents;
using DSDiagnosticRecords;
using DDDObjects;

namespace DRViewComponents
{
  /// <summary>
  /// User controls displaying values of enviromental variables in horizontal data grid view.
  /// </summary>
  public partial class HorizVariableGrid : UserControl
  {
    [Flags]
    public enum eVisibleColumns
    {
      None = 0,
      ColTUName = 2,
      ColUserVersion = 4,
      ColVariable = 8,
      ColUnit = 16
    }

    [Flags]
    public enum eVisibleRows
    {
      None = 0,
      Header = 2,
      RelTime = 4
    }

    /// <summary>
    /// Represents units used for displaying of relative time in first row
    /// </summary>
    public enum eRelTimeUnit
    {
      ms = 1,
      sec = 1000,
      min = 60 * 1000,
      sec_ms = 2,
      min_sec_ms = 3
    }

    #region Private mebers
    protected DDDHelper dddHelper;  //class providing DDD objects functionality
    protected DateTime startTime = DateTime.MinValue; //Absolut starting time to which all variable timestamps are related
    private SortedList<String, VarRowStruct> mapVarRow; //Maps each variable to one row in grid
    private SortedList<DateTime, int> mapTimeCol; //Maps each timestamp to one column in the grid
    private SortedList<String, VarRowStruct> mapBitsetRowStruct; //Maps bitset variables to their representative RowStruct object
    protected eVisibleColumns visibleColumns = eVisibleColumns.ColVariable | eVisibleColumns.ColUnit;
    protected eVisibleRows visibleRows = eVisibleRows.RelTime;
    protected eRelTimeUnit relTimeUnit = eRelTimeUnit.sec_ms; //Selected unit for relative time
    protected const int startingValRowIndx = 1; //Index of first row displaying actual variable values
    protected const int startingValColIndx = 4; //Index of first column displaying actual variable values
    protected const int timeRowIndx = 0;
    protected const int tuNameColIndx = 0;
    protected const int userVerColIndx = 1;
    protected const int varNameColIndx = 2;
    protected const int unitColIndx = 3;
    protected Color headerColor = SystemColors.Control;
    #endregion //Private mebers

    #region Public properties
    /// <summary>
    /// DDD objects functionality
    /// </summary>
    public DDDHelper DDDHelper
    {
      set { dddHelper = value; }
      get { return dddHelper; }
    }

    /// <summary>
    /// Absolut starting time to which all variable timestamps are related
    /// </summary>
    public DateTime StartTime
    {
      set { startTime = value; }
      get { return startTime; }
    }

    /// <summary>
    /// Visible frozen columns
    /// </summary>
    public eVisibleColumns VisibleColumns
    {
      get { return visibleColumns; }
      set { visibleColumns = value; SetVisibleColsRows(); }
    }

    /// <summary>
    /// Visible frozen rows
    /// </summary>
    public eVisibleRows VisibleRows
    {
      get { return visibleRows; }
      set { visibleRows = value; SetVisibleColsRows(); }
    }

    public Color HeaderColor
    {
      get { return headerColor; }
      set { headerColor = value; }
    }

    /// <summary>
    /// Unit used to display relative time in first row
    /// </summary>
    public eRelTimeUnit RelTimeUnit
    {
      get { return relTimeUnit; }
      set 
      {
        if (value != relTimeUnit)
        {
          relTimeUnit = value;
          DisplayRelTime(value);
        }
      }
    }

    /// <summary>
    /// Attach this handler to the source of NewLanguageSelected event
    /// </summary>
    public NewLanguageSelectedEventHandler NewLanguageHandler;
    #endregion //Public properties

    #region Construction and initialization
    /// <summary>
    /// Default constructor
    /// </summary>
    public HorizVariableGrid()
    {
      InitializeComponent();
      mapVarRow = new SortedList<string, VarRowStruct>();
      mapTimeCol = new SortedList<DateTime, int>();
      mapBitsetRowStruct = new SortedList<string, VarRowStruct>();
      NewLanguageHandler = this.ChangeLanguage;
      InitializeGrid();
    }
    #endregion //Construction and initialization

    #region Public methods
    /// <summary>
    /// Removes all records from the grid
    /// </summary>
    public void ClearGrid()
    {
      InitializeGrid();
    }

    /// <summary>
    /// Adds new records to grid
    /// </summary>
    /// <param name="varValues">List of variable values</param>
    /// <param name="dddVersionID">Identifier of DDD Version</param>    
    /// <param name="recordDefID">Identifier of record type</param>    
    public void AddRecords(List<VarValue> varValues, Guid dddVersionID, int recordDefID)
    {
      dgvValues.Visible = false;
      //Create necessary columns and rows to hold values
      InsertRowsAndCols(varValues, dddVersionID, recordDefID);
      //Fill values to grid
      FillValuesToGrid(varValues);
      dgvValues.Visible = true;
    }

    /// <summary>
    /// Adjust widths of all columns in the grid to fit the content
    /// </summary>
    public void ResizeColumns()
    {
      //Set columns widths
      dgvValues.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader);
    }
    #endregion //Public methods

    #region Private helper methods
    /// <summary>
    /// Adds rows and columns to grid that can hold values of passed records
    /// </summary>
    /// <param name="varValues">List of variable values</param>
    /// <param name="dddVersionID">Identifier of DDD Version</param>    
    /// <param name="recordDefID">Identifier of record type</param>    
    protected void InsertRowsAndCols(List<VarValue> varValues, Guid dddVersionID, int recordDefID)
    {
      int colIndex = -1;
      VarRowStruct rowStruct;
      String varIDString;
      DDDElemVar variable;

      //First walk through all the rows and add times and variables to maps
      foreach (VarValue varValue in varValues)
      {
        //Check if timestamp exists in mapTimeCol
        if (!mapTimeCol.TryGetValue(varValue.TimeStamp, out colIndex))
        { //Add timestamp to map
          mapTimeCol.Add(varValue.TimeStamp, -1);
        }
        //Check if variable exists in mapVarRow
        variable = dddHelper.GetVariable(varValue.DDDVersionID, varValue.VariableID) as DDDElemVar;
        varIDString = DDDVariable.GetVarIDString(varValue.TUInstanceID, varValue.DDDVersionID, varValue.VariableID);
        if (variable.IsBitSet() && !mapBitsetRowStruct.TryGetValue(varIDString, out rowStruct))
        { //Bitset variable - add each member to map and parent to bitset map
          //Get used bit masks
          DDDBitsetRepres bitsetRepres = (DDDBitsetRepres)variable.Representation;
          ulong[] masks = bitsetRepres.GetUsedBitMasks();
          String bitIDString;
          foreach (Int64 mask in masks)
          { //For each mask add new row
            bitIDString = DDDBitsetRepres.GetBitIDString((ulong)mask, varIDString); //Get string identifying the bit
            rowStruct = new VarRowStruct(-1, dddHelper.GetTUInstance(varValue.TUInstanceID),  dddHelper.GetVersion(varValue.DDDVersionID),
                                         dddHelper.GetRecord(varValue.DDDVersionID, recordDefID), variable, mask);
            mapVarRow.Add(bitIDString, rowStruct);
          }
          //Now add representative RowStruct to map for Bitset variables
          rowStruct = new VarRowStruct(-1, dddHelper.GetTUInstance(varValue.TUInstanceID),  dddHelper.GetVersion(varValue.DDDVersionID),
                                       dddHelper.GetRecord(varValue.DDDVersionID, recordDefID), variable, 0);
          mapBitsetRowStruct.Add(varIDString, rowStruct);
        }
        else if (!variable.IsBitSet() && !mapVarRow.TryGetValue(varIDString, out rowStruct)) //Ordinary variable
        { //Add rowStruct to map
          //Create new row struct
          rowStruct = new VarRowStruct(-1, dddHelper.GetTUInstance(varValue.TUInstanceID), dddHelper.GetVersion(varValue.DDDVersionID),
                                       dddHelper.GetRecord(varValue.DDDVersionID, recordDefID), variable, 0);
          mapVarRow.Add(varIDString, rowStruct);
        }
      }
      //Walk through column sorted list, add necessary columns to the grid and correct column indexes
      for (int i = 0; i < mapTimeCol.Count; i++)
      {
        if (mapTimeCol.Values[i] == -1)
        { //no column was assigned yet, insert new one
          //Create new column
          DataGridViewTextBoxColumn newCol = new DataGridViewTextBoxColumn();
          newCol.HeaderText = "";
          newCol.Name = mapTimeCol.Keys[i].Ticks.ToString();
          newCol.ReadOnly = true;
          newCol.SortMode = DataGridViewColumnSortMode.NotSortable;
          //Insert column
          dgvValues.Columns.Insert(i + startingValColIndx, newCol);
        }
        //Set or correct column index
        mapTimeCol[mapTimeCol.Keys[i]] = i + startingValColIndx;
      }
      //Set values of relative time row
      DisplayRelTime(RelTimeUnit);

      //Walk trough row sorted list, add necessary rows to the grid and correct row indexes
      for (int i = 0; i < mapVarRow.Count; i++)
      {
        //if (mapVarRow.Values[i].RowIndex == -1)
        if (mapVarRow[mapVarRow.Keys[i]].RowIndex == -1)
        { //no row was assigned yet, insert new one
          //insert row
          dgvValues.Rows.Insert(i + startingValRowIndx, 1);
          //Set text for fixed columns
          dgvValues[tuNameColIndx, i + startingValRowIndx].Value = mapVarRow[mapVarRow.Keys[i]].tuInstance.TUName;
          dgvValues[userVerColIndx, i + startingValRowIndx].Value = mapVarRow[mapVarRow.Keys[i]].dddVersion.UserVersion;
          dgvValues[varNameColIndx, i + startingValRowIndx].Value = mapVarRow[mapVarRow.Keys[i]].dddVariable.GetCompleteVarTitle((ulong)mapVarRow[mapVarRow.Keys[i]].bitMask);
          dgvValues[unitColIndx, i + startingValRowIndx].Value = "[" + mapVarRow[mapVarRow.Keys[i]].dddVariable.GetUnitSymbol() + "]";
        }
        mapVarRow[mapVarRow.Keys[i]].RowIndex = i + startingValRowIndx;
      }
    }

    /// <summary>
    /// Removes all rows and columns from the grid.
    /// Creates fixed columns and TimeStamp row
    /// </summary>
    private void InitializeGrid()
    {
      //Remove all records from internal sorted lists
      mapTimeCol.Clear();
      mapVarRow.Clear();
      mapBitsetRowStruct.Clear();
      //Remove all rows and columns from the grid
      dgvValues.Rows.Clear();
      dgvValues.Columns.Clear();
      //Create and initialize fixed columns
      DataGridViewTextBoxColumn colTUName = new DataGridViewTextBoxColumn();
      DataGridViewTextBoxColumn colUserVersion = new DataGridViewTextBoxColumn();
      DataGridViewTextBoxColumn colVarName = new DataGridViewTextBoxColumn();
      DataGridViewTextBoxColumn colUnit = new DataGridViewTextBoxColumn();
      colTUName.Frozen = true;
      colTUName.HeaderText = "TU Name";
      colTUName.Name = "colTUName";
      colTUName.ReadOnly = true;
      colTUName.DefaultCellStyle.BackColor = headerColor;
      colTUName.SortMode = DataGridViewColumnSortMode.NotSortable;
      colUserVersion.Frozen = true;
      colUserVersion.HeaderText = "DDD Version";
      colUserVersion.Name = "colUserVersion";
      colUserVersion.ReadOnly = true;
      colUserVersion.DefaultCellStyle.BackColor = headerColor;
      colUserVersion.SortMode = DataGridViewColumnSortMode.NotSortable;
      colVarName.Frozen = true;
      colVarName.HeaderText = "Variable";
      colVarName.Name = "colVarName";
      colVarName.ReadOnly = true;
      colVarName.DefaultCellStyle.BackColor = headerColor;
      colVarName.SortMode = DataGridViewColumnSortMode.Programmatic;
      colUnit.Frozen = true;
      colUnit.HeaderText = "Unit";
      colUnit.Name = "colUnit";
      colUnit.ReadOnly = true;
      colUnit.DefaultCellStyle.BackColor = headerColor;
      colUnit.SortMode = DataGridViewColumnSortMode.NotSortable;
      //Add columns to DataGrid
      dgvValues.Columns.Add(colTUName);
      dgvValues.Columns.Add(colUserVersion);
      dgvValues.Columns.Add(colVarName);
      dgvValues.Columns.Add(colUnit);
      //Add relative time row
      dgvValues.Rows.Add(new Object[] {"", "", "Relative time", "[ms]"});
      dgvValues.Rows[timeRowIndx].Frozen = true;
      dgvValues.Rows[timeRowIndx].DefaultCellStyle.BackColor = headerColor;
      //Set frozen objects visibility
      SetVisibleColsRows();
    }

    /// <summary>
    /// Sets visibility of frozen rows and columns
    /// </summary>
    protected void SetVisibleColsRows()
    {
      dgvValues.ColumnHeadersVisible = (visibleRows & eVisibleRows.Header) != 0;
      dgvValues.Rows[timeRowIndx].Visible = (visibleRows & eVisibleRows.RelTime) != 0;
      dgvValues.Columns[tuNameColIndx].Visible = (visibleColumns & eVisibleColumns.ColTUName) != 0;
      dgvValues.Columns[userVerColIndx].Visible = (visibleColumns & eVisibleColumns.ColUserVersion) != 0;
      dgvValues.Columns[varNameColIndx].Visible = (visibleColumns & eVisibleColumns.ColVariable) != 0;
      dgvValues.Columns[unitColIndx].Visible = (visibleColumns & eVisibleColumns.ColUnit) != 0;
    }

    /// <summary>
    /// Method changes all language dependent strings in the grid
    /// </summary>
    /// <param name="language">Currently selected language</param>
    protected void ChangeLanguage(Languages language)
    {
      //Change all variable titles according to selected language
      //Assume new language is already assigned to dddHelper
      foreach (KeyValuePair<String, VarRowStruct> rowStruct in mapVarRow)
        //Set new variable title for each row
        dgvValues[varNameColIndx, rowStruct.Value.RowIndex].Value = 
          rowStruct.Value.dddVariable.GetCompleteVarTitle((ulong)rowStruct.Value.bitMask);
    }

    /// <summary>
    /// Fills grid cells with actual values from passed rows
    /// </summary>
    /// <param name="varValues">List of variable values</param>
    protected void FillValuesToGrid(List<VarValue> varValues)
    {
      VarRowStruct rowStruct;
      int colIndex;
      String varIDString;
      String bitIDString;
      String formatedVal;
      foreach (VarValue varValue in varValues)
      { //Iterate over all passed rows
        //Obtain column index and row structure
        if (!mapTimeCol.TryGetValue(varValue.TimeStamp, out colIndex))
          continue; //no column created for specified timestamp
        //Build string identifying the variable
        varIDString = DDDVariable.GetVarIDString(varValue.TUInstanceID, varValue.DDDVersionID, varValue.VariableID);
        if (mapVarRow.TryGetValue(varIDString, out rowStruct))
        { //found row for simple variable - set cell text
          formatedVal = rowStruct.dddVariable.FormatValue(varValue.Value);
          dgvValues[colIndex, rowStruct.RowIndex].Value = formatedVal;
        }
        else if (mapBitsetRowStruct.TryGetValue(varIDString, out rowStruct))
        { //variable is bitset - find row for each member
          KeyValuePair<ulong, String>[] pairs;
          //obtain array of masks and bit values
          pairs = ((DDDBitsetRepres)rowStruct.dddVariable.Representation).GetMasksAndValues(varValue.Value);
          foreach (KeyValuePair<ulong, String> pair in pairs)
          { //iterate over each used mask, find proper row and set cell text
            bitIDString = DDDBitsetRepres.GetBitIDString(pair.Key, varIDString); //Get string identifying the bit
            if (mapVarRow.TryGetValue(bitIDString, out rowStruct))
            {
              dgvValues[colIndex, rowStruct.RowIndex].Value = pair.Value;
            }
          } //end foreach pair
        } //end bitset variable
      } // end foreach row
    }

    /// <summary>
    /// Displays relative time in correct unit
    /// </summary>
    /// <param name="unit">Unit used to display relative time</param>
    private void DisplayRelTime(eRelTimeUnit unit)
    {
      //Walk through column sorted list, add necessary columns to the grid and correct column indexes
      for (int i = 0; i < mapTimeCol.Count; i++)
      {
        if (mapTimeCol.Values[i] == -1)
          continue; //no column was assigned
        //Compute relative time and use correct unit
        TimeSpan relTime = mapTimeCol.Keys[i] - StartTime;
        if ((relTime.Ticks == 0) && (mapTimeCol.Count > 1))
        { //Column representing cuurent StartTime - highlight and format with complete time
          dgvValues[i + startingValColIndx, timeRowIndx].Value = DDDHelper.FormatTime(StartTime);
          dgvValues.Columns[i + startingValColIndx].DefaultCellStyle.BackColor = Color.LightGreen;
          dgvValues[i + startingValColIndx, timeRowIndx].Style.BackColor = Color.LightGreen;
        }
        else if (unit == eRelTimeUnit.sec_ms)
          dgvValues[i + startingValColIndx, timeRowIndx].Value = ((int)relTime.TotalSeconds).ToString() + "." + Math.Abs(relTime.Milliseconds).ToString("000");
        else if (unit == eRelTimeUnit.min_sec_ms)
          dgvValues[i + startingValColIndx, timeRowIndx].Value = 
            ((int)relTime.TotalMinutes).ToString() + ":" 
            + Math.Abs(relTime.Seconds).ToString("00") + "." 
            + Math.Abs(relTime.Milliseconds).ToString("000");
        else
          dgvValues[i + startingValColIndx, timeRowIndx].Value = (Int64)relTime.TotalMilliseconds / (Int64)unit;
      }
      if (unit == eRelTimeUnit.min_sec_ms)
        dgvValues[unitColIndx, timeRowIndx].Value = "";
      else if (unit == eRelTimeUnit.sec_ms)
        dgvValues[unitColIndx, timeRowIndx].Value = "[" + eRelTimeUnit.sec.ToString() + "]";
      else
        dgvValues[unitColIndx, timeRowIndx].Value = "[" + unit.ToString() +"]";
    }
    #endregion //Private helper methods

    #region Internal event handlers

    private void VisibleColMenuItem_CheckedChanged(object sender, EventArgs e)
    {
      eVisibleColumns visible = eVisibleColumns.None;
      if (tUNameToolStripMenuItem.Checked)
        visible |= eVisibleColumns.ColTUName;
      if (dDDVersionToolStripMenuItem.Checked)
        visible |= eVisibleColumns.ColUserVersion;
      if (variableTitleToolStripMenuItem.Checked)
        visible |= eVisibleColumns.ColVariable;
      if (physicalUintToolStripMenuItem.Checked)
        visible |= eVisibleColumns.ColUnit;
      VisibleColumns = visible;
    }

    private void VisibleRowMenuItem_Click(object sender, EventArgs e)
    {
      eVisibleRows visible = eVisibleRows.None;
      if (headerToolStripMenuItem.Checked)
        visible |= eVisibleRows.Header;
      if (relativeTimeToolStripMenuItem.Checked)
        visible |= eVisibleRows.RelTime;
      VisibleRows = visible;
    }

    private void setRelTimeUnitToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (sender == millisecondsToolStripMenuItem)
        RelTimeUnit = eRelTimeUnit.ms;
      else if (sender == secondsToolStripMenuItem)
        RelTimeUnit = eRelTimeUnit.sec;
      else if (sender == minutesToolStripMenuItem)
        RelTimeUnit = eRelTimeUnit.min;
      else if (sender == secondsMillisecondsToolStripMenuItem)
        RelTimeUnit = eRelTimeUnit.sec_ms;
      else if (sender == minSecmsToolStripMenuItem)
        RelTimeUnit = eRelTimeUnit.min_sec_ms;
    }
    #endregion //Internal event handlers
  }

  /// <summary>
  /// Class holding necessary common data for each value row in the grid
  /// </summary>
  internal class VarRowStruct
  {
    public int RowIndex;
    public TUInstance tuInstance;
    public DDDVersion dddVersion;
    public DDDRecord dddRecord;
    public DDDElemVar dddVariable;
    public Int64 bitMask; //Mask for bits of bitstring variables

    public VarRowStruct(int rowIndex, TUInstance tuInst, DDDVersion dddVer, DDDRecord dddRec, DDDElemVar dddVar, Int64 mask)
    {
      RowIndex = rowIndex;
      tuInstance = tuInst;
      dddVersion = dddVer;
      dddRecord = dddRec;
      dddVariable = dddVar;
      bitMask = mask;
    }
  }
}
