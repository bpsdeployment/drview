using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace DRViewComponents
{
  /// <summary>
  /// Delegate defines event handler for FillNode event
  /// </summary>
  /// <param name="node">Node to fill</param>
  public delegate void FillNodeHandler(TreeListNode node);

  /// <summary>
  /// Component displaying tree together with list which can contain additional
  /// info for each node in the tree.
  /// </summary>
  public partial class TreeListView : UserControl
  {
    #region Construction initialization
    public TreeListView()
    {
      InitializeComponent();
    }
    #endregion //Construction initialization

    #region Public properties
    public TreeListViewItemCollection Items
    {
      get { return view.Items; }
    }

    /// <summary>
    /// Event raised before expanding node with not null Tag.
    /// Node shall be filled with necessary children.
    /// </summary>
    public event FillNodeHandler FillNodeEvent;
    #endregion //Construction initialization

    #region Public methods
    /// <summary>
    /// Removes all visible items from visual objects
    /// </summary>
    public void ClearItems()
    {
      view.Items.Clear();
    }

    /// <summary>
    /// Adds new visible items to the component. Items are added collapsed at the end.
    /// </summary>
    /// <param name="nodes">Collection of nodes to add</param>
    public void AddItems(List<TreeListNode> nodes)
    {
      view.Items.Sortable = false;
      foreach (TreeListNode item in nodes)
        view.Items.Add(item);
      //view.Items.AddRange(nodes);
    }
    #endregion //Public methods

    #region EventHandlers
    private void view_SizeChanged(object sender, EventArgs e)
    {
      ResizeColumns();
    }

    /// <summary>
    /// For nodes with Tag memeber not null, raise FillNode event before expanding
    /// </summary>
    private void view_BeforeExpand(object sender, TreeListViewCancelEventArgs e)
    {
      try
      {
        TreeListNode node = e.Item as TreeListNode;
        if (node.Tag != null)
        { //Remove dummy node, raise FillNode event
          node.Nodes.Clear();
          if (FillNodeEvent != null)
            FillNodeEvent(node);
        }
      }
      catch (Exception)
      { }
    }

    /// <summary>
    /// For nodes with Tag memeber not null, clear all children after collapsing and add dummy node
    /// </summary>
    private void view_AfterCollapse(object sender, TreeListViewEventArgs e)
    {
      try
      {
        TreeListNode node = e.Item as TreeListNode;
        if (node.Tag != null)
        { //clear children, add dummy
          node.Nodes.Clear();
          node.AddDummyNode();
        }
      }
      catch (Exception)
      { }
    }
    #endregion //EventHandlers

    #region Private methods
    private void ResizeColumns()
    {
      colNodes.Width = view.Width / 3;
      colDetail1.Width = view.Width / 3;
      colDetail2.Width = view.Width / 3;
    }
    #endregion //Private methods
  }

  #region Helper classes
  /// <summary>
  /// Enumerates all possible images available in 
  /// TreeListView for nodes
  /// </summary>
  public enum eTreeListImage
  {
    Default = 0,
    Selected = 1,
    EventRec = 2,
    TraceRec = 3,
    SnapRec = 4,
    Attributes = 5,
    FaultCode = 6,
    Time = 7,
    TUName = 8,
    TUType = 9,
    DDDVersion = 10,
    GenAttribute = 11,
    EnvData = 12,
    SimpleInput = 13,
    HistInput = 14,
    Array = 15,
    Structure = 16,
    ElemVar = 17,
    Train = 18,
    Vehicle = 19
  }

  /// <summary>
  /// Class represents one node in the component. 
  /// Each node contains text for three displayed columns
  /// </summary>
  public class TreeListNode : TreeListViewItem, IComparable<TreeListNode>
  {
    protected String sortKey;

    /// <summary>
    /// Publish nested items as "Nodes"
    /// </summary>
    public TreeListViewItemCollection Nodes
    {
      get { return Items; }
    }

    /// <summary>
    /// Creates new node with specified parameters.
    /// </summary>
    /// <param name="text">Text displayed in tree view</param>
    /// <param name="detail1">Text displayed in first column of list view</param>
    /// <param name="detail2">Text displayed in second column of list view</param>
    /// <param name="image">Index of image displayed with the node</param>
    /// <param name="sortKey">Key for sorting the nodes</param>
    public TreeListNode(String text, String detail1, String detail2, eTreeListImage image, String sortKey)
    {
      CreateNode(text, detail1, detail2, image, sortKey);
    }

    /// <summary>
    /// Creates new node with specified parameters.
    /// </summary>
    /// <param name="text">Text displayed in tree view</param>
    /// <param name="detail1">Text displayed in first column of list view</param>
    /// <param name="detail2">Text displayed in second column of list view</param>
    /// <param name="image">Index of image displayed with the node</param>
    public TreeListNode(String text, String detail1, String detail2, eTreeListImage image)
    {
      CreateNode(text, detail1, detail2, image, "");
    }

    /// <summary>
    /// Creates new node with specified parameters. Stores passed object in Tag member and adds one child dummy node
    /// </summary>
    /// <param name="text">Text displayed in tree view</param>
    /// <param name="detail1">Text displayed in first column of list view</param>
    /// <param name="detail2">Text displayed in second column of list view</param>
    /// <param name="image">Index of image displayed with the node</param>
    /// <param name="tag">Object stored in tag member</param>
    public TreeListNode(String text, String detail1, String detail2, eTreeListImage image, Object tag)
    {
      CreateNode(text, detail1, detail2, image, "");
      this.Tag = tag;
      AddDummyNode();
    }

    protected void CreateNode(String text, String detail1, String detail2, eTreeListImage image, String sortKey)
    {
      this.Text = text;
      //Create list view item
      this.SubItems.Add(detail1);
      this.SubItems.Add(detail2);
      this.ImageIndex = (int)image;
      //Store sort key
      this.sortKey = sortKey;
    }

    public int CompareTo(TreeListNode other)
    {
      return sortKey.CompareTo(other.sortKey);
    }

    /// <summary>
    /// Add one dummy node (just to display + sign)
    /// </summary>
    public void AddDummyNode()
    {
      Nodes.Add(new TreeListNode("Dummy", "", "", eTreeListImage.Default));
    }
  }

  /// <summary>
  /// Represents one diagnostic record in tree list view
  /// </summary>
  public class RecordTreeListNode : TreeListNode
  {

    /// <summary>
    /// Creates new diagnostic record node with specified parameters.
    /// </summary>
    /// <param name="text">Text displayed in tree view</param>
    /// <param name="detail1">Text displayed in first column of list view</param>
    /// <param name="detail2">Text displayed in second column of list view</param>
    /// <param name="image">Index of image displayed with the node</param>
    /// <param name="sortKey">Key for sorting the nodes</param>
    public RecordTreeListNode(String text, String detail1, String detail2, eTreeListImage image, String sortKey)
      : base(text, detail1, detail2, image, sortKey)
    {
    }
  }

  /// <summary>
  /// Represents fleet hierarchy item in tree list view
  /// </summary>
  public class HierarchyTreeListNode : TreeListNode
  {
    protected DateTime minRecTime = DateTime.MaxValue; //Time of oldest record under this hierarchy item
    protected DateTime maxRecTime = DateTime.MinValue; //Time of newest record under this hierarchy item
    protected int recCount = 0; //Total number of records under this hierarchy item

    /// <summary>
    /// Creates new hierarchy item node with specified parameters.
    /// </summary>
    /// <param name="text">Text displayed in tree view</param>
    /// <param name="detail1">Text displayed in first column of list view</param>
    /// <param name="detail2">Text displayed in second column of list view</param>
    /// <param name="image">Index of image displayed with the node</param>
    /// <param name="sortKey">Key for sorting the nodes</param>
    public HierarchyTreeListNode(String text, String detail1, String detail2, eTreeListImage image, String sortKey)
      : base(text, detail1, detail2, image, sortKey)
    {
    }

    /// <summary>
    /// Add record node under this hierarchy node
    /// </summary>
    /// <param name="recNode">Record node to add</param>
    /// <param name="startTime">Start time of given record</param>
    public void AddRecordNode(RecordTreeListNode recNode, DateTime startTime)
    {
      //Add to collection of child nodes
      this.Nodes.Add(recNode);
      //Update subitems
      UpdateSubitems(startTime);
    }

    /// <summary>
    /// Updates subitems with number of records and min and max record times.
    /// </summary>
    /// <param name="startTime">Start time of added record</param>
    private void UpdateSubitems(DateTime startTime)
    {
      //Store minimum and maximum record time
      if (startTime < minRecTime)
      {
        minRecTime = startTime;
        this.SubItems[2].Text = this.minRecTime.ToString("yyyy-MM-dd HH:mm") + " - " + this.maxRecTime.ToString("yyyy-MM-dd HH:mm");
      }
      else if (startTime > maxRecTime)
      {
        maxRecTime = startTime;
        this.SubItems[2].Text = this.minRecTime.ToString("yyyy-MM-dd HH:mm") + " - " + this.maxRecTime.ToString("yyyy-MM-dd HH:mm");
      }
      //Modify subitem with record count
      this.recCount++;
      this.SubItems[1].Text = recCount.ToString() + " records";
      //Update parent if hierarchy item
      if (this.Parent != null && this.Parent.GetType() == typeof(HierarchyTreeListNode))
        ((HierarchyTreeListNode)this.Parent).UpdateSubitems(startTime);
    }
  }
  #endregion //Helper classes
}
